#!/bin/bash

file=$1
basepath="$(dirname "$(pwd)")"
basepath+=/public

name=${file%%.*}

oldVideo="EssayFileName"
newVideo=$name
origFileName="$basepath/FacebookShareEssay/FacebookSharing.html"
newFileName="$basepath/FacebookShareEssay/$name.html"

cp -f $origFileName $newFileName

sed -i "s/$oldVideo/$newVideo/g" "$newFileName"