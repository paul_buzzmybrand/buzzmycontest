#!/bin/bash

file=$1
basepath="$(dirname "$(pwd)")"
basepath+=/public

name=${file%%.*}

oldVideo="VideoFileName"
newVideo=$name
oldImage="ImageFileName"
newImage=$name-facebook
origFileName="$basepath/FacebookShare/FacebookSharing.html"
newFileName="$basepath/FacebookShare/$name.html"

cp -f $origFileName $newFileName

sed -i "s/$oldVideo/$newVideo/g" "$newFileName"

sed -i "s/$oldImage/$newImage/g" "$newFileName"
