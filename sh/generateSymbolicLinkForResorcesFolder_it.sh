#!/bin/bash

TandCTarget="/var/www/html/shared/public_it/T_and_C"
FacebookShareTarget="/var/www/html/shared/public_it/FacebookShare"
FacebookShareEssayTarget="/var/www/html/shared/public_it/FacebookShareEssay"
FacebookSharePhotoTarget="/var/www/html/shared/public_it/FacebookSharePhoto"
essaysTarget="/var/www/html/shared/public_it/essays"
photosTarget="/var/www/html/shared/public_it/photos"
templatesTarget="/var/www/html/shared/public_it/templates"
bower_componentsTarget="../dashboard/bower_components"
videosTarget="/var/www/html/shared/public_it/videos"
website_publicTarget="/var/www/html/shared/public_it/website_public"
minisiteTarget="../Minisite/dist/build/web/"

TandC="T_and_C"
FacebookShare="FacebookShare"
FacebookShareEssay="FacebookShareEssay"
FacebookSharePhoto="FacebookSharePhoto"
essays="essays"
photos="photos"
templates="templates"
bower_components="bower_components"
videos="videos"
website_public="website_public"
minisite="minisite"


###################################
#############T & C#################
###################################

if [ -d $TandCTarget ]; then

  if [ ! -L $TandC ]; then

    ln -s $TandCTarget $TandC

  else

    echo $TandC "is already exist!"

  fi

else

  echo $TandCTarget "doesn't exists!"

fi


###################################
########FacebookShareTarget########
###################################

if [ -d $FacebookShareTarget ]; then

  if [ ! -L $FacebookShare ]; then

    ln -s $FacebookShareTarget $FacebookShare

  else

    echo $FacebookShare "is already exist!"

  fi

else

  echo $FacebookShareTarget "doesn't exists!"

fi



###################################
####FacebookShareEssayTarget#######
###################################

if [ -d $FacebookShareEssayTarget ]; then

  if [ ! -L $FacebookShareEssay ]; then

    ln -s $FacebookShareEssayTarget $FacebookShareEssay

  else

    echo $FacebookShareEssay "is already exist!"

  fi

else

  echo $FacebookShareEssayTarget "doesn't exists!"

fi



###################################
####FacebookSharePhotoTarget#######
###################################

if [ -d $FacebookSharePhotoTarget ]; then

  if [ ! -L $FacebookSharePhoto ]; then

    ln -s $FacebookSharePhotoTarget $FacebookSharePhoto

  else

    echo $FacebookSharePhoto "is already exist!"

  fi

else

  echo $FacebookSharePhotoTarget "doesn't exists!"

fi



###################################
####essaysTarget###################
###################################

if [ -d $essaysTarget ]; then

  if [ ! -L $essays ]; then

    ln -s $essaysTarget $essays

  else

    echo $essays "is already exist!"

  fi

else

  echo $essaysTarget "doesn't exists!"

fi



###################################
####photosTarget###################
###################################

if [ -d $photosTarget ]; then

  if [ ! -L $photos ]; then

    ln -s $photosTarget $photos

  else

    echo $photos "is already exist!"

  fi

else

  echo $photosTarget "doesn't exists!"

fi



###################################
####templatesTarget################
###################################

if [ -d $templatesTarget ]; then

  if [ ! -L $templates ]; then

    ln -s $templatesTarget $templates

  else

    echo $templates "is already exist!"

  fi

else

  echo $templatesTarget "doesn't exists!"

fi



###################################
####bower_componentsTarget#########
###################################

if [ -d $bower_componentsTarget ]; then

  if [ ! -L $bower_components ]; then

    ln -s $bower_componentsTarget $bower_components

  else

    echo $bower_components "is already exist!"

  fi

else

  echo $bower_componentsTarget "doesn't exists!"

fi



###################################
####videosTarget##################
###################################

if [ -d $videosTarget ]; then

  if [ ! -L $videos ]; then

    ln -s $videosTarget $videos

  else

    echo $videos "is already exist!"

  fi

else

  echo $videosTarget "doesn't exists!"

fi



###################################
####website_public#################
###################################

if [ -d $website_publicTarget ]; then

  if [ ! -L $website_public ]; then

    ln -s $website_publicTarget $website_public

  else

    echo $website_public "is already exist!"

  fi

else

  echo $website_publicTarget "doesn't exists!"

fi



###################################
####minisiteTarget#################
###################################

if [ -d $minisiteTarget ]; then

  if [ ! -L $minisite ]; then

    ln -s $minisiteTarget $minisite

  else

    echo $minisite "is already exist!"

  fi

else

  echo $minisiteTarget "doesn't exists!"

fi
