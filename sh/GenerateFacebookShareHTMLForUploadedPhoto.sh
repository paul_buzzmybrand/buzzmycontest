#!/bin/bash

file=$1
basepath="$(dirname "$(pwd)")"
basepath+=/public

name=${file%%.*}

oldVideo="PhotoFileName"
newVideo=$name
origFileName="$basepath/FacebookSharePhoto/FacebookSharing.html"
newFileName="$basepath/FacebookSharePhoto/$name.html"

cp -f $origFileName $newFileName

sed -i "s/$oldVideo/$newVideo/g" "$newFileName"