file=$1
basepath="$(dirname "$(pwd)")"
basepath+=/public
#for file in *.mov
#do
	name=${file%%.*}
	convert -size 175x175 $basepath/essays/$file -resize 175x175 $basepath/images/essays/$name.jpg &>/dev/null
	convert -define jpeg:size=200x200 $basepath/essays/$file -thumbnail 245x245^ -gravity center -extent 245x245 $basepath/images/essays/$name-sqthumbnail.jpg &>/dev/null