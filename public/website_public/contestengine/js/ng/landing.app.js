var landingApp = angular.module('landingApp', []);

landingApp.directive('pwCheck', [function () {
    return {
      require: 'ngModel',
      scope: {
        otherModelValue: "=pwCheck"
      },
      link: function (scope, elem, attrs, ngModel) {
        ngModel.$validators.compareTo = function(modelValue) {
          return modelValue == scope.otherModelValue;
        };

        scope.$watch("otherModelValue", function() {
          ngModel.$validate();
        });
      }
    }
  }]);

landingApp.directive('availableEmail', function($q, $http) {
  return {
    restrict: 'A',
    require: '?ngModel',
    link: function(scope, elm, attrs, ctrl) {
      if(!ctrl) return;

      ctrl.$asyncValidators.availableEmail = function(modelValue, viewValue) {
        // Skip if empty
        if(ctrl.$isEmpty(modelValue))
          return $q.when();

        var feedbackEl = angular.element('#email-feedback');

        // Set a loading class
        feedbackEl.addClass('loading');
        feedbackEl.removeClass('valid');
        feedbackEl.removeClass('invalid');

        var def = $q.defer();

        $http.post('/admin/check-av-email', {'email': modelValue})
        .success(function(data) {
          feedbackEl.removeClass('loading');
          if(data.result == "1") {
            def.resolve(true);
            feedbackEl.addClass('valid');
          } else {
            def.reject();
            feedbackEl.addClass('invalid');
          }
        })
        .catch(function() {
          def.reject();
          feedbackEl.removeClass('loading');
        });

        return def.promise;
      };
    }
  };
})

landingApp.controller('SignInController', ['$scope', '$http', function($scope, $http) {
  $scope.passwordRecoveryBlock = false;
  $('#login-dropdown a').click(function(e) {
    $(this).parent().toggleClass('open');
  });

  $scope.signIn = function(data) {
    data['ajax'] = 1;
    angular.element('#btn-login').attr('disabled', true);
    $http.post('/company/getSignin', data)
    .success(function(data) {
      if(data.success == 1) {
        $('body').css('background-color','white');
        $('html').css('opacity', 0);
        window.location = '/dashboard/index.html';
        return;
      }

      $scope.error = data.message;
      angular.element('#btn-login').attr('disabled', false);
    })
    .catch(function(data) {
      console.log(data);
      angular.element('#btn-login').attr('disabled', false);
      $scope.error = data.error.message;
    });
  }

  $scope.showPasswordRecoveryBlock = function() {
    $scope.passwordRecoveryBlock = true;
  }

  $scope.hidePasswordRecoveryBlock = function() {
    $scope.passwordRecoveryBlock = false;
  }

  $scope.sendPasswordRecoveryEmail = function() {
    angular.element('#password-recovery-link').attr('disabled', true);
    $http.post('/admin/resend-password', {
      'email': $scope.login.username,
      'language': window.location.href.indexOf('.com') > -1 ? 'en' : 'it'
    })
    .success(function(data) {
      angular.element('#password-recovery-link').attr('disabled', false);
      if(data.result == 1) {
        $scope.recoveryEmailSent = true;
        $scope.passwordRecoveryBlock = false;
      } else {
        $scope.error = data.message;
      }
    })
    .catch(function(data) {
      $scope.error = data.error.message;
    });
  }
}]);

landingApp.controller('SignupController', function($scope, $location) {
  $scope.errors = {};
  $scope.defaults = {
    language: window.location.href.indexOf('.com') > -1 ? 'en' : 'it',
    newsletter: true,
    terms: true
  }

  $scope.user = angular.copy($scope.defaults);

  $scope.reset = function() {
    $scope.user = angular.copy($scope.defaults);
    $scope.signupForm.$setPristine();
    $scope.exception = null;
    $scope.$apply();
  }

  $scope.close = function(timeout) {
    timeout = timeout === undefined ? 3000 : timeout * 1000;
    setTimeout(function() {
      $('#sign-up').slideUp();
    }, timeout);
  }

  $scope.submit = function(data) {
        
    var target = '/admin/new-company'
    console.log(target);
    var payload = angular.copy(data);
    console.log(payload);
    $.post(target, payload)
    .success(function(response) {
      if(typeof(response) == 'string')
        response = JSON.parse(response);
      if(response.result == 1) {
        $scope.success = true;
        $('body').css('background-color','white');
        $('html').css('opacity', 0);
          setTimeout(function() {
            window.location = '/dashboard/index.html#launch/instant';
          }, 500);
      } else {
        $scope.success = false;
          $scope.exception = response.message;
          $scope.$apply();
      }
    })
    .fail(function(response) {
      $scope.success = false;
      $scope.exception = 'Server error';
      $scope.$apply();
      $scope.close();
    });
  }


});

landingApp.controller('ContactController', function($scope) {
	$scope.success = null;
	$scope.exception = null;
	$scope.defaults = {newsletter: true};
	$scope.contact = angular.copy($scope.defaults);

	$scope.reset = function() {
		$scope.contact = angular.copy($scope.defaults);
        $scope.contactForm.$setPristine();
        $scope.exception = null;
        $scope.$apply();
	}

	$scope.close = function(timeout) {
		timeout = timeout === undefined ? 3000 : timeout * 1000;
		setTimeout(function() {
			$('#get-in-touch').slideUp();
		}, timeout);
	}

	$scope.submit = function(data) {
        
		var target = '/get-in-touch'
		//var target = $('#form-info').attr('action');
		var payload = angular.copy(data);
		payload.newsletter = data.newsletter ? 1 : 0;
        $.post(target, payload)
        .success(function(response) {
        	if(typeof(response) == 'string')
        		response = JSON.parse(response);
        	if(response.result == 1) {
	        	$scope.success = true;
	        	$scope.reset();
            	$scope.close();
	        } else {
	        	$scope.success = false;
	            $scope.exception = response.exception;
	            $scope.$apply();
	        }
        })
        .fail(function(response) {
        	$scope.success = false;
        	$scope.exception = 'Server error';
        	$scope.$apply();
        	$scope.close();
        });
	}

});

landingApp.controller('NewsletterController', function($scope) {
	$scope.success = null;
	$scope.exception = null;

	$scope.reset = function() {
		$scope.newsletter = {};
        $scope.newsletterForm.$setPristine();
        $scope.exception = null;
        $scope.$apply();
	}

	$scope.submit = function(data) {
        
		var target = $('#form-newsletter').attr('action');
		data.newsletter = 1;
        $.post(target, data)
        .success(function(response) {
        	if(typeof(response) == 'string')
        		response = JSON.parse(response);
        	if(response.result == "1") {
	        	$scope.success = true;
	        	$scope.$apply();
	        	$scope.reset();
	        } else {
	        	$scope.success = false;
	            $scope.exception = response.exception;
	            $scope.$apply();
	        }
        })
        .fail(function(response) {
        	$scope.success = false;
        	$scope.exception = 'Server error';
        	$scope.$apply();
        });
	}
});