<?php

namespace ApiMinisite\Controllers;

use Hash;
use Config;
use Validator;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;

class AuthController extends \Controller {

    /**
     * Generate JSON Web Token.
     */

    protected function createToken($id)
    {
        $payload = [
            'sub' => $id,
            'iat' => time(),
            'exp' => time() + ( 4 * 60 * 60) //14gg
        ];
        return JWT::encode($payload, Config::get('constants.token_secret'));
    }

    private function getId($id, $ison) {
      if(!is_numeric($id))
      {
        return $this->getContestId($id, $ison);
      }
      return $id;
    }

    private function getContestId($name, $ison)
    {
    	$id = 0;
    	$keycache= "contest-name--" . $name;
    	if (((boolean)Config::get('constants.ENABLE_CACHE'))){
    		if (Cache::has($keycache)){
    			\Log::debug("GET key in cache: ".print_r($keycache,true)." valore ".print_r(Cache::get($keycache),true));
    			return (integer) Cache::get($keycache);
    		}
    	}

      $contest = \Contest::where('contest_route', $name)->first();

    	/*if (!$ison){
    		$contest = \Contest::where('contest_route', $name)->first();
    	}else{
    		$contest = \Contest::where('contest_route', $name)->where('status',2)->first();
    	}*/

    	if ($contest != null){
    		 $id = $contest->id;
    		 if (((boolean)Config::get('constants.ENABLE_CACHE'))){
    		 		Cache::put($keycache, $id, 30);
    		 		\Log::debug("ADD key in cache: ".print_r(array($keycache,$id),true));
    			}
    	}
    	return $id;
    }

    private function getCompanyId($name)
  	{
  		$id = 0;
  		$keycache= "contest-comapny--" . $name;
  		if (((boolean)Config::get('constants.ENABLE_CACHE'))){
  			if (Cache::has($keycache)){
  				\Log::debug("GET key in cache: ".print_r($keycache,true)." valore ".print_r(Cache::get($keycache),true));
  				return (integer) Cache::get($keycache);
  			}
  		}

  		//$contest = \Contest::where('contest_route', $name)->where('status',2)->first();
      $contest = \Contest::where('contest_route', $name)->first();

  		if ($contest != null){
  			 $id = $contest->company_id;
  			 if (((boolean)Config::get('constants.ENABLE_CACHE'))){
  			 		Cache::put($keycache, $id, 30);
  			 		\Log::debug("ADD key in cache: ".print_r(array($keycache,$id),true));
  				}
  		}
  		return $id;
  	}

    private function joinContestFromWebsite($ownerId, $contestId) {

  		\Log::debug("joinContestFromWebsite()");

  		// ATTACH IF ROW DOESN' T EXIST
  		$contest_2 = \Contest::with(
  						array(
  							'users' => function($query) use ($ownerId) {
  											$query->where('user_id', '=', $ownerId);
  										}
  							)
  					)->where('id', $contestId)->first();

  		if ( $contest_2->users->count() == 0 )
  		{
  			$contest_2->users()->attach($ownerId, array('login_website' => new \DateTime('now')));
  			\Log::debug("joinContestFromWebsite() done");
  			return false;
  		}
  		else
  		{
  			$contest_2->users()->updateExistingPivot($ownerId, array('login_website' => new \DateTime('now')), false);
  			\Log::debug("joinContestFromWebsite() updated");

  			if ($contest_2->type_id == "2")
  			{

  				$userPhotosWithContests=\Photo::select("photos.*")
  					->where("photos.user_id","=", $ownerId)
  					->where("photos.contest_id", "=", $contestId)
  					->get();

  				\Log::debug("Number of photos returned per user: ".count($userPhotosWithContests));

  				\Log::debug("joinContestFromWebsite() counting photos array");

  				//$queries = DB::getQueryLog();
  				//Log::debug("Queries: ".print_r($queries,true));



  				if (count($userPhotosWithContests) > 0)
  				{
  					if ($contest_2->max_entries)
  					{
  						$entries = $contest_2->max_entries;

  						if (count($userPhotosWithContests) > $entries - 1)
  						{
  							\Log::debug("joinContestFromWebsite() yes photos with user_id ".$ownerId." and max_entries ".$entries);
  							return true;
  						}

  					}

  					\Log::debug("joinContestFromWebsite() yes photos with user_id ".$ownerId);
  					return true;
  				}
  				else
  				{
  					\Log::debug("joinContestFromWebsite() no photos with user_id ".$ownerId);
  					return false;
  				}

  			}
  			else if ($contest_2->type_id == "1")
  			{
  				$userVideosWithContests=\Video::select("videos.*")
  					->where("videos.user_id","=", $ownerId)
  					->where("videos.contest_id", "=", $contestId)
  					->get();

  				\Log::debug("Number of videos returned per user: ".count($userVideosWithContests));

  				\Log::debug("joinContestFromWebsite() counting videos array");

  				//$queries = DB::getQueryLog();
  				//Log::debug("Queries: ".print_r($queries,true));

  				if (count($userVideosWithContests) > 0)
  				{
  					\Log::debug("joinContestFromWebsite() yes videos with user_id ".$ownerId);
  					return true;
  				}
  				else
  				{
  					\Log::debug("joinContestFromWebsite() no videos with user_id ".$ownerId);
  					return false;
  				}
  			}
  			else if ($contest_2->type_id == "3")
  			{
  				$userEssaysWithContests=\Essay::select("essays.*")
  					->where("essays.user_id","=", $ownerId)
  					->where("essays.contest_id", "=", $contestId)
  					->get();

  				\Log::debug("Number of essays returned per user: ".count($userEssaysWithContests));

  				\Log::debug("joinContestFromWebsite() counting essays array");

  				//$queries = DB::getQueryLog();
  				//Log::debug("Queries: ".print_r($queries,true));

  				if (count($userEssaysWithContests) > 0)
  				{
  					\Log::debug("joinContestFromWebsite() yes essays with user_id ".$ownerId);
  					return true;
  				}
  				else
  				{
  					\Log::debug("joinContestFromWebsite() no essays with user_id ".$ownerId);
  					return false;
  				}
  			}




  		}
  		//
  		return false;
  	}

  	private function joinContestFromMobile($ownerId, $contestId) {

  		\Log::debug("joinContestFromMobile()");

  		// ATTACH IF ROW DOESN' T EXIST
  		$contest_2 = \Contest::with(
  						array(
  							'users' => function($query) use ($ownerId) {
  											$query->where('user_id', '=', $ownerId);
  										}
  							)
  					)->where('id', $contestId)->first();

  		if ( $contest_2->users->count() == 0 )
  		{
  			$contest_2->users()->attach($ownerId, array('login_mobile' => new \DateTime('now')));
  			\Log::debug("joinContestFromMobile() done");
  		}
  		else
  		{
  			$contest_2->users()->updateExistingPivot($ownerId, array('login_mobile' => new \DateTime('now')), false);
  			\Log::debug("joinContestFromMobile() updated");


  			if ($contest_2->type_id == "2")
  			{

  				$userPhotosWithContests=\Photo::select("photos.*")
  					->where("photos.user_id","=", $ownerId)
  					->where("photos.contest_id", "=", $contestId)
  					->get();

  				\Log::debug("Number of photos returned per user: ".count($userPhotosWithContests));

  				\Log::debug("joinContestFromMobile() counting photos array");

  				//$queries = DB::getQueryLog();
  				//Log::debug("Queries: ".print_r($queries,true));

  				if (count($userPhotosWithContests) > 0)
  				{
  					\Log::debug("joinContestFromMobile() yes photos with user_id ".$ownerId);
  					return true;
  				}
  				else
  				{
  					\Log::debug("joinContestFromMobile() no photos with user_id ".$ownerId);
  					return false;
  				}

  			}
  			else if ($contest_2->type_id == "1")
  			{
  				$userVideosWithContests=\Photo::select("videos.*")
  					->where("videos.user_id","=", $ownerId)
  					->where("videos.contest_id", "=", $contestId)
  					->get();

  				\Log::debug("Number of videos returned per user: ".count($userVideosWithContests));

  				\Log::debug("joinContestFromMobile() counting videos array");

  				//$queries = DB::getQueryLog();
  				//Log::debug("Queries: ".print_r($queries,true));

  				if (count($userVideosWithContests) > 0)
  				{
  					\Log::debug("joinContestFromMobile() yes videos with user_id ".$ownerId);
  					return true;
  				}
  				else
  				{
  					\Log::debug("joinContestFromMobile() no videos with user_id ".$ownerId);
  					return false;
  				}
  			}
  			else if ($contest_2->type_id == "3")
  			{
  				$userEssaysWithContests=\Photo::select("essays.*")
  					->where("essays.user_id","=", $ownerId)
  					->where("essays.contest_id", "=", $contestId)
  					->get();

  				\Log::debug("Number of essays returned per user: ".count($userEssaysWithContests));

  				\Log::debug("joinContestFromMobile() counting essays array");

  				//$queries = DB::getQueryLog();
  				//Log::debug("Queries: ".print_r($queries,true));

  				if (count($userEssaysWithContests) > 0)
  				{
  					\Log::debug("joinContestFromMobile() yes essays with user_id ".$ownerId);
  					return true;
  				}
  				else
  				{
  					\Log::debug("joinContestFromMobile() no essays with user_id ".$ownerId);
  					return false;
  				}
  			}
  		}
  		//
  		return false;
  	}

    private function updateNumberOfFriends(&$facebookProfile, $accessToken) {
      $client = new GuzzleHttp\Client();
      $jsonfriends = $client->request('GET', 'https://graph.facebook.com/v2.1/me/friends?offset=5000&limit=5000', [
          'query' => [
              'access_token' => $accessToken,
              'fields' => ''
          ]
      ]);

      if($jsonfriends->getStatusCode()== "200")
      {
        \Log::debug("Facebook friends JSON: ".$jsonfriends->getBody());
        $decoded_jsonfriends = json_decode($jsonfriends->getBody());
    		if (isset($decoded_jsonfriends->summary))
    		{
          \Log::debug("Facebook friends JSON summary finded ");
    			$facebookProfile->num_friends = $decoded_jsonfriends->summary->total_count;
    		}
      }else{
        \Log::debug("Facebook friends JSON not good: ".$jsonfriends->getStatusCode());
      }

  	}

    public static function downloadAndSaveFacebookProfileImage(&$facebookProfile, $accessToken) {

          $response = 'default.png';
          // Noonic debug
          if (true) {
      		\Log::debug("Obtaining facebook profile image");
      		$fp = tmpfile();
      		$tmpFilename = stream_get_meta_data($fp)['uri'];
      		\Log::debug("Temporary filename: ".$tmpFilename);
          $client = new GuzzleHttp\Client();
          $response = $client->request('GET', 'https://graph.facebook.com/me/picture', [
              'query' => [
                  'access_token' => $accessToken,
                  'fields' => ''
              ]
          ]);

          if($response->getStatusCode()!= "200")
      		{
      			\Log::warning("Error downloading profile picture. Setting the default one");
      			$facebookProfile->image = 'default.png';
      		} else {
            $jsonimage = json_decode($response->getBody(), true);
      			\Log::debug("Picture downloaded");
      			\Log::debug("Facebook Image JSON: ".$jsonimage);

      			$extension = "png";
      			$facebookProfile->image = md5_file($tmpFilename).".".$extension;
      			$destinationFileName = 'images/users/'.$facebookProfile->image;
      			\Log::debug("Destination file name: ".$destinationFileName);
            // TODO: da rivedere
            return $tmpFilename;
      			$uploadSuccess = rename($tmpFilename, $destinationFileName);
      			\Log::debug("rename() success: ".$uploadSuccess);
      		}
              $response = $facebookProfile->image;
          }
          return $response;

  	}



    private function updateFacebookProfile($facebook_account, $accesToken, $company_id, $user_id) {

  		\Log::debug("Update Facebook profile start");

  		$facebook_profile = \FacebookProfile::where("company_id", $company_id)->where("user_id", $user_id)->first();
  		if ( $facebook_profile ) {

  			\Log::debug("Facebook profile founded: " . $facebook_profile->id_fb);
  		} else {

  			$facebook_profile = new \FacebookProfile();
  			\Log::debug("New Facebook profile: " . $facebook_account['id']);

  		}


  		$facebook_profile->user_id = $user_id;
  		if (isset($facebook_account['username']))
  		{
  			$facebook_profile->username = $facebook_account['username'];
  		}
  		$facebook_profile->company_id = $company_id;

  		$facebook_profile->user_fb_token = $accesToken;
  		$facebook_profile->id_fb = $facebook_account['id'];

  		if (isset($facebook_account['name']))
  		{
  			$facebook_profile->name = $facebook_account['name'];
  		}

  		if (isset($facebook_account['first_name']))
  		{
  			$facebook_profile->first_name = $facebook_account['first_name'];
  			//$user->name = $facebook_account['first_name'];
  		}

  		if (isset($facebook_account['middle_name']))
  		{
  			$facebook_profile->middle_name = $facebook_account['middle_name'];
  		}

  		if (isset($facebook_account['last_name']))
  		{
  			$facebook_profile->last_name = $facebook_account['last_name'];
  			//$user->surname = $facebook_account['last_name'];
  		}

  		if (isset($facebook_account['gender']))
  		{
  			$facebook_profile->gender = $facebook_account['gender'];
  		}

  		if (isset($facebook_account['email']))
  		{
  			$facebook_profile->email = $facebook_account['email'];
  		}
  		else
  		{
  			$facebook_profile->email = $facebook_account['id']."@facebook.com";
  		}

  		if (isset($facebook_account['birthday']))
  		{
  			$facebook_profile->birthdate = $facebook_account['birthday'];
  		}

  		if (isset($facebook_account['verified']))
  		{
  			$facebook_profile->verified = $facebook_account['verified'];
  		}

  		if (isset($facebook_account['timezone']))
  		{
  			$facebook_profile->timezone = $facebook_account['timezone'];
  		}

  		if (isset($facebook_account['hometown']))
  		{
  			$facebook_profile->hometown = $facebook_account['hometown']['name'];
  		}

  		if (isset($facebook_account['locale']))
  		{
  			$facebook_profile->locale = $facebook_account['locale'];
  		}

  		$facebook_profile->password = \Hash::make(\Config::get("facebook")["secret_password"]);

  		$this->updateNumberOfFriends($facebook_profile, $accesToken);

  		self::downloadAndSaveFacebookProfileImage($facebook_profile, $accesToken);

  		$facebook_profile->expired = 0;

  		$facebook_profile->save();
  		\Log::debug("Update Facebook profile end");

  	}


    public function test($name,  $comefrom = "is_desktop") {
      return \Request::json()->all();
      /*$data = (object) \Request::json()->all();
      return $data->code*/
    }



    /**
     * Create Email and Password Account.
     */

    /**
     * Login with Facebook.
     */
    public function facebook($name)
    {
        $params = explode("&", $name);
        $name = $params[0];
        $comefrom = $params[1];

        $client = new GuzzleHttp\Client();
        $data = (object) \Request::json()->all();

        //CONTEST ID
        $companyId = $this->getCompanyId($name);
        $id = $this->getId($name,false);
        $contest = \Contest::find($id);

        //FACEBOOK PERMISSIONS
        if ($contest->score_from_userwall)
        {
        	$permissions = 'public_profile,user_friends,email,publish_actions';
        }
        else
        {
        	$permissions = 'public_profile,user_friends,email';
        }

        //FACEBOOK AUTH
        $params = [
            'code' => $data->code,
            'client_id' => $data->clientId,
            'redirect_uri' => $data->redirectUri,
            'client_secret' => Config::get('constants.facebook_secret'),
            'scope' => $permissions
        ];

        // Step 1. Exchange authorization code for access token.
        $accessTokenResponse = $client->request('GET', 'https://graph.facebook.com/v2.5/oauth/access_token', [
            'query' => $params
        ]);

        $accessToken = json_decode($accessTokenResponse->getBody(), true);

        // Step 2. Retrieve profile information about the current user.
        $fields = 'id,email,first_name,last_name,link,name';
        $profileResponse = $client->request('GET', 'https://graph.facebook.com/v2.5/me', [
            'query' => [
                'access_token' => $accessToken['access_token'],
                'fields' => $fields
            ]
        ]);
        //$profile = json_decode($profileResponse->getBody(), true);
        $facebook_account = json_decode($profileResponse->getBody(), true);



      //USERS

			$idFromCookie = Cookie::get('user_id');

			if ( $idFromCookie ) {

				$user = \User::where('id', $idFromCookie)->first();
				Log::debug("User retrieved from cookie: " . $user->email);

			} else {

				$user = \User::where('email', $facebook_account['id'] . '@f.com')->first();

				// There is a user
				if ($user) {

					$twitterProfileConnected = \TwitterProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
					if ( $twitterProfileConnected ) {
						$twitterProfileConnected->expired = 1;
						$twitterProfileConnected->save();
					}
					\Log::debug("User already present: " . $user->id);

				// Create new user
				} else {

					$user = new \User();
					\Log::debug("New user created: " . $user->id);
				}

				$user->email = $facebook_account['id'] . '@f.com';
				$user->password =  \Hash::make(\Config::get("facebook")["secret_password"]);

				if ( isset($facebook_account['first_name']) ){
					$user->name = $facebook_account['first_name'];
				}
				if ( isset($facebook_account['last_name']) ){
					$user->surname = $facebook_account['last_name'];
				}

				$user->save();

				Cookie::queue('user_id', $user->id);
				Log::debug("Created new Cookie for USER ID: " . $user->id);
			}


    		//devo cercare il facebook profile avente company_id del contest, fb_id del profilo loggante. Una volta trovato,
    		//devo verificare che lo/gli user_id associati non abbiano partecipato al contest con qualche submission.
    		$facebook_profiles = \FacebookProfile::where("company_id", $contest->company_id)->where("id_fb", $facebook_account['id'])->where("user_id", $user->id)->first();

        if ( $facebook_profiles ) {

          if (isset($facebook_account['name']) ){
    				$facebook_profiles->name = $facebook_account['name'];
    			}
          if (isset($facebook_account['email']) ){
    				$facebook_profiles->email = $facebook_account['email'];
    			}
          if (isset($facebook_account['id']) ){
    				$facebook_profiles->id_fb = $facebook_account['id'];
    			}

          $facebook_profiles->company_id = $companyId;
          $facebook_profiles->user_id = $user->id;
          $facebook_profiles->password = Hash::make(Config::get("facebook")["secret_password"]);

          \Log::debug("Facebook profile founded: " . $facebook_profiles->id_fb);

        } else {

          $facebook_profiles = new \FacebookProfile();

          if (isset($facebook_account['name']) ){
    				$facebook_profiles->name = $facebook_account['name'];
    			}
          if (isset($facebook_account['email']) ){
    				$facebook_profiles->email = $facebook_account['email'];
    			}
          if (isset($facebook_account['id']) ){
    				$facebook_profiles->id_fb = $facebook_account['id'];
    			}

          $facebook_profiles->company_id = $companyId;
          $facebook_profiles->user_id = $user->id;
          $facebook_profiles->password = Hash::make(Config::get("facebook")["secret_password"]);
          $facebook_profiles->save();

          \Log::debug("New Facebook profile: " . $facebook_account['id']);
        }

    		if ($contest->unique_participation)
    		{

    			foreach ($facebook_profiles as $fb_profile)
    			{
    				$user_id = $fb_profile['user_id'];
    				//devo verificare se questo user ha gi� partecipato al contest


    				if ($contest->type_id == "2")
    				{
    					$userPhotosWithContests=\Photo::select("photos.*")
    							->where("photos.user_id","=", $user_id)
    							->where("photos.contest_id", "=", $id)
    							->get();

    					\Log::debug("Number of photos returned per user: ".count($userPhotosWithContests));

    					//$queries = DB::getQueryLog();
    					//Log::debug("Queries: ".print_r($queries,true));

    					if (count($userPhotosWithContests) > 0)
    					{
    						if ($contest->max_entries)
    						{
    							$entries = $contest->max_entries;
    							if (count($userPhotosWithContests) > $entries - 1)
    							{
    								\Log::debug("Yes photos with user_id ".$user_id." and max_entries ".$entries);
    								$message = trans('messages.fbaccount_already_participate');
    								return \Response::json(['message' => $message], 401);
    							}
    						}
    						\Log::debug("Yes photos with user_id ".$user_id." and fb_id ".$fb_profile['id_fb']);
    						$message = trans('messages.fbaccount_already_participate');
    						return \Response::json(['message' => $message], 401);
    					}
    					else
    					{
    						\Log::debug("No photos with user_id ".$user_id." and fb_id ".$fb_profile['id_fb']);
    						//return true;
    					}


    				}
    				else if ($contest->type_id == "1")
    				{
    					$userVideosWithContests=\Photo::select("videos.*")
    							->where("videos.user_id","=", $user_id)
    							->where("videos.contest_id", "=", $id)
    							->get();

    					\Log::debug("Number of videos returned per user: ".count($userVideosWithContests));

    					//$queries = DB::getQueryLog();
    					//Log::debug("Queries: ".print_r($queries,true));

    					if (count($userVideosWithContests) > 0)
    					{

    						\Log::debug("Yes videos with user_id ".$user_id." and fb_id ".$fb_profile['id_fb']);

    						//$message = "Questo account ha gi� partecipato";
    						$message = trans('messages.fbaccount_already_participate');
                return \Response::json(['message' => $message], 401);
    					}
    					else
    					{
    						\Log::debug("No videos with user_id ".$user_id." and fb_id ".$fb_profile['id_fb']);
    						//return true;
    					}
    				}
    				else if ($contest->type_id == "3")
    				{
    					$userEssaysWithContests=\Essay::select("essays.*")
    							->where("essays.user_id","=", $user_id)
    							->where("essays.contest_id", "=", $id)
    							->get();

    					\Log::debug("Number of essays returned per user: ".count($userEssaysWithContests));
    					if (count($userEssaysWithContests) > 0)
    					{
    						\Log::debug("Yes essays with user_id ".$user_id." and fb_id ".$fb_profile['id_fb']);
    						//$message = "Questo account ha gi� partecipato";
    						$message = trans('messages.fbaccount_already_participate');
    						return \Response::json(['message' => $message], 401);
    					}
    					else
    					{
    						\Log::debug("No essays with user_id ".$user_id." and fb_id ".$fb_profile['id_fb']);
    						//return true;
    					}
    				}
    			}
    		}
        //
    		// ////////
    		$this->updateFacebookProfile($facebook_account, $accessToken['access_token'], $contest->company->id, $user->id);
    		/////////
        if ( $comefrom == 'is_desktop' ) {
          $this->joinContestFromWebsite($user->id, $contest->id);
        } else {
          $this->joinContestFromMobile($user->id, $contest->id);
        }

    		$cachedPassword = $user->password;
    		$user->password = \Hash::make(\Config::get("facebook")["secret_password"]);
    		$user->save();

    		return \Response::json(['token' => $this->createToken($user->id)]);



        // Step 3a. If user is already signed in then link accounts.
        // if (\Request::header('Authorization'))
        // {
        //     $user_check = DB::table('users')
        //       ->join('facebook_profiles', function($join) use($profile) {
        //         $join->on("users.id", "=", "facebook_profiles.user_id")
        //           ->where("facebook_profiles.id_fb", "=", $profile["id"]);
        //     });
        //
        //     if ($user_check->first())
        //     {
        //         return \Response::json(['message' => 'There is already a Facebook account that belongs to you'], 409);
        //     }
        //
        //     $token = explode(' ', \Request::header('Authorization'))[1];
        //     $payload = (array) JWT::decode($token, Config::get('constants.token_secret'), array('HS256'));
        //
        //     $user = \User::find($payload['sub']);
        //     $user->name = $user->name ?: $profile['name'];
        //     $user->email = $user->email ?:$profile['email'];
        //     $user->save();
        //
        //     $userId = $user->id;
        //
        //     $facebookProfile = new \FacebookProfile();
        //     $facebookProfile->name = $profile['name'];
        //     $facebookProfile->email = $profile['email'];
        //     $facebookProfile->company_id = $companyId;
        //     $facebookProfile->id_fb = $profile['id'];
        //     $facebookProfile->user_id = $userId;
        //     $facebookProfile->password = Hash::make(Config::get("facebook")["secret_password"]);
        //     $facebookProfile->save();
        //
        //     return \Response::json(['token' => $this->createToken($user->id)]);
        // }
        // // Step 3b. Create a new user account or return an existing one.
        // else
        // {
        //
        //     $user_check = DB::table('users')
        //       ->join('facebook_profiles', function($join) use($profile) {
        //         $join->on("users.id", "=", "facebook_profiles.user_id")
        //           ->where("facebook_profiles.id_fb", "=", $profile["id"]);
        //       });
        //
        //
        //     if ($user_check->first())
        //     {
        //       return \Response::json(['token' => $this->createToken($user_check->first()->user_id)]);
        //     }
        //
        //     $user = new \User;
        //     $user->name = $profile['name'];
        //     $user->email = $profile['email'];
        //     //$user->company_id = $companyId;
        //     $user->password = Hash::make(Config::get("facebook")["secret_password"]);
        //
        //     $user->save();
        //
        //     $userId = $user->id;
        //
        //     $facebookProfile = new \FacebookProfile();
        //     $facebookProfile->name = $profile['name'];
        //     $facebookProfile->email = $profile['email'];
        //     $facebookProfile->company_id = $companyId;
        //     $facebookProfile->id_fb = $profile['id'];
        //     $facebookProfile->user_id = $userId;
        //     $facebookProfile->password = Hash::make(Config::get("facebook")["secret_password"]);
        //     $facebookProfile->save();
        //
        //     return \Response::json(['token' => $this->createToken($user->id)]);
        //
        // }
    }


    /**
     * Login with Twitter.
     */
    public function twitter($name)
    {
      $params = explode("&", $name);
      $name = $params[0];
      $comefrom = $params[1];

      $stack = GuzzleHttp\HandlerStack::create();
      $data = (object) \Request::json()->all();

        // Part 1 of 2: Initial request from Satellizer.
        if (!property_exists ($data,'oauth_token') || !property_exists ($data,'oauth_verifier'))
        {
            $stack = GuzzleHttp\HandlerStack::create();

            $requestTokenOauth = new Oauth1([
              'consumer_key' => Config::get('constants.twitter_key'),
              'consumer_secret' => Config::get('constants.twitter_secret'),
              'callback' => $data->redirectUri,
              'token' => '',
              'token_secret' => ''
            ]);
            $stack->push($requestTokenOauth);

            $client = new GuzzleHttp\Client([
                'handler' => $stack
            ]);

            // Step 1. Obtain request token for the authorization popup.
            $requestTokenResponse = $client->request('POST', 'https://api.twitter.com/oauth/request_token', [
                'auth' => 'oauth'
            ]);

            $oauthToken = array();
            parse_str($requestTokenResponse->getBody(), $oauthToken);

            // Step 2. Send OAuth token back to open the authorization screen.
            return \Response::json($oauthToken);

        }
        // Part 2 of 2: Second request after Authorize app is clicked.
        else
        {

            $accessTokenOauth = new Oauth1([
                'consumer_key' => Config::get('constants.twitter_key'),
                'consumer_secret' => Config::get('constants.twitter_secret'),
                'token' => $data->oauth_token,
                'verifier' => $data->oauth_verifier,
                'token_secret' => ''
            ]);
            $stack->push($accessTokenOauth);

            $client = new GuzzleHttp\Client([
                'handler' => $stack
            ]);

            // Step 3. Exchange oauth token and oauth verifier for access token.
            $accessTokenResponse = $client->request('POST', 'https://api.twitter.com/oauth/access_token', [
                'auth' => 'oauth'
            ]);

            $accessToken = array();
            parse_str($accessTokenResponse->getBody(), $accessToken);

            $profileOauth = new Oauth1([
                'consumer_key' => Config::get('constants.twitter_key'),
                'consumer_secret' => Config::get('constants.twitter_secret'),
                'oauth_token' => $accessToken['oauth_token'],
                'token_secret' => ''
            ]);
            $stack->push($profileOauth);

            $client = new GuzzleHttp\Client([
                'handler' => $stack
            ]);

            // Step 4. Retrieve profile information about the current user.
            $profileResponse = $client->request('GET', 'https://api.twitter.com/1.1/users/show.json?screen_name=' . $accessToken['screen_name'], [
                'auth' => 'oauth'
            ]);

            $profile = json_decode($profileResponse->getBody(), true);
            $companyId = $this->getCompanyId($name);

            ///newcode here////////////////////////
            //////////////////////////////////////
            $id = $this->getId($name,true);
            $contest = \Contest::find($id);

			$idFromCookie = Cookie::get('user_id');

			if ( $idFromCookie ) {

				$user = \User::where('id', $idFromCookie)->first();
				Log::debug("User retrieved from cookie: " . $user->email);

			} else {


				$user = \User::where('email', $profile['id']. '@t.com')->first();
      			// There is a user
      			if ($user) {
      				$facebookProfileConnected = \FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
      				if ( $facebookProfileConnected ) {
      					$facebookProfileConnected->expired = 1;
      					$facebookProfileConnected->save();
      				}

      				\Log::debug("User already present: " . $user->id);
      			// Create new user
      			} else {
      				$user = new \User();
      				\Log::debug("New user created: " . $user->id);
      			}

      			$user->email = $profile['id'] . '@t.com';
      			$user->password = \Hash::make(\Config::get("facebook")["secret_password"]);
      			$user->name = $profile['name'];
      			$user->save();

				Cookie::queue('user_id', $user->id);
				Log::debug("Created new Cookie for USER ID: " . $user->id);
			}

			$twitterProfile = \TwitterProfile::where("company_id", $contest->company_id)->where("user_id", $user->id)->first();
			if ( $twitterProfile ) {
				\Log::debug("Twitter profile founded: " . $twitterProfile->tw_id);
			} else {
				$twitterProfile = new \TwitterProfile();
				\Log::debug("New Twitter profile: " . $profile['id']);
			}

			$twitterProfile->user_id = $user->id;
			$twitterProfile->tw_id = $profile['id'];
			$twitterProfile->name = $profile['name'];
			$twitterProfile->screen_name = $profile['screen_name'];
			$twitterProfile->oauth_token = $accessToken['oauth_token'];
			$twitterProfile->oauth_token_secret = $accessToken['oauth_token_secret'];
			$twitterProfile->company_id = $contest->company_id;
			$twitterProfile->expired = 0;

			$twitterProfile->save();

			\Log::debug("Update Twitter profile end");
			////////
			if ( $comefrom == 'is_desktop' ) {
				$this->joinContestFromWebsite($user->id, $contest->id);
			} else {
				$this->joinContestFromMobile($user->id, $contest->id);
			}
			////////


			$cachedPassword = $user->password;
			$user->password = \Hash::make(\Config::get("facebook")["secret_password"]);
			$user->save();

            return \Response::json(['token' => $this->createToken($user->id)]);

            // Step 5a. Link user accounts.
            // if (\Request::header('Authorization'))
            // {
            //     $user_check = DB::table('users')
            //       ->join('twitter_profiles', function($join) use($profile) {
            //         $join->on("users.id", "=", "twitter_profiles.user_id")
            //           ->where("twitter_profiles.tw_id", "=", $profile["id"]);
            //       });
            //
            //     if ($user_check->first())
            //     {
            //         return \Response::json(['message' => 'There is already a Twitter account that belongs to you'], 409);
            //     }
            //
            //     $token = explode(' ', \Request::header('Authorization'))[1];
            //     $payload = (array) JWT::decode($token, Config::get('constants.token_secret'), array('HS256'));
            //
            //     $user = \User::find($payload['sub']);
            //     $user->name = $user->name ?: $profile['name'];
            //     $user->save();
            //     $userId = $user->id;
            //
            //     $twitterProfile = new \TwitterProfile();
            //     $twitterProfile->user_id = $userId;
            // 		$twitterProfile->tw_id = $profile['id'];
            // 		$twitterProfile->name =  $profile['name'];
            // 		$twitterProfile->screen_name =  $profile['screen_name'];
            //     $twitterProfile->company_id = $companyId;
            // 		$twitterProfile->oauth_token = $access_token['oauth_token'];
            // 		$twitterProfile->oauth_token_secret = $access_token['oauth_token_secret'];
            // 		//$twitterProfile->company_id = $contest->company_id;
            // 		$twitterProfile->expired = 0;
            //
            //     $twitterProfile->save();
            //
            //     return \Response::json(['token' => $this->createToken($userId)]);
            // }
            // // Step 5b. Create a new user account or return an existing one.
            // else
            // {
            //     $user_check = DB::table('users')
            //       ->join('twitter_profiles', function($join) use($profile) {
            //         $join->on("users.id", "=", "twitter_profiles.user_id")
            //           ->where("twitter_profiles.tw_id", "=", $profile["id"]);
            //       });
            //
            //     if ($user_check->first())
            //     {
            //       return \Response::json(['token' => $this->createToken($user_check->first()->user_id)]);
            //     }
            //     $user = User::where('email', $profile['id'] . '@t.com')->first();
            //
            //     $user = new \User;
            //     $user->name = $profile['name'];
            //     $user->email = $profile['id'] . '@t.com';
            //     $user->password = Hash::make(Config::get("facebook")["secret_password"]);
            //
            //     $user->save();
            //
            //     $userId = $user->id;
            //     $twitterProfile = new \TwitterProfile();
            //     $twitterProfile->user_id = $userId;
            // 		$twitterProfile->tw_id =  $profile['id'];
            // 		$twitterProfile->name =  $profile['name'];
            //     $twitterProfile->company_id = $companyId;
            // 		$twitterProfile->screen_name =  $profile['screen_name'];
            // 		$twitterProfile->oauth_token = $accessToken['oauth_token'];
            // 		$twitterProfile->oauth_token_secret = $accessToken['oauth_token_secret'];
            // 		//$twitterProfile->company_id = $contest->company_id;
            // 		$twitterProfile->expired = 0;
            //
            //     $twitterProfile->save();
            //
            //     return \Response::json(['token' => $this->createToken($userId)]);
            // }
          }
        }

        //end class
    }
