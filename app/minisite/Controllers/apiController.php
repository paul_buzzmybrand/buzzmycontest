<?php

namespace ApiMinisite\Controllers;

  use Hash;
  use Config;
  use Validator;

  use Illuminate\Support\Facades\View;
  use Illuminate\Support\Facades\DB;
  use Illuminate\Support\Facades\Cache;

  class apiController extends \Controller {

  	private function getUser($id){
  		$user = DB::table('users')
  		->where('id', $id)
  		->where('email_confirmed',1)
  		->select('name', 'email')
  		->get();
  	if ($user != null) return $user[0];
  	return null;
  }

  private function getId($id) {
    if(!is_numeric($id))
    {
      return $this->getContestId($id);
    }
    return $id;
  }

  private function getContestId($name)
  {
  	$id = 0;
  	$keycache= "contest-name--" . $name;
  	if (((boolean)Config::get('constants.ENABLE_CACHE'))){
  		if (Cache::has($keycache)){
  			\Log::debug("GET key in cache: ".print_r($keycache,true)." valore ".print_r(Cache::get($keycache),true));
  			return (integer) Cache::get($keycache);
  		}
  	}

		$contest = \Contest::where('contest_route', $name)->first();

  	/*if (!$ison){
  		$contest = \Contest::where('contest_route', $name)->first();
  	}else{
  		$contest = \Contest::where('contest_route', $name)->where('status',2)->first();
  	}*/

  	if ($contest != null){
  		 $id = $contest->id;
  		 if (((boolean)Config::get('constants.ENABLE_CACHE'))){
  		 		Cache::put($keycache, $id, 30);
  		 		\Log::debug("ADD key in cache: ".print_r(array($keycache,$id),true));
  			}
  	}
  	return $id;
  }

  private function getContestType($id){
  	$information = null;
  	$type_information=0;

  	$keycache= "contest-type--" . $id;

  	if (((boolean)Config::get('constants.ENABLE_CACHE'))){
  		if (Cache::has($keycache) ){
  			\Log::debug("GET in cache: ".print_r($keycache,true)." valore ".print_r(Cache::get($keycache),true));
  			return (integer) Cache::get($keycache);
  		}
  	}

  	$information = DB::table('contests')
  			->where('id', $id)
  			//->where('status', 1)
  			->select('type_id')
  			->get();

  	if ($information != null) $type_information = $information[0]->type_id;;

  	if (((boolean)Config::get('constants.ENABLE_CACHE'))){
  		Cache::put($keycache, $type_information, 30);
  		\Log::debug("ADD key in cache: ".print_r(array($keycache,$type_information),true));
  	}

  	return (integer) $type_information;
  }


  private function joinContestFromWebsite($ownerId, $contestId) {

    \Log::debug("joinContestFromWebsite()");

    // ATTACH IF ROW DOESN' T EXIST
    $contest_2 = \Contest::with(
            array(
              'users' => function($query) use ($ownerId) {
                      $query->where('user_id', '=', $ownerId);
                    }
              )
          )->where('id', $contestId)->first();

    if ( $contest_2->users->count() == 0 )
    {
      $contest_2->users()->attach($ownerId, array('login_website' => new \DateTime('now')));
      \Log::debug("joinContestFromWebsite() done");
      return false;
    }
    else
    {
      $contest_2->users()->updateExistingPivot($ownerId, array('login_website' => new \DateTime('now')), false);
      \Log::debug("joinContestFromWebsite() updated");

      if ($contest_2->type_id == "2")
      {

        $userPhotosWithContests=\Photo::select("photos.*")
          ->where("photos.user_id","=", $ownerId)
          ->where("photos.contest_id", "=", $contestId)
          ->get();

        \Log::debug("Number of photos returned per user: ".count($userPhotosWithContests));

        \Log::debug("joinContestFromWebsite() counting photos array");

        //$queries = DB::getQueryLog();
        //Log::debug("Queries: ".print_r($queries,true));

        if (count($userPhotosWithContests) > 0)
        {
          if ($contest_2->max_entries)
          {
            $entries = $contest_2->max_entries;

            if (count($userPhotosWithContests) > $entries - 1)
            {
              \Log::debug("joinContestFromWebsite() yes photos with user_id ".$ownerId." and max_entries ".$entries);
              return true;
            }

          }

          \Log::debug("joinContestFromWebsite() yes photos with user_id ".$ownerId);
          return true;
        }
        else
        {
          \Log::debug("joinContestFromWebsite() no photos with user_id ".$ownerId);
          return false;
        }

      }
      else if ($contest_2->type_id == "1")
      {
        $userVideosWithContests=\Video::select("videos.*")
          ->where("videos.user_id","=", $ownerId)
          ->where("videos.contest_id", "=", $contestId)
          ->get();

        \Log::debug("Number of videos returned per user: ".count($userVideosWithContests));

        \Log::debug("joinContestFromWebsite() counting videos array");

        //$queries = DB::getQueryLog();
        //Log::debug("Queries: ".print_r($queries,true));

        if (count($userVideosWithContests) > 0)
        {
          \Log::debug("joinContestFromWebsite() yes videos with user_id ".$ownerId);
          return true;
        }
        else
        {
          \Log::debug("joinContestFromWebsite() no videos with user_id ".$ownerId);
          return false;
        }
      }
      else if ($contest_2->type_id == "3")
      {
        $userEssaysWithContests=\Essay::select("essays.*")
          ->where("essays.user_id","=", $ownerId)
          ->where("essays.contest_id", "=", $contestId)
          ->get();

        \Log::debug("Number of essays returned per user: ".count($userEssaysWithContests));

        \Log::debug("joinContestFromWebsite() counting essays array");

        //$queries = DB::getQueryLog();
        //Log::debug("Queries: ".print_r($queries,true));

        if (count($userEssaysWithContests) > 0)
        {
          \Log::debug("joinContestFromWebsite() yes essays with user_id ".$ownerId);
          return true;
        }
        else
        {
          \Log::debug("joinContestFromWebsite() no essays with user_id ".$ownerId);
          return false;
        }
      }




    }
    //
    return false;
  }

  private function joinContestFromMobile($ownerId, $contestId) {

    \Log::debug("joinContestFromMobile()");

    // ATTACH IF ROW DOESN' T EXIST
    $contest_2 = \Contest::with(
            array(
              'users' => function($query) use ($ownerId) {
                      $query->where('user_id', '=', $ownerId);
                    }
              )
          )->where('id', $contestId)->first();

    if ( $contest_2->users->count() == 0 )
    {
      $contest_2->users()->attach($ownerId, array('login_mobile' => new \DateTime('now')));
      \Log::debug("joinContestFromMobile() done");
    }
    else
    {
      $contest_2->users()->updateExistingPivot($ownerId, array('login_mobile' => new \DateTime('now')), false);
      \Log::debug("joinContestFromMobile() updated");


      if ($contest_2->type_id == "2")
      {

        $userPhotosWithContests=\Photo::select("photos.*")
          ->where("photos.user_id","=", $ownerId)
          ->where("photos.contest_id", "=", $contestId)
          ->get();

        \Log::debug("Number of photos returned per user: ".count($userPhotosWithContests));

        \Log::debug("joinContestFromMobile() counting photos array");

        //$queries = DB::getQueryLog();
        //Log::debug("Queries: ".print_r($queries,true));

        if (count($userPhotosWithContests) > 0)
        {
          \Log::debug("joinContestFromMobile() yes photos with user_id ".$ownerId);
          return true;
        }
        else
        {
          \Log::debug("joinContestFromMobile() no photos with user_id ".$ownerId);
          return false;
        }

      }
      else if ($contest_2->type_id == "1")
      {
        $userVideosWithContests=\Photo::select("videos.*")
          ->where("videos.user_id","=", $ownerId)
          ->where("videos.contest_id", "=", $contestId)
          ->get();

        \Log::debug("Number of videos returned per user: ".count($userVideosWithContests));

        \Log::debug("joinContestFromMobile() counting videos array");

        //$queries = DB::getQueryLog();
        //Log::debug("Queries: ".print_r($queries,true));

        if (count($userVideosWithContests) > 0)
        {
          \Log::debug("joinContestFromMobile() yes videos with user_id ".$ownerId);
          return true;
        }
        else
        {
          \Log::debug("joinContestFromMobile() no videos with user_id ".$ownerId);
          return false;
        }
      }
      else if ($contest_2->type_id == "3")
      {
        $userEssaysWithContests=Photo::select("essays.*")
          ->where("essays.user_id","=", $ownerId)
          ->where("essays.contest_id", "=", $contestId)
          ->get();

        \Log::debug("Number of essays returned per user: ".count($userEssaysWithContests));

        \Log::debug("joinContestFromMobile() counting essays array");

        //$queries = DB::getQueryLog();
        //Log::debug("Queries: ".print_r($queries,true));

        if (count($userEssaysWithContests) > 0)
        {
          \Log::debug("joinContestFromMobile() yes essays with user_id ".$ownerId);
          return true;
        }
        else
        {
          \Log::debug("joinContestFromMobile() no essays with user_id ".$ownerId);
          return false;
        }
      }


    }
    //
    return false;
  }



  public function getContest($name)
  {
    $id = $this->getId($name);
  	$contest = null;

  	if ($id != 0){
      $contest = \Contest::where('id', $id)->first();
      $fbPages = $contest->fbPages;
  	}

    $user = \User::where("company_id", $contest->company_id)
      ->where('deleted_at', null)
      ->first();


    if(isset($user->timezone_offset)) {

      $contestStartTime = \DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time);
      $StartTimeWithTimeZone = $contestStartTime->add(new \DateInterval('PT' . $user->timezone_offset . 'M'));
      $contest->start_time = $StartTimeWithTimeZone->format('Y-m-d H:i:s');

      $contestEndTime = \DateTime::createFromFormat('Y-m-d H:i:s', $contest->end_time);
      $EndTimeWithTimeZone = $contestEndTime->add(new \DateInterval('PT' . $user->timezone_offset . 'M'));
      $contest->end_time = $EndTimeWithTimeZone->format('Y-m-d H:i:s');
    }

		$is_facebook = 0;
		$is_twitter = 0;

		if (count($contest->fbPages) > 0)	$is_facebook = 1;
		if (count($contest->twPages) > 0) $is_twitter = 1;

    $socialLogin = array('isFacebookLogin' => $is_facebook, 'isTwitterLogin' => $is_twitter);

    return array_merge($contest->toArray(), $socialLogin);
  }


  public function objectives($name)
  {
    $id = $this->getId($name);

  	$contest = \Contest::find($id);

  	return $contest->objectives;
  }

  public function getSocial($name){

  	$object = new \StdClass;
  	$object->facebook = false;
  	$object->twitter = false;

  	$contest = null;
  	$id = $this->getContestId($name);

  	if ($id != 0){
  		$object->facebook = DB::table('fb_pages')->join('fbpage_contest', function ($join)use($id) {
             	$join->on('fb_pages.id', '=', 'fbpage_contest.fbpage_id')
                  ->where('fbpage_contest.contest_id', '=', $id);
          	})->count() > 0;

  		$object->twitter = DB::table('tw_pages')->join('twpage_contest', function ($join)use($id) {
              	$join->on('tw_pages.id', '=', 'twpage_contest.tw_page_id')
                   ->where('twpage_contest.contest_id', '=', $id);
          	})->count() >0;
  	}

  	return \Response::json(['social' => $object]);

  }

  public function getProfile($name){

  	$object = new \StdClass;
  	$user = null;

  	if (\Request::has('user')) $user=(integer) \Request::input('user');

  	if ($user != null){
  		$object = $this->getUser($user);
  	}

  	return \Response::json($object);

  }

  public function getRewards($name)
  {
    $id = $this->getId($name);
  	$users=[];
  	//
    if ($id != 0 ){
  	 	$users = DB::table('rewards')
  		 	->where('contest_id', $id)
  		 	->select('rank','title','description', 'thumbnail as image')
  			->orderBy('rank', 'asc')
  		 	->get();
  	 }
  	 return $users;
  }

  public function getEntries($name) {

  	$user = null;

  	if (\Request::has('user')) {
  		$user = \Request::input('user');
  	}

  	$table_name = array("videos","photos","essays");
  	$select_name = array("name","name","sentence as name");
  	$file_info = array("filename","filename","image as filename");

    $id = $this->getId($name);

  	$id_contest = 0;

  	if ($id != 0) {

  	$type_id = $this->getContestType($id);

  	if ($type_id > 0){

  		if ($user != null ){

  				$id_user = $user;

  				if ($id_user != 0)	{

  					return DB::table($table_name[$type_id-1])
  							->where('contest_id', $id)
  							->where('user_id', $id_user)
  							->where('approval_step_id', '3')
  							->orderBy('rank_position','asc')
  							->select($select_name[$type_id-1],'image',$file_info[$type_id-1],'score','rank_position')
  							->Paginate(20);
  					}
  			}
  			else{

  				return DB::table($table_name[$type_id-1])
  					->where('contest_id', $id)
  					->where('approval_step_id', '3')
  					->orderBy('rank_position','asc')
  					->select($select_name[$type_id-1],'image',$file_info[$type_id-1],'score','rank_position')
  					->Paginate(20);
  			}
  		}
  		return array();
  	}
  	return [];
  }


  public function saveEssay($name) {

  	\Log::debug("saveEssay(): input parameters: ".print_r(\Input::all(),true));
  	$sentence = \Request::input('sentence');
    //$hashtag = \Request::input('hashtag');

  	$user = null;
  	if (\Request::has('user')==false) return \Response::json(array("error" => "Error in Parameter"),500);
  	//find the right contest for approval of content
  	$ownerId = (integer) \Request::input('user');
    $contestId = $this->getId($name);

  	$contest = \Contest::find($contestId);

  	try {

  		$essay = new \Essay();

  		$tag = explode('/', \ContestHelper::createEssayPicture($sentence));

  		$essay->image = $tag[count($tag) - 1];
  		$essay->sentence = $sentence;
  		$essay->user_id = $ownerId;

  		$user = \User::find($ownerId);

  		$facebookProfile = \FacebookProfile::where('user_id', $ownerId)->where('company_id', $contest->company_id)->first();
  		if ( $facebookProfile && !$facebookProfile->expired ) {
  			$essay->user_connected_to_facebook = 1;
  		}

  		$twitterProfile = \TwitterProfile::where('user_id', $ownerId)->where('company_id', $contest->company_id)->first();
  		if ( $twitterProfile && !$twitterProfile->expired ) {
  			$essay->user_connected_to_twitter = 1;
  		}

  		$essay->approval_step_id = 1;

  		$contest = \Contest::find($contestId);
  		$essay->contest_id = $contest->id;
  		$essay->location_id = $contest->location_id;

  		$essay->save();
  		\Log::debug("Essay saved on the db");

  		$output = shell_exec("sh ".base_path()."/sh/GenerateEssayThumbnails.sh ". $tag[count($tag) - 1]);

  		$step = $contest->needsUserRegistration() ? $contest->makeStepClass(4) : $contest->makeStepClass(3);
  		\App::setLocale($contest->location->language);
  		return  \Response::json(array("message" => "OK"),200);

  	} catch(Exception $e) {
  		Log::error($e);
  		return \Response::json(array("error" => "Error function"),500);
  	}

  }


  public function savePhoto($name)
  {
  	//Inputs id_contest,title,image for photo widget
  	//Inputs photo, title, id_contest, input_type for import photo
  	$photoFileName = null;
  	$photoExtension = null;
  	$uploadPhoto_Success = null;
    $inputs = \Input::all();

  	$user = null;

  	if (\Request::has('user') == false) {
  		return \Response::json(array("error" => "Error in Parameter"),500);
  	}

  	$ownerId = (integer) \Request::input('user');
    $contestId = $this->getId($name);

  	try
  	{
  		$photoBase64String = $inputs['file'];

  		$data = base64_decode(substr($photoBase64String, strpos($photoBase64String, ",")+1));

  		$photoFileName = md5(time());
  		$photoExtension = 'jpg';

  		$path = base_path() . "/public/photos/" . $photoFileName . "." . $photoExtension;

  		file_put_contents($path, $data);

  		$uploadPhoto_Success = true;

  		\Log::debug("uploadPhoto_Success: ".$data);

  		if ($uploadPhoto_Success)
  		{
  			//caricare la foto nell'album della pagina Facebook di riferimento, con tanto di tag dell'utente che ha generato il file

  			$title = $inputs['title'];
        //$hashtag = $inputs['hashtag'];

  		//find the right contest for approval of content
  			$contest = \Contest::find($contestId);

  			$photo = new \Photo();
  			$photo->filename = "$photoFileName.$photoExtension";
  			$photo->name = $title;
  			$photo->user_id = $ownerId;
  			$photo->image = "$photoFileName.$photoExtension";
  			$user = \User::find($ownerId);

  			$facebookProfile = \FacebookProfile::where('user_id', $ownerId)
  				->where('company_id', $contest->company_id)
  				->first();

  			if ( $facebookProfile && !$facebookProfile->expired ) {
  				$photo->user_connected_to_facebook = 1;
  			}

  			$twitterProfile = \TwitterProfile::where('user_id', $ownerId)
  				->where('company_id', $contest->company_id)
  				->first();

  		if ( $twitterProfile && !$twitterProfile->expired ) {
  				$photo->user_connected_to_twitter = 1;
  			}

  			$photo->approval_step_id = 1;

  			$output = shell_exec("sh ".base_path()."/sh/GeneratePhotoThumbnails.sh $photoFileName.$photoExtension");

  			$photo->contest_id=$contest->id;
  			$photo->location_id=$contest->location_id;
  			$photo->save();
  			\Log::debug("Photo saved on the db");

  		}
  		else
  		{
  			return \Response::json(array("error" => "Error in File Upload"),500);
  		}

  	}
  	catch(\Exception $e)
  	{
  		\Log::error($e);
  		return \Response::json(array("error" => "Internal error occurred"),500);
  	}

  }



  public function saveVideo($name)
  {

    $inputs = \Input::all();

    \Log::debug("VideoController::store(): input parameters: ".print_r($inputs,true));

    $user = null;

  	if (\Request::has('user') == false) {
  		return \Response::json(array("error" => "Error in Parameter"),500);
  	}

  	$ownerId = (integer) \Request::input('user');

    $contestId = $this->getId($name);

    $title = utf8_encode($inputs['title']);

    if(!isset($inputs['videoName']))
    {
      \Log::debug("VideoController::store(): Required parameter video were not found, returning 400");
      $description='Required Paramater Video Not Found';
      return \Response::json(array("error" => $description),400);
    }

    try
    {

      $pathVideo = $inputs['video'];
      $videoName = $inputs['videoName'];

      //$video = base64_decode(substr($pathVideo, strpos($pathVideo, ",")+1));
      //$videoFileName = md5(time());

      $videoFileName = $videoName;
  		$videoExtension = 'mp4';

  		//$path = base_path() . "/public/videos/" . $videoFileName . "." . $videoExtension;

      $config = \Config::get('hdfvr');
      $error = \ContestHelper::convertFLV2MP4($videoName, $config['pathToVideoCommandLine']);

      $uploadVideo_Success = true;

      $video_attributes = \ContestHelper::get_video_attributes($videoName);

      \Log::debug("Video attributes: ".print_r($video_attributes,true));

      if ($uploadVideo_Success)
      {
        $contest = \Contest::where('id',$contestId)->first();

        \App::setLocale($contest->location->language);

        $ownerId = $ownerId;

        $video = new \Video();
        $video->filename = "$videoFileName.$videoExtension";
        $video->name = $title;
        $video->duration = $inputs['duration'];
        $video->image = "$videoFileName.$videoExtension";
        $video->user_id = $ownerId;

        $facebookProfile = \FacebookProfile::where('user_id', $ownerId)->where('company_id', $contest->company_id)->first();
        if ( $facebookProfile && !$facebookProfile->expired ) {
          $video->user_connected_to_facebook = 1;
        }

        $twitterProfile = \TwitterProfile::where('user_id', $ownerId)->where('company_id', $contest->company_id)->first();
        if ( $twitterProfile && !$twitterProfile->expired ) {
          $video->user_connected_to_twitter = 1;
        }

        ////////////////////////////////////
        $video->approval_step_id = 1;
        ////////////////////////////////////

        ////////////////////////////////////
        ////////////////////////////////////
        //exec("sh ./GenerateThumbnails.sh $videoFileName.$videoExtension", $output);
        ////////////////////////////////////
        ////////////////////////////////////

        $video->contest_id=$contest->id;
        $video->location_id=$contest->location_id;

        //create thumbnail
        $thumbnail = \ContestHelper::createThumbnailVideo(base_path().'/public/videos/'."$videoFileName.$videoExtension");
        $video->image = $thumbnail;

        $output = shell_exec("sh ".base_path()."/public/GenerateThumbnails.sh ".$video->filename);
        \Log::debug("Output GenerateThumbnails: ".$output);

        $step = $contest->needsUserRegistration() ? $contest->makeStepClass(4) : $contest->makeStepClass(3);

        $video->save();

        \Log::debug("Video saved on the db");

        return \Response::json(array("status" => "ok"),200);

      }
      else
      {
        throw new \Exception("Error during File Upload");
        return \Response::json(array("error" => "ko"),500);
      }

    }
    catch(\Exception $e)
    {
      \Log::error($e);
      return \Response::json(array("error" => $e), 500);
    }

  }



  public function signUp($name, $comefrom = "is_desktop") {

      $user = null;
      if (\Request::has('user') == false) {
        return \Response::json(array("error" => "Error in Parameter"),500);
      }

      $ownerId = \Request::input('user');

      $id = $this->getId($name);
      $contest = \Contest::find($id);

      $rules = array();

      if ( $contest->objectives()->first()->pivot->name ) array_push($rules, "'name'=>'required'");
      if ( $contest->objectives()->first()->pivot->surname )  array_push($rules, "'surname'=>'required'");
      if ( $contest->objectives()->first()->pivot->birthdate ) array_push($rules, "'birthdate' => 'required|date_format:d/m/Y'");
      if ( $contest->objectives()->first()->pivot->email ) array_push($rules, "'email'=>'required|email'");
      if ( $contest->objectives()->first()->pivot->phone ) array_push($rules, "'phone' => 'required|digits_between:1,45'");
      if ( $contest->objectives()->first()->pivot->city ) array_push($rules, "'city' => 'required'");
      if ( $contest->objectives()->first()->pivot->cap ) array_push($rules, "'cap'=>'required'");

      $validator = \Validator::make(\Input::all(), $rules);
      $inputs = \Input::all();
      if ($validator->fails()) return \Response::json(array("state"=>"ko" ,"error" => $validator),200);

      $user = \User::where('id', $ownerId)->first();

      if ( $user ) {
        //da capire cosa fare
        if ( $user->company_id != null) \Response::json(array("state"=>"ko","error" => array(\Lang::get("widget.signup_step.deny_access"))),200);
      } else {
        $user = new \User();
      }

      //$user->email = \Input::get('email');
      $user->password = \Hash::make(\Config::get("facebook")["secret_password"]);

      if (isset($inputs['name'])) $user->survey_name = \Input::get('name');
      if (isset($inputs['surname'])) $user->survey_surname = \Input::get('surname');
      if (isset($inputs['birthdate']))$user->survey_birthdate = \DateTime::createFromFormat('d/m/Y', \Input::get('birthdate'));
      if (isset($inputs['email'])) $user->survey_email = \Input::get('email');
      if (isset($inputs['phone'])) $user->survey_surname = \Input::get('phone');
      if (isset($inputs['city']))$user->survey_city = \Input::get('city');
      if (isset($inputs['cap']))$user->survey_cap = \Input::get('cap');

      try {

        $user->save();

      } catch (\Exception $ex){
        \Log::error("Error during login " . $ex);
        \Response::json(array("message"=>"Error registering user",),500);
      }

      if ($comefrom == 'is_mobile') {
        $already_participate = $this->joinContestFromMobile($user->id, $contest->id);
      }

      else {
        $already_participate = $this->joinContestFromWebsite($user->id, $contest->id);
      }

      if (($already_participate) && ($contest->unique_participation))
      {
        return \Response::json(array("state"=>"ko","error" => trans('messages.mail_already_participate')),200);
      }
      else //non ha mai partecipato facendo foto o non � un contest da unique participation
      {
        return \Response::json(array("state"=>"ok","error" => ""),200);
      }
  }



  public function inviteFriend($name)
	{
		//Inputs: name, addressee_name, recipient
		$inputs = \Request::all();

		\Log::debug("inviteMail::store(): input parameters: ".print_r($inputs,true));

		// input validator with its rules
		$validator = \Validator::make(
			array(
				'name' => $inputs['name'],
				'addressee_name' => $inputs['addressee_name'],
				'recipient' => $inputs['recipient']
			),
			array(
				'name' => 'required|min:3',
				'addressee_name' => 'required|min:3',
				'recipient' => 'required|email'
			)
		);

    $contest_id = $this->getId($inputs['contest_name']);

		$contest = \Contest::find($contest_id);

		if ( $contest->contest_route ) {
			$contestRoute = "http://bmb.buzzmybrand.com/" . $contest->contest_route;
		}

		$recipient = $inputs['recipient'];
		$sender_name = $inputs['name'];
		$addressee_name = $inputs['addressee_name'];
		$data = array("name"=>$sender_name,
					  "recipient"=>$recipient,
					  "contest_name" => $contest->name,
					  "name_friend"=> $addressee_name,
					  "contest_fb_link" => $contestRoute,
	    );

		if ($validator->fails())
		{

			\Log::debug("Validation fails!");

      return \Response::json(array("state"=>"ko","error" => 'Validation fails!'),200);
		}
		else
		{
			\Log::debug("Validation ok!");

			try {

				\App::setLocale($contest->location->language);

				\Mail::send('emails.test', $data, function($message) use ($data)
				{
				  $message->to($data["recipient"], $data["name"])
				  ->subject(\Lang::get('email.invite.subject'));
				});

			} catch (Exception $ex) {
				\Log::error('Problem sending email: '.$ex);
        return \Response::json(array("state"=>"ko","error" => 'Problem sending email: '.$ex),500);
			}

			return \Response::json(array("state"=>"ok","error" => ""),200);

		}

    return \Response::json(array("state"=>"ko","error" => "No validator!"),500);
	}

}
