<?php
namespace Minisite\Controllers;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class IndexController extends \Controller {

	public function getContest($name)
	{
		View::addNamespace('msite',  public_path() . '/minisite');

		if($name == "preview")
		{
			$template_id = $_GET['template_id'];
			if($template_id == 0) {
				return View::make('msite::preview')->with('template', "default");
			}
			else {
				$template = \Template::find($template_id);
				if ($template != null) $templatename = Str::lower($template->name);
				$pathtemplate = public_path() . '/minisite/assets/themes/' . $templatename;
				if ( !\File::isDirectory($pathtemplate)) 	$templatename="default";
				return View::make('msite::preview')->with('template', $templatename);
			}

		}

		if(is_numeric($name))
		{
			$contest = \Contest::where('id', $name)->first();
		}
		else {
			$contest = \Contest::where('contest_route', $name)->first();
		}

		$templatename = "default";
		$template = \Template::find($contest->template_id);

		if ($template != null) $templatename = Str::lower($template->name);

		$pathtemplate = public_path() . '/minisite/assets/themes/' . $templatename;

		if ( !\File::isDirectory($pathtemplate)) 	$templatename="default";

		\App::setLocale($contest->location->language);

		$hostname = $_SERVER['HTTP_HOST'] . '/';
		$contest->contest_route = $hostname . $contest->contest_route;

		//metatag description (og) - remove html tags
		$contest->description = strip_tags($contest->concept);
		$contest->name = strip_tags($contest->name);

		//if custom theme
		if(!isset($contest->template_id)) {
			$contest->image = $hostname . 'images/events/' . $contest->image_minisite_bg;
		}
		else {
			$contest->image = $hostname . "minisite/assets/themes/" . $templatename . "/images/bgCanvas.jpg";
		}

		if (View::exists('msite::iniziale')) {

			\Blade::setContentTags('[%', '%]');
			\Blade::setEscapedContentTags('[%%', '%%>]');
			return View::make('msite::iniziale')->with('template', $templatename)->with('contest', $contest);

		} else{
			return "view non trovata";
		}
	}


}
