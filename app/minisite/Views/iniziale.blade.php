<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10 " lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="it-IT" ng-app="bmbMinisite" ng-controller="AppCtrl" >
<!--<![endif]-->
  <head>
    <base href="/" />
    <meta name="author" content="GupDigital">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

    <link rel="apple-touch-icon" href="">
    <link rel="android-touch-icon" href="" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" type="image/ico" href="favicon.ico" />

    <title ng-bind="_state.data.pageTitle"></title>
    <!-- compiled CSS -->
      <link rel="stylesheet" type="text/css" href="[% URL::asset('minisite/bower_components/angular-loading-bar/build/loading-bar.min.css')%]" />
    <link rel="stylesheet" type="text/css" href="[% URL::asset('minisite/assets/bmbMinisite-1.0.0.css') %]" />
    <link rel="stylesheet" type="text/css" href="[% URL::asset('minisite/assets/themes/' . $template . '/theme.css') %]" />

    <!-- compiled JavaScript -->

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/jquery/dist/jquery.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular/angular.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-i18n/angular-locale_it-it.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/jquery.stellar/jquery.stellar.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/jquery-touchswipe/jquery.touchSwipe.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/jquery-is-on-screen/jquery.isonscreen.min.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-cache/dist/angular-cache.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-ui-map/ui-map.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-route/angular-route.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-slick/dist/slick.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-mass-autocomplete/massautocomplete.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-cookies/angular-cookies.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-socialshare/lib/angular-socialshare.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-resource/angular-resource.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-sanitize/angular-sanitize.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-scroll/angular-scroll.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-animate/angular-animate.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-translate/angular-translate.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-ui-router/release/angular-ui-router.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-loading-bar/build/loading-bar.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-local-storage/dist/angular-local-storage.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-bootstrap/ui-bootstrap.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-bootstrap/ui-bootstrap-tpls.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-toastr/dist/angular-toastr.tpls.min.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-touch/angular-touch.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angularjs-datepicker/src/js/angular-datepicker.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angucomplete-alt/angucomplete-alt.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/moment/moment.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-moment/angular-moment.min.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/moment/locale/it.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/humanize-duration/humanize-duration.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/modernizr/modernizr.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-timer/dist/angular-timer.min.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/slick-carousel/slick/slick.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/satellizer/satellizer.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/ng-file-upload-shim/ng-file-upload-shim.min.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/ng-file-upload-shim/ng-file-upload.min.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/ngInfiniteScroll/build/ng-infinite-scroll.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/ngCordova/dist/ng-cordova.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/ng-device-detector/ng-device-detector.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/valdr/valdr.min.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/valdr/valdr-message.min.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/underscore/underscore-min.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/deep_pick/index.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-underscore-module/angular-underscore-module.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/angular-modal-service/dst/angular-modal-service.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/ngmap/build/scripts/ng-map.min.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/OwlCarousel/owl-carousel/owl.carousel.min.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/bower_components/jquery.smartbanner/jquery.smartbanner.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/src/app/app.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/src/app/home/home.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/src/common/conf/apiUri.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/src/common/conf/conf.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/src/common/factories/signUp.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/src/common/factories/contest.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/src/common/factories/counter.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/src/common/factories/entries.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/src/common/factories/profile.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/src/common/factories/rewards.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/src/common/factories/utils.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/src/common/filters/filter.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/src/common/layout/layout.dir.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/templates-common.js')%]"></script>

    <script type="text/javascript" src="[% URL::asset('minisite/templates-app.js')%]"></script>


    <script type="text/javascript" src="[% URL::asset('minisite/assets/app.ui.js')%]"></script>


  </head>
  <body ng-class="{ 'noScroll' : _state.data.overlayIsOpen == true }">

    <div id="loading-bar" class="loader">
      <div class="loader__buzzLogo"></div>
    </div>

    <div class="fading-view" ui-view="main"></div>

    <ngckie></ngckie>
  </body>

</html>
