<!doctype html>
<html>
<head>
</head>
<body>

<h1>REGOLAMENTO CONCORSO A PREMI</h1>


<p>Buzzmybrand S.r.l.semplificata con  sede in Monopoli (BA), VIA Togliatti n. 58 PARTITA IVA n° 07711470729 promuove sulle pagine social network di {{$company_sponsoring}} (azienda sponsor) e sul suo sito web, il seguente concorso a premi tendente a fidelizzare e premiare la propria audience e al contempo a promuovere il marchio dell’azienda sponsor. </p>
<p><b>Ambito territoriale: </b></p> 
<p>Tutto il territorio nazionale e comunitario. </p>
<p><b>Periodo di svolgimento: </b></p> 
<p>Vedi minisito. </p>
<p><b>Destinatari del concorso: </b></p>
<p>Follower creativi dei profili social network Facebook, Instagram, Youtube e Twitter dell’azienda sponsor e del suo sito internet. </p>
<p><b>Meccanica del concorso: </b></p>
<p>In base al tema deciso dall’azienda sponsor inviare in base a quanto richiesto una immagine, un video o del testo secondo le seguenti modalità: </p>
<p>TRAMITE PROFILO FACEBOOK: accedere direttamente alla pagina registrarsi tramite la pagina Facebook ufficiale dell’azienda sponsor. </p> 
<p>TRAMITE PROFILO TWITTER: accedere al profilo Twitter dell’azienda sponsor. e registrarsi tramite log-in con account Twitter o Facebook</p>
<p>TRAMITE SITO INTERNET: accedere al sito internet http://app.buzzmybrand.it/(nome contest) e registrarsi tramite log-in con account Twitter o Facebook</p>
<p>INVIANDO EMAIL: inviare una email all’indirizzo dello sponsor allegando un’immagine, un testo od un video relativa al concorso fornendo e declinando interamente le proprie generalità ed allegando un documento identificativo in corso di validità</p>
<p>Le foto saranno pubblicate sulla pagina Facebook e sul profilo Twitter, Instagram o Youtube dell’azienda sponsor dopo aver verificato che siano effettivamente riferite al tema del concorso e che non siano offensivi alla morale pubblica. </p>
<p>Nel corso della durata del concorso l’utente che ha caricato il video, l’immagine o il testo potrà accumulare il maggior numero possibile di punti nelle seguenti modalità: 


<ul>
  <li> Ogni “mi piace” su Facebook vale 1 punto </li>
<li> Ogni “commento” su Facebook vale 2 punti 
</li>
  <li> Ogni “condivisione” su Facebook vale 2 punti 
</li>
  <li> Ogni “visualizzazione” su Facebook del post vale 0,2 punti
</li>
  <li> Ogni “favorite” su Twitter vale 1 punto
</li>
  <li> Ogni “retweet” su Twitter vale 2 punti
</li>
  <li> Ogni “mi piace” su Youtube vale 1 punto </li>
  <li> Ogni “visualizzazione” su Youtube vale 0,2 punti
</li>
</ul>
</p>
<p>Le azioni multiple per ciascuna categoria, effettuate da uno stesso utente, valgono come una sola. Per poter effettuare una o più tra le azioni indicate gli utenti dovranno registrarsi nelle modalità qui sopra descritte. L’utente che ha caricato l’immagine potrà, inoltre, effettuare le azioni indicate anche per il proprio video. I profili finti vengono rilevati automaticamente e squalificati. </p>
<p>All’atto della registrazione viene automaticamente concessa, da parte dell’utente, l’autorizzazione ad essere ricontattato all’indirizzo e-mail e/o al numero di cellulare indicato al fine di ottenerne i dati necessari per l’assegnazione del premio. La società promotrice si riserva il diritto di chiedere ai partecipanti e/o vincitori, in qualsiasi momento della manifestazione a premio, la prova dell’iscrizione a Facebook e/o a Twitter, Instagram, Youtube prima della data di inizio del concorso o di procedere autonomamente ad acquisire prova in tal senso. Qualora non sia fornita prova o la società promotrice non riesca ad acquisirla autonomamente il partecipante e/o vincitore verrà escluso</p>
<p>La partecipazione al concorso è assolutamente gratuita e non verrà applicata alcuna tariffa aggiuntiva oltre ai normali costi di connessione stabiliti dal proprio gestore. Il costo di connessione al sito varia in funzione del proprio provider. </p>
<p>I premi sono indicati sul minisito del concorso. </p>
<p>Nel caso in cui tra i primi 2 posti ci dovessero essere dei punteggi pari merito si procederà ad estrarre manualmente attraverso procedura di mescolamento il vincitore/ i vincitori. </p> 
<p>I vincitori saranno avvisati via e-mail, ai riferimenti rilasciati in fase di registrazione. Gli stessi, per convalidare la vincita, dovranno inviare all’indirizzo e-mail ufficiale dell’azienda sponsor entro 7 giorni dalla data della comunicazione di vincita, pena la nullità della stessa, la seguente documentazione: 
<ul>
  <li> copia del proprio Documento d’Identità; </li
<li> indicazione del proprio indirizzo e-mail; 
</li>
  <li> l’indirizzo completo; </li>
</ul>

</p>
<p>Nel caso in cui i documenti inviati non siano conformi a quanto previsto dal presente regolamento, con particolare riferimento alla rispondenza dei dati personali indicati nel documento d’identità con quelli rilasciati in fase di registrazione al concorso e corrispondenti al proprio profilo Facebook, Twitter, Instagram o Youtube la partecipazione sarà ritenuta in violazione delle condizioni previste dal presente Regolamento e, di conseguenza, la vincita non sarà convalidata. </p>
<p>Il vincitore riceverà, a seguito dalla ricezione della corretta documentazione di convalida, una e-mail a conferma vincita. Nel caso in cui il vincitore risultasse irreperibile o l’accettazione non sia valida per tempi e modi, il premio verrà assegnato alla prima riserva utile che dovrà convalidare a sua volta il premio con le modalità sopra indicate. </p>
<p><b>Linee guida di Facebook, Twitter, Instagram e Youtube: </b></p>
<p>Non si può partecipare con e-mail indirizzi postali e/o multiple, né con più account di Facebook, Twitter, Instagram o Youtube. Non si può utilizzare qualsiasi altro dispositivo o piattaforma per partecipare più di una (1) volta e/o votare. </p>
<p>Postare duplicati, o contenuti simili, partecipando su Twitter è una violazione delle regole di twitter (<a href="https://support.twitter.com/articles/18311-the-twitter-rules">Twitter Rules</a>) e comporta la squalifica. </p>
<p>Le partecipazioni che violano le linee guida di YouTube (<a href="http://www.youtube.com/t/community_guidelinesYouTube">Community Guidelines</a>) e i Termini d’utilizzo di YouTube (<a href="http://www.youtube.com/static?gl=US&template=terms">Terms of Service</a>) comportano la squalifica. </p> 

<p><b>Adempimenti e garanzie: </b></p>
<p>Buzzmybrand e l’azienda sponsor non si assumono alcuna responsabilità per problemi di accesso, impedimento, disfunzione o difficoltà riguardante la connessione che possa impedire ad un concorrente di partecipare alla modalità on line del concorso. </p> 
<p>Buzzmybrand e l’azienda sponsor non si assumono alcuna responsabilità nel caso in cui si verifichi una delle seguenti condizioni:<br> 
- La mailbox di un vincitore risulti piena; <br>
- L’e-mail indicata dal partecipante in fase di registrazione risulti inesistente, errata o incompleta; <br>
- Non vi sia risposta dall’host computer dopo l’invio dell’e-mail di notifica della vincita; <br> 
- La mailbox di un vincitore risulti disabilitata; <br> 
- L’e-mail indicata in fase di registrazione sia inserita in una blacklist. <br> 
</p>
<p><b>Pubblicità del concorso: </b></p> 
<p>La pubblicità del concorso sarà conforme al regolamento e verrà effettuata attraverso spazi promozionali sui social network maggiormente utilizzati nei giorni precedenti al concorso e nelle altre modalità ritenute necessarie o opportune dall’azienda sponsor. </p>
<p><b>Titolarità dei contenuti</b></p>
<p>BuzzMyBrand e l’azienda sponsor non rivendicano la proprietà sui contenuti inviati dagli utenti nel corso del contest. L’utente concede, invece, a BuzzMyBrand ed alla azienda sponsor una licenza globale, trasferibile, sub-licenziabile, gratuita, non esclusiva, sul contenuto inviato ad utilizzare lo stesso pubblicamente e con finalità commerciali o di lucro. </p>
<p><b>Privacy: </b></p>
<p> Il trattamento dei dati personali avverrà nel pieno rispetto del D.Lgs. n.196/03 e limitatamente ai fini connessi alla gestione del presente concorso. Con la partecipazione al presente concorso, si ritiene che l'ascoltatore/utente abbia preso visione del presente regolamento e abbia accettato ogni singola clausola. L’utilizzo degli indirizzi e-mail, dei numeri di telefono e di ogni altro dato fornito dall’ascoltatore/utente da parte di Buzzmybrand S.r.l. semplificata verrà fatto nel rispetto del Codice in materia di protezione dei dati personali, ai sensi del Decreto Legislativo 30-06-2003, n. 196 solo ai fini della comunicazione della vincita e di tutte le informazioni relative alla consegna del premio. Il conferimento è facoltativo; tuttavia, il mancato, errato ed inesatto conferimento dei dati necessari all’invio non consentirà la fruizione del premio. Il Responsabile del trattamento è Buzzmybrand S.r.l. semplificata con sede in Monopoli (BA), alla Via Togliatti n. 58. </p>
<p>I dati personali saranno portati a conoscenza dei dipendenti del Responsabile del trattamento i quali, operando rispettivamente sotto la diretta autorità del Titolare e del Responsabile, sono nominati incaricati del trattamento ai sensi degli artt. 29 e 30 del D.Lgs. 196/2003 e ricevono al riguardo adeguate istruzioni. In ogni momento si potrà ottenere la conferma dell’esistenza di dati e conoscerne contenuto, origine, verificarne l’esattezza o chiederne integrazione, aggiornamento, cancellazione, blocco se trattati in violazione di legge, opporsi al trattamento (art. 7 D.Lgs. 196/03) nonché conoscere l’elenco dei Responsabili scrivendo al Titolare o al Responsabile. </p>
<p><b>Server</b></p>
<p>Il server di raccolta e gestione di tutti i dati relativi allo svolgimento della manifestazione  a premio ha sede in Italia. </p>
<p><b>Foro competente</b></p> 
<p>Per ogni controversia sarà competente il Foro del consumatore. </p>


</body>
</html>