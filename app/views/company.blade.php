<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>JMTV Admin Panel</title>
        {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap-responsive.min.css') }}
        {{ HTML::style('lib/css/main.css')}}
        {{ HTML::style('lib/css/CustomScrollbar.css')}}
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" >
                        <div class="logo">
                            <img src="../lib/img/logo.png" alt=""/>
                        </div>
                        @if(Session::has('message1'))
                                <div class="alert alert-danger-alt alert-dismissable">
                                    <strong></strong> {{ Session::get('message1') }}
                                </div>
                             @endif
                        <div class="row-fluid" style="color:#fff !important;">
                             
                             
                            <div class="span3" >
                                <img src="../images/companys/{{ $companyWithContests['image'] }}" style="width:100%; height:100%" class="img-responsive">
                            </div>
                            <?php 
                                $companyid = Session::get('companyid');    
                                
                            ?>
                            
                            <div class="span6">
                                <h3 style="margin-top: 0px;">{{ $companyWithContests['name'] }}</h3>
                                <h6>{{ $companyWithContests['description'] }} </h6>
                                <h6>{{ $companyWithContests['address'] }}</h6>
                                <a class="btn" data-toggle="modal" href="#CFModal">
                                    <span class="icon-edit"></span>
                                    Edit  
                                </a>
                                <a class="btn btn-danger" style="margin-left: 15px;" data-toggle="modal" href="{{URL::to('logout')}}">
                                    <span class="icon-off"></span>
                                    logout  
                                </a>
                            </div>
                            <div class="span2">
                                <a class="btn btn-success" data-toggle="modal" href="#betaModal">
                                    <span class=" icon-ok"></span>
                                    Add Events  
                                </a>
                            </div>
                        </div>
                    </div>      
                </div>
            </div> 
             <div class="row">
                 <div class="col-md-6 col-md-offset-3">
                    <div class="span6 heading-div">
                        <h3 style="margin-top: 8px;">Events & Contests</h3>
                    </div>
                 </div>
             </div>
            
            <!--List Start-->
            
            
                        
             <div id="contentScroll" class="row col-md-6 col-md-offset-3 list-scroll">
                 @foreach($companyWithContests['contests'] as $key => $contests)
                 <div style="width: 100%;">
                    <div class="account-box" >
                        
                        <div class="row-fluid" style="color:#fff !important;">
                            <div class="span2">
                                <img style="width:200; height:80px;" src="../images/events/{{ $contests -> image }}" alt="No image" class="img-responsive">
                            </div>

                            <div class="span6" style="width: 47.3%;">
                                <h4 style="margin-top: 0px;">{{ $contests -> name }}</h4>
                                <h6>{{ $contests -> location }}</h6>
                                <h6>{{ $contests -> score }}</h6>
                            </div>
                            <div class="span2">
                                <button type="button" class="btn" data-toggle="modal" data-target="#EditModal<?php echo $contests -> id; ?>">
                                   <span class="icon-edit"></span>
                                    Edit
                                </button>
                            </div>
                            <div class="span2">
                                <a class="btn  btn-danger" href="{{ URL::to('company/event/'.$contests -> id) }}" onclick="if(!confirm('Are you sure to delete this item?')){return false;};">
                                    <span class="icon-trash"></span>
                                    Trash  
                                </a>
                            </div>
                        </div>
                    </div>      
                </div>
               
               <!--   modal ---->
               
               <div id="EditModal<?php echo $contests -> id; ?>" class="modal fade">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">×</button>
                    <h3>Edit Events & Contests</h3>
                </div>
                <div class="modal-body">
                    
                    <div class="row-fluid">
                        
                        <div class="span12">
                            <form action="{{ URL::to('company/event/edit') }}" method="post" enctype="multipart/form-data" runat="server">
                            <div class="span6">
                                
                                <div class="logowrapper">
                                    
                                        <div class="fileUpload btn btn-primary btn-primary-style">
                                            <span>
                                                <div class="LogoText">
                                                    <img style="width:100%; height:100%;" src="../images/events/{{ $contests -> image }}" alt="Click to show Image" id="{{ $contests -> id }}" />
                                                </div>    
                                            </span>
                                            <input onchange="readURL(this,app='{{ $contests -> id }}');" type="file" name='app_image' class="upload my-upload"/>
                                            <input type='hidden' name='eventid' class="prependedInput" size="16" type="text" value="{{ $contests -> id }}" >
                                        </div>
                                 </div>
                              </div>
                            <div class="span6">
                                    <p class="help-block">Name</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on">*</span><input name ="event_name" class="prependedInput" size="16" type="text" value="{{ $contests -> name }}">
                                    </div>
                                    
                                    <p class="help-block">City</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on">*</span><input name ="city" class="prependedInput" size="16" type="text" value="{{ $contests -> location }}">
                                    </div>
                                    <p class="help-block">Concept</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on"></span><input name ="concept" class="prependedInput" size="16" type="text" value="{{ $contests -> concept }}">
                                    </div>
                                    
                                    
                                    
                                    <p class="help-block">Contact Name</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on"></span><input name ="contact_name" class="prependedInput" size="16" type="text" value="{{ $contests -> contact_name }}">
                                    </div>
                                    
                                    <p class="help-block">Email</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on">*</span><input name ="email" class="prependedInput" size="16" type="text" value="{{ $contests -> email }}">
                                    </div>
                                    
                                    <p class="help-block">Phone</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on">*</span><input name ="phone" class="prependedInput" size="16" type="text" value="{{ $contests -> phone }}">
                                    </div>
                                    
                                    <p class="help-block">Judges</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on">*</span><input name ="judges" class="prependedInput" size="16" type="text" value="{{ $contests -> judges }}">
                                    </div>
                                    
                                    <p class="help-block">Reward Type</p>
                                    
                                    <div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 0px;">
					<div class="form-group">
                                            <select class="span2" name="reward_type1" style="padding-left: none;width: 118px !important;">
                                                <option value="{{ $contests -> reward1_type }}">{{ $contests -> reward1_type }}</option>
                                                @foreach($reward_types as $reward)
                                                <option value="{{ $reward -> reward_type}}">{{ $reward -> reward_type }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group">
                                                <input style="height:30px;width: 85px !important;" value="{{ $contests -> reward1_desc }}" type="text" name="reward1" id="reward1" class="form-control input-lg" tabindex="2">
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 0px;">
					<div class="form-group">
                                            <select class="span2" name="reward_type2" style="padding-left: none;width: 118px !important;">
                                                <option value="{{ $contests -> reward2_type }}">{{ $contests -> reward2_type }}</option>
                                                @foreach($reward_types as $reward)
                                                 <option value="{{ $reward -> reward_type }}">{{ $reward -> reward_type }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group">
                                                <input style="height:30px;width: 85px !important;" value="{{ $contests -> reward2_desc }}" type="text" name="reward2" id="reward2" class="form-control input-lg" tabindex="2">
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 0px;">
					<div class="form-group">
                                            
                                            <select class="span2" name="reward_type3" style="width: 118px !important;">
                                                <option value="{{ $contests -> reward3_type }}">{{ $contests -> reward3_type }}</option>
                                                @foreach($reward_types as $key => $reward)
                                                 <option value="{{ $reward -> reward_type }}">{{ $reward -> reward_type }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group">
                                                <input style="height:30px;width: 85px !important;" value="{{ $contests->reward3_desc}}" type="text" name="reward3" id="reward3" class="form-control input-lg" tabindex="2">
                                        </div>
                                    </div>
                                    
                                    <p class="help-block">Rules</p>
                                    
                                    <textarea name ="rules" style="width:218px;height:100px;">{{ $contests -> rules }}</textarea>
                                    
                                    
                                    <input type='hidden' name='event_id' class="prependedInput" size="16" type="text" value="{{ $contests -> id }}">
                                    
                                    <hr>
                                    
                                    <div class="help-block">
                                        <button type="submit" class="btn btn-large btn-info">Submit Events</button>
                                    </div>
                                    
                                
                                
                             </div>
                            </form>
                            </div>
                        
                        </div>
                    </div>
                    
                   <div class="modal-footer">
                        <p><i>JMTV Events and Contests</i></p>
                    </div>
                </div>
               
                
             @endforeach     
            <!--List End--> 
             
          </div>  
                    
            
             <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" style="color:#fff; text-align: center;">
                        JMTV Pte. Ltd. - Co. Reg. n. 201314573M 
                    </div>
                </div>
             </div>
        </div>    
        <div id="betaModal" class="modal fade   ">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">×</button>
                <h3>Add Events & Contests</h3>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span12">
                       <form action="{{ URL::to('create/event') }}" class="form-signin" method="post" enctype="multipart/form-data" runat="server">
                           
                        <div class="span6">
                            <div class="logowrapper">
                                  
                                    
                                    <div class="fileUpload btn btn-primary btn-primary-style">
                                        <span>
                                            <div class="LogoText">
                                                <!--img style="width:100%; height:100%" src="" class="img-responsive"-->
                                                
                                                  <img style="width:100%; height:100%;" alt="Click to show Image" id="1" src="#" />
                                            </div>
                                            <input onchange="readURL(this, app=1);" type="file" name='event_image' class="upload my-upload"/>
                                        </span>
                                        
                                        <input type='hidden' name='eventid' class="prependedInput" size="16" type="text" value="">
                                    
                                    </div>
                                
                            </div>
                        </div>
                        
                        <div class="span6">
                                
                            <p class="help-block">Name</p>
                            <div class="input-prepend">
                                <span class="add-on">*</span><input name='contest_name' class="prependedInput" size="16" type="text">
                            </div>

                            <p class="help-block">City</p>
                            <div class="input-prepend">
                                <span class="add-on">*</span><input name='city' class="prependedInput" size="16" type="text">
                            </div>
                            
                            <p class="help-block">Concept</p>
                            <div class="input-prepend">
                                <span class="add-on"></span><input name ="concept" class="prependedInput" size="16" type="text" >
                            </div>
                            
                            <input type='hidden' name='id' class="prependedInput" size="16" type="text" value="{{ $companyid }}">
                            <hr>

                            <div class="help-block">
                                <button type="submit" class="btn btn-large btn-info">Submit Events</button>
                            </div>
                                
                            
                          </div>
                           </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <p><i>JMTV Events and Contests</i></p>
            </div>
            
        </div>
        
        <div id="CFModal" class="modal fade">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">×</button>
                <h3>Edit Company Profile Info </h3>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <form action="{{ URL::to('company/edit') }}" method="post" enctype="multipart/form-data" runat="server">
                    <div class="span12">
                        
                        <div class="span6">
                            
                            <div class="logowrapper">
                                
                                    
                                    <div class="fileUpload btn btn-primary btn-primary-style">
                                        <span>
                                            <div class="LogoText">
                                                <img style="width:100%; height:100%" src="../images/companys/{{ $companyWithContests['image'] }}" alt="Click to show Image" id="3" class="img-responsive">
                                                
                                            </div>    
                                        </span>
                                        <input onchange="readURL(this,app=3);" type="file" name='company_image' class="upload my-upload"/>
                                        
                                    </div>
                                    
                                    
                                  
                              </div>
                          </div>
                        <div class="span6">
                            
                                <p class="help-block">Name</p>
                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name='name' class="prependedInput" size="16" type="text" value="{{ $companyWithContests['name'] }}">
                                </div>
                                <p class="help-block">Description</p>
                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name='description' class="prependedInput" size="16" type="text" value="{{ $companyWithContests['description'] }}">
                                </div>
                                <p class="help-block">Address</p>
                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name='address' class="prependedInput" size="16" type="text" value="{{ $companyWithContests['address'] }}">
                                </div>
                                <p class="help-block">Contact Name</p>
                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name='contact_name' class="prependedInput" size="16" type="text" value=" {{ $companyWithContests['contact_name'] }} " >
                                </div>
                                <p class="help-block">email</p>
                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name='email' class="prependedInput" size="16" type="text" value="{{ $companyWithContests['email'] }}">
                                </div>
                                <p class="help-block">website</p>
                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name='website' class="prependedInput" size="16" type="text" value="{{ $companyWithContests['website'] }}">
                                </div>
                                <p class="help-block">phone</p>
                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name='phone' class="prependedInput" size="16" type="text" value="{{ $companyWithContests['phone'] }}">
                                </div>
                                <p class="help-block">FAX</p>
                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name='fax' class="prependedInput" size="16" type="text" value="{{ $companyWithContests['fax'] }}">
                                </div>
                                <input type='hidden' name='id' class="prependedInput" size="16" type="text" value="{{ $companyWithContests['id'] }}">
                                <hr>
                                <div class="help-block">
                                    <button type="submit" class="btn btn-large btn-info">Submit</button>
                                </div>
                            
                        </div>
                    </div>
                        </form>
                </div>
            </div>
            <div class="modal-footer">
                <p><i>Company Profile information</i></p>
            </div>
        </div>
        
        
        <script type="text/javascript" src="../packages/bootstrap/js/jquery.js"></script>
        <script type="text/javascript" src="../packages/bootstrap/js/bootstrap.js"></script>
        <script type="text/javascript" src="../packages/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../lib/js/CustomScrollbar.js"></script> 
        <script>
            (function($){
                $(window).load(function(){
                    $("#contentScroll").mCustomScrollbar({
                        scrollButtons:{
                            enable:true
                        }
                    });

                });
            })(jQuery);
            
            function readURL(input, id) {
             if (input.files && input.files[0]) {
                 var reader = new FileReader();
                 reader.onload = function (e) {
                 $('#'+id).attr('src', e.target.result);
                }
                 reader.readAsDataURL(input.files[0]);
                }
             }
             
        </script>
            
        </body>
</html>
