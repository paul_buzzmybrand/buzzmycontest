<div>
    <p>
        BuzzMyBrand charges the customer once a contest has been launched through our websites www.buzzmybrand.co, www.buzzmybrand.com and www.buzzmybrand.it. All services are not-refundable once the contest has started. Once a customer launches a contest and provides billing information BuzzMyBrand will calculate the due amount according to the package selected by the customer.
    </p>
    <p>
        The payment will be performed by credit card. The related credit card account and references are stored in our server. BuzzMyBrand will store one credit card account per time for each customer. The customer must provide BuzzMyBrand with a valid credit card account as a condition to use our service. BuzzMyBrand reserves the right to change or modify its fees structure and introduce subscription plan in the future. 
    </p>
    <p>
        <u>Important: No refunds or credits for partial usage of our service will be refunded to a customer after a contest has started or has already finished. </u>
    </p>
    <p>
        An invoice is issued simultaneously with the payment performed by credit card. The invoice will include the cost of the contest and all the company information pursuant to UAE applicable laws and rules. 
    </p>
    <p>
        All fees are exclusive of all taxes, levies, or duties imposed by taxing authorities, and the customer is responsible for payments of all such taxes, levies, or duties, if any. Any currency exchange settlements are based on the customer agreement with the payment provider. To be clear: these taxes and charges, if any, are the customers’ responsibility. 
    </p>
    <p>
        A contest launched but still not started may be deleted by the customer before the starting date. The customer shall cancel its credit card data whenever he/she considers it necessary directly from the related dashboard section. A contest that has already started cannot be refunded. 
    </p>
    <p>
        Fraud: Without limiting any other remedies, BuzzMyBrand may suspend or terminate your account if we suspect that you (by conviction, settlement, insurance or escrow investigation, or otherwise) have engaged in fraudulent activity in connection with our website. 
    </p>
</div>