<!DOCTYPE html>
<html ng-app="landingApp">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ContestEngine Login</title>
	<link href="{{ asset('website_public/contestengine/css/normalize.css') }}" rel="stylesheet">
	<link href="{{ asset('website_public/contestengine/css/style.css') }}" rel="stylesheet">
</head>
<body>
	<section class="loginform cf">
		<form name="loginForm" id="loginForm" method="POST" ng-submit="loginForm.$valid && signIn(login)" ng-controller="SignInController" novalidate>
			<ul>
				<li>
					<label for="usermail">Email</label>
					<input type="email" placeholder="e-mail" class="form-control input-sm" name="username" value="{{ Input::old('email') }}" ng-model="login.username" required>
				</li>
				<li>
					<label for="password">Password</label>
					<input ng-if="!passwordRecoveryBlock" placeholder="password" class="form-control input-sm" name="password" ng-model="login.password" required ng-minlength="4"></li>
				<li>
					<!--<input type="submit" value="Login">-->
					<button type="submit" id="btn-login" class="btn btn-login">Login</button>
				</li>
			</ul>
		</form>
	</section>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
	<script src="{{ asset('website_public/contestengine/js/ng/landing.app.js') }}"></script> 
	
</body>
</html>