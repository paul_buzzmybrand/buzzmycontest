<div>
    <p>
        BuzzMyBrand addebita al cliente il dovuto importo una volta che lo stesso ha lanciato un contest tramite il nostro sito www.buzzmybrand.co, www.buzzmybrand.com e www.buzzmybrand.it. Tutti i servizi resi non sono rimborsabili. Nel momento in cui un cliente lancia un contest e fornisce le informazioni per la fatturazione, BuzzMyBrand calcolerà il dovuto importo. 
    </p>
    <p>
        Il pagamento deve essere eseguito con carta di credito. I dati relativi alle carte  vengono memorizzati e conservati nel nostro server. BuzzMyBrand memorizzerà una carta di credito per volta per ogni cliente. Il cliente è tenuto a fornire a BuzzMyBrand una carta di credito quale condizione necessaria per usufruire del nostro servizio. BuzzMyBrand riserva il diritto di cambiare o modificare la struttura di pagamento e di introdurre in futuro un piano di abbonamento.
    </p>
    <p>
        <u>Importante: nessun rimborso o credito per mesi, trimestri o anni di servizio parzialmente fruiti saranno rimborsato al cliente nel caso in cui un contest sia già iniziato o terminato.</u>
    </p>
    <p>
        La relativa fattura è emessa contemporaneamente con il pagamento avvenuto con carta di credito. I dati forniti al fine di compilare la fattura saranno pari al costo del contest lanciato con BuzzMyBrand e includono tutte le informazioni della società o della persona fisica ai sensi delle leggi e delle normative applicabili italiane e comunitarie. Una specifica fattura corrisponde ad un singolo contest.
    </p>
    <p>
        Tutte le commissioni sono al netto di tutte le tasse, imposte o dazi imposti dalle autorità fiscali, e l’utente è responsabile per i pagamenti di tutte queste tasse, imposte o dazi. Eventuali saldi dovuti ad un cambio di valuta si basano sul consenso prestato del cliente al provider del mezzo di pagamento. Per essere chiari: queste tasse e le relative spese sono a carico del cliente. 
    </p>
    <p>
        I contest già lanciati ma non ancora iniziati possono essere annullati dal cliente prima dell'inizio del contest stesso. Il cliente dovrà cancellare i dati relativi alla sua carta di credito, in ogni momento in cui lo ritenga necessario, direttamente dalla relativa sezione nel pannello di controllo (c.d. dashboard). Un contest che ha già avuto inizio non può essere rimborsato. 
    </p>
    <p>
        Frodi: Senza limiti sui rimedi esperibili, BuzzMyBrand può sospendere o chiudere l'account, se vi sia il sospetto che (per condanna, transazioni, assicurazioni o di indagini) l’utente sia coinvolto in attività fraudolente in relazione al nostro portale.
    </p>
</div>