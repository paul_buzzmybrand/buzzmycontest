<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>JMTV Company Profile</title>
        {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap-responsive.min.css') }}
        {{ HTML::style('lib/css/main.css')}}
        {{ HTML::style('lib/css/CustomScrollbar.css')}}
    </head>
    <body id="body">
        
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" >
                        <div class="logo">
                            <img src="lib/img/logo.png" alt=""/>
                        </div>
                        @if(Session::has('ErrorMessage'))
                            <div class="alert-box success">
                                <ol class="alert">
                                   <b>{{ Session::get('ErrorMessage') }}</b>
                                    @foreach($errors->all() as $error)
                                        <li style="margin-left: 20px;">{{ $error }}</li>
                                    @endforeach    
                                </ol>
                            </div>
                        @endif
                        <div class="row-fluid" style="color:#fff !important;">     
                            <div class="span13" >
                                <!--a class="btn btn-success" style="float: right; margin-right: -6px;" data-toggle="modal" href="#AddModal">
                                    <span class="icon-ok"></span>
                                        Add new Event & Contest  
                                </a-->
                                
                                <a class="btn btn-danger" style="float: right; margin-right: -6px;" href="{{ URL::to('company/logout')}}">
                                    <span class=" glyphicon glyphicon-log-out"></span>
                                        Logout
                                </a>
                                <a class="btn btn-default" style="float: right; margin-right: 12px;" href="company">
                                    <span class=" glyphicon glyphicon-arrow-left"></span>
                                        Company
                                </a>
                                <a class="btn btn-success" style=" margin-right: -6px;" data-toggle="modal" href="#AddModal">
                                    <span class="icon-ok"></span>
                                        Add new Event & Contest
                                </a>
                            </div>
                        </div>
                    </div>      
                </div>
            </div> 
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="span6 heading-div">
                        <h3 style="margin-top: 8px;">Events & Contests List</h3>
                    </div>
                </div>
            </div>

            <!--List Start-->
            
            
                <div id="contentScroll" class="row col-md-6 col-md-offset-3 list-scroll">
                    <div style="width: 100%;">
                    @foreach($contests as $contest)    
                        <div class="account-box" style="border-top: none;">

                            <div class="row-fluid" style="color:#fff !important;">

                                <div class="span8" >

                                    <h4 style="margin-top: 8px;"> <input type="checkbox" style="margin-right: 10px; margin-top: -3px;" />{{ $contest -> name }}</h4>

                                </div>
                                <div class="span2">
                                    <button type="button" class="btn" data-toggle="modal" data-target="#EditModal{{ $contest -> id }}">
                                        <span class="icon-edit"></span>
                                        Edit
                                    </button>
                                </div>
                                <div class="span2">
                                    <a  class="btn  btn-danger" data-toggle="modal" href="#confirmationModal">
                                        <span class="icon-trash"></span>
                                        Trash  
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        <div id="EditModal{{ $contest -> id }}" class="modal fade">
                            <div class="modal-header">
                                <button class="close" data-dismiss="modal">×</button>
                                <h3>Edit Events & Contests</h3>
                            </div>
                            <div class="modal-body">
                                <div class="row-fluid">
                                  <form action="{{ URL::to('event/editEvent') }}" method="post" enctype="multipart/form-data" runat="server">   
                                    <div class="span12">
                                        <div class="span6">
                                            <div class="logowrapper" >  
                                                <div class="fileUpload btn btn-primary btn-primary-style">
                                                    <span>
                                                        <div class="LogoText">
                                                            <img style="margin-top: -4px;width:100%; height:100%;" src="images/events/{{ $contest -> image }}" alt="Click to show Image" id="{{$contest -> id}}" />
                                                        </div>    
                                                    </span>
                                                    <input onchange="readURL(this,app=<?php echo $contest -> id; ?>)" type="file" name='image' class="upload my-upload" >
                                                    <input type='hidden' name='eventid' class="prependedInput" size="16" type="text" value="{{$contest -> id}}" >
                                                </div>
                                             </div>
                                        </div>
                                  <div class="span6">
                                    <p class="help-block">Name</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on">*</span><input name ="event_name" class="prependedInput" size="16" type="text" value="{{$contest -> name}}">
                                    </div>
                                    
                                    <?php
                                        $companyName = "";
                                        $id = 0;
                                            
                                        foreach($companys as $company)
                                        {
                                            if ($contest->company_id == $company->id){
                                              $companyName = $company -> name;
                                              $id = $company -> id;
                                            }            
                                        }
                                    ?>
                                    <p class="help-block">Company</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on">*</span><select class="span2" name="companyName" style="padding-left: none;width: 205px !important;">
                                                <option value="{{ $id }}"><?php if($companyName!="") { echo $companyName; }else { echo ""; } ?> </option>
                                            @foreach($companys as $company)
                                                 <option value="{{ $company -> id }}">{{ $company -> name }}</option>
                                            @endforeach
                                       
                                        </select>
                                    </div>
                                    
                                    <p class="help-block">City</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on">*</span><input name ="city" class="prependedInput" size="16" type="text" value="{{$contest -> location}}">
                                    </div>
                                    <p class="help-block">Concept</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on"></span><input name ="concept" class="prependedInput" size="16" type="text" value="{{$contest -> concept}}">
                                    </div>
                                    
                                    
                                    
                                    <p class="help-block">Contact Name</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on"></span><input name ="contact_name" class="prependedInput" size="16" type="text" value="{{$contest -> contact_name}}">
                                    </div>
                                    
                                    <p class="help-block">Email</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on">*</span><input name ="email" class="prependedInput" size="16" type="text" value="{{$contest -> email}}">
                                    </div>
                                    
                                    <p class="help-block">Phone</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on"></span><input name ="phone" class="prependedInput" size="16" type="text" value="{{$contest -> phone}}">
                                    </div>
                                    
                                    <p class="help-block">Judges</p>
                                    
                                    <div class="input-prepend">
                                        <span class="add-on">*</span><input name ="judges" class="prependedInput" size="16" type="text" value="{{$contest -> judges}}">
                                    </div>
                                    
                                    <p class="help-block">Reward Type</p>
                                    
                                    <div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 0px;">
					<div class="form-group">
                                            <select class="span2" name="reward_type1" style="padding-left: none;width: 118px !important;">
                                                <option value="{{ $contest -> reward1_type }}">{{ $contest -> reward1_type }}</option>
                                                @foreach($reward_types as $reward)
                                                    <?php if($reward-> reward_type == $contest -> reward1_type){ continue;} ?>
                                                        <option value="{{ $reward-> reward_type }}">{{ $reward-> reward_type }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group">
                                                <input style="height:30px;width: 85px !important;" value="{{ $contest -> reward1_desc }}" type="text" name="reward1" id="reward1" class="form-control input-lg" tabindex="2">
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 0px;">
					<div class="form-group">
                                            <select class="span2" name="reward_type2" style="padding-left: none;width: 118px !important;">
                                                <option value="{{ $contest -> reward2_type }}">{{ $contest -> reward2_type }}</option>
                                                
                                                @foreach($reward_types as $reward)
                                                    <?php if($reward-> reward_type == $contest -> reward2_type){ continue;} ?>
                                                        <option value="{{ $reward-> reward_type }}">{{ $reward-> reward_type }}</option>
                                                @endforeach
                                            
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group">
                                                <input style="height:30px;width: 85px !important;" value="{{ $contest -> reward2_desc }}" type="text" name="reward2" id="reward2" class="form-control input-lg" tabindex="2">
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 0px;">
					<div class="form-group">
                                            
                                            <select class="span2" name="reward_type3" style="width: 118px !important;">
                                                <option value="{{ $contest -> reward3_type }}">{{ $contest -> reward3_type }}</option>
                                                @foreach($reward_types as $reward)
                                                    <?php if($reward-> reward_type == $contest -> reward3_type){ continue;} ?>
                                                        <option value="{{ $reward-> reward_type }}">{{ $reward-> reward_type }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group">
                                                <input style="height:30px;width: 85px !important;" value="{{ $contest -> reward3_desc }}" type="text" name="reward3" id="reward3" class="form-control input-lg" tabindex="2">
                                        </div>
                                    </div>
                                    
                                    <p class="help-block">Rules</p>
                                    
                                    <textarea name ="rules" style="width:218px;height:100px;">{{ $contest -> rules }}</textarea>
                                    
                                    <input type='hidden' name='event_id' class="prependedInput" size="16" type="text" value="{{ $contest -> id }}">
                                    
                                    <hr>
                                    
                                    <div class="help-block">
                                        <button type="submit" class="btn btn-large btn-info">Submit Events</button>
                                    </div>
                                    
                             </div>
                            
                            </div>
                        </form>
                        </div>
                    </div>
                    
                   <div class="modal-footer">
                        <p><i>JMTV Events and Contests</i></p>
                    </div>
                </div>
                    
                    @endforeach
                    </div>

                </div> 
            
            <!--List End--> 



            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" style="color:#fff; text-align: center;">
                        JMTV Pte. Ltd. - Co. Reg. n. 201314573M 
                    </div>
                </div>
            </div>
        </div>    
        <div id="AddModal" class="modal fade">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">×</button>
                <h3>Add new Events & Contests</h3>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <form action="{{ URL::to('event/addEvent') }}" method="post" enctype="multipart/form-data" runat="server">   
                        <div class="span12">
                            <div class="span6">
                                <div class="logowrapper" >  
                                    <div class="fileUpload btn btn-primary btn-primary-style">
                                        <span>
                                            <div class="LogoText">
                                                <img style="margin-top: -4px;width:100%; height:100%;" src="" alt="Click to show Image" id="t100" >
                                            </div>    
                                        </span>
                                        <input onchange="readURL(this,app='t100');" type="file" name='eventImage' class="upload my-upload" >
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <p class="help-block">Name</p>

                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name ="event_name" class="prependedInput" size="16" type="text" value="">
                                </div>
                                
                                <p class="help-block">Company</p>
                                    
                                <div class="input-prepend">
                                    <span class="add-on">*</span><select class="span2" name="companyName" style="padding-left: none;width: 205px !important;">
                                            <option value="">Select Company</option>
                                        @foreach($companys as $company)
                                             <option value="{{ $company -> id }}">{{ $company -> name }}</option>
                                        @endforeach

                                    </select>
                                </div>

                                <p class="help-block">City</p>

                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name ="city" class="prependedInput" size="16" type="text" value="">
                                </div>
                                <p class="help-block">Concept</p>

                                <div class="input-prepend">
                                    <span class="add-on"></span><input name ="concept" class="prependedInput" size="16" type="text" value="">
                                </div>
                                
                                <p class="help-block">Contact Name</p>

                                <div class="input-prepend">
                                    <span class="add-on"></span><input name ="contact_name" class="prependedInput" size="16" type="text" value="">
                                </div>

                                <p class="help-block">Email</p>

                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name ="email" class="prependedInput" size="16" type="text" value="">
                                </div>

                                <p class="help-block">Phone</p>

                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name ="phone" class="prependedInput" size="16" type="text" value="">
                                </div>

                                <p class="help-block">Judges</p>

                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name ="judges" class="prependedInput" size="16" type="text" value="">
                                </div>

                                <p class="help-block">Reward Type</p>

                                <div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 0px;">
                                    <div class="form-group">
                                        <select class="span2" name="reward_type1" style="padding-left: none;width: 118px !important;">
                                            @foreach($reward_types as $reward)
                                                
                                                <option value="{{ $reward-> reward_type }}">{{ $reward-> reward_type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input style="height:30px;width: 85px !important;" value="" type="text" name="reward1" id="reward1" class="form-control input-lg" tabindex="2">
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 0px;">
                                    <div class="form-group">
                                        <select class="span2" name="reward_type2" style="padding-left: none;width: 118px !important;">
                                            @foreach($reward_types as $reward)
                                                
                                                <option value="{{ $reward-> reward_type }}">{{ $reward-> reward_type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input style="height:30px;width: 85px !important;" value="" type="text" name="reward2" id="reward2" class="form-control input-lg" tabindex="2">
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 0px;">
                                    <div class="form-group">

                                        <select class="span2" name="reward_type3" style="width: 118px !important;">
                                            @foreach($reward_types as $reward)
                                                <option value="{{ $reward-> reward_type }}">{{ $reward-> reward_type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input style="height:30px;width: 85px !important;" value="" type="text" name="reward3" id="reward3" class="form-control input-lg" tabindex="2">
                                    </div>
                                </div>

                                <p class="help-block">Rules</p>

                                <textarea name ="rules" style="width:218px;height:100px;"></textarea>
                                
                                <hr>

                                <div class="help-block">
                                    <button type="submit" class="btn btn-large btn-info">Submit Events</button>
                                </div>

                            </div>

                        </div>
                    </form>
                </div>
            </div>

            <div class="modal-footer">
                <p><i>JMTV Events and Contests</i></p>
            </div>
        </div>
        
        <div class="container">
            <div class="row">
                <div id="confirmationModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                   
                        <div class="modal-content">

                            <div class="modal-header">
                                <a class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></a>
                                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure you want to delete this?</p>
                            </div>
                            <div class="modal-footer">
                                <div class="btn-group">
                                    <button class="btn btn-danger" data-dismiss="modal">NO</button>
                                    <a href="{{ URL::to('contest/deleteContest/'.$contest -> id ) }}"><button class="btn btn-primary">YES</button></a>
                                </div>
                            </div>

                        </div><!-- /.modal-content -->
                   
                </div><!-- /.modal -->

        <script type="text/javascript" src="packages/bootstrap/js/jquery.js"></script>
        <script type="text/javascript" src="packages/bootstrap/js/bootstrap.js"></script>
        <script type="text/javascript" src="packages/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="lib/js/CustomScrollbar.js"></script>
        <script>
            (function($) {
                $(window).load(function() {
                    $("#contentScroll").mCustomScrollbar({
                        scrollButtons: {
                            enable: true
                        }
                    });

                });
            })(jQuery);
            function readURL(input, id) {
             if (input.files && input.files[0]) {
                 var reader = new FileReader();
                 reader.onload = function (e) {
                    $('#'+id).attr('src', e.target.result);
                 }
                 reader.readAsDataURL(input.files[0]);
                }
             }

        </script>
    </body>
</html>
