<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>JMTV Sign Up</title>
        {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap-responsive.min.css') }}
    	{{ HTML::style('lib/css/main.css')}}
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="account-box" >
                    <div class="logo">
                        <img src="lib/img/logo.png" alt=""/>
                    </div>
                    
                    {{ Form::open(array('url'=>'registeration', 'class'=>'form-signin', 'files' => true)) }}
                        
                        <div style="color:#fff;">
                            @if(Session::has('message'))
                                <ol class="alert">
                                   <B> {{ Session::get('message') }}</b>
                                    @foreach($errors->all() as $error)
                                        <li style="margin-left: 20px;">{{ $error }}</li>
                                    @endforeach    
                                </ol>
                            @endif
                         </div>   
                        <div class="form-group">
                            <input type="text" name='company_name' class="form-control mytext" placeholder="COMPANY NAME" autofocus/>
                        </div>
                    
                        <div class="form-group">
                            <input type="text" name='website' class="form-control mytext" placeholder="WEBSITE" autofocus/>
                        </div>
                    
                        <div class="form-group">
                            <input type="text" name='username' class="form-control mytext" placeholder="USERNAME" autofocus/>
                        </div>
                        
                        <div>
                            <div class="form-group col-md-6 text_box">
                                <input type="text" name='firstname' class="form-control mytext" placeholder="FIRSTNAME" autofocus />
                            </div>
                            <div class="form-group col-md-6 text_box2">
                                <input type="text" name='lastname' class="form-control mytext" placeholder="LASTNAME" autofocus />
                            </div>
                        </div>
                        
                            <div class="form-group">
                                <label class="radio-inline" style="color:#fff">
                                    <input type="radio" name="sex" id="inlineCheckbox1" value="male" />
                                    Male
                                </label>
                                <label class="radio-inline" style="color:#fff">
                                    <input type="radio" name="sex" id="inlineCheckbox2" value="female" />
                                    Female
                                </label>
                            </div>
                            
                        <div class="form-group">
                            <input type="text" name='country' class="form-control mytext" placeholder="COUNTRY"  autofocus />
                        </div>
                        <div class="form-group">
                            <input type="text" name='city' class="form-control mytext" placeholder="CITY" autofocus />
                        </div>
                        <div class="form-group">
                            <input type="text" name='phone' class="form-control mytext" placeholder="PHONE NUMBER"  autofocus />
                        </div>
                        <div class="form-group">
                            <input type="text" name='address' class="form-control mytext" placeholder="ADDRESS" autofocus />
                        </div>
                        
                        <div class="form-group">
                            <input type="email" name='email' class="form-control mytext" placeholder="E-MAIL"  autofocus />
                        </div>
                        
                        <div class="form-group">
                            <input type="password" name='password' class="form-control mytext" placeholder="PASSWORD" />
                        </div>
                    
                        <div class="form-group">
                            <input type="password" name='password_confirmation' class="form-control mytext" placeholder="PASSWORD" />
                        </div>
                       
                        <!--
                        <div class="profilepic2">
                            <div class="fileUpload btn btn-primary my-uploadbg" style="padding:0px">
                                <span>
                                    <div class="profilepic">
                                        Your Photo
                                    </div>   
                                </span>
                                <input name="companyImage" type="file" class="upload profilepic" />
                            </div>
                        </div>
                        -->
                        <label class="checkbox" style="color:#fff; margin-left: -20px; font-weight: normal;">
                            Signing up you accept JMT <a class="mylink" href="{{ URL::to('terms') }}"> Terms of Service</a> &<a  class="mylink" href="{{ URL::to('privacy') }}"> Privacy Policy</a>
                        </label>
                    
                        <button class="btn-lg btn-block mybtn" type="submit">
                            SIGN UP
                        </button>
                        
                        {{ Form::close() }}
                        
                        <label class="checkbox" style="color:#fff; margin-left: -20px; font-weight: normal;">
                            <a class="mylink" href="{{ URL::to('/') }}"> if you have a account Sign in?</a>
                        </label>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
