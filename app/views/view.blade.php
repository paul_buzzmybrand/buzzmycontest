<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>JMTV Login</title>
        {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap-responsive.min.css') }}
    	{{ HTML::style('lib/css/main.css')}}
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="account-box" >
                    <div class="logo">
                        <img src="lib/img/logo.png" alt=""/>
                    </div>
                    
                    <ul style="color:#fff !important;">
                        @if(Session::has('message'))
                            {{ Session::get('message') }}
                        @endif
                        
                        @foreach($errors->all() as $error)
                           <li>{{ $error }}</li>
                        @endforeach
                     </ul>
                    <div class="form-signin" style="height: 405px;">
                        
                        <a href="{{ URL::to('company') }}" class="btn-lg btn-block mybtn btn-s" style="text-decoration: none;">
                            Company
                        </a>
                        <br>
                        <br>
                        
                        <a href="{{ URL::to('contest') }}" class="btn-lg btn-block mybtn btn-s" style="text-decoration: none;">
                            Events / Contests
                        </a>
                    </div>
                    
                    
                    </div>
                </div>
            
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" style="color:#fff; text-align: center;">
                        JMTV Pte. Ltd. - Co. Reg. n. 201314573M 
                    </div>
                </div>
             </div>
        </div>
        
    </div>
</body>
</html>
