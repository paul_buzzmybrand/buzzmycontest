<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>JMTV Login</title>
        {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap-responsive.min.css') }}
    	{{ HTML::style('lib/css/main.css')}}
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="account-box" >
                    <div class="logo">
                        <img src="lib/img/logo.png" alt=""/>
                    </div>
                    {{ Form::open(array('url'=>'users/login', 'class'=>'form-signin')) }}
                        @if(Session::has('message'))
                            <p style="text-align: center;color:#FF9900"> {{ Session::get('message') }} </p>
                        @endif
                        <div class="form-group">
                            <input type="text" name="username" class="form-control mytext" placeholder="USERNAME" required autofocus />
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control mytext" placeholder="PASSWORD" required />
                        </div>
                        <label class="checkbox">
                            <a class="forgotLnk" href="forget">Forget Passowrd?</a>
                        </label>
                        <button class="btn-lg btn-block mybtn" type="submit">
                            LOGIN
                        </button>
                    {{ Form::close() }}
                    <div class="or-box">
                        <div class="row">
                            <div class="col-md-6 row-block">
                                <a href="#" class=" btn-facebook btn-block">LOGIN WITH <B>FACEBOOK</B></a>
                            </div>
                            <div class="col-md-6 row-block">
                                <a href="#" class="btn-google btn-block">LOGIN WITH <B>TWITTER</B></a>
                            </div>
                        </div>
                    </div>
                    <a href="create" class="btn-signup btn-block">SIGN UP</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
