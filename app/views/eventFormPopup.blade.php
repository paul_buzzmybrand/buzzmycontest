<div class="modal-header">
    <button class="close" data-dismiss="modal">×</button>
    <h3>Edit Events & Contests</h3>
</div>
<div class="modal-body">
    <div class="row-fluid">
        <div class="span12">
            <div class="span6">
                <div class="logowrapper">
                    <img class="logoicon" src="http://placehold.it/300x300/bbb/&text=Your%20Logo" alt="App Logo"/>
                </div>
            </div>
            <div class="span6">
                <form class="form-horizontal">
                    <p class="help-block">Name</p>
                    <div class="input-prepend">
                        <span class="add-on">*</span><input class="prependedInput" size="16" type="text">
                    </div>
                    <p class="help-block">City</p>
                    <div class="input-prepend">
                        <span class="add-on">*</span><input class="prependedInput" size="16" type="text">
                    </div>
                    <p class="help-block">JMTV Score</p>
                    <div class="input-prepend">
                        <span class="add-on">*</span><input class="prependedInput" size="16" type="text">
                    </div>
                    <hr>
                    <div class="help-block">
                        <button type="submit" class="btn btn-large btn-info">Submit Events</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <p><i>JMTV Events and Contests</i></p>
</div>