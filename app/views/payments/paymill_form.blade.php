  <script type="text/javascript">
      var PAYMILL_PUBLIC_KEY = '{{ Config::get('paymill.api_keys.public') }}';
  </script>

  <div class="container span8">
    <!-- START: Payment form -->
    <div class="controls">
      <div class="span4">
        <div class="payment-errors text-error"></div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="well span5">
 
      <form id="payment-form" action="" method="POST">
        <div class="clearfix"></div>
 
        <div id="payment-form-cc" class="payment-input">
          <input class="card-amount-int" type="hidden" value="15"/>
          <input class="card-currency" type="hidden" value="EUR"/>
          <div class="controls controls-row">
            <div class="span3"><label>Card number</label>
                <input class="card-number span3" type="text" size="20" value="4111111111111111"/>
            </div>
            <div class="span1"><label>CVC</label>
                <input class="card-cvc span1" type="text" size="4" value="111"/>
            </div>
          </div>
          <div class="controls">
            <div class="span4">
              <label>Card holder</label>
              <input class="card-holdername span4" type="text" size="20" value="Max Mustermann"/>
            </div>
          </div>
          <div class="controls">
            <div class="span3">
              <label>Valid until (MM/YYYY)</label>
              <input class="card-expiry-month span1" type="text" size="2" value="12"/>
              <span> / </span>
              <input class="card-expiry-year span1" type="text" size="4" value="2015"/>
            </div>
          </div>
        </div>
 
        <div class="controls">
          <div class="span3">
            <button class="submit-button btn btn-primary" type="submit">Buy now</button>
          </div>
        </div>
      </form>
    </div>
    <!-- END: Payment form -->
  </div>
 
  <script src="https://bridge.paymill.com/"></script>
  <script language="javascript" type="text/javascript">
      $(document).ready(function () {
          function PaymillResponseHandler(error, result) {
              if (error) {
                  // Show the error message above the form
                  $(".payment-errors").text(error.apierror);
              } else {
                  $(".payment-errors").text("");
                  var form = $("#payment-form");
                  // Token
                  var token = result.token;
                  // Insert token into the payment form
                  form.append("<input type='hidden' name='paymillToken' value='" + token + "'/>");
                  form.get(0).submit();
              }
              $(".submit-button").removeAttr("disabled");
          }
 
          $("#payment-form").submit(function (event) {
              // Deactivate the submit button to avoid further clicks
              $('.submit-button').attr("disabled", "disabled");
              if (false == paymill.validateCardNumber($('.card-number').val())) {
                  $(".payment-errors").text("Invalid card number");
                  $(".submit-button").removeAttr("disabled");
                  return false;
              }
 
              if (false == paymill.validateExpiry($('.card-expiry-month').val(), $('.card-expiry-year').val())) {
                  $(".payment-errors").text("Invalid date of expiry");
                  $(".submit-button").removeAttr("disabled");
                  return false;
              }
              
              var method = 'cc';
              var params = {
                  amount:         $('.card-amount-int').val(),
                  currency:       $('.card-currency').val(),
                  number:         $('.card-number').val(),
                  exp_month:      $('.card-expiry-month').val(),
                  exp_year:       $('.card-expiry-year').val(),
                  cvc:            $('.card-cvc').val(),
                  cardholder:     $('.card-holdername').val()
              };
 
              paymill.createToken(params, PaymillResponseHandler);
 
              return false;
          });
 
          // Toggle buttons and forms
         $(".paymenttype").click(function() {
            if (jQuery(this).hasClass('btn-primary')) return;
 
            jQuery('.paymenttype').removeClass('btn-primary');
            jQuery(this).addClass('btn-primary');
            var index = jQuery('.paymenttype').index(this);
 
            jQuery('.payment-input').hide();
            jQuery('.payment-input').eq(index).show();
        });
      });
  </script>
</body>
</html>