<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>JMTV Company Profile</title>
        {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap-responsive.min.css') }}
        {{ HTML::style('lib/css/main.css')}}
        {{ HTML::style('lib/css/CustomScrollbar.css')}}
    </head>
    <body id="body">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" >
                        <div class="logo">
                            <img src="lib/img/logo.png" alt=""/>
                        </div>
                        @if(Session::has('ErrorMessage'))
                            <div class="alert-box success">
                               
                                <ol class="alert">
                                   <b>{{ Session::get('ErrorMessage') }}</b>
                                    @foreach($errors->all() as $error)
                                        <li style="margin-left: 20px;">{{ $error }}</li>
                                    @endforeach    
                                </ol>
                            </div>
                        @endif
                        <div class="row-fluid" style="color:#fff !important;">     
                            <div class="span13" >
                                <!--a class="btn btn-success" style="float: right; margin-right: -6px;" data-toggle="modal" href="#betaModal">
                                    <span class="icon-ok"></span>
                                        Add new company  
                                </a-->
                                <a class="btn btn-danger" style="float: right; margin-right: -6px;" href="{{ URL::to('company/logout')}}">
                                    <span class=" glyphicon glyphicon-log-out"></span>
                                        Logout
                                </a>
                                <a class="btn btn-default" style="float: right; margin-right: 12px;" href="contest">
                                    <span class=" glyphicon glyphicon-arrow-left"></span>
                                        Events / Contests
                                </a>
                                <a class="btn btn-success" style=" margin-right: -6px;" data-toggle="modal" href="#betaModal">
                                    <span class="icon-ok"></span>
                                        Add new company 
                                </a> 
                            </div>
                        </div>
                    </div>      
                </div>
            </div> 
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="span6 heading-div">
                        <h3 style="margin-top: 8px;">Company List</h3>
                    </div>
                </div>
            </div>

            <!--List Start-->
            
            
                <div id="contentScroll" class="row col-md-6 col-md-offset-3 list-scroll">
                    <div style="width: 100%;">
                    @foreach($companys as $company)    
                        <div class="account-box" style="border-top: none;">

                            <div class="row-fluid" style="color:#fff !important;">

                                <div class="span8" >

                                    <h4 style="margin-top: 8px;"> <input type="checkbox" style="margin-right: 10px; margin-top: -3px;" />{{ $company -> name }}</h4>

                                </div>
                                <div class="span2">
                                    <button type="button" class="btn" data-toggle="modal" data-target="#EditModal{{ $company -> id }}">
                                        <span class="icon-edit"></span>
                                        Edit
                                    </button>
                                </div>
                                <div class="span2">
                                    <a class="btn  btn-danger" data-toggle="modal" href="#confirmationModal">
                                        <span class="icon-trash"></span>
                                        Trash  
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                        <div id="EditModal{{ $company -> id }}" class="modal fade">
                            <div class="modal-header">
                                <button class="close" data-dismiss="modal">×</button>
                                <h3>Edit company Details</h3>
                            </div>
                            <div class="modal-body">
                                <div class="row-fluid">
                                  <form action="{{ URL::to('company/editCompany') }}" method="post" enctype="multipart/form-data" runat="server">   
                                    <div class="span12">
                                        <div class="span6">
                                            <div class="logowrapper" >  
                                                <div class="fileUpload btn btn-primary btn-primary-style">
                                                    <span>
                                                        <div class="LogoText">
                                                            <img style="margin-top: -4px;width:100%; height:100%;" src="images/companys/{{ $company -> image }}" alt="Click to show Image" id="{{ $company -> id }}" />
                                                        </div>    
                                                    </span>
                                                    <input onchange="readURL(this,app=<?php echo $company -> id ?>);" type="file" name='company_image' class="upload my-upload" >
                                                    <input type='hidden' name='eventid' class="prependedInput" size="16" type="text" value="" >
                                                </div>
                                             </div>
                                        </div>
                                        <div class="span6">

                                                <p class="help-block">Company Name</p>
                                                <div class="input-prepend">
                                                    <span class="add-on">*</span><input name ="companyName" value="{{ $company -> name }}" class="prependedInput" size="16" type="text">
                                                </div>
                                                <p class="help-block">Contact Name</p>
                                                <div class="input-prepend">
                                                    <span class="add-on"></span><input name="contactName" value="{{ $company -> contact_name }}" class="prependedInput" size="16" type="text">
                                                </div>
                                                <p class="help-block">Address</p>
                                                <div class="input-prepend">
                                                    <span class="add-on"></span><input name ="address" value="{{ $company -> address }}" class="prependedInput" size="16" type="text">
                                                </div>
                                                <!--p class="help-block">Zip</p>
                                                <div class="input-prepend">
                                                    <span class="add-on">*</span><input name="zip" value="{{ $company -> zip }}" class="prependedInput" size="16" type="text">
                                                </div-->
                                                <p class="help-block">City</p>
                                                <div class="input-prepend">
                                                    <span class="add-on"></span><input name="city" value="{{ $company -> city }}" class="prependedInput" size="16" type="text">
                                                </div>
                                                <p class="help-block">Email</p>
                                                <div class="input-prepend">
                                                    <span class="add-on">*</span><input name="email" value="{{ $company -> email }}" class="prependedInput" size="16" type="text">
                                                </div>
                                                <p class="help-block">Phone</p>
                                                <div class="input-prepend">
                                                    <span class="add-on"></span><input name="phone" value="{{ $company -> phone }}" class="prependedInput" size="16" type="text">
                                                </div>
                                                <p class="help-block">Fax</p>
                                                <div class="input-prepend">
                                                    <span class="add-on"></span><input name="fax" value="{{ $company -> fax }}" class="prependedInput" size="16" type="text">
                                                </div>
                                                <p class="help-block">Website</p>
                                                <div class="input-prepend">
                                                    <span class="add-on">*</span><input name="website" value="{{ $company -> website }}" class="prependedInput" size="16" type="text">
                                                </div>
                                                <p class="help-block">Facebook page</p>
                                                <div class="input-prepend">
                                                    <span class="add-on"></span><input name="facebookPage" class="prependedInput" size="16" type="text" value="{{ $company -> facebook_link }}">
                                                </div>
                                                <p class="help-block">Twitter page</p>
                                                <div class="input-prepend">
                                                    <span class="add-on"></span><input name="twitterPage" class="prependedInput" size="16" type="text" value="{{ $company -> twitter_link }}">
                                                </div>
                                                
                                                <input type="hidden" value="{{ $company -> id }}" name="company_id" class="prependedInput" size="16">
                                                <hr>
                                                <div class="help-block">
                                                    <button type="submit" class="btn btn-large btn-info">Submit</button>
                                                </div>

                                        </div>
                                    </div>
                                   </form>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <p><i>JMTV Events and Contests</i></p>
                            </div>
                        </div>
                    
                    @endforeach
                    </div>

                </div> 
            
            <!--List End--> 



            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" style="color:#fff; text-align: center;">
                        JMTV Pte. Ltd. - Co. Reg. n. 201314573M 
                    </div>
                </div>
            </div>
        </div>    
        <div id="betaModal" class="modal fade">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">×</button>
                <h3>Add new company</h3>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                  <form action="{{ URL::to('company/addNewCompany') }}" method="post" enctype="multipart/form-data" runat="server">   
                    <div class="span12">
                        <div class="span6">
                            <div class="logowrapper" >  
                                <div class="fileUpload btn btn-primary btn-primary-style">
                                    <span>
                                        <div class="LogoText">
                                            <img style="margin-top: -4px;width:100%; height:100%;" src="images/default.png" alt="Click to show Image" id="0" />
                                        </div>    
                                    </span>
                                    <input onchange="readURL(this,app=0);" type="file" name='company_image' class="upload my-upload" >
                                    <input type='hidden' name='eventid' class="prependedInput" size="16" type="text" value="" >
                                </div>
                             </div>
                        </div>
                        <div class="span6">
                            
                                <p class="help-block">Company Name</p>
                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name ="companyName" class="prependedInput" size="16" type="text">
                                </div>
                                <p class="help-block">Contact Name</p>
                                <div class="input-prepend">
                                    <span class="add-on"></span><input name="contactName" class="prependedInput" size="16" type="text">
                                </div>
                                <p class="help-block">Address</p>
                                <div class="input-prepend">
                                    <span class="add-on"></span><input name ="address" class="prependedInput" size="16" type="text">
                                </div>
                                <!--p class="help-block">Zip</p>
                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name="zip" class="prependedInput" size="16" type="text">
                                </div-->
                                <p class="help-block">City</p>
                                <div class="input-prepend">
                                    <span class="add-on"></span><input name="city" class="prependedInput" size="16" type="text">
                                </div>
                                <p class="help-block">Email</p>
                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name="email" class="prependedInput" size="16" type="text">
                                </div>
                                <p class="help-block">Phone</p>
                                <div class="input-prepend">
                                    <span class="add-on"></span><input name="phone" class="prependedInput" size="16" type="text">
                                </div>
                                <p class="help-block">Fax</p>
                                <div class="input-prepend">
                                    <span class="add-on"></span><input name="fax" class="prependedInput" size="16" type="text">
                                </div>
                                <p class="help-block">Website</p>
                                <div class="input-prepend">
                                    <span class="add-on">*</span><input name="website" class="prependedInput" size="16" type="text">
                                </div>
                                <p class="help-block">Facebook page</p>
                                <div class="input-prepend">
                                    <span class="add-on"></span><input name="facebookPage" class="prependedInput" size="16" type="text">
                                </div>
                                <p class="help-block">Twitter page</p>
                                <div class="input-prepend">
                                    <span class="add-on"></span><input name="twitterPage" class="prependedInput" size="16" type="text">
                                </div>
                                
                                
                                <hr>
                                <div class="help-block">
                                    <button type="submit" class="btn btn-large btn-info">Submit</button>
                                </div>
                           
                        </div>
                    </div>
                   </form>
                </div>
            </div>
            <div class="modal-footer">
                <p><i>JMTV Events and Contests</i></p>
            </div>
        </div>
        
        <div class="container">
            <div class="row">
                <div id="confirmationModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                   
                        <div class="modal-content">

                            <div class="modal-header">
                                <a class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></a>
                                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure you want to delete this?</p>
                            </div>
                            <div class="modal-footer">
                                <div class="btn-group">
                                    <button class="btn btn-danger" data-dismiss="modal">NO</button>
                                    <a href="{{ URL::to('company/deleteCompany/'.$company -> id ) }}"><button class="btn btn-primary">YES</button></a>
                                </div>
                            </div>

                        </div><!-- /.modal-content -->
                   
                </div><!-- /.modal -->
        


        
        
        <script type="text/javascript" src="packages/bootstrap/js/jquery.js"></script>
        <script type="text/javascript" src="packages/bootstrap/js/bootstrap.js"></script>
        <script type="text/javascript" src="packages/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="lib/js/CustomScrollbar.js"></script>
        <script>
            (function($) {
                $(window).load(function() {
                    $("#contentScroll").mCustomScrollbar({
                        scrollButtons: {
                            enable: true
                        }
                    });

                });
            })(jQuery);
            
            function readURL(input, id) {
             if (input.files && input.files[0]) {
                 var reader = new FileReader();
                 reader.onload = function (e) {
                 $('#'+id).attr('src', e.target.result);
                }
                 reader.readAsDataURL(input.files[0]);
                }
             }

        </script>
    </body>
</html>
