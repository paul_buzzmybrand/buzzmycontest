<?php $tmz = new DateTimeZone($contest->location->timezoneDesc)?>
<?php if ($contest->location->language == "en") {
	$date = $start_date->format('Y-m-d');
} elseif ($contest->location->language == "it") {
	$date = $start_date->format('d-m-Y');
}?>

<p>{{Lang::get('email.request_confirmation.sentence1')}} {{$contest->name}} {{Lang::get('email.request_confirmation.sentence2')}} {{$date}}</p>



