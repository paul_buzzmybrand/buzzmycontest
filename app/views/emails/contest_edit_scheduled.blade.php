<p>{{Lang::get('email.contest_edit_scheduled_sentence_1')}} {{$user->name}}</p>
<?php $contestStartTime = DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time); ?>
<?php $contestStartTime->add(new DateInterval('PT' . $user->timezone_offset . 'M')); ?>
<?php $contestEndTime = DateTime::createFromFormat('Y-m-d H:i:s', $contest->end_time); ?>
<?php $contestEndTime->add(new DateInterval('PT' . $user->timezone_offset . 'M')); ?>
<p>{{Lang::get('email.contest_edit_scheduled_sentence_2')}} "{{$contest->name}}" {{ Lang::get('email.contest_edit_scheduled_sentence_3') }} {{ $contestStartTime->format('H:i') }} {{ Lang::get('email.contest_edit_scheduled_sentence_4') }} {{ $contestStartTime->format('d/m/y') }}.</p>
<p>{{ Lang::get('email.contest_edit_scheduled_sentence_5') }}</p>
<p>{{ Lang::get('email.contest_edit_scheduled_sentence_7') }}</p>
<table>
    <tbody>
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.type') }}</td>
            <td>{{ $contest->contestType['name'] }}</td>
        </tr>
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.language') }}</td>
            <td>{{ $contest->location_id == 2 ? 'Italiano' : 'English' }}</td>
        </tr>
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.objectives') }}</td>
            <td>
                @foreach($contest->objectives as $objective)
                    {{ $objective['name'] }}@if($objective != end($contest->objectives)), @endif
                @endforeach
            </td>
        </tr>
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.social') }}</td>
            <td>
				@if (count($contest->fbPages) > 0)
					Facebook: {{ $contest->fbPages->first()['fb_page_name'] }}<br>
				@endif
				@if (count($contest->twPages) > 0)
					Twitter: {{ $contest->twPages->first()['tw_page_name'] }}<br>
				@endif
				@if (count($contest->ytPages) > 0)
					Youtube: {{ $contest->ytPages->first()['yt_display_name'] }}<br>
				@endif
            </td>
        </tr>
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.title') }}</td>
            <td>{{ $contest->name }}</td>
        </tr>
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.hashtag') }}</td>
            <td>{{ $contest->hashtag }}</td>
        </tr>
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.brief') }}</td>
            <td>{{ $contest->concept }}</td>
        </tr>
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.how_to_win') }}</td>
            <td>{{ $contest->how_to_win }}</td>
        </tr>
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.rewards') }}</td>
            <td>
                @foreach($contest->rewards as $reward)
                    {{ $reward['rank'].'°: '.$reward['title'] }}@if($reward != end($contest->rewards)), @endif
                @endforeach
            </td>
        </tr>
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.dates') }}</td>
            <td>{{ $contest->location_id == 2 ? 'Dal' : 'From' }} {{ $contestStartTime->format('d/m/Y, H:i') }} {{ $contest->location_id == 2 ? 'al' : 'to' }} {{ $contestEndTime->format('d/m/Y, H:i') }}</td>
        </tr>
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.flyer') }}</td>
            <td>{{ $contest->template['name'] }}</td>
        </tr>
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.filter') }}</td>
            <td>{{ $contest->location_id == 2 ? ($contest->contest_approval ? 'Si' : 'No') : ($contest->contest_approval ? 'Yes' : 'No') }}</td>
        </tr>
        @if(count($contest->fbPages) > 0)
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.post_visibility') }}</td>                    
            <td>{{ $contest->fbPages->first()['fb_no_story'] }}</td>
        </tr>
        @endif
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.url') }}</td>
            @if ($contest->service_type == '1')
            <td>http://bmb.buzzmybrand.com/{{ $contest->contest_route }}</td>
			@else
			<td>http://www.contestengine.net/{{ $contest->contest_route }}</td>
			@endif
        </tr>
		@if(count($contest->fbPages) > 0)
		<tr>
			<td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.url_facebook_tab') }}</td>
			<td>{{ $contest->fbPages->first()['fb_page_link'].'app/'.$contest->company->fbApp_clientID }}</td>			
		</tr>
		@endif
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.promotion_budget') }}</td>
            <td>{{ $contest->promotion_budget }}</td>
        </tr>
        <tr>
            <td width="20%" style="text-transform:uppercase; font-weight:bold;">{{ Lang::get('contest.package') }}</td>
            <td>{{ Contest::$serviceTypeVerbose[$contest->service_type] }}</td>
        </tr>
    </tbody>
</table>
<p>{{ Lang::get('email.contest_edit_scheduled_sentence_6') }}</p>
<br>
<p>BuzzMyBrand Team</p>