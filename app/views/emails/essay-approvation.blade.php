<?php if ( $essay->approval_step_id  == 3 ) { ?>
<h1 style="color: #000000; font-size: 26px; width: 100%; text-align: center;"><b>{{Lang::get('messages.essay_approved')}}</b></h1>
<br>
<?php } else if ( $essay->approval_step_id  == 1 || $essay->approval_step_id  == 2 ) { ?>
<h1 style="color: #000000; font-size: 26px; width: 100%; text-align: center;"><b>{{Lang::get('messages.essay_not_approved')}}</b></h1>
<br>
<?php } ?>


<?php
	$width = Image::make(base_path()."/public/essays/".$essay->image)->width();
	$height = Image::make(base_path()."/public/essays/".$essay->image)->height();
		
?>

<div style="width: 350px; height: auto; margin: 0 auto;">
	<img src="{{asset('https://www.buzzmybrand.com/essays/'.$essay->image)}}" height="auto" width="350px">
</div>



<div style="font-size: 14px; color: #8d8d8d; width: 384px; height: 50px; margin: 0 auto; text-align: center; line-height: 50px"></div>

<br>










<?php if ( $essay->approval_step_id  == 3 ) { ?>

	<?php if ( $essay->fbPages->count() > 0 ) { ?>
	<div style="width: 384px; height: 37px; margin: 0 auto; text-align: center;">
	<a href="https://www.facebook.com/photo.php?fbid={{$essay->fbPages->first()->pivot->fb_idessay}}" style="color: #000000; font-size: 26px; font-weight: 900; max-width: 640px;">FACEBOOK LINK</a>
	</div>
	<br>
	<?php } ?>
	
	<?php if ( $essay->twPages->count() > 0 ) { ?>
	<?php $tw_idessay =  $essay->twPages->first()->pivot->tw_idessay;
			$screen_name = $essay->twPages->first()['tw_screen_name']; ?>
	<div style="width: 384px; height: 37px; margin: 0 auto; text-align: center;">
	<a href="https://twitter.com/{{$screen_name}}/status/{{$tw_idessay}}" style="color: #000000; font-size: 26px; font-weight: 900; max-width: 640px;">TWITTER LINK</a>
	</div>
	<br>
	<?php } ?>	
		
		
<?php } ?>

<?php
$contestRoute = "https://www.buzzmybrand.com/minisite/" . $contest->id;
if ( $contest->contest_route ) {
	$contestRoute = "http://bmb.buzzmybrand.com/" . $contest->contest_route;
}

?>

<div style="width: 100%; height: 52px;">
	<div style="clear: both;"></div>
	
	<div style="float: left; width: auto; height: 52px;"><a href="{{$contestRoute}}" target="_blank">
	<?php if ( $essay->approval_step_id == 3 ) { ?>
	<img src="http://www.buzzmybrand.com/website_public/landing/contest-site.png" height="52px" width="100%"></img>
	<?php } else if ( $essay->approval_step_id == 1 || $essay->approval_step_id == 2 ) { ?>
	<img src="http://www.buzzmybrand.com/website_public/landing/TryAgain-minisite.png" height="52px" width="100%"></img>
	<?php } ?>
	</a></div>
	
	<div style="clear: both;"></div>
</div>

<br>



