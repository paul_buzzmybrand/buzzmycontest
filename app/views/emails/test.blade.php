<p>{{Lang::get('email.invite.sentence1')}} {{$name_friend}}!</p>
<p>{{Lang::get('email.invite.sentence2')}} {{$name}} {{Lang::get('email.invite.sentence3')}} "{{$contest_name}}" {{Lang::get('email.invite.contest_en')}}</p>
<p>{{Lang::get('email.invite.sentence4')}} <a href="{{$contest_fb_link}}">{{Lang::get('email.invite.sentence5')}}</a>!</p>
