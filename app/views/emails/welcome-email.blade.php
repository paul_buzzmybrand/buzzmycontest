<p><b>{{Lang::get('email.welcome_email_1')}} {{ $user->name }},</b></p>
 
<p>{{Lang::get('email.welcome_email_2')}}<br>{{Lang::get('email.welcome_email_3')}}</p>


<p>{{Lang::get('email.welcome_email_4')}} <a href="{{asset('/admin/verify-email/' . $user->id . '/' . $user->rand_key)}}">{{ Lang::get('email.welcome_email_9') }}</a> {{Lang::get('email.welcome_email_8')}}</p>

<p><a href="{{asset('/admin/verify-email/' . $user->id . '/' . $user->rand_key)}}">{{asset('/admin/verify-email/' . $user->id . '/' . $user->rand_key)}}</a></p>

<p>{{Lang::get('email.welcome_email_5')}} </p>

<p>{{Lang::get('email.welcome_email_6')}} <br> {{Lang::get('email.welcome_email_7')}}</p>
<br>
<p>BuzzMyBrand Team</p>