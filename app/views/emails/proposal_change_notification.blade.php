<p>{{Lang::get('email.proposal_change_notification.hi')}} {{$customer->name}} {{$customer->surname}}</p>
<p>{{Lang::get('email.proposal_change_notification.sentence1')}}: {{$contest->name}}</p>
<?php if ( $activated_influencer ) { ?>
<p>{{Lang::get('email.proposal_change_notification.activated_influencers')}}: {{$activated_influencer}}</p>
<?php } ?>
<?php if ( $estimated_post_reach ) { ?>
<p>{{Lang::get('email.proposal_change_notification.estimated_post_reach')}}: {{$estimated_post_reach}}</p>
<?php } ?>
<?php if ( $estimated_new_leads ) { ?>
<p>{{Lang::get('email.proposal_change_notification.estimated_new_leads')}}: {{$estimated_new_leads}}</p>
<?php } ?>
<?php if ( $estimated_downloads ) { ?>
<p>{{Lang::get('email.proposal_change_notification.estimated_downloads')}}: {{$estimated_downloads}}</p>
<?php } ?>



