<img src="{{asset('/website_public/emails/logo-benvenuto.png')}}"></img>
<br>
<br>
<p>{{Lang::get('email.contest_edit_draft_sentence_1')}} {{$user->name}}</p>
<p>{{Lang::get('email.contest_edit_draft_sentence_2')}} "{{$contest->name}}" {{ Lang::get('email.contest_edit_draft_sentence_3') }} {{ $contest->status == 3 ? Lang::get('email.contest_edit_draft_status_scheduled') : Lang::get('email.contest_edit_draft_status_draft') }}. {{ Lang::get('email.contest_edit_draft_sentence_4') }}</p>
<br>
<p>{{ Lang::get('email.contest_edit_draft_sentence_5') }}</p>
<br>
<p>BuzzMyBrand</p>

<?php $url = asset('/website_public/emails/sep-line.png'); ?>

<img width="100%" height="13px" style="repeat-x" src="{{$url}}"></img>
<img src="{{asset('/website_public/emails/firm.png')}}"></img>