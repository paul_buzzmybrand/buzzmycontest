<p>{{Lang::get("email.contest_standard_approved_sentence")}} {{DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time)->format('d/m/Y H:i:s')}}</p>

<a href="{{asset('/dashboard/index.html')}}">http://www.buzzmybrand.it/dashboard/index.html</a>