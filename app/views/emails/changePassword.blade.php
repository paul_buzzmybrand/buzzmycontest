<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Password change</h2>
 
        <div>This is your new password: {{ $password }} </div>
        <div> Please remember to change it after your next login. </br>
            The JMT team.</div>
        
    </body>
</html>