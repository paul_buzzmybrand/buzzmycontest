<img src="{{asset('/website_public/emails/logo-benvenuto.png')}}"></img>
<br>
<br>
<p><b>{{Lang::get('email.admin_notify_email_1')}} {{ $user->name }} {{ $user->surname }} {{Lang::get('email.admin_notify_email_2')}}.</b></p>
 
<p>{{Lang::get('email.admin_notify_email_3')}}: {{ $user->email }}</p>
<p>{{Lang::get('email.admin_notify_email_4')}}: {{ $company->name }}</p>
<p>{{Lang::get('email.admin_notify_email_5')}}: {{ $user->job_title }}</p>
<p>{{Lang::get('email.admin_notify_email_6')}}: {{ $user->phone }}</p>
<p>{{Lang::get('email.admin_notify_email_7')}}: {{ $user->country }}</p>

<br>
<br>

<?php $url = asset('/website_public/emails/sep-line.png'); ?>

<img width="100%" height="13px" style="repeat-x" src="{{$url}}"></img>
<img src="{{asset('/website_public/emails/firm.png')}}"></img>

