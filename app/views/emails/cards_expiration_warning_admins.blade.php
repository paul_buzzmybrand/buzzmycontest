Le seguenti carte sono in scadenza:

<table>
    <tr>
        <td>Holder</td>
        <td>Last 4</td>
        <td>Scadenza</td>
        <td>Email</td>
    </tr>
    @foreach($cards as $card)
    <tr>
        <td>{{ $card->card_holder }}</td>
        <td>{{ $card->last_four }}</td>
        <td>{{ $card->expire_month }}/{{ $card->expire_year }}
        <td>{{ $card->user->email }}</td>
    </tr>
    @endforeach
</tr>