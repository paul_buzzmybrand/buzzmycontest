<p>{{Lang::get('email.contest_end_sentence_1')}} {{$name}} {{Lang::get('email.contest_end_sentence_2')}} {{$contest_name}} {{Lang::get('email.contest_end_sentence_3')}} </p>

<a href="{{asset('/dashboard/index.html')}}">https://www.buzzmybrand.com/dashboard/index.html</a>
<br>
<p>>{{Lang::get('email.contest_end_sentence_4')}}</p>
<br>
<p>BuzzMyBrand Team</p>