<h1 style="color: #000000; font-size: 26px; max-width: 640px;"><b>{{$contest->name}}</b></h1>
<br>

<span style="color: #000000; max-width: 640px;"><b>{{ trans('tab.brief') }}</b></span><br>
<span style="color: #707173; max-width: 640px;">{{ $contest->concept}}</span><br>
<br>

<span style="color: #000000; max-width: 640px;"><b>{{ trans('tab.date') }}</b></span><br>
<?php $contest_type_desc = '' ?>
<?php if ($contest->type_id =='1') {$contest_type_desc = 'video';} else {$contest_type_desc = 'photo';} ?>
<?php $period_desc = Lang::get('tab.submission_period', array('entry' => $contest_type_desc)); ?>
<span style="color: #707173; max-width: 640px;">{{ trans('tab.from') }} {{tab_HomeController::format_contest_date($contest->location_id, $contest->start_time)}} {{ trans('tab.to') }} {{tab_HomeController::format_contest_date($contest->location_id, $contest->end_time) }} {{explode('-', $contest->end_time)[0]}}</span><br>
<br>

<span style="color: #000000; max-width: 640px;"><b>{{ trans('tab.prizes') }}</b></span><br>
<?php $index = 0; ?>
@foreach ($contest->rewards as $reward)
	<?php $index = $index+1; $final = '';
	if ( $contest->location_id == 1 ) {
		if ( $index == 1 ) {
			$final = 'st';
		} else if ( $index == 2 ) {	
			$final = 'nd';
		} else if ( $index == 3 ) {	
			$final = 'rd';
		} else {	
			$final = 'th';
		} 
	} else if ( $contest->location_id == 2 )  {
		$final = 'o';
	}?>
	<span style="color: #707173; max-width: 640px;">{{$index}}<span style="vertical-align:super; font-size:85%;">{{$final}}</span> {{ trans('tab.prize') }}: {{$reward->title}}, {{$reward->description}}</span><br>
@endforeach
<br>

@if( $contest->how_to_win != '' || $contest->how_to_win != null)
	<span style="color: #000000; max-width: 640px;"><b>{{ trans('tab.how_to_win') }}</b></span><br>
	<div style="color: #707173; max-width: 640px;">{{$contest->how_to_win}}</div>				
@endif
<br>

<h1 style="color: #000000; font-size: 26px; max-width: 640px;"><b>{{Lang::get('messages.video_approved')}}</b></h1>
<br>

<video style="width: 640; height: 360;" poster="{{asset('/images/videos/'. $video->image)}}" controls="controls">
	<source src="{{asset('/videos/'. $video->filename)}}" type="video/mp4">
	<a href="{{asset('/videos/'. $video->filename)}}" ><img height="360" src="{{asset('/images/videos/'. $video->image)}}" width="640" /></a>
</video>





<div style="color: #707173; max-width: 640px;">{{$video->name}}</div>
<br>

<a href="{{asset('/tab/' . $contest->id)}}" style="color: #000000; font-size: 26px; font-weight: 900; max-width: 640px;">SHOW CONTEST PAGE</a>
<br>

