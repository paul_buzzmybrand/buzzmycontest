<p><b>{{Lang::get('email.admin_notify_email_budget_1')}}</b></p>

<p>Company Name: {{ $company->name }}</p>
<p>Company Person: {{ $user->name }} {{ $user->surname }}</p>
<p>Company Mail: {{ $user->email }}</p>
<p>Company Mobile Phone: {{ $user->phone }}</p>
<br>
<br>