<p><b>{{Lang::get('email.approve_content_1')}} {{ $user->name }},</b></p>
 
<p>{{Lang::get('email.approve_content_2')}} {{ $entries_to_approve }} {{Lang::get('email.approve_content_7')}} {{ $contest->name }}</p>

<p>{{Lang::get('email.approve_content_3')}} <a href="{{ asset('/dashboard/index.html#/contests/'.$contest->id.'/approve') }}">{{Lang::get('email.approve_content_4')}}</a> {{Lang::get('email.approve_content_5')}}</p>

<p>{{Lang::get('email.approve_content_6')}}</p>
<br>
<p>BuzzMyBrand Team</p>