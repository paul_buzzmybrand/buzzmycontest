<p>Hi BuzzMyBrand,</p>
<p>You have just received a message from website www.buzzmybrand.it</p>
<p>DETAILS</p>
<p>Name: {{$name}}</p>
<p>Email: {{$email}}</p>
<p>Company: {{$company}}</p>
<p>Telephone: {{$telephone}}</p>
<p>Message: {{$text}}</p>