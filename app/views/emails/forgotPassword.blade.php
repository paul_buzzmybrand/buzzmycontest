<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Forgot Password</h2>
 
        <div>
            <p>If you didn't click on forgot password then don't consider this e-mail and your password will remain un-changed.
            </p>
            <p>If you forgot your Password then you click on the link to Change Password:</p>
            
            <p>
                <a class="btn btn-primary btn-block" href="http://www.joinmethere.tv/jmtvapi/v1/resetPassword?email={{ $email }}&token={{ $token }}" >Change Password</a>
        </div>
        
    </body>
</html>