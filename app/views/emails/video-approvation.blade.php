<?php if ( $video->approval_step_id  == 3 ) { ?>
<h1 style="color: #000000; font-size: 26px; width: 100%; text-align: center;"><b>{{Lang::get('messages.video_approved')}}</b></h1>
<br>
<?php } else if ( $video->approval_step_id  == 1 || $video->approval_step_id  == 2 ) { ?>
<h1 style="color: #000000; font-size: 26px; width: 100%; text-align: center;"><b>{{Lang::get('messages.video_not_approved')}}</b></h1>
<br>
<?php } ?>

<div style="width: 384px; height: 288px; margin: 0 auto;">
<video style="width: 384px; height: 288px;" poster="{{asset('https://www.buzzmybrand.com/images/videos/'. $video->image)}}" controls="controls">
	<source src="{{asset('https://www.buzzmybrand.com/videos/'. $video->filename)}}" type="video/mp4">
	<a href="{{asset('https://www.buzzmybrand.com/videos/'. $video->filename)}}" ><img height="288px" src="{{asset('https://www.buzzmybrand.com/images/videos/'. $video->image)}}" width="384px" /></a>
</video>
</div>




<div style="font-size: 14px; color: #8d8d8d; width: 384px; height: 50px; margin: 0 auto; text-align: center; line-height: 50px"><b>{{$video->name}}</b></div>

<br>

<?php if ( $video->approval_step_id  == 3 ) { ?>

	<?php if ( $video->fbPages->count() > 0 ) { ?>
	<div style="width: 384px; height: 37px; margin: 0 auto; text-align: center;">
	<a href="https://www.facebook.com/video.php?v={{$video->fbPages->first()->pivot->fb_idpost}}" style="color: #000000; font-size: 26px; font-weight: 900; max-width: 640px;">FACEBOOK LINK</a>
	</div>
	<br>
	<?php } ?>
	
	<?php if ( $video->twPages->count() > 0 ) { ?>
	<?php $tw_idvideo =  $video->twPages->first()->pivot->tw_idvideo;
		$screen_name = $video->twPages->first()['tw_screen_name']; ?>
	<div style="width: 384px; height: 37px; margin: 0 auto;  text-align: center;">
	<a href="https://twitter.com/{{$screen_name}}/status/{{$tw_idvideo}}" style="color: #000000; font-size: 26px; font-weight: 900; max-width: 640px;">TWITTER LINK</a>
	</div>
	<br>
	<?php } ?>	
	
	<?php if ( $video->ytPages->count() > 0 ) { ?>
	<div style="width: 384px; height: 37px; margin: 0 auto; text-align: center;">
	<a href="https://www.youtube.com/watch?v={{$video->ytPages->first()->pivot->yt_idvideo}}" style="color: #000000; font-size: 26px; font-weight: 900; max-width: 640px;">YOUTUBE LINK</a>
	</div>
	<br>
	<?php } ?>
		
		
<?php } ?>

<?php
$contestRoute = "https://www.buzzmybrand.com/minisite/" . $contest->id;
if ( $contest->contest_route ) {
	$contestRoute = "http://bmb.buzzmybrand.com/" . $contest->contest_route;
}

?>

<div style="width: 100%; height: 52px;">
	<div style="clear: both;"></div>
	
	<div style="float: left; width: auto; height: 52px;"><a href="{{$contestRoute}}" target="_blank">
	<?php if ( $video->approval_step_id == 3 ) { ?>
	<img src="http://www.buzzmybrand.com/website_public/landing/contest-site.png" height="52px" width="100%"></img>
	<?php } else if ( $video->approval_step_id == 1 || $video->approval_step_id == 2 ) { ?>
	<img src="http://www.buzzmybrand.com/website_public/landing/TryAgain-minisite.png" height="52px" width="100%"></img>
	<?php } ?>
	</a></div>
	
	<div style="clear: both;"></div>
</div>


<br>

