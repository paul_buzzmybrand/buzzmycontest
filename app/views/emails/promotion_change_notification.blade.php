<p>{{Lang::get('email.promotion_change_notification.sentence')}} {{$contest->name}}</p>
<p>{{Lang::get('email.promotion_change_notification.type')}}: </p>
<?php if ( $contest->influencers_promotion ) { ?>
<p>Influencers</p>
<?php } ?>
<?php if ( $contest->display_promotion ) { ?>
<p>Display</p>
<?php } ?>
<?php if ( $contest->video_seeding_promotion ) { ?>
<p>Video Seeding</p>
<?php } ?>
<p>Budget: {{$contest->promotion_budget;}}</p>
