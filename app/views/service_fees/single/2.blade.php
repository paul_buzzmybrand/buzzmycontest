<div class="offer">
    <div class="offer-name">{{ $fee->name }}</div>
    <div class="offer-price">
    	<sup class="price-currency">&euro;</sup> 
    	<span class="price-value">{{ round($fee->fixed_fee) }}</span>
    	<!--<span class="price-specification">/ CONTEST</span>-->
    </div>
    <div class="offer-description">
	    <p class="offer-line">@lang('service_fee.performance_based_price', ['before' => '<strong>', 'after' => '</strong>'])</p>
	    <p class="offer-line"><strong>{{ $fee->varying_fee }} &euro;</strong> / CPM</p>

	    <a class="btn btn-default blue offer-launch" ng-click="selectServiceFee({{ $fee->id}})">@lang('service_fee.select_me')</a>
	</div>
	<div class="coupon-container" ng-show="card.id">
		<ng-form id="coupon-form" ng-submit="StripeCouponSubmit()" novalidate>
			<div class="clearfix"></div>
			<div id="payment-form-ccc" class="payment-input" style="display: flex; align-items: center;">
				<div class="form-group col-md-3 col-xs-12 card-coupon-container">
					<label for="card-coupon">{{ 'COUPON' | translate }}</label>
					<input class="card-coupon form-control" type="text" size="5" value="" id="coupon" name="coupon" />
				</div>
				<div class="col-md-3 col-xs-12 coupon-submit text-center" >
					<button class="submitcoupon-button btn btn-dashboard btn-blue center-block" type="button" ng-click="StripeCouponSubmit()">{{ 'ADD_COUPON' | translate }}</button>
				</div>
			</div>
		</ng-form>
	</div>
</div>