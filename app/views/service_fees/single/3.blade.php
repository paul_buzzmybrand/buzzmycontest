<div class="offer">
    <div class="offer-name">{{ $fee->name }}</div>
    <div class="offer-price">
        <sup class="price-currency">&euro;</sup> 
        <span class="price-value">{{ round($fee->fixed_fee) }}</span>
        <!--<span class="price-specification">/ CONTEST</span>-->
    </div>
    <div class="offer-description">
        <p class="offer-line">@lang('service_fee.performance_based_price', ['before' => '<strong>', 'after' => '</strong>'])</p>
        <p class="offer-line"><strong>{{ $fee->varying_fee }} &euro;</strong> / CPM</p>

        <a class="btn btn-default blue offer-launch" ng-click="selectServiceFee({{ $fee->id }})">@lang('service_fee.select_me')</a>
    </div>
</div>