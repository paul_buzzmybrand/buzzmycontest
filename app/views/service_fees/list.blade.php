<p class="text-info" ng-if="!contest.selectedServiceFee">@lang('service_fee.error_choose_fee')</p>
@foreach($fees->get() as $fee)
<div class="col-md-6 col-sm-6 col-xs-12" ng-class="{selected: contest.selectedServiceFee == {{ $fee->id }} }">
@include('service_fees.single.' . $fee->id)
</div>
@endforeach