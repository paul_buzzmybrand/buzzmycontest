@extends('layouts.public')

@section('metadata')
	<title>@lang('title.homepage')</title>
	<meta name="description" content="{{trans('title.homepage_d')}}">
	<meta property="og:title" content="{{trans('title.homepage')}}"/>
	<meta property="og:description" content="{{trans('title.homepage_d')}}" />
	<meta property="og:url" content="https://www.buzzmybrand.co/" />
	<meta property="og:image" content="https://www.buzzmybrand.co/images/companys/JMTVIDEO-web-Logo.png" />
@stop

@section('content')
    <div id="landing-head" class="container">	
        <h1 class="centered">@lang('landing.head_cta', ['before' => '<span class="yellow">', 'after' => '</span>'])</h1>
        <h5 class="centered">@lang('landing.head_sub')</h5>
        <div class="btn-choice centered">
            
				
			<a class="btn first info-open" href="#" id="btn-sign-up">
				<span class="title" class="info-open">@lang('landing.launch_contest')</span><br />						
			</a><br />
			<a class="btn first contact-open" href="#" id="btn-getintouch">
				<span class="title" class="contact-open">@lang('landing.get_in_touch')</span><br />
			</a>
				
            
            {{-- 
            <a class="btn first" href="#">
                <span class="title">@lang('landing.instant_contest')</span><br />
                <span class="description">@lang('landing.48_hours')</span>
            </a>
            <a class="btn second" href="#">
                <span class="title">@lang('landing.standard_contest')</span><br />
                <span class="description">@lang('landing.1_day_up')</span>
            </a>
            --}}
        </div>
    </div>

    <div id="sign-up" class="landing-block" ng-controller="SignupController">
        <div class="container">
            <h1 class="heading centered">@lang('landing.form_signup_title', ['before' => '<span class="blue fat">', 'after' => '</span>'])</h1>
            <div class="horz-spacer"></div>
            <form id="form-info" name="signupForm" class="signupForm" method="POST" action="/admin/new-company" ng-submit="signupForm.$valid && submit(user)" novalidate>
                <div class="row">
                    <div class="col-md-offset-1 col-md-5">
                        <label for="company">@lang('landing.form_company')</label>
                        <input type="text" name="company" class="fld" ng-model="user.company_name" required />
                    </div>
                    <div class="col-md-5">
                        <div class="signup-name-half signup-name">
                            <label for="name">@lang('landing.form_name')</label>
                            <input type="text" name="name" class="fld" ng-model="user.user_name" required />
                        </div>
                        <div class="signup-name-half signup-surname">
                            <label for="surname">@lang('landing.form_surname')</label>
                            <input type="text" name="surname" class="fld" ng-model="user.user_surname" required />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-1 col-md-5">
                        <label for="job_title">@lang('landing.form_job_title')</label>
                        <input type="text" name="job_title" class="fld" ng-model="user.user_job_title" required />
                    </div>
                    <div class="col-md-5">
                        <label for="company">@lang('landing.form_country')</label>
                        <select name="country" class="fld styled-select" ng-model="user.user_country" required>
                            @foreach(Countries::getList(App::getLocale(), 'php', 'cldr') as $code => $country)
                            <option value="{{ $code }}">{{ $country }}</option>
                            @endforeach
                        </select>
                        <!-- input type="text" name="country" class="fld" ng-model="user.user_country" required / -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-1 col-md-5">
                        <label for="email">@lang('landing.form_email')</label>
                        <input type="email" name="email" class="fld" ng-model="user.user_email" ng-model-options="{debounce: 500}" available-email required />
                        <div id="email-feedback" ng-click="user.user_email = null"></div>
                        <div id="email-error" ng-if="signupForm.email.$error.availableEmail">{{ Lang::get('landing.email_not_available') }}</div>
                    </div>
                    <div class="col-md-5">
                        <label for="phone">@lang('landing.form_tel')</label>
                        <input type="text" name="phone" class="fld" ng-model="user.user_phone" minlength="6" placeholder="{{ Lang::get('landing.form_tel_placeholder') }}" required  ng-pattern="'\\+[\\d\\s\\-]+'" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-1 col-md-5">
                        <label for="password">@lang('landing.form_password')</label>
                        <input type="password" name="password" class="fld" ng-model="user.user_password" minlength="6" required />
                    </div>
                    <div class="col-md-5">
                        <label for="password_repeat">@lang('landing.form_password_repeat')</label>
                        <input type="password" name="password_repeat" class="fld" ng-model="user.user_password_repeat" minlength="6" pw-check="user.user_password" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-1 col-md-10">
                        <div id="form-info-result" ng-class="{'alert-danger': success === false, 'alert-success': success === true}">
                            <span ng-show="exception"><i ng-show="exception">@{{ exception }}</i></span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="terms" id="user_terms" class="checkbox-fld" ng-model="user.terms" checked required><label for="user_terms"></label> <a href="admin/get-tec" title="{{ Lang::get('landing.terms_and_conditions') }}" target="_blank">@lang('landing.terms_and_conditions')</a>
                            </label>
                            <label>
                                <input type="checkbox" name="newsletter" id="user_newsletter" class="checkbox-fld" ng-model="user.newsletter" checked><label for="user_newsletter"></label> @lang('landing.signup_newsletter')
                            </label>
                        </div>
						<input type="hidden" name="language" value="{{ 	App::getLocale() }}" />
                        <input type="hidden" name="contest_type" value="" ng-model="user.contest_type" />
                        <button type="button" class="btn" ng-disabled="!signupForm.$valid" ng-click="submit(user)" id="form-info-send">@lang('landing.get_started')</button>
                    </div>
                </div>
            </form>      
        </div>
        <div class="horz-spacer"></div>
    </div>

    <div id="get-in-touch" class="landing-block" ng-controller="ContactController" style="display: none">
        <div class="container">
            <h1 class="heading centered">@lang('landing.form_title', ['before' => '<span class="blue fat">', 'after' => '</span>'])</h1>
            <form id="form-info" name="contactForm" class="contactForm" method="POST" action="/get-in-touch" ng-submit="contactForm.$valid && submit(contact)" novalidate>
                <div class="row">
                    <div class="col-md-offset-1 col-md-5">
                        <input type="text" name="name" class="fld" placeholder="@lang('landing.form_name')" ng-model="contact.name" required />
                        <input type="email" name="email" class="fld" placeholder="@lang('landing.form_email')" ng-model="contact.email" required />
                    </div>
                    <div class="col-md-5">
                        <input type="text" name="company" class="fld" placeholder="@lang('landing.form_company')" ng-model="contact.company" required />
                        <input type="text" name="telephone" class="fld" placeholder="@lang('landing.form_tel_placeholder')" ng-model="contact.telephone" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-1 col-md-10">
                        <textarea name="message" class="fld" placeholder="@lang('landing.form_message')" rows="4" ng-model="contact.message" required></textarea>
                        <div id="form-info-result" ng-class="{'alert-danger': success === false, 'alert-success': success === true}">
                            <span ng-show="success == true">@lang('landing.form_sent')</span>
                            <span ng-show="success == false">@lang('landing.form_not_sent')<i ng-show="exception">: @{{exception}}</i></span>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="newsletter" class="checkbox-fld" ng-model="contact.newsletter" checked> @lang('landing.form_newsletter')
                            </label>
                        </div>
						<input type="hidden" name="language" value="{{ 	App::getLocale() }}" />
                        <button type="button" class="btn" ng-disabled="!contactForm.$valid" ng-click="submit(contact)" id="form-info-send">@lang('landing.form_send')</button>
                    </div>
                </div>
            </form>      
        </div>
        <div class="horz-spacer"></div>
    </div>

    <div id="brands" class="landing-block">
        <div class="container">
            <div id="brands-carousel">
                @foreach(scandir(public_path() . '/website_public/home_page/img/landing/brands/') as $f)
                @if(strpos(strtolower($f), 'png'))
                <div>
                    <img src="{{ asset('website_public/home_page/img/landing/brands/' . $f) }}" class="brand" style="height: 100px;"/>
                </div>
                @endif
                @endforeach
            </div>
        </div>
    </div>
	
		
		
	<a id="launch"></a>
    <div id="contest-tables" class="landing-block">
        <div class="container">
            <div>
                <h1 class="heading centered">@lang('landing.launch_instant', ['before' => '<span class="blue fat">', 'after' => '</span>'])</h1>
                <p class="description centered separate light contests-block">@lang('landing.launch_instant_description', ['before' => '', 'after' => ''])</p>
				
				<div class="col-md-6">
					<div class="features-list">
						<h5 class="feature">@lang('landing.instant_feature_1')</h5>
						<div class="hr"></div>
						<h5 class="feature">@lang('landing.instant_feature_2')</h5>
						<div class="hr bg-red"></div>
						<h5 class="feature">@lang('landing.instant_feature_3')</h5>
						<div class="hr"></div>
						<h5 class="feature">@lang('landing.instant_feature_4')</h5>

						
						
						
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="features-list">
						<h5 class="feature">@lang('landing.standard_feature_1')</h5>
						<div class="hr bg-red"></div>
						<h5 class="feature">@lang('landing.standard_feature_2')</h5>
						<div class="hr"></div>
						<h5 class="feature">@lang('landing.standard_feature_3')</h5>
						<div class="hr bg-red"></div>
						<h5 class="feature">@lang('landing.standard_feature_4')</h5>

						
					</div>
				</div>
				<!--
				<div class="features-list">
				
					<?php if (Config::get('app.locale') == "it") { ?>
						<a href="#" class="btn blue contact-open">@lang('landing.get_in_touch')</a>
					<?php } else { ?>
						<a href="#" class="btn blue info-open">@lang('landing.launch_contest')</a>
					<?php }; ?>
				</div>
				-->
				<div class="col-md-6">
					<div class="features-list">
						<a href="#" class="btn blue contact-open">@lang('landing.get_in_touch')</a>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="features-list">
						<a href="#" class="btn blue info-open">@lang('landing.launch_contest')</a>
					</div>
				</div>
			</div>
			
			<!--
            <div>
                <h1 class="heading centered">@lang('landing.launch_standard', ['before' => '<span class="red fat">', 'after' => '</span>'])</h1>
                <p class="description centered separate light contests-block">@lang('landing.launch_standard_description', ['before' => '', 'after' => ''])</p>
			</div>
			-->
			
        </div>
    </div>	
	
	
	
    

    <div id="how-it-works" class="landing-block separated">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <h1 class="heading centered">@lang('landing.how_it_works', ['before' => '<span class="blue fat">', 'after' => '</span>'])</h1>
                    <h2 class="sub-heading centered">@lang('landing.how_it_works_sub', ['before' => '<span class="blue">', 'after' => '</span>'])</h2>
                    <div class="hr short spaced"></div>
                </div>
            </div>
            <div class="row">
                <div id="connect-box" class="col-md-6">
                    <h2 class="heading blue">@lang('landing.connect_title')</h2>
                    <p class="description">@lang('landing.connect_description')</p>
                    <div class="horz-spacer"></div>
                    <div class="icons-row connect">
						<!--<div><img src="{{ asset('website_public/home_page/img/landing/connect-1.png') }}" data-toggle="tooltip" data-placement="bottom" title="@lang('landing.connect_photo')"></div>-->
                        <div><img src="{{ asset('website_public/home_page/img/landing/connect-1.png') }}" ></div>
                        <div><img src="{{ asset('website_public/home_page/img/landing/connect-2.png') }}" ></div>
                        <div><img src="{{ asset('website_public/home_page/img/landing/connect-3.png') }}" ></div>
                        <div><img src="{{ asset('website_public/home_page/img/landing/connect-4.png') }}" ></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="{{ asset('website_public/home_page/img/landing/connect.png') }}" title="Connect" width="100%" />
                </div>
            </div>
        </div>
        <div class="horz-spacer"></div>
    </div>

    <div id="engage" class="landing-block">
        <div class="container">
            <div class="row">
                <div class="col-md-6 centered">
                    <img src="{{ asset('website_public/home_page/img/landing/engage.png') }}" title="Connect" width="90%" />
                </div>
                <div class="col-md-6">
                    <h2 class="heading green">@lang('landing.engage_title')</h2>
                    <p class="description">@lang('landing.engage_description')</p>
                    <div class="horz-spacer"></div>
                    <div class="icons-row">
                        <div><img src="{{ asset('website_public/home_page/img/landing/engage-1.png') }}" data-toggle="tooltip" data-placement="bottom" data-template='<div class="tooltip" role="tooltip"><div class="tooltip-arrow green"></div><div class="tooltip-inner green"></div></div>' data-html="true" title="@lang('landing.engage_app', ['br' => '<br />'])"></div>
                        <div><img src="{{ asset('website_public/home_page/img/landing/engage-2.png') }}" data-toggle="tooltip" data-placement="bottom" data-template='<div class="tooltip" role="tooltip"><div class="tooltip-arrow green"></div><div class="tooltip-inner green"></div></div>' title="@lang('landing.engage_facebook')"></div>
                        <div><img src="{{ asset('website_public/home_page/img/landing/engage-3.png') }}" data-toggle="tooltip" data-placement="bottom" data-template='<div class="tooltip" role="tooltip"><div class="tooltip-arrow green"></div><div class="tooltip-inner green"></div></div>' title="@lang('landing.engage_twitter')"></div>
                        <div><img src="{{ asset('website_public/home_page/img/landing/engage-7.png') }}" data-toggle="tooltip" data-placement="bottom" data-template='<div class="tooltip" role="tooltip"><div class="tooltip-arrow green"></div><div class="tooltip-inner green"></div></div>' title="@lang('landing.engage_youtube')"></div>
                        <div><img src="{{ asset('website_public/home_page/img/landing/engage-5.png') }}" data-toggle="tooltip" data-placement="bottom" data-template='<div class="tooltip" role="tooltip"><div class="tooltip-arrow green"></div><div class="tooltip-inner green"></div></div>' title="@lang('landing.engage_website')"></div>
                        <div><img src="{{ asset('website_public/home_page/img/landing/engage-6.png') }}" data-toggle="tooltip" data-placement="bottom" data-template='<div class="tooltip" role="tooltip"><div class="tooltip-arrow green"></div><div class="tooltip-inner green"></div></div>' data-html="true" title="@lang('landing.engage_promotion', ['br' => '<br />'])"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="horz-spacer"></div>
    </div>

    <div id="convert" class="landing-block">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="heading red">@lang('landing.convert_title')</h2>
                    <p class="description">@lang('landing.convert_description')</p>
                    <div class="horz-spacer"></div>
                    <div class="icons-row convert">
                        <div><img src="{{ asset('website_public/home_page/img/landing/convert-1.png') }}" data-toggle="tooltip" data-placement="bottom" data-template='<div class="tooltip" role="tooltip"><div class="tooltip-arrow red"></div><div class="tooltip-inner red"></div></div>' data-html="true" title="@lang('landing.convert_roi', ['br' => '<br />'])"></div>
                        <div><img src="{{ asset('website_public/home_page/img/landing/convert-2.png') }}" data-toggle="tooltip" data-placement="bottom" data-template='<div class="tooltip" role="tooltip"><div class="tooltip-arrow red"></div><div class="tooltip-inner red"></div></div>' data-html="true" title="@lang('landing.convert_analytics', ['br' => '<br />'])"></div>
                        <div><img src="{{ asset('website_public/home_page/img/landing/convert-3.png') }}" data-toggle="tooltip" data-placement="bottom" data-template='<div class="tooltip" role="tooltip"><div class="tooltip-arrow red"></div><div class="tooltip-inner red"></div></div>' data-html="true" title="@lang('landing.convert_channels', ['br' => '<br />'])"></div>
                    </div>
                </div>
                <div class="col-md-6 centered">
                    <div class="horz-spacer"></div>
                    <img src="{{ asset('website_public/home_page/img/landing/convert.png') }}" title="Convert" width="90%" />
                </div>
            </div>
        </div>
        <div class="horz-spacer"></div>
    </div>

    <div id="middle-pic">
        <div id="middle-pic-img" data-parallax='{"y": 200}'></div>
    </div>

    <a id="how-are-we-different"></a>
    <div id="our-plus" class="landing-block">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <h1 class="heading centered">@lang('landing.diff_title', ['before' => '<span class="red fat">', 'after' => '</span>'])</h1>
                    <h2 class="sub-heading centered">@lang('landing.diff_sub', ['before' => '<span class="red">', 'after' => '</span>'])</h2>
                    <div class="horz-spacer"></div>
                </div>
                <div class="col-md-12">
                    <ul class="timeline">
                        <li>
                          <div class="timeline-badge">1</div>
                          <div class="timeline-panel">
                            <div class="timeline-heading">
                              <h1 class="timeline-title">@lang('landing.diff_1_title')</h1>
                            </div>
                            <div class="timeline-body">
                              <p>@lang('landing.diff_1_description')</p>
                            </div>
                          </div>
                        </li>

                        <li class="timeline-inverted">
                          <div class="timeline-badge">2</div>
                          <div class="timeline-panel">
                            <div class="timeline-heading">
                              <h1 class="timeline-title">@lang('landing.diff_2_title')</h1>
                            </div>
                            <div class="timeline-body">
                              <p>@lang('landing.diff_2_description')</p>
                            </div>
                          </div>
                        </li>

                        <li>
                          <div class="timeline-badge">3</div>
                          <div class="timeline-panel">
                            <div class="timeline-heading">
                              <h1 class="timeline-title">@lang('landing.diff_3_title')</h1>
                            </div>
                            <div class="timeline-body">
                              <p>@lang('landing.diff_3_description')</p>
                            </div>
                          </div>
                        </li>

                        <li class="timeline-inverted">
                          <div class="timeline-badge">4</div>
                          <div class="timeline-panel">
                            <div class="timeline-heading">
                              <h1 class="timeline-title">@lang('landing.diff_4_title')</h1>
                            </div>
                            <div class="timeline-body">
                              <p>@lang('landing.diff_4_description')</p>
                            </div>
                          </div>
                        </li>
						<!--
                        <li>
                          <div class="timeline-badge">5</div>
                          <div class="timeline-panel">
                            <div class="timeline-heading">
                              <h1 class="timeline-title">@lang('landing.diff_5_title')</h1>
                            </div>
                            <div class="timeline-body">
                              <p>@lang('landing.diff_5_description')</p>
                            </div>
                          </div>
                        </li>
						-->
                    </ul>
                </div>
            </div>
        </div>
        <div class="horz-spacer"></div>
    </div>

    <div id="launch-now" class="landing-block">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h1 class="heading large">@lang('landing.lcta_title', ['before' => '<span class="yellow fat">', 'after' => '</span>'])</h1>
                    <h3 class="sub-heading">@lang('landing.lcta_sub')</h3>
                </div>
                <div class="col-md-5">
                    <div class="btn-choice centered" style="margin-top: 0;">
						<!--<a class="btn first info-open" href="#">-->
                       
						<a class="btn first info-open" href="#">
							<span class="title" class="info-open">@lang('landing.launch_contest')</span><br />
                                                      
                        </a>
						
                    </div>
                </div>
            </div>
        </div>
    </div>

	
	
		
	
	<div id="pricing" class="landing-block">
        <div class="container">
            <div class="horz-spacer"></div>
            <div class="row">
                <h1 class="heading centered blue"><span class="fat">@lang('landing.pricing_title')</span></h1>
                <h2 class="sub-heading centered">@lang('landing.pricing_sub', ['before' => '<span class="blue">', 'after' => '</span>'])</h2>
            </div>
            <div class="row prices-container">
                <div class="col-md-4">
                    <div class="offer">
                        <h5 class="offer-name">@lang('landing.pricing_1_title')</h5>
                        <h5 class="offer-price"><sup class="price-currency">&euro;</sup> <span class="price-value">499</span><br /> <span class="price-specification">/ CONTEST</span></h5>
                        <p class="offer-line">@lang('landing.pricing_based_on', ['before' => '<strong>', 'after' => '</strong>'])</p>
                        <hr />
                        <p class="offer-line">@lang('landing.pricing_all_included', ['before' => '<strong>', 'after' => '</strong>'])</p>
                        <hr />
                        <p class="offer-line">&nbsp;</p>
                        <p class="offer-line">&nbsp;</p>

                        
						<a href="#" class="btn btn-default blue offer-launch info-open">@lang('landing.pricing_launch_btn')</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="offer">
                        <h5 class="offer-name">@lang('landing.pricing_1_title')</h5>
                        <h5 class="offer-price"><sup class="price-currency">&euro;</sup> <span class="price-value">699</span><br /> <span class="price-specification">/ CONTEST</span></h5>
                        <p class="offer-line">@lang('landing.pricing_based_on', ['before' => '<strong>', 'after' => '</strong>'])</p>
                        <hr />
                        <p class="offer-line">@lang('landing.pricing_all_included', ['before' => '<strong>', 'after' => '</strong>'])</p>
                        <hr />
                        <p class="offer-line">&nbsp;</p>
                        <p class="offer-line">&nbsp;</p>

                        
						<a href="#" class="btn btn-default blue offer-launch info-open">@lang('landing.pricing_launch_btn')</a>
                    </div>
                </div>
				
				<div class="col-md-4">
                    <div class="offer">
                        <h5 class="offer-name">@lang('landing.pricing_2_title')</h5>
                        <div class="offer-price">
                           <img src="{{ asset('website_public/home_page/img/landing/buzzmycontest-logo.png') }}" id="bullhorn" width="110px" />
                        </div>
                        <p class="offer-line">@lang('landing.pricing_tailored', ['before' => '<strong>', 'after' => '</strong>'])</p>
                        <hr />
                        <p class="offer-line">@lang('landing.pricing_onsocial', ['before' => '<strong>', 'after' => '</strong>'])</p>
                        <hr />
                        <p class="offer-line">&nbsp;</p>
                        <p class="offer-line">&nbsp;</p>

                        <a href="#" class="btn btn-default blue offer-launch contact-open">@lang('landing.pricing_1_btn', ['before' => '<strong>', 'after' => '</strong>'])</a>
                    </div>
                </div>


            </div>
        </div>
        <div class="horz-spacer"></div>
    </div>
	
		
	
    



    <div id="newsletter" class="landing-block" ng-controller="NewsletterController">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div id="bullhorn-container">
                        <img src="{{ asset('website_public/home_page/img/landing/bullhorn.png') }}" id="bullhorn" width="70px" />
                    </div>
                    <div>
                        <h1 class="heading large">@lang('landing.newsletter_title', ['before' => '<span class="yellow fat">', 'after' => '</span>'])</h1>
                        <h3 class="sub-heading">@lang('landing.newsletter_sub')</h3>
                    </div>
                </div>
                <form id="form-newsletter" name="newsletterForm" method="POST" action="/subscribe" ng-submit="newsletterForm.$valid && submit(newsletter)" novalidate>
                    <div class="col-md-4 centered">
                        <input type="email" name="email" placeholder="@lang('landing.newsletter_placeholder')" required id="newsletter-email" ng-model="newsletter.email" />
                        <div ng-show="success !== null" ng-class="{'alert-success': success === true, 'alert-danger': success === false}">
                            <strong ng-show="success === false">@lang('landing.form_not_sent')<i ng-show="exception">: @{{exception}}</i></strong>
                            <strong ng-show="success === true">@lang('landing.form_sent')</strong>
                        </div>
                    </div>
                    <div class="col-md-2 centered">
                        <button type="button" class="btn" ng-disabled="!newsletterForm.$valid" ng-click="submit(newsletter)">@lang('landing.newsletter_btn')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop