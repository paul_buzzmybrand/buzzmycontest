

@if( $videos->count() > 0 )

<?php $counter = 0 ?>
<?php foreach($videos as $video): ?>

<?php if ($counter % $cols == 0) : ?>
<div class="row">
<?php endif ?>
	

	<div class="thumbnail col-xs-12 col-sm-6 col-md-6 col-lg-4">


	<!--<a href="{{action('Admin_ContestController@video', $video->id)}}" style='color:#555;'><img class="img-thumbnail" src="{{asset('/images/videos/'.$video->image)}}" width="width" height="250"/></a>-->

	<div id='mediaplayertoapp{{$video->id}}' class="video">
	<a href="javascript:void(0)" onClick="start_jw_player('1', '{{$video->id}}', '{{$video->filename}}', '{{$video->image}}');" style='color:#555;'><img class="img-thumbnail" src="{{asset('/images/videos/'.$video->image)}}" width="width" height="250"/></a>
	</div>

	<div class="caption">
	<h4><b>{{utf8_decode($video->name)}}</b></h4>

	<p style="text-align: right;">{{ trans('admin.user') }}: <a style="color: black;" href="https://www.facebook.com/{{$video->user->facebookProfiles->first()['id_fb']}}"><b>{{ $video->user->facebookProfiles->first()['name'] }}</b></a></p>

	<!-- START Bottoni per approvare, senza approvazione, non approvare -->
	{{ Form::open(array('action' => array('Admin_ContestController@approveVideo', $video->id), 'id' => 'approveForm')) }}
	<input type="hidden" name="approvalStep" value="3"/> 
	<input type="submit" class="btn btn-primary {{ $video->approvalStep->id == 3 ? 'active' : ''}}" value="{{ trans('admin.buttons.Approved') }}">
	{{ Form::close() }}

	{{ Form::open(array('action' => array('Admin_ContestController@approveVideo', $video->id), 'id' => 'approveForm')) }}
	<input type="hidden" name="approvalStep" value="1"/> 
	<input type="submit" class="btn btn-primary {{ $video->approvalStep->id == 1 ? 'active' : ''}}" value="{{ trans('admin.buttons.No_approval') }}">
	{{ Form::close() }}

	{{ Form::open(array('action' => array('Admin_ContestController@approveVideo', $video->id), 'id' => 'approveForm')) }}
	<input type="hidden" name="approvalStep" value="2"/> 
	<input type="submit" class="btn btn-primary {{ $video->approvalStep->id == 2 ? 'active' : ''}}" value="{{ trans('admin.buttons.Not_approved') }}">
	{{ Form::close() }}
	<!-- END Bottoni per approvare, senza approvazione, non approvare -->


	<!-- START Box per mandare le notifiche -->
	<button id="send_notificationt1c{{$video->id}}" onClick="showBoxNotification('1', '{{$video->id}}')" type="button" class="btn btn-success">{{ trans('admin.notifications.Send Notifications') }}</button>
	<div id="box_notificationst1c{{$video->id}}" class="form-group" style="display: none" >
	{{ Form::open(array('action' => array('Admin_ContestController@sendNotification', $video->id), 'id' => 'notificationForm')) }}
	<label for="comment">{{ trans('admin.notifications.Notifications') }}:</label>
	<textarea class="form-control" rows="5" id="comment" name="message" ></textarea>
	<input type="submit" class="btn btn-primary" onClick="closeBoxNotification('1', '{{$video->id}}')" value="{{ trans('admin.notifications.Send') }}" >
	<button type="button" class="btn btn-primary" onClick="closeBoxNotification('1', '{{$video->id}}')">{{ trans('admin.notifications.Cancel') }}</button>
	{{ Form::close() }}
	</div>
	<!-- END Box per mandare le notifiche -->
	
	<!-- START Bottone per eliminare -->
	{{ Form::open(array('action' => array('Admin_ContestController@deleteVideo', $video->id), 'id' => 'deleteForm')) }}
	<input type="submit" class="btn btn-danger" value="{{ trans('admin.buttons.Delete') }}">
	{{ Form::close() }}
	<!-- END Bottone per eliminare -->
	

	</div>
	</div>
	

<?php if ($counter % $cols == $cols - 1) : ?>	
</div>
<?php endif ?>

<?php $counter++; ?>
<?php endforeach; ?>

@else

<?php if ($page == 1) : ?>
<div class="alert alert-info">{{ trans('admin.No videos for this option') }}</div>
<?php endif ?>

<script>
endToApproveEntries = true;
</script>

@endif
