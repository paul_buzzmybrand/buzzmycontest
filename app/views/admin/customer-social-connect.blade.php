<!DOCTYPE html>
<html class="show">
  <head>
    <meta charset="utf-8">
    <title>Buzzmybrand</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="/dashboard/app/index.css">
  </head>
<body>
    Connecting...
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
$(function(response) {
    var scope = window.opener.$('#contest-edit-ctrl').scope()
    if({{ $result }} == 1) {
        scope.socialConnectResult('{{ $social }}', true, "{{ $page }}", "{{ $image }}");
    } else {
        scope.socialConnectResult('{{ $social }}', false, "{{ $message }}", "");
    }
    window.close();
});
</script>
</html>