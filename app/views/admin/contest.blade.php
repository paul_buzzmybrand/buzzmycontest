@extends('layouts.admin')
@section('content')

 <ol class="breadcrumb" style="text-align: left">
 	<li><a href="{{ $username == 'admin' ? action('Admin_ContestController@externalAdminIndex') : action('Admin_ContestController@externalIndex', $contest->company->id)}}">{{ $contest->company->name }}</a></li>
	<li class="active">{{ $contest->name }}</li>
</ol>
@include('website.flash')

<div class="row">
	<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12' style="text-align: left">
		<button id="send_mass_notification" onClick="showMassNotificationBox()" type="button" class="btn btn-success">{{ trans('admin.notifications.Send Notifications') }}</button>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: left">
		<div id="mass_notification_box" class="form-group" style="display: none; background-color:#f5f5f5; padding: 15px" >
		
		{{ Form::open(array('action' => array('Admin_ContestController@sendMassNotification', $contest->id), 'id' => 'massNotificationForm')) }}
		<input type="checkbox" name="mass_type_notification_logged" value="notify">{{ trans('admin.mass_notification.Notify Logged') }}<br>
		<input type="checkbox" name="mass_type_notification_applyer" value="notify">{{ trans('admin.mass_notification.Notify Applyer') }}
		<?php if ( $contest->type_id == 1 ) { ?>
		<input type="hidden" name="contestType" value="1"/>
		<?php } else if ( $contest->type_id == 2 ) { ?>
		<input type="hidden" name="contestType" value="2"/>
		<?php } ?>
		<textarea class="form-control" rows="2" id="comment" name="message" ></textarea>
		<input type="submit" class="btn btn-primary" onClick="closeMassNotificationBox()" value="{{ trans('admin.notifications.Send') }}" >
		<button type="button" class="btn btn-primary" onClick="closeMassNotificationBox()">{{ trans('admin.notifications.Cancel') }}</button>
		{{ Form::close() }}
		</div>
	</div>
</div>	


<div class="row" style="height: 20px;">
</div>

<div class="row">
	<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>
		<p><a href="{{action('Admin_ContestController@contest', $contest->id)}}" style='color:#555;'><img height="333" width="250" src="{{asset('/images/contest/'.$contest->image)}}"/></a></p>
	</div>
	<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>
		
		<div class="well" style="text-align: left; min-height: 350px;">
		
		<b>{{$contest->name}}</b><br>
		
		<!--<div id="contentScroll" style="color:black;height:250px;padding-bottom:10px;">-->
		
		@if( $contest->concept != '' && $contest->concept != null)
		<!--<p class="p_contest" style="text-align:justify; width: 96%;padding-top: 5px;">-->
		
		<b>{{ trans('tab.brief') }}</b><br>
		{{ $contest->concept}}<br>
		<br><b>{{ trans('tab.date') }}</b><br>
		<?php $contest_type_desc = '' ?>
		<?php if ($contest->type_id =='1') {$contest_type_desc = 'video';} else {$contest_type_desc = 'photo';} ?>
		<?php $period_desc = Lang::get('tab.submission_period', array('entry' => $contest_type_desc)); ?>
		{{ $period_desc }}: {{ trans('tab.from') }} {{tab_HomeController::format_contest_date($contest->location_id, $contest->start_time)}} {{ trans('tab.to') }} {{tab_HomeController::format_contest_date($contest->location_id, $contest->end_time) }}<br>
		
		<br><b>{{ trans('tab.prizes') }}</b><br>
		<?php $index = 0; ?>
		@foreach ($contest->rewards as $reward)
			<?php $index = $index+1; ?>
			<b>{{$index}}° {{ trans('tab.prize') }}</b>: {{$reward->title}}, {{$reward->description}}<br>
		@endforeach
		
		@if( $contest->how_to_win != '' || $contest->how_to_win != null)
			<br><b>{{ trans('tab.how_to_win') }}</b><br>
			{{$contest->how_to_win}}			
			<br>
		@endif
		
		<b>{{ trans('tab.fb_pages') }}</b><br>
		<?php foreach($contest->fbPages as $page): ?>
			@if ($page->fb_pagelike_value != 0)
			- {{$page->fb_pagelike_value}} {{ trans('tab.points_if_pagelike') }} "<a href="{{ $page->fb_page_link}}">{{ $page->fb_page_name}}</a>"<br>
			@endif				
		<?php endforeach; ?>

		<!--</p>-->
		@endif	
		<!--</div>-->
		
		</div>
		

		
		
    </div> 
	<div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>
		
		<div class="well" style="text-align: left; min-height: 350px; background-color: #fff">
		
			<?php if ( $contest->type_id == 1 ) { ?>
			<b>{{ trans('admin.statistics.Total Entries') }}: </b>{{count($contest->videosForApproval)}}<br>
			<b>{{ trans('admin.statistics.Approved') }}: </b>{{count($contest->videosAlreadyApproved)}}<br>
			<b>{{ trans('admin.statistics.Not Approved') }}: </b>{{count($contest->videosNotApproved)}}<br>
			<b>{{ trans('admin.statistics.To be Approved') }}: </b>{{count($contest->videosToApprove)}}<br>
			<?php } elseif ( $contest->type_id == 2 ) { ?>
			<b>{{ trans('admin.statistics.Total Entries') }}: </b>{{count($contest->photosForApproval)}}<br>
			<b>{{ trans('admin.statistics.Approved') }}: </b>{{count($contest->photosAlreadyApproved)}}<br>
			<b>{{ trans('admin.statistics.Not Approved') }}: </b>{{count($contest->photosNotApproved)}}<br>
			<b>{{ trans('admin.statistics.To be Approved') }}: </b>{{count($contest->photosToApprove)}}<br>			
			<?php } ?>
			
			<b>{{ trans('admin.statistics.Total Reach') }}: </b>{{$statistics_global['reach']}}<br>
			<b>{{ trans('admin.statistics.Total Impression') }}: </b>{{$statistics_global['impressions']}}<br>
			<b>{{ trans('admin.statistics.Total Views') }}: </b>{{$statistics_global['views']}}<br>
			<b>{{ trans('admin.statistics.Unique Views') }}: </b>{{$statistics_global['unique_views']}}<br>
			<b>{{ trans('admin.statistics.Engaged Users') }}: </b>{{$statistics_global['engaged_users']}}<br>
			<b>{{ trans('admin.statistics.Comments') }}: </b>{{$statistics_global['unique_comments']}}<br>
			<b>{{ trans('admin.statistics.Likes') }}: </b>{{$statistics_global['unique_likes']}}<br>
			<b>{{ trans('admin.statistics.Shares') }}: </b>{{$statistics_global['unique_shares']}}<br>
			<b>{{ trans('admin.statistics.Daily New Likes') }}: </b><?php if ( isset($contest->daily_new_likes) ) {echo $contest->daily_new_likes;} else {echo 0;} ?><br>
			<b>{{ trans('admin.statistics.Page Engaged Users') }}: </b><?php if ( isset($contest->page_engaged_users) ) {echo $contest->page_engaged_users;} else {echo 0;} ?> %<br><br>
			
			<b>{{ trans('admin.logged_from_website') }}: </b>{{$logged_from_website}}<br>
			<b>{{ trans('admin.logged_from_mobile') }}: </b>{{$logged_from_mobile}}<br>
			<b>{{ trans('admin.logged_from_website_and_mobile') }}: </b>{{$logged_from_website_and_mobile}}<br>

		
		</div>
		{{ Form::open(array("action" => array("Admin_ContestController@startServerProcess", $contest->id), "id" => "startServerProcess")) }}
			<button onclick="$('#startServerProcess').submit();" class="btn btn-primary">{{ trans('admin.buttons.start_server_process') }}</button>
		{{ Form::close() }}
		
		<!--
		<div class="panel panel-default">
		  <div class="panel-heading">
			<h4 class="panel-title">{{trans('admin.actions')}}</h4>
		  </div>
		  <div class="panel-body">
			<div class="row" >
			@if ($username == 'admin')
				{{ Form::open(array("action" => array("Admin_ContestController@startServerProcess", $contest->id), "id" => "startServerProcess")) }}
				<button onclick="$('#startServerProcess').submit();" class="btn btn-primary">{{ trans('admin.buttons.start_server_process') }}</button>
				{{ Form::close() }}			
			@endif
			</div>
			<div class="row" >
			@if ($contest->mayFinishApproval)
				{{ Form::open(array("action" => array("Admin_ContestController@finishApproval", $contest->id), "id" => "finishApprovalForm")) }}
				<button onclick="$('#finishApprovalForm').submit();" class="btn btn-primary">{{ trans('admin.buttons.Finish approval') }}</button>
				{{ Form::close() }}
			@else
				<button disabled class="btn btn-primary">{{ trans('admin.buttons.Finish approval') }}</button>
			@endif
			</div>
			<div class="row">
			@if ($contest->mayResetApproval)
				{{ Form::open(array("action" => array("Admin_ContestController@resetApproval", $contest->id), "id" => "resetApprovalForm")) }}
				<button onclick="$('#resetApprovalForm').submit();" class="btn btn-primary">{{ trans('admin.buttons.Reset approval') }}</button>
				{{ Form::close() }}
			@else
				<button disabled class="btn btn-primary">{{ trans('admin.buttons.Reset approval') }}</button>
			@endif
			</div>
			<div class="row">
			@if (($contest->mayUploadYouTube) && ($contest->type_id == '1'))
				{{ Form::open(array("action" => array("Admin_ContestController@upload2YouTube", $contest->id), "id" => "upload2YouTubeForm")) }}
				<button onclick="$('#upload2YouTubeForm').submit();" class="btn btn-primary">{{ trans('admin.buttons.Upload YouTube') }}</button>
				{{ Form::close() }}
			@else
				<button disabled class="btn btn-primary">{{ trans('admin.buttons.Upload YouTube') }}</button>
			@endif
			</div>
		  </div>
		</div>
		
		<div class="panel panel-default">
		  <div class="panel-heading">
			<h4 class="panel-title">{{trans('admin.need help')}}</h4>
		  </div>
		  <div class="panel-body">
			<a href="mailto:ask@joinmethere.com?subject=Help for the Dashboard"><b>CONTACT US</b></a>
		  </div>
	    </div>
		-->

	</div>
</div>
&nbsp;



<ul class="nav nav-tabs nav-justified" role="tablist">
  <li class="active"><a href="#toapprove" role="tab" data-toggle="tab">TO APPROVE</a></li>
  <li><a href="#approved" role="tab" data-toggle="tab">APPROVED</a></li>
  <li><a href="#notapproved" role="tab" data-toggle="tab">NOT APPROVED</a></li>
  <li><a href="#allentries" role="tab" data-toggle="tab">ALL ENTRIES</a></li>  
</ul>

<div class="tab-content">
	
	<div class="tab-pane" id="allentries">
		<div class="panel panel-default">    
			<div class="panel-body">
			</div>
			<div id="loadingbar-allentries" style="margin-bottom:20px">
				<img alt="loading" style="border:none" src="{{asset('images/loader.gif') }}" />
			</div>
		</div>
	</div>
  
	<div class="tab-pane active" id="toapprove">
		<div class="panel panel-default">    
			<div class="panel-body">
			</div>
			<div id="loadingbar-toapprove" style="margin-bottom:20px">
				<img alt="loading" style="border:none" src="{{asset('images/loader.gif') }}" />
			</div>
		</div>
	</div>
  
	<div class="tab-pane" id="approved">
		<div class="panel panel-default">    
			<div class="panel-body">
			</div>
			<div id="loadingbar-approved" style="margin-bottom:20px">
				<img alt="loading" style="border:none" src="{{asset('images/loader.gif') }}" />
			</div>
		</div>
	</div>
  
	<div class="tab-pane" id="notapproved">
		<div class="panel panel-default">    
			<div class="panel-body">
			</div>
			<div id="loadingbar-notapproved" style="margin-bottom:20px">
				<img alt="loading" style="border:none" src="{{asset('images/loader.gif') }}" />
			</div>
		</div>	
	</div>
	

	
</div>





@stop



