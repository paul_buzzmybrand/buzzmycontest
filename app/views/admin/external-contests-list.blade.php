@extends('layouts.admin')
@section('content')

<div class="row">
	<ol class="breadcrumb" style="text-align: left">
		<li class="active">{{ $company_name }}</li>
	</ol>
</div>
        
<div class="row">
	<?php $count = 0; ?>

	<?php foreach($contests as $contest): ?>
	
		<div class="thumbnail col-xs-12 col-sm-6 col-md-6 col-lg-3">
				<a href="{{action('Admin_ContestController@contest', $contest->id)}}">
					<img height="333" width="250" src="{{asset('/images/contest/'.$contest->image)}}" />
				</a>
			<div class="caption">
				<h4>{{ $contest->name }}</h4>
			</div>
		</div>
		
			
	<?php $count ++; ?>		
	
	  
	<?php endforeach; ?>

</div>
      	

@stop
