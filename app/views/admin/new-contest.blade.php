@extends('layouts.admin')
@section('content')

<form action="{{ URL::to('event/addEvent') }}" method="post" enctype="multipart/form-data" runat="server">
									
	<div class="panel panel-default">

		<div class="panel-heading">
			<h3 class="panel-title">Contest Detail</h3>
		</div>
		
		<div class="panel-body">

			<div class="form-group">
				<label class="col-sm-2 control-label">Name</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name ="name" value="">
			</div>
		
		</div>
																			
							
		<div class="form-group">
			<label class="col-sm-2 control-label">Company Name</label>
			<div class="col-sm-10">
				<select class="form-control" name="company_id">
				  @foreach($companys as $company)														
						<option value="{{ $company -> id }}">{{ $company -> name }}</option>
				  @endforeach
				</select>
			</div>
		</div>
										
										
										<div class="form-group">
											<label class="col-sm-2 control-label">Location</label>
											<div class="col-sm-10">
												<select class="form-control" name="location_id">
												  @foreach($locations as $location)
														
														<option value="{{ $location -> id }}">{{ $location -> name }}</option>
												  @endforeach
												</select>
											</div>
										</div>
										
										
										<div class="form-group">
											<label class="col-sm-2 control-label">Main Hashtag</label>
											 <div class="col-sm-10">
												<input type="text" class="form-control" name ="description" size="30" value="">
											</div>
										</div>
										
										
										<div class="form-group">
										  <label class="col-sm-2 control-label">Secondary HashTags</label>
										  <div class="col-sm-10">
											<input type="text" class="form-control" name ="hashtags" size="30" value="">											
										  </div>
										</div>
										  
										  
											<div class="divaux" id="divaux{{$contest->id}}">
												<div class="form-group">
													<label class="col-sm-2 control-label">Contest Type</label>
													<div class="col-sm-10">
														<select class="form-control" id="contestTypeSelect" name="type_id">
															<option value="1" selected>Video Contest</option>
															<option value="2" >Photo Contest</option>	
															<option value="3">Video Resume</option>																
														</select>
													</div>
												</div>
											</div>
											
											
											<div class="divaux2" id="divaux2{{$contest->id}}">									
												<div class="form-group">
													<label class="col-sm-2 control-label">Video Length</label>
													<div class="col-sm-10">
														<select class="form-control" name="video_length">
															@for ($x = 5; $x <= 15; $x++)
																	
																	<option value="{{$x}}">{{$x}}</option>												
															@endfor
														</select>
													</div>
												  
												</div>
												
												
												
												
											</div>
											<div class="form-group">
											  <label class="col-sm-2 control-label">Concept</label>
											  <div class="col-sm-10">
												<textarea class="form-control" name="concept" rows="5" ></textarea>
											  </div>
											</div>
											<div class="form-group">
											  <label class="col-sm-2 control-label">T&C Link</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name="terms_cond" size="30" value="">
											  </div>
											</div>
											<div class="form-group">
											  <label class="col-sm-2 control-label">Start Time</label>
											  <div class="col-sm-10">
												<input type="datetime" class="form-control" name ="start_time" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">End Time</label>
											  <div class="col-sm-10">
												<input type="datetime" class="form-control" name ="end_time" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Start Start Time</label>
											  <div class="col-sm-10">
												<input type="datetime" class="form-control" name ="score_start_time" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Score End Time</label>
											  <div class="col-sm-10">
												<input type="datetime" class="form-control" name ="score_end_time" size="30" value="">
											  </div>
											</div>
										  </div>
										</div>
										
										
										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Images</h3>
										  </div>
										  <div class="panel-body">
											
											
										
											<div class="form-group">
												<span>
													<div>
														<img style="margin-top: -4px;width:10%; height:10%;" src="" alt="Click here to show the image" id="t100" >
													</div>    
												</span>
												<label class="col-sm-2 control-label">Banner</label>
												<div class="col-sm-10">
													<input onchange="readURL(this,app='t100');" type="file" name='bannerImage' class="form-control" >
												</div>
											</div>
													
											
											<div class="form-group">
												<span>
													<div>
														<img style="margin-top: -4px;width:10%; height:10%;" src="" alt="Click here to show the image" id="t100" >
													</div>    
												</span>
												<label class="col-sm-2 control-label">Watermark</label>
												<div class="col-sm-10">
													<input onchange="readURL(this,app='t100');" type="file" name='watermarkImage' class="form-control" >
												</div>
											</div>
											
											<div class="form-group">
												<span>
													<div>
														<img style="margin-top: -4px;width:10%; height:10%;" src="" alt="Click here to show the image" id="t100" >
													</div>    
												</span>
												<label class="col-sm-2 control-label">TAB Background</label>
												<div class="col-sm-10">
													<input onchange="readURL(this,app='t100');" type="file" name='backgroundImage' class="form-control" >
												</div>
											</div>
											
											<div class="form-group">
												<span>
													<div>
														<img style="margin-top: -4px;width:10%; height:10%;" src="" alt="Click here to show the image" id="t100" >
													</div>    
												</span>
												<label class="col-sm-2 control-label">TAB Banner</label>
												<div class="col-sm-10">
													<input onchange="readURL(this,app='t100');" type="file" name='tabBannerImage' class="form-control" >
												</div>
											</div>
											
											
											
										  </div>
										</div>
										
										

										
										
										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Prizes Detail</h3>
										  </div>
										  <div class="panel-body">
											<div class="form-group">
											  <label class="col-sm-2 control-label">First Prize</label>
											  <div class="col-sm-10">
											  <input type="text" class="form-control" name ="first_reward" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Second Prize</label>
											  <div class="col-sm-10">
											  <input type="text" class="form-control" name ="second_reward" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Third Prize</label>
											  <div class="col-sm-10">
											  <input type="text" class="form-control" name ="third_reward" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Consolation Prizes</label>
											  <div class="col-sm-10">
											  <input type="text" class="form-control" name ="consolation_rewards" size="30" value="">
											  </div>
											</div>
										  </div>
										</div>



										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Facebook Detail</h3>
										  </div>
										  <div class="panel-body">
											<div class="form-group">
											  <label class="col-sm-2 control-label">Page ID</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name ="fb_page_id" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Page Name</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name ="fb_page_name" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Page Link</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name ="fb_page_link" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Album ID</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name ="id_album" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">No Timeline</label>
											  <div class="col-sm-10">
													<select class="form-control" name="no_story_fb">													
														
														<option value="NO">NO</option>
														<option value="YES">YES</option>
													</select>
												</div>										  
											</div>
										  </div>
										</div>


										

										
										
										
										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Functionalities</h3>
										  </div>
										  <div class="panel-body">
											
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Contest Approval</label>
												<div class="col-sm-10">
													<select class="form-control" name="contest_approval">	
														<option value="1">YES</option>
														<option value="0" selected>NO</option>
														
													</select>
												</div>
											</div>
											
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Customer Logo Insertion</label>
											  <div class="col-sm-10">
												<select class="form-control" name="should_insert_contest_watermark">													
													
													<option value="1">YES</option>
													<option value="0" selected>NO</option>
													
												</select>
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">JMT Logo Insertion</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="should_insert_company_watermark">													
													
													<option value="1">YES</option>
													<option value="0" selected>NO</option>
													
												</select>
											  
												
											  </div>
											</div>
											
											<div class="divaux2" id="divaux3{{$contest->id}}">
											<div class="form-group">
											  <label class="col-sm-2 control-label">Upload To YouTube</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="should_upload_to_company_youtube">	
													<option value="1">YES</option>
													<option value="0" selected>NO</option>
													
												</select>
											  										  
											  
												
											  </div>
											</div>
											</div>
											
											
											
											
											
										  </div>
										</div>
										
										
										
										
										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Scoring Detail</h3>
										  </div>
										  <div class="panel-body">
										  
										  
										  
											<div class="form-group">
											  <label class="col-sm-2 control-label">Like Value</label>
											  <div class="col-sm-10">
												
												<select class="form-control" name="like_value">
													@for ($x = 0; $x <= 5; $x++)
															
															<option value="{{$x}}">{{$x}}</option>
																										
													@endfor
												</select>
											  
												
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Comment Value</label>
											  <div class="col-sm-10">
												
												<select class="form-control" name="comment_value">
													@for ($x = 0; $x <= 5; $x++)
															
															<option value="{{$x}}">{{$x}}</option>									
													@endfor
												</select>
											  
												
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Share Value</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="share_value">
													@for ($x = 0; $x <= 5; $x++)
															
															<option value="{{$x}}">{{$x}}</option>													
													@endfor
												</select>
											  
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Click Post Value</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="click_post">
													@for ($x = 0; $x <= 5; $x++)
															
															<option value="{{$x}}">{{$x}}</option>									
													@endfor
												</select>
											  
												
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Page Like Value</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="pagelike_value">
													@for ($x = 0; $x <= 5; $x++)
															
															<option value="{{$x}}">{{$x}}</option>												
													@endfor
												</select>
												
												
												
											  </div>
											</div>
										  
										  
										  
										  
										  
										  </div>
										</div>
										
										
										
										
										
										
										
										
										<hr>
										
										<div class="help-block">
											<button type="submit" class="btn btn-large btn-info">Submit Event</button>
										</div>
					</form>
@stop