@extends('layouts.admin')
@section('content')
        
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" >                        
                        @if(Session::has('ErrorMessage'))
                            <div class="alert-box success">
                                <ol class="alert">
                                   <b>{{ Session::get('ErrorMessage') }}</b>
                                    @foreach($errors->all() as $error)
                                        <li style="margin-left: 20px;">{{ $error }}</li>
                                    @endforeach    
                                </ol>
                            </div>
                        @endif
                        <div class="row-fluid" >     
                            <div class="span13" >
                                <!--a class="btn btn-success" style="float: right; margin-right: -6px;" data-toggle="modal" href="#AddModal">
                                    <span class="icon-ok"></span>
                                        Add new Event & Contest  
                                </a-->
                                
                                
                                <a class="btn btn-default" style="float: right; margin-right: 12px;" href="company">
                                    <span class=" glyphicon glyphicon-arrow-left"></span>
                                        Company
                                </a>
                                <a class="btn btn-success" style=" margin-right: -6px;" data-toggle="modal" href="#AddModal">
                                    <span class="icon-ok"></span>
                                        Add new Contest
                                </a>
                            </div>
                        </div>
                    </div>      
                </div>
            </div> 
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="span6 heading-div">
                        <h3 style="margin-top: 8px;">CONTESTS LIST</h3>
                    </div>
                </div>
            </div>

            <!--List Start-->
            
            
                <div id="contentScroll" class="row col-md-6 col-md-offset-3 list-scroll">
                    <div style="width: 100%;">
                    @foreach($contests as $contest)  

						<div class="panel panel-default">
						  <div class="panel-heading">
							<h3 class="panel-title">{{ $contest -> name }}</h3>
						  </div>
						  <div class="panel-body">
							@if ($contest -> type_id == '1')
							<h4>VIDEO CONTEST</h4>
							@elseif ($contest -> type_id == '2')
							<h4>PHOTO CONTEST</h4>
							@elseif ($contest -> type_id == '3')
							<h4>VIDEO RESUME</h4>
							@endif							
							
							<button id="EditModal{{ $contest -> id }}" type="button" class="btn" data-toggle="modal" data-target=".bs-example-modal-lg#EditModal{{ $contest -> id }}">
								<span class="icon-edit"></span>
								Edit
							</button>
							
							
							
							
							<a  class="btn  btn-danger" data-toggle="modal" href="#confirmationModal">
								<span class="icon-trash"></span>
								Trash  
							</a>
                                
						  </div>
						</div>

						
						<div class="modal fade bs-example-modal-lg" id="EditModal{{ $contest -> id }}" tabindex="-1" 
							role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
								  <div class="modal-header">
									<button class="close" data-dismiss="modal">×</button>
									<h3>Edit Contest</h3>
								  </div>
								  
								  
								  <div class="modal-body">
									  <form action="{{ URL::to('event/editEvent') }}" class="form-horizontal" method="post" enctype="multipart/form-data" runat="server" role="form"> 
										
										
										
							

										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Contest Detail</h3>
										  </div>
										  <div class="panel-body">
										  
										  
										  <div class="form-group">
										  <label class="col-sm-2 control-label">Name</label>
										  <div class="col-sm-10">
										  <input type="text" class="form-control" name ="name" value="{{$contest -> name}}">
										  </div>
										 </div>
																			
										
										<?php
											$companyName = "";
											$id = 0;
												
											foreach($companys as $company)
											{
												if ($contest->company_id == $company->id){
												  $companyName = $company -> name;
												  $id = $company -> id;
												}            
											}
										?>
										
										
										<div class="form-group">
											<label class="col-sm-2 control-label">Company Name</label>
											<div class="col-sm-10">
												<select class="form-control" name="company_id">
												  @foreach($companys as $company)
														@if ($company -> name == $companyName)
															<option value="{{ $company -> id }}" selected>{{ $company -> name }}</option>
														@else
															<option value="{{ $company -> id }}">{{ $company -> name }}</option>
														@endif
														
												  @endforeach
												</select>
											</div>
										</div>
										
										
										<div class="form-group">
											<label class="col-sm-2 control-label">Location</label>
											<div class="col-sm-10">
												<select class="form-control" name="location_id">
												  @foreach($locations as $location)
														@if ($location -> id == $contest->location_id)
															<option value="{{ $location -> id }}" selected>{{ $location -> name }}</option>
														@else
															<option value="{{ $location -> id }}">{{ $location -> name }}</option>
														@endif
														
												  @endforeach
												</select>
											</div>
										</div>
										
										
										<div class="form-group">
											<label class="col-sm-2 control-label">Main Hashtag</label>
											 <div class="col-sm-10">
												<input type="text" class="form-control" name ="description" size="30" value="{{$contest -> description}}">
											</div>
										</div>
										
										
										<div class="form-group">
										  <label class="col-sm-2 control-label">Secondary HashTags</label>
										  <div class="col-sm-10">
											<input type="text" class="form-control" name ="hashtags" size="30" value="{{$contest -> hashtags}}">											
										  </div>
										</div>
										  
										  
										  
										  
											<div class="divaux" id="divaux{{$contest->id}}">
												<div class="form-group">
													<label class="col-sm-2 control-label">Contest Type</label>
													<div class="col-sm-10">
														<select class="form-control" id="contestTypeSelect" name="type_id">
															@if ($contest->type_id == '1')
															<option value="1" selected>Video Contest</option>
															<option value="2">Photo Contest</option>
															<option value="3">Video Resume</option>
															@elseif ($contest->type_id == '2')
															<option value="2" selected>Photo Contest</option>
															<option value="1">Video Contest</option>
															<option value="3">Video Resume</option>
															@elseif ($contest->type_id == '3')
															<option value="2">Photo Contest</option>
															<option value="1">Video Contest</option>
															<option value="3" selected>Video Resume</option>
															@endif
														</select>
													</div>
												</div>
											</div>
											
											
											<div class="divaux2" id="divaux2{{$contest->id}}">									
												<div class="form-group">
													<label class="col-sm-2 control-label">Video Length</label>
													<div class="col-sm-10">
														<select class="form-control" name="video_length">
															@for ($x = 5; $x <= 15; $x++)
																	@if ($contest->video_length == $x)
																	<option value="{{$x}}" selected>{{$x}}</option>
																	@else
																	<option value="{{$x}}">{{$x}}</option>
																	@endif													
															@endfor
														</select>
													</div>
												  
												</div>
												
												
												
												
											</div>
											<div class="form-group">
											  <label class="col-sm-2 control-label">Concept</label>
											  <div class="col-sm-10">
												<textarea class="form-control" name="concept" rows="5" >{{$contest -> concept}}</textarea>
											  </div>
											</div>
											<div class="form-group">
											  <label class="col-sm-2 control-label">T&C Link</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name="terms_cond" size="30" value="{{$contest -> terms_cond}}">
											  </div>
											</div>
											<div class="form-group">
											  <label class="col-sm-2 control-label">Start Time</label>
											  <div class="col-sm-10">
												<input type="datetime" class="form-control" name ="start_time" size="30" value="{{$contest -> start_time}}">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">End Time</label>
											  <div class="col-sm-10">
												<input type="datetime" class="form-control" name ="end_time" size="30" value="{{$contest -> end_time}}">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Start Start Time</label>
											  <div class="col-sm-10">
												<input type="datetime" class="form-control" name ="score_start_time" size="30" value="{{$contest -> score_start_time}}">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Score End Time</label>
											  <div class="col-sm-10">
												<input type="datetime" class="form-control" name ="score_end_time" size="30" value="{{$contest -> score_end_time}}">
											  </div>
											</div>
										  </div>
										</div>
										
										
																				<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Images</h3>
										  </div>
										  <div class="panel-body">
											
											
											<?php $contest_image = explode('.', $contest->image); ?>
										
											<div class="form-group">
												<span>
													<div>
														<img style="margin-top: -4px;width:10%; height:10%;" src="http://images.joinmethere.tv/events/{{$contest -> image}}" alt="Click here to show the image" id="{{$contest -> id}}" >
													</div>    
												</span>
												<label class="col-sm-2 control-label">Banner</label>
												<div class="col-sm-10">
													<input onchange="readURL(this,app=<?php echo $contest -> id; ?>);" type="file" name='bannerImage' class="form-control" >
												</div>
											</div>
													
											
											<div class="form-group">
												<span>
													<div>
														<img style="margin-top: -4px;width:10%; height:10%;" src="http://images.joinmethere.tv/events/{{$contest_image[0] . '_wt.png'}}" alt="Click here to show the image" id="{{$contest -> id}}" >
													</div>    
												</span>
												<label class="col-sm-2 control-label">Watermark</label>
												<div class="col-sm-10">
													<input onchange="readURL(this,app=<?php echo $contest -> id; ?>);" type="file" name='watermarkImage' class="form-control" >
												</div>
											</div>
											
											<div class="form-group">
												<span>
													<div>
														<img style="margin-top: -4px;width:10%; height:10%;" src="http://images.joinmethere.tv/events/{{$contest_image[0] . '_bg.jpg'}}" alt="Click here to show the image" id="{{$contest -> id}}" >
													</div>    
												</span>
												<label class="col-sm-2 control-label">TAB Background</label>
												<div class="col-sm-10">
													<input onchange="readURL(this,app=<?php echo $contest -> id; ?>);" type="file" name='backgroundImage' class="form-control" >
												</div>
											</div>
											
											<div class="form-group">
												<span>
													<div>
														<img style="margin-top: -4px;width:10%; height:10%;" src="http://images.joinmethere.tv/events/{{$contest_image[0] . '_sq.jpg'}}" alt="Click here to show the image" id="{{$contest -> id}}" >
													</div>    
												</span>
												<label class="col-sm-2 control-label">TAB Banner</label>
												<div class="col-sm-10">
													<input onchange="readURL(this,app=<?php echo $contest -> id; ?>);" type="file" name='tabBannerImage' class="form-control" >
												</div>
											</div>
											
											
											
										  </div>
										</div>
										
			
										
										
										
										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Prizes Detail</h3>
										  </div>
										  <div class="panel-body">
											<div class="form-group">
											  <label class="col-sm-2 control-label">First Prize</label>
											  <div class="col-sm-10">
											  <input type="text" class="form-control" name ="first_reward" size="30" value="{{$contest -> first_reward}}">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Second Prize</label>
											  <div class="col-sm-10">
											  <input type="text" class="form-control" name ="second_reward" size="30" value="{{$contest -> second_reward}}">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Third Prize</label>
											  <div class="col-sm-10">
											  <input type="text" class="form-control" name ="third_reward" size="30" value="{{$contest -> third_reward}}">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Consolation Prizes</label>
											  <div class="col-sm-10">
											  <input type="text" class="form-control" name ="consolation_rewards" size="30" value="{{$contest -> consolation_rewards}}">
											  </div>
											</div>
										  </div>
										</div>



										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Facebook Detail</h3>
										  </div>
										  <div class="panel-body">
											<div class="form-group">
											  <label class="col-sm-2 control-label">Page ID</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name ="fb_page_id" size="30" value="{{$contest -> fb_page_id}}">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Page Name</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name ="fb_page_name" size="30" value="{{$contest -> fb_page_name}}">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Page Link</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name ="fb_page_link" size="30" value="{{$contest -> fb_page_link}}">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Album ID</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name ="id_album" size="30" value="{{$contest -> id_album}}">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">No Timeline</label>
											  <div class="col-sm-10">
													<select class="form-control" name="no_story_fb">													
														@if ($contest->no_story_fb == 'YES')
														<option value="YES" selected>YES</option>
														<option value="NO">NO</option>
														@else
														<option value="NO" selected>NO</option>
														<option value="YES">YES</option>
														@endif
													</select>
												</div>										  
											</div>
										  </div>
										</div>


										

										
										
										
										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Functionalities</h3>
										  </div>
										  <div class="panel-body">
											
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Contest Approval</label>
												<div class="col-sm-10">
													<select class="form-control" name="contest_approval">													
														@if ($contest->contest_approval == '1')
														<option value="1" selected>YES</option>
														<option value="0">NO</option>
														@else
														<option value="0" selected>NO</option>
														<option value="1">YES</option>
														@endif
													</select>
												</div>
											</div>
											
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Customer Logo Insertion</label>
											  <div class="col-sm-10">
												<select class="form-control" name="should_insert_contest_watermark">													
													@if ($contest->should_insert_contest_watermark == '1')
													<option value="1" selected>YES</option>
													<option value="0">NO</option>
													@else
													<option value="0" selected>NO</option>
													<option value="1">YES</option>
													@endif
												</select>
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">JMT Logo Insertion</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="should_insert_company_watermark">													
													@if ($contest->should_insert_company_watermark == '1')
													<option value="1" selected>YES</option>
													<option value="0">NO</option>
													@else
													<option value="0" selected>NO</option>
													<option value="1">YES</option>
													@endif
												</select>
											  
												
											  </div>
											</div>
											
											<div class="divaux2" id="divaux3{{$contest->id}}">
											<div class="form-group">
											  <label class="col-sm-2 control-label">Upload To YouTube</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="should_upload_to_company_youtube">													
													@if ($contest->should_upload_to_company_youtube == '1')
													<option value="1" selected>YES</option>
													<option value="0">NO</option>
													@else
													<option value="0" selected>NO</option>
													<option value="1">YES</option>
													@endif
												</select>
											  										  
											  
												
											  </div>
											</div>
											</div>
											
											
											
											
											
										  </div>
										</div>
										
										
										
										
										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Scoring Detail</h3>
										  </div>
										  <div class="panel-body">
										  
										  
										  
											<div class="form-group">
											  <label class="col-sm-2 control-label">Like Value</label>
											  <div class="col-sm-10">
												
												<select class="form-control" name="like_value">
													@for ($x = 0; $x <= 5; $x++)
															@if ($contest->like_value == $x)
															<option value="{{$x}}" selected>{{$x}}</option>
															@else
															<option value="{{$x}}">{{$x}}</option>
															@endif													
													@endfor
												</select>
											  
												
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Comment Value</label>
											  <div class="col-sm-10">
												
												<select class="form-control" name="comment_value">
													@for ($x = 0; $x <= 5; $x++)
															@if ($contest->comment_value == $x)
															<option value="{{$x}}" selected>{{$x}}</option>
															@else
															<option value="{{$x}}">{{$x}}</option>
															@endif													
													@endfor
												</select>
											  
												
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Share Value</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="share_value">
													@for ($x = 0; $x <= 5; $x++)
															@if ($contest->share_value == $x)
															<option value="{{$x}}" selected>{{$x}}</option>
															@else
															<option value="{{$x}}">{{$x}}</option>
															@endif													
													@endfor
												</select>
											  
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Click Post Value</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="click_post">
													@for ($x = 0; $x <= 5; $x++)
															@if ($contest->click_post == $x)
															<option value="{{$x}}" selected>{{$x}}</option>
															@else
															<option value="{{$x}}">{{$x}}</option>
															@endif													
													@endfor
												</select>
											  
												
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Page Like Value</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="pagelike_value">
													@for ($x = 0; $x <= 5; $x++)
															@if ($contest->pagelike_value == $x)
															<option value="{{$x}}" selected>{{$x}}</option>
															@else
															<option value="{{$x}}">{{$x}}</option>
															@endif													
													@endfor
												</select>
												
												
												
											  </div>
											</div>
										  
										  
										  
										  
										  
										  </div>
										</div>
										
										
										
										
										
										
										<input type='hidden' name='id' class="prependedInput" size="16" type="text" value="{{ $contest -> id }}">
										
										<hr>
										
										<div class="help-block">
											<button type="submit" class="btn btn-large btn-info">Submit Event</button>
										</div>
										
								 
								
									   
									  </form>
							        
						</div>
								  
								  
								</div>
							</div>
												
							
                  
						</div>
                    
                    @endforeach
                    </div>

                </div> 
            
            <!--List End--> 




        </div>    
        <div id="AddModal" class="modal fade">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
				  <div class="modal-header">
					<button class="close" data-dismiss="modal">×</button>
					<h3>Add new Contest</h3>
				  </div>
				  <div class="modal-body">
					<form action="{{ URL::to('event/addEvent') }}" method="post" enctype="multipart/form-data" runat="server">
									
										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Contest Detail</h3>
										  </div>
										  <div class="panel-body">
										  
										  <div class="form-group">
										  <label class="col-sm-2 control-label">Name</label>
										  <div class="col-sm-10">
										  <input type="text" class="form-control" name ="name" value="">
										  </div>
										 </div>
																			
										
										
										
										
										<div class="form-group">
											<label class="col-sm-2 control-label">Company Name</label>
											<div class="col-sm-10">
												<select class="form-control" name="company_id">
												  @foreach($companys as $company)														
														<option value="{{ $company -> id }}">{{ $company -> name }}</option>
												  @endforeach
												</select>
											</div>
										</div>
										
										
										<div class="form-group">
											<label class="col-sm-2 control-label">Location</label>
											<div class="col-sm-10">
												<select class="form-control" name="location_id">
												  @foreach($locations as $location)
														
														<option value="{{ $location -> id }}">{{ $location -> name }}</option>
												  @endforeach
												</select>
											</div>
										</div>
										
										
										<div class="form-group">
											<label class="col-sm-2 control-label">Main Hashtag</label>
											 <div class="col-sm-10">
												<input type="text" class="form-control" name ="description" size="30" value="">
											</div>
										</div>
										
										
										<div class="form-group">
										  <label class="col-sm-2 control-label">Secondary HashTags</label>
										  <div class="col-sm-10">
											<input type="text" class="form-control" name ="hashtags" size="30" value="">											
										  </div>
										</div>
										  
										  
											<div class="divaux" id="divaux{{$contest->id}}">
												<div class="form-group">
													<label class="col-sm-2 control-label">Contest Type</label>
													<div class="col-sm-10">
														<select class="form-control" id="contestTypeSelect" name="type_id">
															<option value="1" selected>Video Contest</option>
															<option value="2" >Photo Contest</option>	
															<option value="3">Video Resume</option>																
														</select>
													</div>
												</div>
											</div>
											
											
											<div class="divaux2" id="divaux2{{$contest->id}}">									
												<div class="form-group">
													<label class="col-sm-2 control-label">Video Length</label>
													<div class="col-sm-10">
														<select class="form-control" name="video_length">
															@for ($x = 5; $x <= 15; $x++)
																	
																	<option value="{{$x}}">{{$x}}</option>												
															@endfor
														</select>
													</div>
												  
												</div>
												
												
												
												
											</div>
											<div class="form-group">
											  <label class="col-sm-2 control-label">Concept</label>
											  <div class="col-sm-10">
												<textarea class="form-control" name="concept" rows="5" ></textarea>
											  </div>
											</div>
											<div class="form-group">
											  <label class="col-sm-2 control-label">T&C Link</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name="terms_cond" size="30" value="">
											  </div>
											</div>
											<div class="form-group">
											  <label class="col-sm-2 control-label">Start Time</label>
											  <div class="col-sm-10">
												<input type="datetime" class="form-control" name ="start_time" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">End Time</label>
											  <div class="col-sm-10">
												<input type="datetime" class="form-control" name ="end_time" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Start Start Time</label>
											  <div class="col-sm-10">
												<input type="datetime" class="form-control" name ="score_start_time" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Score End Time</label>
											  <div class="col-sm-10">
												<input type="datetime" class="form-control" name ="score_end_time" size="30" value="">
											  </div>
											</div>
										  </div>
										</div>
										
										
										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Images</h3>
										  </div>
										  <div class="panel-body">
											
											
										
											<div class="form-group">
												<span>
													<div>
														<img style="margin-top: -4px;width:10%; height:10%;" src="" alt="Click here to show the image" id="t100" >
													</div>    
												</span>
												<label class="col-sm-2 control-label">Banner</label>
												<div class="col-sm-10">
													<input onchange="readURL(this,app='t100');" type="file" name='bannerImage' class="form-control" >
												</div>
											</div>
													
											
											<div class="form-group">
												<span>
													<div>
														<img style="margin-top: -4px;width:10%; height:10%;" src="" alt="Click here to show the image" id="t100" >
													</div>    
												</span>
												<label class="col-sm-2 control-label">Watermark</label>
												<div class="col-sm-10">
													<input onchange="readURL(this,app='t100');" type="file" name='watermarkImage' class="form-control" >
												</div>
											</div>
											
											<div class="form-group">
												<span>
													<div>
														<img style="margin-top: -4px;width:10%; height:10%;" src="" alt="Click here to show the image" id="t100" >
													</div>    
												</span>
												<label class="col-sm-2 control-label">TAB Background</label>
												<div class="col-sm-10">
													<input onchange="readURL(this,app='t100');" type="file" name='backgroundImage' class="form-control" >
												</div>
											</div>
											
											<div class="form-group">
												<span>
													<div>
														<img style="margin-top: -4px;width:10%; height:10%;" src="" alt="Click here to show the image" id="t100" >
													</div>    
												</span>
												<label class="col-sm-2 control-label">TAB Banner</label>
												<div class="col-sm-10">
													<input onchange="readURL(this,app='t100');" type="file" name='tabBannerImage' class="form-control" >
												</div>
											</div>
											
											
											
										  </div>
										</div>
										
										

										
										
										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Prizes Detail</h3>
										  </div>
										  <div class="panel-body">
											<div class="form-group">
											  <label class="col-sm-2 control-label">First Prize</label>
											  <div class="col-sm-10">
											  <input type="text" class="form-control" name ="first_reward" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Second Prize</label>
											  <div class="col-sm-10">
											  <input type="text" class="form-control" name ="second_reward" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Third Prize</label>
											  <div class="col-sm-10">
											  <input type="text" class="form-control" name ="third_reward" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Consolation Prizes</label>
											  <div class="col-sm-10">
											  <input type="text" class="form-control" name ="consolation_rewards" size="30" value="">
											  </div>
											</div>
										  </div>
										</div>



										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Facebook Detail</h3>
										  </div>
										  <div class="panel-body">
											<div class="form-group">
											  <label class="col-sm-2 control-label">Page ID</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name ="fb_page_id" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Page Name</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name ="fb_page_name" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Page Link</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name ="fb_page_link" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Album ID</label>
											  <div class="col-sm-10">
												<input type="text" class="form-control" name ="id_album" size="30" value="">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">No Timeline</label>
											  <div class="col-sm-10">
													<select class="form-control" name="no_story_fb">													
														
														<option value="NO">NO</option>
														<option value="YES">YES</option>
													</select>
												</div>										  
											</div>
										  </div>
										</div>


										

										
										
										
										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Functionalities</h3>
										  </div>
										  <div class="panel-body">
											
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Contest Approval</label>
												<div class="col-sm-10">
													<select class="form-control" name="contest_approval">	
														<option value="1">YES</option>
														<option value="0" selected>NO</option>
														
													</select>
												</div>
											</div>
											
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Customer Logo Insertion</label>
											  <div class="col-sm-10">
												<select class="form-control" name="should_insert_contest_watermark">													
													
													<option value="1">YES</option>
													<option value="0" selected>NO</option>
													
												</select>
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">JMT Logo Insertion</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="should_insert_company_watermark">													
													
													<option value="1">YES</option>
													<option value="0" selected>NO</option>
													
												</select>
											  
												
											  </div>
											</div>
											
											<div class="divaux2" id="divaux3{{$contest->id}}">
											<div class="form-group">
											  <label class="col-sm-2 control-label">Upload To YouTube</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="should_upload_to_company_youtube">	
													<option value="1">YES</option>
													<option value="0" selected>NO</option>
													
												</select>
											  										  
											  
												
											  </div>
											</div>
											</div>
											
											
											
											
											
										  </div>
										</div>
										
										
										
										
										<div class="panel panel-default">
										  <div class="panel-heading">
											<h3 class="panel-title">Scoring Detail</h3>
										  </div>
										  <div class="panel-body">
										  
										  
										  
											<div class="form-group">
											  <label class="col-sm-2 control-label">Like Value</label>
											  <div class="col-sm-10">
												
												<select class="form-control" name="like_value">
													@for ($x = 0; $x <= 5; $x++)
															
															<option value="{{$x}}">{{$x}}</option>
																										
													@endfor
												</select>
											  
												
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Comment Value</label>
											  <div class="col-sm-10">
												
												<select class="form-control" name="comment_value">
													@for ($x = 0; $x <= 5; $x++)
															
															<option value="{{$x}}">{{$x}}</option>									
													@endfor
												</select>
											  
												
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Share Value</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="share_value">
													@for ($x = 0; $x <= 5; $x++)
															
															<option value="{{$x}}">{{$x}}</option>													
													@endfor
												</select>
											  
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Click Post Value</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="click_post">
													@for ($x = 0; $x <= 5; $x++)
															
															<option value="{{$x}}">{{$x}}</option>									
													@endfor
												</select>
											  
												
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-sm-2 control-label">Page Like Value</label>
											  <div class="col-sm-10">
											  
												<select class="form-control" name="pagelike_value">
													@for ($x = 0; $x <= 5; $x++)
															
															<option value="{{$x}}">{{$x}}</option>												
													@endfor
												</select>
												
												
												
											  </div>
											</div>
										  
										  
										  
										  
										  
										  </div>
										</div>
										
										
										
										
										
										
										
										
										<hr>
										
										<div class="help-block">
											<button type="submit" class="btn btn-large btn-info">Submit Event</button>
										</div>
					</form>
				  </div>
				  
				</div>
			</div>
		
		
           
            

            
        </div>
        
        
		<div id="confirmationModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		   
				<div class="modal-content">

					<div class="modal-header">
						<a class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></a>
						<h4 class="modal-title" id="myModalLabel">Confirmation</h4>
					</div>
					<div class="modal-body">
						<p>Are you sure you want to delete this?</p>
					</div>
					<div class="modal-footer">
						<div class="btn-group">
							<button class="btn btn-danger" data-dismiss="modal">NO</button>
							<a href="{{ URL::to('contest/deleteContest/'.$contest -> id ) }}"><button class="btn btn-primary">YES</button></a>
						</div>
					</div>

				</div><!-- /.modal-content -->
		   
		</div><!-- /.modal -->
		


		<script type="text/javascript" src="packages/bootstrap/js/jquery.js"></script>
		<script type="text/javascript" src="packages/bootstrap/js/bootstrap.js"></script>
		<script type="text/javascript" src="packages/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="lib/js/CustomScrollbar.js"></script>
        

<script>
$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
	
    


});

$("button[id*='EditModal']").click(function(){


var idContest = $(this).attr('id').replace('EditModal', '');

if ($("#divaux" + idContest).find("option:selected").text() == 'Photo Contest'){
	$("#divaux2" + idContest).hide();
	$("#divaux3" + idContest).hide();}
	else {
	$("#divaux2" + idContest).show();
	$("#divaux3" + idContest).show();}




 
});

(function($) {
	$(window).load(function() {
		$("#contentScroll").mCustomScrollbar({
			scrollButtons: {
				enable: true
			}
		});
	
	
	
	
	});
})(jQuery);
			
function readURL(input, id) {
 if (input.files && input.files[0]) {
	 var reader = new FileReader();
	 reader.onload = function (e) {
		$('#'+id).attr('src', e.target.result);
		
		var image = new Image();

    image.src = e.target.result;

    image.onload = function() {alert(image.width);};
		
		
	 }
	 reader.readAsDataURL(input.files[0]);
	}
 }
			 
			 
				
				

$('.divaux select.form-control').change(function () {
	 var optionSelected = $(this).find("option:selected");
	 var valueSelected  = optionSelected.val();
	 var textSelected   = optionSelected.text();
	 
	if (textSelected == 'Photo Contest'){
	$(".divaux2").hide(); }
	else {
	$(".divaux2").show(); }
 });


				


			
			 
			 
 $(document).on('change', '.btn-file :file', function() {
	  var input = $(this),
		  numFiles = input.get(0).files ? input.get(0).files.length : 1,
		  label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	  input.trigger('fileselect', [numFiles, label]);
	});



</script>
	@stop
