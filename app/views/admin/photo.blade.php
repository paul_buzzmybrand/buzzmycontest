@extends('layouts.admin')
@section('content')

 <ol class="breadcrumb" style="text-align: left">
 	<li><a href="{{ $username == 'admin' ? action('Admin_ContestController@externalAdminIndex') : action('Admin_ContestController@externalIndex', $photo->contest->company->id)}}">{{ trans('admin.External Contests') }}</a></li>
	<li><a href="{{ action('Admin_ContestController@contest', $photo->contest->id)}}">{{ $photo->contest->name }}</a></li>
	<li class="active">{{ $photo->name }}</li>
	</ol>

<div class="row" style="margin-bottom: 10px">
	 <img src="{{asset('photos/'.$photo->filename)}}" alt="" height="{{$height}}" width="{{$width}}"> 
</div>

@if (!$photo->contest->contest_approval)
<div class="alert alert-info">{{ trans('admin.Contest does not require photo approval') }}</div>
@elseif ($photo->contest->contentApprovalDone)
<div class="alert alert-info">{{ trans('admin.Contest approval done and photo review not available any more') }}</div>
@else
<div class="row">
{{ Form::open(array('action' => array('Admin_ContestController@approvePhoto', $photo->id), 'id' => 'approveForm')) }}
<div class="btn-group" data-toggle="buttons" value="1">

  <label class="approval-btn btn btn-primary {{ $photo->approvalStep->id == 3 ? 'active' : ''}}">
    <input class="approval-btn" type="radio" name="approvalStep" id="option3" value="3"/>{{ trans('admin.buttons.Approved') }}
  </label>
  <label class="btn btn-primary {{ $photo->approvalStep->id == 1 ? 'active' : ''}}">
    <input class="approval-btn" type="radio" name="approvalStep" id="option1" value="1"/>{{ trans('admin.buttons.No_approval') }}
  </label>
  <label class="approval-btn btn btn-primary {{ $photo->approvalStep->id == 2 ? 'active' : ''}}">
    <input class="approval-btn" type="radio" name="approvalStep" id="option2" value="2"/>{{ trans('admin.buttons.Not_approved') }}
  </label>

</div>
{{ Form::close() }}
</div>

<script>
	$(document).ready(function() {
		$(".approval-btn").change(function(e) {
			$("#approveForm").submit();
		});
	});
</script>
@endif

@stop
