<!DOCTYPE html>
<html class="show">
  <head>
    <meta charset="utf-8">
    <title>Buzzmybrand</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link href='http://fonts.googleapis.com/css?family=Abel|Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <style>
        body {
            margin: 0;
        }
        html, a {
            font-family: "Open Sans", Helvetica, Arial, sans-serif;
            color: #375e99;
            text-decoration: none;
            text-align: center;
        }
        h2 {
            font-size: 1.5em;
        }
        .header {
            width: 100%;
            height: 50px;
            background-color: #375e99;
        }
        .container {
            padding: 10px;
        }
        .social-account {
            float: left;
            margin-left: 10px;
        }
        .social-account span {
            color: #fff;
            font-size: 12px;
            display: inline-block;
            vertical-align: top;
            margin: 18px 5px 0 5px;
        }
        .social-account img {
            margin-top: 10px;
        }
        .social-logout {
            float: right;
            margin-right: 10px;
        }
        .social-logout a {
            color: #fff;
            font-size: 12px;
            display: inline-block;
            vertical-align: top;
            margin-top: 18px;
        }

        ul { margin: 0; padding: 0;}

        li {
            list-style: none;
            margin: 0;
            padding: 6px 0;
            border-bottom: 1px solid #ddd;
        }

        li:hover{
            background-color: rgba(55,94,153,0.1);
        }

        button {
            background-color: #375e99;
            color: white;
            border: 0;
            text-align: center;
        }

        .page-picture {
            width: 80px;
            margin-left: 20px;
            border-radius: 4px;
        }
        .page-name {
            padding-left: 30px;
        }
        .loading {
            min-height: 40px;
            position: relative;
        }
        .loading.fullpage {
            width: 100%;
            position: absolute;
            top: 0;
            bottom: 0;
        }
        .loading:before {
            border-radius: inherit;
            position: absolute;
            width: 100%;
            min-height: 40px;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background-color: #fff;
            content: '';
            z-index: 10;
            opacity: 0.6;
            background-image: url('/dashboard/assets/images/ajax-loader.gif');
            background-position: 50%;
            background-repeat: no-repeat;
        }
    </style>
  </head>
<body>
    <div class="loading fullpage" style="display:none"></div>
    <div class="header">
        <div class="social-account">
            <img src="{{ $user['profile_image'] }}">
            <span>{{ $user['name']}}</span>
        </div>
        <div class="social-logout">
            <a href="{{ $logoutUrl }}">{{ Lang::get('dashboard.logout') }}</a>
        </div>
    </div>
    <div class="container">
        @if(!$accounts)
        <h2>{{ Lang::get('dashboard.you_dont_administer_any_fb_page') }}</h2>
        <button class="btn btn-default" onclick="window.close()">{{ Lang::get('dashboard.close_and_go_back') }}</button>
        @else
        <h2>{{ Lang::get('dashboard.choose_a_facebook_page') }}</h2>
        @endif    
        <ul>
            @foreach($accounts as $id => $page)
            <li>
                <a href="#" data-accountid="{{ $id }}" data-page_image="{{ $page['picture'] }}">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <img class="page-picture" src="{{ $page['picture'] }}" >
                            </td>
                            <td>
                                <p class="page-name">{{ $page['name'] }}</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </a>
            </li>
            @endforeach
        </ul>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
$(function() {
    $('ul li > a').click(function(e) {
        $('.loading').show();
        var page_id = $(this).data('accountid');
        var admin_token = '{{ $access_token }}';
		var page_image = $(this).data('page_image');

        $.post('/admin/facebook-customer-connect/{{ $contest_id }}', {
            'admin_token': admin_token,
            'page_id': page_id,
			'page_image': page_image
        }).then(function(response) {
            var data = JSON.parse(response);
            var scope = window.opener.$('#contest-edit-ctrl').scope();
            if(data.result == 1) {
                scope.socialConnectResult('facebook', true, data.page, data.image);
            } else {
                scope.socialConnectResult('facebook', false, data.message, '');
            }
            window.close();
        });
    });
});
</script>
</html>