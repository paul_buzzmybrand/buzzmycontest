

@if( $videos->count() > 0 )

<?php $counter = 0 ?>
<?php foreach($videos as $video): ?>

<?php if ($counter % $cols == 0) : ?>
<div class="row">
<?php endif ?>
	

	<div class="thumbnail col-xs-12 col-sm-6 col-md-6 col-lg-4">

	<div id='mediaplayerall{{$video->id}}' class="video">
	<a href="javascript:void(0)" onClick="start_jw_player('0', '{{$video->id}}', '{{$video->filename}}', '{{$video->image}}');" style='color:#555;'><img class="img-thumbnail" src="{{asset('/images/videos/'.$video->image)}}" width="width" height="250"/></a>
	</div>

	<div class="caption">

	<h4><b>{{utf8_decode($video->name)}}</b></h4>

	@if ($video->approval_step_id=='3')
	<?php foreach($video->fbPages as $p): ?>
	<p style="text-align: right;"><a href="https://www.facebook.com/video.php?v={{ $p->pivot->fb_idpost }}">{{ trans('admin.View it on Facebook') }}</a></p>
	<?php endforeach; ?>
	@endif

	<p style="text-align: right;">{{ trans('admin.user') }}: <a style="color: black;" href="https://www.facebook.com/{{$video->user->facebookProfiles->first()['id_fb']}}"><b>{{ $video->user->facebookProfiles->first()['name'] }}</b></a></p>

	@if ($video->approval_step_id!='3')
	<p style="text-align: right;">{{ trans('admin.approval status') }}: <b>{{ $video->approvalStep->name }}</b></p>
	@endif

	@if ($video->approval_step_id=='3')
	<p style="text-align: right;">{{ trans('admin.JMT Score') }}: <b>{{ $video->score }}</b></p>
	<p style="text-align: right;">{{ trans('admin.statistics.Likes') }}: <b>{{ @$statistics[$video->id]['unique_likes'] }}</b></p>
	<p style="text-align: right;">{{ trans('admin.statistics.Comments') }}: <b>{{ @$statistics[$video->id]['unique_comments'] }}</b></p>
	<p style="text-align: right;">{{ trans('admin.statistics.Shares') }}: <b>{{ @$statistics[$video->id]['unique_shares'] }}</b></p>
	<p style="text-align: right;">{{ trans('admin.statistics.Views') }}: <b>{{ @$statistics[$video->id]['unique_views'] }}</b></p>
	<p style="text-align: right;">{{ trans('admin.statistics.Impressions') }}: <b>{{ @$statistics[$video->id]['reach'] }}</b></p>
	<p style="text-align: right;">{{ trans('admin.Ranking') }}: <b>{{ $video->rank_position }}</b></p> 
	@endif

	@if ($video->approval_step_id=='3')
	<h2 style="text-align: left;color: red;">{{ $video->rank_position }}</h2>
	@endif


	<!-- START Bottoni per approvare, senza approvazione, non approvare -->
	{{ Form::open(array('action' => array('Admin_ContestController@approveVideo', $video->id), 'id' => 'approveForm')) }}
	<input type="hidden" name="approvalStep" value="3"/> 
	<input type="submit" class="btn btn-primary {{ $video->approvalStep->id == 3 ? 'active' : ''}}" value="{{ trans('admin.buttons.Approved') }}">
	{{ Form::close() }}

	{{ Form::open(array('action' => array('Admin_ContestController@approveVideo', $video->id), 'id' => 'approveForm')) }}
	<input type="hidden" name="approvalStep" value="1"/> 
	<input type="submit" class="btn btn-primary {{ $video->approvalStep->id == 1 ? 'active' : ''}}" value="{{ trans('admin.buttons.No_approval') }}">
	{{ Form::close() }}

	{{ Form::open(array('action' => array('Admin_ContestController@approveVideo', $video->id), 'id' => 'approveForm')) }}
	<input type="hidden" name="approvalStep" value="2"/> 
	<input type="submit" class="btn btn-primary {{ $video->approvalStep->id == 2 ? 'active' : ''}}" value="{{ trans('admin.buttons.Not_approved') }}">
	{{ Form::close() }}
	<!-- END Bottoni per approvare, senza approvazione, non approvare -->


	<!-- START Box per mandare le notifiche -->
	<button id="send_notificationt0c{{$video->id}}" onClick="showBoxNotification('0', '{{$video->id}}')" type="button" class="btn btn-success">{{ trans('admin.notifications.Send Notifications') }}</button>
	<div id="box_notificationst0c{{$video->id}}" class="form-group" style="display: none" >
	{{ Form::open(array('action' => array('Admin_ContestController@sendNotification', $video->id), 'id' => 'notificationForm')) }}
	<label for="comment">{{ trans('admin.notifications.Notifications') }}:</label>
	<textarea class="form-control" rows="5" id="comment" name="message" ></textarea>
	<input type="submit" class="btn btn-primary" onClick="closeBoxNotification('0', '{{$video->id}}')" value="{{ trans('admin.notifications.Send') }}" >
	<button type="button" class="btn btn-primary" onClick="closeBoxNotification('0', '{{$video->id}}')">{{ trans('admin.notifications.Cancel') }}</button>
	{{ Form::close() }}
	</div>
	<!-- END Box per mandare le notifiche -->

	
	<!-- START Bottone per eliminare -->
	{{ Form::open(array('action' => array('Admin_ContestController@deleteVideo', $video->id), 'id' => 'deleteForm')) }}
	<input type="submit" class="btn btn-danger" value="{{ trans('admin.buttons.Delete') }}">
	{{ Form::close() }}
	<!-- END Bottone per eliminare -->
	

	</div>


	</div>
 
	

<?php if ($counter % $cols == $cols - 1) : ?>	
</div>
<?php endif ?>

<?php $counter++; ?>
<?php endforeach; ?>

@else

<?php if ($page == 1) : ?>
<div class="alert alert-info">{{ trans('admin.No videos for this option') }}</div>
<?php endif ?>


<script>
endAllEntries = true;

</script>


@endif
