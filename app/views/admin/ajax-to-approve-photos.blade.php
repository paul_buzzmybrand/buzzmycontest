

@if( $photos->count() > 0 )

<?php $counter = 0 ?>
<?php foreach($photos as $photo): ?>

<?php if ($counter % $cols == 0) : ?>
<div class="row">
<?php endif ?>
	

	<div class="thumbnail col-xs-12 col-sm-6 col-md-6 col-lg-4">

	<!--<a href="{{action('Admin_ContestController@photo', $photo->id)}}" style='color:#555;'><img class="img-thumbnail" src="{{asset('/images/photos/'.$photo->image)}}" width="width" height="250"/></a>-->
	<img class="img-thumbnail" src="{{asset('/images/photos/'.$photo->image)}}" width="width" height="250"/>

	<div class="caption">
	<h4><b>{{utf8_decode($photo->name)}}</b></h4>

	<?php if ( $photo->user && $photo->user->facebookProfiles->count() ) { ?>
	<p style="text-align: right;">{{ trans('admin.user') }}: <a style="color: black;" href="https://www.facebook.com/{{$photo->user->facebookProfiles->first()['id_fb']}}"><b>{{ $photo->user->facebookProfiles->first()['name'] }}</b></a></p>
	<?php } else if ( $photo->user && $photo->user->twitterProfiles->count() ) { ?>
	<?php } ?>

	<!-- START Bottoni per approvare, senza approvazione, non approvare -->
	{{ Form::open(array('action' => array('Admin_ContestController@approvePhoto', $photo->id), 'id' => 'approveForm')) }}
	<input type="hidden" name="approvalStep" value="3"/> 
	<input type="submit" class="btn btn-primary {{ $photo->approvalStep->id == 3 ? 'active' : ''}}" value="{{ trans('admin.buttons.Approved') }}">
	{{ Form::close() }}

	{{ Form::open(array('action' => array('Admin_ContestController@approvePhoto', $photo->id), 'id' => 'approveForm')) }}
	<input type="hidden" name="approvalStep" value="1"/> 
	<input type="submit" class="btn btn-primary {{ $photo->approvalStep->id == 1 ? 'active' : ''}}" value="{{ trans('admin.buttons.No_approval') }}">
	{{ Form::close() }}

	{{ Form::open(array('action' => array('Admin_ContestController@approvePhoto', $photo->id), 'id' => 'approveForm')) }}
	<input type="hidden" name="approvalStep" value="2"/> 
	<input type="submit" class="btn btn-primary {{ $photo->approvalStep->id == 2 ? 'active' : ''}}" value="{{ trans('admin.buttons.Not_approved') }}">
	{{ Form::close() }}
	<!-- END Bottoni per approvare, senza approvazione, non approvare -->

	<!-- START Box per mandare le notifiche -->
	<button id="send_notificationt1c{{$photo->id}}" onClick="showBoxNotification('1', '{{$photo->id}}')" type="button" class="btn btn-success">{{ trans('admin.notifications.Send Notifications') }}</button>
	<div id="box_notificationst1c{{$photo->id}}" class="form-group" style="display: none" >
	{{ Form::open(array('action' => array('Admin_ContestController@sendNotificationPhoto', $photo->id), 'id' => 'notificationForm')) }}
	<label for="comment">{{ trans('admin.notifications.Notifications') }}:</label>
	<textarea class="form-control" rows="5" id="comment" name="message" ></textarea>
	<input type="submit" class="btn btn-primary" onClick="closeBoxNotification('1', '{{$photo->id}}')" value="{{ trans('admin.notifications.Send') }}" >
	<button type="button" class="btn btn-primary" onClick="closeBoxNotification('1', '{{$photo->id}}')">{{ trans('admin.notifications.Cancel') }}</button>
	{{ Form::close() }}
	</div>
	<!-- END Box per mandare le notifiche -->
	
	<!-- START Bottone per eliminare -->
	{{ Form::open(array('action' => array('Admin_ContestController@deletePhoto', $photo->id), 'id' => 'deleteForm')) }}
	<input type="submit" class="btn btn-danger" value="{{ trans('admin.buttons.Delete') }}">
	{{ Form::close() }}
	<!-- END Bottone per eliminare -->

	</div>
	</div>
 
	

<?php if ($counter % $cols == $cols - 1) : ?>	
</div>
<?php endif ?>

<?php $counter++; ?>
<?php endforeach; ?>

@else

<?php if ($page == 1) : ?>
<div class="alert alert-info">{{ trans('admin.No photos for this option') }}</div>
<?php endif ?>

<script>
endToApproveEntries = true;
</script>

@endif
