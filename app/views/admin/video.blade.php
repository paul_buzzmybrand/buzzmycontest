@extends('layouts.admin')
@section('content')

 <ol class="breadcrumb" style="text-align: left">
 	<li><a href="{{ $username == 'admin' ? action('Admin_ContestController@externalAdminIndex') : action('Admin_ContestController@externalIndex', $video->contest->company->id)}}">{{ trans('admin.External Contests') }}</a></li>
	<li><a href="{{ action('Admin_ContestController@contest', $video->contest->id)}}">{{ $video->contest->name }}</a></li>
	<li class="active">{{ $video->name }}</li>
	</ol>

<div class="row" style="margin-bottom: 10px">
	@if (($rotation == "0") || ($rotation == "180"))
	<video width="640" height="480" controls>
		<source src="{{asset('videos/'.$video->filename)}}" type="video/mp4"/>
		Your browser does not support the video tag.
	</video>
	@elseif (($rotation == "90") || ($rotation == "270"))
	<video width="480" height="640" controls>
		<source src="{{asset('videos/'.$video->filename)}}" type="video/mp4"/>
		Your browser does not support the video tag.
	</video>
	@endif
</div>

@if (!$video->contest->contest_approval)
<div class="alert alert-info">{{ trans('admin.Contest does not require video approval') }}</div>
@elseif ($video->contest->contentApprovalDone)
<div class="alert alert-info">{{ trans('admin.Contest approval done and video review not available any more') }}</div>
@else
<div class="row">
{{ Form::open(array('action' => array('Admin_ContestController@approveVideo', $video->id), 'id' => 'approveForm')) }}
	<div class="btn-group" data-toggle="buttons" value="1">
  <label class="approval-btn btn btn-primary {{ $video->approvalStep->id == 3 ? 'active' : ''}}">
    <input class="approval-btn" type="radio" name="approvalStep" id="option3" value="3"/>{{ trans('admin.buttons.Approved') }}
  </label>
  <label class="btn btn-primary {{ $video->approvalStep->id == 1 ? 'active' : ''}}">
    <input class="approval-btn" type="radio" name="approvalStep" id="option1" value="1"/>{{ trans('admin.buttons.No_approval') }}
  </label>
  <label class="approval-btn btn btn-primary {{ $video->approvalStep->id == 2 ? 'active' : ''}}">
    <input class="approval-btn" type="radio" name="approvalStep" id="option2" value="2"/>{{ trans('admin.buttons.Not_approved') }}
  </label>
</div>
{{ Form::close() }}
</div>



<script>
	$(document).ready(function() {
		$(".approval-btn").change(function(e) {
			$("#approveForm").submit();
		});
	});
</script>
@endif

@if ($video->contest->contest_fields == 'complex')
<div class="row">
	<?php $submission_info_id = $video->extraInfo_id; ?>
	<?php $submission_info = ExtraInfo::findOrFail($submission_info_id); ?>
	<h3>Company Name: <span class="label label-default">{{ $submission_info->company }}</span></h3>
	<h3>Registration Number: <span class="label label-default">{{ $submission_info->regnumber }}</span></h3>
	<h3>Phone Number: <span class="label label-default">{{ $submission_info->telephone }}</span></h3>
</div>
@endif

@stop
