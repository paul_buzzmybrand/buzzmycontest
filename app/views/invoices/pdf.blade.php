<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{ trans('invoices.invoice') }} #{{{ $invoice->id }}}</title>
    <style>
        body {
        	font-family: sans-serif;
        	font-size: 12px;
        }
        #content {
        	margin: 1cm;
        }
        table.heading, table.heading td {
        	border: 0;
        }
        #logo img {
        	width: 250px;
        	margin-top: -100px;
        }
        #mu_address {
        	right: 0px;
        	margin-top: -1cm;
        	width: 300px;
        }

        span.large {
        	font-size: 120%;
        	font-weight: bold;
        }

        .invoice_ref {
        	width: 100%;
        	background-color: #eeeeee;
        	padding: 10px;
        	margin-bottom: 1cm;
        }

        table {
        	margin-top: 1cm;
        	margin-bottom: 1cm;
        	width: 100%;
        	border-spacing: 0;
        	border: 1px solid #dddddd;
        }

        table td.large, table th.large {
        	width: 70%;
        }
        td.small {
        	width: 10%;
        }
        td, th {
        	padding: 2px;
        	border: 2px solid #dddddd;
        }

        table.details th, table.transactions th, table tr.totals {
        	background-color: #eeeeee;
        }

        table.transactions td {
        	text-align: center;
        }

        table tr.totals td.large {
        	text-align: right;
        	font-weight: bold;
        }
        table .footer {
            margin: 0;
            border: 0px solid #fff;
        }
        table .footer td, table .footer th {
            padding: 2px;
            border: 0px solid #fff;
            vertical-align: top;
            color: black;
        }

        .footer-container {
            width: 100%;
            text-align: center;
            position: fixed;
            bottom: 100;
        }

        .line-separator{
            height:1px;
            background:#717171;
            border-bottom:1px solid #313030;
        }

        h1 {
        	margin-bottom: 0;
        }
    </style>
</head>
<body>
    <div id="content">
    <table class="heading">
        <tr>
        <td id="logo"><img src="{{ storage_path() }}/images/logo-fattura.png"></td>
        <td id="mu_address" align="right">
            <div class="buyer_address">
                To: {{ $invoice->user->company->name }}<br />
                Att.n: {{ $invoice->user->full_name }}<br />
                {{ $invoice->user->address1 }}<br />
                @if($invoice->user->address2)
                    {{ $invoice->user->address2 }}<br />
                @endif
                {{ Countries::getOne($invoice->user->country, App::getLocale(), 'cldr') }}
                <br />
            </div>
        </td>
        </tr>
    </table>

    <div style="clear: both"></div>

    <div class="invoice_ref">
        <span class="number">{{ trans('invoices.invoice') }} <b>#{{ $invoice->getFormattedId() }}</b></span><br />
        {{ trans('invoices.invoice_date') }} {{ $invoice->created_at->format('d/m/Y') }}<br />
    </div>

    <table class="details">
        <tr>
            <th class="large centered">{{ trans('invoices.items') }}</th>
            <th class="large centered" style="width: 70%">{{ trans('invoices.description') }}</th>
            <th class="centered">{{ trans('invoices.total') }}</th>
        </tr>
        <tr>
            <td class="large">{{ $invoice->description }}</td>
            <td class="large">{{ $invoice->contest->name }}</td>
			<td class="large"  align="center">{{ Config::get('ecommerce.currency_'.$invoice->currency) }} {{ number_format($invoice->contest_amount/100, Config::get('ecommerce.decimal_places')) }}</td>         
        </tr>
        @if($invoice->coupon && $invoice->coupon_amount > 0)
        <tr>
            <td class="large">COUPON</td>
            <td class="large">{{ trans('invoices.coupon_code') }}: {{ $invoice->coupon }}</td>
            <td class="large"  align="center"> - {{ Config::get('ecommerce.currency_'.$invoice->currency) }} {{ number_format($invoice->coupon_amount/100, Config::get('ecommerce.decimal_places')) }}</td>
        </tr>
        @endif
		<!--
        <tr class="totals">
            <td></td>
            <td class="large">{{ trans('invoices.total_te') }}</td>
            <td align="center">{{ Config::get('ecommerce.currency')[$invoice->currency] }} {{ number_format($invoice->amount/100, Config::get('ecommerce.decimal_places')) }}</td>
        </tr>
        <tr class="totals">
            <td></td>
            <td class="large">{{ trans('invoices.vat') }}% {{ number_format(Config::get('ecommerce.vat'), Config::get('ecommerce.decimal_places')) }}</td>
            <td align="center">{{ Config::get('ecommerce.currency')[$invoice->currency] }} {{ number_format((Config::get('ecommerce.vat') / 100 * $invoice->amount / 100), Config::get('ecommerce.decimal_places')) }}</td>
        </tr>-->
        <tr class="totals">
            <td></td>
            <td class="large">{{ trans('invoices.total') }}</td>
            <td align="center">{{ Config::get('ecommerce.currency_'.$invoice->currency) }} {{ number_format($invoice->amount/100, Config::get('ecommerce.decimal_places')) }}</td>
        </tr>
    </table>
	
	@if ($invoice->amount != 0)
		
		<h1>{{ trans('invoices.transactions') }}</h1>

		<table class="transactions">
			<tr>
				<th>{{ trans('invoices.transaction_date') }}</th>
				<th>{{ trans('invoices.payment_gateway') }}</th>
				<th>{{ trans('invoices.credit_card_used') }}</th>
				<th>{{ trans('invoices.amount') }}</th>
			</tr>
			
			<tr>
				<td>{{{ $invoice->created_at->format('d/m/Y') }}}</td>
				<td>{{{ $invoice->card()->provider }}}</td>
				<td>**** **** **** {{{ $invoice->card()->last_four }}}</td>
				<td>{{{ Config::get('ecommerce.currency_'.$invoice->currency) }}} {{{ number_format($invoice->amount/100, Config::get('ecommerce.decimal_places')) }}}</td>
			</tr>
	  
		</table>
	
	@endif
    
    </div>
    <div class="footer-container">
        <table cellpadding="0" width="100%" cellspacing="0" border="0" class="footer">
            <tbody>
                <tr>
                    <td>
                        <table cellpadding="0" width="100%" cellspacing="0" border="0" class="footer">
                            <tbody>
                                <tr>
                                    <td width="100%">
                                        <h1>BuzzMyBrand</h1>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table cellpadding="0" width="100%" cellspacing="0" border="0" class="footer">
                            <tbody>
                                <tr>
                                    <td width="100%"><div class="line-separator"></div></td>
                                </tr>
                            </tbody>
                        </table>
                        <table  cellpadding="0" width="100%" cellspacing="0" border="0" class="footer">
                            <tbody>
                                <tr>
                                    <td width="25%">Tel +971 563719010</td>
                                    <td width="25%">APT. 303, Iris Blue, Po<br>BOX 213982, Dubai<br>Marina, Dubai, United Arab Emirates</td>
                                    <td width="25%">support@buzzmybrand.com</td>
                                    <td width="25%"><img width="150" src="{{ storage_path() }}/images/logo-fattura.png"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>