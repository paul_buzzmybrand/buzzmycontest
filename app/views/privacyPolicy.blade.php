<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>JMTV Privacy Policy</title>
        {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap-responsive.min.css') }}
        {{ HTML::style('lib/css/CustomScrollbar.css')}}
        {{ HTML::style('lib/css/main.css')}}
    </head>
    <body style="font-family: Futura Bk">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" >
                        <div class="logo">
                            <img src="lib/img/logo.png" alt=""/>
                        </div>
                        
                        <div  class="row" style="color:#fff;padding:20px;height:395px;">
                            
                            <h2 style="color:#ffc000;margin-top: -37px;"><b>Privacy Policy</b></h2>
                            <h4><b><I>Effective: February, 4th 2014</I></b></h4>
                            <div id="contentScroll" style="color:#fff;height:354px; ">
                            <p style="width:96%; text-align:justify;">This privacy policy has been created to provide information about JMTV Pte. Ltd. 
                                    whose legal office is at 45 Mosque Street #02-03, Singapore 059523 (“JMT”, “JoinMeThere”, “Us” or “our Company”) 
                                    and its mobile application services (the “Service”) and applies to all visitors, users, and others who access the 
                                    Service (“you”, “your”). Regardless of which country you reside in or supply information from, you authorize JMT to
                                    use your information in any country where JMT operates. IF YOU DO NOT AGREE WITH THE TERMS OF THIS PRIVACY POLICY,
                                    PLEASE DO NOT USE OUR SERVICE.
                            </p>
                            <h4 style="color:#ffc000;"><b><I>Data that JMT collect</I></b></h4>
                            <p style="width:96%; text-align:justify;">We may collect your title, account registration information such as your name, address, 
                                email, date of birth, user name, password and security questions and answers. We may also collect: public profile
                                information such as your uploads and interests, information on your searches and community discussions. Many of our 
                                services let you share information with others. Remember that when you share information publicly, it may be 
                                indexable by search engines.
                            </p>
                            <p style="width:96%; text-align:justify;">
                                When you interact with us through the Service, we receive and store certain personally non-identifiable information
                                (for example, operating system, geo-location and device ID). We may store such information itself or such
                                information may be included in databases owned and maintained by JMT affiliates, agents or service providers.
                                We may share your private personal information with such service providers subject to confidentiality obligations
                                consistent with this Privacy Policy.
                            </p>
                            <p style="text-align:justify;width:96%;">
                                We also use cookies and anonymous identifiers when you interact with services we offer to our partners, such as 
                                advertising services.
                            </p>
                            <p style="text-align:justify; width:96%;">
                                By providing us with your Personal Data described above, you are acknowledging to our use of it in accordance with 
                                this Privacy Policy.
                            </p>
                            <p style="text-align:justify; width:96%;">
                                We may allow third-parties, including our authorized service providers, advertising companies and ad networks, 
                                to display advertisements on our site. These companies may use tracking technologies, such as cookies, to collect
                                information about users who view or interact with their advertisements. Anyhow We are not responsible for the
                                practices employed by any websites or services linked to or from our Service, including the information or 
                                content contained within them.
                                
                            </p>
                            <p style="text-align:justify; width:96%;">
                                We collect information when you register to use services by logging into an account that you already have with certain 
                                third-party social networking sites like Facebook and Twitter.    
                            </p>
                            <h4 style="color:#ffc000;"><b>What JMT do with your data</b></h4>
                            
                            <p style="text-align:justify; width:96%;">
                                Any of the information we collect from you may be used to improve our website and our customer service, 
                                to process transactions. Further, we will use the information you provide to send you information we think
                                you may find useful or which you have requested from us, including information about our products and 
                                services, provided you have indicated that you have not objected to being contacted for these purposes.
                                If you prefer not to receive any marketing communications from us, you can opt out at any time. We cannot 
                                assume any liability for misuse of passwords unless this misuse is our fault.
                            </p>
                            <p style="text-align:justify; width:96%;">
                                We may use your personal information to send you other information about us, the Site, our other websites, 
                                our products, sales promotions, our newsletters. If you would prefer not to receive any of this additional 
                                information as detailed in this paragraph (or any part of it) please click the ‘unsubscribe’ link in any
                                email that we send to you. We may use your email address to inform you about our services, such as letting 
                                you know about upcoming changes or improvements.
                                
                            </p>
                            
                            <p style="text-align:justify; width:96%;">
                                We may pass your details to other companies in our group. We may also pass your details to our agents and 
                                subcontractors to help us with any of our uses of your data set out in our Privacy Policy. We may exchange 
                                information with third parties for the purposes of fraud protection. We may transfer our databases containing 
                                your personal information if we sell our business or part of it. Our Service may contain advertising of
                                third parties and links to other sites or frames of other sites. Please be aware that we are not responsible 
                                for the privacy practices or content of those third parties or other sites, nor for any third party to whom
                                we transfer your data in accordance with our Privacy Policy.
                            </p>
                            
                            <p style="text-align:justify; width:96%;">
                                We may share information publicly to show trends about the general use of our services.
                            </p>
                            
                            <p style="text-align:justify; width:96%;">
                                In the event that JMT is involved in a wind up, merger, acquisition, reorganization or sale of assets, your 
                                information may be sold or transferred as part of that transaction. You will continue to own your user account. 
                                The buyer or transferee will have to honor the commitments we have made in this Privacy Policy.
                            </p>
                            
                            <p style="text-align:justify; width:96%;">
                                We will share personal information with companies, organizations or individuals if we have a good-faith belief that access,
                                use, preservation or disclosure of the information is reasonably necessary to: meet any applicable law, regulation, 
                                legal process or enforceable governmental request, enforce applicable Terms of Service, including investigation of 
                                potential violations, detect or prevent fraud or security.
                            </p>
                            <p style="text-align:justify; width:96%;">
                                We may preserve or disclose your information if we believe that it is reasonably necessary to comply with a legal
                                obligation, protect and defend the rights or property of JMT or protect against legal liability.
                            </p>
                            <p style="text-align:justify; width:96%;">
                                We may use third-party analytics tools to help us measure traffic and usage trends for the Service. We may 
                                collect information sent by your device, including the web pages you visit and other information that help us 
                                in improving our services.
                            </p>
                            <p style="text-align:justify; width:96%;">
                                In case an user win a contest launched through our Service JMT may disclose your personal data to the Partner 
                                Sponsor and the Partner Sponsor may use such Personal Data and disclose such Personal Data to third parties in 
                                order to notify you that you have won, provide you with an opportunity to claim your prize, obtain your consent
                                and release with respect to your contest entry and prize and otherwise in connection with the administration and
                                operation of the Contest and prize fulfillment.
                            </p>
                            
                            <h4 style="color:#ffc000;"><b><I>Cookies</I></b></h4>
                            
                            <p style="text-align:justify; width:96%;">
                                We may use cookies to understand and save your preferences for future visits, keep track of advertisements and
                                compile aggregate data about site traffic and site interaction so that we can offer better site experiences and
                                tools in the future. We may contract with third party service providers to assist us in better understanding our 
                                site visitors. These service providers are not permitted to use the information collected on our behalf except
                                to help us conduct and improve our business.
                            </p>
                            <p style="text-align:justify; width:96%;">
                                Cookies from specific business partners are allowed by JMT for the purpose of tailoring their adverts to your
                                tastes and browsing habits, so cookies from some of our partners may be stored on your computer when you visit
                                our site. These cookies are temporary/permanent cookies, which will be automatically deleted from your computer 
                                at the end of their lifespan (14 days to 10 years).
                            </p>
                            <p style="text-align:justify; width:96%;">
                                As with our cookies, these third party cookies do not collect any personal information. The data stored is 
                                collected at certain times, allowing these companies to see which products have been viewed most frequently 
                                but this is done completely anonymously under randomly designated user ID numbers. Therefore, this data will 
                                never be pooled with any personal information you have given JMT. Third party cookies have the sole purpose 
                                of allowing our partners to advertise products that may interest you. You hereby give your permission for your 
                                user information to be stored on cookie files. You also allow these files to remain on my computer after you 
                                have ended your browser session so they may be used upon your next visit to the website. You reserve the right 
                                to retract this permission by changing your browser setting to refuse cookies. 
                            </p>
                            
                            <h4 style="color:#ffc000;"><b><I>Variations and updates of the JMT Privacy Policy</I></b></h4>
                            <p style="text-align:justify; width:96%;">
                                JMT may modify or simply update this site Privacy Policy, in part or completely, even owing to variations of 
                                the laws and regulations that govern this matter and protect your rights. We therefore invite you to visit
                                this section often to read the most recent and updated version of this Privacy Policy. If you have questions
                                or complaints you can contact us through by e-mail at ted@joinmethere.com, or by postal mail to the address 
                                below: 
                            </p>
                            <div class="row" style="color:#fff;text-align: center;font-style:italic;text-decoration:underline;">
                                JMTV Pte. Ltd. 45 Mosque Street #02-03, Singapore 059523
                           </div>
                       </div>
                    </div>
                </div>
            </div>
            
        </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" style="color:#fff; text-align: center;">
                        JMTV Pte. Ltd. - Co. Reg. n. 201314573M 
                    </div>
                </div>
            </div>
    </div>

     <script type="text/javascript" src="packages/bootstrap/js/jquery.js"></script>
        <script type="text/javascript" src="packages/bootstrap/js/bootstrap.js"></script>
        <script type="text/javascript" src="packages/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="lib/js/CustomScrollbar.js"></script> 
        <script>
            (function($){
                $(window).load(function(){
                    $("#contentScroll").mCustomScrollbar({
                        scrollButtons:{
                            enable:true
                        }
                    });

                });
            })(jQuery);

        </script>
</body>
</html>
