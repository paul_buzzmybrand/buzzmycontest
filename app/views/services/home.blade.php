<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/common.css') }}
    </head>
    <body>
        <div class="home">
            <div class="row">
                <div class="col-md-12">
                    <h1>Services</h1>
                    <p class="lead home_content" >In this screen the customer must register himself in order to have access to the services of JMT</p>
                    
                    <div class="row">
                        <div class="col-md-4">
                            
                        </div>
                        <div class="col-md-4">
                            <p class="lead">
                                <a href="{{URL::to('services/user')}}" class="btn btn-lg btn-primary">Customer&nbsp;<span class="glyphicon glyphicon-user"></span></a>
                            </p>
                        </div>
                        
                        <div class="col-md-4">
                            
                        </div>
                    </div>
                    <div class="home_footer">
                        <div class="inner">
                          <p>© 2014 - Graphics, layouts and Contents are exclusive property of  <a href="#">jmtv</a> Pte. Ltd. - Co. Reg. n. 201314573M.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>