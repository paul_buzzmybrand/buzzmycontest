<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/invite_popup.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
        @yield('head')
    </head>
    <body>
    	<div class="container">    	
            <h2><b>{{Lang::get('tab.invite_success_message')}}</b></h2>
			<button class="btn btn-lg btn-inverse btn-block" onclick="javascript: window.close();">{{Lang::get("tab.close_window")}}</button>
	    </div>
	{{ HTML::script('website_public/js/bootstrap.min.js') }}

</body>
</html>