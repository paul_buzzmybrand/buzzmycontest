<!doctype html>
@include('Mobile_Detect')
<?php


//var_dump("og_description: ".$og_description);
$detect = new Mobile_Detect;
if ( $detect->isMobile() ) {

if ( ( ( strpos($_SERVER['HTTP_USER_AGENT'], "FBAN/FBIOS") > -1 
					&& (strpos($_SERVER['HTTP_USER_AGENT'], "iPhone") > -1 || strpos($_SERVER['HTTP_USER_AGENT'], "iPod") > -1) )
		
		
		|| ( strpos($_SERVER['HTTP_USER_AGENT'], "FB_IAB") > -1 && strpos($_SERVER['HTTP_USER_AGENT'], "Android") > -1 ) )

		||

		 ( strpos($_SERVER['HTTP_USER_AGENT'], "FBAN/MessengerForiOS") > -1 )
		
		
		) {

?>		
	@include('tab.index-m-fb-device')

<?php
} else {
?>
@include('tab.index-m')
<?php 
}

}
else {
$template = Template::find($id);
?>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/contest.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
        {{ HTML::script('website_public/js/jquery.validate.min.js') }}
        {{ HTML::script('website_public/home_page/js/jquery.parallax-scroll.js') }}
        {{ HTML::script('website_public/js/jquery-scrolltofixed.js') }}
		<script>
			
			$(document).ready(function(){


                $('.header').scrollToFixed();

				var mySelect = document.getElementById("mySelect");
				if (typeof(mySelect) != 'undefined' && mySelect != null)
				{
					
					
					mySelect.onchange = function(){
				
						if ( document.getElementById("mySelect").value == 1 ) {
							change_order('most_viral');
						} else if ( document.getElementById("mySelect").value == 2 ) {
							change_order('most_recent');
						}
						
					}
					
				}
				
				
			});



            function apri(url) {
                newin = window.open(url,'titolo','scrollbars=no,resizable=yes, width=500,height=300,status=no,location=no,toolbar=no');
            }

            function likeFunction(id_post)
            {
                FB.getLoginStatus(function(response) {
                    if (response.status === 'connected') {


                        FB.api('/'+id_post+'/likes', 'post');
                        alert('Like done!');
                    }
                    else {
                        FB.login(function(){
                            FB.api('/'+id_post+'/likes', 'post');
                            alert('Like done!');
                        });
                    }
                });
            }

            
            var page = 1;            
            var order = 'most_viral';
            var contest_type = '2';
            //var contest_id = '{{ $id }}';
			var template = '{{ $id }}';
            var loading = false;
            var end = false;
            function change_order( new_order )
            {
                if( order != new_order)
                {

                    $("#li_"+new_order).addClass("active");
                    $("#li_"+order).removeClass("active");

                    page = 1;
                    end = false;
                    order = new_order;
                    $('.submissions_grid').html('');
                    FB.XFBML.parse(document.getElementById('id_grid'));
                    get_submissions();
                }

            }
            function get_submissions()
            {
                $('#loadingbar').css("display","block");
                loading = true;

                if ( $(window).width() <= 820 ) {
                    tab = "true";
                } else {
                    tab = "false";
                }

                //Ajax Post - CREARE FUNZIONE CHE DA IN OUTPUT HTML CON 4 IMMAGINI CHE CAMBIANO A SECONDA DEL TEMPLATE
                $.post('/tab/getTemplateSubmissions?tab=' + tab, {contest_type: contest_type, page: page, order: order, template: template } , function(results){
                    $('.submissions_grid').append(results);
                    page++;
                    loading = false;
                    FB.XFBML.parse(document.getElementById('id_grid'));
                    $('#loadingbar').css("display","none");
                });

            }


            $(window).scroll(function(){
                if((($(window).scrollTop()+$(window).height())+10)>=$(document).height()){

                    if(loading == false && end == false){
                        get_submissions();
                    }
                }
            });

            
            function fullscreen_popup(url, name)
            {
                var tab = "";
                if ( $(window).width() <= 820 ) {
                    tab = "true";
                } else {
                    tab = "false";
                }

                //Ajax Post
                $.post('/tab/getSubmission', { url: url, contest_type: 2, tab: tab } , function(results){

                    $('.fullscreen_popup_title').html(name);
                    $("#fullscreen_popup").css("display", 'block' );
                    $("#fullscreen_popup_content").css("display", 'block' );
                    $("#submissione_popup_content").html(results);
                });
            }

            function close_fullscreen_popup()
            {
                $("#fullscreen_popup").css("display", 'none' );
                $("#fullscreen_popup_content").css("display", 'none' );
                $("#submissione_popup_content").html('');
            }
        </script>
		
		<title>CONTEST DEMO</title>
		
		 
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
		
		
    </head>
    <body>
	
		<div id="divId" style="display:none;"></div>

    	<div id="fb-root"></div>
		<script>
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3&appId=566800206740950";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
		
    	<div id="fullscreen_popup">
			<div id="fullscreen_popup_content"> 
				<a href="javascript:void(0)" onClick="close_fullscreen_popup();" class="cancel">&times;</a>
				<div class="fullscreen_popup_title"></div>
				<div id="submissione_popup_content"></div> 
			</div>
		</div>


        <div class="parent">
            <div class="header_bar parallax__layer" data-parallax='{"y": 100}' style="background-image:url('/templates/{{$template->contest_flyer}}')"></div>
			<div class="header" style="font-family: '{{ $template->font }}';">
            CONTEST DEMO
            </div>
        </div>




        <div class="contest parallax__layer">


            <div class="header_contest">

                
                    <div class="contentDescription">
                        <div class="facebook_tab_contest_description">
							<br><br>
                            <span style="color: {{$template->subtitles_color}};font-family: '{{ $template->font }}';">

                                <b>{{ trans('tab.brief') }}</b>

                            </span><br>
                            <div class="paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div><br>
                            <span style="color: {{$template->subtitles_color}}; font-family: '{{ $template->font }}';"><b>{{ trans('tab.date') }}</b></span><br>
							
							<?php $datetime = new DateTime(); ?>
							<?php $today = $datetime->format("Y-m-d H:i:s"); ?>
							<?php $tomorrow = $datetime->modify('+2 day')->format("Y-m-d H:i:s"); ?>
							
                            <div class="paragraph">{{ trans('tab.from') }} {{tab_HomeController::format_contest_date('1', $today)}} {{ trans('tab.to') }} {{tab_HomeController::format_contest_date('1', $tomorrow) }} {{explode('-', $tomorrow)[0]}}</div><br>


                            


                                <div class="paragraph">

                                    <br>
                                    <span style="color: {{$template->subtitles_color}}; font-family: '{{ $template->font }}';"><b>{{ trans('tab.prizes') }}</b></span>
                                    <br> 
										<span>1<span>st</span> {{ trans('tab.prize') }}: title prize, description prize</span><br>
										<span>2<span>nd</span> {{ trans('tab.prize') }}: title prize, description prize</span><br>
										<span>3<span>rd</span> {{ trans('tab.prize') }}: title prize, description prize</span><br>                                   

                                </div>



                            


                            
                                <br><span style="color: {{$template->subtitles_color}}; font-family: '{{ $template->font }}';"><b>{{ trans('tab.how_to_win') }}</b></span><br>
                                <div class="paragraph">Contest winners will be selected through the social buzz, an algorythm that calculates the actions on the social networks where the contest is launched. Make your entry the most popular and win!</div>
                                <br>
                                <br>
                            

                            <div class="paragraph">
                                <p style="text-align: right;">
                                    
                                    <a style="text-decoration: underline;" target="_blank" href="#">{{ trans('tab.terms_and_cond') }}</a><br>
                                    
                                </p>



                                
                                    <span>{{ trans('tab.visit_website') }} <a href="http://www.buzzmybrand.com" target="_blank">http://www.buzzmybrand.com</a></span><br>
                                


                                
                                
                                <span>{{ trans('tab.like_the_page') }} <a href="https://www.facebook.com/socialbuzzmybrand" target="_blank">BuzzMyBrand</a></span>
                                <div style="text-align:left;margin-top: 10px;margin-bottom: 10px;">
                                    <div class="fb-like" data-href="https://www.facebook.com/socialbuzzmybrand" style="margin:auto;"
                                         data-width="100" data-layout="standard" data-action="like" data-show-faces="false" data-share="true">
                                    </div>
                                </div>

                                <div style="text-align:left;margin-top: 10px;margin-bottom: 10px;">
                                    <a href="https://twitter.com/buzzmybrand_" class="twitter-follow-button" data-show-count="true" data-dnt="true">Follow BuzzMyBrand</a>
                                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                </div>

                                <div style="text-align:left;margin-top: 10px;margin-bottom: 10px;">
									<a href="https://www.instagram.com/buzzmybrand/" target="_blank"><img src="/website_public/tab/Instagram_icon.png"> /buzzmybrand</a> </span><br>
								</div>
                                
                            </div>






                        </div>
                    </div>
                
                <br>
            </div>


            <div class="buttons">
                <button class="btn btn-lg btn-block join_now_contest" style="background-color: {{$template->subtitles_color}}" disabled="disabled">{{ trans('tab.join_now') }}</button>
                <button class="btn btn-lg btn-block invite_contest" style="background-color: {{$template->subtitles_color}}" disabled="disabled">{{ trans('tab.invite') }}</button>

            </div>




        </div>

        <div class="submissions parallax__layer">

            <a id="entries" style="color: {{$template->subtitles_color}}"></a>
            
            <div class="categories_submissions" style="width:100%">
                <div class="form-group" >
                    <div style="float:right;">
                        <select id="mySelect" class="form-control" style="color: #999999">
                            <option value="1" selected="selected">{{ trans('tab.most_viral') }}</option>
                            <option value="2">{{ trans('tab.most_recent') }}	</option>
                        </select>
                    </div>
                    <div style="height: 46px; line-height: 46px; vertical-align: middle; color: #999999; float:right;">{{trans('tab.ordered') }}&nbsp;&nbsp;</div>

                </div>
            </div>
            
            <div class="hidden_clear"></div>
            <div id="id_grid" class="submissions_grid">
                <?php // Submissions load by ajax ?>
            </div>
            <div id="loadingbar">
                <img alt="loading" style="border:none" src="{{asset('images/loader.gif') }}" />
            </div>

        </div>



    </body>

<!--
<div style="background-color: #3d3d3f; width: 100%; height: 60px;">

	<div class="container_footer" >

		<div class="logo_footer">
		</div>
		<div style="float: left; color: #ffffff; margin-left: 40px;">
			<p style="line-height: 60px; margin: 0;">co.reg.n.: 02153931205 <span style="color: #c59f18;">|</span> <a style="color: #ffffff"href="mailto:info@asmisura.com">mail</a></p>
			
		</div>
		<div  style="float: right;">
			<a style="margin: 10px 0;" class="youtube_link_footer" href="https://www.youtube.com/user/admisuraagency" target="_blank"></a>
		</div>
		<div  style="float: right; margin-right: 10px;">
			<a style="margin: 10px 0;" class="gplus_link_footer" href="https://plus.google.com/109132852807748986774" target="_blank"></a>
		</div>
		<div  style="float: right; margin-right: 10px;">
			<a style="margin: 10px 0;" class="linkedin_link_footer" href="https://www.linkedin.com/in/admisura" target="_blank"></a>
		</div>
		<div  style="float: right; margin-right: 10px;">
			<a style="margin: 10px 0;" class="twitter_link_footer" href="https://twitter.com/admisura" target="_blank"></a>
		</div>
		<div  style="float: right; margin-right: 10px;">
			<a style="margin: 10px 0;" class="facebook_link_footer" href="https://www.facebook.com/Admisura" target="_blank"></a>
		</div>
	
	</div>
	
</div>
-->


</html>
<?php
}
?>

