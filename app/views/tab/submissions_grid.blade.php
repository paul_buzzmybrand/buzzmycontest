@include('Mobile_Detect')
<?php 
$detect = new Mobile_Detect;
if ( $detect->isMobile() ) :
?>
@include('tab.submissions_grid-m')
<?php 
else:
?>
@include('tab.submissions_grid-desktop')
<?php
endif;
?>