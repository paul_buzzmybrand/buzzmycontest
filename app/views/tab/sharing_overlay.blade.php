<div id="sharing-overlay">
  <div id="sharing-close"><i class="glyphicon glyphicon-remove"></i></div>
  <div class="social-buttons">

  <ul>
    <li><a href="http://facebook.com/sharer/sharer.php?u=http://bmb.buzzmybrand.co/{{ urlencode($contest->contest_route) }}", "{{ trans('tag.share') }}"" class="btn btn-lg btn-inverse btn-block btn-fb center-block" rel="nofollow">Facebook</a></li>
    <li><a href="http://twitter.com/share?text={{ trans('tab.check_out_contest') }}&url=http://bmb.buzzmybrand.co/{{ urlencode($contest->contest_route) }}", "{{ trans('tag.share') }}" class="btn btn-lg btn-inverse btn-block btn-twitter center-block" rel="nofollow">Twitter</a></li>
    @if($detect->isMobile())
    <li><a href="whatsapp://send?text={{ trans('tab.check_out_contest') . ' http://bmb.buzzmybrand.co/'. urlencode($contest->contest_route) }}" data-text="{{ trans('tab.check_out_this_contest') }}" data-href class="wa_btn wa_btn_m btn btn-lg btn-inverse btn-block btn-whatsapp center-block" target="_top" onclick="window.parent.null">WhatsApp</a></li>
    @endif
  </ul>

</div>

<script>



  function shareFb() {
  
    $.post('/tab/computeSharingScore', {'destination': 'facebook'}).success(function(r) {
      window.open("http://facebook.com/sharer/sharer.php?u=http://bmb.buzzmybrand.co/{{ urlencode($contest->contest_route) }}", "{{ trans('tag.share') }}", "_blank");
    });
  }
  function shareTw() {    
    $.post('/tab/computeSharingScore', {'destination': 'twitter'}).success(function(r) {
      window.open("http://twitter.com/share?text={{ trans('tab.check_out_this_contest') }}&url=http://bmb.buzzmybrand.co/{{ urlencode($contest->contest_route) }}", "{{ trans('tag.share') }}", "_blank");
    });     
  }

  function shareWa() {
    $.post('/tab/computeSharingScore', {'destination': 'whatsapp'}).success(function(r) {  });
  }
  function enableSharingOverlay()
  {
    // Share button click handling
    $('.share_now_contest').bind('click', function(e) {
        $('#sharing-overlay').show();
    });

    $('#sharing-close').bind('click', function(e) {
        $('#sharing-overlay').hide();
    });
    $('.wa_btn').bind('click', function(e) {
        shareWa();
    });
  }
  
</script><script>
  function shareFb() {
    $.post('/tab/computeSharingScore', {'destination': 'facebook'}).success(function(r) {
      window.open("http://facebook.com/sharer/sharer.php?u=http://bmb.buzzmybrand.co/{{ urlencode($contest->contest_route) }}", "{{ trans('tag.share') }}");
    });
  }
  function shareTw() {    
    $.post('/tab/computeSharingScore', {'destination': 'twitter'}).success(function(r) {
      window.open("http://twitter.com/share?text={{ trans('tab.check_out_this_contest') }}&url=http://bmb.buzzmybrand.co/{{ urlencode($contest->contest_route) }}", "{{ trans('tag.share') }}");
    });     
  }

  function shareWa() {
    $.post('/tab/computeSharingScore', {'destination': 'whatsapp'}).success(function(r) {  });
  }
  function enableSharingOverlay()
  {
    // Share button click handling
    $('.share_now_contest').bind('click', function(e) {
        $('#sharing-overlay').show();
    });

    $('#sharing-close').bind('click', function(e) {
        $('#sharing-overlay').hide();
    });
    $('.wa_btn').bind('click', function(e) {
        shareWa();
    });
  }
  
</script>