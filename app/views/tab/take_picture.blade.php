@extends('layouts.contest_popup')
@section('head')
	
	{{ HTML::script('website_public/js/swfobject.js') }}
	{{ HTML::script('website_public/js/scriptcam.js') }}

		<script language="JavaScript"> 
			$(document).ready(function() {
				$("#webcam").scriptcam({
					showMicrophoneErrors:false,
					onError:onError,
					disableHardwareAcceleration:1,
					cornerColor:'e3e5e2',
					onWebcamReady:onWebcamReady,
					onPictureAsBase64:base64_tofield_and_image,
					cornerRadius: 0,
//					width: 640,
//					height: 480,
					width: 500,
					height: 350,

				});
			});

			function upload_snapshot() {

				var picture = $('#image').attr('src');
				picture = picture.split('data:image/png;base64,');

				$('#input_photo').val(picture[1]);
				
				document.getElementById("uploadForm").submit();
			};

			function base64_tofield() {
				$('#photo_input').val($.scriptcam.getFrameAsBase64());
			};
			function base64_toimage() {

				$('#image').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64());
				$('#image').data("shot", 'true');

				var title = $('#inputTitleTakePicture').val();
				if( title != '' ){
    				$('#SubmitTakePicture').addClass('SubmitTakePictureActive');
    				change_onclick('enable');
    			}
    			else{
    				change_onclick('disable');
    				$('#SubmitTakePicture').removeClass('SubmitTakePictureActive');
    			}
			};
			/*function base64_toimage() {

				$('#image').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64());
				$('#image').data("shot", 'true');
				var title = $('#input_title_small').val();
				if( title != '' )
    				$('#uploadSnapshot').addClass('uploadSnapshotActive');
    			else
    				$('#uploadSnapshot').removeClass('uploadSnapshotActive');
			};
			*/
			function base64_tofield_and_image(b64) {
				$('#formfield').val(b64);
				$('#image').attr("src","data:image/png;base64,"+b64);
			};
			function changeCamera() {
				$.scriptcam.changeCamera($('#cameraNames').val());
			}
			function onError(errorId,errorMsg) {
				$( "#btn1" ).attr( "disabled", true );
				$( "#snapshotToImage" ).attr( "disabled", true );
				alert(errorMsg);
			}			
			function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
				$.each(cameraNames, function(index, text) {
					$('#cameraNames').append( $('<option></option>').val(index).html(text) )
				}); 
				$('#cameraNames').val(camera);
			}
		</script> 
@stop
@section('step') step4 @stop
@section('button')
	<div class="btn_choose" onClick="javascript:history.back()"></div>
@stop




@section('box_class')
take_picture
@stop
@section('box')

	<!-- ////////////// CUSTOMIZZAZIONE ///////////////////-->
	<script>

		var num_photo = <?php if ( isset($photos) ) { echo count($photos) + 1; } else { echo 0; } ?>;
		var id_photo = num_photo - 1;
		
		$(document).ready(function(){
		
			$('#prev_photo').click(function( event ){
				event.preventDefault();
				id_photo = Math.abs((id_photo - 1) % num_photo);
				id_photo_to_hide = Math.abs((id_photo - 1) % num_photo);
				$('#photo_' + id_photo).css('display', 'block');
				$('#photo_' + id_photo_to_hide).css('display', 'none');
			});
			
			$('#succ_photo').click(function( event ){
				event.preventDefault();
				id_photo =  Math.abs((id_photo + 1) % num_photo);
				id_photo_to_hide = Math.abs((id_photo - 1) % num_photo);
				$('#photo_' + id_photo).css('display', 'block');
				$('#photo_' + id_photo_to_hide).css('display', 'none');
			});
	  

		
		});

	</script>

	<?php if ( isset($photos) ) { $counter = 0; ?>
	<div style="margin: 0 auto; text-align: center; font-size: 30px;"><a id="prev_photo"><</a>&nbsp;<a id="succ_photo">></a></div>
	<?php foreach( $photos as $p) {  ?>
	<div id="{{'photo_' . $counter}}" class="upload_submission" style="display: none;">	
		<img src="{{asset('/images/photos/' . $p->image)}}" width="width" height="328" />
		<div style="width: 376px; margin: 5px auto;">
			{{$p->name;}}
		</div>
	</div>
	<?php $counter++; } ?>
	<?php } ?>


	<?php if ( isset($photos) ) { $stringId = 'id="photo_' . ($counter) . '"'; } else { $stringId = ""; } ?>
	<!-- ////////////// CUSTOMIZZAZIONE ///////////////////-->


	<div {{$stringId}}>



	<!-- ////////////// CUSTOMIZZAZIONE ///////////////////-->
	<div class="webcamImage">
		<div id="webcam"></div>
		
	</div>
	<script>
	function shoot()
	{
		//anteprima
		base64_toimage();
		//cambio pulsante shoot
		$('#shoot').attr("onclick","shoot_again()");
		$('#shoot').addClass('shoot_again');
		//nascondo il div live_image
		$('.webcamImage').css('display','none');

	}
	function shoot_again()
	{
		$('#shoot').attr("onclick","shoot()");
		$('#shoot').removeClass('shoot_again');
		$('.webcamImage').css('display','block');
	}
	</script>
	<!-- ////////////// CUSTOMIZZAZIONE ///////////////////-->


	<div style="width:500px;height:350px;margin:auto;background-image:url('{{asset('website_public/contest_popup/preview.png') }}')">
		<p><img id="image" data-shot="false" style="width:500px;height:350px;"/></p>
	</div>

	<div style="width: 503px; height: 53px; margin: 20px auto 0px auto	;">
	
		{{ Form::open(array('action' => array('tab_HomeController@savePhoto'), 'method' => 'post', 'id' => 'uploadForm')) }}
			
			<div style="width: 503px; height: 53px;">
			<div style="clear: both;"></div>
			@if ((($item != '1') && (!$contest->max_entries)) || ($item == '2'))
				
				<textarea id="inputTitleTakePicture" name="title" style="float: left; width: 440px; height: 53px;border: solid 1px grey; font-size: 10px;" maxlength="80" placeholder="{{Lang::get('widget.creation_step.placeholder')}} '{{$contest->hashtag}}' {{Lang::get('widget.creation_step.placeholder2')}} <?php if ($contest->id == "259") { ?> Se scalerai la classifica arrivando tra i primi 20, la tua foto potrà essere scelta da una giuria e potrai vincere uno dei 5 buoni da 350 euro da spendere online <?php } ?>" maxlength="40"></textarea>
			
			@else
				<div style="float: left; width: 440px; height: 53px;border: solid 1px grey; font-size: 12px;">Ti ricordiamo che questa foto serve come prova per partecipare e non verrà pubblicata sul tuo account Facebook né sulla pagina Facebook di IoRompoLeScatole.</div>
				<input id="inputTitleTakePicture" name='title' type='hidden' value="Cartone smaltito">
			@endif
			
			<div id="shoot" style="float: left; width: 53px; height: 53px; margin-left: 10px;" onclick="shoot();"></div>
			<div style="clear: both;"></div>
			</div>
			<input id="id_contest" name='id_contest' type='hidden' value="{{ $contest_id }}">
			<input id="input_photo" name='photo' type='hidden' value="">
		{{ Form::close() }}
	
	</div>
	<div style="width: 503px; height: 21px; margin: 5px auto; font-size: 10px;">
		@if ((($item != '1') && (!$contest->max_entries)) || ($item == '2'))
			{{Lang::get('widget.creation_step.policy')}}
		@endif
	</div>
	<div id="SubmitTakePicture" class="SubmitTakePicture2" style="width: 505px; height: 41px; margin: 0 auto;">
	</div>

</div>
<div style="width:701px;height:442px; display: none;">

	<div style="width:512px;height:415px;margin:auto;background-image:url('{{asset('website_public/contest_popup/preview.png') }}')">
		<p><img id="image" data-shot="false" style="width:512px;height:415px;"/></p>
	</div>
	
	<div class="hidden_clear"></div>
	<div class="live_button">
		<div id="shoot" onclick="shoot();"></div>
		<form id="uploadForm" action="{{ action('tab_HomeController@savePhoto') }}" method="POST">
			<div id="submissionTitleTakePicture">
				<input type="text" id="inputTitleTakePicture" maxlength="50" name="title" placeholder="INSERT TITLE" autocomplete="off" />
			</div>
			<div id="SubmitTakePicture" onClick=""></div>
			<input id="id_contest" name='id_contest' type='hidden' value="{{ $contest_id }}">
			<input id="input_photo" name='photo' type='hidden' value="">
		</form>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#inputTitleTakePicture').keyup(function(){
			var title = $('#inputTitleTakePicture').val();
    		if( $('#image').data("shot") == 'true' && title != ''){
    			$('#SubmitTakePicture').addClass('SubmitTakePictureActive');
    			change_onclick('enable');
    		}
    		else{
    			change_onclick('disable');
    			$('#SubmitTakePicture').removeClass('SubmitTakePictureActive');
    		}
    		});
			
		$("#SubmitTakePicture").click( function()
		   {
			 var title = $('#inputTitleTakePicture').val();
			 if( $('#image').data("shot") != 'true' || title == '')
			 {
				 //alert('Scatta la foto e inserisci il titolo!');
				 alert('{{ trans("tab.shoot_pic_insert_title")}}');
			 }
			 
		   }
		);
	});
	function change_onclick( action )
    { 
        if( action == 'disable')
        {
            $('#SubmitTakePicture').attr("onclick","");
        }
        else if( action == 'enable')
        {
            $('#SubmitTakePicture').attr("onclick","upload_snapshot();");
        }
    }
</script>
@stop