<!doctype html>
<html lang="en">
    <head>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta charset="UTF-8">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        {{ HTML::style('website_public/css-m/contest.css') }}
        {{ HTML::style('lib/css/CustomScrollbar.css')}}



		<title>{{$contest->name}}</title>
		<meta property="og:url" content="{{$contestRoute}}" />
		<meta property="og:title" content="{{$contest->name}}" />
		<meta property="og:description" content="{{$og_description}}" />
		<meta property="og:site_name" content="{{$contest->name}}" />
		<?php $contest_image = explode('.', $contest->image); ?>
		<meta property="og:image" content="{{asset('/templates/'.$contest->template->contest_flyer)}}" />



    <style media="screen">
      .hidden {display: none}
    </style>


		<script>

			function likeFunction(id_post)
			{
				FB.getLoginStatus(function(response) {
				  if (response.status === 'connected') {


						FB.api('/'+id_post+'/likes', 'post');
						alert('Like done!');
				  }
				  else {
					FB.login(function(){
						 FB.api('/'+id_post+'/likes', 'post');
						 alert('Like done!');
						});
				  }
				});
			}

      function checkAllLikes()
      {
          $('.fb-like-custom').each(function(i, obj) {
              checkLike(obj);
          });
      }

      /**
       * Checks if the user has already liked the object.
       * If he did, replace the button and disable the like action.
       */
      function checkLike(el)
      {
          var id_post = $(el).data('post-id');
          FB.getLoginStatus(function(response) {
          if (response.status === 'connected') {
            FB.api('/me?fields=id', function(meResponse) {
              var uid = meResponse.id;
              FB.api('/'+id_post+'/likes', function(response) {
                  $(response).each(function(obj) {
                      if(obj.id == uid) {
                          setLiked(el);
                      }
                  });
              });
            });
          }
        });
      }

      function setLiked(el)
      {
          $(el).html('<img style="width: 100%;" src="/website_public/tab-m/liked-button.png" />');
      }


		</script>


	<?php if (isset($contest->ua_google_code)) { ?>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', '<?php echo $contest->ua_google_code; ?>', 'auto');
		  ga('send', 'pageview');

		</script>
		<?php }  ?>
    </head>
    <body>
		<?php $type_id = $contest->type_id; ?>
    	<?php App::setlocale($contest->location->language); ?>
		<?php $url_invite = "invite/".$contest->id; ?>
    	<div id="fb-root"></div>
		<script>
		(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3&appId=566800206740950";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>
    	<div id="fullscreen_popup">
			<div id="fullscreen_popup_content">
				<a href="javascript:void(0)" onClick="close_fullscreen_popup();" class="cancel">&times;</a>
				<div class="fullscreen_popup_title">PROVA</div>
				<div id="submissione_popup_content"></div>
			</div>
		</div>
		<?php $contest_image = explode('.', $contest->image); ?>


        <nav class="navbar navbar-default header_bar" id="top-header" >
		  <div class="container-fluid">
			<div class="" style="padding: 5% 0">
			   <!-- <img alt="{{$contest->sponsor_log}}" class="logo1 img-responsive" src="{{asset('images/events/'.$contest->sponsor_logo) }}" height="50">  //
			   <img alt="{{$contest->sponsor_logo}}" class="logo1" src="{{asset('images/events/'.$contest->sponsor_logo) }}" >-->

			  <h1 style="font-family: '{{ $contest->template->font }}';" class="contest_name">{{$contest->name}}</h1>
			</div>
		  </div>

		</nav>


        <div class="container_fluid contest_container">
		<!--
			<?php if ( !$contest->template_id ) { ?>
				<div class="left_contest"><img src="images/events/{{$contest_image[0].'_sq.jpg'}}" style="width: 100%" /></div>
			<?php } else { ?>
				<div class="left_contest"><img src="templates/squared/{{$template->background_image}}" style="width: 100%" /></div>
			<?php } ?>
		-->

			<div class="left_contest">

			@if ($contest->use_template == 1)
				<img src="templates/{{$contest->template->contest_flyer}}" style="width: 100%" />
			@else
				<img src="images/events/{{$contest->image_minisite_bg}}" style="width: 100%" />
			@endif
			</div>


			<section id="mainblock" class="container header_contest">
				<div class="row">

					<div class="col-xs-12 col-sm-12 right_contest" >
						@if( $contest->concept != '' && $contest->concept != null)
						<div class="p_contest" style="text-align:justify; /*width: 96%;*/ padding-top: 5px;">
							<?php if ( $contest->id == "259") { ?>
							<h3 class="contest_block_title" style="color: {{$contest->template->subtitles_color}};font-family: '{{ $contest->template->font }}';">Non sentitevi obbligati, sentitevi utili</h3>
							<span style="text-align:justify;">Rompere le scatole è utile e premia.<br>Utile perché rompere, ripulire da nastri e punti metallici il cartone e poi piegarlo costituisce il 90% di una perfetta raccolta differenziata di carta e cartone. Il poco che resta da fare consiste nel conferire il cartone (e la carta naturalmente) nell'apposito contenitore condominiale o stradale.<br><br> E poi, rompere le scatole premia: ci sono <strong>240 premi immediati</strong> (instant win) e <strong>5 super premi</strong> finali per chi rompe dal 18 dicembre 2015 al 18 aprile 2016.</span><br />
							<?php } ?>
							<h3 class="contest_block_title" style="color: {{$contest->template->subtitles_color}};font-family: '{{ $contest->template->font }}';">
							<?php if ($contest->id == '259') { ?>
								Partecipa subito
							<?php } else { ?>
								{{ trans('tab.brief') }}
							<?php } ?>
							</h3>
							<span style="color: {{$contest->font_color}}; text-align:justify;">{{ $contest->concept}}</span><br />
							<h3 class="contest_block_title" style="color: {{$contest->template->subtitles_color}};font-family: '{{ $contest->template->font }}';">{{ trans('tab.date') }}</h3>

							<span style="color: {{$contest->font_color}};">{{ trans('tab.from') }} {{tab_HomeController::format_contest_date($contest->location_id, $contest->start_time)}} {{ trans('tab.to') }} {{tab_HomeController::format_contest_date($contest->location_id, $contest->end_time) }} {{explode('-', $contest->end_time)[0]}}</span><br>
							<!--@if (($contest->score_start_time != null) && ($contest->score_end_time != null))
							{{ trans('tab.scoring_period') }}: {{ trans('tab.from') }} {{tab_HomeController::format_contest_date($contest->location_id,$contest->score_start_time)}} {{ trans('tab.to') }} {{ tab_HomeController::format_contest_date($contest->location_id, $contest->score_end_time) }}<br>
							@endif-->




							<?php if ( count($contest->rewards) ) { ?>




								<?php if ($contest->id == '259') { ?>
									<!--
									<h3 class="contest_block_title" style="color: {{$contest->title_color}}">{{ trans('tab.prizes') }}</h3>
									<span style="color: {{$contest->font_color}}">Premi instant win: n. 240 Buoni Acquisto da € 20,00 cad. da spendere online</span><br>
									<span style="color: {{$contest->font_color}}">Premi finali: n. 5 buoni acquisto da € 350,00 cad. da spendere online</span>
									<br>
									-->
								<?php } else { ?>


									<h3 class="contest_block_title" style="color: {{$contest->template->subtitles_color}};font-family: '{{ $contest->template->font }}';">{{ trans('tab.prizes') }}</h3>

									<?php if ( count($contest->rewards) == 1 ) { ?>
										@foreach ($contest->rewards as $reward)
										<span style="color: {{$contest->font_color}}">{{$reward->title}}
										<?php if ($reward->description)  { ?>
											, {{$reward->description}}</span><br>
										<?php } else { ?>
											</span><br>
										<?php } ?>
										@endforeach
									<?php } else { ?>


										<?php $index = 0; ?>
										@foreach ($contest->rewards as $reward)
											<?php $index = $index+1; $final = '';
											if ( $contest->location_id == 1 ) {
												if ( $index == 1 ) {
													$final = 'st';
												} else if ( $index == 2 ) {
													$final = 'nd';
												} else if ( $index == 3 ) {
													$final = 'rd';
												} else {
													$final = 'th';
												}
											} else if ( $contest->location_id == 2 )  {
												$final = 'o';
											}?>
											<span style="color: {{$contest->font_color}}">{{$index}}<span style="vertical-align:super; font-size:85%;">{{$final}}</span> {{ trans('tab.prize') }}: {{$reward->title}}
											<?php if ($reward->description)  { ?>
											, {{$reward->description}}</span><br>
											<?php } else { ?>
											</span><br>
											<?php } ?>
										@endforeach




									<?php } ?>




								<?php } ?>


							<?php } ?>











							@if( $contest->how_to_win != '' || $contest->how_to_win != null)
								<h3 class="contest_block_title" style="color: {{$contest->template->subtitles_color}};font-family: '{{ $contest->template->font }}';">{{ trans('tab.how_to_win') }}</h3>
								<span style="color: {{$contest->font_color}};">{{$contest->how_to_win}}</span>

								<div class="facebook_tab_contest_t_e_c" style="margin-right:0px; text-align:right;">
									<?php if ( $contest->terms_cond ) { ?>
										<a style=" color: {{$contest->font_color}}; text-decoration: underline;" href="{{asset('/view_t_c/' . $contest->id)}}">{{ trans('tab.terms_and_cond') }}</a>
									<?php } else { ?>
										<a style=" color: {{$contest->font_color}}; text-decoration: underline;" href="{{asset('/admin/get-terms-' . $contest->location->language . '/' . $contest->term_cond_company_sponsoring)}}" >{{ trans('tab.terms_and_cond') }}</a>
									<?php } ?>
								</div>

							@endif
							<br>

							<!--
							@if (!empty($contest->company->company_link))
								<span style="color: {{$contest->font_color}}">{{ trans('tab.visit_website') }} <a href="http://{{ $contest->company->company_link }}" target="_blank">{{ $contest->company->company_link }}</a></span><br>
							@endif
							-->

							<!--<b><span style="color: #296098">{{ trans('tab.fb_pages') }}</b></span><br>-->
							<?php foreach($contest->fbPages as $page): ?>
								@if ($page->fb_pagelike_value != 0)
								<span style="color: {{$contest->font_color}}">{{ trans('tab.points_if_pagelike1') }} <strong> {{$page->fb_pagelike_value}} </strong> {{ trans('tab.points_if_pagelike2') }} <a href="{{ $page->fb_page_link}}">{{ $page->fb_page_name}}</a></span><br>
								@endif

								<span style="color: {{$contest->font_color}}">{{ trans('tab.like_the_page') }} <a href="{{ $page->fb_page_link}}" target="_blank">{{ $page->fb_page_name}}</a></span>
								<div style="text-align:center;margin-top: 25px;">
									<div class="fb-like" data-href="{{$page->fb_page_link}}" style="margin-top: 10px;margin-bottom: 10px; "
										data-layout="standard" data-action="like" data-show-faces="false" data-share="false" data-width="100">
									</div>
								</div>
							<?php endforeach; ?>

							<?php foreach($contest->twPages as $page): ?>

									<div style="text-align:center;margin-top: 25px;">
										<a href="{{ $page->tw_page_link}}" class="twitter-follow-button" data-show-count="true" data-dnt="true">Follow {{ $page->tw_page_name}}</a>
										<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
									</div>
								<?php endforeach; ?>

							@if (!empty($contest->instagram))

								<div style="text-align:center;margin-top: 25px;">
									<a href="https://instagram.com{{ $contest->instagram }}" target="_blank"><img src="/website_public/tab/Instagram_icon.png"></img> {{ $contest->instagram }}</a>
								</div>
								<br>
							@endif

						</div>
						@endif


						<br>


						<?php $datetime = new DateTime(); ?>
						<?php $today = $datetime->format("Y-m-d H:i:s"); ?>

						<div class="row" id="actions-middle">
							<div class="col-xs-12">
								@if (($today >= $contest->start_time) && ($today <= $contest->end_time) && $contest->status == 1 )
									@if ($contest->max_entries != '0')
										<button class="btn btn-lg btn-inverse btn-block join_now_contest center-block" onclick="popup();">{{ trans('tab.join_now') }}</button>
										<button class="btn btn-lg btn-inverse btn-block share_now_contest center-block" >{{ trans('tab.share') }}</button>
									@else
										<button class="btn btn-lg btn-inverse btn-block share_now_contest center-block" >{{ trans('tab.share') }}</button>
									@endif

								@else
									@if ($contest->max_entries != '0')
										<button class="btn btn-lg btn-inverse btn-block join_now_contest center-block" onclick="popup();" disabled="disabled">{{ trans('tab.join_now') }}</button>
										<button class="btn btn-lg btn-inverse btn-block share_now_contest center-block" disabled="disabled" >{{ trans('tab.share') }}</button>
									@else
										<button class="btn btn-lg btn-inverse btn-block share_now_contest center-block" disabled="disabled" >{{ trans('tab.share') }}</button>
									@endif
								@endif
							</div>
						</div>



					</div>
				</div>
			</section>





		</div>
		<a id="entries" style="color: {{$contest->template->subtitles_color}}"></a>
		<div class="container_fluid submissions">
			<!--
			<div class="categories_submissions">
				<ul class="nav nav-categories">
					<li id="li_most_viral" class="active"><a href="javascript:void(0)" onClick="change_order('most_viral');">{{ trans('tab.most_viral') }}</a></li>
					<li id="li_most_recent"><a href="javascript:void(0)" onClick="change_order('most_recent');">{{ trans('tab.most_recent') }}</a></li>
				</ul>
			</div>
			-->
			<div class="row" >
				<?php if ( $contest->win_modality_id != 2 ) { ?>
				<div class="categories_submissions">
					<select id="mySelect" class="form-control" style="color: #999999">
						  <option value="1" selected="selected">{{ trans('tab.most_viral') }}</option>
						  <option value="2">{{ trans('tab.most_recent') }}	</option>
					</select>
				</div>
			</div>
			<?php } else { ?>
				<div style="width: 100%; height: 122px;"></div>
			<?php } ?>
			<div class="clearfix"></div>
			<div id="id_grid" class="row submissions_grid">
				<?php // Submissions load by ajax ?>

			</div>

			<div id="loadingbar">
				<img alt="loading" style="border:none" src="{{asset('images/loader.gif') }}" />
			</div>

		</div>
		<div class="clearfix"></div>


		@include('tab/sharing_overlay')


<!--
<nav class="navbar navbar-default navbar-fixed-bottom" id="actions">
  <div class="container">
    <div class="row col-xs-12">
        <button class="btn btn-lg btn-inverse btn-block join_now_contest center-block" onclick="location.href='{{ URL::to( $url ) }}'" @if (!($today >= $contest->start_time && $today <= $contest->end_time)) disabled="disabled" @endif >{{ trans('tab.join_now') }}</button>
        <button class="btn btn-lg btn-inverse btn-block share_now_contest center-block" @if (!($today >= $contest->start_time && $today <= $contest->end_time)) disabled="disabled" @endif >{{ trans('tab.share') }}</button>
    </div>
      <!--<div class="col-xs-6 col-sm-6 center-block">
      	<button class="btn btn-lg btn-inverse btn-block share_now_contest center-block"  onclick="location.href='{{ URL::to($url_invite) }}'">{{ trans('tab.share') }}</button>

      </div>
  </div>



</nav>
-->
		<!--
		<footer class="container_fluid text-center">
            <a href="http://www.buzzmybrand.co">
				<img src="/website_public/home_page/img/logo-navbar-m.png" class="img-responsive center-block" width="90%"  />
			</a>
		</footer>
		-->
        <?php /* <div class="footer"></div> */ ?>
		<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		{{ HTML::script('website_public/js/jquery.validate.min.js') }}
		{{ HTML::script('lib/js/CustomScrollbar.js') }}
		<script>
			(function($){
				$(window).load(function(){
					$("#contentScroll").mCustomScrollbar({
						scrollButtons:{
							enable:true
						}
					});

				});
			})(jQuery);

		</script>
		<script>
        function popup()
        {
			<?php if (isset($contest->ua_google_code))  {?>
			ga('send', 'event', 'button', 'click', 'Join_now', '0');
			<?php } ?>
            /*
			var w = 800;
            var h = 680;
            var l = Math.floor((screen.width-w)/2);
            var t = Math.floor((screen.height-h)/2);
			*/
			window.open("{{ URL::to( $url ) }}",'_blank');
            //window.open("{{ URL::to( $url ) }}","nazwa","width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);

        }
        function invite_popup()
        {
            var w = 700;
            var h = 500;
            var l = Math.floor((screen.width-w)/2);
            var t = Math.floor((screen.height-h)/2);
            window.open("{{ URL::to($url_invite) }}","invite","width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
        }
        var page = 1;
        <?php if ( $contest->win_modality_id == 2 ) { ?>
		var order = 'most_recent';
	    <?php } else { ?>
		var order = 'most_viral';
		<?php } ?>
        var contest_type = '{{ $type_id }}';
        var contest_id = '{{ $id }}';
        var loading = false;
        var end = false;
        function change_order( new_order )
        {
        	if( order != new_order)
        	{
        		$("#li_"+new_order).addClass("active");
        		$("#li_"+order).removeClass("active");

        		page = 1;
        		end = false;
        		order = new_order;
        		$('.submissions_grid').html('');
        		try{
        		  FB.XFBML.parse(document.getElementById('id_grid'));
        		}catch(e)
        		{
        		}
        		get_submissions();
        	}

        }
        function get_submissions()
        {
        	$('#loadingbar').show();
        	loading = true;

        	//Ajax Post
    		$.post('/tab/getSubmissions', {contest_id: contest_id , contest_type: contest_type, page: page, order: order } , function(results){
    			$('.submissions_grid').append(results);
       				page++;
        		loading = false;
        		try{
        		  FB.XFBML.parse(document.getElementById('id_grid'));
        		}catch(e)
        		{
        		}
        		$('#loadingbar').hide();
    		});

        }

        $(document).ready(function(){
          	get_submissions();
            handle_scroll();

            enableSharingOverlay();

            {{-- checkAllLikes(); --}}

        });

        $(window).scroll(handle_scroll);

    function handle_scroll() {
      if((($(window).scrollTop()+$(window).height())+10)>=$(document).height()){

      if(loading == false && end == false){
        get_submissions();
        }
      }

      /* Show action buttons only when show_actions_marker is shown */
	  /*
      if ($('#show_actions_marker')[0].getBoundingClientRect().top * 5 - $(window).scrollTop() < 0) {
        $('#actions').fadeIn();
      } else {
          $('#actions').fadeOut();
      }
		*/
    };

		function fullscreen_popup(url, name)
		{
			//Ajax Post
    		$.post('/tab/getSubmission', { url: url, contest_type: {{ $type_id }} } , function(results){

        		$('.fullscreen_popup_title').html(name);
        		$("#fullscreen_popup").css("display", 'block' );
				$("#fullscreen_popup_content").css("display", 'block' );
				$("#submissione_popup_content").html(results);
    		});
    	}

		function close_fullscreen_popup()
		{
			$("#fullscreen_popup").css("display", 'none' );
			$("#fullscreen_popup_content").css("display", 'none' );
			$("#submissione_popup_content").html('');
		}


		$(document).ready(function(){

			var mySelect = document.getElementById("mySelect");
			if (typeof(mySelect) != 'undefined' && mySelect != null)
			{


				mySelect.onchange = function(){

					if ( document.getElementById("mySelect").value == 1 ) {
						change_order('most_viral');
					} else if ( document.getElementById("mySelect").value == 2 ) {
						change_order('most_recent');
					}

				}

			}


		})

		function apri(url) {
			newin = window.open(url,'titolo','scrollbars=no,resizable=yes, width=500,height=300,status=no,location=no,toolbar=no');
		}

		</script>
    </body>
</html>
