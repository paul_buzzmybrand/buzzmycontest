<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/magnific.css') }}
        {{ HTML::style('website_public/css/common.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
        {{ HTML::script('website_public/js/jquery.validate.min.js') }}
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
    </head>
    <body class="widget_facebook">
    	 <div class="popup-content">
        <img class="tab-image" src="{{asset('/images/contest/RDSLandingImage.png')}}">
        <div class="col-md-12">
        	@if($countVideos > 0)
        	<div id="myCarousel"  class="carousel slide" data-interval="5000" data-ride="carousel">
				<!-- Carousel indicators -->
				<ol class="carousel-indicators">
					<?php $num_ind = (int)$countVideos/3; ?>
					<?php if ($countVideos % 3 === 0): ?>
						<?php for ($i = 1; $i <= $num_ind; $i++): ?>
							<?php if ($i == 1): ?>
								<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<?php endif; ?>
							<?php if ($i > 1): ?>
								<li data-target="#myCarousel" data-slide-to="{{$i-1}}" ></li>
							<?php endif; ?>
						<?php endfor; ?>
					<?php endif; ?>	
					<?php if ($countVideos % 3 != 0): ?>
						<?php for ($i = 1; $i <= $num_ind+1; $i++): ?>
							<?php if ($i == 1): ?>
								<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<?php endif; ?>
							<?php if ($i > 1): ?>
								<li data-target="#myCarousel" data-slide-to="{{$i-1}}" ></li>
							<?php endif; ?>
						<?php endfor; ?>
					<?php endif; ?>		
						
					
					
					
						
				</ol>   
                  <!-- Carousel items -->
                  <div class="carousel-inner">
                      <?php $count = 0; ?>
                      <?php foreach($videos as $video): ?>
                          <?php if($count == 0): ?>
                              <div class="item active">
                          <?php endif; ?>

                          <?php if($count != 0): ?>     
                              <?php if($count % 3 === 0): ?>
                              </div>
                              <?php endif; ?>
                          <?php endif; ?>

                          <?php if($count != 0): ?>  
                              <?php if($count % 3 === 0): ?>
                                  <div class="item">
                              <?php endif; ?>
                          <?php endif; ?>

                          <div class="col-md-3-fb">
                              <div class="well-fb">
                                  <div class="popup">
									  <div style="padding:1px;">									  
										<span class="label label-info">User: {{DB::table('users')->where('id', $video->user_id)->pluck('first_name')." ".DB::table('users')->where('id', $video->user_id)->pluck('last_name')}}</span>
										
									  </div>
									  <div style="padding:1px;">								  
										
										<span class="label label-info">Score: {{$video->score}}</span>
									  </div>
									  <a href="#video-mini" class="video-view" video="{{$video->filename}}" title="{{$video->name}}" style='color:#555;'><img style="max-width: 150px; max-height: 150px" src="{{asset('/images/videos/'.$video->image)}}"/></a>
									  <div id="video-mini" style="width:500px;" class="video-white-fb-popup mfp-with-anim mfp-hide">
										<div class='popup-fb-title'><div class="video-title">{{ trans('messages.delete_account_desc') }}</div></div>
										<div class="popup-fb-content">
										   <video class="video-source" width="320" height="240" src="" controls>Your browser does not support the video tag.</video>
									  </div>
								  </div>
                                  <div style="padding:5px;">
                                  	<button type="button" class="btn btn-primary btn-xs">Vote</button>
  								  	<button type="button" class="btn btn-primary btn-xs">Share</button>
                                  </div>
									  
								 </div>
                              </div>
                          </div>

                          <?php if($countVideos -1  == $count): ?>
                              </div>
                          <?php endif; ?>
                          <?php $count ++; ?>
                      <?php endforeach; ?>
                  </div>
                  <!-- Carousel nav -->
                  <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left"></span>
                  </a>
                  <a class="carousel-control right" href="#myCarousel" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right"></span>
                  </a>
              </div>
              @else
                  <div class="alert alert-info" style='margin-top:40px;'>{{ trans('messages.no_results') }}</div>
              @endif
        
        <div class="widget-button">
            <!--<a href="{{URL::to('website/widget/login')}}" class='btn btn-lg btn-turqoise'>Sign in</a>-->
            <a class='btn btn-lg btn-primary' onclick="mywindow = window.open('{{ URL::to("/website/widget/login/".$id) }}', 'nazwa', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no,fullscreen=no,channelmode=no,width=300,height=350,left=300,top=100'); ">{{ trans('messages.Join_the_contest') }}</a>
        </div>   
    	</div>
    </div>
    

        {{ HTML::script('website_public/js/magnific.js') }}
        {{ HTML::script('website_public/js/popup.js') }}
        {{ HTML::script('website_public/js/bootstrap.min.js') }}
           
            <script>
            $(document).ready(function()
            {
              //Handles menu drop down`enter code here`
              $('.dropdown-menu').find('form').click(function (e) {
               e.stopPropagation();
               });
             });
             </script>
    		
			
    </body>
</html>
<script>    
	$( ".video-view" ).on('click', function(event) {
        event.preventDefault();
        var video = $(this).attr('video');
        var title = $(this).attr('title');
        $(".video-title").text(title);
        var url = 'http://www.joinmethere.tv/jmtvapi_samsung/videos';
        var videoUrl = url+'/'+video;
        $('.video-source').attr("src", videoUrl);      
           
    });
	

</script>
