<?php 
$current_page = $submissions->getCurrentPage();
$counter = 0;
?>
@if( $submissions->count() > 0 )
	
	@foreach( $submissions as $submission)
		
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3  submission_row">
				<?php App::setlocale($submission->contest->location->language); ?>
					<?php 
					
					if ( $submission->contest->type_id == '3' ) {
						$submission_thumbails = explode('.', $submission->image); 
					} else {
						$submission_thumbails = explode('.', $submission->filename);
					}
					
					?>
						
					@if ($submission->contest->type_id == '2')
					
					
					
					
					<div class="clearfix"></div>
					
					
					<!-- icona Facebook + like Facebook posta sopra l'immagine, azione dipendente da modalità di vincita -->
					@if ((count($submission->contest->fbPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 1))
						<div style="float:left; width: 56px; height: 56px;"><a href="javascript:apri('http://www.facebook.com/sharer/sharer.php?u=http://www.contestengine.net/FacebookSharePhoto/{{str_replace('jpg', 'html', $submission->filename)}}&title={{utf8_decode($submission->name)}}');"><img src="/website_public/tab-m/f.png" width="56px" height="56px" /></a></div>
						<?php $fb_idpost = $submission->fbPages->first()->pivot->fb_idpost; ?>
						<?php $details_page = explode("_", $fb_idpost); ?>
						<div style="float:right; width:55px; margin-left: 9px;margin-top:15px" class="fb-like-custom" data-post-id="{{$fb_idpost}}"><a href="javascript:likeFunction('{{$fb_idpost}}');"><img style="width: 100%;" src="/website_public/tab-m/like-button.png" /></a></div>
					@elseif ((count($submission->contest->fbPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 2))
						<div style="float:left; width: 56px; height: 56px;"><a href="https://www.facebook.com/photo.php?fbid={{$submission->fbPages->first()->pivot->fb_idphoto}}" target="_blank"><img src="/website_public/tab-m/f.png" width="56px" height="56px" /></a></div>
					@endif
					
					
					<!-- icona Twitter posta sopra l'immagine, azione dipendente da modalità di vincita -->
					@if ((count($submission->contest->twPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 1))
						<div style="float:left; width: 56px; height: 56px; margin-left: 3px;"><a href="javascript:apri('https://twitter.com/intent/tweet?url=http://www.contestengine.net/FacebookSharePhoto/{{str_replace('jpg', 'html', $submission->filename)}}&title={{utf8_decode($submission->name)}}&amp;text=Check it out!+&amp;via=logicum_co');"><img src="/website_public/tab-m/t.png" width="56px" height="56px" /></a></div>
					@elseif ((count($submission->contest->twPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 2))
						<div style="float:left; width: 56px; height: 56px; margin-left: 3px;"><img src="/website_public/tab-m/t.png"></img></div>
					@endif
					
					
					
					<!-- icona Instagram posta sopra l'immagine -->
					@if ($submission->user_connected_to_instagram)
						<div style="float:left; width: 56px; height: 56px;"><a href="javascript:apri('http://www.facebook.com/sharer/sharer.php?u=http://www.contestengine.net/FacebookSharePhoto/{{str_replace('jpg', 'html', $submission->filename)}}&title={{utf8_decode($submission->name)}}');"><img src="/website_public/tab-m/f.png" width="56px" height="56px" /></a></div>
						<div style="float:right; width: 56px; height: 56px;"><a href="{{ $submission->ig_source_url }}" target="_blank"><img src="/website_public/tab-m/ig_icon.png"></img></a></div>
					@endif
					
										
					<div class="clearfix"></div>
					
					
					
					<div style="border: 1px solid #e2dede; padding: 2px; margin-top: 7px;">
					
						@if ($submission->user_connected_to_instagram)
						
							<a href="javascript:void(0)">
								<div class="submission_media"style="background-image:url('{{ $submission->image }}')">
								</div>
							</a>
						
						@else							
							
							<a href="javascript:void(0)" >
								<div class="submission_media"style="background-image:url('{{ asset('http://www.contestengine.net/images/photos/' . $submission_thumbails[0] . '-sqthumbnail.jpg') }}')">
								</div>
							</a>
						
						
						@endif

						<div class="<?php echo ( $submission->contest->win_modality_id != 2 )?"submission_info_box":"submission_info_box_2"; ?>">
							<h4 style="margin:0 10px 4px 10px!important;padding-top:6px;font-weight: bold;font-size: small;text-align:left;color: #707173; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">{{ utf8_decode($submission->name)}}</h4>
							<h5 style="margin:0 10px 4px 10px!important;font-weight: bold;text-align:left; color: black; font-size: 22px;text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">{{ $submission->user->name}}&nbsp;{{ $submission->user->surname}}</h5>
							
							<?php if ( $submission->contest->win_modality_id != 2 ) { ?>
							<h3 style="float: left; width: 70px; height: 45px; padding-bott: 20px; margin:0 10px 4px 10px;text-align:left; color: #707173; font-size: 42px;">{{ $submission->rank_position}}</h3>
							<h4 style="float: right; width: 135px; margin:0 10px 4px 10px!important;padding-top: 20px;font-size: small;text-align:right;color: #707173;">{{trans('tab.score')}}</h4>
							<h5 style="float: right; width: 135px; margin:0 10px 4px 10px!important;font-weight: bold;font-size: 22px;text-align:right;color: #a52828;">{{ $submission->score}}</h5>
							<?php } ?>
							
							
							<div style="clear: both;"></div>
							<!--<div style="float: left;width: 32.5%; height: 35px; background-color: #5b7a36; text-align: left;"><img style="float: left; margin: 8px 0px 8px 8px;" src="/website_public/tab/like.png"></img><div style="float: left; width: 51px; height: 35px; font-size: 10px; line-height: 35px; text-align: center; color: #fff">{{$statistics[$submission->id]['unique_likes']}}</div></div>
							<div style="float: left;margin-left: 2px; width: 33%; height: 35px; background-color: #5b7a36;"><img style="float: left; margin: 8px 0px 8px 8px;" src="/website_public/tab/comments.png"></img><div style="float: left; width: 51px; height: 35px; font-size: 10px; line-height: 35px; text-align: center; color: #fff">{{$statistics[$submission->id]['unique_comments']}}</div></div>
							<div style="float: left; width: 32.5%; margin-left: 2px; height: 35px; background-color: #5b7a36;"><img style="float: left; margin: 8px 0px 8px 8px;" src="/website_public/tab/share.png"></img><div style="float: left; width: 51px; height: 35px; font-size: 10px; line-height: 35px; text-align: center; color: #fff">{{$statistics[$submission->id]['unique_shares']}}</div></div>   //-->
							<div class="clearfix"></div>
						</div>
						
					</div>
					@elseif ($submission->contest->type_id == '1')
						
						
						
						<!-- icona Facebook + like Facebook posta sopra l'immagine, azione dipendente da modalità di vincita -->
						<div class="clearfix"></div>
						@if ((count($submission->contest->fbPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 1))
							<div style="float:left; width: 56px; height: 56px;"><a href="javascript:apri('http://www.facebook.com/sharer/sharer.php?u=http://www.contestengine.net/FacebookShare/{{str_replace('mp4', 'html', $submission->filename)}}&title={{utf8_decode($submission->name)}}');"><img src="/website_public/tab-m/f.png" width="56px" height="56px" /></a></div>
							<div style="float:left; width: 100px; height: 56px; margin-left: 9px;" class="fb-like" data-href="https://www.facebook.com/video.php?v={{$submission->fbPages->first()->pivot->fb_idpost}}" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>
						@elseif ((count($submission->contest->fbPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 2))
							<div style="float:left; width: 56px; height: 56px;"><a href="https://www.facebook.com/video.php?v={{$submission->fbPages->first()->pivot->fb_idpost}}" target="_blank"><img src="/website_public/tab-m/f.png" width="56px" height="56px" /></a></div>
						@endif
						
						
						<!-- icona Twitter posta sopra l'immagine, azione dipendente da modalità di vincita -->
						@if ((count($submission->contest->twPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 1))
							<?php $filename = str_replace('mp4', 'html', $submission->filename); ?>
							<?php $link = CommonHelper::get_short_link("http://www.contestengine.net/FacebookShare/".$filename); ?>
							<div style="float:left; width: 56x; height: 56px; margin-left: 3px;"><a href="https://twitter.com/intent/tweet?url={{$link}}&text=Check it out!&via=bmbdemo"><img src="/website_public/tab-m/t.png" width="56px" height="56px" /></a></div>
						@elseif ((count($submission->contest->twPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 2))
							<div style="float:left; width: 56x; height: 56px; margin-left: 3px;"><img src="/website_public/tab-m/t.png"></img></div>
						@endif
						
						<!-- icona Instagram posta sopra l'immagine -->
						@if ($submission->user_connected_to_instagram)
							<div style="float:left; width: 24px; height: 29px;"><a href="{{ $submission->ig_source_url }}" target="_blank"><img src="/website_public/tab/Instagram_icon_24.png"></img></a></div>
						@endif
						
						
						
						
						
						<div class="clearfix"></div>
						
						
						
						<div style="border: 1px solid #e2dede; padding: 2px; margin-top: 7px;">
						
						
						
						<a href="{{ asset('http://www.contestengine.net/videos/' . $submission->filename) }}" >
							<div class="submission_media" style="background-image:url('{{ asset('http://www.contestengine.net/images/videos/' . $submission_thumbails[0] . '-sqthumbnail.jpg') }}')">
							</div>
						</a>
						<div class="<?php echo ( $submission->contest->win_modality_id != 2 )?"submission_info_box":"submission_info_box_2"; ?>">
							<h4 style="margin:0 10px 4px 10px!important;padding-top:6px;font-weight: bold;font-size: small;text-align:left;color: #707173;text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">{{ utf8_decode($submission->name)}}</h4>
							<h5 style="margin:0 10px 4px 10px!important;font-weight: bold;text-align:left; color: black; font-size: 22px;text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">{{ $submission->user->name}}&nbsp;{{ $submission->user->surname}}</h5>
							
							<?php if ( $submission->contest->win_modality_id != 2 ) { ?>
							<h3 style="float: left; width: 70px; height: 45px; padding-top: 20px; margin:0 10px 4px 10px;text-align:left; color: #707173; font-size: 42px;">{{ $submission->rank_position}}</h3>
							<h4 style="float: right; width: 135px;margin:0 10px 4px 10px!important;padding-top: 20px;font-weight: bold;font-size: small;text-align:right;color: #707173;">{{trans('tab.score')}}</h4>
							<h5 style="float: right; width: 135px; margin:0 10px 4px 10px!important;font-weight: bold;font-size: 22px;text-align:right;color: #a52828;">{{ $submission->score}}</h5>
							<?php } ?>
							
							
							<div class="clearfix"></div>
						<!--	<div style="float: left; width: 32.5%; height: 35px; background-color: #5b7a36; text-align: left;"><img style="float: left; margin: 8px 0px 8px 8px;" src="/website_public/tab/like.png"></img><div style="float: left; width: 51px; height: 35px; font-size: 10px; line-height: 35px; text-align: center; color: #fff">{{$statistics[$submission->id]['unique_likes']}}</div></div>
							<div style="float: left; margin-left: 2px; width: 33%; height: 35px; background-color: #5b7a36;"><img style="float: left; margin: 8px 0px 8px 8px;" src="/website_public/tab/comments.png"></img><div style="float: left; width: 51px; height: 35px; font-size: 10px; line-height: 35px; text-align: center; color: #fff">{{$statistics[$submission->id]['unique_comments']}}</div></div>
							<div style="float: left; margin-left: 2px; width: 32.5%; height: 35px; background-color: #5b7a36;"><img style="float: left; margin: 8px 0px 8px 8px;" src="/website_public/tab/share.png"></img><div style="float: left; width: 51px; height: 35px; font-size: 10px; line-height: 35px; text-align: center; color: #fff">{{$statistics[$submission->id]['unique_shares']}}</div></div> //-->
							<div class="clearfix"></div>
						</div>
						</div>
						
					@elseif ($submission->contest->type_id == '3')
					
					
					
					<!-- se il contest ha pagine facebook collegate è un contest che gira su facebook-->
					<!-- se il contest ha pagine twitter collegate è un contest che gira su twitter-->
					<div class="clearfix"></div>
					@if (count($submission->contest->fbPages) > 0)
						<div style="float:left; width: 56px; height: 56px;"><a href="javascript:apri('http://www.facebook.com/sharer/sharer.php?u=http://www.contestengine.net/FacebookShareEssay/{{str_replace('jpg', 'html', $submission->image)}}&title={{utf8_decode($submission->sentence)}}');"><img src="/website_public/tab-m/f.png" width="56px" height="56px" /></a></div>
					@endif
					@if (count($submission->contest->twPages) > 0)
						<div style="float:left; width: 56px; height: 56px; margin-left: 3px;"><a href="javascript:apri('https://twitter.com/intent/tweet?url=http://www.contestengine.net/FacebookShareEssay/{{str_replace('jpg', 'html', $submission->image)}}&title={{utf8_decode($submission->sentence)}}&amp;text=Check it out!+&amp;via=logicum_co');"><img src="/website_public/tab-m/t.png" width="56px" height="56px" /></a></div>
					@endif
					<!--<div style="float:left; width: 24px; height: 29px; margin-left: 3px;"><a href=""><img src="/website_public/tab/i.png"></img></a></div>-->
					@if (count($submission->contest->fbPages) > 0)
						<?php $fb_idpost = $submission->fbPages->first()->pivot->fb_idpost; ?>
						<?php $details_page = explode("_", $fb_idpost); ?>
						<div style="float:right; width:55px; margin-left: 9px;margin-top:15px" class="fb-like-custom" data-post-id="{{$fb_idpost}}"><a href="javascript:likeFunction('{{$submission->fbPages->first()->pivot->fb_idessay}}');"><img style="width: 100%;" src="/website_public/tab-m/like-button.png" /></a></div>
					@endif
					
					<!--<div ><a href=""><img src="/website_public/tab/like-button.png"></img></a></div>-->
					<div class="clearfix"></div>
					
					
					
					<div style="border: 1px solid #e2dede; padding: 2px; margin-top: 7px;">
					

						
						<!--<a href="javascript:void(0)" onClick="fullscreen_popup('{{ asset('http://www.contestengine.net/photos/' . $submission->filename) }}', '{{ $submission->name}}');" >-->
						<a href="javascript:void(0)" >
							<div class="submission_media"style="background-image:url('{{ asset('http://www.contestengine.net/images/essays/' . $submission_thumbails[0] . '-sqthumbnail.jpg') }}')">
							</div>
						</a>
						

						<div class="<?php echo ( $submission->contest->win_modality_id != 2 )?"submission_info_box":"submission_info_box_2"; ?>">
							<h5 style="margin:0 10px 4px 10px!important;font-weight: bold;text-align:left; color: black; font-size: 22px;text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">{{ $submission->user->name}}&nbsp;{{ $submission->user->surname}}</h5>
							
							<?php if ( $submission->contest->win_modality_id != 2 ) { ?>
							<h3 style="float: left; width: 70px; height: 45px; padding-bott: 20px; margin:0 10px 4px 10px;text-align:left; color: #707173; font-size: 42px;">{{ $submission->rank_position}}</h3>
							<h4 style="float: right; width: 135px; margin:0 10px 4px 10px!important;padding-top: 20px;font-size: small;text-align:right;color: #707173;">{{trans('tab.score')}}</h4>
							<h5 style="float: right; width: 135px; margin:0 10px 4px 10px!important;font-weight: bold;font-size: 22px;text-align:right;color: #a52828;">{{ $submission->score}}</h5>
							<?php } ?>
							
							
							<div style="clear: both;"></div>
							<!--<div style="float: left;width: 32.5%; height: 35px; background-color: #5b7a36; text-align: left;"><img style="float: left; margin: 8px 0px 8px 8px;" src="/website_public/tab/like.png"></img><div style="float: left; width: 51px; height: 35px; font-size: 10px; line-height: 35px; text-align: center; color: #fff">{{$statistics[$submission->id]['unique_likes']}}</div></div>
							<div style="float: left;margin-left: 2px; width: 33%; height: 35px; background-color: #5b7a36;"><img style="float: left; margin: 8px 0px 8px 8px;" src="/website_public/tab/comments.png"></img><div style="float: left; width: 51px; height: 35px; font-size: 10px; line-height: 35px; text-align: center; color: #fff">{{$statistics[$submission->id]['unique_comments']}}</div></div>
							<div style="float: left; width: 32.5%; margin-left: 2px; height: 35px; background-color: #5b7a36;"><img style="float: left; margin: 8px 0px 8px 8px;" src="/website_public/tab/share.png"></img><div style="float: left; width: 51px; height: 35px; font-size: 10px; line-height: 35px; text-align: center; color: #fff">{{$statistics[$submission->id]['unique_shares']}}</div></div>   //-->
							<div class="clearfix"></div>
						</div>
						
					</div>
						
					@endif
					
				
				

		</div>
	@endforeach
	
@else
	<!-- 
	<div class="alert alert-info" role="alert">
		@if( $current_page == 1 ){{ 'There are not submissions for this contest!!!' }}
		@else {{ 'There are not others submissions for this contest!!!' }}
		@endif
	</div>
	 -->
	<script>
	end = true;
	</script>
@endif
<div class="clearfix"></div>