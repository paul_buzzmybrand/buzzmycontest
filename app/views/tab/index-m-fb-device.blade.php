<!doctype html>
<html lang="en">
    <head>
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta charset="UTF-8">

        {{ HTML::style('website_public/css-m/contest.css') }}


        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
		{{ HTML::script('website_public/js/jquery.js') }}
        {{ HTML::script('website_public/js/jquery.validate.min.js') }}

		<style>
		
		.arrow {
			width: 52px;
			background-image: url("website_public/tab-m/arrow-repeat.png");
		}
		
		.arrow2 {
			width: 52px;
			height: 210px;
			background-image: url("website_public/tab-m/arrow-down.png");
		}
		
		</style>
		
		<script>
		
			$(document).ready(function(){
			
				$(".arrow").css("height", $(window).height() - 210 + "px");
				$(".column_left").css("height", $(window).height() + "px");
				$(".column_left").css("width", $(window).width() - 100 + "px");
			});
		
		</script>
		
    </head>
    <body style="background-color: grey; height: 100%">
		
		<?php if ( ( strpos($_SERVER['HTTP_USER_AGENT'], "FBAN/FBIOS") > -1 && (strpos($_SERVER['HTTP_USER_AGENT'], "iPhone") > -1 || strpos($_SERVER['HTTP_USER_AGENT'], "iPod") > -1) )	
					|| ( strpos($_SERVER['HTTP_USER_AGENT'], "FB_IAB") > -1 && strpos($_SERVER['HTTP_USER_AGENT'], "Android") > -1 ) ) { ?>
			
			
			
			<?php if ( 	strpos($_SERVER['HTTP_USER_AGENT'], "FBAN/FBIOS") > -1 && 
						(strpos($_SERVER['HTTP_USER_AGENT'], "iPhone") > -1 || strpos($_SERVER['HTTP_USER_AGENT'], "iPod") > -1) ) { ?>
				
				
				<div style="max-width: 860px; float: right;">
					<div style="float: right;">
						<div class="arrow"></div>
						<div class="arrow2"></div>
					</div>
					<!--<img src="{{asset('website_public/tab-m/arrow-down.png')}}" style="position: absolute;bottom: 0;right:0; padding-bottom: 100px;"></img>-->
					<div style="color: #ffffff; font-size: 36px; text-align: right; display: table-cell; vertical-align: bottom;">
						{{Lang::get('tab.tab_fb_device.sentence1_apple')}}
					</div>
					<div style="color: green; font-size: 30px; margin-top: 5px; margin-right: 52px; text-align: right;">
					{{Lang::get('tab.tab_fb_device.sentence2_apple')}}
					</div>
					<div style="width: 200px; height: 200px; margin: 0 auto; margin-top: 5px;">
						<img style="max-width:100%; max-height:100%;" src="{{asset('website_public/tab-m/brows.png')}}"></img>
					</div>
				</div>
				
				
				
				
				
				
			<?php } else if ( strpos($_SERVER['HTTP_USER_AGENT'], "FB_IAB") > -1 && strpos($_SERVER['HTTP_USER_AGENT'], "Android") > -1 ) { ?>
				<div style="max-width: 860px; float: right;">
					<img src="{{asset('website_public/tab-m/arrow.png')}}" style="float: right;"></img>
					<div style="height: 210px; color: #ffffff; font-size: 36px; text-align: right; display: table-cell; vertical-align: bottom;">
						{{Lang::get('tab.tab_fb_device.sentence1_android')}}
					</div>
				</div>
				<div style="clear:both;"></div>
				<div style="color: green; font-size: 30px; margin-top: 30px; margin-right: 52px; text-align: right;">
				{{Lang::get('tab.tab_fb_device.sentence2_android')}}
				</div>
				<div style="width: 200px; height: 200px; margin: 0 auto; margin-top: 30px;">
					<img style="max-width:100%; max-height:100%;" src="{{asset('website_public/tab-m/brows.png')}}"></img>
				</div>
				
				
				
				
			<?php } ?>
			
		<?php } else if ( strpos($_SERVER['HTTP_USER_AGENT'], "FBAN/MessengerForiOS") > -1 ) { ?>
			<div style="float: right;">
				<div class="arrow"></div>
				<div class="arrow2"></div>
			</div>
			<div class="column_left" style="float: right;">
				<p style="color: #ffffff; font-size: 36px; text-align: right; margin-top: 150px;">{{Lang::get('tab.tab_fb_device.sentence1')}}</p>
				<p style="color: green; font-size: 30px; margin-top: 30px; text-align: right;">{{Lang::get('tab.tab_fb_device.sentence2_android')}}</p>
				<div style="width: 200px; height: 200px; margin: 0 auto; margin-top: 30px;">
					<img style="max-width:100%; max-height:100%;" src="{{asset('website_public/tab-m/brows.png')}}"></img>
				</div>
			</div>
		<?php } ?>
		
    </body>
</html>
