<!doctype html>
<html lang="en">
    <head>
       <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
     
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
{{ HTML::style('website_public/css-m/contest_popup.css') }}
 <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script> 
{{ HTML::style('website_public/css-m/common.css') }}

	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />

<style>
	body {
		font-family: Open Sans;
	}

    .spinner {
      margin: 300px 200px 300px 280px;
      width: 300px;
      height: 100px;
      text-align: center;
      font-size: 15px;
      position:absolute;
      color:white;
      top:-100px;
      right:-20px;
      display:none;
      background-color:#333333;
      padding-top:20px;
      border-radius:5px;
    }

    .spinner > div {
      background-color: white;
      height: 60%;
      width: 6px;
      display: inline-block;
      
      -webkit-animation: stretchdelay 1.2s infinite ease-in-out;
      animation: stretchdelay 1.2s infinite ease-in-out;
    }

    .spinner .rect2 {
      -webkit-animation-delay: -1.1s;
      animation-delay: -1.1s;
    }

    .spinner .rect3 {
      -webkit-animation-delay: -1.0s;
      animation-delay: -1.0s;
    }

    .spinner .rect4 {
      -webkit-animation-delay: -0.9s;
      animation-delay: -0.9s;
    }

    .spinner .rect5 {
      -webkit-animation-delay: -0.8s;
      animation-delay: -0.8s;
    }

    @-webkit-keyframes stretchdelay {
      0%, 40%, 100% { -webkit-transform: scaleY(0.4) }  
      20% { -webkit-transform: scaleY(1.0) }
    }

    @keyframes stretchdelay {
      0%, 40%, 100% { 
        transform: scaleY(0.4);
        -webkit-transform: scaleY(0.4);
      } 20% {
        transform: scaleY(1.0);
        -webkit-transform: scaleY(1.0);
      }
    }
</style>

</head>
<body>
<div class="container widget">
   
		 
	
	<div class="step {{ $step }}"> 
	</div>
		 
	<div  class="col-xs-12 col-md-12" style="margin-top: 40px">
	
		{{ Form::open(array('action' => array('tab_HomeController@saveVideo'), 'method' => 'post', 'id' => 'uploadForm')) }}
		<input name="video" type='hidden' value="{{ $data }}">
		<input id="input_type" name='input_type' type='hidden' value="import">
		<input id="id_contest" name='id_contest' type='hidden' value="{{ $contest_id }}">
		
        <div class="row" style="max-width: 500px;  margin:1px auto 0 auto;">
          
			<div class="row">
			
				<div id="file-video-contest" class="center-block">
					<video width="100%" height="auto" controls autoplay style="min-height: 240px">
						<source src="{{ $data }}"  type="video/mp4">
					</video>
				</div>
			
			</div>

		
		</div>
		
		
		<div  class="row" style="max-width: 260px; height: 65px; margin: 10px auto;">	
				<textarea id="title-video" name="title" style="float: left; width: 260px; height: 65px;border: solid 1px grey; margin-top:10px;" placeholder="{{Lang::get('widget.creation_step.placeholder')}} '{{$contest->hashtag}}' {{Lang::get('widget.creation_step.placeholder2')}}" maxlength="140"></textarea>
		</div>

		
		<div class="row" style="max-width: 530px; height: 40px; margin: 20px auto;">
		
			<div id="uploadSubmission" onClick="">
				<?php if ( $contest->location_id == 2 ) { ?>
				<img id="foto-submit" src="{{asset('website_public/contest_popup/conferma-upload.png') }}" width="260" height="auto"/>
				<?php } else { ?>
				<img id="foto-submit" src="{{asset('website_public/contest_popup/5-submit.png') }}" width="260" height="auto"/>
				<?php } ?>
			</div>
		</div>
		
		
		{{ Form::close() }}
	</div>
		

</div>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
 <script>
 
 $(document).ready(function(){
	
	$('#title-video').keyup(function(){
		validate();
	});
  
  
	$('#uploadSubmission').click(function() {
		if ( !submitDisabled ) {
			document.getElementById('uploadForm').submit();
		}
	});
  
});

function validate()
    {
    	var title = $('#title-video').val();
    	
       	if( title != '')
       	{
       		//$('#submitButton').addClass('submitButtonActive');
       		$('#uploadSubmission').prop('disabled', false );
          change_onclick('enable');
		  
			//$('#uploadSubmission').addClass('submit-upload-submission-active');
			
			$('.submit-upload-submission').addClass('submit-upload-submission-active');
		  
       	}
       	else
       	{
          change_onclick('disable');
       		//$('#submitButton').removeClass('submitButtonActive');
       		$('#uploadSubmission').prop('disabled', true );  
			
			//$('#submit-upload-submission').removeClass('submit-upload-submission-active');
			
			$('.submit-upload-submission').removeClass('submit-upload-submission-active');
       	} 

		
       		
    }
 
 
var submitDisabled = true;

function change_onclick( action )
    { 
        if( action == 'disable')
        {
            $('#uploadSubmission').attr("onclick","");
            
        }
        else if( action == 'enable')
        {
            $('#uploadSubmission').attr("onclick","document.getElementById('uploadForm').submit();");
        }
    }

 </script>
 </body>
</html>