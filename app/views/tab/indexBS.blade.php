<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/magnific.css') }}
        {{ HTML::style('website_public/css/common.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
        {{ HTML::script('website_public/js/jquery.validate.min.js') }}
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
    </head>
    <body class="widget_facebook">
    	 <div class="popup-content">
        <img class="tab-image" src="{{asset('/images/contest/'.$contestImage)}}">
        <div class="col-md-12">
        	@if($countVideos > 0)
        	<div id="myCarousel"  class="carousel slide" data-interval="5000" data-ride="carousel">
                  <!-- Carousel items -->
                  <div class="carousel-inner">
                      <?php $count = 0; ?>
                      <?php foreach($videos as $video): ?>
                          <?php if($count == 0): ?>
                              <div class="item active">
                          <?php endif; ?>

                          <?php if($count != 0): ?>     
                              <?php if($count % 3 === 0): ?>
                              </div>
                              <?php endif; ?>
                          <?php endif; ?>

                          <?php if($count != 0): ?>  
                              <?php if($count % 3 === 0): ?>
                                  <div class="item">
                              <?php endif; ?>
                          <?php endif; ?>

                          <div class="col-md-3-fb">
                              <div class="well-fb">
                                  <div class="popup">
                                  <div style="padding:5px;">
                                  <span class="label label-primary">Score: {{$video->score}}</span>
                                  </div>
                                  <a href="#video-mini" class="video-view" video="{{$video->filename}}" title="{{$video->name}}" style='color:#555;'><img style="max-width: 150px; max-height: 150px" src="{{asset('/images/videos/'.$video->image)}}"/></a></div>
                                  <div style="padding:5px;">
                                  	<button type="button" class="btn btn-primary btn-xs">Vote</button>
  								  	<button type="button" class="btn btn-primary btn-xs">Share</button>
                                  </div>
                              </div>
                          </div>

                          <?php if($countVideos -1  == $count): ?>
                              </div>
                          <?php endif; ?>
                          <?php $count ++; ?>
                      <?php endforeach; ?>
                  </div>
                  <!-- Carousel nav -->
                  <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left"></span>
                  </a>
                  <a class="carousel-control right" href="#myCarousel" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right"></span>
                  </a>
              </div>
              @else
                  <div class="alert alert-info" style='margin-top:40px;'>{{ trans('messages.no_results') }}</div>
              @endif
        <div class="widget-text">
            You need to reply to all the question to save! What are you waiting for?!
        </div>
        <div class="widget-button" style="width: 100%; text-align: right;">
        	
        	
        	@if($first_Answered == 0)
        		<h4 style="padding: 5px;">Why are you applying for BlackShape? <span class="btn btn-lg btn-primary"  onclick="mywindow = window.open('{{ URL::to("/website/widget/loginResume/".$id."/1") }}', 'nazwa', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no,fullscreen=no,channelmode=no,width=300,height=350,left=300,top=100'); ">Record</span></h4>
        	@else
        		<h4 style="padding: 5px;">Why are you applying for BlackShape? <span class="btn btn-lg btn-primary disabled"  onclick="mywindow = window.open('{{ URL::to("/website/widget/loginResume/".$id."/1") }}', 'nazwa', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no,fullscreen=no,channelmode=no,width=300,height=350,left=300,top=100'); ">Record</span></h4>
        	@endif
        	
        	@if($second_Answered == 0)
        		<h4 style="padding: 5px;">Describe a difficult experience at work and how you handled it <span class="btn btn-lg btn-primary"  onclick="mywindow = window.open('{{ URL::to("/website/widget/loginResume/".$id."/2") }}', 'nazwa', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no,fullscreen=no,channelmode=no,width=300,height=350,left=300,top=100'); ">Record</span></h4>
        	@else
        		<h4 style="padding: 5px;">Describe a difficult experience at work and how you handled it <span class="btn btn-lg btn-primary disabled"  onclick="mywindow = window.open('{{ URL::to("/website/widget/loginResume/".$id."/2") }}', 'nazwa', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no,fullscreen=no,channelmode=no,width=300,height=350,left=300,top=100'); ">Record</span></h4>
        	@endif
            
            <div style="width: 100%; float: left; padding: 5px;"></div>
            @if(($first_Answered == 1)&&($second_Answered == 1))	            
				<a class="btn btn-lg btn-primary" style="text-align: right;" href="{{action('Website_WidgetController@saveResume', $id)}}">
                                    <span class="glyphicon glyphicon-save"></span>
                                        Save
                </a>
			@else
				<a class="btn btn-lg btn-primary" style="text-align: right;" href="{{action('Website_WidgetController@saveResume', $id)}}">
                                    <span class="glyphicon glyphicon-save disabled"></span>
                                        Save
                </a>				
			@endif
				<button type="button" class="btn btn-lg btn-primary" style="text-align: left;">
				  <span class="glyphicon glyphicon-remove"></span> Cancel
				</button>
        </div>   
    	</div>
    </div>
    

        {{ HTML::script('website_public/js/magnific.js') }}
        {{ HTML::script('website_public/js/popup.js') }}
        {{ HTML::script('website_public/js/bootstrap.min.js') }}
           
            <script>
            $(document).ready(function()
            {
              //Handles menu drop down`enter code here`
              $('.dropdown-menu').find('form').click(function (e) {
               e.stopPropagation();
               });
             });
             </script>
    		@include('website.user.popup')
    </body>
</html>
<script>
    $( ".video-view" ).on('click', function(event) {
        event.preventDefault();
        var video = $(this).attr('video');
        var title = $(this).attr('title');
        $( ".video-title" ).text(title);
        var url = '{{asset("videos")}}'
        var videoUrl = url+'/'+video;
        $('.video-source').attr( "src", videoUrl );      
           
    });

</script>