<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/contest.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
        {{ HTML::script('website_public/js/jquery.validate.min.js') }}
        {{ HTML::style('lib/css/CustomScrollbar.css')}}
		{{ HTML::style('https://www.tok.tv/juventus/socialselfie/juve-contest.css')}}
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
    </head>
    <body>
		<?php $type_id = $contest->type_id; ?>
    	<?php App::setlocale($contest->location->language); ?>
		<?php $url_invite = "invite/".$contest->id; ?>
		<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>		
    	<div id="fullscreen_popup">
		<div id="fullscreen_popup_content"> 
			<a href="javascript:void(0)" onClick="close_fullscreen_popup();" class="cancel">&times;</a>
			<div class="fullscreen_popup_title">PROVA</div>
			<div id="submissione_popup_content"></div> 
		</div>
	</div>
		<?php $contest_image = explode('.', $contest->image); ?>
    	<div class="header_bar">
            <div class="logo1" style="float:left;">
				@if( $contest->company->image != '' && $contest->company->image != null)
            	<img src="{{asset('images/companys/' . $contest->company->image) }}" height="70" width="70" />
				@endif
            </div>
			<div class="logo2" style="float:right;">
            	<img src="{{asset('images/events/' . $contest_image[0] . '_wt.png') }}" height="70" width="70" />
            </div>
        </div>
        <div class="hidden_clear"></div>
        <div class="contest_container">
        

<div class="hidden_clear"></div>
<div class="submissions">
	<div class="categories_submissions">
		<ul class="nav nav-categories">
			<li id="li_most_viral" class="active"><a href="javascript:void(0)" onClick="change_order('most_viral');">{{ trans('tab.most_viral') }}</a></li>
			<li id="li_most_recent"><a href="javascript:void(0)" onClick="change_order('most_recent');">{{ trans('tab.most_recent') }}</a></li>
		</ul>
	</div>
	<div class="hidden_clear"></div>
	<div id="id_grid" class="submissions_grid">
		<?php // Submissions load by ajax ?>
	</div>
	<div id="loadingbar">
		<img alt="loading" style="border:none" src="{{asset('images/loader.gif') }}" />
	</div>

</div>
<div class="hidden_clear"></div>
 </div>
        <?php /* <div class="footer"></div> */ ?>
        
        {{ HTML::script('website_public/js/bootstrap.min.js') }}
        {{ HTML::script('lib/js/CustomScrollbar.js') }}
        <script>
            (function($){
                $(window).load(function(){
                    $("#contentScroll").mCustomScrollbar({
                        scrollButtons:{
                            enable:true
                        }
                    });

                });
            })(jQuery);

        </script>
        <script>
        function popup()
        {
            var w = 740;
            var h = 580;
            var l = Math.floor((screen.width-w)/2);
            var t = Math.floor((screen.height-h)/2);
            window.open("{{ URL::to( $url ) }}","nazwa","width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
            <?php //choose ?>
        }		
        function invite_popup()
        {
            var w = 700;
            var h = 130;			
            var l = Math.floor((screen.width-w)/2);
            var t = Math.floor((screen.height-h)/2);
            window.open("{{ URL::to($url_invite) }}","invite","width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
        }
        var page = 1;
        var order = 'most_viral';
        var contest_type = '{{ $type_id }}';
        var contest_id = '{{ $id }}';
        var loading = false;
        var end = false;
        function change_order( new_order )
        {
        	if( order != new_order)
        	{
        		$("#li_"+new_order).addClass("active");
        		$("#li_"+order).removeClass("active");

        		page = 1;
        		end = false;
        		order = new_order;
        		$('.submissions_grid').html('');
        		  FB.XFBML.parse(document.getElementById('id_grid'));
        		get_submissions();
        	}

        }
        function get_submissions()
        {
        	$('#loadingbar').css("display","block");
        	loading = true;
        	
        	//Ajax Post
    		$.post('getSubmissions', {contest_id: contest_id , contest_type: contest_type, page: page, order: order } , function(results){
    			$('.submissions_grid').append(results);
       				page++;
        		loading = false;
        		  FB.XFBML.parse(document.getElementById('id_grid'));
        		$('#loadingbar').css("display","none");
    		});
        	
        }
        </script>

        <script>
        $(document).ready(function(){
        	get_submissions();
        });
        
        $(window).scroll(function(){
			if((($(window).scrollTop()+$(window).height())+10)>=$(document).height()){
		
			if(loading == false && end == false){
				get_submissions();
				}
			}
		});
        </script>
        <script>
		function fullscreen_popup(url, name)
		{
			//Ajax Post
    		$.post('getSubmission', { url: url, contest_type: {{ $type_id }} } , function(results){
    			
        		$('.fullscreen_popup_title').html(name);
        		$("#fullscreen_popup").css("display", 'block' );
				$("#fullscreen_popup_content").css("display", 'block' );
				$("#submissione_popup_content").html(results);
    		});
    	}

		function close_fullscreen_popup()
		{
			$("#fullscreen_popup").css("display", 'none' );
			$("#fullscreen_popup_content").css("display", 'none' );
			$("#submissione_popup_content").html('');
		}
	</script>
    </body>
</html>