<!doctype html>
@include('Mobile_Detect')
<?php
$type_id = $contest->type_id;
App::setlocale($contest->location->language);
$url_invite = "invite/".$contest->id;
$contestRoute = "http://www.buzzmybrand.co/tab/" . $contest->id;
if ( $contest->contest_route ) {
	$contestRoute = "http://bmb.buzzmybrand.co/" . $contest->contest_route;
}
$og_description = str_replace("<br>","",$contest->concept);
if (strpos($contest->concept, '</a>') !== false) {
	$pos_a_end_char = strrpos($contest->concept, "</a>") + 4;
	$pos_a_start_char = strrpos($contest->concept, "<a");
	$lenght_string = $pos_a_end_char - $pos_a_start_char;

	$stringToRemove = substr($contest->concept, $pos_a_start_char, $lenght_string);

	if (strpos($contest->concept, 'Instagram') !== false)
	{
		$og_description = str_replace($stringToRemove, str_replace("/", "@", $contest->instagram), $og_description);
	}
	else
	{
		$og_description = str_replace($stringToRemove,"",$og_description);
	}

}



//var_dump("og_description: ".$og_description);
$detect = new Mobile_Detect;
if ( $detect->isMobile() ) {

if ( ( ( strpos($_SERVER['HTTP_USER_AGENT'], "FBAN/FBIOS") > -1
					&& (strpos($_SERVER['HTTP_USER_AGENT'], "iPhone") > -1 || strpos($_SERVER['HTTP_USER_AGENT'], "iPod") > -1) )


		|| ( strpos($_SERVER['HTTP_USER_AGENT'], "FB_IAB") > -1 && strpos($_SERVER['HTTP_USER_AGENT'], "Android") > -1 ) )

		||

		 ( strpos($_SERVER['HTTP_USER_AGENT'], "FBAN/MessengerForiOS") > -1 )


		) {

?>
	@include('tab.index-m-fb-device')

<?php
} else {
?>
@include('tab.index-m')
<?php
}

}
else{
//var_dump($contest->template->font);
?>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/contest.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
        {{ HTML::script('website_public/js/jquery.validate.min.js') }}
        {{ HTML::script('website_public/home_page/js/jquery.parallax-scroll.js') }}
        {{ HTML::script('website_public/js/jquery-scrolltofixed.js') }}
		<script>

			$(document).ready(function(){


                $('.header').scrollToFixed();

				var mySelect = document.getElementById("mySelect");
				if (typeof(mySelect) != 'undefined' && mySelect != null)
				{


					mySelect.onchange = function(){

						if ( document.getElementById("mySelect").value == 1 ) {
							change_order('most_viral');
						} else if ( document.getElementById("mySelect").value == 2 ) {
							change_order('most_recent');
						}

					}

				}


			});



            function apri(url) {
                newin = window.open(url,'titolo','scrollbars=no,resizable=yes, width=500,height=300,status=no,location=no,toolbar=no');
            }

            function likeFunction(id_post)
            {
                FB.getLoginStatus(function(response) {
                    if (response.status === 'connected') {


                        FB.api('/'+id_post+'/likes', 'post');
                        alert('Like done!');
                    }
                    else {
                        FB.login(function(){
                            FB.api('/'+id_post+'/likes', 'post');
                            alert('Like done!');
                        });
                    }
                });
            }

            function popup()
            {
                <?php if (isset($contest->ua_google_code))  {?>
                ga('send', 'event', 'button', 'click', 'Join_now', '0');
                        <?php } ?>
                var w = 900;
                var h = 680;
                var l = Math.floor((screen.width-w)/2);
                var t = Math.floor((screen.height-h)/2);
                window.open("{{ URL::to( $url ) }}","nazwa","width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
                <?php //choose ?>
            }
            function invite_popup()
            {
                <?php if (isset($contest->ua_google_code))  {?>
                ga('send', 'event', 'button', 'click', 'Invite_friends', '0');
                        <?php } ?>

                var w = 700;
                var h = 500;
                var l = Math.floor((screen.width-w)/2);
                var t = Math.floor((screen.height-h)/2);
                window.open("{{ URL::to($url_invite) }}","invite","width=" + w + ",height=" + h + ",top=" + t + ",left=" + l);
            }
            var page = 1;
            <?php if ( $contest->win_modality_id == 2 ) { ?>
            var order = 'most_recent';
            <?php } else { ?>
            var order = 'most_viral';
            <?php } ?>
            var contest_type = '{{ $type_id }}';
            var contest_id = '{{ $id }}';
            var loading = false;
            var end = false;
            function change_order( new_order )
            {
                if( order != new_order)
                {

                    $("#li_"+new_order).addClass("active");
                    $("#li_"+order).removeClass("active");

                    page = 1;
                    end = false;
                    order = new_order;
                    $('.submissions_grid').html('');
                    FB.XFBML.parse(document.getElementById('id_grid'));
                    get_submissions();
                }

            }
            function get_submissions()
            {
                $('#loadingbar').css("display","block");
                loading = true;

                if ( $(window).width() <= 820 ) {
                    tab = "true";
                } else {
                    tab = "false";
                }

                //Ajax Post
                $.post('/tab/getSubmissions?tab=' + tab, {contest_id: contest_id , contest_type: contest_type, page: page, order: order } , function(results){
                    $('.submissions_grid').append(results);
                    page++;
                    loading = false;
                    FB.XFBML.parse(document.getElementById('id_grid'));
                    $('#loadingbar').css("display","none");
                });

            }


            $(window).scroll(function(){
                if((($(window).scrollTop()+$(window).height())+10)>=$(document).height()){

                    if(loading == false && end == false){
                        get_submissions();
                    }
                }
            });

            /*
            jQuery(
                    function($)
                    {
                        $('body').bind('scroll', function()
                        {
                            if($(this).scrollTop() + $(this).innerHeight()+10>=$(this)[0].scrollHeight)
                            {
                                if(loading == false && end == false) {
                                    //alert ('end reached');
                                    get_submissions();
                                }
                            }

                            //sticky_relocate(this);
                        })
                    }
            );
            */


            function fullscreen_popup(url, name)
            {
                var tab = "";
                if ( $(window).width() <= 820 ) {
                    tab = "true";
                } else {
                    tab = "false";
                }

                //Ajax Post
                $.post('/tab/getSubmission', { url: url, contest_type: {{ $type_id }}, tab: tab } , function(results){

                    $('.fullscreen_popup_title').html(name);
                    $("#fullscreen_popup").css("display", 'block' );
                    $("#fullscreen_popup_content").css("display", 'block' );
                    $("#submissione_popup_content").html(results);
                });
            }

            function close_fullscreen_popup()
            {
                $("#fullscreen_popup").css("display", 'none' );
                $("#fullscreen_popup_content").css("display", 'none' );
                $("#submissione_popup_content").html('');
            }
        </script>

		<title>{{$contest->name}}</title>

		<meta property="og:url" content="{{$contestRoute}}" />
		<meta property="og:title" content="{{$contest->name}}" />
		<meta property="og:description" content="{{$og_description}}" />
		<meta property="og:site_name" content="{{$contest->name}}" />
		<?php $contest_image = explode('.', $contest->image); ?>
		<meta property="og:image" content="{{asset('/templates/'.$contest->template->contest_flyer)}}" />
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

		<?php if (isset($contest->ua_google_code)) { ?>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', '<?php echo $contest->ua_google_code; ?>', 'auto');
		  ga('send', 'pageview');

		</script>
		<?php }  ?>
    </head>
    <body>

		<div id="divId" style="display:none;"></div>

    	<div id="fb-root"></div>
		<script>
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3&appId=566800206740950";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
    	<div id="fullscreen_popup">
			<div id="fullscreen_popup_content">
				<a href="javascript:void(0)" onClick="close_fullscreen_popup();" class="cancel">&times;</a>
				<div class="fullscreen_popup_title"></div>
				<div id="submissione_popup_content"></div>
			</div>
		</div>
		<?php $contest_image = explode('.', $contest->image); ?>



        <div class="parent">
			@if ($contest->use_template == 1)
				<div class="header_bar parallax__layer" data-parallax='{"y": 100}' style="background-image:url('/templates/{{$contest->template->contest_flyer}}')"></div>
			@else
				<div class="header_bar parallax__layer" data-parallax='{"y": 100}' style="background-image:url('/images/events/{{$contest->image_minisite_bg}}')"></div>
			@endif
			<div class="header" style="font-family: '{{ $contest->template->font }}';">
            {{$contest->name}}
            </div>
        </div>




        <div class="contest parallax__layer">


            <div class="header_contest">

                @if( $contest->concept != '' && $contest->concept != null)
                    <div class="contentDescription">
                        <div class="facebook_tab_contest_description">
							<br>
                            <span style="color: {{$contest->template->subtitles_color}};font-family: '{{ $contest->template->font }}';">

                                <b>{{ trans('tab.brief') }}</b>

                            </span><br>
                            <div class="paragraph" style="color: {{$contest->font_color}}; ">{{ $contest->concept}}</div><br>
                            <span style="color: {{$contest->template->subtitles_color}}; font-family: '{{ $contest->template->font }}';"><b>{{ trans('tab.date') }}</b></span><br>
                            <div class="paragraph" style="color: {{$contest->font_color}}; ">{{ trans('tab.from') }} {{tab_HomeController::format_contest_date($contest->location_id, $contest->start_time)}} {{ trans('tab.to') }} {{tab_HomeController::format_contest_date($contest->location_id, $contest->end_time) }} {{explode('-', $contest->end_time)[0]}}</div><br>

                            <?php if ( count($contest->rewards) ) { ?>


                                <div class="paragraph" style="color: {{$contest->font_color}};">

                                    <br>
                                    <span style="color: {{$contest->template->subtitles_color}}; font-family: '{{ $contest->template->font }}';"><b>{{ trans('tab.prizes') }}</b></span>
                                    <br>

                                    <?php if ( count($contest->rewards) == 1 ) { ?>
                                        @foreach ($contest->rewards as $reward)
                                        <span style="color: {{$contest->font_color}}">{{$reward->title}}
                                        <?php if ($reward->description)  { ?>
                                            , {{$reward->description}}</span><br>
                                        <?php } else { ?>
                                            </span><br>
                                        <?php } ?>
                                        @endforeach
                                    <?php } else { ?>


                                        <?php $index = 0; ?>
                                        @foreach ($contest->rewards as $reward)
                                            <?php $index = $index+1; $final = '';
                                            if ( $contest->location_id == 1 ) {
                                                if ( $index == 1 ) {
                                                    $final = 'st';
                                                } else if ( $index == 2 ) {
                                                    $final = 'nd';
                                                } else if ( $index == 3 ) {
                                                    $final = 'rd';
                                                } else {
                                                    $final = 'th';
                                                }
                                            } else if ( $contest->location_id == 2 )  {
                                                $final = 'o';
                                            }?>
                                            <span style="color: {{$contest->font_color}}">{{$index}}<span>{{$final}}</span> {{ trans('tab.prize') }}: {{$reward->title}}
                                            <?php if ($reward->description)  { ?>
                                            , {{$reward->description}}</span><br>
                                            <?php } else { ?>
                                            </span><br>
                                            <?php } ?>
                                        @endforeach




                                    <?php } ?>

                                </div>



                            <?php } ?>


                            @if( $contest->how_to_win != '' || $contest->how_to_win != null)
                                <span style="color: {{$contest->template->subtitles_color}}; font-family: '{{ $contest->template->font }}';"><b>{{ trans('tab.how_to_win') }}</b></span><br>
                                <div class="paragraph" style="color: {{$contest->font_color}};">{{$contest->how_to_win}}</div>
                                <br>
                            @endif

                            <div class="paragraph" style="color: {{$contest->font_color}};">
                                <p style="text-align: right;">
                                    <?php if ( $contest->terms_cond ) { ?>
                                    <a style=" color: {{$contest->font_color}}; text-decoration: underline;" target="_blank" href="{{asset('/view_t_c/' . $contest->id)}}">{{ trans('tab.terms_and_cond') }}</a><br>
                                    <?php } else { ?>
                                    <a style=" color: {{$contest->font_color}}; text-decoration: underline;" href="{{asset('/admin/get-terms-' . $contest->location->language . '/' . $contest->term_cond_company_sponsoring)}}" >{{ trans('tab.terms_and_cond') }}</a><br>
                                    <?php } ?>
                                </p>



                                @if (!empty($contest->company->company_link))
                                    <br><span style="color: {{$contest->font_color}}">{{ trans('tab.visit_website') }} <a href="http://{{ $contest->company->company_link }}" target="_blank">{{ $contest->company->company_link }}</a></span><br>
                                @endif


                                <?php foreach($contest->fbPages as $page): ?>
                                @if ($page->fb_pagelike_value != 0)
                                    <span style="color: {{$contest->font_color}}">{{ trans('tab.points_if_pagelike1') }} <strong> {{$page->fb_pagelike_value}} </strong> {{ trans('tab.points_if_pagelike2') }} <a href="{{ $page->fb_page_link}}">{{ $page->fb_page_name}}</a></span><br>
                                @endif
                                <span style="color: {{$contest->font_color}}">{{ trans('tab.like_the_page') }} <a href="{{ $page->fb_page_link}}" target="_blank">{{ $page->fb_page_name}}</a></span>
                                <div style="text-align:left;margin-top: 10px;margin-bottom: 10px;">
                                    <div class="fb-like" data-href="{{$page->fb_page_link}}" style="margin:auto;"
                                         data-width="100" data-layout="standard" data-action="like" data-show-faces="false" data-share="true">
                                    </div>
                                </div>

                                <?php endforeach; ?>

                                <?php foreach($contest->twPages as $page): ?>

                                <div style="text-align:left;margin-top: 10px;margin-bottom: 10px;">
                                    <a href="{{ $page->tw_page_link}}" class="twitter-follow-button" data-show-count="true" data-dnt="true">Follow {{ $page->tw_page_name}}</a>
                                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                </div>
                                <?php endforeach; ?>

                                @if (!empty($contest->instagram))
                                    <div style="text-align:left;margin-top: 10px;margin-bottom: 10px;">
                                        <a href="https://instagram.com{{ $contest->instagram }}" target="_blank"><img src="/website_public/tab/Instagram_icon.png"> {{ $contest->instagram }}</a> </span><br>
                                    </div>
                                @endif
                            </div>






                        </div>
                    </div>
                @endif
                <br>
            </div>


            <div class="buttons">
                <?php $datetime = new DateTime(); ?>
                <?php $today = $datetime->format("Y-m-d H:i:s"); ?>
                @if (($today >= $contest->start_time) && ($today <= $contest->end_time) && $contest->status == 1 )
                    @if ($contest->max_entries != '0')
                        <button class="btn btn-lg btn-block join_now_contest" style="background-color: {{$contest->template->subtitles_color}}" onclick="popup();">{{ trans('tab.join_now') }}</button>
                        <button class="btn btn-lg btn-block invite_contest" style="background-color: {{$contest->template->subtitles_color}}" onclick="invite_popup();">{{ trans('tab.invite') }}</button>
                    @else
                        <button class="btn btn-lg btn-block invite_contest_alone" style="background-color: {{$contest->template->subtitles_color}}" onclick="invite_popup();">{{ trans('tab.invite') }}</button>
                    @endif
                @else
                    @if ($contest->max_entries != '0')
                        <button class="btn btn-lg btn-block join_now_contest" style="background-color: {{$contest->template->subtitles_color}}" disabled="disabled" onclick="popup();">{{ trans('tab.join_now') }}</button>
                        <button class="btn btn-lg btn-block invite_contest" style="background-color: {{$contest->template->subtitles_color}}" disabled="disabled" onclick="invite_popup();">{{ trans('tab.invite') }}</button>
                    @else
                        <button class="btn btn-lg btn-block invite_contest_alone" style="background-color: {{$contest->template->subtitles_color}}" disabled="disabled" onclick="invite_popup();">{{ trans('tab.invite') }}</button>
                    @endif
                @endif

            </div>




        </div>

        <div class="submissions parallax__layer">

            <a id="entries" style="color: {{$contest->template->subtitles_color}}"></a>
            <?php if ( $contest->win_modality_id != 2 ) { ?>
            <div class="categories_submissions" style="width:100%">
                <div class="form-group" >
                    <div style="float:right;">
                        <select id="mySelect" class="form-control" style="color: #999999">
                            <option value="1" selected="selected">{{ trans('tab.most_viral') }}</option>
                            <option value="2">{{ trans('tab.most_recent') }}	</option>
                        </select>
                    </div>
                    <div style="height: 46px; line-height: 46px; vertical-align: middle; color: #999999; float:right;">{{trans('tab.ordered') }}&nbsp;&nbsp;</div>

                </div>
            </div>
            <?php } else { ?>
            <div style="width: 100%; height: 45px; margin: 15px 0px 15px 0px"></div>
            <?php } ?>
            <div class="hidden_clear"></div>
            <div id="id_grid" class="submissions_grid">
                <?php // Submissions load by ajax ?>
            </div>
            <div id="loadingbar">
                <img alt="loading" style="border:none" src="{{asset('images/loader.gif') }}" />
            </div>

        </div>



    </body>

<!--
<div style="background-color: #3d3d3f; width: 100%; height: 60px;">

	<div class="container_footer" >

		<div class="logo_footer">
		</div>
		<div style="float: left; color: #ffffff; margin-left: 40px;">
			<p style="line-height: 60px; margin: 0;">co.reg.n.: 02153931205 <span style="color: #c59f18;">|</span> <a style="color: #ffffff"href="mailto:info@asmisura.com">mail</a></p>

		</div>
		<div  style="float: right;">
			<a style="margin: 10px 0;" class="youtube_link_footer" href="https://www.youtube.com/user/admisuraagency" target="_blank"></a>
		</div>
		<div  style="float: right; margin-right: 10px;">
			<a style="margin: 10px 0;" class="gplus_link_footer" href="https://plus.google.com/109132852807748986774" target="_blank"></a>
		</div>
		<div  style="float: right; margin-right: 10px;">
			<a style="margin: 10px 0;" class="linkedin_link_footer" href="https://www.linkedin.com/in/admisura" target="_blank"></a>
		</div>
		<div  style="float: right; margin-right: 10px;">
			<a style="margin: 10px 0;" class="twitter_link_footer" href="https://twitter.com/admisura" target="_blank"></a>
		</div>
		<div  style="float: right; margin-right: 10px;">
			<a style="margin: 10px 0;" class="facebook_link_footer" href="https://www.facebook.com/Admisura" target="_blank"></a>
		</div>

	</div>

</div>
-->


</html>
<?php
}
?>
