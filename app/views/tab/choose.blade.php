@include('Mobile_Detect')
<?php 
$detect = new Mobile_Detect;
if ( $detect->isMobile() ) :
?>
@include('tab.choose-m')
<?php 
else:
?>
@include('tab.choose-desktop')
<?php
endif;
?>
