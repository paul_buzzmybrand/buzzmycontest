<?php 
$current_page = $submissions->getCurrentPage();
$counter = 0;
if ( $tab == "true" ) {
	$cols = 3;
} else {
	$cols = 4;
};


?>
@if( $submissions->count() > 0 )
	@foreach( $submissions as $submission)
		<?php if  ( $counter % $cols == 0 ) { ?>
		<div class="submission_row">
		<?php } ?>

			<div class="submission_content">

				
				
			
					<?php App::setlocale($submission->contest->location->language); ?>
					<?php 
					if ( $submission->contest->type_id == 3 ) {
						$submission_thumbails = explode('.', $submission->image);
					} else {
						$submission_thumbails = explode('.', $submission->filename);
					}
					?>
					
					@if ($submission->contest->type_id == '2')
					
					
					
					<div style="clear: both;"></div>
					
					<!-- icona Facebook + like Facebook posta sopra l'immagine, azione dipendente da modalità di vincita -->
					@if ((count($submission->contest->fbPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 1))
						<div style="float:left; width: 24px; height: 29px;"><a href="javascript:apri('http://www.facebook.com/sharer/sharer.php?u=http://www.buzzmybrand.co/FacebookSharePhoto/{{str_replace('jpg', 'html', $submission->filename)}}&title={{utf8_decode($submission->name)}}');" <?php if ($social_passive) { ?> onclick="return false" <?php } ?> ><img src="/website_public/tab/f.png"></img></a></div>
						<div style="float:left; width: 49px; height: 22px; margin-left: 9px;" class="fb-like-custom" ><a href="javascript:likeFunction({{$submission->fbPages->first()->pivot->fb_idphoto}});" <?php if ($social_passive) { ?> onclick="return false" <?php } ?>><img src="/website_public/tab/like-button.png"></img></a></div>
					@elseif ((count($submission->contest->fbPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 2))
						<div style="float:left; width: 24px; height: 29px;"><a href="https://www.facebook.com/photo.php?fbid={{$submission->fbPages->first()->pivot->fb_idphoto}}" target="_blank" <?php if ($social_passive) { ?> onclick="return false" <?php } ?> ><img src="/website_public/tab/f.png"></img></a></div>
					@endif
					
					
					<!-- icona Twitter posta sopra l'immagine, azione dipendente da modalità di vincita -->
					@if ((count($submission->contest->twPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 1))
						
					
						<?php $filename = str_replace('jpg', 'html', $submission->filename); ?>
						<?php $link = CommonHelper::get_short_link("http://www.buzzmybrand.co/FacebookSharePhoto/".$filename); ?>
						<div style="float:left; width: 24px; height: 29px; margin-left: 3px;"><a href="https://twitter.com/intent/tweet?url={{$link}}&text=Check it out!&via=bmbdemo" <?php if ($social_passive) { ?> onclick="return false" <?php } ?> ><img src="/website_public/tab/t.png"></img></a></div>
									
						
					@elseif ((count($submission->contest->twPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 2))
						<div style="float:left; width: 24px; height: 29px; margin-left: 3px;"><img src="/website_public/tab/t.png"></img></div>
					@endif
					
					
					
					<!-- icona Instagram posta sopra l'immagine -->
					@if ($submission->user_connected_to_instagram)
						<div style="float:left; width: 24px; height: 29px;"><a href="javascript:apri('http://www.facebook.com/sharer/sharer.php?u=http://www.buzzmybrand.co/FacebookSharePhoto/{{str_replace('jpg', 'html', $submission->filename)}}&title={{utf8_decode($submission->name)}}');" <?php if ($social_passive) { ?> onclick="return false" <?php } ?> ><img src="/website_public/tab/f.png"></img></a></div>
						<div style="float:right; width: 24px; height: 29px;"><a href="{{ $submission->ig_source_url }}" target="_blank" <?php if ($social_passive) { ?> onclick="return false" <?php } ?> ><img src="/website_public/tab/Instagram_icon_24.png"></img></a></div>
					@endif
					
					<div style="clear: both;"></div>
					
					
					
					<div style="width: 245px;">
					
						@if ($submission->user_connected_to_instagram)
							
							<a href="javascript:void(0)" onClick="fullscreen_popup('{{ asset('http://www.buzzmybrand.co/photos/' . $submission->filename) }}', '{{  trim(preg_replace('/\s+/', ' ', str_replace("'", "\'", utf8_decode($submission->name))))    }}' );" >
								<div class="submission_media"style="background-image:url('{{ $submission->image }}')">
								</div>
							</a>
						
						@else
							
							
							
							<a href="javascript:void(0)" onClick="fullscreen_popup('{{ asset('http://www.buzzmybrand.co/photos/' . $submission->filename) }}', '{{ trim(preg_replace('/\s+/', ' ', str_replace("'", "\'", utf8_decode($submission->name)))) }}' );" >
								<div class="submission_media"style="background-image:url('{{ asset('http://www.buzzmybrand.co/images/photos/' . $submission_thumbails[0] . '-sqthumbnail.jpg') }}')">
								</div>
							</a>
							
							
						@endif
						<div class="<?php echo ( $submission->contest->win_modality_id != 2 )?"submission_info_box":"submission_info_box_2"; ?>" style="font-family: '{{ $submission->contest->template->font }}';">
							<div class="submission_title">{{ utf8_decode($submission->name)}}</div>
							<div class="submission_user">{{ $submission->user->name}}&nbsp;{{ $submission->user->surname}}</div>
							@if ($submission->contest->win_modality_id != 2)
								<h3 class="submission_rank">{{ $submission->rank_position}}</h3>
								<h4 class="submission_score_label">{{trans('tab.score')}}</h4>
								<h5 class="submission_score">{{ $submission->score}}</h5>
							@endif				
							
							<div style="clear: both;"></div>
						</div>
					
						
						
						
					
					</div>
					@elseif ($submission->contest->type_id == '1')
						
						
						
						<div style="clear: both;"></div>
						
						
						
						
						
						<!-- icona Facebook + like Facebook posta sopra l'immagine, azione dipendente da modalità di vincita -->
						@if ((count($submission->contest->fbPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 1))
							<div style="float:left; width: 24px; height: 29px;"><a href="javascript:apri('http://www.facebook.com/sharer/sharer.php?u=http://www.buzzmybrand.co/FacebookShare/{{str_replace('mp4', 'html', $submission->filename)}}&title={{utf8_decode($submission->name)}}');"><img src="/website_public/tab/f.png"></img></a></div>
							<div style="float:left; width: 49px; height: 22px; margin-left: 9px;" class="fb-like" data-href="https://www.facebook.com/video.php?v={{$submission->fbPages->first()->pivot->fb_idpost}}" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>
						@elseif ((count($submission->contest->fbPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 2))
							<div style="float:left; width: 24px; height: 29px;"><a href="https://www.facebook.com/video.php?v={{$submission->fbPages->first()->pivot->fb_idpost}}" target="_blank"><img src="/website_public/tab/f.png"></img></a></div>
						@endif
						
						
						<!-- icona Twitter posta sopra l'immagine, azione dipendente da modalità di vincita -->
						@if ((count($submission->contest->twPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 1))
							<?php $filename = str_replace('mp4', 'html', $submission->filename); ?>
							<?php $link = CommonHelper::get_short_link("http://www.buzzmybrand.co/FacebookShare/".$filename); ?>
							<div style="float:left; width: 24px; height: 29px; margin-left: 3px;"><a href="https://twitter.com/intent/tweet?url={{$link}}&text=Check it out!&via=bmbdemo"><img src="/website_public/tab/t.png"></img></a></div>
						@elseif ((count($submission->contest->twPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 2))
							<div style="float:left; width: 24px; height: 29px; margin-left: 3px;"><img src="/website_public/tab/t.png"></img></div>
						@endif
						
						<!-- icona Instagram posta sopra l'immagine -->
						@if ($submission->user_connected_to_instagram)
							<div style="float:left; width: 24px; height: 29px;"><a href="{{ $submission->ig_source_url }}" target="_blank"><img src="/website_public/tab/Instagram_icon_24.png"></img></a></div>
						@endif
						
						
						<div style="clear: both;"></div>
						
						
						
						<div style="width: 245px;">
						
						
						
							<a href="javascript:void(0)" onClick="fullscreen_popup('{{ asset('http://www.buzzmybrand.co/videos/' . $submission->filename) }}', '{{str_replace("'", "\'", utf8_decode($submission->name))}}');" >
								<div class="submission_media"style="background-image:url('{{ asset('http://www.buzzmybrand.co/images/videos/' . $submission_thumbails[0] . '-sqthumbnail.jpg') }}')">
								</div>
							</a>
							<div class="<?php echo ( $submission->contest->win_modality_id != 2 )?"submission_info_box":"submission_info_box_2"; ?>" style="font-family: '{{ $submission->contest->template->font }}';">
								<div style="margin:0 10px 4px 10px!important;padding-top:6px;font-weight: bold;font-size: small;text-align:left;color: #707173; text-overflow: ellipsis; white-space: nowrap; overflow: hidden; width: 240px;">{{ utf8_decode($submission->name)}}</div>
								<div style="margin:0 10px 4px 10px!important;font-weight: bold;text-align:left; color: #296098; font-size: 22px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden; width: 240px;">{{ $submission->user->name}}&nbsp;{{ $submission->user->surname}}</div>
								
								<?php if ( $submission->contest->win_modality_id != 2 ) { ?>
								<h3 style="float: left; width: 70px; height: 45px; padding-top: 20px; margin:0 10px 4px 10px;text-align:left; color: #707173; font-size: 42px;">{{ $submission->rank_position}}</h3>
								<h4 style="float: left; width: 135px;margin:0 10px 4px 10px!important;padding-top: 20px;font-weight: bold;font-size: small;text-align:right;color: #707173;">{{trans('tab.score')}}</h4>
								<h5 style="float: left; width: 135px; margin:0 10px 4px 10px!important;font-weight: bold;font-size: 22px;text-align:right;color: #a52828;">{{ $submission->score}}</h5>
								<?php } ?>
								
								
								<div style="clear: both;"></div>
							</div>
						
						</div>
					
					@elseif ($submission->contest->type_id == '3')
					
					
					
					<!-- icona Facebook + like Facebook posta sopra l'immagine, azione dipendente da modalità di vincita -->
					@if ((count($submission->contest->fbPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 1))
						<div style="float:left; width: 24px; height: 29px;"><a href="javascript:apri('http://www.facebook.com/sharer/sharer.php?u=http://www.buzzmybrand.co/FacebookShareEssay/{{str_replace('jpg', 'html', $submission->image)}}&title={{utf8_decode($submission->sentence)}}');"><img src="/website_public/tab/f.png"></img></a></div>
						<div style="float:left; width: 49px; height: 22px; margin-left: 9px;" class="fb-like-custom" ><a href="javascript:likeFunction({{$submission->fbPages->first()->pivot->fb_idessay}});"><img src="/website_public/tab/like-button.png"></img></a></div>
					@elseif ((count($submission->contest->fbPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 2))
						<div style="float:left; width: 24px; height: 29px;"><a href="https://www.facebook.com/photo.php?fbid={{$submission->fbPages->first()->pivot->fb_idessay}}" target="_blank"><img src="/website_public/tab/f.png"></img></a></div>
					@endif
					
					
					<!-- icona Twitter posta sopra l'immagine, azione dipendente da modalità di vincita -->
					@if ((count($submission->contest->twPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 1))
						<div style="float:left; width: 24px; height: 29px; margin-left: 3px;"><a href="javascript:apri('https://twitter.com/intent/tweet?url=http://www.buzzmybrand.co/FacebookShareEssay/{{str_replace('jpg', 'html', $submission->image)}}&title={{utf8_decode($submission->sentence)}}&amp;text=Check it out!+&amp;via=bmbdemo');"><img src="/website_public/tab/t.png"></img></a></div>
					@elseif ((count($submission->contest->twPages) > 0) && (empty($submission->user_connected_to_instagram)) && ($submission->contest->win_modality_id == 2))
						<div style="float:left; width: 24px; height: 29px; margin-left: 3px;"><img src="/website_public/tab/t.png"></img></div>
					@endif
									
					
					
					<div style="clear: both;"></div>
					
					
					
					<div style="width: 245px;">
					
					
						<a href="javascript:void(0)" onClick="fullscreen_popup('{{ asset('http://www.buzzmybrand.co/essays/' . $submission->image) }}', '{{str_replace("'", "\'", utf8_decode($submission->sentence))}}' );" >
							<div class="submission_media"style="background-image:url('{{ asset('http://www.buzzmybrand.co/images/essays/' . $submission_thumbails[0] . '-sqthumbnail.jpg') }}')">
							</div>
						</a>
						<div class="<?php echo ( $submission->contest->win_modality_id != 2 )?"submission_info_box":"submission_info_box_2"; ?>" style="font-family: '{{ $submission->contest->template->font }}';">
							<div style="margin:0 10px 4px 10px!important;font-weight: bold;text-align:left; color: #296098; font-size: 22px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden; width: 240px;">{{ $submission->user->name}}&nbsp;{{ $submission->user->surname}}</div>
							
							<?php if ( $submission->contest->win_modality_id != 2 ) { ?>
							<h3 style="float: left; width: 70px; height: 45px; padding-top: 20px; margin:0 10px 4px 10px;text-align:left; color: #707173; font-size: 42px;">{{ $submission->rank_position}}</h3>
							<h4 style="float: left; width: 135px; margin:0 10px 4px 10px!important;padding-top: 20px;font-size: small;text-align:right;color: #707173;">{{trans('tab.score')}}</h4>
							<h5 style="float: left; width: 135px; margin:0 10px 4px 10px!important;font-weight: bold;font-size: 22px;text-align:right;color: #a52828;">{{ $submission->score}}</h5>
							<?php } ?>
							
							
							<div style="clear: both;"></div>
						</div>
					
					</div>
					
					@endif
					
				
			
			</div>
				
				
		<?php if  ( $counter % $cols == $cols - 1 ) { ?>
		</div>
		<?php } ?>
		<?php $counter++; ?>
	@endforeach
@else
	<?php /*
	<div class="alert alert-info" role="alert">
		@if( $current_page == 1 ){{ 'There are not submissions for this contest!!!' }}
		@else {{ 'There are not others submissions for this contest!!!' }}
		@endif
	</div>
	*/ ?>
	<script>
	end = true;
	</script>
@endif
<div class="hidden_clear"></div>