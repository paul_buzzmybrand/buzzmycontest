@include('Mobile_Detect')
<?php 
$detect = new Mobile_Detect;
if ( $detect->isMobile() ) :
?>
	@if($contest_type == 1)
		@include('tab.uploadVideo')
	@elseif($contest_type == 2)
		@include('tab.uploadPicture')
	@endif
<?php 
else:
?>
@include('tab.upload_submission-desktop')
<?php
endif;
?>