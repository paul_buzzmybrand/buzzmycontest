@extends('layouts.contest_popup')
@section('step') step4 @stop
@section('button')
  <!--
  <div class="btn_choose" onClick="javascript:history.back()"></div>
    -->
@stop
@section('box')



<!-- ////////////// CUSTOMIZZAZIONE ///////////////////-->
<script>

    var num_photo = <?php if ( isset($photos) ) { echo count($photos) + 1; } else { echo 0; } ?>;
    var id_photo = num_photo - 1;
    
    $(document).ready(function(){
    
        $('#prev_photo').click(function( event ){
            event.preventDefault();
            id_photo = Math.abs((id_photo - 1) % num_photo);
            id_photo_to_hide = Math.abs((id_photo - 1) % num_photo);
            $('#photo_' + id_photo).css('display', 'block');
            $('#photo_' + id_photo_to_hide).css('display', 'none');
        });
        
        $('#succ_photo').click(function( event ){
            event.preventDefault();
            id_photo =  Math.abs((id_photo + 1) % num_photo);
            id_photo_to_hide = Math.abs((id_photo - 1) % num_photo);
            $('#photo_' + id_photo).css('display', 'block');
            $('#photo_' + id_photo_to_hide).css('display', 'none');
        });
  

    
    });

</script>

<?php if ( isset($photos) ) { $counter = 0; ?>
<div style="margin: 0 auto; text-align: center; font-size: 30px;"><a id="prev_photo"><</a>&nbsp;<a id="succ_photo">></a></div>
<?php foreach( $photos as $p) {  ?>
<div id="{{'photo_' . $counter}}" class="upload_submission" style="display: none;"> 
    <img src="{{asset('/images/photos/' . $p->image)}}" width="width" height="328" />
    <div style="width: 376px; margin: 5px auto;">
        {{$p->name;}}
    </div>
</div>
<?php $counter++; } ?>
<?php } ?>


<?php if ( isset($photos) ) { $stringId = 'id="photo_' . ($counter) . '"'; } else { $stringId = ""; } ?>
<!-- ////////////// CUSTOMIZZAZIONE ///////////////////-->

<div {{$stringId}} class="upload_submission">
	@if($contest_type == 1)
		<div style="width: 384px; height: 288px; margin: 0 auto;">
		<!--<div id='mediaplayer' ></div>-->
		
		<video width="320" height="240" autoplay controls>
		  <source src="{{$data}}" type="video/mp4">
		  Your browser does not support the video tag.
		</video>
		
		</div>
	@elseif($contest_type == 2)
		<img src="{{$data}}" width="width" height="328" />
	@endif
	
	@if($contest_type == 1)
	
		<div style="width: 376px; height: 81px; margin: 20px auto 0px auto	">
		
			{{ Form::open(array('action' => array('tab_HomeController@saveVideo'), 'method' => 'post', 'id' => 'uploadForm')) }}
				<input name="video" type='hidden' value="{{ $data }}">
				<textarea id="keyUpId" name="title" style="width: 376px; height: 60px;border: solid 1px grey" maxlength="80" placeholder="{{Lang::get("widget.creation_step.placeholder")}} {{$contest->hashtag}}" maxlength="140"></textarea>
				<input id="id_contest" name='id_contest' type='hidden' value="{{ $contest_id }}">
			{{ Form::close() }}
		
		</div>
		<div style="width: 376px; height: 21px; margin: 5px auto; font-size: 10px;">
			{{Lang::get('widget.creation_step.policy')}}
		</div>
		<div id="submit-upload-submission" class="submit-upload-submission" style="width: 376px; height: 41px; margin: 0 auto;">
		</div>
		
			
	@elseif($contest_type == 2)
	
		<div style="width: 376px; height: 75px; margin: 20px auto 0px auto	;">
		
			{{ Form::open(array('action' => array('tab_HomeController@savePhoto'), 'method' => 'post', 'id' => 'uploadForm')) }}
				<input name="photo" type='hidden' value="{{ $data }}">
				@if ((($item != '1') && (!$contest->max_entries)) || ($item == '2'))
					<textarea id="keyUpId" name="title" style="width: 376px; height: 80px;border: solid 1px grey;font-size: 12px;" placeholder="{{Lang::get('widget.creation_step.placeholder')}} '{{$contest->hashtag}}' {{Lang::get('widget.creation_step.placeholder2') }} <?php if ($contest->id == "259") { ?> Se scalerai la classifica arrivando tra i primi 20, la tua foto potrà essere scelta da una giuria e potrai vincere uno dei 5 buoni da 350 euro da spendere online <?php } ?> " maxlength="140"></textarea>	
				@else
					<div>Ti ricordiamo che questa foto serve come prova per partecipare e non verrà pubblicata sul tuo account Facebook né sulla pagina Facebook di IoRompoLeScatole.</div>
					<input id="title" name='title' type='hidden' value="Cartone smaltito">
				@endif
				<input id="id_contest" name='id_contest' type='hidden' value="{{ $contest_id }}">
				<input id="input_type" name='input_type' type='hidden' value="import">
			{{ Form::close() }}
		
		</div>
		<div style="width: 376px; height: 21px; margin: 5px auto; font-size: 10px;">
		@if (($item != '1') && (!$contest->max_entries))
			{{Lang::get('widget.creation_step.policy')}}
		@endif
		</div>
		<div style="width: 376px; height: 41px; margin: 0 auto;">
			
			
			@if ($contest->max_entries)
			<a href="javascript:history.go(-1)">
			<div id="change-submission" class="change-submission" style="display: inline-block; width: 153px; height: 41px;"></div>


			</a>			
			<div id="submit-upload-submission" class="submit-upload-submission" style="display: inline-block; width: 153px; height: 41px;"></div>
			@else
				<div id="submit-upload-submission" class="submit-upload-submission" style="display: inline-block; width: 153px; height: 41px;"></div>
			@endif
		</div>

	@elseif( false )

    <div class="uploadSubmission_button">
      <div id="uploadSubmissionTitle">
          @if($contest_type == 1)
          <form id="uploadForm" action="{{ action('tab_HomeController@saveVideo') }}" method="POST">
              <input name="video" type='hidden' value="{{ $data }}">
            @elseif($contest_type == 2)
            <form id="uploadForm" action="{{ action('tab_HomeController@savePhoto') }}" method="POST">
              <input name="photo" type='hidden' value="{{ $data }}">
              <input id="input_type" name='input_type' type='hidden' value="import">
            @endif
            <input type="text" id="inputUploadSubmission" name="title" maxlength="50" autocomplete="off" />
            <input id="id_contest" name='id_contest' type='hidden' value="{{ $contest_id }}">           
          </form>
        </div>
        <div id="submitButton" onClick=""></div>
    </div>
    
    @endif

    
</div>

<script>


var submitDisabled = true;

function change_onclick( action )
    { 
        if( action == 'disable')
        {
            $('#submitButton').attr("onclick","");
            
        }
        else if( action == 'enable')
        {
            $('#submitButton').attr("onclick","document.getElementById('uploadForm').submit();");
        }
    }
function validate()
    {
        var title = $('#inputUploadSubmission').val();
        
        if( title != '')
        {
            $('#submitButton').addClass('submitButtonActive');
            $('#submitButton').prop('disabled', false );
          change_onclick('enable');
          
            $('#submit-upload-submission').addClass('submit-upload-submission-active');
          
        }
        else
        {
          change_onclick('disable');
            $('#submitButton').removeClass('submitButtonActive');
            $('#submitButton').prop('disabled', true );  
            
            $('#submit-upload-submission').removeClass('submit-upload-submission-active');
        } 

        var title2 = $('#keyUpId').val();   
        
        if( title2 != '')
        {
            submitDisabled = false;
            $('#submit-upload-submission').addClass('submit-upload-submission-active');
    
        }
        else
        { 
            submitDisabled = true;
            $('#submit-upload-submission').removeClass('submit-upload-submission-active');
        } 
            
    }
   </script>
<script>
$(document).ready(function(){	
	$('.change-submission').addClass('change-submission-active');
	<?php if (isset($item)) { ?>
	
		<?php if ((($item != '1') && (!$contest->max_entries)) || ($item == '2')) { ?>
		
			$('#inputUploadSubmission').keyup(function(){
				validate();
			});
		  
			$('#keyUpId').keyup(function(){
				validate();
			});
			
		<?php } else {  ?>
		
			$('#submit-upload-submission').addClass('submit-upload-submission-active');
		
			$('#submitButton').attr("onclick","document.getElementById('uploadForm').submit();");
		
		<?php } ?>
	
	<?php } else { ?>
	
		$('#inputUploadSubmission').keyup(function(){
				validate();
			});
		  
			$('#keyUpId').keyup(function(){
				validate();
			});
	
		$('#submit-upload-submission').addClass('submit-upload-submission-active');
		
		$('#submitButton').attr("onclick","document.getElementById('uploadForm').submit();");
	
	<?php } ?>
	
	
	
	
	
	
	
  
	$('#submit-upload-submission').click(function() {
		/*
		if ( !submitDisabled ) {
			document.getElementById('uploadForm').submit();
		}
		*/
		
		
		<?php if (isset($item)) { ?>
		
			
			
			<?php if ((($item != '1') && (!$contest->max_entries)) || ($item == '2')) { ?>
		
				if ( !submitDisabled ) {
					document.getElementById('uploadForm').submit();
				}
				  
			<?php } else { ?>
				document.getElementById('uploadForm').submit();
			<?php } ?>		
			
			
		
		<?php } else { ?>
		
			document.getElementById('uploadForm').submit();
		
		<?php } ?>
		
		
		
		
		
	});  
});  
</script>
@stop