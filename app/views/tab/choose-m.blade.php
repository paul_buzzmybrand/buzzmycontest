@extends('layouts.contest_popup-m')
@section('step') {{$step}} @stop
@section('box')



@if( $contest_type == 1)

		
	
	
	
	
	<!-- NUOVO CODICE PER VIDEO -->
	<div class="col-md-12">
		<div class="row">

			<div  class="col-xs-12 col-md-12" style="margin-top: 40px">
				<div id="preview"><img src="{{asset('website_public/contest_popup/3-record-video.png') }}" class="uploadPicture" /></div>
				<div style="margin-top:30px;">

					<form id="form_upload_video" action="{{ action('tab_HomeController@uploadSubmission') }}" method="POST" enctype="multipart/form-data">
						<input id="input_videofile" type="file" class="hidden" accept="video/*" name="video" />
						<input type="test" class="hidden" name="contest_type" value="{{ $contest_type }}"/>
						<input type="test" class="hidden" name="contest_id" value="{{ $contest_id }}"/>
					
					</form>
					<div id="uploadFromLibrary" class="UploadYourFile uploadVideo"></div>
				</div>
				<div style="width: 100%; height: 30px; margin: 0 auto;" class="visible-xs-block visible-sm-block">
				</div>
			</div>

			<div style="clear: left"></div>

		</div>
	</div>
	
	
	
	
@elseif($contest_type == 2)

<!-- ////////////// CUSTOMIZZAZIONE ///////////////////-->
<script>

	var num_photo = <?php if ( isset($photos) ) { echo count($photos) + 1; } else { echo 0; } ?>;
	var id_photo = num_photo - 1;
	
	$(document).ready(function(){
	
		$('#prev_photo').click(function( event ){
			event.preventDefault();
			id_photo = Math.abs((id_photo - 1) % num_photo);
			id_photo_to_hide = Math.abs((id_photo - 1) % num_photo);
			$('#photo_' + id_photo).css('display', 'block');
			$('#photo_' + id_photo_to_hide).css('display', 'none');
		});
		
		$('#succ_photo').click(function( event ){
			event.preventDefault();
			id_photo =  Math.abs((id_photo + 1) % num_photo);
			id_photo_to_hide = Math.abs((id_photo - 1) % num_photo);
			$('#photo_' + id_photo).css('display', 'block');
			$('#photo_' + id_photo_to_hide).css('display', 'none');
		});
  

	
	});

</script>

<?php if ( isset($photos) ) { $counter = 0; ?>
<div style="margin: 0 auto; text-align: center; font-size: 30px;"><a id="prev_photo"><</a>&nbsp;<a id="succ_photo">></a></div>
<?php foreach( $photos as $p) {  ?>
<div id="{{'photo_' . $counter}}" style="display: none;">	
	<img src="{{asset('/images/photos/' . $p->image)}}" width="width" height="328" />
	<div class="row">
		<div  class="col-xs-12 col-md-12" >
			{{$p->name;}}
		</div>
	</div>
</div>
<?php $counter++; } ?>
<?php } ?>


<?php if ( isset($photos) ) { $stringId = 'id="photo_' . ($counter) . '"'; } else { $stringId = ""; } ?>
<!-- ////////////// CUSTOMIZZAZIONE ///////////////////-->


<div {{$stringId}}>

<div class="col-md-12">

<div class="row">
	<div  class="col-xs-12 col-md-12" >
		<?php if ( $contest->max_entries ) { 
			$item = Session::get('num_entry') + 1; 
			//echo $item . ' / ' . $contest->max_entries; ?>
			
			<?php if ($item == '1')  {?>
				<div style="margin: 0 auto;">Carica la foto del cartone smaltito correttamente (foto esempio)</div>
			<?php } else if ($item == '2') { ?>
				<div style="margin: 0 auto;">Carica il tuo selfie con la scritta #iorompolescatole, verrà pubblicata sul tuo profilo Facebook e su quello dedicato all'iniziativa</div>
			<?php } ?>
			<div style="margin: 0 auto; padding: 5px;">
			<img src="{{asset('images/events/Comieco_choose_'. $item .'.jpg') }}" >
			</div>
			
			
		<?php } ?>
	</div>
</div>

<div class="row">

	<div  class="col-xs-12 col-md-12" >
		<div id="preview">
			<img src="{{asset('website_public/contest_popup/3-record-foto.png') }}" class="uploadPicture" />
		</div>
		<div style="margin-top:30px;">
			
			<form id="form_upload" action="{{ action('tab_HomeController@savePhoto') }}" method="POST" enctype="multipart/form-data">
				<input id="input_file" style="position:absolute;top: -1000px" data-maxwidth="640" data-maxheight="640" type="file" name="photo" accept=".jpg" />
				<!-- input id="input_file" type="file" class="hidden" name="photo" accept="image/*" / -->
				<input type="text" class="hidden" name="image_base64" value=""/>
		        <input type="text" class="hidden" name="contest_type" value="{{ $contest_type }}"/>
				<input type="text" class="hidden" name="contest_id" value="{{ $contest_id }}"/>
			<div id="uploadFromLibrary" class="uploadPicture shootNow"></div>

			<div  class="row" style="width: 260px; height: 65px; margin: 10px auto;">	
				@if (!isset($item))
					<textarea id="title-video" name="title" style="width: 260px; height: 75px;border: solid 1px grey; margin-top:10px; margin-bottom:10px; font-size: 10px;" maxlength="140"	placeholder="{{Lang::get('widget.creation_step.placeholder')}} '{{$contest->hashtag}}' {{Lang::get('widget.creation_step.placeholder2')}} "></textarea>
				@elseif ((($item != '1') && (!$contest->max_entries)) || ($item == '2'))
					<textarea id="title-video" name="title" style="width: 260px; height: 75px;border: solid 1px grey; margin-top:10px; margin-bottom:10px; font-size: 10px;" maxlength="140"	placeholder="{{Lang::get('widget.creation_step.placeholder')}} '{{$contest->hashtag}}' {{Lang::get('widget.creation_step.placeholder2')}} Se scalerai la classifica arrivando tra i primi 20, la tua foto potrà essere scelta da una giuria e potrai vincere uno dei 5 buoni da 350 euro da spendere online"></textarea>
				@else
					<div class="description_foto" style="display: none;">Ti ricordiamo che questa foto serve come prova per partecipare e non verrà pubblicata sul tuo account Facebook né sulla pagina Facebook di IoRompoLeScatole.</div>
					<input id="title-video" name='title' type='hidden' value="Cartone smaltito">
				@endif
			</div>

			<div id="uploadSubmission" style="display: none; margin-top: 20px;">
				<?php if ( $contest->location_id == 2 ) { ?>
				<img id="foto-submit" src="{{asset('website_public/contest_popup/conferma-upload.png') }}" width="260" height="auto"/>
				<?php } else { ?>
				<img id="foto-submit" src="{{asset('website_public/contest_popup/5-submit.png') }}" width="260" height="auto"/>
				<?php } ?>
			</div>

			</form>
		</div>
		<div style="width: 100%; height: 30px; margin: 0 auto;" class="visible-xs-block visible-sm-block">
		</div>	
	</div>
 
	<div style="clear: left"></div>
	
</div>
</div>

</div>



	
@endif
	@if( $contest_type == 2)
	<script type="text/javascript" src="{{ asset('website_public/js/load-image.all.min.js') }}"></script>
	@endif
	<script>
	$(document).ready(function(){
		loader( false );
		
		$(".uploadVideo").click(function(){
			$("#input_videofile").click();
		});
		
		$(".uploadPicture").click(function(){
			$("#input_file").click();
		});
		
		
		$(".recordVideo").click(function(){
			$("#input_video").click();
		});

		$(".shootPicture").click(function(){
			$("#input_foto").click();
		});

		$("input:file").change(function (){
        	validate(this);
     	});

		
		<?php if (!isset($item)) { ?>			
			$('.submit-upload-submission').addClass('submit-upload-submission-active');
			$('#uploadSubmission').attr("onclick","document.getElementById('form_upload').submit();");

		<?php } elseif ((($item != '1') && (!$contest->max_entries)) || ($item == '2')) { ?>
			$('#title-video').keyup(function(){
				validateTitle();
			});
		<?php } else { ?>
			$('.submit-upload-submission').addClass('submit-upload-submission-active');
			$('#uploadSubmission').attr("onclick","document.getElementById('form_upload').submit();");
		<?php } ?>
     	

		$('#input_file').change(function(e) {


			loadImage.parseMetaData(e.target.files[0],
				function(data) {
					if(data.exif)
						var orientation = data.exif.get('Orientation');
					else
						var orientation = null;
					loadImage(
					e.target.files[0],
					function(canvas) {
						canvas.id = 'canvas-preview';
						var preview = document.getElementById('preview');
						var form = document.getElementById('form_upload');
						$('#preview').children().remove();
						$('#preview').css('margin-top', '50px');
						$('.description_foto').css('display', 'block');
						$('.uploadFromLibrary').removeClass('shootNow');
						$('.uploadFromLibrary').css("background-image", "url('{{asset('/website_public/contest_popup/photo-scatta.png')}}')");  
						preview.appendChild(canvas); // do the actual resized preview
						loader(false);
						$('#uploadSubmission').css('display', 'block');
						var resized = canvas.toDataURL("image/jpeg",0.8).replace(/^data:image\/(png|jpg|jpeg);base64,/, ""); // get the data from canvas as 70% JPG (can be also PNG, etc.)	
				        var newinput = document.createElement("input");
				        newinput.type = 'hidden';
				        newinput.name = 'photo';
				        newinput.value = resized; // put result from canvas into new hidden input
				        form.appendChild(newinput);
					},
					{
						maxWidth: 640,
						maxHeight: 640,
						canvas: true,
						orientation: orientation
					});
				}
			);
			
			
		});
	});
  function loader( action )
  {
    if( action == true )
      $("#new-loader").css("display","block");
    else if(action == false)
      $("#new-loader").css("display","none");
  }
  function validate(obj)
    {
      loader( true );
      $('#alert_box').html('');
    	var fileName = $(obj).val();
    	var fileNameForInput = fileName.split('\\');
    	var size = fileNameForInput.length - 1;
      if( size !== undefined) 
	  {
		var name = $(obj).attr('name');
		var id = $(obj).attr('id');
    	var input = document.getElementById(id);
       	var size_file = input.files[0].size;
    	var input_fileName = document.getElementById('fileName');
    	var input_fileNameValue = fileNameForInput[size];
    	var fileNameSplitted = input_fileNameValue.split('.');
    	var fileNameSplittedSize = fileNameSplitted.length - 1;

    	var ext_val = false;
       	var extensions_messaage = 'Error';

    	if( name == 'photo'){
       		var extensions = ['jpg', 'jpeg'];
       		extensions_messaage = 'jpg, jpeg';
          if( fileNameSplitted[fileNameSplittedSize] == 'jpg' || fileNameSplitted[fileNameSplittedSize] == 'jpeg'
            || fileNameSplitted[fileNameSplittedSize] == 'JPG' || fileNameSplitted[fileNameSplittedSize] == 'JPEG')
       			ext_val = true;
       		else
       			ext_val = false

       }
       else if ( name == 'video' )
       {
       		var extensions = 'mp4';
       		var extensions_messaage = 'mp4,mov';
       		if( fileNameSplitted[fileNameSplittedSize] == 'mp4' || fileNameSplitted[fileNameSplittedSize] == 'MP4' || fileNameSplitted[fileNameSplittedSize] == 'mov' || fileNameSplitted[fileNameSplittedSize] == 'MOV')
       			ext_val = true;
       		else
       			ext_val = false
       }
       //Validazione

       // - Dimensione MB
       
       if( size_file > 20000000){
        loader( false );
       			$('#alert_box').html('<div class="alert alert-danger" role="alert">Maximum allowed size for uploaded files: 20Mb</div>')
       	}
       	// - Estensioni
       	else if( !ext_val )
       	{
          loader( false );
       		$('#alert_box').html('<div class="alert alert-danger" role="alert">Only files with the following extensions are allowed: '+extensions_messaage+'</div>')
       	}
        else
        {
          $('#alert_box').html();
         
         if(id=="input_videofile") 
         document.getElementById("form_upload_video").submit();
         else if(id=="input_foto") {
         // document.getElementById("form_upload_foto").submit();
     	}
         else {
          // document.getElementById("form_upload").submit();
      	}
        }
      }
      else
      {
        loader( false );
      }
    }

    function change_onclick( action )
    { 
        if( action == 'disable')
        {
            $('#uploadSubmission').attr("onclick","");
            
        }
        else if( action == 'enable')
        {
            $('#uploadSubmission').attr("onclick","document.getElementById('form_upload').submit();");
        }
    }

	function validateTitle()
    {
    	var title = $('#title-video').val();
    	
       	if( title != '')
       	{
       		//$('#submitButton').addClass('submitButtonActive');
       		$('#uploadSubmission').prop('disabled', false );
          change_onclick('enable');
		  
			//$('#uploadSubmission').addClass('submit-upload-submission-active');
			
			$('.submit-upload-submission').addClass('submit-upload-submission-active');
		  
       	}
       	else
       	{
          change_onclick('disable');
       		//$('#submitButton').removeClass('submitButtonActive');
       		$('#uploadSubmission').prop('disabled', true );  
			
			//$('#submit-upload-submission').removeClass('submit-upload-submission-active');
			
			$('.submit-upload-submission').removeClass('submit-upload-submission-active');
       	} 

		
       		
    }    
	</script>
@stop