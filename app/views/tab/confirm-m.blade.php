@extends('layouts.contest_popup-m')
@section('step') {{ $step }} @stop
@section('button')
<!--
  <div class="btn_choose" onClick="javascript:history.go(-2)"></div>
  <div class="btn_join" onClick="javascript:history.back()"></div>
-->
 @stop
@section('box')
{{ HTML::style('website_public/css-m/contest.css') }}
<?php $url = "/".$contest_route; ?>

<div >
	<!--<div class="center-block finalStep" style="height:226px; margin-top: -110px;padding:10px;"></div>-->
	<img src="{{asset('images/companys/' . $contest->company->image_login) }}" style="height:150px; margin-top: 0px;padding:10px;" class="popup-content"></img>	
	<div class="center-block congratulations">{{Lang::get("widget.end_step.sentence1")}}</div>
	<div class="center-block contentuploaded">{{Lang::get("widget.end_step.sentence2")}}</div>
	<div class="center-block notified">{{Lang::get("widget.end_step.sentence3")}}</div>
	<div style="height: 2px;background-color: #a6322d;width:100%;margin-top: 10px;margin-bottom:20px;"></div>
	<!--<div class="center-block congratulations">{{trans('widget.end_step.share')}}</div>
	<div class="center-block congratulations" style="font-size: 14px">{{trans('widget.end_step.share_second_sentence')}}</div>
	<button class="btn-share-confirmation share_now_contest"></button>

	@include('tab/sharing_overlay')-->

	<!--

	@if (isset($contest->instantwin_timeslot))
		<div style="color: #a5a5a5; font-size: 14px;color: #000000;" class="center-block">Intanto scopri se hai vinto uno dei 240 buoni da 20 euro in palio, clicca il bottone sotto</div>
		<div class="center-block instantWinButton" style="max-width: 276px; height:30px; " onClick="instantwin();"></div>
		<div style="color: #a5a5a5; font-size: 12px; display: none; " class="center-block div_instantWin">
			<p class="sentence_instantWin" style="border-style: solid;border-width:medium; padding: 2px; color: #000000;></p>
			
			<div style="display: none;" class="page_likes">
					
				<?php foreach($contest->fbPages as $page): ?>					
					<span>{{ trans('tab.like_the_page') }} <a href="{{ $page->fb_page_link}}" target="_blank">{{ $page->fb_page_name}}</a></span>
					<div style="text-align:center;">
						<div class="fb-like" data-href="{{$page->fb_page_link}}" style="margin:auto;"
						data-width="100" data-layout="standard" data-action="like" data-show-faces="false" data-share="true">
						</div>
					</div>
								
				<?php endforeach; ?>
				
			</div>
		</div>
	@else
		@include('tab/sharing_overlay')
	@endif
	-->
	
	
	<div class="center-block congratulations">{{trans('widget.end_step.share')}}</div>
	<div class="center-block congratulations" style="font-size: 14px">{{trans('widget.end_step.share_second_sentence')}}</div>
	<button class="btn-share-confirmation share_now_contest"></button>
	
	@include('tab/sharing_overlay')
</div>
<div  style="max-width: 395px; height:10px; ">
</div>

<div class="center-block closeButton"  style="max-width: 276px; height:41px; " onclick="location.href='{{ URL::to( $url ) }}'"> </div>
<div style="width: 100%; height: 30px; margin: 0 auto;" class="visible-xs-block visible-sm-block"></div>	
<script type="text/javascript">
/*
$(function() {
	enableSharingOverlay();
});
*/


</script>
@stop