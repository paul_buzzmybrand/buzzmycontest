@extends('layouts.contest_popup')
@section('step') step3 @stop
@section('box')

@if( $contest_type == 1)
<div style="width: 400px; height: 100px; margin: 0 auto;">

</div>

<div style="width: 605px; margin: 0 auto;">

	<!--<div style="float: left; width: 260; height: 300px; margin: 0 20px;">-->
	<div style="text-align: center; width: 260; height: 300px; margin: 0 20px;">
		<div><img src="{{asset('website_public/contest_popup/3-upload-video.png') }}"></img></div>
		
		<div style="margin-top:30px;">
			
			<form id="form_upload" action="{{ action('tab_HomeController@uploadSubmission') }}" method="POST" enctype="multipart/form-data">
				<input id="input_file" type="file" class="hidden" name="video" />
		        <input type="test" class="hidden" name="contest_type" value="{{ $contest_type }}"/>
				<input type="test" class="hidden" name="contest_id" value="{{ $contest_id }}"/>
			</form>
			<div id="uploadFromLibrary" class="UploadYourFile"></div>
		
		</div>
		
	</div>
	<!--
	<div style="float: left; width: 260; height: 300px; margin: 0 20px;">
		<div><img src="{{asset('website_public/contest_popup/imgshoot.png') }}"></img></div>
		<div style="margin-top:28px;"><a id="recordVideoButton" href="{{ action('Website_WidgetController@record', array('contest_type' => $contest_type, 'contest_id' => $contest_id, 'user_id' => $user_id) ) }}"></a></div>
	</div>
	-->
	<div style="clear: left"></div>
	
</div>
@elseif( $contest_type == 2)
<?php if ( $contest->max_entries > 0 ) {
	$item = Session::get('num_entry') + 1; 
	//echo $item . ' / ' . $contest->max_entries; ?>

	<div style="width: 400px; height: 0px; margin: 0 auto;"></div>
	<div style="width: 500px; height: 200px; margin: 0 auto; text-align: center; font-size: 14px;">
		
		<?php if ($item == '1')  {?>
			<div style="margin: 0 auto;">Carica la foto del cartone smaltito correttamente (foto esempio)</div>
		<?php } else if ($item == '2') { ?>
			<div style="margin: 0 auto;">Carica il tuo selfie con la scritta #iorompolescatole, verrà pubblicata sul tuo profilo Facebook e su quello dedicato all'iniziativa</div>
		<?php } ?>
		<div style="margin: 0 auto;">
		<img src="{{asset('images/events/Comieco_choose_'. $item .'.jpg') }}" >
		</div>
		
		
	</div>
<?php } else { ?>

	<div style="width: 400px; height: 100px; margin: 0 auto;">
	</div>
<?php } ?>
<div style="width: 605px; margin: 0 auto;">

	<div style="float: left; width: 260; height: 200px; margin: 0 20px;">
		<div><img src="{{asset('website_public/contest_popup/imgupl.png') }}"></img></div>
		
		<div style="margin-top:30px;">
			
			<form id="form_upload" action="{{ action('tab_HomeController@uploadSubmission') }}" method="POST" enctype="multipart/form-data">
				<input id="input_file" type="file" class="hidden" name="photo" />
		        <input type="test" class="hidden" name="contest_type" value="{{ $contest_type }}"/>
				<input type="test" class="hidden" name="contest_id" value="{{ $contest_id }}"/>				
			</form>
			<div id="uploadFromLibrary" class="UploadBtn"></div>
		
		</div>
		
	</div>
	<div style="float: left; width: 260; height: 300px; margin: 0 20px;">
		<div><img src="{{asset('website_public/contest_popup/imgshoot.png') }}"></img></div>
		<div style="margin-top:28px;"><a id="takePictureButton" href="{{ action('tab_HomeController@takePicture', array('contest_type' => $contest_type, 'contest_id' => $contest_id) ) }}"></a></div>
	</div>
	<div style="clear: left"></div>
	
</div>


@else
<div class="top_box" style="height:170px;">
		@if( $contest_type == 1)
		
		<a href="{{ action('Website_WidgetController@record', array('contest_type' => $contest_type, 'contest_id' => $contest_id, 'user_id' => $user_id) ) }}">
			<div class="openRecorder"></div>
		</a>
		@elseif( $contest_type == 2)
		<a href="{{ action('tab_HomeController@takePicture', array('contest_type' => $contest_type, 'contest_id' => $contest_id) ) }}">
			<div class="shootNow"></div>
		</a>
		@endif
	</div>
	<div class="bottom_box">
		@if($contest_type == 1)
			<form id="form_upload" action="{{ action('tab_HomeController@uploadSubmission') }}" method="POST" enctype="multipart/form-data">
				<input id="input_file" type="file" class="hidden" name="video" />
		@else
			<form id="form_upload" action="{{ action('tab_HomeController@uploadSubmission') }}" method="POST" enctype="multipart/form-data">
				<input id="input_file" type="file" class="hidden" name="photo" />
		@endif
        <input type="test" class="hidden" name="contest_type" value="{{ $contest_type }}"/>
        <input type="test" class="hidden" name="contest_id" value="{{ $contest_id }}"/>
		</form>
		@if( $contest_type == 1)
      <div id="uploadFromLibrary" class="UploadYourFile"></div>
		@elseif( $contest_type == 2)
      <div id="uploadFromLibrary" class="UploadBtn"></div>
      
    @endif
	</div>
	
@endif
	<script>
	$(document).ready(function(){
    loader( false );
		$("#uploadFromLibrary").click(function(){
			$("#input_file").click();
		});
		$("input:file").change(function (){
        validate();
     	});
	});
  function loader( action )
  {
    if( action == true )
      $("#new-loader").css("display","block");
    else if(action == false)
      $("#new-loader").css("display","none");
  }
  function validate()
    {
      loader( true );
      $('#alert_box').html('');
    	var fileName = $('#input_file').val();
    	var fileNameForInput = fileName.split('\\');
    	var size = fileNameForInput.length - 1;
      if( size !== undefined)
    	{
      var name = $('#input_file').attr('name');
    	var input = document.getElementById('input_file');
       	var size_file = input.files[0].size;
    	var input_fileName = document.getElementById('fileName');
    	var input_fileNameValue = fileNameForInput[size];
    	var fileNameSplitted = input_fileNameValue.split('.');
    	var fileNameSplittedSize = fileNameSplitted.length - 1;

    	var ext_val = false;
       	var extensions_messaage = 'Error';

    	if( name == 'photo'){
       		var extensions = ['jpg', 'jpeg'];
       		extensions_messaage = 'jpg, jpeg';
          if( fileNameSplitted[fileNameSplittedSize] == 'jpg' || fileNameSplitted[fileNameSplittedSize] == 'jpeg'
            || fileNameSplitted[fileNameSplittedSize] == 'JPG' || fileNameSplitted[fileNameSplittedSize] == 'JPEG')
       			ext_val = true;
       		else
       			ext_val = false

       }
       else if ( name == 'video' )
       {
       		var extensions = 'mp4';
       		var extensions_messaage = 'mp4';
       		if( fileNameSplitted[fileNameSplittedSize] == 'mp4' || fileNameSplitted[fileNameSplittedSize] == 'MP4' )
       			ext_val = true;
       		else
       			ext_val = false
       }
       //Validazione

       // - Dimensione MB
       
       if( size_file > 8000000){
        loader( false );
       			$('#alert_box').html('<div class="alert alert-danger" role="alert">Maximum allowed size for uploaded files: 8Mb</div>')
       	}
       	// - Estensioni
       	else if( !ext_val )
       	{
          loader( false );
       		$('#alert_box').html('<div class="alert alert-danger" role="alert">Only files with the following extensions are allowed: '+extensions_messaage+'</div>')
       	}
        else
        {
          $('#alert_box').html('');
          
          document.getElementById("form_upload").submit();
        }
      }
      else
      {
        loader( false );
      }
    } 
	</script>
@stop