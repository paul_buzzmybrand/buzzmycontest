<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
         <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        {{ HTML::style('website_public/css-m/invite_popup.css') }}
       
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
        @yield('head')
    </head>
    <body style="font-family: Open Sans">
		<?php $contest = Contest::find($contest_id); ?>
		<?php $contest_name = $contest->name; ?>
		<?php $contest_link = asset("/tab/".$contest->id); ?>
		<?php $contest_caption = ""; ?>
		<?php $contest_picture = asset("/events/".$contest->image); ?>
		<?php $contest_description = ""; ?>
	<script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo $contest->company->fbApp_clientID; ?>',
          xfbml      : true,
          version    : 'v2.2'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>

		
		<div class="container">
		
		
			<div style="text-align: center; font-size: 22px; margin: 40px auto 0px auto; color: #8d8d8d">SHARE<div>
			<div style="width: 156px; height: 45px;  margin: 0 auto;">
				<div style="clear: both;"></div>
				<div style="float: left; width: 43px; height: 45px;"><a href="" onclick='shareOnFacebook();'><img src="/website_public/contest_popup/invite/fb.png" /></a></div>
				<div style="float: left; width: 43px; height: 45px; margin-left: 10px;"><a href="https://twitter.com/share" data-url="{{URL::to('tab', $contest_id)}}" data-text="Check this contest! From Buzzmybrand" data-count="none"><img src="/website_public/contest_popup/invite/tw.png"></img></a></div>
				<div style="float: left; width: 43px; height: 45px; margin-left: 10px;">
        <a href="whatsapp://send" data-text="Take a look at this awesome website:" data-href="" class="btn btn-lg btn-inverse btn-block share center-block wa_btn wa_btn_s" style="height: 45px; width: 45px; border-radius: 35px; display:none; background: #fff url(/website_public/contest_popup-m/invite/wa.png) center center no-repeat; border: none; padding: 5px; margin: 0; background-size: 45px; margin-top: -5px;"></a>
        </div>                                                                                                                                                                                                                                                                                                                                                                                          
        <div style="clear: both;"></div>
			</div>
			
			

			
	    </div>
  <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script>
function shareOnFacebook(){
	var contest_id = '{{ $contest_id }}';
    FB.ui({
        method: 'feed',
        name: '{{ $contest_name }}',
        link: '{{ $contest_link }}',
        caption: '{{ $contest_caption }}',
        picture: '{{ $contest_picture }}',
        description: '{{ $contest_description }}'
    }, function(response) {
        if(response && response.post_id){}
        else{}
    });
}
</script>
<script>
  $('.popup').click(function(event) {
    var width  = 300,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        url    = this.href,
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;
    
    window.open(url, 'twitter', opts);
 
    return false;
  });
</script>
 {{ HTML::script('website_public/js/whatsapp-button.js') }}
</body>
</html>