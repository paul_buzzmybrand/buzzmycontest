@extends('layouts.contest_popup-m')
@section('step') step4 @stop
@section('button')
  <!--
  <div class="btn_choose" onClick="javascript:history.back()"></div>
	-->
@stop
@section('box')

    
	{{ Form::open(array('action' => array('tab_HomeController@savePhoto'), 'method' => 'post', 'id' => 'uploadForm')) }}
		<input name="photo" type='hidden' value="{{ $data }}">
		<input id="input_type" name='input_type' type='hidden' value="import">
		<input id="id_contest" name='id_contest' type='hidden' value="{{ $contest_id }}">
		
		<div class="row" style="max-width: 530px; margin:20px auto 0 auto;">
			
			
			<div class="row">
				<div id="file-foto-contest" class="center-block">
					<img src="{{$data}}" width="100%" height="auto" style="padding: 0 20px;"/>
				</div>
			</div>


		</div>


		<div  class="row" style="max-width: 530px; height: 40px; margin: 10px auto;">	
			<input id="title-video" style="width: 100%; max-width: 530px; height: 40px; font-size: 18px; padding-left:10px; margin-top:10px;" type="text" name="title" maxlength="50" autocomplete="off" placeholder="{{Lang::get('widget.creation_step.title')}}"></input>

		</div>
		<div class="row" style="max-width: 530px; height: 40px; margin: 20px auto;">
		
			<div id="uploadSubmission" onClick="">
				<?php if ( $contest->location_id == 2 ) { ?>
				<img id="foto-submit" src="{{asset('website_public/contest_popup/conferma-upload.png') }}" width="260" height="auto"/>
				<?php } else { ?>
				<img id="foto-submit" src="{{asset('website_public/contest_popup/5-submit.png') }}" width="260" height="auto"/>
				<?php } ?>
			</div>
		</div>


	
	{{ Form::close() }}
<script>

var submitDisabled = true;

function change_onclick( action )
    { 
        if( action == 'disable')
        {
            $('#uploadSubmission').attr("onclick","");
            
        }
        else if( action == 'enable')
        {
            $('#uploadSubmission').attr("onclick","document.getElementById('form_upload').submit();");
        }
    }
function validate()
    {
    	var title = $('#title-video').val();
    	
       	if( title != '')
       	{
       		//$('#submitButton').addClass('submitButtonActive');
       		$('#uploadSubmission').prop('disabled', false );
          change_onclick('enable');
		  
			//$('#uploadSubmission').addClass('submit-upload-submission-active');
			
			$('.submit-upload-submission').addClass('submit-upload-submission-active');
		  
       	}
       	else
       	{
          change_onclick('disable');
       		//$('#submitButton').removeClass('submitButtonActive');
       		$('#uploadSubmission').prop('disabled', true );  
			
			//$('#submit-upload-submission').removeClass('submit-upload-submission-active');
			
			$('.submit-upload-submission').removeClass('submit-upload-submission-active');
       	} 

		
       		
    }
   </script>
<script>
$(document).ready(function(){
	
	$('#title-video').keyup(function(){
		validate();
	});
  
  
	$('#uploadSubmission').click(function() {
		if ( !submitDisabled ) {
			document.getElementById('uploadForm').submit();
		}
	});
  
});  
</script>
@stop
