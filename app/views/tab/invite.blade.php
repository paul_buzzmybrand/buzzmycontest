<!doctype html>
@include('Mobile_Detect')
<?php 
$detect = new Mobile_Detect;
if ( $detect->isMobile() ) {
?>
@include('tab.invite-m')
<?php 
}
else{
?>

<html>
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/invite_popup.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
        @yield('head')
		
		<style>
		<?php $contest = Contest::find($contest_id); ?>
		<?php if ( $contest->location->language == "it") { ?>
			
			.sendButton
			{    
				background-image: url('{{asset('/website_public/contest_popup/invite/invita-invia.png')}}');
			}

			.buzzmybrand_invite_image1
			{
				background-image: url('{{asset('/website_public/contest_popup/invite/box-invita-amici.png')}}');
			}
			
			
			<?php } ?>
		</style>
		
		
		
		
		
		
    </head>
    <body style="font-family: Open Sans">
		<?php $contest = Contest::find($contest_id); ?>
		<?php
			$contestRoute = "http://www.buzzmybrand.co/tab/" . $contest->id;
			if ( $contest->contest_route ) {
				$contestRoute = "http://bmb.buzzmybrand.co/" . $contest->contest_route;
			}
		?>
		<?php $contest_name = $contest->name; ?>
		<?php $contest_link = $contestRoute; ?>
		<?php $contest_caption = ""; ?>
		<?php $contest_picture = asset("/events/".$contest->image); ?>
		<?php $contest_description = ""; ?>
	<script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo $contest->company->fbApp_clientID; ?>',
          xfbml      : true,
          version    : 'v2.2'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>

		
		<div class="container">
		
			<div class="buzzmybrand_invite_image1"style="width: 271px; height: 115px; margin: 20px auto 0px auto;">
			</div>
			<div style="text-align: center; font-size: 22px; margin: 40px auto 0px auto; color: #8d8d8d">{{Lang::get("tab.share")}}<div>
			<div <div style="width: 96px; height: 45px;  margin: 0 auto;">
				<div style="clear: both;"></div>
				<div style="float: left; width: 43px; height: 45px;"><a href="javascript:apri('http://www.facebook.com/sharer/sharer.php?u={{ $contest_link }}');"><img src="/website_public/contest_popup/invite/fb.png"></img></a></div>
				<!--<div style="float: left; width: 43px; height: 45px;"><a href="" onclick='shareOnFacebook();'><img src="/website_public/contest_popup/invite/fb.png"></img></a></div>-->
				<div style="float: left; width: 43px; height: 45px; margin-left: 10px;"><a href="javascript:apri('https://twitter.com/intent/tweet?url={{ $contest_link }}&amp;text={{ Lang::get('tab.check_out_contest') }}+&amp;');"><img src="/website_public/contest_popup/invite/tw.png"></img></a></div>
				<!--<div style="float:left; width: 24px; height: 29px; margin-left: 3px;"><a href="javascript:apri('https://twitter.com/intent/tweet?url=http://www.buzzmybrand.it/tab/{{$contest_id}}&amp;text=Check it out!+&amp;via=Bmbdemo');"><img src="/website_public/tab/t.png"></img></a></div>-->
				<div style="clear: both;"></div>
			</div>
			<div style="text-align: center; font-size: 22px; margin: 40px auto 0px auto; color: #8d8d8d">{{Lang::get("tab.invite_friend")}}<div>
			
			
			<form id="sendEmail" action="{{ action('tab_HomeController@sendMail') }}" method="POST">
				<input id="id_contest" name='id_contest' type='hidden' value="{{ $contest_id }}">
				<div style="width: 400px; height: 35px; margin: 30px auto 0px auto; font-size: 14px;">
					<input type="text" id="name" name="name" autocomplete="off" placeholder="{{Lang::get("tab.your_name")}}" style="width:190px; padding-left: 10px; height: 35px;"/>
					<input type="email" id="addressee_name" name="addressee_name" autocomplete="off" placeholder="{{Lang::get("tab.name_friend")}}" style="width:190px; margin-left: 10px;padding-left: 10px; height: 35px;"/>
				</div>
				<div style="width: 400px; height: 35px; font-size: 14px;  margin: 14px auto 0px auto;">
					<input type="text" id="recipient" name="recipient" autocomplete="off" placeholder="{{Lang::get("tab.email_friend")}}" style="float: left; width:255px;padding-left: 10px; height: 35px; margin-left: 3px"/>
					<div id="submit" class="sendButton" onClick="document.getElementById('sendEmail').submit(); " style="float: left; margin-left: 15px;"></div>
				</div>
					
			</form>
			
			
	<!--	
	    	<div class="content">
                <div class="inviteButton"></div>
                <div class="type">
                    <div class="facebookButton" onclick='shareOnFacebook();'></div>
					
                    
					<a class="twitter popup" href="https://twitter.com/share" data-url="{{URL::to('tab', $contest_id)}}" data-text="Check this contest! From JoinMeThere" data-count="none"><div class="twitterButton"></div></a>
						
					
                </div>
                <div class="email">				
                    <form id="sendEmail" action="{{ action('tab_HomeController@sendMail') }}" method="POST">
                        <div class="top_input">
                            <div class="div_input" style="width:150px;">
                                <input type="text" id="name" name="name" autocomplete="off" placeholder="{{Lang::get('tab.invite_your_name')}}" style="width:133px;"/>
                            </div>
                            <div class="div_input" style="width:200px;margin-left:6px;">
                                <input type="email" id="addressee_name" name="addressee_name" autocomplete="off" placeholder="{{Lang::get('tab.invite_your_friend')}}" style="width:180px;"/>
                            </div>
                        </div>
                        <div class="bottom_input">
                            <div class="div_input" style="width:250px;">
                                <input type="text" id="recipient" name="recipient" autocomplete="off" placeholder="{{Lang::get('tab.invite_your_friend_email')}}" style="width:235px;"/>
                            </div>
                            <div id="submit" class="sendButton" onClick="document.getElementById('sendEmail').submit();"></div>
                        </div>
						<input id="id_contest" name='id_contest' type='hidden' value="{{ $contest_id }}">	
                    </form>
                </div>
            </div>
			-->
			
	    </div>
	{{ HTML::script('website_public/js/bootstrap.min.js') }}
</body>
</html>
<script>
function shareOnFacebook(){
	var contest_id = '{{ $contest_id }}';
    FB.ui({
        method: 'feed',
        name: '{{ $contest_name }}',
        link: '{{ $contest_link }}',
        caption: '{{ $contest_caption }}',
        picture: '{{ $contest_picture }}',
        description: '{{ $contest_description }}'
    }, function(response) {
        if(response && response.post_id){}
        else{}
    });
}
</script>
<script>
  $('.popup').click(function(event) {
    var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        url    = this.href,
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;
    
    window.open(url, 'twitter', opts);
 
    return false;
  });
  function apri(url) { 
				newin = window.open(url,'titolo','scrollbars=no,resizable=yes, width=500,height=300,status=no,location=no,toolbar=no');
			}
</script>

<?php
}
?>