
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>WSG Terms of Services</title>
        {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap-responsive.min.css') }}
        {{ HTML::style('lib/css/CustomScrollbar.css')}}
        {{ HTML::style('lib/css/main.css')}}
    </head>
    <body style="font-family: Futura Bk">
        <div class="container">
            <div class="row">
                <iframe src="http://docs.google.com/gview?url=http://www.joinmethere.tv/WSG_SuzukiCupT&C.pdf&embedded=true" style="width:718px; height:700px;" frameborder="0"></iframe>
                    <div class="account-box" >
                        <div class="logo">
                            <img src="lib/img/logo.png" alt=""/>
                        </div>
                        
                        <div  class="row" style="font-family: Futura Bk; padding:20px;height:395px;">
                            <h2 style="color:#ffc000;margin-top: -37px;"><b>Terms and Conditions rules for contest</b></h2>
                            <div id="contentScroll" style="color:#fff;padding:5px;height:354px; ">    
                            <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                <li>World Sport Group Pte. Ltd. (also referred hereinafter as to “WSG”) will conduct (including managing, moderating, supervising and administering) the Contest as described in these Rules. Conditions of access and participation in the Contest are defined in these Terms and Conditions (also named hereinafter "Rules"). The Contest consists of making works whose characteristics are detailed hereafter (hereafter designated as "Submissions") complying with the guidelines defined onto the WSG and Suzuki Cup ‘14 Websites. In the following terms and conditions “WSG” is hereinafter referred also as to “the Company”.
Accepted Submissions in the Contest are: photos, videos, animated movies, movie editing (hereafter referred to as the "Contents").
                                </li>
							</ul>
                            <h3 style="color:#ffc000;margin-top: -37px;"><b>Winning method</b></h3>    

                            
                             <h4 style="color:#ffc000;"><b><I>2. Registration</I></b></h4>
                             <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                 <li><b>2.1</b> You may use Facebook Connect to log in with your Facebook username and password or Twitter log in to access some features of the Software or WSG Service. Your use of Facebook or Twitter is governed by the Facebook and Twitter Terms of Use and Facebook and Twitter shall be considered a Third Party Service under these Terms.We are not responsible for the operation or functionality of any social network site. Facebook is a registered trademark of Facebook, Inc. Twitter is a registered trademark of Twitter, Inc.
                                </li>
                                <li style="margin-top:25px;"><b>2.2</b> Some areas of the Service require registration for use and you must complete the registration process by providing WSG with complete and accurate information as prompted by the registration form.  
                                </li>
                                <li style="margin-top:25px;"><b>2.3</b> Upon registration for your account, you will choose a user identification name and a password. You acknowledge that you are solely responsible for keeping the confidentiality of your user identification name and password. Moreover, you understand and agree that the account is yours and accept liability for all activities and damages that may occur under your account.You agree that you will not solicit, collect or use the login credentials of other WSG users. 
                                </li>
                                <li style="margin-top:25px;"><b>2.4</b> Registration data and certain other information about you are governed by our Privacy Policy.You agree to provide and maintain true, complete and current account information.
                                </li>
                             </ul>   
                             <h4 style="color:#ffc000;"><b><I>3. Terms of Use</I></b></h4>
                             <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                 <li style="margin-top:25px;"><b>3.1</b>
                                     You agree that you will not use the Service if you are not of legal age to form a binding contract with WSG or you are a person who is legally prohibited from receiving or using the Service under the laws of the country in which you are resident or from which you access or use the Service.
                                 </li>
                                 <li style="margin-top:25px;"><b>3.2</b>
                                     You agree you will not distribute any part of the Service or the Content without prior written authorization from WSG.
                                     
                                 </li>
                                 <li style="margin-top:25px;"><b>3.3</b>
                                     You  acknowledge that you shall not create, submit or share any  any data, text, files, information,
                                     usernames, images, graphics, photos, profiles, audio and video clips, sounds, musical works, works of 
                                     authorship, applications, links and other content that in any respect:
                                 </li>
                                 <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                    <li>a) is in breach of any law, statute, regulation or byelaw of any applicable jurisdiction;</li>
                                    <li>b) is fraudulent, criminal or unlawful; </li>
                                    <li>c) may be obscene, indecent, pornographic, vulgar, profane, racist, sexist, discriminatory, offensive,
                                        harmful, harassing, threatening, abusive, hateful, menacing or defamatory or harmful to children in any way; 
                                    </li>
                                    <li>d) may infringe or breach the copyright or any intellectual property rights (including without limitation 
                                        copyright, trade mark rights and broadcasting rights) or privacy or other rights of us or any third party; 
                                    </li>
                                    <li>e) may be contrary to our interests;  </li>
                                    <li>f) is contrary to any specific rule or requirement that we stipulate on the Service in relation to a 
                                        particular part of the Service or the Service generally; or 
                                    </li>
                                    <li>g) involves your use, delivery or transmission of any viruses, unsolicited emails, trojan horses,
                                        trap doors, back doors, easter eggs, worms, time bombs, cancelbots or computer programming routines that 
                                        are intended to damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, 
                                        data or personal information.
                                    </li>
                                 </ul>   
                                 
                                 <li style="margin-top:25px;"><b>3.4</b>
                                     In case of any violation of the above, WSG reserves the right to cancel or rename your account and/or to block you from accessing the Service as well as to disclose any information as necessary to satisfy any law, regulation or governmental request, at its sole discretion. WSG also reserves the right to block the distribution of User Content through the Service. 
                                 </li>
                                 <li style="margin-top:25px;"><b>3.5</b>
                                     You agree that you will not disclose or share your password with any third parties or use your password for any unauthorized purpose and you agree that WSG, in its own discretion, may restrict, terminate or deactivate your access to the Service and its Content at any time for any reason without giving you prior notice.   
                                 </li>
                                 <li style="margin-top:25px;"><b>3.6</b>
                                    You agree not to submit unwanted email or other forms of commercial or harassing communications (i.e. "spam") to 
                                    any WSG users. 
                                 </li>
                               </ul>  
                             
                                <h4 style="color:#ffc000;"><b><I>4. Content</I></b></h4>
                                
                                <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                 <li style="margin-top:25px;"><b>4.1</b>
                                    The Service may allow you as a WSG  account holder to submit Content. With “Content” we mean any text, audio, 
                                    video, software, scripts, graphics, digital images, files, music, interactive features, comments and other
                                    materials or media you submit using the Service. You acknowledge  that whether or not Content is published,
                                    we do not guarantee any confidentiality with its respect.  
                                 </li>
                                 <li style="margin-top:25px;"><b>4.2</b>
                                    As you publish a Content utilizing the Service, that Content becomes public and  you retain all of your
                                    ownership rights in your Content, but you are required to grant limited license rights to WSG  and other
                                    users of the Service as described in the next paragraph. Such Content may also be generally available to 
                                    the public at large from search engines over internet and  you explicitly declare that you have no 
                                    expectation to confidentiality for any Content available through the Service.
                                 </li>
                                 <li style="margin-top:25px;"><b>4.3</b>
                                    As you publish a Content utilizing the Service, that Content becomes public and  you retain all of your
                                    ownership rights in your Content, but you are required to grant limited license rights to WSG  and other
                                    users of the Service as described in the next paragraph. Such Content may also be generally available to 
                                    the public at large from search engines over internet and  you explicitly declare that you have no 
                                    expectation to confidentiality for any Content available through the Service.You agree that you are solely
                                    responsible for any User Content that you Publish and for the consequences of posting or publishing it. 
                                    WSG expressly disclaims any and all liability in connection with such Content. We may, but are not required 
                                    to monitor or control the Content posted via the Service and we cannot take responsibility for such Content.
                                    
                                 </li>
                                 <li style="margin-top:25px;"><b>4.4</b>
                                    You represent and warrant that you own and will continue to own during your use of the Service the 
                                    necessary licenses, rights, consents and permissions to any Content you Publish on the Service.
                                 </li>
                                 <li style="margin-top:25px;"><b>4.5</b>
                                    You agree that you will not post or upload any Content which contains material which it is 
                                    for you to possess in the country in which you are resident or contain third party copyrighted material,
                                    unless you have permission from the rightful owner of the material. We reserve the right to remove any
                                    infringing user Content and terminate a user's access for uploading Content which is in violation of 
                                    these provisions, as soon as is commercially reasonable, without any prior notice and at our sole discretion.
                                    
                                 </li>
                                 <li style="margin-top:25px;"><b>4.6</b>
                                    WSG  expressly disclaims any liability that may derive from the user Content and abides by all laws protecting
                                    copyrights, trademarks and other intellectual property. WSG will cooperate with that third party, any law 
                                    enforcement agency, court of law or applicable regulatory body with the investigation into that infringement. 
                                    This means your personal information, including your name, address, email, IP address and other identifiable 
                                    information, may be distributed for the sole purpose of protecting third parties’ intellectual property.
                                 </li>
                                 <li style="margin-top:25px;"><b>4.7</b>
                                    In the case of violation of the Terms of Service, WSG reserve the right to limit your access to the Service 
                                    or terminate your account. WSG may also at its sole discretion limit access to the Service and/or terminate
                                    the accounts of any users who infringe any intellectual property rights of others, whether or not there is 
                                    any repeat infringement.
                                 </li>
                                </ul>
                                 <h4 style="color:#ffc000;"><b><I>5. Rights grant to Join Me There </I></b></h4>
                                    <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                        <li><b>5.1</b> 
                                            You may use your Content for purposes outside of this agreement. Though, by publishing your Content,
                                            you thereby grant WSG a worldwide, non-exclusive, royalty-free, sub-licensable and transferable 
                                            license to use, reproduce, distribute, prepare derivative works of, display and perform that Content
                                            in connection with the provision of the Service. You also grant to each user of the Service a 
                                            worldwide, non-exclusive, royalty-free license to access your Content through the Service.
                                        </li>
                                        <li style="margin-top:25px;"><b>5.2</b> 
                                            You may revoke the Content License by deleting or removing your Content from the Service. 
                                            Nonetheless, you may not revoke the Content License as it relates to any prior uses, 
                                            derivative works or already published materials created by means of the Content License prior to
                                            its revocation.  You, additionally, recognize that, despite your revocation of the Content License, 
                                            copies of your Content may survive in archived form on WSG’s servers and backups or may have been
                                            reproduced or displayed by WSG users or other third parties on other websites or other media channels 
                                            outside of WSG’s managing and control. 
                                        </li>
                                        <li style="margin-top:25px;"><b>5.3</b> 
                                            The Contents available on the Service may not be copied, reproduced, republished, uploaded, posted, 
                                            transmitted or distributed in any way. Any modification or use of the Contents for any purpose other
                                            than personal or non-commercial use is a violation of the copyright and other proprietary rights in 
                                            the Contents and is expressly prohibited.
                                        </li>
                                        <li style="margin-top:25px;"><b>5.4</b> 
                                            You hereby authorize WSG to access and use the address book, contact lists or other similar
                                            items contained in the devices to which you have downloaded the Software for purposes of your
                                            use of the WSG Service.
                                        </li>
                                        <li style="margin-top:25px;"><b>5.5</b> 
                                            WSG may run advertisements, which may be targeted to the Content or information on the Service, 
                                            queries made through the Service, or other information and you hereby agree that WSG may place such
                                            advertising and promotions on the Service. You acknowledge that we may not always identify paid
                                            services, sponsored content, or commercial communications as such. WSG is not responsible or 
                                            liable for any loss or damage of any sort incurred as the result of any such dealings or as the 
                                            result of the presence of such non-WSG advertisers or third party information on the Service. 
                                        </li>
                                        <li style="margin-top:25px;"><b>5.6</b> 
                                            WSG shall modify or adapt your Content in order to transmit, display or distribute it over computer 
                                            networks and in various media and/or make changes to your Content as are necessary to conform and 
                                            adapt that Content to any requirements or limitations of any networks, devices, services or media. 
                                        </li>
                                        
                                    <h4 style="color:#ffc000;"><b><I>6. Rights grant to Join Me There </I></b></h4>
                                    <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                        <li><b>6.1</b>
                                            WSG shall not have any liability (whether in contract, tort, negligence, misrepresentation, 
                                            restitution or under any legal head of liability) in relation to your use or inability to use or
                                            delay in use of the Service or any material in it or accessible from it or from any action for any: 
                                            
                                        </li>
                                        <ul style="color:#fff;list-style: none;">
                                            <li>a)indirect or consequential losses, damages, costs or expenses; </li>
                                            <li>b)loss of reputation; </li>
                                            <li>c)loss of business; </li>
                                            <li>d)loss of, damage to or corruption of, data; whether or not such losses were reasonably 
                                                foreseeable or we had been advised of the possibility of you incurring such losses; 
                                            </li>
                                            <li>e)any damage to your computer system or loss of data that results from the download of such material 
                                                and/or data.
                                            </li>
                                            
                                        </ul>
                                        <li style="margin-top:25px;"><b>6.2</b>
                                            WSG makes no warranty that the service will meet your requirements or that the service will be 
                                            uninterrupted, timely, secure or error free.  Whilst our best endeavor we cannot be liable if for
                                            any reason the Service is unavailable for any time or for any period. In addition, we may 
                                            occasionally need to carry out repairs, maintenance or introduce new features and functions.                              
                                        </li>
                                        <li style="margin-top:25px;"><b>6.3</b>
                                            WSG makes no warranty regarding any products or services purchased or obtained through the Service or
                                            any transactions entered into through the Service.                              
                                        </li>
                                        <li style="margin-top:25px;"><b>6.4</b>
                                            You agree to indemnify, defend and hold harmless WSG, its officers, directors, employees and agents 
                                            from and against all losses, expenses, damages and costs, including reasonable attorney’s fees, 
                                            resulting from any violation of this agreement or any activity arising from or related to your use
                                            of the Service.                              
                                        </li>
                                     </ul>
                                     <h4 style="color:#ffc000;"><b><I>7. Intellectual Property Rights</I></b></h4>
                                     <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                        <li><b>7.1</b>
                                            WSG names and logos and all related names, design marks and slogans are the trade marks or service 
                                            marks of us and may not be copied, imitated or used, in whole or in part, without the prior written 
                                            permission of WSG.
                                        </li>
                                        <li style="margin-top:25px;"><b>7.2</b>
                                            We are the owner or the licensee of all intellectual property rights in the Service, and in the
                                            material published on it. Those works are protected by copyright laws and treaties around the world.
                                            All such rights are reserved.
                                        </li>
                                        <li style="margin-top:25px;"><b>7.3</b>
                                            You may print off one copy and may download extracts of any content or page(s) from the Service for
                                            your personal reference. You must not modify the paper or digital copies of any materials you have
                                            printed off or downloaded in any way and you must not use any illustrations, photographs, video or 
                                            audio sequences or any graphics separately from any accompanying text.
                                        </li>
                                        <li style="margin-top:25px;"><b>7.4</b>
                                            You must not use any part of the materials on the Service for commercial purposes without obtaining
                                            a license to do so from us. You may not use any metatags or any other "hidden text" utilizing
                                            "Join me there” or “WSG” or any other name, trademark or product or service name of WSG without 
                                            our prior written permission.
                                        </li>
                                        <li style="margin-top:25px;"><b>7.5</b>
                                            Reference to any products, services, processes or other information, by trade name, trademark, 
                                            manufacturer, supplier or otherwise does not constitute or imply endorsement, sponsorship or 
                                            recommendation thereof by us.
                                        </li>
                                        
                                     </ul>
                                     <h4 style="color:#ffc000;"><b><I>8. Links to Other Websites</I></b></h4>
                                     <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                        <li>
                                            WSG may contain links to other web sites which are in no way connected to WSG. We do not control or
                                            monitor such third party web sites or their contents. WSG shall not be held liable for the contents
                                            of such sites and/or for the rules adopted by them in respect of, but not limited to, your privacy 
                                            and the processing of your personal data when you are visiting those web sites. Please, pay attention 
                                            when you access these web sites through the links provided on joinmethere.tv and carefully read their 
                                            terms and conditions of use and their privacy policies.
                                        </li>
                                     </ul>   
                                     <h4 style="color:#ffc000;"><b><I>9. Video Contest</I></b></h4>
                                     <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                        <li><b>9.1</b>
                                            WSG may offer to our users periodic contests on or through one of the Services (each, a ”Video Contest“).
                                            Participation in any and all Contests is strictly voluntary. Each such Video Contest shall  be subject 
                                            to and governed by this Terms of Service.
                                        </li>
                                         <li style="margin-top:25px;"><b>9.2</b>
                                           By participating to a Video Contest, you acknowledge the conditions established in this Terms of Service,
                                           and undertake to be bound by the final decisions of such a Video Contest and warrant that you are 
                                           eligible to participate in such Video Contest.
                                        </li>
                                        <li style="margin-top:25px;"><b>9.3</b>
                                           By submitting any Contest Submission, you acknowledge that you have read and agree to all of the terms,
                                           conditions and rules set forth in the Rules.
                                        </li>
                                      </ul>
                                     <h4 style="color:#ffc000;"><b><I>10. Severability</I></b></h4>
                                     <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                        <li>
                                            If any of these terms should be determined to be illegal, invalid or otherwise unenforceable by reason
                                            of the laws of any state or country in which these terms are intended to be effective, then to the
                                            extent and within the jurisdiction which that term is illegal, invalid or unenforceable, it shall be 
                                            severed and deleted and the remaining Terms of Use shall survive, remain in full force and effect and
                                            continue to be binding and enforceable.
                                        </li>
                                         
                                      </ul>
                                     <h4 style="color:#ffc000;"><b><I>11. Law and Jurisdiction</I></b></h4>
                                     <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                        <li>
                                            These Terms of Use and your use of the Website shall be governed by and construed in accordance with
                                            the law of the Republic of Singapore. You submit to the exclusive jurisdiction of the courts of the 
                                            Republic of Singapore to settle any dispute which may arise under these Terms of Service.
                                        </li>
                                         
                                     </ul>
                                     <h4 style="color:#ffc000;"><b><I>12. Minors</I></b></h4>
                                     <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                        <li>
                                            If you are under 13 years of age, you are not authorized to use the Service, with or without
                                            registering. If you are between the age of 13 and 18, you must obtain permission from your parent 
                                            or guardian to use WSG, and you must have a parent assist in the completion of registration. 
                                            In addition, if you are between the age of 13 and 18, you represent that you possess legal parental
                                            or guardian consent to use the Service.
                                        </li>
                                         
                                     </ul>
                                     
                                     <h4 style="color:#ffc000;"><b><I>13. Severability</I></b></h4>
                                     <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                        <li>
                                           If any provision of these Terms shall be unlawful, void, or for any reason unenforceable, then that 
                                           provision shall be deemed severable from these Terms and shall not affect the validity and 
                                           enforceability of any remaining provisions.
                                        </li>
                                         
                                     </ul>
                                     
                                     <h4 style="color:#ffc000;"><b><I>14. Limitation of Liability</I></b></h4>
                                     <ul style="color:#fff;list-style: none; width: 96%; margin-left: 2px;">
                                        <li>
                                           YOU EXPRESSLY UNDERSTAND AND AGREE THAT WSG WILL NOT BE LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, 
                                           CONSEQUENTIAL, EXEMPLARY DAMAGES, OR DAMAGES FOR LOSS OF PROFITS INCLUDING BUT NOT LIMITED TO, DAMAGES
                                           FOR LOSS OF GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES. UNDER NO CIRCUMSTANCES, AND UNDER NO LEGAL
                                           THEORY, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE, SHALL WSG OR ITS AFFILIATES, CONTRACTORS, EMPLOYEES,
                                           AGENTS, OR THIRD PARTY PARTNERS OR SUPPLIERS, BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, 
                                           CONSEQUENTIAL, OR EXEMPLARY DAMAGES (INCLUDING WITHOUT LIMITATION, LOSS OF PROFITS, DATA OR USE OR 
                                           COST OF COVER) ARISING OUT OF OR RELATING TO THESE TERMS OR THAT RESULT FROM YOUR USE OR THE INABILITY 
                                           TO USE THE WSG MATERIALS OR WSG SERVICE ITSELF, OR ANY OTHER INTERACTIONS WITH WSG. CERTAIN STATES 
                                           AND/OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES OR LIMITATION OF LIABILITY FOR
                                           INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE EXCLUSIONS SET FORTH ABOVE MAY NOT APPLY TO YOU. WSG IS
                                           NOT RESPONSIBLE FOR TYPOGRAPHICAL ERRORS OR OMISSIONS RELATING TO PRICING, TEXT OR PHOTOGRAPHY.
                                        </li>
                                        <li style="margin-top:25px;">
                                            If you have any question regarding the use of the Service, please send us an email at
                                            <b style="color:#7DCAFF">info@wsgworld.com</b>
                                            or by postal mail to the address below:
                                        </li>
                                         
                                     </ul>
                                     
                                     <div class="row" style="text-align: center;text-decoration:underline;font-style:italic;margin-top:25px;">
                                         World Sport Group Pte Ltd 8 Shenton Way #30-01, Singapore 068811
                                     </div>

                            </div>
                    </div>
                </div>
                    
            </div>
            
        </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" style="color:#fff; text-align: center;">
                        JMTV Pte. Ltd. - Co. Reg. n. 201314573M 
                    </div>
                </div>
            </div>
    </div>

   <script type="text/javascript" src="packages/bootstrap/js/jquery.js"></script>
        <script type="text/javascript" src="packages/bootstrap/js/bootstrap.js"></script>
        <script type="text/javascript" src="packages/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="lib/js/CustomScrollbar.js"></script> 
        <script>
            (function($){
                $(window).load(function(){
                    $("#contentScroll").mCustomScrollbar({
                        scrollButtons:{
                            enable:true
                        }
                    });

                });
            })(jQuery);

        </script>
</body>
</html>
