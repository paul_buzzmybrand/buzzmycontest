<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/magnific.css') }}
        {{ HTML::style('website_public/css/common.css') }}
		{{ HTML::style('website_public/css/rds.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
        {{ HTML::script('website_public/js/jquery.validate.min.js') }}
        <link href='https://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
		<script type="text/javascript" src="/website_public/jwplayer/jwplayer.js"></script>
		
		<script type="text/javascript">	
			
			
			function apri(url) { 
				newin = window.open(url,'titolo','scrollbars=no,resizable=yes, width=400,height=200,status=no,location=no,toolbar=no');
			} 
			
			function apri_invite(url) { 
				newin = window.open(url,'titolo','scrollbars=no,resizable=yes, width=680,height=500,status=no,location=no,toolbar=no');
			} 

		</script>
		
    </head>
    <body style="font-family: Open Sans">
		<?php App::setlocale($essay->contest->location->language); ?>
		<div class="container page">
            
			
			
			<div style="width: 100%; height: 90px; padding: 10px; margin: 0 auto; text-align: center;">
				
				
				<?php if ($essay->contest->service_type == 2) { ?>
					<img src="{{asset('images/events/' . $essay->contest->sponsor_logo) }}" width='auto' height='80px' />
				<?php } else { ?>
					<img src="{{asset('images/events/logo-site.png') }}" width='auto' height='80px'/>
				<?php } ?>
				
			</div>
			
			<!--<div class="bmb_landing_banner"></div>-->


			
			
			<div style="width: 375px; height: 70px; margin: 0 auto; line-height: 70px; text-align: center;">
				<?php if ( $essay->approval_step_id == 3 ) { ?>
				<img src="{{asset('/website_public/landing/approved.png') }}"></img>
				<?php } ?>
				<span style="font-size: 18px; color: #296098 ;margin-left: 10px;">
				<?php if ( $essay->approval_step_id == 3 ) { ?>
				{{Lang::get('messages.essay_approved')}}
				<?php } else { ?>
				{{Lang::get('messages.essay_not_approved')}}
				<?php } ?>
				</span>
			</div>
			
			<div style="width: 350px; height: auto; margin: 0 auto;">
				<img src="{{asset('/essays/'.$essay->filename)}}" height="auto" width="350px">
			</div>
			
			<div style="width: 345px; height: 50px; margin: 0 auto; line-height: 50px">
				<div style="font-size: 14px; color: #8d8d8d ;"><b>{{$essay->name}}</b></div>
			</div>
			
			
			
			<?php if (( $essay->approval_step_id == 3 ) && ( $essay->contest->company->type_id != '2' )) { ?>
			<div style="width: 345px; height: 100px; margin: 50px auto 0px auto;">
				<div style="font-size: 18px; color: #296098 ;">{{Lang::get('messages.share_to_improve_1')}}</div>
				<div style="font-size: 12px; color: #296098 ;">{{Lang::get('messages.share_to_improve_2')}}</div>
				<div style="width: 89px; height: 45px; margin: 0 auto; margin-top: 10px;">
					<div style="clear: both;"></div>
					<div style="float:left; width: 43px; height: 45px;"><a href="javascript:apri('http://www.facebook.com/sharer/sharer.php?u=http://www.buzzmybrand.com/FacebookSharePhoto/{{str_replace('jpg', 'html', $essay->filename)}}&title={{$essay->name}}');"><img src="/website_public/landing/fb-icon.png"></img></a></div>
					<div style="float:left; width: 43px; height: 45px; margin-left: 3px;"><a href="javascript:apri('https://twitter.com/intent/tweet?url=http://www.buzzmybrand.com/FacebookSharePhoto/{{str_replace('jpg', 'html', $essay->filename)}}&title={{$essay->name}}&text=Check it out!');"><img src="/website_public/landing/tw-icon.png"></img></a></div>
					<div style="clear: both;"></div>
				</div>
			</div>
			<?php } ?>
			
			
			
			<?php  
			
				$counter = 0;
				$margin_left1 = '';
				$margin_left2 = '';
				
				if ( $essay->fbPages->count() > 0 ) {
					$counter++;
					$margin_left1 = 'margin-left: 15px;';
					$margin_left2 = 'margin-left: 15px;';
				}
				if ( $essay->twPages->count() > 0 ) {
					$counter++;
					$margin_left2 = 'margin-left: 15px;';
				}
				
				$width = 231 * $counter + 15 * ($counter - 1);
			     
			?>
			
			<?php if ( $essay->approval_step_id == 3 ) { ?>
			<div style="width: {{$width}}px; height: 100px; margin: 20px auto 0px auto;">
				<div style="font-size: 18px; color: #296098 ;">{{Lang::get('messages.links')}}</div>
				<div style="clear: both;"></div>
				
				<?php if ( $essay->fbPages->count() > 0 ) { ?>
				<div style="float: left; width: 231px; height: 46px;"><a href="https://www.facebook.com/photo.php?fbid={{$essay->fbPages->first()->pivot->fb_idessay}}" target="_blank"><img src="/website_public/landing/facebook.png"></img></a></div>
				<?php } ?>
				
				<?php if ( $essay->twPages->count() > 0 ) { ?>
				<?php $tw_idphoto =  $essay->twPages->first()->pivot->tw_idphoto;
						$screen_name = $essay->user->twitterProfiles->first()['screen_name']; ?>
				
				<div style="float: left; width: 231px; height: 46px; {{$margin_left1}}"><a href="https://twitter.com/{{$screen_name}}/status/{{$tw_idphoto}}" target="_blank"><img src="/website_public/landing/twitter.png"></img></a></div>
				<?php } ?>
				
				
				
				<div style="clear: both;"></div>
			</div>
			<?php } else { ?>
			<div style="width: 700px; height: 0px; margin: 50px auto 0px auto;"></div>
			<?php } ?>
			
			<div style="width: 721px; height: 52px; margin: 0 auto 100px auto;">
				<div style="clear: both;"></div>
				
				<div style="float: left; width: 721px; height: 52px;"><a href="{{asset('/minisite/' . $essay->contest->id) }}" target="_blank">
				<?php if (( $essay->approval_step_id == 3 ) && ( $essay->contest->company->type_id != '2' )){ ?>
				<img src="/website_public/landing/contest-site.png"></img>
				<?php } else if (( $essay->approval_step_id == 1 || $essay->approval_step_id == 2 ) && ( $essay->contest->company->type_id != '2' )) { ?>
				<img src="/website_public/landing/TryAgain-minisite.png"></img>
				<?php } ?>
				</a></div>
				
				<div style="clear: both;"></div>
			</div>
			
			
			<!--
			<div class="row">
				<div class="col-md-6">
					
					
					
					
					
				</div>
				<div class="col-md-6">
					@if ($essay->approval_step_id == 2)
						<?php $messageNotif = Lang::get('messages.essay_not_approved', array('contestname' => $essay->contest->name)); ?>
					<div class="alert alert-info">{{ $messageNotif }} </div>
					@elseif ($essay->approval_step_id == 3)
						<?php $messageNotif = Lang::get('messages.essay_approved', array('contestname' => $essay->contest->name)); ?>
						<div class="alert alert-info">{{ $messageNotif }}<br>Il tuo video è stato pubblicato. Ecco dove trovarlo </div>
						<?php foreach($essay->fbPages as $page): ?>
							<div class="alert alert-info">{{ trans('messages.facebook_page') }}  "{{$page->fb_page_name}}"  <a href="https://www.facebook.com/video.php?v={{$page->pivot->fb_idpost}}" target="_blank" style='color:#333;'>CLICCA QUI!</a></div>
						<?php endforeach; ?>
							@if ($essay->fb_idpost_usr != '' && $essay->fb_idpost_usr != null)
							<div class="alert alert-info">La tua bacheca  <a href="https://www.facebook.com/video.php?v={{$essay->fb_idpost_usr}}" target="_blank" style='color:#333;'>CLICCA QUI!</a></div>
							@endif
					@else
						<div class="alert alert-info">{{ trans('messages.video_needs_approval') }}</div>
				</div>
			</div>
			-->
			
			
			
			
			
			
        </div>


{{ HTML::script('website_public/js/magnific.js') }}
        {{ HTML::script('website_public/js/popup.js') }}
        {{ HTML::script('website_public/js/bootstrap.min.js') }}
           
            <script>
            $(document).ready(function()
            {
              //Handles menu drop down`enter code here`
              $('.dropdown-menu').find('form').click(function (e) {
               e.stopPropagation();
               });
             });
             </script>
    </body>
</html>

@endif





<script>
	$(document).ready(function() {
		$(".approval-btn").change(function(e) {
			$("#approveForm").submit();
		});
	});
</script>