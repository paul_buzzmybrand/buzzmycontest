<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/magnific.css') }}
        {{ HTML::style('website_public/css/common.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="container page">
            <div class="row"> 
                <div class="col-md-4">
                    <a href="{{URL::to('/website')}}" style='color:#333;'><div class="alert alert-info">Logo</div></a>
                </div>
                <div class='col-md-5'>
                    
                </div>
                <div class="col-md-3">
                    <div class='row' style='margin-bottom:12px;'>
                         <a href="http://www.facebook.com/JoinMeThere.Tv"><img class="social" src='{{asset("/images/FB_logo.png")}}'></a>
                         <a href="http://www.youtube.com/channel/UCxX0i2Z1rDdpNSGPC1abk4g/"><img class="social" src='{{asset("/images/twitter.png")}}'></a>
                         <a href="http://twitter.com/JMTvee"><img class="social" src='{{asset("/images/youtube.png")}}'></a>
                    </div>
                    <div class='row' style='margin-bottom:10px;'>
                    @if (CommonHelper::loggedInAsCompany())
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <span class="glyphicon glyphicon-briefcase"></span>&nbsp;{{ CompanyHelper::getLoggedIn()->name }}&nbsp; <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <div class='popup'><li><a style='text-decoration:none;color:black;' href="#delete-account" data-effect="mfp-zoom-in">{{ trans('messages.delete_account') }}</a></li></div>
                              <li><a href="#">{{ trans('messages.language') }}</a></li>
                              @if (CompanyHelper::getLoggedIn()->yt_access_token)
                              <li><a href="{{ URL::action('Website_CompanyController@disconnectYouTubeAccount') }}">{{ trans('messages.disconnect_yt_account') }}</a></li>
                              @else
                              <li><a href="{{ URL::action('Website_CompanyController@associateYouTubeAccount') }}">{{ trans('messages.associate_yt_account') }}</a></li>
                              @endif
                              	<li class="divider"></li>
                              <li><a href="{{action('Website_CompanyController@logout')}}">Log out</a></li>
                            </ul>
                        </div>
                    @else
                        <div class="popup">
                            <a href="#login" data-effect="mfp-zoom-in" class='btn btn-primary login'>{{ trans('messages.log_in') }}&nbsp;<span class="glyphicon glyphicon-user"></span></a>
                            <a href="#register" data-effect="mfp-zoom-in" class='btn btn-primary'>{{ trans('messages.sign_up') }}&nbsp;<span class="glyphicon glyphicon-plus"></span></a>
                        </div>
                    @endif
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col-md-12'>
                    <nav class="navbar navbar-default" role="navigation">
                        <ul class="nav navbar-nav">
                          <li><a href="{{action('Website_CompanyController@wordpress', 'our-services')}}">{{ trans('messages.our_services') }}</a></li>
                          <li><a href="{{action('Website_CompanyController@wordpress', 'clients-and-partners')}}">{{ trans('messages.client_partners') }}</a></li>
                          <li><a href="{{action('Website_CompanyController@wordpress', 'success-stories')}}">{{ trans('messages.success_stories') }}</a></li>
                          <li><a href="{{action('Website_CompanyController@team')}}">{{ trans('messages.team') }}</a></li>
                          <li><a href="{{action('Website_CompanyController@launchContest')}}">{{ trans('messages.launch_contest') }}</a></li>
                          <li><a href="{{action('Website_CompanyController@dashboard')}}">{{ trans('messages.dashboard') }}</a></li>
                        </ul>
                    </nav>
                </div>
            </div> 
            @yield('content')    
        </div>
        @include('website.footer')
        {{ HTML::script('website_public/js/magnific.js') }}
        {{ HTML::script('website_public/js/popup.js') }}
        {{ HTML::script('website_public/js/jquery.validate.min.js') }}
        {{ HTML::script('website_public/js/bootstrap.min.js') }}
        {{ HTML::script('website_public/js/holder.js') }}
        @include('website.company.popup')
    </body>
</html>