<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/magnific.css') }}
        {{ HTML::style('website_public/css/common.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
        {{ HTML::script('website_public/js/jquery.validate.min.js') }}
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="container page">
            <div class="row"> 
                <div class="col-md-4">
                    <a href="{{URL::to('/website')}}" style='color:#333;'><div class="alert alert-info">Logo</div></a>
                </div>
                <div class='col-md-5' style="margin-top:10px;">
                    <a href="http://www.facebook.com/JoinMeThere.Tv"><img class="social" src='{{asset("/images/FB_logo.png")}}'></a>
                    <a href="http://www.youtube.com/channel/UCxX0i2Z1rDdpNSGPC1abk4g/"><img class="social" src='{{asset("/images/twitter.png")}}'></a>
                    <a href="http://twitter.com/JMTvee"><img class="social" src='{{asset("/images/youtube.png")}}'></a>
                </div>
                <div class="col-md-3 ">
                    <div class="row">
                    @if (CommonHelper::loggedInAsUser())
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <span class="glyphicon glyphicon-user"></span>&nbsp;{{ UserHelper::getLoggedIn()->username }}&nbsp; <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <div class='popup'><li><a style='text-decoration:none;color:black;' href="#change-picture" data-effect="mfp-zoom-in">{{ trans('messages.change_picture') }}</a></li></div>
                              <li><a href="#">{{ trans('messages.language') }}</a></li>
                              <div class='popup'><li><a style='text-decoration:none;color:black;' href="#delete-account" data-effect="mfp-zoom-in">{{ trans('messages.delete_account') }}</a></li></div>    
                              <li class="divider"></li>
                              <li><a href="{{action('Website_CompanyController@logout')}}">{{ trans('messages.log_out') }}</a></li>
                            </ul>
                        </div>
                        <img style="width:50px;" src='{{asset("/images/users/".UserHelper::getLoggedIn()->image)}}'>
                    @else
                        <div class="menu">
                            <div class='popup'>
                                <a href="#login" data-effect="mfp-zoom-in" class='btn btn-primary login'>{{ trans('messages.log_in') }}&nbsp;<span class="glyphicon glyphicon-user"></span></a>
                                <a href="#register" data-effect="mfp-zoom-in" class='btn btn-primary'>{{ trans('messages.sign_up') }}&nbsp;<span class="glyphicon glyphicon-plus"></span></a>
                            </div>
                        </div>
                     @endif
                    </div>
                </div>
            </div>
            @yield('content')
        </div>
        @include('website.footer')
        
        <!-- Popups -->
        
        <!-- end popups -->
        
        
        {{ HTML::script('website_public/js/magnific.js') }}
        {{ HTML::script('website_public/js/popup.js') }}
        {{ HTML::script('website_public/js/bootstrap.min.js') }}
           
            <script>
            $(document).ready(function()
            {
              //Handles menu drop down`enter code here`
              $('.dropdown-menu').find('form').click(function (e) {
               e.stopPropagation();
               });
             });
             </script>
        @include('website.user.popup')
    </body>
</html>
