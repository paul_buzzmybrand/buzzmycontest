<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/contest_popup.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
		 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		{{ HTML::script('website_public/js/datepicker-it.js') }}
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
		<link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="/website_public/jwplayer/jwplayer.js"></script>
		
		<?php if ( isset($filename) ) { ?>
		<script type="text/javascript">
			/*
			var mediaplayerid = 'mediaplayer';

			$( document ).ready(function() {
			
				
				
				if( navigator.userAgent.match(/webOS/i)
					 || navigator.userAgent.match(/iPhone/i)
					 || navigator.userAgent.match(/iPad/i)
					 || navigator.userAgent.match(/iPod/i) ) {
					
					window.location.assign("http://www.buzzmybrand.it:1935/vod/mp4:{{$filename}}/playlist.m3u8")
				 
				} else if ( navigator.userAgent.match(/Android/i) ) {
				
					window.location.assign("rtsp://www.buzzmybrand.it:1935/vod/{{$filename}}")
				
				} else {
					
					jwplayer(mediaplayerid).setup({
						'file': 'rtmp://www.buzzmybrand.it:1935/vod/mp4:{{$filename}}',
						'width': '384',
						'height': '288',
						'autostart': 'true',
						'events': {
							onComplete: function(e) {
							},
							onReady: function(e) {
								
							}
						}
					});
				
				}
			});
			*/
		</script>
		
		<?php } ?>
		
		
		
		
        @yield('head')
		
		<style>
		
			textarea { 
				border-style: none; 
				border-color: Transparent; 
				overflow: auto;        
			}
			
			textarea:focus, input:focus {
				outline: 0;
			}
	
		
			a.remove_decoration {
				text-decoration: none;
			}
			.spinner {
			  margin: 100px auto;
			  width: 40px;
			  height: 40px;
			  position: relative;
			  text-align: center;
			  
			  -webkit-animation: rotate 2.0s infinite linear;
			  animation: rotate 2.0s infinite linear;
			}

			.dot1, .dot2 {
			  width: 60%;
			  height: 60%;
			  display: inline-block;
			  position: absolute;
			  top: 0;
			  background-color: #333;
			  border-radius: 100%;
			  
			  -webkit-animation: bounce 2.0s infinite ease-in-out;
			  animation: bounce 2.0s infinite ease-in-out;
			}

			.dot2 {
			  top: auto;
			  bottom: 0px;
			  -webkit-animation-delay: -1.0s;
			  animation-delay: -1.0s;
			}

			@-webkit-keyframes rotate { 100% { -webkit-transform: rotate(360deg) }}
			@keyframes rotate { 100% { transform: rotate(360deg); -webkit-transform: rotate(360deg) }}

			@-webkit-keyframes bounce {
			  0%, 100% { -webkit-transform: scale(0.0) }
			  50% { -webkit-transform: scale(1.0) }
			}

			@keyframes bounce {
			  0%, 100% { 
				transform: scale(0.0);
				-webkit-transform: scale(0.0);
			  } 50% { 
			  transform: scale(1.0);
				-webkit-transform: scale(1.0);
			  }
			}
			
			<?php if ( $contest->location->language == "it") { ?>
			
			#recordVideoButton
			{    
				background-image: url('{{asset('/website_public/contest_popup/3-button-rec-ita.png')}}');
			}

			#recordVideoButton:hover
			{
				background-image: url('{{asset('/website_public/contest_popup/3-button-rec-hover-ita.png')}}');
			}
			
			.UploadYourFile
			{
				background-image: url('{{asset('/website_public/contest_popup/3-button-up-ita.png')}}');	
			}
			.UploadYourFile:hover
			{
				background-image: url('{{asset('/website_public/contest_popup/3-button-up-hover-ita.png')}}');
			}
			
			.closeButton {
				background-image: url('{{asset('/website_public/contest_popup/6-close-1-ita.png')}}');
			}

			.closeButton:hover {
				background-image: url('{{asset('/website_public/contest_popup/6-close-2-ita.png')}}');
			}
			
			.SubmitTakePictureActive:hover
			{
				background-image: url('{{asset('/website_public/contest_popup/conferma-shoot-hover.png')}}');
			}
			
			.SubmitTakePicture2
			{
				background-image: url('{{asset('/website_public/contest_popup/conferma-shoot.png')}}');
			}

			.submit-upload-submission {
				background-image: url('{{asset('/website_public/contest_popup/conferma-upload_new.png')}}');
			}

			.submit-upload-submission-active:hover {
				background-image: url('{{asset('/website_public/contest_popup/conferma-upload-hover_new.png')}}');
			}
			
			.change-submission {
				background-image: url('{{asset('/website_public/contest_popup/change-photo.png')}}');
			}

			.change-submission-active:hover {
				background-image: url('{{asset('/website_public/contest_popup/change-photo-hover.png')}}');
				cursor: pointer;
			}
			
			#takePictureButton {
				background-image: url('{{asset('/website_public/contest_popup/photo-scatta.png')}}');
			}

			#takePictureButton:hover {
				background-image: url('{{asset('/website_public/contest_popup/photo-scatta-hover.png')}}');
			}
			
			.UploadBtn {
				background-image:url('{{asset('/website_public/contest_popup/photo-carica-file.png')}}');
			}
			
			.UploadBtn:hover
			{
				background-image: url('{{asset('/website_public/contest_popup/photo-carica-file-hover.png')}}');
			}
			
			.facebookButton {
				background-image:url('{{asset('/website_public/contest_popup/fb-connect.png')}}');
			}
			
			.facebookButton:hover
			{
				background-image:url('{{asset('/website_public/contest_popup/fb-connect-hover.png')}}');
			}
			
			.facebookButtonWithOver {
				background-image:url('{{asset('/website_public/contest_popup/fb-connect-hover.png')}}');
			}
			
			.twitterButton {
				background-image:url('{{asset('/website_public/contest_popup/2-twitter-connect-ita.png')}}');
			}
			
			.twitterButton:hover
			{
				background-image:url('{{asset('/website_public/contest_popup/2-twitter-connect-hover-ita.png')}}');
			}
			
			.twitterButtonWithOver {
				background-image:url('{{asset('/website_public/contest_popup/2-twitter-connect-hover-ita.png')}}');
			}
				
			.sigUpButton {
				background-image:url('{{asset('/website_public/contest_popup/registrati.png')}}');
			}
			
			<?php } ?>
			
			<?php if ( !$contest->needsUserRegistration() ) { ?>
			
				<?php if ( $contest->type_id == 1 || $contest->type_id == 2 ) { ?>
				
					.step2
					{
						background-image: url('{{asset('/website_public/contest_popup/desktop-4palle-1.png')}}');	
					}
					.step3
					{
						background-image: url('{{asset('/website_public/contest_popup/desktop-4palle-2.png')}}');	
					}
					
					.step4
					{
						background-image: url('{{asset('/website_public/contest_popup/desktop-4palle-3.png')}}');	
					}
					
					.step5
					{
						background-image: url('{{asset('/website_public/contest_popup/desktop-4palle-4.png')}}');	
					}
				
				<?php } else if ( $contest->type_id == 3 ) { ?>	
				
				
					.step2
					{
						background-image: url('{{asset('/website_public/contest_popup/1-barra-minisito-3palle.png')}}');	
					}
					.step3
					{
						background-image: url('{{asset('/website_public/contest_popup/2-barra-minisito-3palle.png')}}');	
					}
					.step5
					{
						background-image: url('{{asset('/website_public/contest_popup/3-barra-minisito-3palle.png')}}');	
					}
				
				
				<?php } ?>
			
			<?php } else { ?>
			
				<?php if ( $contest->type_id == 3 ) { ?>	
				
				
					.step1
					{
						background-image: url('{{asset('/website_public/contest_popup/desktop-4palle-1.png')}}');	
					}
					.step2
					{
						background-image: url('{{asset('/website_public/contest_popup/desktop-4palle-2.png')}}');	
					}
					
					.step3	
					{
						background-image: url('{{asset('/website_public/contest_popup/desktop-4palle-3.png')}}');	
					}
					
					.step5
					{
						background-image: url('{{asset('/website_public/contest_popup/desktop-4palle-4.png')}}');	
					}
				
				
				<?php } ?>
			
			<?php } ?>
		
		
		</style>
    </head>
    <body>

		<div id="fb-root"></div>
		<script>
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3&appId=566800206740950";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
		
        <?php
        if(preg_match('/(?i)msie [1-10]/',$_SERVER['HTTP_USER_AGENT'])){
        //versione 10 o inferiore
             ?> <div class="alert alert-danger" role="alert">Update your IE version to 11</div> <?php
        }
        else{ ?>
		
		<!--
        <div id="loader">
            <img src="{{asset('website_public/contest_popup/loader.gif') }}" height="70" width="70" />
        </div>
		-->
		
		<div style="position: fixed; width: 100%; margin: 0 auto; top: 300px">
			<div id="new-loader" class="spinner" style="display: none;">
				<div class="dot1"></div>
				<div class="dot2"></div>
			</div>
		</div>
		


    	<div class="container">
			<?php if ( $contest->needsUserRegistration() ) { ?>
				<?php if ( $contest->type_id == 1 || $contest->type_id == 2 ) { ?>
					<div style="width: 800px; height: 30px; margin: 0 auto; font-size: 20px; color: #8d8d8d"> 
							<div style="float:left; width:160px; height:30px">{{Lang::get('widget.signup')}}</div>
							<div style="float:left; width:160px; height:30px">{{Lang::get('widget.connect')}}</div>
							<div style="float:left; width:160px; height:30px">{{Lang::get('widget.actions')}}</div>
							<div style="float:left; width:160px; height:30px">{{Lang::get('widget.create')}}</div>
							<?php if (isset($contest->instantwin_timeslot)) { ?><div style="float:left; width:160px; height:30px">{{Lang::get('widget.instant_win')}}</div><?php } else { ?>
							<div style="float:left; width:160px; height:30px">{{Lang::get('widget.end')}}</div><?php } ?>
					</div>
				<?php } else if ( $contest->type_id == 3 ) { ?>
					<div style="width: 844px; height: 30px; margin: 0 auto; font-size: 20px; color: #8d8d8d"> 
							<div style="float:left; width:211px; height:30px">{{Lang::get('widget.signup')}}</div>
							<div style="float:left; width:211px; height:30px">{{Lang::get('widget.connect')}}</div>
							<div style="float:left; width:211px; height:30px">{{Lang::get('widget.create')}}</div>
							<?php if (isset($contest->instantwin_timeslot)) { ?><div style="float:left; width:211px; height:30px">{{Lang::get('widget.instant_win')}}</div><?php } else { ?>
							<div style="float:left; width:211px; height:30px">{{Lang::get('widget.end')}}</div><?php } ?>
					</div>
				<?php } ?>
			<?php } else { ?>
				<?php if ( $contest->type_id == 1 || $contest->type_id == 2 ) { ?>
					<div style="width: 844px; height: 30px; margin: 0 auto; font-size: 20px; color: #8d8d8d"> 
							<div style="float:left; width:211px; height:30px">{{Lang::get('widget.connect')}}</div>
							<div style="float:left; width:211px; height:30px">{{Lang::get('widget.actions')}}</div>
							<div style="float:left; width:211px; height:30px">{{Lang::get('widget.create')}}</div>
							<?php if (isset($contest->instantwin_timeslot)) { ?>
								<div style="float:left; width:211px; height:30px">{{Lang::get('widget.instant_win')}}</div>
							<?php } else { ?>
								<div style="float:left; width:211px; height:30px">{{Lang::get('widget.end')}}</div>
							<?php } ?>
					</div>	
				<?php } else if ( $contest->type_id == 3 ) { ?>	
					<div style="width: 942px; height: 30px; margin: 0 auto; font-size: 20px; color: #8d8d8d"> 
							<div style="float:left; width:314px; height:30px">{{Lang::get('widget.connect')}}</div>
							<div style="float:left; width:314px; height:30px">{{Lang::get('widget.create')}}</div>
							<?php if (isset($contest->instantwin_timeslot)) { ?><div style="float:left; width:314px; height:30px">{{Lang::get('widget.instant_win')}}</div><?php } else { ?>
							<div style="float:left; width:314px; height:30px">{{Lang::get('widget.end')}}</div><?php } ?>
					</div>	
				<?php } ?>				

			<?php } ?>
	    	<div class="step @yield('step')"> 
                @yield('button')
            </div>
            <div id="alert_box">
                @if( isset($alert) && $alert != '')
                <div class="alert alert-danger" role="alert">{{ $alert }}</div>
                @endif
            </div>
            <div class="live_image">@yield('live_image')</div>
	    	
	    	@yield('box')

	    </div>
	{{ HTML::script('website_public/js/bootstrap.min.js') }}
    <?php } ?>
</body>
</html>