<!DOCTYPE html>
<html ng-app="landingApp">
  <head>
    <meta charset="utf-8"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
	@yield('metadata')
	
    <!-- Bootstrap -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>
    <link href="{{ asset('website_public/home_page/css/app.css') }}" rel="stylesheet">
    <!-- link href="{{ asset('website_public/home_page/img/logo-navbar.png') }}" rel="stylesheet" -->
    <link rel="stylesheet" type="text/css" href="{{ asset('website_public/home_page/js/slick/slick.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('website_public/home_page/js/slick/slick-theme.css') }}" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <style>
        #middle-pic-img {
            position: absolute;
            background-image: url('/img/landing/middle-bg.png');
            background-size: cover;
            width: 100%;
            height: 200%;
            top: -100px;
            left: 0;
        }
    
        #brands-carousel button {
            display: none !important;
        }
    </style>
    <![endif]-->
    <!--[if IE]>
    <style>
        .checkbox-fld:after {
            content: '<br />';
        }
    </style>
    <![endif]-->

    @yield('scripts-top')
		
	
  </head>
  <body>
	@yield('schema-html')
	

	
    <!-- NAVBAR START -->
    <nav class="navbar navbar-default navbar-fixed-top" id="navbar">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-links" id="btn-navbar-collapse">
                    <span class="sr-only">@lang('nav.toggle_navigation')</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="{{ asset('website_public/home_page/img/logo-navbar.png') }}" title="Buzzmybrand" /></a>
            </div>

            <div class="collapse navbar-collapse" id="navbar-links">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{asset('/')}}">@lang('nav.home')</a></li>
					<li><a href="#how-it-works" class="scroll-nav">@lang('nav.how_it_works')</a></li>
                    <li><a href="#how-are-we-different" class="scroll-nav">@lang('nav.how_we_are_different')</a></li>
                    <li><a href="#pricing" class="scroll-nav">@lang('nav.pricing')</a></li>
                    <li><a href="#launch" class="yellow scroll-nav">@lang('nav.launch_campaign')</a></li>
					<li><a href="#" class="contact-open" >@lang('nav.contact_us')</a></li>
                    <!-- LOGIN FORM -->
					<li class="dropdown" id="login-dropdown">
						<a class="dropdown-toggle signin" href="#" title="{{ Lang::get('nav.login') }}">{{ Lang::get('nav.login') }}</a>
						<div class="dropdown-menu" id="nav-login-form">
							<form name="loginForm" id="nav-login" method="POST" ng-submit="loginForm.$valid && signIn(login)" ng-controller="SignInController" novalidate>
								<div class="form-group">
									<input type="email" placeholder="e-mail" class="form-control input-sm" name="username" value="{{ Input::old('email') }}" ng-model="login.username" ng-change="emailChanged()" required>
									<input type="password" ng-if="!passwordRecoveryBlock" placeholder="password" class="form-control input-sm" name="password" ng-model="login.password" ng-change="passwordChanged()" required ng-minlength="4">
									<a ng-if="!passwordRecoveryBlock" ng-click="showPasswordRecoveryBlock()" class="forgotten-link" title="{{ Lang::get('nav.password_forgotten') }}">{{ Lang::get('nav.password_forgotten') }}</a>
									<a ng-if="passwordRecoveryBlock" ng-click="hidePasswordRecoveryBlock()" class="forgotten-link" title="{{ Lang::get('nav.password_forgotten') }}">{{ Lang::get('nav.back_to_login') }}</a>
									<p class="login-error" ng-if="!passwordRecoveryBlock && error">@{{ error }}</p>
									<p class="login-error" ng-if="passwordRecoveryBlock && error">@{{ error }}</p>
									<p class="login-success" ng-if="recoveryEmailSent">{{ Lang::get('nav.recovery_email_sent') }}</p>
									<button type="submit" ng-if="!passwordRecoveryBlock" id="btn-login" class="btn btn-login" ng-disabled="!loginForm.$valid">{{ Lang::get('nav.login') }}</button>
									<button type="submit" ng-if="passwordRecoveryBlock" id="password-recovery-link" ng-click="sendPasswordRecoveryEmail()" class="btn btn-login" ng-disabled="!loginForm.$valid">{{ Lang::get('nav.recover_your_password') }}</button>
								</div>
							</form>
						</div>
					</li>
                    <!-- END LOGIN FORM -->
                    <li><a href="#" class="info-open">@lang('nav.signup')</a></li>

                    <li><a href="{{ action('LanguageController@select', array('lang' => 'it') ) }}">ITA</a></li>
                    <li><a href="{{ action('LanguageController@select', array('lang' => 'en') ) }}">ENG</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- /NAVBAR -->

    @yield('content')

    <div id="scroll-top"><a href="#"><i class="glyphicon glyphicon-chevron-up"></i></a></div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-6" id="footer-left-block">
                    <a href="{{ asset('/home') }}"><img src="{{ asset('website_public/home_page/img/logo-navbar.png') }}" title="Buzzmybrand" width="264px" /></a>
                    <!--<p id="footer-cities">NEW YORK - MILAN - DOHA - SINGAPORE - TEL AVIV</p>
                    
					<p id="footer-reg">
                        Copyright &copy; {{ date('Y') }} Buzzmybrand S.r.l.s. All rights reserved.<br />
                        co.reg.n. : 07711470729 <span class="yellow">|</span> <a href="mailto:info@buzzmybrand.it">mail</a>
                    </p>
					-->
                </div>
				<div class="col-md-2">
					<ul class="footer-nav">
						<li><a href="{{ asset('/socialBuzz') }}">@lang('footer.social_buzz')</a></li>
						<!--<li><a href="{{ asset('/'.trans('links.awards')) }}">@lang('footer.awards')</a></li>-->
						<li><a href="{{ asset('/terms') }}">@lang('footer.terms')</a></li>
						<li><a href="{{ asset('/privacy') }}">@lang('footer.privacy')</a></li>
						<li><a href="{{ asset('/contact') }}">@lang('footer.contact')</a></li>
						<li><a href="{{ asset('/careers') }}">@lang('footer.careers')</a></li>
                        <li><a href="{{ asset('/cancellationpolicy') }}">@lang('footer.cancellationpolicy')</a></li>
						<li><a href="{{ asset('/sitemap') }}">@lang('footer.sitemap')</a></li>
					</ul>
				</div>
				<div class="col-md-2">
					<ul class="footer-nav">
						<li><a href="{{ asset('/about') }}">@lang('footer.about')</a></li>
						<li><a href="{{ asset('/blog') }}">@lang('footer.blog')</a></li>
						<!--<li><a href="https://www.dropbox.com/sh/3y7pwxmke2elap5/AAAKct4TtKnOMwFhPauuqJl4a?dl=0">@lang('footer.media_kit')</a></li>-->
						<li><a href="{{ asset('/compliance') }}">@lang('footer.compliance')</a></li>
						<li><a href="{{ asset('/our_flow') }}">@lang('footer.our_flow')</a></li>
						<li><a href="{{ asset('/faqs') }}">@lang('footer.faqs')</a></li>
                        <li><a href="{{ asset('/invoices') }}">@lang('footer.invoices')</a></li>
					</ul>
				</div>
                <div class="col-md-2">
                    <h3 class="heading footer"><span class="yellow fat">FOLLOW</span> US</h3>
                    <div id="social-links" class="icons-row">
                        <div><a href="https://www.facebook.com/pages/BuzzMyBrand/800510606652304"><div class="link-facebook"></div></a></div>
                        <div><a href="https://twitter.com/buzzmybrand_"><div class="link-twitter"></div></a></div>
                        <!--<div><a href="https://instagram.com/buzzmybrand"><div class="link-instagram"></div></a></div>
                        <div><a href="https://plus.google.com/104032167175729056293"><div class="link-googleplus"></div></a></div>
                        <div><a href="https://www.youtube.com/channel/UCZd-yCCk-kJ_fza2IPMOj-Q"><div class="link-youtube"></div></a></div>-->
                    </div>
                </div>
            </div>
        </div>
    </footer>
	


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>    

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js" type="text/javascript"></script>
    <script>
      CSRF_TOKEN = '{{ csrf_token() }}';
    </script>
    <script src="{{ asset('website_public/home_page/js/ng/landing.app.js') }}" type="text/javascript"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('website_public/home_page/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('website_public/home_page/js/bootstrap/collapse.js') }}" type="text/javascript"></script>
	
    <!--[if lt IE 9]>
    <script src="{{ asset('js/jquery.placeholder.min.js') }}"></script>
    <script type="text/javascript">
    $(function() {
        $('input, textarea').placeholder();
    });
    </script>
    <![endif]-->
    
    <script src="{{ asset('website_public/home_page/js/slick/slick.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('website_public/home_page/js/jquery.parallax-scroll.js') }}" type="text/javascript"></script>
	<script src="{{ asset('website_public/home_page/js/jquery.scrollTo.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    $('#brands-carousel').slick({
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 5,
      slidesToScroll: 1,
      centerMode: true,
      variableWidth: false,
      autoplay: true,
      autoplaySpeed: 2000,
      responsive: [
        {
          breakpoint: 1170,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            // infinite: true,
            // dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    </script>    



    <script type="text/javascript">
	
	$(document).ready(function () {
		$(document).click(function (event) {
			var clickover = $(event.target);
			var _opened = $("#navbar-links").hasClass("navbar-collapse collapse in");
			if (_opened === true && !clickover.hasClass("navbar-toggle")) {
				$("button.navbar-toggle").click();
			}
		});
	});
	

    $('[data-toggle="tooltip"]').tooltip()

    $('.scroll-nav').bind('click', function(ev) {
        ev.preventDefault();
        if($('#btn-navbar-collapse').css('display') == 'block')
            $('#btn-navbar-collapse').click();
		// Go home when elsewhere
        if($(ev.target).attr('href').indexOf('#') == 0 && window.location.pathname != '/') {
            window.location = '/' + $(ev.target).attr('href');
        }
        $('body').scrollTo($(ev.target).attr('href').replace('/',''), 500, {margin: true, offset:-20});
    });

    function hookNavigation () {
        if ($(window).scrollTop() > 20) {
            $('#navbar').addClass('navbar-fixed');
        } else {
            $('#navbar').removeClass('navbar-fixed');
        }
    }

    // Handle nav menu trasition effect on scroll
    $(window).scroll(hookNavigation);
    // For initial scroll status
    $(function() { hookNavigation() });

    $('.info-open').bind('click', function(ev) {
        ev.preventDefault();
        $('#sign-up').slideToggle(function(el) {
            $('body').scrollTo('#sign-up', 500, {margin: true, offset:-20});
        });
    });

    $('.contact-open').bind('click', function(ev) {
        ev.preventDefault();
        $('#get-in-touch').slideToggle(function(el) {
            $('body').scrollTo('#get-in-touch', 500, {margin: true, offset:-20});
        });
    });

    </script>















    @yield('scripts-bottom')

  </body>
</html>