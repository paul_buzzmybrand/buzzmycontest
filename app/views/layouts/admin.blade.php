<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/magnific.css') }}
        {{ HTML::style('website_public/css/common.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
        {{ HTML::script('website_public/js/jquery.validate.min.js') }}
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
		
		<script type="text/javascript" src="/website_public/jwplayer/jwplayer.js"></script>
		  <script type="text/javascript">
			
			
			// START MASS NOTIFICATION BOX
			function showMassNotificationBox() {
			
				$( "#send_mass_notification" ).hide();
				$( "#mass_notification_box" ).show(); 
			}
			
			function closeMassNotificationBox() {
			
				$( "#send_mass_notification" ).show();
				$( "#mass_notification_box" ).hide();	
			
			}
			// END MASS NOTIFICATION BOX
			
			
			
			// START NOTIFICATION BOX
			function showBoxNotification(t, c) {
			
				$( "#send_notificationt" + t + "c" + c ).hide();
				$( "#box_notificationst" + t + "c" + c ).show(); 
			}
			
			function closeBoxNotification(t, c) {
			
				$( "#send_notificationt" + t + "c" + c ).show();
				$( "#box_notificationst" + t + "c" + c ).hide();	
			
			}
			// END NOTIFICATION BOX
			
			function start_jw_player(tab, id, v, i) {
				
				var mediaplayerid = 'mediaplayer';

				if (tab == '0') {
					mediaplayerid += 'all';
				} else if (tab == '1') {
					mediaplayerid += 'toapp';
				} else if (tab == '3') {
					mediaplayerid += 'app';
				} else if (tab == '2') {
					mediaplayerid += 'notapp';
				}
				
				if( navigator.userAgent.match(/webOS/i)
					 || navigator.userAgent.match(/iPhone/i)
					 || navigator.userAgent.match(/iPad/i)
					 || navigator.userAgent.match(/iPod/i) ) {
					
					window.location.assign("http://www.buzzmybrand.it:1935/vod/mp4:" + v + "/playlist.m3u8")
				 
				} else if ( navigator.userAgent.match(/Android/i) ) {
				
					window.location.assign("rtsp://www.buzzmybrand.it:1935/vod/" + v)
				
				} else {
					
					jwplayer(mediaplayerid  + id).setup({
						'file': 'rtmp://www.buzzmybrand.it:1935/vod/mp4:' + v,
						'width': 'auto',
						'height': '250',
						'autostart': 'true',
						'events': {
							onComplete: function(e) {
								jwplayer(mediaplayerid + id).remove();
								$('#' + mediaplayerid + id).html('<a href="javascript:void(0)" onClick="start_jw_player(\'' + tab + '\', \'' + id + '\', \'' + v + '\', \'' + i + '\');" style=\'color:#555;\'><img class="img-thumbnail" src="/images/videos/' + i + '" width="width" height="250"/></a>');
							}
						}
					});
				
				}
				
			} 
		  </script>
	
	<?php if ( isset($contest) ) { ?>	
	<script>
		
		var cols = 1;

		var contest_type = '{{ $contest->type_id; }}';
		var contest_id = '{{ $contest->id }}';
		
		var loading = false;
        
		var endAllEntries = false;
		var endNotApprovedEntries = false;
		var endApprovedEntries = false;
		var endToApproveEntries = false;
		
		var pageAllEntries = 1;
		var pageNotApprovedEntries = 1;
		var pageApprovedEntries = 1;
		var pageToApproveEntries = 1;
		
		function updateCols() {
			
			if ( $(window).width() >= 768 && $(window).width() < 992 ) {
				cols = 2;
			} else if ( $(window).width() >= 992 && $(window).width() < 1200 ) {
				cols = 2
			} else if ( $(window).width() >= 1200 ) {
				cols = 3
			}
			
		}
		
		
		function getAllEntries() {
			
			
			
			loading = true;
			$('#loadingbar-allentries').css("display","block");
			
			if ( contest_type == 1 ) {
				
				$.post('/admin/get-all-videos' , {contest_id: contest_id, page: pageAllEntries, cols: cols} , function(results){
					
					$('#allentries .panel-body').append(results);
					pageAllEntries++;
					loading = false;
					$('#loadingbar-allentries').css("display","none");
					
				});
			
			} else if ( contest_type == 2 ) {

				$.post('/admin/get-all-photos' , {contest_id: contest_id, page: pageAllEntries, cols: cols} , function(results){
					
					$('#allentries .panel-body').append(results);
					pageAllEntries++;
					loading = false;
					$('#loadingbar-allentries').css("display","none");
					
				});
				
			}
			
		}
		
		function getNotApprovedEntries() {
			
			loading = true;
			$('#loadingbar-notapproved').css("display","block");
			
			if ( contest_type == 1 ) {
				
				$.post('/admin/get-not-approved-videos' , {contest_id: contest_id, page: pageNotApprovedEntries, cols: cols} , function(results){
					
					$('#notapproved .panel-body').append(results);
					pageNotApprovedEntries++;
					loading = false;
					$('#loadingbar-notapproved').css("display","none");
					
				});
			
			} else if ( contest_type == 2 ) {

				$.post('/admin/get-not-approved-photos' , {contest_id: contest_id, page: pageNotApprovedEntries, cols: cols} , function(results){
					
					$('#notapproved .panel-body').append(results);
					pageNotApprovedEntries++;
					loading = false;
					$('#loadingbar-notapproved').css("display","none");
					
				});
				
			}
			
		}
		
		function getApprovedEntries() {
			
			loading = true;
			$('#loadingbar-approved').css("display","block");
			
			if ( contest_type == 1 ) {
				
				$.post('/admin/get-approved-videos' , {contest_id: contest_id, page: pageApprovedEntries, cols: cols} , function(results){
					
					$('#approved .panel-body').append(results);
					pageApprovedEntries++;
					loading = false;
					$('#loadingbar-approved').css("display","none");
					
				});
			
			} else if ( contest_type == 2 ) {

				$.post('/admin/get-approved-photos' , {contest_id: contest_id, page: pageApprovedEntries, cols: cols} , function(results){
					
					$('#approved .panel-body').append(results);
					pageApprovedEntries++;
					loading = false;
					$('#loadingbar-approved').css("display","none");
					
				});
				
			}
			
		}
		
		
		function getToApproveEntries() {
			
			loading = true;
			$('#loadingbar-toapprove').css("display","block");
			
			if ( contest_type == 1 ) {
				
				$.post('/admin/get-to-approve-videos' , {contest_id: contest_id, page: pageToApproveEntries, cols: cols} , function(results){
					
					$('#toapprove .panel-body').append(results);
					pageToApproveEntries++;
					loading = false;
					$('#loadingbar-toapprove').css("display","none");
					
				});
			
			} else if ( contest_type == 2 ) {

				$.post('/admin/get-to-approve-photos' , {contest_id: contest_id, page: pageToApproveEntries, cols: cols} , function(results){
					
					$('#toapprove .panel-body').append(results);
					pageToApproveEntries++;
					loading = false;
					$('#loadingbar-toapprove').css("display","none");
					
				});
				
			}
			
		}
		
		$(document).ready(function() {
			
			$('ul.nav li').click(function(e) { 
					
				if ( $(this).children().first().html() == 'ALL ENTRIES') {
					
					if(loading == false && endAllEntries == false){
						
						if ( pageAllEntries == 1 ) {
							
							updateCols();
							getAllEntries();
						
						}
						
					}
					
				} else if ( $(this).children().first().html() == 'NOT APPROVED' ) {
				
					if(loading == false && endNotApprovedEntries == false){
						
						if ( pageNotApprovedEntries == 1 ) {
							
							updateCols();
							getNotApprovedEntries();
						
						}
						
					}
				} else if ( $(this).children().first().html() == 'APPROVED' ) {
				
					if(loading == false && endApprovedEntries == false){
						
						if ( pageApprovedEntries == 1 ) {
							
							updateCols();
							getApprovedEntries();
						
						}
						
					}
				} else if ( $(this).children().first().html() == 'TO APPROVE' ) {
				
					if(loading == false && endToApproveEntries == false){
						
						if ( pageToApproveEntries == 1 ) {
							
							updateCols();
							getToApproveEntries();
						
						}
						
					}
				}
				
			});
			
		});
		
        $(window).scroll(function(){

			if((($(window).scrollTop()+$(window).height())+300)>=$(document).height()){
	
				
				
				var classList = document.getElementById('allentries').className.split(/\s+/);
				for (var i = 0; i < classList.length; i++) {
					if (classList[i] === 'active') {
						
						if(loading == false && endAllEntries == false){
						
							updateCols();
							getAllEntries();
							
							break;
						
						}
					}
				}
					
				var classList = document.getElementById('notapproved').className.split(/\s+/);
				for (var i = 0; i < classList.length; i++) {
					if (classList[i] === 'active') {
						
						if(loading == false && endNotApprovedEntries == false){
						
							updateCols();
							getNotApprovedEntries();
							
							break;
						
						}
					}
				}
					
				var classList = document.getElementById('approved').className.split(/\s+/);
				for (var i = 0; i < classList.length; i++) {
					if (classList[i] === 'active') {
						
						if(loading == false && endApprovedEntries == false){
						
							updateCols();
							getApprovedEntries();
							
							break;
						
						}
					}
				}
				
				var classList = document.getElementById('toapprove').className.split(/\s+/);
				for (var i = 0; i < classList.length; i++) {
					if (classList[i] === 'active') {
						
						if(loading == false && endToApproveEntries == false){
						
							updateCols();
							getToApproveEntries();
							
							break;
						
						}
					}
				}
				
				
			
			}
		
		});
		
    </script>
	<?php } ?>
		
    </head>
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <body>
	<script>
  $(function () {
    $('#myTab a:last').tab('show')
  })
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=566800206740950&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	
        <div class="container page">
            <div class="row"> 
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <a href="http://www.jmtvideo.com" style='color:#333;'><img src="{{asset('/images/companys/jmt_vertical.png')}}" height="100" width="300"></a>
                </div>
                <div class='col-xs-12 col-sm-12 col-md-4 col-lg-4' style="margin-top:10px;">&nbsp;
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 ">
                    <div class="row">

						<a href="mailto:sap@buzzmybrand.it" class="btn btn-link" role="button">CONTACT US</a>
						<a href="{{action('AdminController@logout')}}" class="btn btn-link" role="button">LOGOUT</a>

						@if (CommonHelper::loggedInAsAdminUser())
							<div class="btn-group">
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
									<span class="glyphicon glyphicon-user"></span>{{ UserHelper::getAdminUserLoggedIn() }}<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu">
								  @if (UserHelper::getAdminUserLoggedIn() == 'admin')
								  <li><a href="{{ URL::to('admin/external-contests')}}"> Contests Dashboard</a></li>
								  <li><a href="{{ URL::to('admin/contest')}}"> Contests Management</a></li>
								  <li><a href="{{ URL::to('admin/company')}}"> Companies Management</a></li>
								  @endif
								  <!--<li><a href="{{action('AdminController@logout')}}">{{ trans('messages.log_out') }}</a></li>-->
								</ul>
							</div>       
						@endif
                    </div>
                </div>
            </div>
            @yield('content')
        </div>
		



        @include('website.footer')
        
        <!-- Popups -->
        
        <!-- end popups -->
        
        
        {{ HTML::script('website_public/js/magnific.js') }}
        {{ HTML::script('website_public/js/popup.js') }}
        {{ HTML::script('website_public/js/bootstrap.min.js') }}
           
            <script>
            $(document).ready(function()
            {
              //Handles menu drop down`enter code here`
              $('.dropdown-menu').find('form').click(function (e) {
               e.stopPropagation();
               });
             });
             </script>
			 
    </body>
</html>
