<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/magnific.css') }}
        {{ HTML::style('website_public/css/common.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
        {{ HTML::script('website_public/js/jquery.validate.min.js') }}
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="container page">
            <div class="row"> 
                <div class="col-md-4">
                    <a href="{{URL::to('/website')}}" style='color:#333;'><div class="alert alert-info">Logo</div></a>
                </div>
                <div class='col-md-5' style="margin-top:10px;">&nbsp;
                </div>
                <div class="col-md-3 ">
                    <div class="row">&nbsp;
                    </div>
                </div>
            </div>
            
			
			 <ol class="breadcrumb" style="text-align: left">
				<li class="active">{{ $video->name }}</li>
			</ol>

			<div class="row">
				<video width="640" height="480" controls>
					<source src="{{asset('videos/'.$video->filename)}}" type="video/mp4"/>
			Your browser does not support the video tag.
				</video>
			</div>

			@if (!$video->contest->contest_approval)
			<div class="alert alert-info">{{ trans('admin.Contest does not require video approval') }}</div>
			@elseif ($video->contest->contentApprovalDone)
			<div class="alert alert-info">{{ trans('admin.Contest approval done and video review not available any more') }}</div>
			@else
			<div class="row">
			{{ Form::open(array('action' => array('Admin_ContestController@approveVideo', $video->id), 'id' => 'approveForm')) }}
				<div class="btn-group" data-toggle="buttons" value="1">
			  <label class="approval-btn btn btn-primary">
				<input class="approval-btn" type="radio" name="approvalStep" id="option2" value="-1"/>{{ trans('admin.buttons.Reset') }}
			  </label>
			  <label class="approval-btn btn btn-primary {{ $video->approvalStep->id == 3 ? 'active' : ''}}">
				<input class="approval-btn" type="radio" name="approvalStep" id="option3" value="3"/>{{ trans('admin.buttons.Approved') }}
			  </label>
			  <label class="btn btn-primary {{ $video->approvalStep->id == 1 ? 'active' : ''}}">
				<input class="approval-btn" type="radio" name="approvalStep" id="option1" value="1"/>{{ trans('admin.buttons.No_approval') }}
			  </label>
			  <label class="approval-btn btn btn-primary {{ $video->approvalStep->id == 2 ? 'active' : ''}}">
				<input class="approval-btn" type="radio" name="approvalStep" id="option2" value="2"/>{{ trans('admin.buttons.Not_approved') }}
			  </label>
			  <label class="approval-btn btn btn-primary">
				<input class="approval-btn" type="radio" name="approvalStep" id="option2" value="-2"/>{{ trans('admin.buttons.Approve_all') }}
			  </label>
			</div>
			{{ Form::close() }}
			</div>
			
			
			
        </div>


{{ HTML::script('website_public/js/magnific.js') }}
        {{ HTML::script('website_public/js/popup.js') }}
        {{ HTML::script('website_public/js/bootstrap.min.js') }}
           
            <script>
            $(document).ready(function()
            {
              //Handles menu drop down`enter code here`
              $('.dropdown-menu').find('form').click(function (e) {
               e.stopPropagation();
               });
             });
             </script>
    </body>
</html>







<script>
	$(document).ready(function() {
		$(".approval-btn").change(function(e) {
			$("#approveForm").submit();
		});
	});
</script>
@endif

@stop
