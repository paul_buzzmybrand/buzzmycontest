<!doctype html>
<html lang="en">
    <head>
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
     
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
 <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>     
        {{ HTML::style('website_public/css-m/contest_popup.css') }}

        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
		<link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="/website_public/jwplayer/jwplayer.js"></script>

		

		
		<?php if ( isset($filename) ) { ?>
		<script type="text/javascript">
			
			var mediaplayerid = 'mediaplayer';

			$( document ).ready(function() {
			
				
				
				if( navigator.userAgent.match(/webOS/i)
					 || navigator.userAgent.match(/iPhone/i)
					 || navigator.userAgent.match(/iPad/i)
					 || navigator.userAgent.match(/iPod/i) ) {
					
					window.location.assign("http://www.buzzmybrand.it:1935/vod/mp4:{{$filename}}/playlist.m3u8")
				 
				} else if ( navigator.userAgent.match(/Android/i) ) {
				
					window.location.assign("rtsp://www.buzzmybrand.it:1935/vod/{{$filename}}")
				
				} else {
					
					jwplayer(mediaplayerid).setup({
						'file': 'rtmp://www.buzzmybrand.it:1935/vod/mp4:{{$filename}}',
						'width': '384',
						'height': '288',
						'autostart': 'true',
						'events': {
							onComplete: function(e) {
							},
							onReady: function(e) {
								$( "#mediaplayer_wrapper" ).css( "margin", "0 auto" );
							}
						}
					});
				
				}
			});
			


		</script>
		
		<?php } ?>
		
		
		
		
        @yield('head')
		
		<style>
		
			a.remove_decoration {
				text-decoration: none;
			}
			.spinner {
			  margin: 100px auto;
			  width: 40px;
			  height: 40px;
			  position: relative;
			  text-align: center;
			  
			  -webkit-animation: rotate 2.0s infinite linear;
			  animation: rotate 2.0s infinite linear;
			}

			.dot1, .dot2 {
			  width: 60%;
			  height: 60%;
			  display: inline-block;
			  position: absolute;
			  top: 0;
			  background-color: #333;
			  border-radius: 100%;
			  
			  -webkit-animation: bounce 2.0s infinite ease-in-out;
			  animation: bounce 2.0s infinite ease-in-out;
			}

			.dot2 {
			  top: auto;
			  bottom: 0px;
			  -webkit-animation-delay: -1.0s;
			  animation-delay: -1.0s;
			}

			@-webkit-keyframes rotate { 100% { -webkit-transform: rotate(360deg) }}
			@keyframes rotate { 100% { transform: rotate(360deg); -webkit-transform: rotate(360deg) }}

			@-webkit-keyframes bounce {
			  0%, 100% { -webkit-transform: scale(0.0) }
			  50% { -webkit-transform: scale(1.0) }
			}

			@keyframes bounce {
			  0%, 100% { 
				transform: scale(0.0);
				-webkit-transform: scale(0.0);
			  } 50% { 
			  transform: scale(1.0);
				-webkit-transform: scale(1.0);
			  }
			}
			
			<?php if ( $contest->location->language == "it") { ?>
			
			#recordVideoButton
			{    
				background-image: url('{{asset('/website_public/contest_popup/3-button-rec-ita.png')}}');
			}

			#recordVideoButton:hover
			{
				background-image: url('{{asset('/website_public/contest_popup/3-button-rec-hover-ita.png')}}');
			}
			
			.closeButton {
				background-image: url('{{asset('/website_public/contest_popup/6-close-1-ita.png')}}');
			}

			.closeButton:hover {
				background-image: url('{{asset('/website_public/contest_popup/6-close-2-ita.png')}}');
			}
			
			.instantWinButton {
				background-image: url('{{asset('/website_public/contest_popup/instantwinButton.png')}}');
				background-size: cover;
			}

			.instantWinButton:hover {
				background-image: url('{{asset('/website_public/contest_popup/instantwinButton-hover.png')}}');
				background-size: cover;
			}
			
			.shootNow {
				background-image: url('{{asset('/website_public/contest_popup/photo-scatta.png')}}');
			}
			
			.shootAgainNow {
				background-image: url('{{asset('/website_public/contest_popup/photo-scatta-dinuovo.png')}}');
			}

			#link-foto-contest{
				background-image: url('{{asset('/website_public/contest_popup/photo-scatta.png')}}');
			}
			#link-foto-contest:hover{ 
				background-image: url('{{asset('/website_public/contest_popup/photo-scatta-hover.png')}}');
			}
			
			.UploadYourFile
			{
				background-image:url('{{asset('/website_public/contest_popup/photo-carica-file.png')}}');

			}
			.UploadYourFile:hover
			{
				background-image: url('{{asset('/website_public/contest_popup/photo-carica-file-hover.png')}}');
			}
			
			.facebookButton {
				background-image:url('{{asset('/website_public/contest_popup/fb-connect.png')}}');
			}
			
			.facebookButton:hover
			{
				background-image:url('{{asset('/website_public/contest_popup/fb-connect-hover.png')}}');
			}
			
			.facebookButtonWithOver {
				background-image:url('{{asset('/website_public/contest_popup/fb-connect-hover.png')}}');
			}
			
			.twitterButton {
				background-image:url('{{asset('/website_public/contest_popup/2-twitter-connect-ita.png')}}');
			}
			
			.twitterButton:hover
			{
				background-image:url('{{asset('/website_public/contest_popup/2-twitter-connect-hover-ita.png')}}');
			}
			
			.twitterButtonWithOver {
				background-image:url('{{asset('/website_public/contest_popup/2-twitter-connect-hover-ita.png')}}');
			}
			
			.step5 {
				background-image:url('{{asset('/website_public/contest_popup-m/x2/mobile-top-bar-5-ita.png')}}');
			}
			
			.step4 {
				background-image:url('{{asset('/website_public/contest_popup-m/x2/mobile-top-bar-4-ita.png')}}');
			}
			
			.step3 {
				background-image:url('{{asset('/website_public/contest_popup-m/x2/mobile-top-bar-3-ita.png')}}');
			}
				
			.step2 {
				background-image:url('{{asset('/website_public/contest_popup-m/x2/mobile-top-bar-2-ita.png')}}');
			}
				
			.step1 {
				background-image:url('{{asset('/website_public/contest_popup-m/x2/mobile-top-bar-1-ita.png')}}');
			}
			
			.sigUpButton {
				background-image:url('{{asset('/website_public/contest_popup/registrati.png')}}');
			}
			
			<?php } ?>
		
		
		</style>
    </head>
    <body>

        <?php
        if(preg_match('/(?i)msie [1-10]/',$_SERVER['HTTP_USER_AGENT'])){
        //versione 10 o inferiore
             ?> <div class="alert alert-danger" role="alert">Update your IE version to 11</div> <?php
        }
        else{ ?>
		
		<!--
        <div id="loader">
            <img src="{{asset('website_public/contest_popup/loader.gif') }}" height="70" width="70" />
        </div>
		-->
		
		
		<div style="position: fixed; width: 100%; margin: 0 auto; top: 10%; z-index: 999;">
			<div id="new-loader" class="spinner" style="display: none;">
				<div class="dot1"></div>
				<div class="dot2"></div>
			</div>
		</div>
		


    	<div class="container-fluid">
			
           <div class="row"> 
	    	<div class="col-xs-12 step @yield('step')"> 
                @yield('button')
            </div>
            
            </div>
            
            <div class="row"> 
            <div id="alert_box">
                @if( isset($alert) && $alert != '')
                <div class="alert alert-danger" role="alert">{{ $alert }}</div>
                @endif
            </div>
              </div>
            <div class="live_image">@yield('live_image')</div>
	    	<div class="row box @yield('box_class')">
	    		@yield('box')
	    	</div>
	  
	
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
      
    <?php } ?>
</body>
</html>