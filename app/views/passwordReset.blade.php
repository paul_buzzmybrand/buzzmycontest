<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>JMTV Forget Password</title>
        {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap-responsive.min.css') }}
        {{ HTML::style('lib/css/main.css')}}
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" >
                        <div class="logo">
                            <img src="../lib/img/logo.png" alt=""/>
                        </div>
                        <form class="form-signin" action="{{ URL::to('resetPassword') }}" method="post">
                            
                            <div class="form-group">
                                <input type="text" name="email" class="form-control mytext" placeholder="Enter Email" />
                            </div>
                            
                            <div class="form-group">
                                <input type="password" name="password" class="form-control mytext" placeholder="Enter New Password" />
                            </div>
                            
                            <div class="form-group">
                                <input type="password" name="password_confirmation" class="form-control mytext" placeholder="Confirm Password" />
                            </div>
                            
                            <button class="btn-lg btn-block mybtn" type="submit">
                                SUBMIT
                            </button>
                          
                        </form>
                        
                        @if(Session::has('message'))
                        
                            <div class="Errormessage" style="padding:5px !important;">
                                <p style="line-height: 0px !important; text-align:left;padding:5px !important;">{{ Session::get('message') }}</p>
                                <ul>
                                    @foreach($errors->all() as $error)
                                     <li style="color:#fff;font-size: 14px;">{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        
                        @endif
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" style="color:#fff; text-align: center;">
                        JMTV Pte. Ltd. - Co. Reg. n. 201314573M 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
