<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/common.css') }}
    </head>
    <body>
        <div class="home">
            <div class="row">
                <div class="col-md-12">
                    <h1>Introductive Page</h1>
                    <p class="lead home_content" >Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    
                    <div class="row">
                        <div class="col-md-3">
                            
                        </div>
                        <div class="col-md-3">
                            <p class="lead">
                                <a href="{{URL::to('website/user')}}" class="btn btn-lg btn-primary">Simple User&nbsp;<span class="glyphicon glyphicon-user"></span></a>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <p class="lead">
                                <a href="{{action('Website_CompanyController@wordpress', 'our-services')}}" class="btn btn-lg btn-primary">Company User&nbsp;<span class="glyphicon glyphicon-briefcase"></span></a>
                            </p>
                        </div>
                        <div class="col-md-3">
                            
                        </div>
                    </div>
                    <div class="home_footer">
                        <div class="inner">
                          <p>© 2013 - Graphics, layouts and Contents are exclusive property of  <a href="#">jmtv</a> Pte. Ltd. - Co. Reg. n. 201314573M.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>