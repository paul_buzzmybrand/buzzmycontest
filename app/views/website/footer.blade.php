<div class="footer">
    <div class="inner">
      <p>&copy; 2014 - Graphics, layouts and Contents are exclusive property of <a href="http://www.buzzmybrand.it">BuzzMyBrand S.r.l.s.</a> - Via Togliatti n. 58, 70043 Monopoli (BA), P.I.07711470729</p>      <p>
          <a href="#">{{ trans('messages.app_store') }}&nbsp;</a>
          <a href="#">{{ trans('messages.google_play') }}&nbsp;</a>
          <a id="terms-of-service" class="footer-description">{{ trans('messages.terms_service') }}&nbsp;</a>
          <a id="privacy-police" class="footer-description">{{ trans('messages.privacy_police') }}&nbsp;</a>
          <a id="contact" class="footer-description">{{ trans('messages.contact') }}&nbsp;</a>
      </p>
    </div>
</div>
<script>
    $( "body" ).on('click', '.footer-description',function(event) {
        event.preventDefault();
        var input = $(this).attr('id');
        var url = "{{URL::to('website/footer')}}";
        var request = url+"/"+input;
        window.open(request, 'description', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no,fullscreen=no,channelmode=no,width=400,height=450,left=300,top=100');
    });
</script>
