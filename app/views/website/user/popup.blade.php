<div id="login" class="white-popup mfp-with-anim mfp-hide">
    <div class='popup-title'>{{ trans('messages.log_in') }}</div>
    <div class="popup-content">
        <div class="popup-button">
            <a href="{{action('Website_UserController@facebook')}}" class='btn btn-primary'>{{ trans('messages.log_in_with_facebook') }}</a></br>
        </div>
        <div class="popup-button">
            <a href="{{action('Website_UserController@twitter')}}" class='btn btn-primary'>{{ trans('messages.log_in_with_twitter') }}</a>
        </div>
    <h4>or</h4>
    <div style='color:#a94442;text-align:center;' class='error-login'></div>
    {{ Form::open(array('url' => 'website/user/my-contests', 'method' => 'post', 'id' => 'user-login')) }}
        <label for="username" class="error"></label>
        {{ Form::text('username',null, array('class' => 'form-control', 'placeholder' => trans('messages.form_username'), 'id' => 'username')) }}
        <label for="password" class="error"></label>
        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => trans('messages.form_password'), 'id' => 'password')) }}
        {{ Form::submit( trans('messages.log_in') , array('class' => 'btn btn-primary'))  }}
    {{ Form::close() }}   
    </div>
</div>



<div id="register" class="white-popup mfp-with-anim mfp-hide">
    <div class='popup-title'>{{ trans('messages.sign_up') }}</div>
    <div class="popup-content">
    {{ Form::open(array('url' => 'website/user/register', 'method' => 'post', 'id' => 'user-register', 'files' => true)) }}
        <label for="username" class="error"></label>
        {{ Form::text('username',null, array('class' => 'form-control', 'placeholder' => trans('messages.form_username'), 'id' => 'username')) }}
        <label for="email" class="error"></label>
        {{ Form::text('email',null, array('class' => 'form-control', 'placeholder' => trans('messages.form_email'), 'id' => 'email')) }}
        <label for="password" class="error"></label>
        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => trans('messages.form_password'), 'id' => 'password')) }}
        <label for="confirmPassword" class="error"></label>
        {{ Form::password('confirmPassword', array('class' => 'form-control', 'placeholder' => trans('messages.form_password_r'), 'id' => 'confirmPassword')) }}
        
        <div style="position:relative;margin-bottom: 12px;">
                <label for="images" class="error"></label>
                <a class='btn btn-primary' href='javascript:;'>
                        {{ trans('messages.choose_photo') }}
                        <input type="file" style='width:100%;height:100%;position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="images" size="40"  id='files-user'  onchange='$("#upload-file-info").html($(this).val());'>
                </a>
                &nbsp;
                <span class='label label-info' id="upload-file-info"></span><output id="list-user"></output>
        </div>
        {{ Form::submit(trans('messages.sign_up'), array('class' => 'btn btn-primary'))  }}
    {{ Form::close() }}   
    </div>
</div>
<div id="delete-account" class="white-popup mfp-with-anim mfp-hide">
   <div class='popup-title'>{{ trans('messages.delete_account_desc') }}</div>
   <div class="popup-content">
       <a href="{{action('Website_UserController@deleteAccount')}}" class='btn btn-primary'>{{ trans('messages.delete') }}</a>
   </div>
</div>    
<div id="video" style="width:900px;" class="video-white-popup mfp-with-anim mfp-hide">
   <div class='popup-title'><div class="video-title">{{ trans('messages.delete_account_desc') }}</div></div>
   <div class="popup-content">
       <video controls="" autoplay="" name="media"><source class="video-source" src="" type="video/mp4"></video>
   </div>
</div>
   
<div id="video-mini" style="width:500px;" class="video-white-fb-popup mfp-with-anim mfp-hide">
   <div class='popup-fb-title'><div class="video-title">{{ trans('messages.delete_account_desc') }}</div></div>
   <div class="popup-fb-content">
       <video width="320" height="240" controls><source class="video-source" type="video/mp4"></video>
   </div>
</div>

<div id="copy-link" class="white-popup mfp-with-anim mfp-hide">
   <div class='popup-title'>{{ trans('messages.copy_link') }}</div>
   <div class="popup-content">
       {{ Form::text('email',Request::url(), array('class' => 'form-control')) }}
   </div>
</div> 

<div id="change-picture" class="white-popup mfp-with-anim mfp-hide">
   <div class='popup-title'>{{ trans('messages.change_picture') }}</div>
   <div class="popup-content">
       <div class="row" style="margin-bottom:10px">
            <div class="col-md-6">
                 {{ Form::open(array('url' => 'website/user/change-photo', 'method' => 'post', 'id' => 'user-register', 'files' => true)) }}
                 <label for="images" class="error"></label>
                    <a class='btn btn-primary' href='javascript:;'>
                        {{ trans('messages.choose_new_photo') }}
                        <input type="file" style='width:100%;height:100%;position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="images" size="40" id='files-web'  onchange='$("#upload-file-info").html($(this).val());'>
                    </a>
            </div>
            <div class="col-md-6">
                <output  id="list-web"></output>
            </div>
           
       </div>
       {{ Form::submit(trans('messages.change_picture'), array('class' => 'btn btn-primary'))  }}
       {{Form::close()}}
   </div>
</div>

<script>
$(document).ready(function() {
   
    $("#user-register").validate({
        rules: {
                username: {
                        required: true,
                        minlength: 2,
                        maxlength: 50,
                        remote: {
                            url: "{{URL::to('website/user/check-username')}}",
                            type: "post",
                            data: {
                                username: function() {
                                    return $("#user-register #username").val();
                                }
                            }
                        }
                },
                email: {
                        required: true,
                        email: true,
                        maxlength: 60,
                        remote: {
                            url: "{{URL::to('website/user/check-email')}}",
                            type: "post",
                            data: {
                                email: function() {
                                    return $("#user-register #email").val();
                                }
                            }
                        }
                },
                password: {
                        required: true,
                        minlength: 4,
                        maxlength: 12,
                },
                confirmPassword: {
                        required: true,
                        equalTo: "#password"
                },
                images: {
                        required: true
                }
        },
        messages: {
                username: {
                        required: "{{ trans('messages.required') }}",
                        minlength: "{{ trans('messages.min_length', array('number' => '2')) }}",
                        maxlength: "{{ trans('messages.max_length', array('number' => '50')) }}",
                        remote: "{{ trans('messages.username_registered') }}"
                },
                email: {
                        required: "{{ trans('messages.required') }}",
                        maxlength: "{{ trans('messages.max_length', array('number' => '60')) }}",
                        remote: "{{ trans('messages.email_registered') }}"
                },
                password: {
                        required: "{{ trans('messages.required') }}",
                        minlength: "{{ trans('messages.min_length', array('number' => '4')) }}",
                        maxlength: "{{ trans('messages.max_length', array('number' => '12')) }}"
                    
                },
                confirmPassword: {
                        required: "{{ trans('messages.required') }}"
                },
                images: {
                        required: "{{ trans('messages.required') }}"
                }
        }
    });
     $("#user-login").validate({   
        submitHandler: function(form) {
           $('.error-login').text("");
           var password = $("#user-login #password").val();
           var username = $("#user-login #username").val();
           $.ajax({
               type     : "POST",
               url      : "{{URL::to('website/user/login')}}",
               data     : 
               {
                       username : username,
                       password : password
               },
               success : function(msg) {
                   if(msg == 'false')
                   {
                       $('.error-login').text("{{ trans('messages.wrong_data_login') }}");
                   }
                   else if(msg == 'true'){
                       form.submit();
                   }
               }
           });

        },
        rules: {
                username: {
                        required: true
                },
                password: {
                        required: true
                }

        },
        messages: {
                username: {
                        required: "{{ trans('messages.required') }}",
                        remote: "{{ trans('messages.wrong_data_login') }}"
                },
                password: {
                        required: "{{ trans('messages.required') }}"
                }
        },

    });
});
	
</script>
<script>
    document.getElementById('files-web').addEventListener('change', handleFileSelect, false);
    function handleFileSelect(evt) {
        var files = evt.target.files;
        var f = files[0];
        var reader = new FileReader();
          reader.onload = (function(theFile) {
                return function(e) {
                  document.getElementById('list-web').innerHTML = ['<img src="', e.target.result,'" title="', theFile.name, '" width="100" />'].join('');
                };
          })(f);
           
          reader.readAsDataURL(f);
}
</script>
<script>
    document.getElementById('files-user').addEventListener('change', handleFileSelect, false);
    function handleFileSelect(evt) {
        var files = evt.target.files;
        var f = files[0];
        var reader = new FileReader();
          reader.onload = (function(theFile) {
                return function(e) {
                  document.getElementById('list-user').innerHTML = ['<img src="', e.target.result,'" title="', theFile.name, '" width="100" />'].join('');
                };
          })(f);
           
          reader.readAsDataURL(f);
}
</script>