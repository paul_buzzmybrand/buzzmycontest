@extends('layouts.user')
@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<script>
	@if ($contest->userIsJoin)
        $(document).ready(function() {
            var text = '{{trans("messages.shoot")}}';
            $("#join-the-contest-logged").html('<span class="glyphicon glyphicon-camera"></span>&nbsp;'+text);
            var id = '{{$contest->id}}';
            var url = "{{URL::to('website/widget/login')}}";
            var request = url+"/"+id;
            $("#join-the-contest-logged").attr('onclick', "window.open('"+request+"', 'nazwa', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no,fullscreen=no,channelmode=no,width=300,height=350,left=300,top=100');");
		});
    @else
		$( "body" ).on( "click", "#join-the-contest-logged", function() {
	    	var url = "{{ URL::to('website/user/contest').'/'.$contest->id.'/join' }}";
	        window.location.href = url;
	        return;
	        
	       var text = '{{trans("messages.shoot")}}';
	       $(this).html('<span class="glyphicon glyphicon-camera"></span>&nbsp;'+text);
	       var id = '{{$contest->id}}';
	       var url = "{{URL::to('website/widget/login')}}";
	       var request = url+"/"+id;
	       $(this).attr('onclick', "window.open('"+request+"', 'nazwa', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no,fullscreen=no,channelmode=no,width=300,height=350,left=300,top=100');");
	    });
    @endif   
</script>
<div class="panel panel-default">
    <div class="panel-heading"><h3>{{$contest->name}}</h3></div>
    <div class="panel-body">
        <div class="row" style="margin-top:20px;">
            <div class="col-md-offset-1 col-md-3">
                <div class="jumbotron" style="padding:10px 5px;">
                    <div class="login-info">
                        @if (CommonHelper::loggedInAsUser())
                            {{trans('messages.logged')}} <span class="glyphicon glyphicon-ok"></span>
                        @else
                            {{trans('messages.not_logged')}} <span class="glyphicon glyphicon-remove"></span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-offset-1 col-md-7">
                @if (CommonHelper::loggedInAsUser())
                    <div class="col-md-offset-1 col-md-3" style="text-align:right;padding-right:0px;">
                       <a class='btn btn-primary btn-sm' id='join-the-contest-logged'  ><span class="glyphicon glyphicon-camera"></span>&nbsp;{{ trans('messages.Join_the_contest') }}</a>
                    </div>
                @else
                <div class="col-md-3" id="hide" style="text-align:right;padding-right:0px;display:none;">
                    <div class="shoot" ></div>
                </div>
                <div class="col-md-3" id="show" style="text-align:right;padding-right:0px;">
                    <div class="contest-popup" style="">
                        <a href="#contest-login" id='join-the-contest' data-effect="mfp-zoom-in" class='btn btn-primary login btn-sm'><span class="glyphicon glyphicon-camera"></span>&nbsp;{{ trans('messages.Join_the_contest') }}</a>
                    </div>
                </div>
                @endif
                <div class='popup col-md-2' style="text-align:left;padding-left:0px;margin-left:10px;padding-right:0px;">  
                    <a href="#copy-link" data-effect="mfp-zoom-in" class='btn btn-primary btn-sm'><span class="glyphicon glyphicon-share"></span>&nbsp;{{ trans('messages.copy_link') }}</a>
                </div>     
                <div class="col-md-4" style="padding-left:0px;">
                    <div class="fb-share-button"  data-href="{{URL::to('website/user/contest', $contest->id)}}" data-type="button"></div>
                    <a style="padding-top:15px;" href="https://twitter.com/share" class="twitter-share-button" data-url="{{URL::to('website/user/contest', $contest->id)}}" data-text="Check this out! From JoinMeThere" data-count="none">Tweet</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>    
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top:0px;">
          <div class='col-md-4'>
              <a href="{{action('Website_UserController@contest', $contest->id)}}" style='color:#555;'><img src="{{asset('/images/contest/'.$contest->image)}}"/></a>
          </div>
          <div class='col-md-8'>
              <div class='well' style='text-align:left'>
                  <b>{{ trans('messages.brief_rules') }}</b> {{$contest->rules}}</br>
                  <b>{{ trans('messages.rewards') }}</b><ol>
                  <?php if (!empty($contest->reward1_type)): ?>
                  <li>{{ $contest->reward1_type }}: {{ $contest->reward1_desc }}</li>
                  <?php  endif; ?>
                  <?php if (!empty($contest->reward2_type)): ?>
                  <li>{{ $contest->reward2_type }}: {{ $contest->reward2_desc }}</li>
                  <?php  endif; ?>
                  <?php if (!empty($contest->reward3_type)): ?>
                  <li>{{ $contest->reward3_type }}: {{ $contest->reward3_desc }}</li>
                  <?php  endif; ?>
                  </ol>
                  <b>{{ trans('messages.judges') }}</b> {{$contest->judges}}</br>
                  <b>{{ trans('messages.entries_user') }}</b> ???</br>
                  <b>{{ trans('messages.jmt_score') }}</b> {{$contest->contestJMTScore}}</br>
              </div>
          </div>    
          <div class="col-md-12">
              @if($countVideos > 0)
              <div id="myCarousel"  class="carousel slide" data-interval="3000" data-ride="carousel">
                  <!-- Carousel items -->
                  <div class="carousel-inner">
                      <?php $count = 0; ?>
                      <?php foreach($videos as $video): ?>
                          <?php if($count == 0): ?>
                              <div class="item active">
                          <?php endif; ?>

                          <?php if($count != 0): ?>     
                              <?php if($count % 4 === 0): ?>
                              </div>
                              <?php endif; ?>
                          <?php endif; ?>

                          <?php if($count != 0): ?>  
                              <?php if($count % 4 === 0): ?>
                                  <div class="item">
                              <?php endif; ?>
                          <?php endif; ?>

                          <div class="col-md-3">
                              <div class="well">
                                  <div class="popup"><a href="#video" class="video-view" video="{{$video->filename}}" title="{{$video->name}}" style='color:#555;'><img style="max-width: 200px; max-height: 200px" src="{{asset('/images/videos/'.$video->image)}}"/></a></div>
                              </div>
                          </div>

                          <?php if($countVideos -1  == $count): ?>
                              </div>
                          <?php endif; ?>
                          <?php $count ++; ?>
                      <?php endforeach; ?>
                  </div>
                  <!-- Carousel nav -->
                  <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left"></span>
                  </a>
                  <a class="carousel-control right" href="#myCarousel" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right"></span>
                  </a>
              </div>
              @else
                  <div class="alert alert-info" style='margin-top:40px;'>{{ trans('messages.no_results') }}</div>
              @endif
          </div>
      </div>
  </div>
</div>




  
<div id="contest-login" class="white-popup mfp-with-anim mfp-hide">
    <div class='popup-title'>{{ trans('messages.log_in') }}</div>
    <div class="popup-content">
        <div class="popup-button">
            <a href="{{action('Website_UserController@facebook')}}" class='btn btn-primary'>{{ trans('messages.log_in_with_facebook') }}</a></br>
        </div>
        <div class="popup-button">
            <a href="{{action('Website_UserController@twitter')}}" class='btn btn-primary'>{{ trans('messages.log_in_with_twitter') }}</a>
        </div>
    <h4>or</h4>
    <div style='color:#a94442;text-align:center;' class='error-login'></div>
    {{ Form::open(array('url' => URL::to('website/user/contest', $id), 'method' => 'post', 'id' => 'record-login')) }}
        <label for="username" class="error"></label>
        {{ Form::text('username',null, array('class' => 'form-control', 'placeholder' => trans('messages.form_username'), 'id' => 'username')) }}
        <label for="password" class="error"></label>
        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => trans('messages.form_password'), 'id' => 'password')) }}
        {{ Form::submit( trans('messages.log_in') , array('class' => 'btn btn-primary'))  }}
    {{ Form::close() }}   
    </div>
</div>

<!-- menu to logged user -->
<div class="menu-login" style="display:none;">
    <div class="btn-group">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            <span class="glyphicon glyphicon-user"></span>&nbsp;{{ trans('messages.your_account') }}&nbsp; <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
          <div class='popup'><li><a style='text-decoration:none;color:black;' href="#change-picture" data-effect="mfp-zoom-in">{{ trans('messages.change_picture') }}</a></li></div>
          <li><a href="#">{{ trans('messages.language') }}</a></li>
          <div class='popup'><li><a style='text-decoration:none;color:black;' href="#delete-account" data-effect="mfp-zoom-in">{{ trans('messages.delete_account') }}</a></li></div>    
          <li class="divider"></li>
          <li><a href="{{action('Website_CompanyController@logout')}}">{{ trans('messages.log_out') }}</a></li>
        </ul>
    </div>
</div>

<script>
$("#record-login").validate({   
        submitHandler: function(form) {
           $('.error-login').text("");
           var password = $("#record-login #password").val();
           var username = $("#record-login #username").val();
           console.log("Login username: " + username);
           $.ajax({
               type     : "POST",
               url      : "{{URL::to('website/user/login')}}",
               data     : 
               {
                       username : username,
                       password : password
               },
               success : function(msg) {
                   console.log("Response from login function: " + msg);
               	   if(msg == 'false')
                   {
                       $('.error-login').text("{{ trans('messages.wrong_data_login') }}");
                   }
                   else if(msg == 'true'){
                       var url = "{{ URL::to('website/user/contest').'/'.$contest->id.'/join' }}";
                       window.location.href = url;
                       return;
                       $('#join-the-contest').text('Shoot');
                       $.magnificPopup.close();
                       var id = '{{$contest->id}}';
                       var url = "{{URL::to('website/widget/login')}}";
                       var request = url+"/"+id;
                       $( ".contest-popup" ).remove();
                       $('#hide').css('display', 'block');
                       $('#show').css('display', 'none');
                       $(".shoot").html("<a class='btn btn-primary btn-sm' id='join-the-contest'  ><span class='glyphicon glyphicon-camera'></span>&nbsp;{{ trans('messages.shoot') }}</a>");
                       $('#join-the-contest').attr('onclick', "window.open('"+request+"', 'nazwa', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no,fullscreen=no,channelmode=no,width=300,height=350,left=300,top=100');");
                       $('#join-the-contest').attr('href','#');
                       var logged = '{{trans("messages.logged")}}';
                       $('.login-info').html(logged+'<span class="glyphicon glyphicon-remove"></span>');
                       $('.menu').html('');
                       menu = $('.menu-login');
                       $(menu).attr('style', '');
                       $('.menu').html(menu);
                   }
               }
           });

        },
        rules: {
                username: {
                        required: true
                },
                password: {
                        required: true
                }

        },
        messages: {
                username: {
                        required: "{{ trans('messages.required') }}",
                        remote: "{{ trans('messages.wrong_data_login') }}"
                },
                password: {
                        required: "{{ trans('messages.required') }}"
                }
        },

    });
</script>
<script>
    $( "body" ).on('click', '.video-view', function(event) {
        event.preventDefault();
        var video = $(this).attr('video');
        var title = $(this).attr('title');
        $( ".video-title" ).text(title);
        var url = '{{asset("videos")}}'
        var videoUrl = url+'/'+video;
        $('.video-source').attr( "src", videoUrl );      
    });
</script>

@stop