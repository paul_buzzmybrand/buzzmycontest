@extends('layouts.user')
@section('content')
@include('website.user.user-menu') 
<div class="panel panel-default">
    <div class="panel-heading"><h3>{{ $video->name }}</h3></div>
  <div class="panel-body">
      <div class="col-md-12">
        <video width="640" height="480" controls>
  <source src="{{asset('videos/'.$video->filename)}}" type="video/filename"/>
Your browser does not support the video tag.
</video>
    </div>
  </div>
</div>
@stop
