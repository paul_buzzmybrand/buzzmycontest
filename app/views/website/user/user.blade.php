@extends('layouts.user')
@section('content')
<div class="panel panel-default">
  <div class="panel-heading"><h3>{{ trans('messages.contest_list') }}</h3></div>
    <div class="panel-body">
        <div class="col-md-6">
        </div>
        <div class=" col-md-6">
        <div class="btn-group">
            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                <span class="glyphicon glyphicon-search"></span>&nbsp;{{ trans('messages.search') }}&nbsp; <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" style='width:250px;padding:10px 10px' role="menu">
                {{ Form::open(array('url' => 'website/user', 'method' => 'get')) }}
                {{ Form::text('keyword', null, array('class' => 'form-control', 'placeholder' => trans('messages.keyword'), 'id' => 'keyword')) }}
                <div class='row'>
                    <div class='col-md-5' style='margin-top:10px;'>
                        Reward:
                    </div>
                    <div class='col-md-7'>
                        {{ Form::select('reward', array('cash' => trans('messages.cash'), 'vouchers' => trans('messages.vouchers'), 'productsServices' => trans('messages.products_services'), 'others' => trans('messages.others')),null, array('class' => 'form-control', 'id' => 'select-reward')) }}
                    </div>
                </div>
                <div class='row' style='margin-bottom:10px;'>
                    <div class='col-md-5' style='margin-top:10px;'>
                        Location:
                    </div>
                    <div class='col-md-7'>
                        {{ Form::select('location', array('italy' => 'Italy', 'singapore' => 'Singapore'),null, array('class' => 'form-control')) }}
                    </div>
                </div>
                {{ Form::submit(trans('messages.search'), array('class' => 'btn btn-primary'))  }}
                {{ Form::close() }} 
            </ul>
        </div>
        </div>
        <div class="col-md-12" style="margin-top:20px;">
        @if($countContest > 0)
        <div id="myCarousel"  class="carousel slide" data-interval="3000" data-ride="carousel">
            <!-- Carousel items -->
            <div class="carousel-inner">
                <?php $count = 0; ?>
                <?php foreach($contests as $contest): ?>
                    <?php if($count == 0): ?>
                        <div class="item active">
                    <?php endif; ?>

                    <?php if($count != 0): ?>     
                        <?php if($count % 4 === 0): ?>
                        </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if($count != 0): ?>  
                        <?php if($count % 4 === 0): ?>
                            <div class="item">
                        <?php endif; ?>
                    <?php endif; ?>

                    <div class="col-md-3">
                        <div class="well" style="height:350px;">
                            <a href="{{action('Website_UserController@contest', $contest->id)}}" style='color:#555;'><img style='max-width:100%; max-height:100%;' src="{{asset('/images/contest/'.$contest->image)}}"/></a>
                            <div class="box-data">
                                <b>{{ trans('messages.start') }}</b> {{ date('d M Y', strtotime($contest->start_time)) }}</br>
                                <b>{{ trans('messages.end') }} </b> {{ date('d M Y', strtotime($contest->end_time)) }}</br>
                            </div>
                            <div class="box-title">{{$contest->name}}</div>
                        </div>
                    </div>

                    <?php if($countContest -1  == $count): ?>
                        </div>
                    <?php endif; ?>
                    <?php $count ++; ?>
                <?php endforeach; ?>
            </div>
            <!-- Carousel nav -->
            <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
        @else
            <div class="alert alert-info" style='margin-top:40px;'>{{ trans('messages.no_results') }}&nbsp;<a href='{{URL::to("website/user")}}' class='btn btn-info btn-xs' >{{ trans('messages.back') }}</a></div>
        @endif
    </div>
  </div>
</div>

        


      
@stop
