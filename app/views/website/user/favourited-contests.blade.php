@extends('layouts.user')
@section('content')
@include('website.user.user-menu')
<div class="panel panel-default">
    <div class="panel-heading"><h3>{{ trans('messages.favourited_contests') }}</h3></div>
    <div class="panel-body">
        <div class="col-md-12">
            @if($countContest > 0)
            <div id="myCarousel"  class="carousel slide" data-interval="3000" data-ride="carousel">
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <?php $count = 0; ?>
                    <?php foreach($contests as $contest): ?>
                        <?php if($count == 0): ?>
                            <div class="item active">
                        <?php endif; ?>
                                
                        <?php if($count != 0): ?>     
                            <?php if($count % 4 === 0): ?>
                            </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    
                        <?php if($count != 0): ?>  
                            <?php if($count % 4 === 0): ?>
                                <div class="item">
                            <?php endif; ?>
                        <?php endif; ?>

                        <div class="col-md-3">
                            <div class="well" style="height:350px;">
                                <a href="{{action('Website_UserController@contest', $contest->id)}}" style='color:#555;'><img style='max-width:100%; max-height:100%;' src="{{asset('/images/contest/'.$contest->image)}}"/></a>
                                <div class="box-data"> 
                                    <b>{{ trans('messages.start') }}</b> {{ date('d M Y', strtotime($contest->start_time)) }}</br>
                                    <b>{{ trans('messages.end') }} </b> {{ date('d M Y', strtotime($contest->end_time)) }}</br>
                                </div>
                                <div class="box-title">{{$contest->name}}</div>
                            </div>
                        </div>
                                    
                        <?php if($countContest -1  == $count): ?>
                            </div>
                        <?php endif; ?>
                        <?php $count ++; ?>
                    <?php endforeach; ?>
                </div>
                <!-- Carousel nav -->
                <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
            @else
                <div class="alert alert-info" style='margin-top:40px;'>{{ trans('messages.no_results') }}</div>
            @endif
        </div>
    </div>
</div>
   
    
        

      
@stop
