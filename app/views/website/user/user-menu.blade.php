<div class='row'>
    <div class='col-md-12'>
        <nav class="navbar navbar-default" role="navigation">
            <ul class="nav navbar-nav">
              <li><a href="{{action('Website_UserController@favouritedContests')}}">{{ trans('messages.favourited_contests') }}</a></li>
              <li><a href="{{action('Website_UserController@userContests')}}">{{ trans('messages.my_contests') }}</a></li>
              <ul class="nav navbar-nav">
                <li class="dropdown">
                  <a href="{{action('Website_UserController@searchContests')}}" class="dropdown-toggle" data-toggle="dropdown">{{ trans('messages.search_contests') }} <b class="caret"></b></a>
                  <ul class="dropdown-menu" style='width:250px;padding:10px 10px'>
                    {{ Form::open(array('url' => 'website/user/search-contests', 'method' => 'get')) }}
                    {{ Form::text('keyword', null, array('class' => 'form-control', 'placeholder' => trans('messages.keyword'), 'id' => 'keyword')) }}
                    <div class='row'>
                        <div class='col-md-5' style='margin-top:10px;'>
                            Reward:
                        </div>
                        <div class='col-md-7'>
                            {{ Form::select('reward', array('cash' => trans('messages.cash'), 'vouchers' => trans('messages.vouchers'), 'productsServices' => trans('messages.products_services'), 'others' => trans('messages.others')),null, array('class' => 'form-control', 'id' => 'select-reward')) }}
                        </div>
                    </div>
                    <div class='row' style='margin-bottom:10px;'>
                        <div class='col-md-5' style='margin-top:10px;'>
                            Location:
                        </div>
                        <div class='col-md-7'>
                            {{ Form::select('location', array('italy' => 'Italy', 'singapore' => 'Singapore'),null, array('class' => 'form-control')) }}
                        </div>
                    </div>
                    {{ Form::submit(trans('messages.search'), array('class' => 'btn btn-primary'))  }}
                    {{ Form::close() }}
                  </ul>
                </li>
              </ul>
           </ul>
        </nav>
    </div>
</div>