<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sign Up</title>
        
	{{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
	{{ HTML::style('packages/bootstrap/css/bootstrap-responsive.min.css') }}
		
	<style>
	
	
		.alert { text-align: center; list-style-position:inside;}

	
	</style>
    	
</head>
<body>
    
	<div class="container" >
	
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3"	>
				<div style="text-align: center;">BUZZ MY BRAND LOGIN</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3"	>
				<div style="text-align: center;"><br></div>
			</div>
		</div>

		<div class="row">
            
			<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3"	>
                
	                   
				{{ Form::open(array('action' => array('Website_WidgetController@buzzMyBrandPerformLogin', $contest_id), 'method' => 'post')) }}
					
				@if(Session::has('message'))
				<ol class="alert">
					<b> {{ Session::get('message') }}</b>
					@foreach($errors->all() as $error)
					<li style="margin-left: 20px;">{{ $error }}</li>
					@endforeach    
				</ol>
				@endif
			  

				<div style="text-align: center;"><label for="email" style="width: 180px; text-align:left;">Email</label><input type="email" name="email" style="width: 250px;"/></div>
				<div style="text-align: center;"><label for="password" style="width: 180px; text-align:left;">Password</label><input type="password" name="password" style="width: 250px;"/></div>
				<div style="text-align: center;"><input type="submit" name="signup" value="Login"/></div>
					
				{{ Form::close() }}
                        
                    				
 
            </div>
			
        </div>
		
    </div>
	
</body>
</html>
