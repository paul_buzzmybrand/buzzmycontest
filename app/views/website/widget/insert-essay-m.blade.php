@extends('layouts.contest_popup-m')

@section('step') {{$step}} @stop

@section('box')

	<div style="width: 250px; height: 250px; margin: 50px auto 0px auto; background-color: #e9b4b4; padding: 5px;">
		<div style="width: 240px; height: 27px; text-align: left;">
			<img src="{{asset('website_public/contest_popup/virgoletta-top.png')}}"></img>
		</div>
		<div style="width: 240px; height: 146px; margin-top: 20px; margin-bottom: 20px; "> 
			<form id="insertEssayForm" action="{{ action('tab_HomeController@saveEssay') }}" method="POST">
				<textarea id="inputTitleInsertTitle" name="sentence" style="width: 240px; height: 146px; border: none; font-size: 20px; font-family: Abel" placeholder="{{Lang::get('widget.creation_step.placeholder')}} {{$contest->hashtag}}" maxlength="100"></textarea>
				<input type="hidden" name="id_contest" value="{{$contest->id}}">
			</form>
		</div>
		<div style="width: 240px; height: 27px; text-align: right;">
			<img src="{{asset('website_public/contest_popup/virgoletta-bottom.png')}}"></img>
		</div>		
	</div>
	<div style="width: 240px; height: 45px; margin: 5px auto; font-size: 10px;">
		{{Lang::get('widget.creation_step.policy')}}
	</div>
	<div class="submit_insert_essay" style="width: 247px; height: 41px; margin: 0px auto 20px auto;"></div>
	
<script>
	$(document).ready(function(){
		$('#inputTitleInsertTitle').keyup(function(){
			
			var title = $('#inputTitleInsertTitle').val();
	
    		if( title != '' ){
    			$('.submit_insert_essay').addClass('submit_insert_essay_active');
    			change_onclick('enable');
    		}
    		else{
    			change_onclick('disable');
    			$('.submit_insert_essay').removeClass('submit_insert_essay_active');
    		}
    	});
	});
	function change_onclick( action )
    { 
        if( action == 'disable')
        {
            $('.submit_insert_essay').attr("onclick","");
            
        }
        else if( action == 'enable')
        {
            $('.submit_insert_essay').attr("onclick","document.getElementById('insertEssayForm').submit()");
        }
    }
</script>
	
@stop