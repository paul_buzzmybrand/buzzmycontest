@extends('layouts.contest_popup-m')
@section('step') {{ $step }}@stop
@section('box')
<div class="widget">
    <div>
   
     <img src="{{asset('images/companys/' . $contest->company->image_login) }}" class="popup-content" style="margin-top:22px; margin-bottom: 25px;"> 


    
    <div class="form center-block">
    
        <div style="height: 28px; margin-bottom: 20px; font-size: 20px; color: #8d8d8d">
            <img src="{{asset('website_public/contest_popup-m/2-register-icon.png') }}">
            
        {{Lang::get("widget.signup_step.register_now")}}
        </div>
        <div style="clear:left;"></div>
                
        @if(Session::has('message'))
        <ol class="alert" style="text-align: left; margin-left: 40px;">
            <b> {{ Session::get('message') }}</b>
            @foreach($errors->all() as $error)
            <li style="margin-left: 20px;">{{ $error }}</li>
            @endforeach  
            
        </ol>
        @endif
    
    {{ Form::open(array('action' => array('Website_WidgetController@registerUser', $contest->id, 'is_mobile'), 'method' => 'post', 'id' => 'registrationForm')) }}
		<?php if ( $contest->objectives()->first()->pivot->name ) { ?>
			<div class="form-group">
				<div class="input-group">
					<input class="form-control" type="text" id="name" name="name" maxlength="25" placeholder="{{Lang::get("widget.signup_step.name")}}">
					<div id="name_flag" class="input-group-addon"><img  src="{{asset('website_public/contest_popup-m/2-validate.png') }}"  /></div>
				</div>
			</div>
		<?php } ?>
		<?php if ( $contest->objectives()->first()->pivot->surname ) { ?>
			<div class="form-group">
				<div class="input-group">
					<input class="form-control" type="text" id="surname" name="surname" maxlength="25" placeholder="{{Lang::get("widget.signup_step.surname")}}">
					<div id="surname_flag" class="input-group-addon" ><img  src="{{asset('website_public/contest_popup-m/2-validate.png') }}"/></div>
				</div>
			</div>
		<?php } ?>
		<?php if ( $contest->objectives()->first()->pivot->birthdate ) { ?>
			<div class="form-group">
				<div class="input-group">
					<input class="form-control" type="text" id="birthdate" name="birthdate" placeholder="{{Lang::get("widget.signup_step.birthdate")}}">
					<div id="birthdate_flag" class="input-group-addon" ><img  src="{{asset('website_public/contest_popup-m/2-validate.png') }}"/></div>
				</div>
			</div>
		<?php } ?>
		<?php if ( $contest->objectives()->first()->pivot->email ) { ?>
			<div class="form-group">
				<div class="input-group">
					<input class="form-control" type="email" id="email" name="email" placeholder="{{Lang::get("widget.signup_step.email_address")}}">
					<div id="email_flag" class="input-group-addon" ><img  src="{{asset('website_public/contest_popup-m/2-validate.png') }}"/></div>
				</div>
			</div>
		<?php } ?>	
		<?php if ( $contest->objectives()->first()->pivot->phone ) { ?>
			<div class="form-group">
				<div class="input-group">
					<input class="form-control" type="text" id="phone" name="phone" placeholder="{{Lang::get("widget.signup_step.phone")}}">
					<div id="phone_flag" class="input-group-addon" ><img  src="{{asset('website_public/contest_popup-m/2-validate.png') }}"/></div>
				</div>
			</div>
		<?php } ?>
		<?php if ( $contest->objectives()->first()->pivot->city ) { ?>
		<div class="form-group">
			<div class="input-group">
				<input class="form-control" type="text" id="city" name="city" placeholder="{{Lang::get("widget.signup_step.city")}}">
				<div id="city_flag" class="input-group-addon" ><img  src="{{asset('website_public/contest_popup-m/2-validate.png') }}"/></div>
			</div>
		</div>
		<?php } ?>
		<?php if ( $contest->objectives()->first()->pivot->cap ) { ?>
		<div class="form-group">
			<div class="input-group">
				<input class="form-control" type="text" id="cap" name="cap" placeholder="{{Lang::get("widget.signup_step.cap")}}">
				<div id="cap_flag" class="input-group-addon" ><img  src="{{asset('website_public/contest_popup-m/2-validate.png') }}"/></div>
			</div>
		</div>
		<?php } ?>
		
		
		<div class="form-group">
		<div class="checkbox">
			<label>
			  <input type="checkbox" id="confirmPrivacy" name="confirmPrivacy">Autorizzo il trattamento dei dati e delle immagini personali ai sensi del D.lgs. 196/2003 per le finalita' del concorso. I dati forniti non saranno ceduti a terzi.
			</label>
		</div>
		</div>
			
		<!--
		<div class="form-group">
			<div class="input-group">
				<select class="form-control" id="sex" name="sex" title="{{Lang::get("widget.signup_step.sex")}}">
					<option value="">{{Lang::get("widget.signup_step.selectsex")}}</option>
					<option value="male">{{Lang::get("widget.signup_step.male")}}</option>
					<option value="female">{{Lang::get("widget.signup_step.female")}}</option>
				</select>
				<div id="sex_flag" class="input-group-addon" ><img  src="{{asset('website_public/contest_popup-m/2-validate.png') }}"/></div>
			</div>
		</div>
		-->
				
				
		
		
		
		
		<div class="form-group">
	
			<!--
	
			<div class="col-xs-1 col-sd-1" style="padding: 5px 0"><div  id="tEc_flag" style="border: 1px solid #ccc; width: 25px; height: 25px;"><img id="tecFlagImg" style="display:none;" class="img-responsive" src="{{asset('website_public/contest_popup-m/2-validate.png') }}"/></div>
			</div>
			<div class="col-xs-5 col-sd-5" style="padding: 5px; height: 25px; line-height: 25px; text-align: left; color: #8d8d8d; letter-spacing: -1px;padding-left: 10px;">
			<a 
			<?php if ( $contest->terms_cond ) { ?>
			href="{{asset('/t&c/' . $contest->id)}}" 
			<?php } else { ?>
			href="{{asset('/admin/get-terms-' . $contest->location->language . '/' . $contest->term_cond_company_sponsoring)}}"	
			<?php } ?>
			
			target="_blank"
			>{{Lang::get("widget.signup_step.t_e_c")}}</a></div>
			<div style="clear: both;"></div>
			<div class="col-xs-1 col-sd-1" style="padding: 5px 0"><div  id="pri_flag" style="border: 1px solid #ccc; width: 25px; height: 25px;"><img id="pri_flagImg" style="display:none;" class="img-responsive" src="{{asset('website_public/contest_popup-m/2-validate.png') }}"/></div>
			</div>
			<div class="col-xs-5 col-sd-5" style="padding: 5px; height: 25px; line-height: 25px; text-align: left; color: #8d8d8d; letter-spacing: -1px;padding-left: 10px;">
			<a href="{{asset('/get-privacy-minisite')}}">Privacy</a></div>
		
			-->
		
			<div class="col-xs-6 col-sd-6" style="padding: 2px; align:center;">
				<input  type="submit" class="sigUpButton" value=""  />
			</div>
	
		</div>
	{{ Form::close() }}
    </div>
        

    
    </div>    
    
</div>

<script>

        
window.fbAsyncInit = function() {
    FB.init({
        appId      : '<?php echo $contest->company->fbApp_clientID ?>', // App ID
        status     : true, // check login status
        cookie     : true, // enable cookies to allow the server to access the session
        xfbml      : true  // parse XFBML
    });

// Additional initialization code here
};

// Load the SDK Asynchronously
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));
    

$( "#disconnectLink" ).click(function() {

    FB.logout();
});



     
        var nameIsValid = false;
        var surnameIsValid = false;
        var birthdateIsValid = false;
        var emailIsValid = false;
        var phoneIsValid = false;
        var cityIsValid = false;
        var capIsValid = false;
        //var sexIsValid = false;
        //var tEcApplyed = false;
        //var pri_flagApplyed = false;
        
        function validateEmail(email) {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return re.test(email);
        }
        
        function validateDate(date) {
            var re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
            if(regs = date.match(re)) {
                if(regs[1] < 1 || regs[1] > 31) {
                  return false;
                }
                if(regs[2] < 1 || regs[2] > 12) {
                  return false;
                }
                var currentYear = (new Date()).getFullYear();
                if(regs[3] < currentYear - 80 || regs[3] > currentYear) {
                  return false;
                }
                return true;
            } else {
                return false;
            }
            
        }
        
        function validatePhone(phone) {
            var re = /^\d+$/;
            return re.test(phone);
        }
        
            function validateCap(cap) {
            var re = /^\d{2}[01589]\d{2}$/;
            return re.test(cap);
        }
        
        $( document ).ready(function() {
            $('.input-group-addon img').hide();
            
            
            if ( $('#name').length ) {
            
            $( "#name" ).blur(function() {
                
                if ( $( "#name" ).val() != '') {
                    $( "#name_flag" ).show();
                    nameIsValid = true;
                } else {
                    $( "#name_flag" ).hide();
                    nameIsValid = false;
                }
                
            });
            
        };
        
        if ( $('#surname').length ) {
            
            $( "#surname" ).blur(function() {
                
                if ( $( "#surname" ).val() != '') {
                    $( "#surname_flag" ).show();
                    surnameIsValid = true;
                } else {
                    $( "#surname_flag" ).hide();
                    surnameIsValid = false;
                }
                
            });
            
        };
        
        if ( $('#birthdate').length ) {
            
            $( "#birthdate" ).blur(function() {
            
                if ( validateDate($( "#birthdate" ).val()) ) {
                    $( "#birthdate_flag" ).show();
                    birthdateIsValid = true;
                } else {
                    $( "#birthdate_flag" ).hide();
                    birthdateIsValid = false;
                }
                
            });
            
        };
        
        if ( $('#city').length ) {
            
            $( "#city" ).blur(function() {
            
            
            
                if ( $( "#city" ).val() != '' ) {
                    $( "#city_flag" ).show();
                    cityIsValid = true;
                } else {
                    $( "#city_flag" ).hide();
                    cityIsValid = false;
                }
                
            });
            
        };
        
        if ( $('#email').length ) {
            
            $( "#email" ).blur(function() {
            
            
            
                if ( validateEmail($( "#email" ).val()) ) {
                    $( "#email_flag" ).show();
                    emailIsValid = true;
                } else {
                    $( "#email_flag" ).hide();
                    emailIsValid = false;
                }
                
            });
            
        };
        
        if ( $('#phone').length ) {
            
            $( "#phone" ).blur(function() {
            
                if ( validatePhone($( "#phone" ).val()) ) {
                    $( "#phone_flag" ).show();
                    phoneIsValid = true;
                } else {
                    $( "#phone_flag" ).hide();
                    phoneIsValid = false;
                }
                
            });
            
        };
        
        if ( $('#cap').length ) {
            
            $( "#cap" ).blur(function() {
            
            
            
                if ( validateCap($( "#cap" ).val()) ) {
                    $( "#cap_flag" ).show();
                    capIsValid = true;
                } else {
                    $( "#cap_flag" ).hide();
                    capIsValid = false;
                }
                
            });
            
        };
            
            
            /*
            $( "#name" ).blur(function() {
                
                if ( $( "#name" ).val() != '') {
                    $( "#name_flag img" ).show();
                    nameIsValid = true;
                } else {
                    $( "#name_flag img" ).hide();
                    nameIsValid = false;
                }
                
            });
            $( "#surname" ).blur(function() {
                
                if ( $( "#surname" ).val() != '') {
                    $( "#surname_flag img" ).show();
                    surnameIsValid = true;
                } else {
                    $( "#surname_flag img" ).hide();
                    surnameIsValid = false;
                }
                
            });
            
            $( "#birthdate" ).blur(function() {
                
                if ( validateDate($( "#birthdate" ).val()) ) {
                    $( "#birthdate_flag img" ).show();
                    birthdateIsValid = true;
                } else {
                    $( "#birthdate_flag img" ).hide();
                    birthdateIsValid = false;
                }
                
            });
            $( "#birthdate" ).change(function() {
                
                if ( validateDate($( "#birthdate" ).val()) ) {
                    $( "#birthdate_flag img" ).show();
                    birthdateIsValid = true;
                } else {
                    $( "#birthdate_flag img" ).hide();
                    birthdateIsValid = false;
                }
                
            });
            
                        
            $( "#city" ).blur(function() {
                
                
                
                if ( $( "#city" ).val() != '' ) {
                    $( "#city_flag img" ).show();
                    cityIsValid = true;
                } else {
                    $( "#city_flag img" ).hide();
                    cityIsValid = false;
                }
                
            });
            
            $( "#sex" ).blur(function() {
                
                
                
                if ( $( "#sex" ).val() != '' ) {
                    $( "#sex_flag" ).show();
                    sexIsValid = true;
                } else {
                    $( "#sex_flag" ).hide();
                    sexIsValid = false;
                }
                
            });     
            
            $( "#email" ).blur(function() {
                
                
                
                if ( validateEmail($( "#email" ).val()) ) {
                    $( "#email_flag img" ).show();
                    emailIsValid = true;
                } else {
                    $( "#email_flag img" ).hide();
                    emailIsValid = false;
                }
                
            });
            
            $( "#phone" ).blur(function() {
                
                
                
                if ( validatePhone($( "#phone" ).val()) ) {
                    $( "#phone_flag img" ).show();
                    phoneIsValid = true;
                } else {
                    $( "#phone_flag img" ).hide();
                    phoneIsValid = false;
                }
                
            });
            
            
            
            $( "#cap" ).blur(function() {
                
                
                
                if ( validateCap($( "#cap" ).val()) ) {
                    $( "#cap_flag" ).show();
                    capIsValid = true;
                } else {
                    $( "#cap_flag" ).hide();
                    capIsValid = false;
                }
                
            });
            */
                
            
            /*
            $( "#tEc_flag" ).click(function() {
                
                if ( $( "#tecFlagImg" ).css("display") == 'none') {
                    $( "#tecFlagImg" ).show();
                    tEcApplyed = true;
                } else {
                    $( "#tecFlagImg" ).hide();
                    tEcApplyed = false;
                }
                
            });
            
            $( "#pri_flag" ).click(function() {
                
                if ( $( "#pri_flagImg" ).css("display") == 'none') {
                    $( "#pri_flagImg" ).show();
                    pri_flagApplyed = true;
                } else {
                    $( "#pri_flagImg" ).hide();
                    pri_flagApplyed = false;
                }
                
            });
            */
            $("#registrationForm").submit(function(e){
                
                var errorMessage = '';
                

                
                
                if ( !nameIsValid && ($('#name').length) ) {
                    errorMessage += "{{Lang::get('widget.signup_step.name_required')}}\n";
                }
                
                if ( !surnameIsValid && ($('#surname').length)  ) {
                    errorMessage += "{{Lang::get('widget.signup_step.surname_required')}}\n";
                }
                
                if ( !birthdateIsValid && ($('#birthdate').length)  ) {
                    errorMessage += "{{Lang::get('widget.signup_step.birthdate_required')}}\n";
                }
                            
                if ( !cityIsValid && ($('#city').length)  ) {
                    errorMessage += "{{Lang::get('widget.signup_step.city_required')}}\n";
                }
                
                if ( !emailIsValid && ($('#email').length)  ) {
                    errorMessage += "{{Lang::get('widget.signup_step.email_required')}}\n";
                }
                
                if ( !phoneIsValid && ($('#phone').length)  ) {
                    errorMessage += "{{Lang::get('widget.signup_step.phone_required')}}\n";
                }
                
                if ( !capIsValid && ($('#cap').length)  ) {
                    errorMessage += "{{Lang::get('widget.signup_step.cap_required')}}\n";
                }
                
                var validCheck = $("#confirmPrivacy").is(':checked');
                
                if (!validCheck) {
                    errorMessage += "{{Lang::get('widget.signup_step.privacycheck_required')}}\n";
                    alert('Prego cliccare sulla casella di controllo');
                }
                
                if (errorMessage.length)
                {
                    return false;
                }
                else
                {
                    return true;
                }
                
                
            });
            
        });
        


    
</script>
@stop