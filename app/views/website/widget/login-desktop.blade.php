@extends('layouts.contest_popup')
@section('step') step1 @stop
@section('box')
<div class="widget">
    <div>
	 <img src="{{asset('images/companys/' . $contest->company->image_login) }}" class="popup-content"> 


	
	<div style="width: 400px; margin: 0 auto;">
	
		<div style="float:left; width: 40px; height: 28px; margin-bottom: 20px; margin-left: 40px;">
			<img src="{{asset('website_public/contest_popup/2-register-icon.png') }}">
			
		</div>
		<div style="float:left; width: 200px; height: 28px; margin-bottom: 20px;line-height: 28px; text-align: left;font-size: 20px; color: #8d8d8d">
			{{Lang::get("widget.signup_step.register_now")}}
		</div>
		<div style="clear:left;"></div>
		
				
		@if(Session::has('message'))
		<ol class="alert" style="text-align: left; margin-left: 40px;">
			<b> {{ Session::get('message') }}</b>
			@foreach($errors->all() as $error)
			<li style="margin-left: 20px;">{{ $error }}</li>
			@endforeach  
			
		</ol>
		@endif
	
	{{ Form::open(array('action' => array('Website_WidgetController@registerUser', $contest->id, 'is_desktop'), 'method' => 'post', 'id' => 'registrationForm')) }}


	
		<?php if ( $contest->objectives()->first()->pivot->name ) { ?>
		
			<input style="float: left; width: 310px; height: 30px; margin-bottom: 7px; padding-left:10px; margin-left: 40px;" maxlength="25" type="text" id="name" name="name" placeholder="{{Lang::get("widget.signup_step.name")}}">
			<div id="name_flag" style="float: left;width: 40px; height: 30px; line-height: 30px; display: none;"><img  src="{{asset('website_public/contest_popup/2-validate.png') }}"/></div>
			<div style="clear:left;"></div>
		
		<?php } ?>
	
	
		<?php if ( $contest->objectives()->first()->pivot->surname ) { ?>
			<input style="float: left; width: 310px; height: 30px; margin-bottom: 7px; padding-left:10px; margin-left: 40px;" maxlength="25" type="text" id="surname" name="surname" placeholder="{{Lang::get("widget.signup_step.surname")}}">
			<div id="name_flag" style="float: left;width: 40px; height: 30px; line-height: 30px; display: none;"><img  src="{{asset('website_public/contest_popup/2-validate.png') }}"/></div>
			<div style="clear:left;"></div>
		<?php } ?>		
		
		<?php if ( $contest->objectives()->first()->pivot->birthdate ) { ?>
			<input style="float: left; width: 310px; height: 30px; margin-bottom: 7px; padding-left:10px; margin-left: 40px;" type="text" id="birthdate" name="birthdate" placeholder="{{Lang::get("widget.signup_step.birthdate")}}">
			<div id="birthdate_flag" style="float: left;width: 40px; height: 30px; line-height: 30px; display: none;"><img  src="{{asset('website_public/contest_popup/2-validate.png') }}"/></div>
			<div style="clear:left;"></div>
		<?php } ?>		
		
		<?php if ( $contest->objectives()->first()->pivot->email ) { ?>
			<input style="float: left; width: 310px; height: 30px; margin-bottom: 7px; padding-left:10px; margin-left: 40px;" type="email" id="email" name="email" placeholder="{{Lang::get("widget.signup_step.email_address")}}">
			<div id="email_flag" style="float: left;width: 40px; height: 30px; line-height: 30px; display: none;"><img  src="{{asset('website_public/contest_popup/2-validate.png') }}"/></div>
			<div style="clear:left;"></div>
		<?php } ?>	
		
		<?php if ( $contest->objectives()->first()->pivot->phone ) { ?>
			<input style="float: left; width: 310px; height: 30px; margin-bottom: 7px; padding-left:10px; margin-left: 40px;" type="text" id="phone" name="phone" placeholder="{{Lang::get("widget.signup_step.phone")}}">
			<div id="phone_flag" style="float: left;width: 40px; height: 30px; line-height: 30px; display: none;"><img  src="{{asset('website_public/contest_popup/2-validate.png') }}"/></div>
			<div style="clear:left;"></div>
		<?php } ?>	
		
		<?php if ( $contest->objectives()->first()->pivot->city ) { ?>
			<input style="float: left; width: 310px; height: 30px; margin-bottom: 7px; padding-left:10px; margin-left: 40px;" type="text" id="city" name="city" placeholder="{{Lang::get("widget.signup_step.city")}}">
			<div id="city_flag" style="float: left;width: 40px; height: 30px; line-height: 30px; display: none;"><img  src="{{asset('website_public/contest_popup/2-validate.png') }}"/></div>
			<div style="clear:left;"></div>
		<?php } ?>	
		
		<?php if ( $contest->objectives()->first()->pivot->cap ) { ?>
			<input style="float: left; width: 310px; height: 30px; margin-bottom: 7px; padding-left:10px; margin-left: 40px;" type="text" id="cap" name="cap" placeholder="{{Lang::get("widget.signup_step.cap")}}">
			<div id="cap_flag" style="float: left;width: 40px; height: 30px; line-height: 30px; display: none;"><img  src="{{asset('website_public/contest_popup/2-validate.png') }}"/></div>
			<div style="clear:left;"></div>
		<?php } ?>	
	
		<div class="checkbox">
			<label style="padding: 5px; text-align: left; color: #8d8d8d; ">
			  <input type="checkbox" id="confirmPrivacy" name="confirmPrivacy">{{Lang::get("widget.signup_step.checkbox_info")}}
			</label>
		</div>
		
		<!--
		<select style="float: left; width: 310px; height: 30px; margin-bottom: 7px; padding-left:10px; margin-left: 40px;" id="sex" name="sex" title="{{Lang::get("widget.signup_step.sex")}}">
			<option value="">{{Lang::get("widget.signup_step.selectsex")}}</option>
			<option value="male">{{Lang::get("widget.signup_step.male")}}</option>
			<option value="female">{{Lang::get("widget.signup_step.female")}}</option>
		</select>
		<div id="sex_flag" style="float: left;width: 40px; height: 30px; line-height: 30px; display: none;"><img  src="{{asset('website_public/contest_popup/2-validate.png') }}"/></div>
		-->
		<div style="clear:left;"></div>
		<!--
		<div style="float: left;width: 160px; height: 38px; margin-left: 40px;">
			<div id="tEc_flag" style="float:left; width: 25px; height: 25px; line-height: 25px; border: 1px solid grey; margin: 6px 0px 6px 0px"><img id="tecFlagImg" style="display:none;" src="{{asset('website_public/contest_popup/2-validate.png') }}"/></div>
			<div style="float:left; width: 100px; height: 25px; line-height: 25px; margin: 6px 0px 6px 0px; color: #8d8d8d">
			<a 
			<?php if ( $contest->terms_cond ) { ?>
			
			href="{{asset('/t&c/' . $contest->id)}}"
			<?php } else { ?>
			href="{{asset('/admin/get-terms-' . $contest->location->language . '/' . $contest->term_cond_company_sponsoring)}}"	
			<?php } ?>
			
			>{{Lang::get("widget.signup_step.t_e_c")}}</a></div>
		</div>
		<div style="clear: both;"></div>
		<div style="float: left;width: 160px; height: 38px; margin-left: 40px;">
			<div id="pri_flag" style="float:left; width: 25px; height: 25px; line-height: 25px; border: 1px solid grey; margin: 6px 0px 6px 0px"><img id="pri_flagImg" style="display:none;" src="{{asset('website_public/contest_popup/2-validate.png') }}"/></div>
			<div style="float:left; width: 100px; height: 25px; line-height: 25px; margin: 6px 0px 6px 0px; color: #8d8d8d">
			<a href="{{asset('/get-privacy-minisite')}}">Privacy</a></div>
		</div>
		-->
		<div style="text-align: center;">
			<input  type="submit" class="sigUpButton" value=""></input>
		</div>
	
	{{ Form::close() }}

	</div>
		

	
    </div>    
    
</div>

  <script>
  $(function() {
	<?php if ($contest->location->language == "it") { ?>
	$.datepicker.setDefaults($.datepicker.regional['it']);
    $( "#birthdate" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear: true, yearRange: "-80:+0",});
	<?php } else { ?>
	$.datepicker.setDefaults($.datepicker.regional['en']);
    $( "#birthdate" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear: true, yearRange: "-80:+0",});
	<?php } ?>
  });

  
		
	var nameIsValid = false;
	var surnameIsValid = false;
	var birthdateIsValid = false;
	var emailIsValid = false;
	var phoneIsValid = false;
	var cityIsValid = false;
	var capIsValid = false;
	//var sexIsValid = false;
	//var tEcApplyed = false;
	//var pri_flagApplyed = false;

	function validateEmail(email) {
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		return re.test(email);
	}
	
	function validateDate(date) {
		var re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
		if(regs = date.match(re)) {
			if(regs[1] < 1 || regs[1] > 31) {
			  return false;
			}
			if(regs[2] < 1 || regs[2] > 12) {
			  return false;
			}
			var currentYear = (new Date()).getFullYear();
			if(regs[3] < currentYear - 80 || regs[3] > currentYear) {
			  return false;
			}
			return true;
		} else {
			return false;
		}
		
	}
	
	function validatePhone(phone) {
		var re = /^\d+$/;
		return re.test(phone);
	}
	
		
	function validateCap(cap) {
		<?php if ($contest->location->language == "it") { ?>
		
			var re = /^\d{2}[01589]\d{2}$/;
			return re.test(cap);
		
		<?php } else { ?>
		
			var re = /^[0-9]+$/;
			return re.test(cap);
		
		<?php } ?>
		
	}

	$( document ).ready(function() {
		
		if ( $('#name').length ) {
			
			$( "#name" ).blur(function() {
				
				if ( $( "#name" ).val() != '') {
					$( "#name_flag" ).show();
					nameIsValid = true;
				} else {
					$( "#name_flag" ).hide();
					nameIsValid = false;
				}
				
			});
			
		};
		
		if ( $('#surname').length ) {
			
			$( "#surname" ).blur(function() {
				
				if ( $( "#surname" ).val() != '') {
					$( "#surname_flag" ).show();
					surnameIsValid = true;
				} else {
					$( "#surname_flag" ).hide();
					surnameIsValid = false;
				}
				
			});
			
		};
		
		if ( $('#birthdate').length ) {
			
			$( "#birthdate" ).blur(function() {
			
				if ( validateDate($( "#birthdate" ).val()) ) {
					$( "#birthdate_flag" ).show();
					birthdateIsValid = true;
				} else {
					$( "#birthdate_flag" ).hide();
					birthdateIsValid = false;
				}
				
			});
			
		};
		
		if ( $('#city').length ) {
			
			$( "#city" ).blur(function() {
			
			
			
				if ( $( "#city" ).val() != '' ) {
					$( "#city_flag" ).show();
					cityIsValid = true;
				} else {
					$( "#city_flag" ).hide();
					cityIsValid = false;
				}
				
			});
			
		};
		
		if ( $('#email').length ) {
			
			$( "#email" ).blur(function() {
			
			
			
				if ( validateEmail($( "#email" ).val()) ) {
					$( "#email_flag" ).show();
					emailIsValid = true;
				} else {
					$( "#email_flag" ).hide();
					emailIsValid = false;
				}
				
			});
			
		};
		
		if ( $('#phone').length ) {
			
			$( "#phone" ).blur(function() {
			
				if ( validatePhone($( "#phone" ).val()) ) {
					$( "#phone_flag" ).show();
					phoneIsValid = true;
				} else {
					$( "#phone_flag" ).hide();
					phoneIsValid = false;
				}
				
			});
			
		};
		
		if ( $('#cap').length ) {
			
			$( "#cap" ).blur(function() {
			
			
			
				if ( validateCap($( "#cap" ).val()) ) {
					$( "#cap_flag" ).show();
					capIsValid = true;
				} else {
					$( "#cap_flag" ).hide();
					capIsValid = false;
				}
				
			});
			
		};
		
		/*
		

		
		
		$( "#birthdate" ).change(function() {
			
			if ( validateDate($( "#birthdate" ).val()) ) {
				$( "#birthdate_flag" ).show();
				birthdateIsValid = true;
			} else {
				$( "#birthdate_flag" ).hide();
				birthdateIsValid = false;
			}
			
		});
		
		
		
		
		
		$( "#sex" ).blur(function() {
			
			
			
			if ( $( "#sex" ).val() != '' ) {
				$( "#sex_flag" ).show();
				sexIsValid = true;
			} else {
				$( "#sex_flag" ).hide();
				sexIsValid = false;
			}
			
		});
		*/
		
		
		
		
		
		
		
		
		
		
		
		
		/*
		$( "#tEc_flag" ).click(function() {
			
			if ( $( "#tecFlagImg" ).css("display") == 'none') {
				$( "#tecFlagImg" ).show();
				tEcApplyed = true;
			} else {
				$( "#tecFlagImg" ).hide();
				tEcApplyed = false;
			}
			
		});
		
		$( "#pri_flag" ).click(function() {
			
			if ( $( "#pri_flagImg" ).css("display") == 'none') {
				$( "#pri_flagImg" ).show();
				pri_flagApplyed = true;
			} else {
				$( "#pri_flagImg" ).hide();
				pri_flagApplyed = false;
			}
			
		});
		*/
		$("#registrationForm").submit(function(e){
			
			var errorMessage = '';
			

			
			
			if ( !nameIsValid && ($('#name').length) ) {
				errorMessage += "{{Lang::get('widget.signup_step.name_required')}}\n";
			}
			
			if ( !surnameIsValid && ($('#surname').length)  ) {
				errorMessage += "{{Lang::get('widget.signup_step.surname_required')}}\n";
			}
			
			if ( !birthdateIsValid && ($('#birthdate').length)  ) {
				errorMessage += "{{Lang::get('widget.signup_step.birthdate_required')}}\n";
			}
						
			if ( !cityIsValid && ($('#city').length)  ) {
				errorMessage += "{{Lang::get('widget.signup_step.city_required')}}\n";
			}
			
			if ( !emailIsValid && ($('#email').length)  ) {
				errorMessage += "{{Lang::get('widget.signup_step.email_required')}}\n";
			}
			
			if ( !phoneIsValid && ($('#phone').length)  ) {
				errorMessage += "{{Lang::get('widget.signup_step.phone_required')}}\n";
			}
			
			if ( !capIsValid && ($('#cap').length)  ) {
				errorMessage += "{{Lang::get('widget.signup_step.cap_required')}}\n";
			}
			
			
			var validCheck = $("#confirmPrivacy").is(':checked');
			
			if (!validCheck) {
				errorMessage += "{{Lang::get('widget.signup_step.privacycheck_required')}}\n";
				alert("{{Lang::get('widget.signup_step.privacycheck_required')}}");
			}
			
			/*
			if (!error_free){
				event.preventDefault(); 
			}
			*/
			
			if (errorMessage.length)
			{
				return false;
			}
			else
			{
				return true;
			}
			
			
			
			
		});
		
	});
	

</script>

@stop