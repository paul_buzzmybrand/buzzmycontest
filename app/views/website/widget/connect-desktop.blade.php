@extends('layouts.contest_popup')
@section('step') step2 @stop
@section('box')
<div class="widget">
    <div>
	 
		<div style="width: 400px; height: 0px; margin: 0 auto;">
		
		</div>
		
		<div style="width: 636; margin: 0 auto;">
			
			<?php 
			
			$enableFacebook = false;
			$enableYoutube = false;
			$enableTwitter = false;
			$counter = 0;
			$counter2 = 0;
			$sentence = Lang::get("widget.connect_step.sentence");
			$socialArray = array();
			
			if ( $contest->fbPages->count() > 0 ) {
				$enableFacebook = true;
				$socialArray[$counter] = 'Facebook';
				$counter++;
				$counter2++;
			}
			
			if ( $contest->ytPages->count() > 0 ) {
				$enableYoutube = true;
				$socialArray[$counter] = 'Youtube';
				$counter++;
			}
			
			if ( $contest->twPages->count() > 0 ) {
				$enableTwitter = true;
				$socialArray[$counter] = 'Twitter';
				$counter++;
				$counter2++;
			}
			
			
			$sentence .= $socialArray[0] ;
			
			for ( $i = 1; $i < $counter - 1; $i++ ) {
				$sentence .= ', ' . $socialArray[$i];
			}
			
			if ( $counter > 1 ) {
				$sentence .= ' ' . Lang::get("widget.connect_step.sentence_conj") . ' ' . $socialArray[$counter - 1];
			}
			
			?>
			<!--<img src="{{asset('images/companys/' . $contest->company->image_login) }}" class="popup-content">-->
			
			<p style="font-size: 22px; color: #8d8d8d">{{$sentence}}</p>
			
			<p style="font-size: 18px; color: #8d8d8d; margin-top: 20px;">
				<?php if ( $counter > 1 ) { ?>
					{{Lang::get("widget.connect_step.sentence_2")}} 
				<?php } else { ?>
					{{Lang::get("widget.connect_step.sentence_2_s")}}
				<?php } ?>
			</p>
		
			
		
			<div style="margin: 0 auto; width: {{250 * $counter2 + 20 * $counter}}px; height: 49px; margin-top: 20px; text-align: center">
		
				<?php if ( $enableFacebook ) { ?>
		
				<div style="float: left; width: 250px; height: 49px; margin: 0 10px;">
					{{ Form::open(array('action' => array('Website_WidgetController@facebook', $contest->id), 'method' => 'get', 'id' => 'facebook-form-login')) }}
					
					
					
					
					<?php if ( !$checkFacebook ) { ?>
						<input class="facebookButton" type="submit" name="login" value=""/>
					<?php } else { ?>
						<input class="facebookButtonWithOver" type="submit" name="login" value=""/>
					<?php } ?>
					
					
					
					<input type="hidden" name="device" value="desktop"/>
 					{{ Form::close() }}
				</div>
				
				<?php } ?>
				
				<?php if ( $enableTwitter ) { ?>
				
				<div style="float: left; width: 192px; height: 49px; margin: 0 10px;">
					{{ Form::open(array('action' => array('Website_WidgetController@twitter', $contest->id), 'method' => 'get', 'id' => 'twitter-form-login')) }}
					<?php if ( !$checkTwitter ) { ?>
						<input class="twitterButton" type="submit" name="login" value=""/>
					<?php } else { ?>
						<input class="twitterButtonWithOver" type="submit" name="login" value=""/>
					<?php } ?>
					<input type="hidden" name="device" value="desktop"/>
					{{ Form::close() }}
				</div>
				
				<?php } ?>
				
				
			
			</div>
			
			<div style="font-size: 18px; color: #8d8d8d; margin-top: 160px;" >
			<?php if (( $user_id ) && ($contest->id != '259')) { ?>
				<?php if ( $contest->type_id == 1 || $contest->type_id == 2 ) { ?>
					{{ HTML::linkAction('Website_WidgetController@choose', Lang::get("widget.connect_step.proceed"), array($contest->id, $user_id), array('class' => 'remove_decoration')) }} 
				<?php } else if (( $contest->type_id == 3 ) && ($contest->id != '259')) { ?>
					{{ HTML::linkAction('Website_WidgetController@insertEssay', Lang::get("widget.connect_step.proceed"), array($contest->id), array('class' => 'remove_decoration')) }} 
				<?php } ?>
			<?php } else if ($counter > 1) { ?>
				
				<a class="remove_decoration" href="javascript:alert('{{Lang::get("widget.connect_step.social_alert")}}')">{{Lang::get("widget.connect_step.proceed")}}</a>
			<?php } ?>
			</div>
			
			
		</div>

	
    </div>    
    
</div>

<script>

/*	
window.fbAsyncInit = function() {
	FB.init({
		appId      : '<?php echo $contest->company->fbApp_clientID ?>', // App ID
		status     : true, // check login status
		cookie     : true, // enable cookies to allow the server to access the session
		xfbml      : true  // parse XFBML
	});

// Additional initialization code here
};

// Load the SDK Asynchronously
(function(d){
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = "//connect.facebook.net/en_US/all.js";
	ref.parentNode.insertBefore(js, ref);
}(document));
	

$( "#disconnectLink" ).click(function() {

	FB.logout();
});

$(function() {
    $('#facebook-form-login').submit(function() {
        FB.logout();
        return true; // return false to cancel form action
    });
});
*/


$( document ).ready(function() {
    $( "#facebook-form-login" ).submit(function(event) {
		
		var contest_id = '{{ $contest->id }}';
		if (contest_id == '228') {
			
			var error_free=true;
			var capField = $("#cap");
			var validCap = capField.hasClass("valid");
			var validCheck = $("#confirmFB").is(':checked');
			if (!validCap || !validCheck) {
				error_free=false;
				alert('Prego inserire il CAP corretto e cliccare sulla casella di controllo');
			}
			if (!error_free){
				event.preventDefault(); 
			}
			else{			
				$("#new-loader").css("display","block");
			}
			
		} else {
			$("#new-loader").css("display","block");
		}
			
		
		
		
		
	});
	
	$( "#twitter-form-login" ).submit(function() {
		$("#new-loader").css("display","block");
	});
	
	
	$('#cap').on('input', function() {
		var input=$(this);
		var re = /^\d{2}[01589]\d{2}$/;
		var is_cap=re.test(input.val());
		if (is_cap) {
			input.removeClass("invalid").addClass("valid");
		}
		else {
			input.removeClass("valid").addClass("invalid");
		}
			
		
	});
	
	
});
	
</script>

@stop