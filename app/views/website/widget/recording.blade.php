<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
{{ HTML::style('website_public/css/bootstrap.css') }}
{{ HTML::style('website_public/css/common.css') }}
{{ HTML::script('website_public/hdfvr/js/swfobject.js') }}
{{ HTML::script('website_public/js/jquery.js') }}
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
<style>
    #flashContent { display:none; }
</style>
<style>
	body {
		font-family: Open Sans;
	}

    .spinner {
      margin: 300px 200px 300px 280px;
      width: 300px;
      height: 100px;
      text-align: center;
      font-size: 15px;
      position:absolute;
      color:white;
      top:-100px;
      right:-20px;
      display:none;
      background-color:#333333;
      padding-top:20px;
      border-radius:5px;
    }

    .spinner > div {
      background-color: white;
      height: 60%;
      width: 6px;
      display: inline-block;

      -webkit-animation: stretchdelay 1.2s infinite ease-in-out;
      animation: stretchdelay 1.2s infinite ease-in-out;
    }

    .spinner .rect2 {
      -webkit-animation-delay: -1.1s;
      animation-delay: -1.1s;
    }

    .spinner .rect3 {
      -webkit-animation-delay: -1.0s;
      animation-delay: -1.0s;
    }

    .spinner .rect4 {
      -webkit-animation-delay: -0.9s;
      animation-delay: -0.9s;
    }

    .spinner .rect5 {
      -webkit-animation-delay: -0.8s;
      animation-delay: -0.8s;
    }

    @-webkit-keyframes stretchdelay {
      0%, 40%, 100% { -webkit-transform: scaleY(0.4) }
      20% { -webkit-transform: scaleY(1.0) }
    }

    @keyframes stretchdelay {
      0%, 40%, 100% {
        transform: scaleY(0.4);
        -webkit-transform: scaleY(0.4);
      } 20% {
        transform: scaleY(1.0);
        -webkit-transform: scaleY(1.0);
      }
    }

	<?php if ( $contest->location->language == "it") { ?>

	.preview-button {
		background-image: url('{{asset('/website_public/contest_popup/4-button-preview-ita.png')}}');
		background-position: 0px 34px;
	}

	.preview-button-active:hover {
		background-position: 0px 0px;
	}

	<?php } ?>

	<?php if ( !$contest->needsUserRegistration() ) { ?>


	.step4
	{
		background-image: url('{{asset('/website_public/contest_popup/desktop-4palle-3.png')}}');
	}


	<?php } ?>

</style>
<script type="text/javascript">
	var flashvars = {
		userId : "XXY",
		qualityurl: "{{URL::to('website_public/hdfvr/500x350x30x90.xml')}}",
		recorderId: "123",
		sscode: "php",
		lstext : "Loading Settings..."
	};
	var params = {
		quality : "high",
		bgcolor : "#dfdfdf",
		play : "true",
		loop : "false",
		allowscriptaccess: "",
                base: '{{URL::to("website_public/hdfvr")}}/',
                settingsurl: "your_settings_url"

	};
	var attributes = {
		name : "VideoRecorder",
		id :   "VideoRecorder",
		align : "middle"
	};

	var mobile = false;
	var ua = navigator.userAgent.toLowerCase();
	if(navigator.appVersion.indexOf("iPad") != -1 || navigator.appVersion.indexOf("iPhone") != -1 || ua.indexOf("android") != -1 || ua.indexOf("ipod") != -1 || ua.indexOf("windows ce") != -1 || ua.indexOf("windows phone") != -1){
		mobile = true;
	}

	if(mobile == false){
		swfobject.embedSWF("{{URL::to('website_public/hdfvr/VideoRecorder.swf')}}", "flashContent", "500", "350", "10.3.0", "", flashvars, params, attributes);
	}else{
		alert("Aiuto!!!");

		//do nothing
	}
</script>

<!-- The following script is used for mobile devices ONLY -->
<script type="text/javascript">

	var previewDisabled = true;

	var pauseDisabled = true;
	var recDisabled = false;
	var stopDisabled = true;
	var submitDisabled = true;

$(document).ready(function() {
	var options = {
			target:   '#output',
			beforeSubmit:  beforeSubmit,
			success:       afterSuccess,
			uploadProgress: OnProgress,
			resetForm: true
		};

	 $('#recordingForm').submit(function() {
			$(this).ajaxSubmit(options);
			alert("Sono cazzi!!!");
			return false;
		});

	$('#recorderVideo').hide();

function afterSuccess()
{
	$('#submit-btn').show();
	$('#recorderVideo').show();
	$('#loading-img').hide();
	$('#progressbox').delay( 1000 ).fadeOut();
	fileName = document.getElementById("output").innerHTML;
	var res = fileName.split("#");
	var video = document.getElementById("recorderVideo");
	video.setAttribute("src", "mobileRecordings/"+res[0]);
}


function beforeSubmit(){

   if (window.File && window.FileReader && window.FileList && window.Blob)
	{

		if( !$('#FileInput').val())
		{
			$("#output").html("Are you kidding me?");
			return false
		}

		var fsize = $('#FileInput')[0].files[0].size;
		var ftype = $('#FileInput')[0].files[0].type;

		switch(ftype)
        {
			case 'video/mp4':
			case 'video/quicktime':
                break;
            default:
                $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
				return false
        }

		/*if(fsize>5242880)
		{
			$("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big file! <br />File is too big, it should be less than 5 MB.");
			return false
		}*/

		$('#submit-btn').hide();
		$('#loading-img').show();
		$("#output").html("");
	}
	else
	{
		$("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
		return false;
	}
}


function OnProgress(event, position, total, percentComplete)
{
	$('#progressbox').show();
    $('#progressbar').width(percentComplete + '%')
    $('#statustxt').html(percentComplete + '%');
    if(percentComplete>50)
        {
            $('#statustxt').css('color','#000');
        }
}

function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}





	$( ".preview-button" ).click(function() {
		if ( !previewDisabled ) {
			javascript:document.VideoRecorder.playVideo()
		}
	});


	$( "#circle1" ).click(function() {
		if ( !pauseDisabled ) {
			javascript:document.VideoRecorder.pause()
		}

	});


	$( "#circle2" ).click(function() {
		if ( !recDisabled ) {
			doRecord();
		}
	});


	$( "#circle3" ).click(function() {
		if ( !stopDisabled ) {
			javascript:document.VideoRecorder.stopVideo()
		}
	});


	$( ".submit-button" ).click(function() {
		if ( !submitDisabled ) {
			javascript:document.VideoRecorder.save()
		}
	});

	$('#inputTitleTakePicture').keyup(function(){
		var title = $('#inputTitleTakePicture').val();
		if( title != '')
			{
				$('#saveBtn').addClass('submit_button_active');
				document.getElementById("saveBtn").disabled = false;
			}
		else
		{
			$('#saveBtn').removeClass('submit_button_active');
			document.getElementById("saveBtn").disabled = true;
		}

	});

	$('#title-video').keyup(function(){
		var title2 = $('#title-video').val();
		if( title2 != '') {
			submitDisabled = false;
		} else {
			submitDisabled = true;
		}

		checkHover();

	});



	checkHover();



});

	function checkHover() {

		if ( !previewDisabled ) {
			$( ".preview-button" ).addClass('preview-button-active');
		} else {
			$( ".preview-button" ).removeClass('preview-button-active');
		}
		if ( !pauseDisabled ) {
			$( "#circle1" ).addClass('circle-active');
		} else {
			$( "#circle1" ).removeClass('circle-active');
		}
		if ( !recDisabled ) {
			$( "#circle2" ).addClass('circle-active');
		} else {
			$( "#circle2" ).removeClass('circle-active');
		}
		if ( !stopDisabled ) {
			$( "#circle3" ).addClass('circle-active');
		} else {
			$( "#circle3" ).removeClass('circle-active');
		}
		if ( !submitDisabled ) {
			$( ".submit-button" ).addClass('submit-button-active');
		} else {
			$( ".submit-button" ).removeClass('submit-button-active');
		}

	}

</script>
</head>
    <body style="overflow:hidden">
<div class="widget">
    <div class="popup-content">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
            <p>
            {{ trans('messages.uploading') }}</p>
            </div>
         </div>

		<?php if ( $contest->needsUserRegistration() ) { ?>
		<div style="width: 800px; height: 30px; margin: 0 auto; font-size: 20px; color: #8d8d8d">
				<div style="float:left; width:160px; height:30px">{{Lang::get('widget.signup')}}</div>
				<div style="float:left; width:160px; height:30px">{{Lang::get('widget.connect')}}</div>
				<div style="float:left; width:160px; height:30px">{{Lang::get('widget.actions')}}</div>
				<div style="float:left; width:160px; height:30px">{{Lang::get('widget.create')}}</div>
				<div style="float:left; width:160px; height:30px">{{Lang::get('widget.end')}}</div>
		</div>
		<?php } else { ?>
		<div style="width: 844px; height: 30px; margin: 0 auto; font-size: 20px; color: #8d8d8d">
				<div style="float:left; width:211px; height:30px">{{Lang::get('widget.connect')}}</div>
				<div style="float:left; width:211px; height:30px">{{Lang::get('widget.actions')}}</div>
				<div style="float:left; width:211px; height:30px">{{Lang::get('widget.create')}}</div>
				<div style="float:left; width:211px; height:30px">{{Lang::get('widget.end')}}</div>
		</div>

		<?php } ?>
		<div class="step step4">
		</div>

        @if(isset($error))
            <div class="alert alert-danger" style="margin-top:10px;">
                {{$error}}
            </div>
        @endif

        <div class="wrapper" style="width: 500px; height:350px;margin:20px auto 15px auto;">
            <div id="flashContent" >

				<!--window.location.href = "{{URL::to('website/widget/save_video')}}/"+streamName+"/"+streamDuration+"/{{$contestId}}/{{$userId}}/"+videoTitle+"";	-->
				<form id="recordingForm" action="{{ action('tab_HomeController@saveVideo') }}" method="POST">
					<input name="video" id="FileInput" type="file" accept="video/*" capture="camcorder" value="Start Recording" />
					<input type="submit"  id="submit-btn" value="Upload" />
					<input id="id_contest" name='id_contest' type='hidden' value="{{ $contestId }}">
					<input id="input_type" name='input_type' type='hidden' value="recorder">
					<img src="ajax-loader.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
				</form>
				<div id="progressbox" ><div id="progressbar"></div ><div id="statustxt">0%</div></div>
				<div id="output"></div>
				<video id='recorderVideo' controls width="320" height="240">
					<source src="">
				</video>


            </div>

		</div>
		<div style="clear:both"></div>

		<div style="width: 500px; height: 65px; margin: 0 auto;">
			<textarea id="title-video" name="title" style="float: left; width: 500px; height: 65px;border: solid 1px grey" maxlength="80" placeholder="{{Lang::get('widget.creation_step.placeholder')}} {{$contest->hashtag}}" maxlength="140"></textarea>
		</div>

		<div style="width: 500px; height: 21px; margin: 5px auto; font-size: 10px;">
			{{Lang::get('widget.creation_step.policy')}}
		</div>

		<div style="width: 550px; height: 60px; margin: 0 auto;">
			<div style="float:left; width: 207px; height: 60px;">
				<div class="preview-button" style="width: 134px; height: 34px; margin: 13px 50px 13px 23px;"></div>
			</div>
			<div class="pulsanti-azione" style="float:left; width: 156px; height: 60px; padding: 4px 0 4px 0;)">
				<div id="circle1" style="float: left;"></div>
				<div id="circle2" style="float: left;"></div>
				<div id="circle3" style="float: left;"></div>
				<div style="clear: left;"></div>
			</div>
			<div style="float:left; width: 187px; height: 60px;">
				<div class="submit-button" style="width: 134px; height: 34px; margin: 13px 0px 13px 30px;"></div>
			</div>
			<div style="clear: left"></div>
		</div>

		<style>
		.rec_button
		{
			width: 42px;
			height: 41px;
			background-size: contain;
			background-image: url('{{asset("website_public/contest_popup/Rec.png") }}');
		}
		.stop_button
		{
			width: 42px;
			height: 41px;
			background-size: contain;
			background-image: url('{{asset("website_public/contest_popup/Stop.png") }}');
		}
		.preview_button
		{
			width: 94px;
			height: 41px;
			background-size: contain;
			background-image: url('{{asset("website_public/contest_popup/PreviewButton.png") }}');
		}
		.pause_button
		{
			width: 42px;
			height: 41px;
			background-size: contain;
			background-image: url('{{asset("website_public/contest_popup/Pause.png") }}');
		}
		.submit_button
		{
			width: 180px;
			height: 41px;
			background-size: contain;
			background-image: url('{{asset("website_public/contest_popup/SubmitTakePicture.png") }}');
		}
		.submit_button_active
		{
			width: 183px;
			height: 41px;
			background-size: contain;
			background-image: url('{{asset("website_public/contest_popup/SubmitTakePictureActive.png") }}') !important;
		}
		#submissionTitleTakePicture
		{
			width: 183px;
			height: 41px;
			margin: 0 15px 0 20px;
			background-size: contain;
			float: left;
			background-image: url('{{asset("website_public/contest_popup/submissionTitleTakePicture.png") }}');

		}
		#inputTitleTakePicture
		{
			position: relative;
			top: 13px;
			height: 17px;
			width: 126px;
			left: 24px;
			background-color: #F6A029;
			color: white;
			font-weight: bold;
			text-align: center;
			font-size: 12px;

		}
		.input_btn
		{
			float: left;
			margin-right: 5px;
		}
		</style>

        <div class="popup-button" style="margin: 10px auto 0 auto;width:640px; display: none">
			<div class="input_btn" style="width:42px;height:41px;">
				<input type="button" disabled name="record" id="recordbtn" class='rec_button'/>
			</div>
				<input type="hidden" onclick="javascript:document.VideoRecorder.pauseRecording()" disabled name="pauseRecording" value="Pause Recording" id="pauseRecBtn" class='btn btn-primary '/>
				<input type="hidden" onclick="javascript:document.VideoRecorder.resumeRecording()" disabled name="resumeRecording" value="Resume Recording" id="resumeRecBtn" class='btn btn-primary '/>
			<div class="input_btn" style="width:42px;height:41px;">
				<input type="button" onclick="javascript:document.VideoRecorder.stopVideo()"  id="stopBtn" disabled class='stop_button' />
			</div>
			<div class="input_btn" style="width:42px;height:41px;">
				<input type="button" onclick="javascript:document.VideoRecorder.pause()" id="pauseBtn" disabled class='pause_button' />
			</div>
			<div class="input_btn" style="width:94px;height:41px;">
				<input type="button" onclick="javascript:document.VideoRecorder.playVideo()" id="playBtn" disabled  class='preview_button'/>
			</div>
			<!-- <input type="button" onclick="javascript:document.VideoRecorder.save()" value="{{ trans('messages.save_video') }}" id="saveBtn" disabled  class='btn btn-primary '/> -->

			<div id="submissionTitleTakePicture">
				<input type="text" id="inputTitleTakePicture" name="title" placeholder="INSERT TITLE" autocomplete="off" />
			</div>
			<div class="input_btn" style="width:180px;height:41px;margin-right:0;">
				<!--<input type="button" id="saveBtn" disabled  class='submit_button'/>-->

				<input type="button" onclick="javascript:document.VideoRecorder.save()" id="saveBtn" disabled  class='submit_button'/>
			</div>
		</div>

</div>
<script>

	function userHasCamMic(cam_number,mic_number,recorderId){
		//alert("userHasCamMic("+cam_number+","+mic_number+")");
		//this function is called when HDFVR is initialized
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}

	function btRecordPressed(recorderId){
		//alert("btRecordPressed");
		//this function is called whenever the Record button is pressed to start a recording
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
		document.getElementById("recordbtn").disabled = true;
		document.getElementById("pauseRecBtn").disabled = false;
		document.getElementById("resumeRecBtn").disabled = true;
		document.getElementById("stopBtn").disabled = false;
		document.getElementById("playBtn").disabled = true;
		document.getElementById("pauseBtn").disabled = true;
		document.getElementById("saveBtn").disabled = true;

		recDisabled = true;
		stopDisabled = false;
		previewDisabled = true;
		pauseDisabled = true;
		submitDisabled = true;

     checkHover();
	}

	function btPauseRecordingPressed(recorderId){
		//alert("btPauseRecordingPressed");
		//this function is called whenever the Pause Recording button is pressed to pause a recording
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page

		document.getElementById("recordbtn").disabled = true;
		document.getElementById("pauseRecBtn").disabled = true;
		document.getElementById("resumeRecBtn").disabled = false;
		document.getElementById("stopBtn").disabled = true;
		document.getElementById("playBtn").disabled = true;
		document.getElementById("pauseBtn").disabled = true;
		document.getElementById("saveBtn").disabled = true;

	}

	function btResumeRecordingPressed(recorderId){
		//alert("btResumeRecordingPressed");
		//this function is called whenever the Resume Recording button is pressed to resume a recording
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page

		document.getElementById("recordbtn").disabled = true;
		document.getElementById("pauseRecBtn").disabled = false;
		document.getElementById("resumeRecBtn").disabled = true;
		document.getElementById("stopBtn").disabled = false;
		document.getElementById("playBtn").disabled = true;
		document.getElementById("pauseBtn").disabled = true;
		document.getElementById("saveBtn").disabled = true;

	}

	function btStopRecordingPressed(recorderId){
		//alert("btStopRecordingPressed");
		//this function is called whenever a recording is stopped
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page

		document.getElementById("recordbtn").disabled = true;
		document.getElementById("pauseRecBtn").disabled = true;
		document.getElementById("resumeRecBtn").disabled = true;
		document.getElementById("stopBtn").disabled = true;
		document.getElementById("playBtn").disabled = true;
		document.getElementById("pauseBtn").disabled = true;
		document.getElementById("saveBtn").disabled = true;

		recDisabled = true;
		stopDisabled = true;
		previewDisabled = true;
		pauseDisabled = true;
		submitDisabled = true;


checkHover();
	}

	function btPlayPressed(recorderId){
		//alert("btPlayPressed");
		//this function is called whenever the Play button is pressed to start/resume playback
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page

		document.getElementById("recordbtn").disabled = true;
		document.getElementById("pauseRecBtn").disabled = true;
		document.getElementById("resumeRecBtn").disabled = true;
		document.getElementById("stopBtn").disabled = true;
		document.getElementById("playBtn").disabled = true;
		document.getElementById("pauseBtn").disabled = false;
		document.getElementById("saveBtn").disabled = true;

		recDisabled = true;
		stopDisabled = true;
		previewDisabled = true;
		pauseDisabled = false;
		submitDisabled = true;


          checkHover();
	}

	function btPausePressed(recorderId){
		//alert("btPausePressed");
		//this function is called whenever the Pause button is pressed during playback
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page

		document.getElementById("recordbtn").disabled = false;
		document.getElementById("pauseRecBtn").disabled = true;
		document.getElementById("resumeRecBtn").disabled = true;
		document.getElementById("stopBtn").disabled = true;
		document.getElementById("playBtn").disabled = false;
		document.getElementById("pauseBtn").disabled = true;
		var title = $('#inputTitleTakePicture').val();
		//alert(title);
		if( title != '')
		{
			$('#saveBtn').addClass('SubmitTakePictureActive');
			document.getElementById("saveBtn").disabled = false;
		}
		else
		{

			$('#saveBtn').removeClass('SubmitTakePictureActive');
			document.getElementById("saveBtn").disabled = true;
		}

		recDisabled = false;
		stopDisabled = true;
		previewDisabled = false;
		pauseDisabled = true;


		var title2 = $('#title-video').val();
		if( title2 != '') {
			submitDisabled = false;
		} else {
			submitDisabled = true;
		}


		checkHover();
	}

	function onUploadDone(streamName,streamDuration,userId,recorderId){
            document.getElementById("saveBtn").disabled = false;
            document.getElementById("recordbtn").disabled = false;
            document.getElementById("pauseRecBtn").disabled = true;
            document.getElementById("resumeRecBtn").disabled = true;
            document.getElementById("stopBtn").disabled = true;
            document.getElementById("playBtn").disabled = false;
            var title = $('#inputTitleTakePicture').val();
		if( title != '')
		{
			$('#saveBtn').addClass('SubmitTakePictureActive');
			document.getElementById("saveBtn").disabled = false;
		}
		else
		{

			$('#saveBtn').removeClass('SubmitTakePictureActive');
			document.getElementById("saveBtn").disabled = true;
		}

		submitDisabled = false;
		recDisabled = false;
		stopDisabled = true;
		previewDisabled = false;

		var title2 = $('#title-video').val();
		if( title2 != '') {
			submitDisabled = false;
		} else {
			submitDisabled = true;
		}

		checkHover();

		//alert("onUploadDone("+streamName+","+streamDuration+","+userId+")");

		//this function is called when the video/audio stream has been all sent to the video server and has been saved to the video server HHD,
		//on slow client->server connections, because the data can not reach the video server in real time, it is stored in the recorder's buffer until it is sent to the server, you can configure the buffer size in avc_settings.XXX

		//this function is called with 3 parameters:
		//streamName: a string representing the name of the stream recorded on the video server including the .flv extension
		//userId: the userId sent via flash vars or via the avc_settings.XXX file, the value in the avc_settings.XXX file takes precedence if its not empty
		//duration of the recorded video/audio file in seconds but acccurate to the millisecond (like this: 4.322)
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}

	function onSaveOk(streamName,streamDuration,userId,cameraName,micName,recorderId){
		//var videoTitle = $('#inputTitleTakePicture').val();
		var videoTitle = $('#title-video').val();
		//alert("onSaveOk("+streamName+","+streamDuration+","+"{{$userId}}"+","+videoTitle+","+"{{$contestId}}"+")");

		//the user pressed the [save] button inside the recorder and the save_video_to_db.XXX script returned save=ok
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page

		//var videoTitle = document.getElementById("txtVideoTitle").value;
		//var videoTitle = $('#inputTitleTakePicture').val();


		window.location.href = "{{URL::to('website/widget/save_video')}}/"+streamName+"/"+streamDuration+"/{{$contestId}}/{{$userId}}/"+videoTitle+"";



	}

	function onSaveFailed(streamName,streamDuration,userId,recorderId){
		//alert("onSaveFailed("+streamName+","+streamDuration+","+userId+")");

		//the user pressed the [save] button inside the recorder but the save_video_to_db.XXX script returned save=fail
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}
	function onSaveJpgOk(streamName,userId,recorderId){
		//alert("onSaveJpgOk("+streamName+","+userId+")");

		//the user pressed the [save] button inside the recorder and the save_video_to_db.XXX script returned save=ok
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}

	function onSaveJpgFailed(streamName,userId,recorderId){
		//alert("onSaveJpgFailed("+streamName+","+userId+")");
		//the user pressed the [save] button inside the recorder but the save_video_to_db.XXX script returned save=fail
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}

	function onFlashReady(recorderId){
		//alert("onFlashReady()");
		//you can now communicate with HDFVR using the JS Control API
		//Example : document.VideoRecorder.record(); will make a call to flash in order to start recording
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}

	function onPlaybackComplete(recorderId){
		//alert("onPlaybackComplete()")
		//this function is called when HDFVR plays back a recorded video and the playback completes
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
		document.getElementById("recordbtn").disabled = false;
		document.getElementById("pauseRecBtn").disabled = true;
		document.getElementById("resumeRecBtn").disabled = true;
		document.getElementById("stopBtn").disabled = true;
		document.getElementById("playBtn").disabled = false;
		document.getElementById("pauseBtn").disabled = true;
		document.getElementById("saveBtn").disabled = false;
		var title = $('#inputTitleTakePicture').val();
		if( title != '')
		{
			$('#saveBtn').addClass('SubmitTakePictureActive');
			document.getElementById("saveBtn").disabled = false;
		}
		else
		{

			$('#saveBtn').removeClass('SubmitTakePictureActive');
			document.getElementById("saveBtn").disabled = true;
		}

		recDisabled = false;
		stopDisabled = true;
		previewDisabled = false;
		pauseDisabled = true;
		submitDisabled = false;


		var title2 = $('#title-video').val();
		if( title2 != '') {
			submitDisabled = false;
		} else {
			submitDisabled = true;
		}

		checkHover();

	}

	function onRecordingStarted(recorderId){
		//alert("onRecordingStarted()")
		//this function is called when HDFVR connects to the media server and the recording starts.
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page

	}

	function onCamAccess(allowed,recorderId){
		//alert("onCamAccess("+allowed+")");
		//the user clicked Allow or Deny in the Camera/Mic access dialog box in Flash Player
		//when the user clicks Deny this function is called with allowed=false
		//when the user clicks Allow this function is called with allowed=true
		//you should wait for this function before allowing the user to cal the record() function on HDFVR
		//this function can be called anytime during the life of the HDFVR instance as the user has permanent access to the Camera/Mic access dialog box in Flash Player
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page

		if (allowed){
			recDisabled = false;
			document.getElementById("recordbtn").disabled = false;

		}else{
			recDisabled = true;
			document.getElementById("recordbtn").disabled = true;

		}

		checkHover();
	}

	function onFPSChange(recorderId, currentFPS){
		//alert("onFPSChange()" + recorderId + " " + currentFPS);
		//this function is called by HDFVR every second
		//currentFPS:the current frames-per-second that HDFVR reports (during recording, playback, uploading and saving) depending of the state of HDFVR.
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}

	function onConnectionClosed(recorderId){
            console.log(recorderId);
		//alert("onConnectionClosed()" + recorderId);
		//this function is called by HDFVR when the connection to the media server has been lost
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}

</script>
<script type="text/javascript">
    function doRecord()
    {
        var video = document.VideoRecorder.getStreamName();
        if(video)
        {
            if (confirm('{{ trans("messages.record_again_video") }}'))
            {
                document.VideoRecorder.record();
            } else {

            }
        }
        else
        {
            document.VideoRecorder.record();
        }
    }
    $('body').on( "click", "#recordbtn", function() {
        doRecord();
    });
</script>
<script type="text/javascript">
    $('body').on( "click", "#saveBtn", function() {
    	var videoTitle = document.getElementById("txtVideoTitle").value;
        if ((videoTitle != 'Insert Title') && (videoTitle != ''))
        {

	    	$('.spinner').css({
	            "display" : "block"
	        });
	        document.getElementById("saveBtn").disabled = true;
	        document.getElementById("recordbtn").disabled = true;
	        document.getElementById("pauseRecBtn").disabled = true;
	        document.getElementById("resumeRecBtn").disabled = true;
	        document.getElementById("stopBtn").disabled = true;
	        document.getElementById("playBtn").disabled = true;
	        document.getElementById("pauseBtn").disabled = true;
	        document.getElementById("saveBtn").disabled = true;
	        document.VideoRecorder.save();
        }
	    else
	    {
	    	alert('{{ trans("messages.alert_miss_title") }}');
	    	return false;
	    }

    });
	function inputFocus(i){
		if(i.value==i.defaultValue){ i.value=""; i.style.color="#000"; }
	}
	function inputBlur(i){
		if(i.value==""){ i.value=i.defaultValue; i.style.color="#888"; }
	}
</script>
    </body>
</html>
