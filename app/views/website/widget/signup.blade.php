<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sign Up</title>
        
	{{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
	{{ HTML::style('packages/bootstrap/css/bootstrap-responsive.min.css') }}
		
	<style>
	
	
		.alert { text-align: center; list-style-position:inside;}

	
	</style>
    	
</head>
<body>
    
	<div class="container" >
	
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3"	>
				<div style="text-align: center;">SIGN UP FORM</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3"	>
				<div style="text-align: center;"><br></div>
			</div>
		</div>

		<div class="row">
            
			<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3"	>
                
	            
				{{ Form::open(array('action' => array('Website_WidgetController@registerUser', $contest_id), 'method' => 'post')) }}

				@if(Session::has('message'))
				<ol class="alert">
					<b> {{ Session::get('message') }}</b>
					@foreach($errors->all() as $error)
					<li style="margin-left: 20px;">{{ $error }}</li>
					@endforeach    
				</ol>
				@endif
			  

				<div style="text-align: center;"><label for="name" style="width: 180px; text-align:left;">Nome</label><input type="texy" name="name" style="width: 250px;"/></div>
				<div style="text-align: center;"><label for="surname" style="width: 180px; text-align:left;">Cognome</label><input type="text" name="surname" style="width: 250px;"/></div>
				<div style="text-align: center;"><label for="email" style="width: 180px; text-align:left;">Email</label><input type="email" name="email" style="width: 250px;"/></div>
				<div style="text-align: center;"><label for="password" style="width: 180px; text-align:left;">Password</label><input type="password" name="password" style="width: 250px;"/></div>
				<div style="text-align: center;"><label for="password_confirmation" style="width: 180px; text-align:left;">Confirm password</label><input type="password" name="password_confirmation" style="width: 250px;"/></div>
				<div style="text-align: center;"><input type="submit" name="signup" value="Sign Up"/></div>
					
				{{ Form::close() }}
                        
                    				
 
            </div>
			
        </div>
		
    </div>
	
</body>
</html>
