{{ HTML::style('website_public/css/bootstrap.css') }}
{{ HTML::style('website_public/css/common.css') }}
<div class="widget"><div class="popup-content" style='margin-top:50px;'>
    @if($errors)    
        <div class="alert alert-danger">
            {{$errors->first()}}
        </div>
    @endif
    @if($message)    
        <div class="alert alert-danger">
            {{$message}}
        </div>
    @endif
    <div class="popup-button">
        <a onclick="mywindow = window.open('{{ URL::to("/website/widget/facebook/".$id."/".$videopart) }}', 'nazwa', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no,fullscreen=no,channelmode=no,width=300,height=350,left=300,top=100'); " class='btn btn-primary'>{{ trans('messages.log_in_with_facebook') }}</a></br>
        
    </div>
    <!--<div class="popup-button">
        <a href="{{URL::to('website/widget/twitter', $id)}}" onclick="$('.widget').html('');window.resizeTo(600,600);" class='btn btn-primary'>{{ trans('messages.log_in_with_twitter') }}</a>
    </div>
    <h4>or</h4>
    {{ Form::open(array('action' => array('Website_WidgetController@login', $id), 'method' => 'post')) }}
        {{ Form::text('username',null, array('class' => 'form-control', 'placeholder' => trans('messages.form_username')  )) }}
        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => trans('messages.form_password')  )) }}
        {{ Form::submit(trans('messages.log_in'), array('class' => 'btn btn-primary'))  }}
    {{ Form::close() }}   
    </div>  -->
</div>
{{ HTML::script('website_public/js/jquery.js') }}
<script>
    window.resizeTo(400,450);
</script>