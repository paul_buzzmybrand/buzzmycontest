{{ HTML::style('website_public/css/bootstrap.css') }}
{{ HTML::style('website_public/css/common.css') }}
<script>
    window.resizeTo(300,350);
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="widget">
    <div class="popup-content">
        <div class="widget-text" style="margin-top:20px;">
            {{ trans('messages.confirm_video') }}
        </div>
           
        <div class="popup-button" style="margin-top:20px;">
            <a  class='btn btn-primary' onclick="window.opener.location.reload(); window.close();">{{trans('messages.close')}}</a>
        </div> 
    </div>
</div>