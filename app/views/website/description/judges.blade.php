{{ HTML::style('website_public/css/bootstrap.css') }}
<div class="row">
    <div class="col-md-12">
        <div class="well" style="height:100%;">
            <h3>{{ trans('messages.judges') }}</h3>
            <div class="jumbotron">
                {{ trans('messages.desc_judges') }}
            </div>
        </div>
    </div>
</div>