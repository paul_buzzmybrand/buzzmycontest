{{ HTML::style('website_public/css/bootstrap.css') }}
<div class="row">
    <div class="col-md-12">
        <div class="well" style="height:100%;">
            <h3>{{ trans('messages.entries_user') }}</h3>
            <div class="jumbotron">
                {{ trans('messages.desc_entries') }}
            </div>
        </div>
    </div>
</div>