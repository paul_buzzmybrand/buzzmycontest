{{ HTML::style('website_public/css/bootstrap.css') }}
<div class="row">
    <div class="col-md-12">
        <div class="well" style="height:100%;">
            <h3>{{ trans('messages.brief_rules') }}</h3>
            <div class="jumbotron">
                {{ trans('messages.desc_brief_rules') }}
            </div>
        </div>
    </div>
</div>