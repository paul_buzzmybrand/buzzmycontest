
{{ HTML::style('website_public/css/bootstrap.css') }}
{{ HTML::style('website_public/css/common.css') }}
{{ HTML::script('website_public/hdfvr/js/swfobject.js') }}
{{ HTML::script('website_public/js/jquery.js') }}
<style>
    #flashContent { display:none; }	
</style>
<style>
    .spinner {
      margin: 300px 200px 300px 280px;
      width: 300px;
      height: 100px;
      text-align: center;
      font-size: 15px;
      position:absolute;
      color:white;
      top:-100px;
      right:-20px;
      display:none;
      background-color:#333333;
      padding-top:20px;
      border-radius:5px;
    }

    .spinner > div {
      background-color: white;
      height: 60%;
      width: 6px;
      display: inline-block;
      
      -webkit-animation: stretchdelay 1.2s infinite ease-in-out;
      animation: stretchdelay 1.2s infinite ease-in-out;
    }

    .spinner .rect2 {
      -webkit-animation-delay: -1.1s;
      animation-delay: -1.1s;
    }

    .spinner .rect3 {
      -webkit-animation-delay: -1.0s;
      animation-delay: -1.0s;
    }

    .spinner .rect4 {
      -webkit-animation-delay: -0.9s;
      animation-delay: -0.9s;
    }

    .spinner .rect5 {
      -webkit-animation-delay: -0.8s;
      animation-delay: -0.8s;
    }

    @-webkit-keyframes stretchdelay {
      0%, 40%, 100% { -webkit-transform: scaleY(0.4) }  
      20% { -webkit-transform: scaleY(1.0) }
    }

    @keyframes stretchdelay {
      0%, 40%, 100% { 
        transform: scaleY(0.4);
        -webkit-transform: scaleY(0.4);
      } 20% {
        transform: scaleY(1.0);
        -webkit-transform: scaleY(1.0);
      }
    }
</style>
<script type="text/javascript">
	var flashvars = {
		userId : "XXY",
		qualityurl: "{{URL::to('website_public/hdfvr/640x480x30x90.xml')}}",
		recorderId: "123",
		sscode: "php",
		lstext : "Loading Settings..."	
	};
	var params = {
		quality : "high",
		bgcolor : "#dfdfdf",
		play : "true",
		loop : "false",
		allowscriptaccess: "",
                base: '{{URL::to("website_public/hdfvr")}}/',
                settingsurl: "your_settings_url"
                
	};
	var attributes = {
		name : "VideoRecorder",
		id :   "VideoRecorder",
		align : "middle"
	};
	swfobject.embedSWF("{{URL::to('website_public/hdfvr/VideoRecorder.swf')}}", "flashContent", "640", "480", "10.3.0", "", flashvars, params, attributes);
</script>
<div class="widget">
    <div class="popup-content">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
            <p>
            {{ trans('messages.uploading') }}</p>
            </div>
         </div>
        @if(isset($error))    
            <div class="alert alert-danger" style="margin-top:10px;">
                {{$error}}
            </div>
        @endif
        <div class="wrapper"><br/><br/>
            <div id="flashContent" >

            </div>
	</div>		
        <div class="popup-button">
            <input type="button" disabled name="record" value="{{ trans('messages.start_record') }}" id="recordbtn" class='btn btn-primary '/>
            <input type="hidden" onclick="javascript:document.VideoRecorder.pauseRecording()" disabled name="pauseRecording" value="Pause Recording" id="pauseRecBtn" class='btn btn-primary '/>
            <input type="hidden" onclick="javascript:document.VideoRecorder.resumeRecording()" disabled name="resumeRecording" value="Resume Recording" id="resumeRecBtn" class='btn btn-primary '/>
            <input type="button" onclick="javascript:document.VideoRecorder.stopVideo()" value="{{ trans('messages.stop_record') }}" id="stopBtn" disabled class='btn btn-primary ' />
            <input type="button" onclick="javascript:document.VideoRecorder.playVideo()" value="{{ trans('messages.play_video') }}" id="playBtn" disabled  class='btn btn-primary '/>
            <input type="button" onclick="javascript:document.VideoRecorder.pause()" value="{{ trans('messages.stop_video') }}" id="pauseBtn" disabled class='btn btn-primary ' />
            <input type="button" onclick="javascript:document.VideoRecorder.save()" value="{{ trans('messages.save_video') }}" id="saveBtn" disabled  class='btn btn-primary '/>
    </div>
</div>
<script>
	
	function userHasCamMic(cam_number,mic_number,recorderId){
		//alert("userHasCamMic("+cam_number+","+mic_number+")");
		//this function is called when HDFVR is initialized
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}
	
	function btRecordPressed(recorderId){
		//alert("btRecordPressed");
		//this function is called whenever the Record button is pressed to start a recording
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
		
		//alert("btRecordPressed");
		//this function is called whenever the Record button is pressed to start a recording
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
		document.getElementById("recordbtn").disabled = true;
		document.getElementById("pauseRecBtn").disabled = false;
		document.getElementById("resumeRecBtn").disabled = true;
		document.getElementById("stopBtn").disabled = false;
		document.getElementById("playBtn").disabled = true;
		document.getElementById("pauseBtn").disabled = true;
		document.getElementById("saveBtn").disabled = true;
		return true;
		
              
          
	}
	
	function btPauseRecordingPressed(recorderId){
		//alert("btPauseRecordingPressed");
		//this function is called whenever the Pause Recording button is pressed to pause a recording
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
		
		document.getElementById("recordbtn").disabled = true;
		document.getElementById("pauseRecBtn").disabled = true;
		document.getElementById("resumeRecBtn").disabled = false;
		document.getElementById("stopBtn").disabled = true;
		document.getElementById("playBtn").disabled = true;
		document.getElementById("pauseBtn").disabled = true;
		document.getElementById("saveBtn").disabled = true;
        
	}
	
	function btResumeRecordingPressed(recorderId){
		//alert("btResumeRecordingPressed");
		//this function is called whenever the Resume Recording button is pressed to resume a recording
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
		
		document.getElementById("recordbtn").disabled = true;
		document.getElementById("pauseRecBtn").disabled = false;
		document.getElementById("resumeRecBtn").disabled = true;
		document.getElementById("stopBtn").disabled = false;
		document.getElementById("playBtn").disabled = true;
		document.getElementById("pauseBtn").disabled = true;
		document.getElementById("saveBtn").disabled = true;
 
	}
	
	function btStopRecordingPressed(recorderId){
		//alert("btStopRecordingPressed");
		//this function is called whenever a recording is stopped
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
		
		document.getElementById("recordbtn").disabled = true;
		document.getElementById("pauseRecBtn").disabled = true;
		document.getElementById("resumeRecBtn").disabled = true;
		document.getElementById("stopBtn").disabled = true;
		document.getElementById("playBtn").disabled = true;
		document.getElementById("pauseBtn").disabled = true;
		document.getElementById("saveBtn").disabled = true;

	}
	
	function btPlayPressed(recorderId){
		//alert("btPlayPressed");
		//this function is called whenever the Play button is pressed to start/resume playback
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
		
		document.getElementById("recordbtn").disabled = true;
		document.getElementById("pauseRecBtn").disabled = true;
		document.getElementById("resumeRecBtn").disabled = true;
		document.getElementById("stopBtn").disabled = true;
		document.getElementById("playBtn").disabled = true;
		document.getElementById("pauseBtn").disabled = false;
		document.getElementById("saveBtn").disabled = true;
          
	}
	
	function btPausePressed(recorderId){
		//alert("btPausePressed");
		//this function is called whenever the Pause button is pressed during playback
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
		
		document.getElementById("recordbtn").disabled = false;
		document.getElementById("pauseRecBtn").disabled = true;
		document.getElementById("resumeRecBtn").disabled = true;
		document.getElementById("stopBtn").disabled = true;
		document.getElementById("playBtn").disabled = false;
		document.getElementById("pauseBtn").disabled = true;
		document.getElementById("saveBtn").disabled = false;
        
	}
	
	function onUploadDone(streamName,streamDuration,userId,recorderId){
            document.getElementById("saveBtn").disabled = false;
            document.getElementById("recordbtn").disabled = false;
            document.getElementById("pauseRecBtn").disabled = true;
            document.getElementById("resumeRecBtn").disabled = true;
            document.getElementById("stopBtn").disabled = true;
            document.getElementById("playBtn").disabled = false;
            document.getElementById("pauseBtn").disabled = true;
		
		//alert("onUploadDone("+streamName+","+streamDuration+","+userId+")");
		
		//this function is called when the video/audio stream has been all sent to the video server and has been saved to the video server HHD, 
		//on slow client->server connections, because the data can not reach the video server in real time, it is stored in the recorder's buffer until it is sent to the server, you can configure the buffer size in avc_settings.XXX
		
		//this function is called with 3 parameters: 
		//streamName: a string representing the name of the stream recorded on the video server including the .flv extension
		//userId: the userId sent via flash vars or via the avc_settings.XXX file, the value in the avc_settings.XXX file takes precedence if its not empty
		//duration of the recorded video/audio file in seconds but acccurate to the millisecond (like this: 4.322)
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}
	
	function onSaveOk(streamName,streamDuration,userId,cameraName,micName,recorderId){
		//alert("onSaveOk("+streamName+","+streamDuration+","+userId+","+cameraName+","+micName+")");
	
		//the user pressed the [save] button inside the recorder and the save_video_to_db.XXX script returned save=ok
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
		
		var videoProject = getParameterProject();
		
		//window.location.href = "{{URL::to('website/widget/save_video')}}/"+streamName+"/"+streamDuration+"/{{$contestId}}/{{$userId}}";	}
		window.location.href = "{{URL::to('website/widget_CBC_Thailand/save_video')}}/"+streamName+"/"+streamDuration+"/{{$contestId}}/{{$userId}}/"+videoProject+"";	}
	
	function onSaveFailed(streamName,streamDuration,userId,recorderId){
		//alert("onSaveFailed("+streamName+","+streamDuration+","+userId+")");	
		
		//the user pressed the [save] button inside the recorder but the save_video_to_db.XXX script returned save=fail
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}
	function onSaveJpgOk(streamName,userId,recorderId){
		//alert("onSaveJpgOk("+streamName+","+userId+")");
		
		//the user pressed the [save] button inside the recorder and the save_video_to_db.XXX script returned save=ok
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}
	
	function onSaveJpgFailed(streamName,userId,recorderId){
		//alert("onSaveJpgFailed("+streamName+","+userId+")");	
		//the user pressed the [save] button inside the recorder but the save_video_to_db.XXX script returned save=fail
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}
	
	function onFlashReady(recorderId){
		//alert("onFlashReady()");
		//you can now communicate with HDFVR using the JS Control API
		//Example : document.VideoRecorder.record(); will make a call to flash in order to start recording
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}
	
	function onPlaybackComplete(recorderId){
		//alert("onPlaybackComplete()")
		//this function is called when HDFVR plays back a recorded video and the playback completes
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
		document.getElementById("recordbtn").disabled = false;
		document.getElementById("pauseRecBtn").disabled = true;
		document.getElementById("resumeRecBtn").disabled = true;
		document.getElementById("stopBtn").disabled = true;
		document.getElementById("playBtn").disabled = false;
		document.getElementById("pauseBtn").disabled = true;
		document.getElementById("saveBtn").disabled = false;
	}
	
	function onRecordingStarted(recorderId){
		//alert("onRecordingStarted()")
		//this function is called when HDFVR connects to the media server and the recording starts.
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
                
	}
	
	function onCamAccess(allowed,recorderId){
		//alert("onCamAccess("+allowed+")");
		//the user clicked Allow or Deny in the Camera/Mic access dialog box in Flash Player
		//when the user clicks Deny this function is called with allowed=false
		//when the user clicks Allow this function is called with allowed=true
		//you should wait for this function before allowing the user to cal the record() function on HDFVR
		//this function can be called anytime during the life of the HDFVR instance as the user has permanent access to the Camera/Mic access dialog box in Flash Player
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
		if (allowed){
			document.getElementById("recordbtn").disabled = false
		}else{
			document.getElementById("recordbtn").disabled = true
		}
	}
	
	function onFPSChange(recorderId, currentFPS){
		//alert("onFPSChange()" + recorderId + " " + currentFPS);
		//this function is called by HDFVR every second
		//currentFPS:the current frames-per-second that HDFVR reports (during recording, playback, uploading and saving) depending of the state of HDFVR.
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}
	
	function onConnectionClosed(recorderId){
            console.log(recorderId);
		//alert("onConnectionClosed()" + recorderId);
		//this function is called by HDFVR when the connection to the media server has been lost
		//recorderId: the recorderId sent via flash vars, to be used when there are many recorders on the same web page
	}
	
</script>
<script type="text/javascript">  
    function doRecord() 
    { 
        var video = document.VideoRecorder.getStreamName();
        if(video)
        {
            if (confirm('{{ trans("messages.record_again_video") }}')) 
            {        
                document.VideoRecorder.record();
            } else {              
                        
            } 
        }
        else
        {
            document.VideoRecorder.record();
        }        
    }       
    $('body').on( "click", "#recordbtn", function() {
        doRecord();  
    });
</script>
<script type="text/javascript">
    $('body').on( "click", "#saveBtn", function() {
        $('.spinner').css({
            "display" : "block"
        });
        document.getElementById("saveBtn").disabled = true;
        document.getElementById("recordbtn").disabled = true;
        document.getElementById("pauseRecBtn").disabled = true;
        document.getElementById("resumeRecBtn").disabled = true;
        document.getElementById("stopBtn").disabled = true;
        document.getElementById("playBtn").disabled = true;
        document.getElementById("pauseBtn").disabled = true;
        document.getElementById("saveBtn").disabled = true;
    });  
	function inputFocus(i){
		if(i.value==i.defaultValue){ i.value=""; i.style.color="#000"; }
	}
	function inputBlur(i){
		if(i.value==""){ i.value=i.defaultValue; i.style.color="#888"; }
	}	
	function getParameterProject() {
	var winURL = window.location.href;
    var queryStringArray = winURL.split("/");
	var project = queryStringArray.pop();
	project = decodeURIComponent(project.replace(/\+/g, " "))
	
    return project;
}
</script>
<script
<script>
    window.resizeTo(700,700);
</script>
    