{{ HTML::style('website_public/css/bootstrap.css') }}
{{ HTML::style('website_public/css/common.css') }}
{{ HTML::style('website_public/css/jquery.ui.css') }}
{{ HTML::style('//fonts.googleapis.com/css?family=Roboto:300,900') }}

<div class="widget">
    <div class="popup-content">
        <img class="widget-image" src="{{asset('/images/contest/'.$contestImage)}}">
        <div class="widget-text">
            {{ trans('messages.widget_signup_text') }}
        </div>
        <div class="popup-button">
            <!--<a href="{{URL::to('website/widget/login')}}" class='btn btn-lg btn-turqoise'>Sign in</a>-->
            <a class='btn btn-lg btn-primary' onclick="mywindow = window.open('{{ URL::to("/website/widget_CBC_Thailand/login/".$id) }}', 'nazwa', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no,fullscreen=no,channelmode=no,width=300,height=350,left=300,top=100'); ">{{ trans('messages.Join_the_contest') }}</a>
        </div>   
    </div>
</div>
