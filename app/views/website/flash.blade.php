@if (Session::has('flash.message'))
<div class="alert {{ Session::get('flash.class') }}">
{{ Session::get('flash.message') }}
</div>
@endif