@extends('layouts.company')
@section('content')
<div class="panel panel-default">
  <div class="panel-heading"><h2>{{ trans('messages.team') }}</h2></div>
    <div class="panel-body">
      <div class="carousel slide" id="myCarousel" data-interval="3000" data-ride="carousel">
    <!-- Carousel items -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="col-md-4">
                    <div class="well" style="height:400px">
                        <img src="holder.js/200x150/social">
                        <div class="team-name">
                            Jan Kowalski
                        </div>
                        <div class="team-role">
                            manager
                        </div>
                        <div class="team-email">
                            manager@jmtv.com
                        </div>
                        <div class="team-description">
                            Lorem ipsum dolor sit amet, atqui conclusionemque vis cu. An erat eligendi qui, id sed dicat graece aperiam, vis etiam euismod dissentias ne. Et eam stet iudicabit reprehendunt, mei ad nonumes disputando conclusionemque
                        </div>
                     </div> 
                </div>
                <div class="col-md-4">
                    <div class="well" style="height:400px">
                        <img src="holder.js/200x150/social">
                        <div class="team-name">
                            Jan Kowalski
                        </div>
                        <div class="team-role">
                            manager
                        </div>
                        <div class="team-email">
                            manager@jmtv.com
                        </div>
                        <div class="team-description">
                            Choro comprehensam cu vim. Ne quo nisl quodsi interpretaris, ad alii iusto eirmod eum, usu autem corpora convenire te.
                        </div>
                     </div> 
                </div>
                <div class="col-md-4">
                    <div class="well" style="height:400px">
                        <img src="holder.js/200x150/social">
                        <div class="team-name">
                            Jan Kowalski
                        </div>
                        <div class="team-role">
                            manager
                        </div>
                        <div class="team-email">
                            mmanager@jmtv.com
                        </div>
                        <div class="team-description">
                            At mea lobortis adolescens, vix id quem persequeris, tota evertitur persecuti quo ad. Ut usu nobis albucius, everti equidem qui in. Ius ornatus nominavi ullamcorper ei
                        </div>
                     </div> 
                </div>
             </div>
            <div class="item">
                <div class="col-md-4">
                    <div class="well" style="height:400px">
                        <img src="holder.js/200x150/social">
                        <div class="team-name">
                            Jan Kowalski
                        </div>
                        <div class="team-role">
                            manager
                        </div>
                        <div class="team-email">
                            manager@jmtv.com
                        </div>
                        <div class="team-description">
                            Lorem ipsum dolor sit amet, atqui conclusionemque vis cu. An erat eligendi qui, id sed dicat graece aperiam, vis etiam euismod dissentias ne. Et eam stet iudicabit reprehendunt, mei ad nonumes disputando conclusionemque
                        </div>
                     </div> 
                </div>
                <div class="col-md-4">
                    <div class="well" style="height:400px">
                        <img src="holder.js/200x150/social">
                        <div class="team-name">
                            Jan Kowalski
                        </div>
                        <div class="team-role">
                            manager
                        </div>
                        <div class="team-email">
                            manager@jmtv.com
                        </div>
                        <div class="team-description">
                            Choro comprehensam cu vim. Ne quo nisl quodsi interpretaris, ad alii iusto eirmod eum, usu autem corpora convenire te.
                        </div>
                     </div> 
                </div>
                <div class="col-md-4">
                    <div class="well" style="height:400px">
                        <img src="holder.js/200x150/social">
                        <div class="team-name">
                            Jan Kowalski
                        </div>
                        <div class="team-role">
                            manager
                        </div>
                        <div class="team-email">
                            manager@jmtv.com
                        </div>
                        <div class="team-description">
                            At mea lobortis adolescens, vix id quem persequeris, tota evertitur persecuti quo ad. Ut usu nobis albucius, everti equidem qui in. Ius ornatus nominavi ullamcorper ei
                        </div>
                     </div> 
                </div>
             </div>

           

            

          

        </div>
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a
      </div>
    </div>
  </div>
</div>



@stop
