@extends('layouts.company')
@section('content')
{{ HTML::style('website_public/css/jquery.ui.base.css') }}
{{ HTML::style('website_public/css/jquery.ui.theme.css') }}
<div class="panel panel-default">
  <div class="panel-heading"><h3>{{ trans('messages.new_contest_step_one') }}</h3></div>
  <div class="panel-body">
      <div class="row" style='margin-top:50px;'>
        <div class="col-md-2">
        </div>
        <div class="col-md-7">
            <?php if($messages): ?>
            <div class="alert alert-danger">
                <?php foreach ($messages->all() as $message): ?>
                    <?php echo $message; ?></br>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
            @if($error)
                <div class="alert alert-danger">
                {{$error}}
                </div>
            @endif
            {{ Form::open(array('action' => array('Website_CompanyController@draftContest', $id), 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form'))}}
                <div class="form-group">
                  <label for="contestName" class="col-sm-4 control-label normal-form"><a id="contest-name" class="form-description">(*)</a>&nbsp;{{ trans('messages.contest_name') }}</label>
                  <div class="col-sm-8">
                      {{ Form::text('contestName', $contest['contestName'], array('class' => 'form-control', 'id' => 'email' )) }}
                  </div>
                </div>
                <div class="form-group">
                  <label for="startDate" class="col-sm-4 control-label normal-form"><a id="start-date" class="form-description">(*)</a>&nbsp;{{ trans('messages.start_date') }}</label>
                  <div class="col-sm-7">
                      {{ Form::text('startDate', null, array('class' => 'form-control', 'id' => 'datepicker', 'placeholder' => trans('messages.start_date') )) }}
                  </div>

                </div>
                <div class="form-group">
                  <label for="endDate" class="col-sm-4 control-label normal-form"><a id="end-date" class="form-description">(*)</a>&nbsp;{{ trans('messages.end_date') }}</label>
                  <div class="col-sm-7">
                      {{ Form::text('endDate', null, array('class' => 'form-control', 'id' => 'datepicker2', 'placeholder' => trans('messages.end_date') )) }}
                  </div>
                </div>
                <div class="form-group">
                  <label for="appPicture" class="col-sm-4 control-label normal-form"><a id="upload-app-picture" class="form-description">(*)</a>&nbsp;{{ trans('messages.app_picture') }}</label>
                  <div class="col-sm-5">
                            <a class='btn btn-primary' href='javascript:;'>
                                    {{ trans('messages.choose_photo') }}
                                    <input type="file" style='width:100%;height:100%;position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="appPicture" size="40" id='files-app'  onchange='$("#upload-file-info-app").html($(this).val());'>
                            </a>
                            <span class='label label-info' id="upload-file-info-app"></span>
                  </div>
                  <div class='col-sm-3'>
                      <output id="list-app"></output>
                  </div>
                </div>
                <div class="form-group">
                  <label for="webPicture" class="col-sm-4 control-label normal-form"><a id="upload-web-picture" class="form-description">(*)</a>&nbsp;{{ trans('messages.web_picture') }}</label>
                  <div class="col-sm-5">
                            <a class='btn btn-primary' href='javascript:;'>
                                    {{ trans('messages.choose_photo') }}
                                    <input type="file" style='width:100%;height:100%;position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="webPicture" size="40" id='files-web'  onchange='$("#upload-file-info").html($(this).val());'>
                            </a>
                            <span class='label label-info' id="upload-file-info"></span>
                  </div>
                  <div class='col-sm-3'>
                      <output id="list-web"></output>
                  </div>
                </div>
                <div class="form-group">
                  <label for="briefRules" class="col-sm-4 control-label normal-form"><a id="brief-and-rules" class="form-description">(*)</a>&nbsp;{{ trans('messages.brief_rules') }}</label> 
                  <div class="col-sm-8">
                      {{ Form::textarea('briefRules', $contest['briefRules'], array('class' => 'form-control', 'id' => 'email', 'placeholder' => trans('messages.brief_rules'), 'rows' => '4' )) }}
                  </div>
                </div>
                <div class="form-group">
                  <label for="Judges" class="col-sm-4 control-label normal-form"><a id="judges" class="form-description">(*)</a>&nbsp;{{ trans('messages.judges') }}</label>
                  <div class="col-sm-8">
                      <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-3">

                            </div>
                            <div class="col-sm-1">
                              {{ Form::radio('judges', 'internalJudging', true )}}
                            </div>
                            <div class="col-sm-6" style='text-align:left;'>
                              {{ trans('messages.internal_judging') }}
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-3">

                            </div>
                            <div class="col-sm-1">
                              {{ Form::radio('judges', 'jmtScore') }}
                            </div>
                            <div class="col-sm-6" style='text-align:left;'>
                              {{ trans('messages.jmt_score') }}
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-4 control-label normal-form"><a id="entries-user" class="form-description">(*)</a>&nbsp;{{ trans('messages.entries_user') }}</label> 
                  <div class="col-sm-8">
                      <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-3">

                            </div>
                            <div class="col-sm-1">
                              {{ Form::radio('entriesUser', 'onePerUser', true) }}
                            </div>
                            <div class="col-sm-6" style='text-align:left;'>
                              {{ trans('messages.one_per_user') }}
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-3">

                            </div>
                            <div class="col-sm-1">
                              {{ Form::radio('entriesUser', 'unlimitedPerUser') }}
                            </div>
                            <div class="col-sm-6" style='text-align:left;'>
                              {{ trans('messages.unlimited_per_user') }}
                            </div>
                        </div>

                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-4 control-label normal-form"><a id="location" class="form-description">(*)</a>&nbsp;{{ trans('messages.location') }}</label>  
                  <div class="col-sm-8">  
                      {{ Form::select('location', array('italy' => 'Italy', 'singapore' => 'Singapore'),null, array('class' => 'form-control')) }}
                  </div>                
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-4 control-label normal-form"><a id="reward-type" class="form-description">(*)</a>&nbsp;{{ trans('messages.reward_type') }}</label>
                  <div class="col-sm-8">  
                      {{ Form::select('rewardType', array('cash' => trans('messages.cash'), 'vouchers' => trans('messages.vouchers'), 'productsServices' => trans('messages.products_services'), 'others' => trans('messages.others')),null, array('class' => 'form-control', 'id' => 'select-reward')) }}           
                  </div>                
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-4 control-label normal-form" style="padding-top:56px;"><a id="rewards" class="form-description">(*)</a>&nbsp;{{ trans('messages.rewards') }}</label>
                  <div class="col-sm-8">
                        <div class='row'>
                            <div class='col-sm-12'>
                                  <div class='col-sm-2'>
                                      {{ Form::label('email', trans('messages.1st'), array('class' => 'col-sm-3 control-label normal-form')) }}
                                   </div>
                                <div class="reward-type-1">
                                   <div class='col-sm-5'>
                                      {{ Form::text('1st', null, array('class' => 'form-control', 'id' => 'email' )) }}
                                   </div>
                                   <div class='col-sm-5'>
                                      {{ Form::select('currency1st', array('SGD' => 'SGD', 'IDR' => 'IDR', 'THB' => 'THB', 'HKD' => 'HKD', 'USD' => 'USD', 'EUR' => 'EUR'),null, array('class' => 'form-control')) }}
                                   </div>
                                </div>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-sm-12'>
                                <div class='col-sm-2'>
                                    {{ Form::label('email', trans('messages.2nd'), array('class' => 'col-sm-3 control-label normal-form')) }}
                                 </div>
                                <div class="reward-type-2">
                                   <div class='col-sm-5'>
                                      {{ Form::text('2nd', null, array('class' => 'form-control', 'id' => 'email' )) }}
                                   </div>
                                   <div class='col-sm-5'>
                                      {{ Form::select('currency2nd', array('SGD' => 'SGD', 'IDR' => 'IDR', 'THB' => 'THB', 'HKD' => 'HKD', 'USD' => 'USD', 'EUR' => 'EUR'),null, array('class' => 'form-control')) }}
                                   </div>
                                </div>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-sm-12'>
                                <div class='col-sm-2'>
                                    {{ Form::label('email', trans('messages.3rd'), array('class' => 'col-sm-3 control-label normal-form')) }}
                                </div>
                                <div class="reward-type-3">
                                   <div class='col-sm-5'>
                                      {{ Form::text('3rd', null, array('class' => 'form-control', 'id' => 'email' )) }}
                                   </div>
                                   <div class='col-sm-5'>
                                      {{ Form::select('currency3rd', array('SGD' => 'SGD', 'IDR' => 'IDR', 'THB' => 'THB', 'HKD' => 'HKD', 'USD' => 'USD', 'EUR' => 'EUR'),null, array('class' => 'form-control')) }}
                                   </div>
                                </div>
                            </div>
                        </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-6">
                      {{ Form::submit(trans('messages.draft'), array('class' => 'btn btn-primary', 'name' => 'submit')) }}
                      {{ Form::submit(trans('messages.submit'), array('class' => 'btn btn-primary', 'name' => 'submit')) }}  
                  </div>
                </div>
            {{ Form::close() }} 
        </div>
    </div>
  </div>
</div>



{{ HTML::script('website_public/js/jquery.ui.core.js') }}
{{ HTML::script('website_public/js/jquery.ui.datepicker.js') }}
<script>
$(function() {
        $( "#datepicker" ).datepicker({
                showOn: "button",
                buttonImage: "{{ URL::to('images/calendar.gif') }}",
                buttonImageOnly: true
        });
        $( "#datepicker2" ).datepicker({
                showOn: "button",
                buttonImage: "{{ URL::to('images/calendar.gif') }}",
                buttonImageOnly: true
        });
});
</script>
<script>
    $('#select-reward').on({
    'change' : function() {
        var value = $(this).find('option:selected').val();  
 
        if(value == 'vouchers')
        {
            $('.reward-type-1').html('');
            $('.reward-type-2').html('');
            $('.reward-type-3').html('');
            $('.reward-type-1').html('<div class="col-sm-10 reward-text">{{ trans("messages.voucher_description") }}</div>');
            $('.reward-type-2').html('<div class="col-sm-10 reward-text">{{ trans("messages.voucher_description") }}</div>');
            $('.reward-type-3').html('<div class="col-sm-10 reward-text">{{ trans("messages.voucher_description") }}</div>');
        }
        else if(value == 'cash')
        {
            $('.reward-type-1').html('');
            $('.reward-type-2').html('');
            $('.reward-type-3').html('');
            $('.reward-type-1').html('<div class="col-sm-5"> {{ Form::text("1st", Input::old("1st"), array("class" => "form-control", "id" => "email" )) }} </div><div class="col-sm-5">{{ Form::select("size", array("SGD" => "SGD", "IDR" => "IDR", "THB" => "THB", "HKD" => "HKD", "USD" => "USD", "EUR" => "EUR"),null, array("class" => "form-control")) }}</div>');
            $('.reward-type-2').html('<div class="col-sm-5"> {{ Form::text("2nd", Input::old("2nd"), array("class" => "form-control", "id" => "email" )) }} </div><div class="col-sm-5">{{ Form::select("size", array("SGD" => "SGD", "IDR" => "IDR", "THB" => "THB", "HKD" => "HKD", "USD" => "USD", "EUR" => "EUR"),null, array("class" => "form-control")) }}</div>');
            $('.reward-type-3').html('<div class="col-sm-5"> {{ Form::text("3rd", Input::old("3rd"), array("class" => "form-control", "id" => "email" )) }} </div><div class="col-sm-5">{{ Form::select("size", array("SGD" => "SGD", "IDR" => "IDR", "THB" => "THB", "HKD" => "HKD", "USD" => "USD", "EUR" => "EUR"),null, array("class" => "form-control")) }}</div>');
       }
        else if(value == 'others')
        {
            $('.reward-type-1').html('');
            $('.reward-type-2').html('');
            $('.reward-type-3').html('');
            $('.reward-type-1').html('<div class="col-sm-10 reward-text">{{ trans("messages.other_description") }}</div>');
            $('.reward-type-2').html('<div class="col-sm-10 reward-text">{{ trans("messages.other_description") }}</div>');
            $('.reward-type-3').html('<div class="col-sm-10 reward-text">{{ trans("messages.other_description") }}</div>');
        }
        else if(value == 'productsServices')
        {
            $('.reward-type-1').html('');
            $('.reward-type-2').html('');
            $('.reward-type-3').html('');
            $('.reward-type-1').html('<div class="col-sm-10 reward-text">{{ trans("messages.products_services_desc") }}</div>');
            $('.reward-type-2').html('<div class="col-sm-10 reward-text">{{ trans("messages.products_services_desc") }}</div>');
            $('.reward-type-3').html('<div class="col-sm-10 reward-text">{{ trans("messages.products_services_desc") }}</div>');
        }
    }
});
</script>
<script>
    document.getElementById('files-app').addEventListener('change', handleFileSelect, false);
    function handleFileSelect(evt) {
        var files = evt.target.files;
        var f = files[0];
        var reader = new FileReader();
          reader.onload = (function(theFile) {
                return function(e) {
                  document.getElementById('list-app').innerHTML = ['<img src="', e.target.result,'" title="', theFile.name, '" width="50" />'].join('');
                };
          })(f);
           
          reader.readAsDataURL(f);
}
</script>
<script>
    document.getElementById('files-web').addEventListener('change', handleFileSelect, false);
    function handleFileSelect(evt) {
        var files = evt.target.files;
        var f = files[0];
        var reader = new FileReader();
          reader.onload = (function(theFile) {
                return function(e) {
                  document.getElementById('list-web').innerHTML = ['<img src="', e.target.result,'" title="', theFile.name, '" width="50" />'].join('');
                };
          })(f);
           
          reader.readAsDataURL(f);
}
</script>
<script>    
        $( ".form-description" ).click(function(event) {
            event.preventDefault();
            var input = $(this).attr('id');
            var url = "{{URL::to('website/form/description')}}";
            var request = url+"/"+input;
            window.open(request, 'description', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no,fullscreen=no,channelmode=no,width=400,height=450,left=300,top=100');
        });
</script>

@stop