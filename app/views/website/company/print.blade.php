{{ HTML::style('website_public/css/bootstrap.css') }}
<div class='container'>
    <div class="row"style='margin-top:20px;'>
        <div class='col-md-12'>
            <h2 style='text-align:center;'>{{$contest['title']}}</h2>
            <div class="well" >
                <h3>{{ trans('messages.analytics') }}</h3>
                <div class='jumbotron' style="text-align:left;padding-top:20px;padding-bottom:20px;">
                    <div class="row">
                        <div class='col-sm-6'>
                            <b>{{ trans('messages.video_collected') }}</b>&nbsp;{{$contest['number_to_video_selected']}}</br>
                            <b>{{ trans('messages.user_engaged') }}</b>&nbsp;{{$contest['user_engaded']}}</br>
                            <b>{{ trans('messages.average_video') }}</b>&nbsp;{{$contest['awerage_views_video']}}</br>
                            <b>{{ trans('messages.total_views') }}</b>&nbsp;{{$contest['total_views']}}</br>
                        </div>
                        <div class='col-sm-6'>
                            <b>{{ trans('messages.induced_views') }}</b>&nbsp;{{$contest['induced_virality_views']}}</br>
                            <b>{{ trans('messages.campaign_score') }}</b>&nbsp;{{$contest['campaign_jmt_score']}}</br>
                            <b>{{ trans('messages.video_highest_score') }}</b>&nbsp;{{$contest['video_with_heighest_score']}}</br>
                        </div>
                    </div>
                </div>
                <h3>{{ trans('messages.engagement_details') }}</h3>
                <div class='jumbotron' style="padding-top:20px;padding-bottom:20px;">
                    <div class="row" style="text-align:left;">
                        <div class='col-sm-6'>
                            <b>{{ trans('messages.facebook_likes') }}</b>&nbsp;{{$contest['facebook_likes']}}</br>
                            <b>{{ trans('messages.facebook_coments') }}</b>&nbsp;{{$contest['facebook_comments']}}</br>
                            <b>{{ trans('messages.facebook_shares') }}</b>&nbsp;{{$contest['facebook_shares']}}</br>
                            <b>{{ trans('messages.twitter_favouriters') }}</b>&nbsp;{{$contest['twitter_favourites']}}</br>
                            <b>{{ trans('messages.twitter_replies') }}</b>&nbsp;{{$contest['twitter_replies']}}</br>
                        </div>
                        <div class='col-sm-6'>
                            <b>{{ trans('messages.twitter_retweets') }}</b>&nbsp;{{$contest['twitter_reetwets']}}</br>
                            <b>{{ trans('messages.youtube_likes') }}</b>&nbsp;{{$contest['youtube_likes']}}</br>
                            <b>{{ trans('messages.youtube_comments') }}</b>&nbsp;{{$contest['youtube_commnets']}}</br>
                            <b>{{ trans('messages.youtube_shares') }}</b>&nbsp;{{$contest['youtube_shares']}}</br>
                        </div>
                    </div>
                </div>

            </div>
            <div class='row'>
                <div class='col-md-12'>
                    <img style='width:100%;' src='{{asset($graph)}}'>
                </div>
            </div>
        </div>
    </div>
</div>