@extends('layouts.company')
@section('content')

<div class="panel panel-default">
  <div class="panel-heading"><h3>{{ trans('messages.new_contest_step_two') }}</h3></div>
  <div class="panel-body">
      <div class="row" style='margin-top:50px;'>
    {{ Form::open(array('action' => array('Website_CompanyController@launchContestStep2', $id), 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form'))}}
    <div class='col-md-3'>
       <label style="color:black;">
        <div class="well well-sm" >
            <div class="jumbotron payment-plan">
                <h4 class='title-payment-plan'>{{ trans('messages.free_plan') }}</h4>
            </div>
            {{ Form::radio('jakas', 'value', true, array('class' => 'plan')) }}
        </div>
    </label>
    </div>
    <div class='col-md-3'>
       <label style="color:black;">
            <div class="well well-sm">
                <div class="jumbotron payment-plan">
                    <h4 class='title-payment-plan'>{{ trans('messages.plan_1') }}</h4>
                </div>
                {{ Form::radio('jakas', 'value',null, array('class' => 'plan')) }}
            </div>
        </label>
    </div>
    <div class='col-md-3'>
        <label style="color:black;">
            <div class="well well-sm">
                <div class="jumbotron payment-plan">
                    <h4 class='title-payment-plan'>{{ trans('messages.plan_2') }}</h4>
                </div>
                {{ Form::radio('jakas', 'value',null, array('class' => 'plan')) }}
            </div>
        </label>
    </div>
    <div class='col-md-3'>
        <div class="well well-sm">
            <div class="jumbotron payment-plan">
                <div class="checkbox">
                    <label style="color:black;">
                      {{ Form::checkbox('name', 'value',  null, array('class' => 'function')) }}
                      Function&nbsp;<a id="function" class="function-description">(*)</a>
                    </label>
                </div>
                <div class="checkbox">
                    <label style="color:black;">
                      {{ Form::checkbox('name', 'value',  null, array('class' => 'function')) }}
                      Function&nbsp;<a id="function" class="function-description">(*)</a>
                    </label>
                </div>
                <div class="checkbox">
                    <label style="color:black;">
                      {{ Form::checkbox('name', 'value',  null, array('class' => 'function')) }}
                      Function&nbsp;<a id="function" class="function-description">(*)</a>
                    </label>
                </div>
                <div class="checkbox">
                    <label style="color:black;">
                      {{ Form::checkbox('name', 'value',  null, array('class' => 'function')) }}
                      Function&nbsp;<a id="function" class="function-description">(*)</a>
                    </label>
                </div>
                <div class="checkbox">
                    <label style="color:black;" >
                      {{ Form::checkbox('name', 'value',  null, array('class' => 'function')) }}
                      Function&nbsp;<a id="function" class="function-description">(*)</a>
                    </label>
                </div>
                <div class="checkbox">
                    <label style="color:black;">
                      {{ Form::checkbox('name', 'value',  null, array('class' => 'function')) }}
                      Function&nbsp;<a id="function" class="function-description">(*)</a>
                    </label>
                </div>
                <div class="checkbox">
                    <label style="color:black;">
                      {{ Form::checkbox('name', 'value',  null, array('class' => 'function')) }}
                      Function&nbsp;<a id="function" class="function-description">(*)</a>
                    </label>
                </div>
                <div class="checkbox">
                    <label style="color:black;">
                      {{ Form::checkbox('name', 'value',  null, array('class' => 'function')) }}
                      Function&nbsp;<a id="function" class="function-description">(*)</a>
                    </label>
                </div>

            </div>
            <label>
                {{ Form::radio('jakas', 'value', null, array('class' => 'plan', 'id' => 'functions')) }}
            </label>
        </div>
    </div>
    <div class=" col-sm-12">
      <button type="submit" class="btn btn-primary btn-lg">{{ trans('messages.submit') }}</button>
    </div>
</div>   
{{ Form::close() }}  
  </div>
</div>



<script>
    $('.function').attr("disabled",true);
    $('.plan').on({
    'change' : function() {
        if ($("#functions").is(":checked")) {
            $('.function').attr("disabled",false);
        }
        else {
            $('.function').attr("disabled",true);
        }
    }
});
</script>

<script>
    $( "body" ).on('click', '.function-description',function(event) {
        event.preventDefault();
        var input = $(this).attr('id');
        var url = "{{URL::to('website/function/description')}}";
        var request = url+"/"+input;
        window.open(request, 'description', 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=no,fullscreen=no,channelmode=no,width=400,height=450,left=300,top=100');
    });
</script>
@stop