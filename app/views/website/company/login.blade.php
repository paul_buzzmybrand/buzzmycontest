<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/common.css') }}
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="container page">
            <div class="row" style='margin-top:100px;'>
                <div style='color:#a94442;text-align:center;' class='error-login'></div>
                <div id="login" class="white-popup mfp-with-anim mfp-hide">
                    <div class='popup-title'>{{ trans('messages.log_in') }}</div>
                    <div class="popup-content">
                    @if ($request == 'dashboard')
                        {{ Form::open(array('url' => URL::to('website/company/login-request', $request), 'method' => 'post',  'id' => 'company-login')) }}
                    @endif
                    @if ($request == 'launch-a-contest')
                        {{ Form::open(array('url' => URL::to('website/company/login-request', $request), 'method' => 'post',  'id' => 'company-login')) }}
                    @endif
                        <label for="companyName" class="error"></label>
                        {{ Form::text('companyName',null, array('class' => 'form-control', 'id' => 'companyName', 'placeholder' =>  trans('messages.form_company') )) }}
                        <label for="password" class="error"></label>
                        {{ Form::password('password', array('class' => 'form-control', 'id' => 'password', 'placeholder' =>  trans('messages.form_password') )) }}
                        {{ Form::submit(trans('messages.log_in'), array('class' => 'btn btn-primary'))  }}
                    {{ Form::close() }}   
                    </div>
                </div>
            </div>
        </div>
        {{ HTML::script('website_public/js/jquery.js') }}     
        {{ HTML::script('website_public/js/jquery.validate.min.js') }}
        <script>
        $(document).ready(function() {
            $("#company-login").validate({ 
                submitHandler: function(form) {
                    $('.error-login').text("");
                    var username = $("#company-login #companyName").val();
                    var password = $("#company-login #password").val();
                    $.ajax({
                        type     : "POST",
                        url      : "{{action('Website_CompanyController@login', $request)}}",
                        data     : 
                        {
                                company : username,
                                password : password
                        },
                        success : function(msg) {
                            if(msg == 'false')
                            {
                                $('.error-login').text("{{ trans('messages.wrong_data_login') }}");
                            }
                            else if(msg == 'true'){
                                form.submit();
                            }
                        }
                    });

                },
                rules: {
                        companyName: {
                                required: true,
//                                remote: {
//                                    url: "{{URL::to('website/company/login', $request)}}",
//                                    type: "post",
//                                    data: {
//                                        company: function() {
//                                            return $("#company-login #companyName").val();
//                                        },
//                                        password: function(){
//                                            return $("#company-login #password").val();
//                                        }
//                                    }
//                                },
                        },
                        password: {
                                required: true
                        }
                     
                },
                messages: {
                        companyName: {
                                required: "{{ trans('messages.required') }}",
                                remote: "{{ trans('messages.wrong_data_login') }}"
                        },
                        password: {
                                required: "{{ trans('messages.required') }}"
                        }
                },
                
            });
        });
        </script>
    </body>
</html>

