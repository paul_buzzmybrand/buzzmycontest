@extends('layouts.company')
@section('content')
@include('website.flash')
<div class="panel panel-default">
  <div class="panel-heading"><h3>{{ trans('messages.dashboard') }}</h3></div>
    <div class="panel-body">
      @if($allContests)
        <div class='row' style='margin-top:20px;'>
            <div class='col-md-4' style='padding-top:10px;'>
                {{ trans('messages.choose_event') }}
            </div>
            <div class='col-md-5'>
                <select name="select-content" class='form-control' id='select-content'>
                    <?php foreach($allContests as $data):?>
                        <option value='<?php echo $data->id; ?>'>
                            <?php echo $data->name; ?>
                            <?php if($data->status == Contest::STATUS_DRAFT): ?>
                                &nbsp;({{ trans('messages.draft') }})
                            <?php endif; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
                
            </div>
            <div class="col-md-3">
                <a class='btn btn-primary'  id='print'>{{ trans('messages.print') }}</a>
            </div>
        </div>
        <div class='row' style='margin-top:20px;'>
            <div class='col-md-12'>
                <div class="well" >
                    <h3>{{ trans('messages.analytics') }}</h3>
                    <div class='jumbotron' style="text-align:left;padding-top:20px;padding-bottom:20px;">
                        <div class="row">
                            <div class='col-sm-6'>
                                <b>{{ trans('messages.video_collected') }}</b>&nbsp;{{$contest['number_to_video_selected']}}</br>
                                <b>{{ trans('messages.user_engaged') }}</b>&nbsp;{{$contest['user_engaded']}}</br>
                                <b>{{ trans('messages.average_video') }}</b>&nbsp;{{$contest['awerage_views_video']}}</br>
                                <b>{{ trans('messages.total_views') }}</b>&nbsp;{{$contest['total_views']}}</br>
                            </div>
                            <div class='col-sm-6'>
                                <b>{{ trans('messages.induced_views') }}</b>&nbsp;{{$contest['induced_virality_views']}}</br>
                                <b>{{ trans('messages.campaign_score') }}</b>&nbsp;{{$contest['campaign_jmt_score']}}</br>
                                <b>{{ trans('messages.video_highest_score') }}</b>&nbsp;{{$contest['video_with_heighest_score']}}</br>
                            </div>
                        </div>
                    </div>
                    <h3>{{ trans('messages.engagement_details') }}</h3>
                    <div class='jumbotron' style="padding-top:20px;padding-bottom:20px;">
                        <div class="row" style="text-align:left;">
                            <div class='col-sm-6'>
                                <b>{{ trans('messages.facebook_likes') }}</b>&nbsp;{{$contest['facebook_likes']}}</br>
                                <b>{{ trans('messages.facebook_coments') }}</b>&nbsp;{{$contest['facebook_comments']}}</br>
                                <b>{{ trans('messages.facebook_shares') }}</b>&nbsp;{{$contest['facebook_shares']}}</br>
                                <b>{{ trans('messages.twitter_favouriters') }}</b>&nbsp;{{$contest['twitter_favourites']}}</br>
                                <b>{{ trans('messages.twitter_replies') }}</b>&nbsp;{{$contest['twitter_replies']}}</br>
                            </div>
                            <div class='col-sm-6'>
                                <b>{{ trans('messages.twitter_retweets') }}</b>&nbsp;{{$contest['twitter_reetwets']}}</br>
                                <b>{{ trans('messages.youtube_likes') }}</b>&nbsp;{{$contest['youtube_likes']}}</br>
                                <b>{{ trans('messages.youtube_comments') }}</b>&nbsp;{{$contest['youtube_commnets']}}</br>
                                <b>{{ trans('messages.youtube_shares') }}</b>&nbsp;{{$contest['youtube_shares']}}</br>
                            </div>
                        </div>
                    </div>

                </div>
                <div class='row'>
                    <div class='col-md-12'>
                        <img style='width:100%;' src='{{asset($graph)}}'>
                    </div>
                </div>
            </div>
        </div>
        @else
            <div class="alert alert-info" style='margin-top:40px;'>{{ trans('messages.no_results') }}</div>
        @endif
  </div>
</div>


<script>
    $('#select-content').on({
        'change' : function() {
            var value = $(this).find('option:selected').val();  
            var url = "{{URL::to('website/company/dashboard')}}";
            var request = url+"/"+value;
            window.location.href = request;   
        }
    });
</script>
<script>
    var id = '{{$id}}';
    if(id)
    {
        $('#select-content').val(id).attr("selected",true);
    }
</script>
<script>
    
        $( "body" ).on('click', '#print',function(event) {
            event.preventDefault();
            var id = '{{$id}}';
            var url = "{{URL::to('website/company/print')}}";
            var request = url+"/"+id;
     
            window.open(request);
        });
    </script>
@stop
