<div id="login" class="white-popup mfp-with-anim mfp-hide">
   <div class='popup-title'>{{ trans('messages.log_in') }}</div>
   <div style='color:#a94442;text-align:center;' class='error-login'></div>
   <div class="popup-content">
   {{ Form::open(array('url' => URL::to('website/company/dashboard'), 'method' => 'post',  'id' => 'company-login')) }}
        <label for="companyName" class="error"></label>
        {{ Form::text('companyName',null, array('class' => 'form-control', 'id' => 'companyName', 'placeholder' =>  trans('messages.form_company') )) }}
        <label for="password" class="error"></label>
        {{ Form::password('password', array('class' => 'form-control', 'id' => 'password', 'placeholder' =>  trans('messages.form_password') )) }}
        {{ Form::submit(trans('messages.log_in'), array('class' => 'btn btn-primary'))  }}
   {{ Form::close() }}    
   </div>
</div>
<div id="register" class="white-popup mfp-with-anim mfp-hide">
   <div class='popup-title'>{{ trans('messages.sign_up') }}</div>
   <div class="popup-content">
   {{ Form::open(array('id' => 'company-register', 'url' => 'website/company/register', 'method' => 'post')) }}
       <label for="companyName" class="error"></label>
       {{ Form::text('companyName', null, array('class' => 'form-control', 'id' => 'companyName', 'placeholder' => trans('messages.form_company') )) }}
       <label for="contactName" class="error"></label>
       {{ Form::text('contactName', null, array('class' => 'form-control', 'id' => 'contactName', 'placeholder' => trans('messages.form_contact') )) }}
       <label for="address" class="error"></label>
       {{ Form::text('address', null, array('class' => 'form-control', 'id' => 'address', 'placeholder' => trans('messages.form_address') )) }}
       <label for="zipCode" class="error"></label>
       {{ Form::text('zipCode', null, array('class' => 'form-control', 'id' => 'zipCode', 'placeholder' => trans('messages.form_zip') )) }}
       <label for="city" class="error"></label>
       {{ Form::text('city', null, array('class' => 'form-control', 'id' => 'city', 'placeholder' => trans('messages.form_city') )) }}
       <label for="email" class="error"></label>
       {{ Form::text('email', null, array('class' => 'form-control', 'id' => 'email', 'placeholder' => trans('messages.form_email') )) }}
       <label for="telephone" class="error"></label>
       {{ Form::text('telephone', null, array('class' => 'form-control', 'id' => 'telephone', 'placeholder' => trans('messages.form_phone') )) }}
       <label for="fax" class="error"></label>
       {{ Form::text('fax', null, array('class' => 'form-control', 'id' => 'fax', 'placeholder' => trans('messages.form_fax') )) }}
       <label for="website" class="error"></label>
       {{ Form::text('website', null, array('class' => 'form-control', 'id' => 'website', 'placeholder' => trans('messages.form_website') )) }}
       <label for="password" class="error"></label>
       {{ Form::password('password', array('class' => 'form-control', 'id' => 'password', 'placeholder' => trans('messages.form_password') )) }}
       <label for="confirmPassword" class="error"></label>
       {{ Form::password('confirmPassword', array('class' => 'form-control', 'id' => 'confirmPassword', 'placeholder' => trans('messages.form_password_r') )) }}
       <p>
           {{ trans('messages.form_accept') }}</br>
           <a href="{{action('Website_CompanyController@wordpress', 'terms-of-service')}}">{{ trans('messages.terms_service') }}&nbsp;</a>&
           <a href="{{action('Website_CompanyController@wordpress', 'privacy-police')}}">{{ trans('messages.privacy_police') }}</a>
       </p>
   {{ Form::submit(trans('messages.sign_up'), array('class' => 'btn btn-primary'))  }}   
   {{ Form::close() }}   
   </div>
<div id="delete-account" class="white-popup mfp-with-anim mfp-hide">
   <div class='popup-title'>{{ trans('messages.delete_account_desc') }}</div>
   <div class="popup-content">
       <a href="{{action('Website_CompanyController@deleteAccount')}}" class='btn btn-primary'>{{ trans('messages.delete') }}</a>
   </div>
</div>
    
</div>
<script>
$(document).ready(function() {
    $("#company-register").validate({
        rules: {
                companyName: {
                        required: true,
                        minlength: 2,
                        maxlength: 50,
                        remote: {
                            url: "{{URL::to('website/company/check-username')}}",
                            type: "post",
                            data: {
                                username: function() {
                                    return $("#company-register #companyName").val();
                                }
                            }
                        }
                },
                contactName: {
                        required: true,
                        minlength: 2,
                        maxlength: 50,
                },
                address: {
                        required: true,
                        maxlength: 40,
                },
                zipCode: {
                        required: true,
                        maxlength: 30,
                },
                city: {
                        required: true,
                        maxlength: 50,
                },
                email: {
                        required: true,
                        email: true,
                        maxlength: 60,
                        remote: {
                            url: "{{URL::to('website/company/check-email')}}",
                            type: "post",
                            data: {
                                email: function() {
                                    return $("#company-register #email").val();
                                }
                            }
                        }
                },
                telephone: {
                        required: true,
                        maxlength: 20,
                },
                fax: {
                        required: true,
                        maxlength: 20,
                },
                website: {
                        required: true,
                        maxlength: 50,
                },
                password: {
                        required: true,
                        minlength: 4,
                        maxlength: 12,
                },
                confirmPassword: {
                        required: true,
                        equalTo: "#password"
                },
        },
        messages: {
                companyName: {
                        required: "{{ trans('messages.required') }}",
                        minlength: "{{ trans('messages.min_length', array('number' => '2')) }}",
                        maxlength: "{{ trans('messages.max_length', array('number' => '50')) }}",
                        remote: "{{ trans('messages.comapny_name_registered') }}"
                },
                contactName: {
                        required: "{{ trans('messages.required') }}",
                        minlength: "{{ trans('messages.min_length', array('number' => '2')) }}",
                        maxlength: "{{ trans('messages.max_length', array('number' => '50')) }}"
                        
                },
                address: {
                        required: "{{ trans('messages.required') }}",
                        maxlength: "{{ trans('messages.max_length', array('number' => '40')) }}"
                },
                zipCode: {
                        required: "{{ trans('messages.required') }}",
                        maxlength: "{{ trans('messages.max_length', array('number' => '30')) }}"
                },
                city: {
                        required: "{{ trans('messages.required') }}",
                        maxlength: "{{ trans('messages.max_length', array('number' => '50')) }}"
                },
                email: {
                        required: "{{ trans('messages.required') }}",
                        maxlength: "{{ trans('messages.max_length', array('number' => '60')) }}",
                        remote: "{{ trans('messages.email_registered') }}"
                },
                telephone: {
                        required: "{{ trans('messages.required') }}",
                        maxlength: "{{ trans('messages.max_length', array('number' => '20')) }}"
                },
                fax: {
                        required: "{{ trans('messages.required') }}",
                        maxlength: "{{ trans('messages.max_length', array('number' => '20')) }}"                     
                },
                website: {
                        required: "{{ trans('messages.required') }}",
                        maxlength: "{{ trans('messages.max_length', array('number' => '50')) }}"
                },
                password: {
                        required: "{{ trans('messages.required') }}",
                        minlength: "{{ trans('messages.min_length', array('number' => '4')) }}",
                        maxlength: "{{ trans('messages.max_length', array('number' => '12')) }}"
                    
                },
                confirmPassword: {
                        required: "{{ trans('messages.required') }}",
                },
        }
    });
     $("#company-login").validate({
        submitHandler: function(form) {
           $('.error-login').text("");
           var username = $("#company-login #companyName").val();
           var password = $("#company-login #password").val();
           $.ajax({
               type     : "POST",
               url      : "{{action('Website_CompanyController@login', 'dashboard')}}",
               data     : 
               {
                       company : username,
                       password : password
               },
               success : function(msg) {
                   console.log(msg);
                   if(msg == 'false')
                   {
                       $('.error-login').text("{{ trans('messages.wrong_data_login') }}");
                   }
                   else if(msg == 'true'){
                       form.submit();
                   }
               }
           });

        },
        rules: {
                companyName: {
                        required: true,
//                        remote: {
//                            url: "{{URL::to('website/company/login', 'dashboard')}}",
//                            type: "post",
//                            data: {
//                                company: function() {
//                                    return $("#company-login #companyName").val();
//                                },
//                                password: function() {
//                                    return $("#company-login #password").val();
//                                }
//                            }
//                        },
                },
                password: {
                        required: true
                }

        },
        messages: {
                companyName: {
                        required: "{{ trans('messages.required') }}",
                        remote: "{{ trans('messages.wrong_data_login') }}"
                },
                password: {
                        required: "{{ trans('messages.required') }}"
                }
        },

    });
});
	
</script>