@extends('layouts.public')

@section('metadata')
	<title>@lang('title.awards')</title>
	<meta name="description" content="{{trans('title.awards_d')}}">
@stop

@section('content')
<div id="static-head" class="container">
    <h1 class="centered">@lang('landing.head_cta', ['before' => '<span class="yellow">', 'after' => '</span>'])</h1>
</div>

<div class="content-block">
	<div class="container">
		<h1 class="centered">Awards</h1>
		<!--<h1>I am a standard title</h1>
		<p>This is a simple paragraph.</p>-->
		<div class="row">
		  <div class="col-sm-12 col-md-6">
			<div class="thumbnail">
			  <img src="{{ asset('website_public/home_page/img/links/rds_startuplab.jpg') }}" alt="RDS Startup Lab">
			  <div class="caption">
				<h3 class="centered">Winner - RDS Startup Lab</h3>				
			  </div>
			</div>			
		  </div>
		  <div class="col-sm-12 col-md-6">
			<div class="thumbnail">
			  <img src="{{ asset('website_public/home_page/img/links/fulbright.jpg') }}" alt="RDS Startup Lab">
			  <div class="caption">
				<h3 class="centered">Winner - Best Scolarship</h3>				
			  </div>
			</div>			
		  </div>
		  <div class="col-sm-12 col-md-6">
			<div class="thumbnail">
			  <img src="{{ asset('website_public/home_page/img/links/get.jpg') }}" alt="RDS Startup Lab">
			  <div class="caption">
				<h3 class="centered">Winner - Creative Business Cup Thailand 2014</h3>				
			  </div>
			</div>			
		  </div>
		  <div class="col-sm-12 col-md-6">
			<div class="thumbnail">
			  <img src="{{ asset('website_public/home_page/img/links/startupsg.jpg') }}" alt="RDS Startup Lab">
			  <div class="caption">
				<h3 class="centered">Winner - NUS Startup Competition</h3>				
			  </div>
			</div>			
		  </div>
		  <div class="col-sm-12 col-md-6">
			<div class="thumbnail">
			  <img src="{{ asset('website_public/home_page/img/links/startcup.jpg') }}" alt="RDS Startup Lab">
			  <div class="caption">
				<h3 class="centered">Winner - StartCup Puglia 2013</h3>				
			  </div>
			</div>			
		  </div>
		  <div class="col-sm-12 col-md-6">
			<div class="thumbnail">
			  <img src="{{ asset('website_public/home_page/img/links/sitf.jpg') }}" alt="RDS Startup Lab">
			  <div class="caption">
				<h3 class="centered">Finalist – SITF Awards 2013</h3>				
			  </div>
			</div>			
		  </div>
		  <div class="col-sm-12 col-md-6">
			<div class="thumbnail">
			  <img src="{{ asset('website_public/home_page/img/links/innovami.jpg') }}" alt="RDS Startup Lab">
			  <div class="caption">
				<h3 class="centered">Winner - Innovami Start-up 2013</h3>				
			  </div>
			</div>			
		  </div>
		</div>
		<hr />
	</div>
</div>
@stop