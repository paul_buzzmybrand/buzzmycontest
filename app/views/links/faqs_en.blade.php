<div>
    <p>
        <strong>What is BuzzMyBrand?</strong></br>
        BuzzMyBrand is a social network management platform. Specifically, with BuzzMyBrand you can launch integrated, simultaneous photo or video contests on the largest and most popular social networks like Facebook, Twitter, YouTube and Instagram.
    </p>
    <p>
        <strong>Can I prevent someone from posting inappropriate content on my social pages?</strong></br>
        Of course you can. BuzzMyBrand provides you with a personalized dashboard where you can set and moderate each contest. Among the various functions we provide is the ability to filter content that you feel is inappropriate, preventing it from appearing on your social media pages.
    </p>
    <p>
        <strong>What is a watermark?</strong></br>
        Watermarking is, technically, the inclusion of information within a media file, which can subsequently be detected or extracted to get information about the file's origin. Basically, it is a stamp placed over the photos or videos that your users will create during the BuzzMyBrand contest; this stamp shows your company logo, which will give you huge visibility on all social media during a campaign.
    </p>
    <p>
        <strong>How do you decide who wins contests?</strong></br>
        The Internet will decide! To win a contest launched by companies using BuzzMyBrand, users must receive the highest number of likes, shares and comments on major social media like Facebook, Twitter, YouTube and Instagram. Software will evaluate the success of a photo or video on any social media and quantify it, ranking content in real time for everyone to view. By doing so, users will become your brand's main ambassadors.
    </p>
    <p>
        <strong>How do I contact the contest winner?</strong></br>
        BuzzMyBrand provides you with a standard email template (which you can then customize) that will be sent automatically to the winner or winners at the end of each contest.
    </p>
    <p>
        <strong>What do Facebook guidelines say about contests?</strong></br>
        Find out more in our Compliance section.
    </p>
    <p>
        <strong>Who writes the legal terms and conditions for my contest?</strong></br>
        You have two options: you can use the standard terms and conditions created for you by BuzzMyBrand (you only have to load them when creating a contest) or you can prepare and upload your own. In any case, a user who wants to participate in your contest will be required to read and approve them.
    </p>
    <p>
        <strong>Why should I use BuzzMyBrand to promote my business?</strong></br>
        Because you can no longer ignore the importance of a strong social network presence, and BuzzMyBrand offers one of the best solutions to achieve this: engage and reward your community, get your brand known online and turn your users into ambassadors.
    </p>
    <p>
        <strong>Do you have any APIs I can use to integrate your services with my platform?</strong></br>
        Not yet, but we are working to release them soon. You'll want to subscribe to our newsletter to stay informed about future updates.
    </p>
</div>