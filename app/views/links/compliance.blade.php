@extends('layouts.public')

@section('content')
<div id="static-head" class="container">
    <h1 class="centered">@lang('landing.head_cta', ['before' => '<span class="yellow">', 'after' => '</span>'])</h1>
</div>

<div class="content-block">
	<div class="container">
		<h1 class="centered">@lang('links.compliance_title')</h1>
		

		@if (Session::get('my.locale') == 'it')
		@include('links.compliance_it')
		@else
		@include('links.compliance_en')
		@endif
		<hr />
	</div>
</div>
@stop