@extends('layouts.public')

@section('content')
<div id="static-head" class="container">
    <h1 class="centered">@lang('landing.head_cta', ['before' => '<span class="yellow">', 'after' => '</span>'])</h1>
</div>

<div class="content-block">
	<div class="container">
		<h1 class="centered">I AM A <span class="yellow fat">COOL</span> title</h1>
		<h1>I am a standard title</h1>
		<p>This is a simple paragraph.</p>
		<hr />
	</div>
</div>
@stop