@extends('layouts.public')

@section('metadata')
	<title>@lang('title.social_buzz')</title>
	<meta name="description" content="{{trans('title.social_buzz_d')}}">
@stop

@section('content')
<div id="static-head" class="container">
    <h1 class="centered">@lang('landing.head_cta', ['before' => '<span class="yellow">', 'after' => '</span>'])</h1>
</div>

<div class="content-block">
	<div class="container">
		<h1 class="centered">@lang('links.social_buzz_title')</h1>
		<!--<h1>I am a standard title</h1>-->
		<p style="line-height: 2.4em;">@lang('links.social_buzz_description', ['before' => '<strong>', 'after' => '</strong>', 'br' => '</br>'])</p>
		<hr />
	</div>
</div>
@stop