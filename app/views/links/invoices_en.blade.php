<div>
    <p>
        BuzzMyBrand charges the client once he/she launch a contest through our website www.buzzmybrand.co and www.buzzmybrand.it. All services rendered are non-refundable. Once a customer launch a contest and provides billing information, BuzzMyBrand will calculate the due amount.
    </p>
    <p>
        The payment shall be performer by credit card or Paypal® system. The related credit card’s or Paypal’s accounts and references are stored in our server. BuzzMyBrand shall store one credit card or paypal account per time. The client must provide BuzzMyBrand with a valid credit card or Paypal® accountas a condition to use our service service. BuzzMyBrand reserves the right to change or modify its fee structure and introduce subscription plan in the future.
    </p>
    <p>
        <u>Important: No refunds or credits for partial usage of our service will be refunded to a customer upon cancellation.</u>
    </p>
    <p>
        The due invoice is issued simultaneously with the payment performer by credit card or Paypall ®. The submitted data to fill the invoice shall be equal to the cost of the contest launched and include all the company’s information pursuant to italian and european appliable laws and rules. An invoice concurs to each contest.
    </p>
    <p>
        All fees are exclusive of all taxes, levies, or duties imposed by taxing authorities, and the customer is responsible for payments of all such taxes, levies, or duties. Any currency exchange settlements are based on the customer's agreement with the payment method provider. To be clear: these taxes and charges are the customer's responsibility.
    </p>
    <p>
        The contests launched but not already beginned may be cancelled by the client before the beginning of the said contest. The client shall cancel its credit card’s datas, whenever he/she deem it necessary, directly from the related dashboard section. A contest that has already beginned cannot be refunded.
    </p>
    <p>
        Fraud: Without limiting any other remedies, BuzzMyBrand may suspend or terminate your account if we suspect that you (by conviction, settlement, insurance or escrow investigation, or otherwise) have engaged in fraudulent activity in connection with our website.
    </p>
</div>