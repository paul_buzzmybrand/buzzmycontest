@extends('layouts.public')

@section('metadata')
    <title>@lang('title.invoices')</title>
    <meta name="description" content="{{trans('title.invoices_d')}}">
@stop

@section('content')
    <div id="static-head" class="container">
        <h1 class="centered">@lang('landing.head_cta', ['before' => '<span class="yellow">', 'after' => '</span>'])</h1>
    </div>

    <div class="content-block">
        <div class="container">
            <h1 class="centered">@lang('links.invoices_title')</h1>
            @if (Session::get('my.locale') == 'it')
                @include('links.invoices_it')
            @else
                @include('links.invoices_en')
            @endif

            <hr />
        </div>
    </div>
@stop