<div>
<p>
Effective: July 2016
</p>
<p>
Thank you for your visit at www.BuzzMyBrand.com (“BuzzMyBrand” or “we”), we share your concerns about privacy and hope this BuzzMyBrand.com Privacy Policy (the “Privacy Policy”) will help you understand our privacy policies and the rights you have to use or access of www.buzzmybrand.com, www.buzzmybrand.it and www.buzzmybrand.co, including any sites or applications hosted on the BuzzMyBrand.com domain which may appear as iframes in third party sites (collectively, the “Site”), any service available on the Site, as a visitor, user or contest organizer, sponsor or publisher. If you are accepting this Privacy Policy on behalf of a company or other legal entity, you represent that you have the authority to bind such entity to this Privacy Policy, in which case the terms "you" or "your" shall refer to such entity. This Privacy Policy covers how BuzzMyBrand treats your personal information that we collect or receive. For the purposes of this Privacy Policy, “personal information” is personally identifiable information about you such as your name, address, email address, and other information which is not otherwise publicly available. This Privacy Policy is subject to the BuzzMyBrand.com Terms of Service (the “Terms of Service”). Your use or access of the Site and any personal information you provide on the Site remains subject to the terms of this Privacy Policy and the Terms of Service.
</p>
<p>
BuzzMyBrand may update this Privacy Policy at any time, and without notification to you, and you should review this Privacy Policy from time to time by accessing the Site. Your continued use of the Site shall be deemed irrevocable acceptance of any such revisions.
</p>
</br>
<h4>Overview</h4>
<p>
This Privacy Policy is subject to applicable government laws of each jurisdiction in which you use or access the Site.
</br>
BuzzMyBrand does not send spam, or support the activities of spammers.
</br>
You have access to edit your information at any time on request.
</br>
We are committed to protecting the privacy of personal information.
</br>
When you register for a BuzzMyBrand account, including registering or logging in with any third party authentication service, and become a registered user of the Site, we ask for personal information that will be used to activate your account, provide you with access to the Site, communicate with you about the status of your account, and for other purposes set out in this Privacy Policy.
</br>
</p>

<p>
<h4>Information Collection</h4>
By providing personal information to us and by using or accessing the Site, you voluntarily consent to the collection, use and disclosure of personal information as specified in this Privacy Policy. Without limiting the foregoing, we may on occasion ask you to consent when we collect, use, or disclose your personal information in specific circumstances. Sometimes your consent will be implied through your conduct with us if the purpose of the collection, use or disclosure is obvious and you voluntarily provide the information.
</br>
We do not knowingly provide access to the Site to, and will not knowingly collect personal information from, anyone under the age of 13 unless such user has parental or legal guardian consent.
</p>

<p>
<h4>Registration and BuzzMyBrand Account Information</h4>
You do not have to be a registered user to access the Site or view content on the Site but if you would like to upload content, participate in any online contest, games, and sweepstakes (each a “Contest”) or promotions, marketing activities or other activities or events (each an “Activity”) enabled through the Site, ensure any votes or ratings you have cast in an Contest or Activity are recorded, post comments or flag content or carry out other activities which require you to be registered user, you must complete a basic registration form and create a BuzzMyBrand account or login using a third party authentication. We do ask for some personal information when you create an account on BuzzMyBrand, including but not limited to your name, email address and password, which is used to protect your BuzzMyBrand account from unauthorized access and to provide the Site to you in accordance with this Privacy Policy (the “Account Information”).
</br>
<!-- riprendere da qui -->
</p>

<p>
<h4>Variations and updates of the BUZZMYBRAND Privacy Policy</h4>
BUZZMYBRAND may modify or simply update this site Privacy Policy, in part or completely, even owing to variations of the laws and regulations that govern this matter and protect your rights. We therefore invite you to visit this section often to read the most recent and updated version of this Privacy Policy.If you have questions or complaints you can contact us through by e-mail at <a href="mailto:info@buzzmybrand.it">info@buzzmybrand.it</a>
</p>
</div>