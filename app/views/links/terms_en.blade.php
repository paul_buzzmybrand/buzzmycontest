<div>
<p>
These Terms were last modified May 1st 2015</br>
</p>
<p>
Welcome to BuzzMyBrand and thank you for taking the time to read through this most wordy but important document. This BuzzMyBrand.co Terms of Service (the “Terms of Service”) creates a binding legal agreement between BuzzMyBrand S.r.l.s. (“BuzzMyBrand”) and you. If you are accepting these Terms of Service on behalf of a company or other legal entity, you represent that you have the authority to bind such entity to these Terms of Service, in which case the terms “you” or “your” shall refer to such entity. By using or accessing this website (the “Site”) and any service available on the Site, you agree that you have read and agree to the terms of these Terms of Service and the BuzzMyBrand.com <a href="{{asset('/privacy')}}">Privacy Policy</a> (the “Privacy Policy”). These Terms of Service also applies to any BuzzMyBrand software downloaded on your computer.</p>
</br>
<p>
<h4>1. About Us</h4>
BuzzMyBrand provides software, Facebook enabled applications, websites and other online services to enable its business clients (Sponsors) to operate contests, campaigns, websites, website plugins, Facebook page apps,  collectively called Promotions or Services. BuzzMyBrand Contest Software and Services are designed to allow Sponsors to gather personally identifiable information and content from Users who choose to enter a specific Contest. </p>
</br>
<p>
<h4>2. General terms</h4>
2.1 You agree that you will not use the Service if you are not of legal age to form a binding contract with BuzzMyBrand or you are a person who is legally prohibited from receiving or using the Service under the laws of the country in which you are resident or from which you access or use the Service.
2.2 You agree you will not distribute any part of the Service or the Content without prior written authorization from BuzzMyBrand.
2.3 You acknowledge that you shall not create, submit or share any  any data, text, files, information, usernames, images, graphics, photos, profiles, audio and video clips, sounds, musical works, works of authorship, applications, links and other content that in any respect:
a) is in breach of any law, statute, regulation or byelaw of any applicable jurisdiction; 
b) is fraudulent, criminal or unlawful; 
c) may be obscene, indecent, pornographic, vulgar, profane, racist, sexist, discriminatory, offensive, harmful, harassing, threatening, abusive, hateful, menacing or defamatory or harmful to children in any way; 
d) may infringe or breach the copyright or any intellectual property rights (including without limitation copyright, trade mark rights and broadcasting rights) or privacy or other rights of us or any third party; 
e) purposely affect, damage or injure in any way whatsoever a brand or a company reputation by means of false, misleading and defamatory content;
f) may be contrary to our interests; 
g) is contrary to any specific rule or requirement that we stipulate on the Service in relation to a particular part of the Service or the Service generally; or
h) involves your use, delivery or transmission of any viruses, unsolicited emails, trojan horses, trap doors, back doors, easter eggs, worms, time bombs, cancelbots or computer programming routines that are intended to damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or personal information.
</p>
</br>
<p>
<h4>3. Intellectual Property Rights</h4>
3.1 BuzzMyBrand names and logos and all related names, design marks and slogans are the trade marks or service marks of usand may not be copied, imitated or used, in whole or in part, without the prior written permission of BuzzMyBrand.
3.2 We are the owner or the licensee of all intellectual property rights in the Service, and in the material published on it. Those works are protected by copyright laws and treaties around the world. All such rights are reserved. 
3.3 You may print off one copy and may download extracts of any content or page(s) from the Service for your personal reference. You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text.
3.4 You must not use any part of the materials on the Service for commercial purposes without obtaining a license to do so from us.You may not use any metatags or any other "hidden text" utilizing "Join me there” or “BuzzMyBrand” or any other name, trademark or product or service name of BuzzMyBrand without our prior written permission.
3.5 Reference to any products, services, processes or other information, by trade name, trademark, manufacturer, supplier or otherwise does not constitute or imply endorsement, sponsorship or recommendation thereof by us.
3.6 You agree that BuzzMyBrand is not responsible for any violations of any third party intellectual property rights in any Content that you submit to BuzzMyBrand.
3.7 You agree to pay all royalties, fees and any other monies owing to any person by reason of Content displayed or posted by you to www.BuzzMyBrand.co or www.BuzzMyBrand.it. Except for your Content, you may not copy, modify, publish, broadcast, transmit, distribute, perform, display or sell any Content appearing on www.BuzzMyBrand.co or www.BuzzMyBrand.it. 
</p>
</br>
<p>
<h4>4. User(s) Submission(s)</h4>
4.1 The Service may allow you as a BuzzMyBrand account holder to submit Content. With “Content” we mean any text, audio, video, software, scripts, graphics, digital images, files, music, interactive features, comments and other materials or media you submit using the Service. You acknowledge  that whether or not Content is published, we do not guarantee any confidentiality with its respect. 
4.2 As you publish a Content utilizing the Service, that Content becomes public and  you retain all of your ownership rights in your Content, but you are required to grant limited license rights to BuzzMyBrand and other users of the Service as described in the next paragraph. Such Content may also be generally available to the public at large from search engines over internet and  you explicitly declare that you have no expectation to confidentiality for any Content available through the Service.
4.3 You agree that you are solely responsible for any User Content that you Publish and for the consequences of posting or publishing it. BuzzMyBrand expressly disclaims any and all liability in connection with such Content.We may, but are not required to monitor or control the Content posted via the Service and we cannot take responsibility for such Content. 
4.4 You represent and warrant that you own and will continue to own during your use of the Service the necessary licenses, rights, consents and permissions to any Content you Publish on the Service.
4.5 You agree that you will not post or upload any Content which contains material which it is unlawful for you to possess in the country in which you are resident or contain third party copyrighted material, unless you have permission from the rightful owner of the material. We reserve the right to remove any infringing user Content and terminate a user's access for uploading Content which is in violation of these provisions, as soon as is commercially reasonable, without any prior notice and at our sole discretion. 
4.6 BuzzMyBrand expressly disclaims any liability that may derive from the user Content and abides by all laws protecting copyrights, trademarks and other intellectual property. BuzzMyBrand will cooperate with that third party, any law enforcement agency, court of law or applicable regulatory body with the investigation into that infringement. This means your personal information, including your name, address, email, IP address and other identifiable information, may be distributed for the sole purpose of protecting third parties’ intellectual property.
4.7 In the case of violation of the Terms of Service, BuzzMyBrand reserve the right to limit your access to the Service or terminate your account. BuzzMyBrand may also at its sole discretion limit access to the Service and/or terminate the accounts of any users who infringe any intellectual property rights of others, whether or not there is any repeat infringement.
</p>
</br>
<p>
<h4>5. Account termination</h4>
Notwithstanding any provision of these Terms, BuzzMyBrand reserves the right, without notice and in its sole discretion, without any notice or liability to you, to (a) terminate your right to use or access our website(s), or any portion thereof; (b) block or prevent your future access to and use of all or any portion of the website(s); (c) change, suspend or discontinue any aspect of the website(s); and (d) impose limits on the website(s).</p>
</br>
<p>
<h4>6. Third Party Content</h4>
6.1 The inclusion of any third party content on our website(s) does not imply BuzzMyBrand’s affiliation to the provider of such third party content or its endorsement of such third party content. Because BuzzMyBrand does not control third party content, you agree that BuzzMyBrand is not responsible for any such third party content, including the accuracy, integrity, quality, legality, usefulness, safety or intellectual property rights of or relating to such third party content. BuzzMyBrand reserves the right, but shall have no obligation, to pre-screen, filter, remove, refuse to accept, post, display or transmit any third party content (including on any social networking website) in whole or part at any time for any reason with or without notice and with no liability of any kind. 
6.2 You are solely responsible (and assume all associated liability and risk) for determining whether or not such third party content is appropriate or acceptable to you.
6.3 Access and use of third party sites, including the information, material, products, and services on third party sites or available through third party sites, is solely at your own risk.
</p>
</br>
<p>
<h4>7. Payments</h4>
7.1 When opening your BuzzMyBrand account, a valid credit card will be required for billing purposes. BuzzMyBrand uses pricing model mixing an upfront fee (according to the selected schedule) and a performance based model that allows BuzzMyBrand to bill your account according to the metrycs related to your campaign laucnhed through BuzzMyBrand. You hereby allow BuzzMyBrand to store such payment information and agree to the following policies and procedures for payment of fees related to BuzzMyBrand. 
7.2 All payments made to BuzzMyBrand in connection with your BuzzMyBrand account are non-refundable, and BuzzMyBrand does not offer, and is not required to provide, any refunds or credits. There is no circumstance in which you will be entitled to, or BuzzMyBrand is required to provide, a refund or credit. 
</p>
</br>
<p>
<h4>8. Indemnification</h4>
Each user shall, and hereby agrees to, indemnify and hold harmless, BuzzMyBrand, and its officers, directors, shareholders, employees and affiliates, from any and all claims, losses, damages, liabilities and any other costs and expenses (including attorneys’ fees), arising from or related to such user’s (i) failure to comply with these Terms and Conditions, (ii) submission or posting of content using BuzzMyBrand, or (iii) any use of, access to or other activity engaged in, on or through BuzzMyBrand.</p>
</br>
<p>
<h4>9. Severability</h4>
If any of these terms should be determined to be illegal, invalid or otherwise unenforceable by reason of the laws of any state or country in which these terms are intended to be effective, then to the extent and within the jurisdiction which that term is illegal, invalid or unenforceable, it shall be severed and deleted and the remaining Terms of Use shall survive, remain in full force and effect and continue to be binding and enforceable.</p>
</br>
<p>
<h4>10. Law and Jurisdiction</h4>
These Terms of Use and your use of the Website shall be governed by and construed in accordance with the law of the Italy. You submit to the exclusive jurisdiction of the courts of Milan to settle any dispute which may arise under these Terms of Service.</p>
</br>
<p>
<h4>11.  Severability</h4>
If any provision of these Terms shall be unlawful, void, or for any reason unenforceable, then that provision shall be deemed severable from these Terms and shall not affect the validity and enforceability of any remaining provisions.</p>
</br>
<p>
<h4>12. Limitation of Liability</h4>
YOU EXPRESSLY UNDERSTAND AND AGREE THAT BUZZMYBRAND WILL NOT BE LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, EXEMPLARY DAMAGES, OR DAMAGES FOR LOSS OF PROFITS INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES.UNDER NO CIRCUMSTANCES, AND UNDER NO LEGAL THEORY, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE, SHALL BUZZMYBRAND OR ITS AFFILIATES, CONTRACTORS, EMPLOYEES, AGENTS, OR THIRD PARTY PARTNERS OR SUPPLIERS, BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, CONSEQUENTIAL, OR EXEMPLARY DAMAGES (INCLUDING WITHOUT LIMITATION, LOSS OF PROFITS, DATA OR USE OR COST OF COVER) ARISING OUT OF OR RELATING TO THESE TERMS OR THAT RESULT FROM YOUR USE OR THE INABILITY TO USE THE BUZZMYBRAND MATERIALS OR BUZZMYBRAND SERVICE ITSELF, OR ANY OTHER INTERACTIONS WITH BUZZMYBRAND. CERTAIN STATES AND/OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES OR LIMITATION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE EXCLUSIONS SET FORTH ABOVE MAY NOT APPLY TO YOU. BUZZMYBRAND IS NOT RESPONSIBLE FOR TYPOGRAPHICAL ERRORS OR OMISSIONS RELATING TO PRICING, TEXT OR PHOTOGRAPHY.
If you have any question regarding the use of the Service, please send us an email at <a href="mailto:info@buzzmybrand.it">info@buzzmybrand.it</a> 
</p>
</br>
</div>