<div>
    <p>
        <strong>Che cos’è BuzzMyBrand?</strong></br>
        BuzzMyBrand è un portale che offre tecnologie gestionali per social network. Nello specifico con BuzzMyBrand puoi lanciare foto o video contest in maniera integrata e simultanea sui maggiori e più diffusi social network ovvero Facebook, Twitter, YouTube ed Instagram.
    </p>
    <p>
        <strong>Posso evitare che qualcuno posti sulle mie pagine dei contenuti che io ritengo inopportuni?</strong></br>
        Ovviamente puoi farlo. BuzzMyBrand ti fornisce una dashboard personalizzata con la quale potrai impostare ogni contest e regolarne lo svolgimento. Fra le varie funzioni che ti diamo vi è quella che ti consente di filtrare i contenuti che riterrai non opportuni, impedendo a questi di comparire sulle pagine dei tuoi canali social.
    </p>
    <p>
        <strong>Cos’è un watermark?</strong></br>
        Il  watermarking è, tecnicamente, l’inserimento di informazioni all'interno di un file multimediale o di altro genere, che può essere successivamente rilevato o estratto per trarre informazioni sulla sua origine e provenienza. In sostanza è un timbro che viene messo sulle foto o sui video che verrano creati dai tuoi utenti nel corso del contest che lanci con BuzzMyBrand; questo timbro consiste nel logo della tua azienda e ciò ti consentirà di avere un’enorme visibilità su tutti i social media nel corso della campagna.
    </p>
    <p>
        <strong>Come si decide chi è il vincitore del contest?</strong></br>
        Sarà le rete a deciderlo! Per vincere i contest lanciati dalle aziende con BuzzMyBrand gli utenti devono ricevere il maggior numero possibile di like, condivisioni e commenti sui maggiori social media come Facebook, Twitter, YouTube e Instagram. Un software valuterà il successo di una foto o di un video su ogni social media e fornirà un numero che ne quantifica il valore, andando a creare una classifica in tempo reale visibile a tutti. E quindi gli utenti diventeranno i principali sponsor del tuo brand.
    </p>
    <p>
        <strong>Come informo il vincitore del contest?</strong></br>
        BuzzMyBrand ti fornisce una email standard (che potrai comunque personalizzare) che verrà inviata in automatico al vincitore o ai vincitori del contest al termine dello stesso.
    </p>
    <p>
        <strong>Cosa dicono le guidelines di Facebook in tema di contest?</strong></br>
        Trovi tutto nella nostra sezione note legali.
    </p>
    <p>
        <strong>Chi scrive le condizioni legali per il mio contest? </strong></br>
        Hai due opzioni: puoi utilizzare le condizioni standard che BuzzMyBrand ha creato per te (devi solo caricarle al momento della creazione del contest oppure puoi redigere personalmente le condizioni per il contest e caricarle. In ogni caso l’utente che vorrà partecipare al tuo contest sarà tenuto a prenderne visione ed approvarle per poterlo fare.
    </p>
    <p>
        <strong>Perché dovrei usare BuzzMyBrand per promuovere la mia azienda?</strong></br>
        Perché non puoi più prescindere da una presenza effettiva sui social network e BuzzMyBrand ti offre soluzione migliore per farlo: coinvolgere la tua community, premiarla e far girare il tuo brand nella rete facendo dei tuoi utenti i tuoi ambasciatori.
    </p>
    <p>
        <strong>Avete delle API con cui posso integrare i vostri servizi nella mia piattaforma?</strong></br>
        Non ancora ma ci stiamo lavorando e presto saranno rilasciate. Ti consigliamo di iscriverti alla nostra newsletter per rimanere informato anche su questo punto.
    </p>
</div>