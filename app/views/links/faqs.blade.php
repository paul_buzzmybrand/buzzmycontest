@extends('layouts.public')

@section('metadata')
	<title>@lang('title.faq')</title>
	<meta name="description" content="{{trans('title.faq_d')}}">
@stop

@section('content')
<div id="static-head" class="container">
    <h1 class="centered">@lang('landing.head_cta', ['before' => '<span class="yellow">', 'after' => '</span>'])</h1>
</div>

<div class="content-block">
	<div class="container">
		<h1 class="centered">@lang('links.faqs_title')</h1>
		@if (Session::get('my.locale') == 'it')
		@include('links.faqs_it')
		@else
		@include('links.faqs_en')
		@endif
			
		<hr />
	</div>
</div>
@stop