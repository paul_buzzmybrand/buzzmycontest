@extends('layouts.public')

@section('content')
<div id="static-head" class="container">
    <h1 class="centered">@lang('landing.head_cta', ['before' => '<span class="yellow">', 'after' => '</span>'])</h1>
</div>

<div class="content-block">
	<div class="container">
		<h1 class="centered">@lang('links.our_flow_title')</h1>
		<img style="display: block;margin: 0 auto;" src="{{ asset('website_public/home_page/img/links/flow.jpg') }}" alt="Flow">
		<hr />
	</div>
</div>
@stop