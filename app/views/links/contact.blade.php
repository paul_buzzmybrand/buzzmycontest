@extends('layouts.public')

@section('content')
<div id="static-head" class="container">
    <h1 class="centered">@lang('landing.head_cta', ['before' => '<span class="yellow">', 'after' => '</span>'])</h1>
</div>

<div class="content-block">
	<div class="container">
		<h1 class="centered">@lang('links.contacts_title')</h1>
		<p>
			<strong>@lang('links.contacts_enquiries'): </strong>info@buzzmybrand.it
		</p>
		
		<hr />
	</div>
</div>
@stop