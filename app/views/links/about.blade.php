@extends('layouts.public')

@section('metadata')
	<title>@lang('title.about')</title>
	<meta name="description" content="{{trans('title.about_d')}}">
@stop

@section('content')
<div id="static-head" class="container">
    <h1 class="centered">@lang('landing.head_cta', ['before' => '<span class="yellow">', 'after' => '</span>'])</h1>
</div>

<div class="content-block">
	<div class="container">
		<h1 class="centered">@lang('links.about_title')</h1>
		<p style="line-height: 2.4em;">@lang('links.about_description', ['before' => '<strong>', 'after' => '</strong>', 'br' => '</br>'])</p>
		<hr />
	</div>
</div>
@stop