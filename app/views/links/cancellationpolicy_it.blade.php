<div>
    <p>
        Nel momento in cui un utente cancelli il suo account generato con BuzzMyBrand tutti i contest passati, presenti e programmati non saranno più disponibili.
    </p>
    <p>
        Quando si cancella un account BuzzMyBrand, si è l'unico responsabile per la cancellazione stessa. Il tuo account può essere cancellato in qualsiasi momento richiedendo tale cancellazione inviando una mail con OGGETTO: CANCELLAZIONE ACCOUNT all’indirizzo info@buzzmybrand.it. 
    </p>
    <p>
        Nessun contenuto o informazione può essere recuperato una volta che l’account è stato cancellato e l’utente si assume ogni responsabilità per la conservazione di qualsiasi contenuto o informazione prima della sua cancellazione. BuzzMyBrand può conservare contenuti o informazioni dell’account dell’utente dopo la cancellazione del backup e/o copie d'archivio e banche dati correlate, ma tali eventuali copie non saranno disponibili all’utente. E’ possibile richiedere la distruzione di tutti i dati e copie inviando una mail a info@buzzmybrand.it con OGGETTO: CANCELLAZIONE DATI.
    </p>
</div>