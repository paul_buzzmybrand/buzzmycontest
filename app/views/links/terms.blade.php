@extends('layouts.public')

@section('content')
<div id="static-head" class="container">
    <h1 class="centered">@lang('landing.head_cta', ['before' => '<span class="yellow">', 'after' => '</span>'])</h1>
</div>

<div class="content-block">
	<div class="container">
		<h1 class="centered">Terms & Conditions</h1>
		@include('links.terms_en')		
	</div>
</div>
@stop