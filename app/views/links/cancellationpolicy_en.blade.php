<div>
    <p>
        In case a user cancels its account with BuzzMyBrand all running, draft, scheduled and finished contests your account owns will no longer be available.
    </p>
    <p>
        When you cancel your account, you are solely responsible for properly cancelling your BuzzMyBrand account. Your account may be cancelled at any time by requesting such cancellation in writing to info@buzzmybrand.it with SUBJECT: CANCEL ACCOUNT.
    </p>
    <p>
        No content or information can be recovered once a user account is cancelled, and the user assumes all responsibility for preserving any content or information on the account prior to its cancellation. BuzzMyBrand may retain content or information from your account after cancellation in backup and/or archival copies of BuzzMyBrand and related databases, but such copies, if any, will not be available to you. You may request in writing to info@buzzmybrand.it with SUBJECT: DATA DELETE so that BuzzMyBrand timely deletes such copies of your account after such a request is made. 
    </p>
</div>