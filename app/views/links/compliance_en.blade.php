<div>
 <p>
  <span class="yellow">Q: Can I still use Page tabs and apps to administer a promotion? </span></br>
  Yes, Pages can still administer promotions via their Page tabs and apps as long as the promotion is run according to Facebook’s promotion guidelines.
 </p>
 <p>
  <span class="yellow">Q: Why can’t I ask people to take part in a promotion by liking, sharing, or posting something on their personal Timeline? </span></br>
  We want to make sure that people continue to post authentic, high quality content to their Facebook Timelines to stay better connected with the people they care about. There are technical reasons for confining the administration of the promotion to either a Page or in an app. For example, because people can choose to limit the visibility of the content they put on Facebook to only themselves, friends, or to a custom group of people, Pages won’t have the ability to access all of the entries that people post on their own Timelines unless these entries are public. We may explore enabling this in the future, but to ensure a good experience for administrators of promotions and prospective entrants to a promotion, we are limiting the administration of promotions on Facebook to Pages and within apps at this time.
 </p>
 <p>
  <span class="yellow">Q: How should I decide if I should administer my promotion via an app or directly on my Page? </span></br>
  Creating a promotion with an app on Facebook allows a Page to create a more personalized experience, more in line with your branding strategy. Apps provide more space and flexibility for content than Page posts alone. Promotions run through apps can collect data in a secure, structured way that may be appealing to advertisers, particularly larger brands. Creating a promotion with a Page is faster and easier. Additionally, as with all Page posts, Page posts about promotions are eligible to be displayed in the News Feeds of the people who like the Page and can be promoted to a broader audience. Businesses always have the option of using both an app and their Page to administer a promotion.
 </p>
 </br>
 <p>
 <h4>Promotions</h4>
 1. If you use Facebook to communicate or administer a promotion (ex: a contest or sweepstakes), you are responsible for the lawful operation of that promotion, including:</br>
 a.   The official rules;</br>
 b.   Offer terms and eligibility requirements (ex: age and residency restrictions); </br>
 c.   Compliance with applicable rules and regulations governing the promotion and all prizes offered (ex: registration and obtaining necessary regulatory approvals) </br>
 2. Promotions on Facebook must include the following:</br>
 a.   A complete release of Facebook by each entrant or participant.</br>
 b.   Acknowledgement that the promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook.</br>
 3. Promotions may be administered on Pages or within apps on Facebook. Personal Timelines and friend connections must not be used to administer promotions (ex: “share on your Timeline to enter” or “share on your friend's Timeline to get additional entries”, and "tag your friends in this post to enter" are not permitted).</br>
 4. We will not assist you in the administration of your promotion, and you agree that if you use our service to administer your promotion, you do so at your own risk.</br>
 </p>
</div>