<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/magnific.css') }}
        {{ HTML::style('website_public/css/common.css') }}
		{{ HTML::style('https://www.tok.tv/juventus/socialselfie/fb-app.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
        {{ HTML::script('website_public/js/jquery.validate.min.js') }}
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
    </head>
    <body>
		<?php App::setlocale($photo->contest->location->language); ?>
	    <div class="container page">            
			<div class="page-header">	
				<div class="row">
					<img src="{{asset('/images/companys/'.$photo->contest->company->image)}}" height="80" width="80">	
				</div>
				<div class="row">				
					<div class="col-md-6"><h1>{{ $photo->name }}</h1></div>
					<div class="col-md-6"></div>
				</div>
			</div>
			

			<div class="row">	
				<div class="col-md-6"><img src="{{asset('/photos/'.$photo->filename)}}" height="{{$height/2}}" width="{{$width/2}}"></div>
				<div class="col-md-6">
					@if ($photo->approval_step_id == 2)
					<div class="alert alert-info">{{ trans('messages.photo_not_approved') }} </div>
					@elseif ($photo->approval_step_id == 3)
					<div class="alert alert-info">{{ trans('messages.photo_approved') }} </div>
					<?php foreach($photo->fbPages as $page): ?>
						<div class="alert alert-info">{{ trans('messages.facebook_page') }}  "{{$page->fb_page_name}}"<br><a href="{{$page->fb_page_link}}/photos/{{$page->pivot->fb_idphoto}}" target="_blank" style='color:#333;'>{{ trans('messages.click_increase_score') }} </a></div>
					<?php endforeach; ?>
						@if ($photo->fb_idpost_usr != '' && $photo->fb_idpost_usr != null)
						<div class="alert alert-info">{{ trans('messages.tag_your_friends') }}<br><a href="https://www.facebook.com/photo.php?fbid={{$photo->fb_idpost_usr}}" target="_blank" style='color:#333;'>{{ trans('messages.click_increase_score') }}</a></div>
						@endif
					@else
					<div class="alert alert-info">{{ trans('messages.photo_needs_approval') }}</div>
				</div>

				

				
			</div>

			
			
			
			
        </div>


{{ HTML::script('website_public/js/magnific.js') }}
        {{ HTML::script('website_public/js/popup.js') }}
        {{ HTML::script('website_public/js/bootstrap.min.js') }}
           
            <script>
            $(document).ready(function()
            {
              //Handles menu drop down`enter code here`
              $('.dropdown-menu').find('form').click(function (e) {
               e.stopPropagation();
               });
             });
             </script>
    </body>
</html>

@endif





<script>
	$(document).ready(function() {
		$(".approval-btn").change(function(e) {
			$("#approveForm").submit();
		});
	});
</script>