<!doctype html>
<html>
<head>
</head>
<body>



<h1>Buzzmybrand’s Boilerplate Contest Rules</h1>


<h2>Last Updated: April 31st, 2015</h2> 


<h2>Contest Rules</h2>

<p>Definitions</p>

<p>In these Official Contest Rules ("Rules"), the following terms have the meanings set out below. In addition, other capitalized terms have the meaning so given to them.</p>
<p>"Administrator" means Buzzmybrand S.r.l.s.;</p>
<p>"Contest" means the contest, game, sweepstake, promotion, marketing activity or other event specified in the Contest Description;</p>
<p>"Contest Facilitators" means, collectively, the Administrator, the Organizer and any sponsors arranged by the Organizer; </p>
<p> “Contest Requirements” means the guidelines and criteria that entrants must abide by to be eligible for the Contest; </p>
<p> “Contest Site” means a subdomain of the Site, a domain or website of the Organizer, or, if applicable, an application within Facebook, Twiiter and/or Instagram;</p>
<p>"Contest Description" means the Contest Description page for a particular contest, which specifies certain information for that Contest;</p>
<p>"Organizer" means the organization that organizes and sponsors a Contest i.e. {{$company_sponsoring}}, as specified in the Contest Description;</p>
<p> “Prize” or “Prizes” means the prize or prizes (if there are multiple prizes) to be awarded for a particular Contest as specified in the Contest Description;</p>
<p>"Region" means the region(s) selected in which the Contest is held; and</p>
<p> “Site” means www.buzzmybrand.co any and any services available thereon.</p>
<p>Eligibility: The Contest is FREE to enter and open to all individuals who are legal residents of the Region(s) specified, and have reached the age of 16 at the time of their Entry (as defined below) (“you” or “Contestant”). It is your responsibility to ensure that you are legally eligible to enter the Contest under any laws applicable to you in your jurisdiction of residence or otherwise. The employees, officers and directors of the Contest Facilitators as well as the immediate family and household members of each such employee, officer and director are not eligible to participate in the Contest. The Contest is void outside the Region(s) specified and where otherwise prohibited by applicable federal, state, provincial or local laws, rules or regulations.</p>

<p>Agreement to Official Rules: By participating in the Contest, you fully and unconditionally agree to and accept these Rules and the decisions of the Organizer and/or Administrator, which are final and binding in all matters related to the Contest. Whether or not you receive a Prize is contingent upon fulfilling all requirements set forth in these Rules. The Contest is subject to all applicable federal, state, and local laws. The Organizer is responsible for ensuring the Contest complies with all laws in all jurisdictions where the Contest is offered.</p> 
<p>CONTESTS are void where prohibited by law. </p>
<p>Contest Period: The Contest opens on the date and time specified in the Contest Description, and ends on the date and time specified in the Contest Description (the "Contest Period"). The Administrator’s computer is the official time-keeping device for the Contest. </p>
<p>How to Enter: To enter the Contest, complete the registration form on the Contest Site with any required questions, and then follow the directions specified for the Contest in the Contest Description during the Contest Period. You must login with a valid Facebook account in order to participate in an Contest and, if required, with a Twitter and Instagram one. You must then provide an entry ("Entry") that fulfills the Contest Requirements ("Contest Requirements"), as specified in the Contest Description, to be eligible to win a Prize. Entries that do not include all the required information and adhere to the Rules will be considered void. The Contest Facilitators are not responsible for lost, misdirected or incomplete Entries.
The Organizer determines if an Entry meets the Contest Requirements and otherwise complies with the Rules. </p>
<p>Terms of Submission: You may enter the Contest as many times as is specified in the Contest Description. You may delete your Entry at anytime. By participating in the Contest you permit public voting in connection with your Entry. </p>

<p>Rights Granted by you: By entering this Contest, you agree that:
the Administrator, Organizer, and their respective licensees, successors and assigns will have the right to use all or a part of your Entry, your name and address (city and state/province/territory), and the names, likenesses, photographs, voices and images of all persons appearing in the Entry anywhere in the world and in perpetuity, for future advertising, trade, promotion and publicity in any manner and in any medium now known or hereafter devised throughout the world in perpetuity, without compensation and without notice to you and without consideration, review or approval from you; and
you agree that you will not now nor in the future be paid for your Entry or for granting the Administrator any of the rights set out in these Rules. </p>

<p>Your Representations and Warranties: By entering the Contest, you represent and warrant that:
your Entry, in its entirety, is an original work by you and you have not included third party content (such as writing, poetry, text, graphics, artwork, logos, photographs, likeness of any third party, musical recordings, clips of videos, television programs or motion pictures) in or in connection with your Entry without permission; </p>

<p>your Entry, the use thereof by the Contest Facilitators, or the exercise by the Organizer or Administrator of any of the rights granted by you under these Contest Rules, does not and will not infringe or violate any rights of any third party or entity, including, without limitation, patent, copyright, trademark, trade secret, defamation, privacy, publicity, false light, misappropriation, confidentiality, or any contractual or other rights; </p>

<p>you have all the rights, licenses, permissions and consents necessary to submit the Entry and to grant all of the rights that you have granted to the Contest Facilitators under these Contest Rules; </p>

<p>all persons who were engaged by you to work on the Entry, who appear in the Entry in any manner, or have contributed to the Entry have:
given you their written consent to submit the Entry without claims for payment of any kind; and
provided written permission to include their name, likenesses, image or pictures in or with your Entry (or if a minor who is not your child, you must have the permission of their parent or legal guardian) and you may be asked by the Contest Facilitators to provide such permission in writing to the Contest Facilitators; 
Prizes: The Organizer will provide the Prize(s) to be awarded for a particular Contest. No substitution of a Prize will be made unless the Organizer, in its sole discretion, determines otherwise. A Prize must be accepted as awarded, and the odds of winning are dependant upon the total number of eligible Entries received for the Contest Period. If, as a result of an error relating to the entry process, drawing or any other aspect of the Contest, there are more potential winners of Prizes than contemplated in these rules, there will be a random draw amongst all eligible Prize claimants after the Contest Closing Date (as set out in the Contest Description) to award the correct number of Prizes. </p>

<p>Winner(s): After the Content is uploaded onto any of the platforms above referred and the organizer approves the submission in that the content meets the Contest Theme and does not violate the provision contained in previous clauses above, the Content will be fed to the Organizer official social media Pages. Thereafter, the participants will need to collect as many points for their Content posted on such social media Pages by the following means:

<ul>
  <li>1 points for each “Like” on Facebook</li>
  <li>2 points for each “Comment” on Facebook</li>
  <li>2 points for each “Share” on Facebook</li>
  <li>0,2 points for each “View” on Facebook</li>
  <li>1 points for each “Favourite”” on Twitter</li>
  <li>2 points for each “Retweet” on Twitter</li>
  <li>1 points for each “Like” on YouTube</li>
  <li>0,2 points for each “View” on YouTube</li>
</ul>
</p>

	
<p>Please note that multiple actions from the same user will count as 1 act. A user who is found to be using fake ID will have his acts disregarded. </p>


<p>The winner(s) ("Winner(s)") of the Prize(s) </p>

<p>You will be notified of the selection of your Entry by the phone or email address provided by you on the Registration Form. If you do not respond to the phone or email notification of your selection as a Winner within three (3) days, an alternate Winner will be selected to receive the Prize. 
To Redeem Prize: Except where prohibited, a Winner (or his/her parent/legal guardian if the Winner is a minor in his/her jurisdiction of residence) shall be required to execute and return to the Organizer within ten (10) days of the date notice or attempted notice is sent, an Eligibility, Declaration of Compliance with the Rules and Liability & Publicity Release Form (“Declaration and Release Form”) in order to claim the Prize. 
If a Winner cannot be contacted, or fails to execute and return the Release Form within the required time period (if applicable), or if the Prize is returned as undeliverable, the Winner forfeits the Prize. Acceptance of any Prize shall constitute and signify the Winner's agreement and consent that the Contest Facilitators may use the Winner's name, photo, city, state, likeness, Entry and Prize information in connection with the Contest, worldwide, including on the internet, without limitation and without further payment or consideration, except where prohibited by law. 
Each Winner (or his/her parent/guardian if the Winner is a minor) is responsible for all taxes and fees associated with receipt of the Prize. </p>
<p>Your personal information will be processed in accordance with the Administrator’s Privacy Policy, as amended from time to time. You should direct any request to access, update, or correct your personal information to Administrator. </p>
<p><b>You may not enter with multiple e-mail and/or street addresses, nor multiple Facebook, Twitter, Instagram or Youtube account, nor may you use any other device or artifice to submit more than one (1) Entry or vote.</b> If you use fraudulent Entry methods, or otherwise attempt to participate with multiple e-mail and/or street addresses under multiple identities, or use any device or artifice to enter more than one (1) Entry, you will be disqualified. In the event of a dispute as to any Entry, the authorized account holder of the e-mail address used to enter will be deemed to be the person who made the Entry. For the purposes of these Rules, the authorized “account holder” is the natural person assigned an e-mail address by an internet access provider, online service provider or other organization responsible for assigning e-mail addresses for the domain associated with the submitted address. </p>
<p>Posting duplicate, or near duplicate, of your entry on Twitter is a violation of the <a href="https://support.twitter.com/articles/18311-the-twitter-rules">Twitter Rules</a> and you will be disqualified. </p>
<p>Entries which not comply with YouTube <a href="http://www.youtube.com/t/community_guidelinesYouTube">Community Guidelines</a> and <a href="http://www.youtube.com/static?gl=US&template=terms">Terms of Service</a> will be disqualified. </p>
<p>By entering the Contest, you fully and unconditionally agree to and accept these Rules and the decisions of the Contest Facilitators and the members of the voting community, which are final and binding on all matters relating to this Contest. </p>
<p>Contest Facilitators reserve the right to cancel, modify or suspend the Contest at any time (subject to approval of any regulatory body having jurisdiction), if it is determined that the Contest cannot be run as originally planned or if fraud or any other occurrence comprises the fairness or integrity of the Contest. </p>
<p>You understand and agree that this Contest is in no way sponsored, endorsed or administered by, or associated with, Facebook, Twitter or Instagram and any comments, questions or concerns regarding the Contest shall be directed to the Contest Facilitators and not to Facebook, Twitter or Instagram. </p>
<p>Liability Provisions: The Contest Facilitators are not responsible for human error, theft, destruction, or damage to Entries or other factors beyond its reasonable control. </p>
<p>You assume all risk of damaged, lost, late, incomplete, invalid, incorrect or misdirected Entries. The Contest Facilitators make no warranty, representation or guarantee, express or implied, in connection with its ability to in any way enable, develop or market the idea set forth in your Entry. </p>
<p>The Contest Facilitators shall not be liable to a Winner or any other person for failure to supply the Prize or any part thereof, by reason of the Prize becoming for reasons beyond the reasonable control of the Contest Facilitators unavailable or impracticable to award, or for any force majeure event, technical or equipment failure, terrorist acts, labor dispute, or act/omission of any kind (whether legal or illegal), transportation interruption, civil disturbance, or any other cause similar or dissimilar beyond the Organizer’s control. </p>
<p>CAUTION: ANY ATTEMPT BY YOU TO DELIBERATELY DAMAGE THE SITE OR ANY CONTEST SITE OR UNDERMINE THE LEGITIMATE OPERATION OF THE CONTEST IS A VIOLATION OF APPLICABLE CRIMINAL AND CIVIL LAW. SHOULD SUCH AN ATTEMPT BE MADE, THE ORGANIZER AND ADMINISTRATOR RESERVE THE RIGHT TO SEEK DAMAGES FROM ANY SUCH PERSON TO THE FULLEST EXTENT OF THE LAW AND TO DISQUALIFY YOU FROM THE CONTEST. </p>
<p>LIMITATIONS OF LIABILITY AND RELEASE: BY SUBMITTING AN ENTRY, YOU AGREE THAT THE RELEASED PARTIES WILL HAVE NO LIABILITY WHATSOEVER FOR, AND WILL BE RELEASED AND HELD HARMLESS BY YOU AND YOUR HEIRS, EXECUTORS, REPRESENTATIVES, SUCCESSORS AND ASSIGNS FOR ANY CLAIMS, LIABILITIES, OR CAUSES OF ACTION OF ANY KIND OR NATURE FOR ANY INJURY, LOSS OR DAMAGES OF ANY KIND, INCLUDING DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES TO PERSONS, INCLUDING, WITHOUT LIMITATION, DISABILITY OR DEATH, AND DAMAGE TO PROPERTY, DUE IN WHOLE OR IN PART, ARISING DIRECTLY OR INDIRECTLY, NOW OR IN THE FUTURE FROM THE DELIVERY, ACCEPTANCE, USE OR MISUSE OF THE PRIZE, ANY AND ALL USE AS PROVIDED HEREIN OF YOUR ENTRY, OR PARTICIPATION IN THIS CONTEST AND/OR ANY RELATED CONTEST. WITHOUT LIMITING THE FOREGOING, EVERYTHING ON THE SITE AND ANY CONTEST SITE IS PROVIDED “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. SOME JURISDICTIONS MAY NOT ALLOW THE LIMITATIONS OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES OR EXCLUSION OF IMPLIED WARRANTIES, IN WHICH CASE SUCH LIMITATION OR EXCLUSION SHALL APPLY ONLY TO THE EXTENT PERMITTED BY THE LAW IN THE RELEVANT JURISDICTION. </p>
<p>BuzzMyBrand shall be considered not responsible and has no liability in case an error is caused by unexpected restriction performed by Facebook Inc. and/or Twitter Inc. and/or Youtube LLC during the execution of this agreement. </p>
<p>Choice of Law: These Rules shall be governed by and construed in accordance with the laws of Italy, as applicable herein, without reference to its conflicts of laws principles. Any and all disputes, claims, and causes of action with the Contest Facilitators arising out of or connected with this Contest, other than the determination or validity of claims, shall be resolved individually, and exclusively by arbitration under the Court of Rome, in accordance with its rules, as amended from time to time. Any and all claims, judgments, and awards shall be limited to actual out-of-pocket costs incurred, including costs associated with participating in this Contest, but in no event attorneys’ fees. You hereby waive all rights to: (i) claim or be awarded any punitive, direct, indirect, incidental, and consequential damages and any other damages, other than for actual out-of-pocket expenses; and (ii) to have damages multiplied or otherwise increased. </p>
<p>Disclaimer: This Contest is in no way sponsored, endorsed or administered by, or associated with, Facebook, Inc., Twitter Inc., Youtube LLC or Instagram Inc. Any comments or questions about the Contest should be addressed to the Contest Facilitators. </p>

</body>
</html>