<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>JMTV Forget Password</title>
        {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap-responsive.min.css') }}
        {{ HTML::style('lib/css/main.css')}}
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" style="height:690px;">
                        <div class="logo">
                            <img src="lib/img/logo.png" alt=""/>
                        </div>
                        <form class="form-signin" action="{{ URL::to('forgot/password') }}">
                                
                            <div class="form-group">
                                <input name="emailAdd" type="email" class="form-control mytext" placeholder="E-MAIL" required autofocus />
                            </div>
                            <button class="btn-lg btn-block mybtn" type="submit">
                                CONFIRM
                            </button>
                            
                        </form>
                            
                        @if(Session::has('message'))
                        
                            {{ Session::get('message') }}
                        
                        @endif
                        
                     </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="account-box" style="color:#fff; text-align: center;">
                        JMTV Pte. Ltd. - Co. Reg. n. 201314573M 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
