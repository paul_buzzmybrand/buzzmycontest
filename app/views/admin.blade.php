<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>JMT Dashboard</title>
        {{ HTML::style('website_public/css/bootstrap.css') }}
        {{ HTML::style('website_public/css/magnific.css') }}
        {{ HTML::style('website_public/css/common.css') }}
        {{ HTML::script('website_public/js/jquery.js') }}
        {{ HTML::script('website_public/js/jquery.validate.min.js') }}
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,900' rel='stylesheet' type='text/css'>
</head>
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12  col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4"> 
               
                    
				<a href="http://www.jmtvideo.com" style='color:#333;'><img src="images/companys/JMTVIDEO-web-Logo.png" alt="" height="300" width="300"/></a>
				
				
				@if(Session::has('message'))
				<div class="alert alert-error">
					{{ Session::get('message'); }}
				</div>
				@endif
				<form class="form-signin" action="company/getSignin" method="post" style="height: 385px;">
					<div class="form-group">
						<input type="text" name="username" class="form-control mytext" placeholder="USERNAME"  />
					</div>
					<div class="form-group">
						<input type="password" name="password" class="form-control mytext" placeholder="PASSWORD" />
					</div>                        
					<button class="btn-lg btn-block mySignin" type="submit">
						LOGIN
					</button>

										<div style="text-align: right">PROBLEM WITH LOGIN? CONTACT US</div> 
					<div style="float:right; text-align: right">&nbsppao@buzzmybrand.it</div>
					<div style="float:right; text-align: right">&nbspsap@buzzmybrand.it</div>
				</form>
                    
                    
                 
                </div>
            
            </div>
            <div class="row">
                <div class="col-xs-12  col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">                    
                        JMTV Pte. Ltd. - Co. Reg. n. 201314573M                     
                </div>
             </div>
        </div>
        
    </div>
</body>
</html>