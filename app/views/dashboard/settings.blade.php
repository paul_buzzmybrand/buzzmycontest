'use strict';

var PAYMILL_PUBLIC_KEY = '{{ Config::get('paymill.api_keys.public') }}';

var COUNTRY_LIST = {
    @foreach(Countries::getList(App::getLocale(), 'php', 'cldr') as $code => $country)
    '{{ $code }}': '{{{ $country }}}',
    @endforeach
};

var LOCALE = '{{ App::getLocale() }}';

var CONTACT_US_EMAIL = '{{ Config::get("mail.from.address") }}';

var LANGUAGES = {'2': 'Italiano', '1': 'English'};

var INSTANT_ONLY = false;

var OBJECTIVES = {
  1: {
    'label': '{{ addslashes(Lang::get("objectives.community_engagement.label")) }}',
    'description': '{{ addslashes(Lang::get("objectives.community_engagement.description")) }}',
  },
  2: {
    'label': '{{ addslashes(Lang::get("objectives.brand_awareness.label")) }}',
    'description': '{{ addslashes(Lang::get("objectives.brand_awareness.description")) }}',
  },
  3: {
    'label': '{{ addslashes(Lang::get("objectives.brand_loyalty.label")) }}',
    'description': '{{ addslashes(Lang::get("objectives.brand_loyalty.description")) }}',
  },
  4: {
    'label': '{{ addslashes(Lang::get("objectives.sales.label")) }}',
    'description': '{{ addslashes(Lang::get("objectives.sales.description")) }}',
    // 'sales_father': true
  },
  // 4: {
    // 'label': '{{ addslashes(Lang::get("objectives.offline_sales.label")) }}',
    // 'description': '{{ addslashes(Lang::get("objectives.offline_sales.description")) }}',
    // 'sales_child': true
  // },
  // 5: {
    // 'label': '{{ addslashes(Lang::get("objectives.online_sales.label")) }}',
    // 'description': '{{ addslashes(Lang::get("objectives.online_sales.description")) }}',
    // 'sales_child': true
  // },
}

var TEMPLATE_HEADINGS_COLORS = {
  '#000000': '{{ Lang::get('settings.colors.black') }}',
  '#FFFFFF': '{{ Lang::get('settings.colors.white') }}'
}

var WIN_MODALITY_INSTRUCTIONS_BUZZ_IT = 'I vincitori del contest sono gli utenti che hanno totalizzato il più alto social buzz. Il social buzz calcola il numero di azioni sui social in cui il contest è attivo. Rendi il tuo post popolare e vinci!';
var WIN_MODALITY_INSTRUCTIONS_BUZZ_EN = 'Contest winners will be selected through the social buzz, an algorythm that calculates the actions on the social networks where the contest is launched. Make your entry the most popular and win!';

var ALERT_CHOOSE_TEMPLATE_IT = 'Hai selezionato il template';
var ALERT_CHOOSE_TEMPLATE_EN = 'You have chosen the template';