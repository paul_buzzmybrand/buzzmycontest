<?php
use Firebase\JWT\JWT;

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	if (strpos(Request::server('HTTP_HOST'), 'bmb.buzzmybrand.com') !== false)
	{
		//c'è la stringa, è un minisito

	}
	else
	{
		if( ! Request::secure())
		{

			return Redirect::secure(Request::path());
		}
	}

	/*
	if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
		$statusCode = 204;
		$headers = [
			'Access-Control-Allow-Origin'      => '*',
			'Access-Control-Allow-Methods'     => 'GET, POST, PUT, DELETE, OPTIONS',
			'Access-Control-Allow-Headers'     => 'Content-Type'
		];

		return Response::make(null, $statusCode, $headers);
	}
	*/

	//Log::debug("Remove mylocale");
	//Session::forget('my.locale');
	//
	//Log::debug("Result for http_accept_language: ".print_r(Request::server('HTTP_ACCEPT_LANGUAGE'),true));
	// Set default session language if none is set
    if(!Session::has('my.locale'))
    {
		if (strpos(Request::root(), 'buzzmybrand.it'))
		{
			$lang = "it";
		}
		else
		{
			$lang = "en";
		}




		//$lang = "en";
		//$ip = Request::server('REMOTE_ADDR');

		//$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
		//Log::debug("Result for root: ".print_r($root,true));


		//Log::debug("Result for HOST: ".print_r(Request::server('HTTP_HOST'),true));
		//$total_url = Request::server('HTTP_HOST').Request::server('REQUEST_URI');

		//$root_urls = array('www.buzzmybrand.it/', 'www.buzzmybrand.co/', 'www.buzzmybrand.com/');

		//CommonHelper::console(Request::server('REMOTE_ADDR') ,1 , 5);
		//CommonHelper::console($total_url ,1 , 5);
		/*
		if(!empty(Request::server('REMOTE_ADDR')))
		{

			if ( (strpos($root, 'buzzmybrand.it') !== false) || (strpos($root, 'buzzmybrand.com') !== false) )
			{


				//Log::debug("It is not a miniwebsite");
				//CommonHelper::console("It is not a miniwebsite" ,1 , $debug);
				$details = null;
				try
				{
					$locationDetails = CommonHelper::ip_info(Request::server('REMOTE_ADDR'), "Location");
					Log::debug("Result for geoplugin: ".print_r($locationDetails,true));

					//CommonHelper::console("" ,1 , 5);

					if ($locationDetails['country_code'] == "IT")
					{
						$lang = "it";
					}
					else
					{
						$lang = "en";
					}

				}
				catch (Exception $ex)
				{
					$lang = "en";
					Log::debug("Error message: ".$ex);
				}



			}


		}
		*/



		// fetch language from browser settings
		/*
		Log::debug("Result for http_accept_language: ".print_r(Request::server('HTTP_ACCEPT_LANGUAGE'),true));
		if(!is_null(Request::server('HTTP_ACCEPT_LANGUAGE')))
		{
			Log::debug("HTTP_ACCEPT_LANGUAGE is not null");
			$headerlang = substr(Request::server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
			if(in_array($headerlang, Config::get('app.languages')))
			{
				// browser lang is supported, use it
				Log::debug("browser lang is supported, use it: ".$headerlang);
				$lang = $headerlang;
			}
			// use default application lang
			else
			{
				$lang = Config::get('app.locale');
			}
		}
		*/

		/*
		// fetch language from url extension
		if (strpos(Request::root(), 'buzzmybrand.it'))
		{
			$lang = 'it';
		}
		else if (strpos(Request::root(), 'buzzmybrand.co'))
		{
			$lang = 'en';
		}
		else
		{
				// use default application lang
				$lang = Config::get('app.locale');
		}
		*/


		// set application language for that user
		//Session::put('my.locale', $lang);
		//settare il linguaggio dell'applicazione
		App::setLocale($lang);
		//Config::set('application.language',  $lang);


	}
    // session is available
    else
    {
        // set application to session lang
		//Session::get('my.locale');
        //Config::set('application.language', Session::get('language'));
    }

});


App::after(function($request, $response)
{
	$content = $response->getContent();

	$contentLength = strlen($content);

	$response->header('Content-Length', $contentLength);

});



/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('login');
});

Route::filter('auth_company', function()
{
	if (Auth::guest() || !Auth::user()->isCompany()) {
		if(Request::ajax())
			return Response::json(null, 400);

		return Redirect::to('admin');
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});



Route::filter('tokencheck', function()
{
	$pass = false;

	if (\Request::header('Authorization'))
	{
		try
		{
			$token = explode(' ', \Request::header('Authorization'))[1];
			$payload = (array) JWT::decode($token, \Config::get('constants.token_secret'), array('HS256'));

			if(array_key_exists('exp',$payload))
			{
				if ($payload['exp'])
				{
					if (date("Y-m-d H:i:s", $payload['exp']) > date('Y-m-d H:i:s'))
					{
						$pass = true;
					}
				}
			}
		}
		catch(\Exception $e)
		{
			\Log::debug("ADD key in cache: ".print_r(array($e),true));
			return Response::json(['message' => 'login expired'], 401);
		}
	}

	if ($pass) {
		\Request::merge(['user'=> $payload['sub']]);
	}
	else {
		return Response::json(['message' => 'login expired'], 401);
	}

});
