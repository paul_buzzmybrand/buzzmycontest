<?php

class PaymillController extends BaseController {

	function __construct()
	{
		// FIXME: un-comment this before going live
		// $this->beforeFilter('auth_company');
        $this->testUser = User::find(1);
	}

	public function getPaymentForm()
	{
		return View::make('payments/paymill_form');
	}

	public function makePayment()
	{
		$token = Input::get('paymillToken');

		if ($token) {
			try {
			    $transaction = CreditCard::makeAndCharge($token, 100.23, $this->testUser);
			    return Response::make($transaction->toJSON());
			} catch (PaymentException $err) {
				return Response::make("Error: " . $err->getMessage());
			}
		}
	}

	public function chargeUser()
	{
		try {
		    $transaction = CreditCard::chargeUser($this->testUser, 5.23, 'Test!');
		    return Response::make($transaction->toJSON());
		} catch (PaymentException $err) {
			return Response::make("Error: " . $err->getMessage());
		}
	}

    public function removeCard()
    {
        $card = CreditCard::getCardByUser($this->testUser);
        $card->delete();
        return Response::make($card->toJSON());
    }

}