<?php

class Services_CompanyController extends Controller {


    public function wordpress($resource) 
    {
        try
    	{
                $wordpress = new Wordpress();
	    	$pageContent = $wordpress->getPageContent($resource, 'en');
	    	return View::make('website/company/wordpress')->with('pageContent', $pageContent);
    	}
    	catch(\Exception $e)
    	{
                Log::warning("Could not download wordpress page. Name: ".$resource.", locale: en, error: ".$e);
    		return View::make('website/company/wordpress')->with('pageContent', '');
    	}
        
    }
    
    public function footer($resource)
    {
        try
    	{
                $wordpress = new Wordpress();
	    	$pageContent = $wordpress->getPageContent($resource, 'en');
	    	return View::make('website/company/footer')->with('pageContent', $pageContent);
    	}
    	catch(\Exception $e)
    	{
                Log::warning("Could not download wordpress page. Name: ".$resource.", locale: en, error: ".$e);
    		return View::make('website/company/footer')->with('pageContent', '');
    	}
    }
    
    public function register()
    {
		$dataToRegistration = Input::all();
        CompanyHelper::registerCompany($dataToRegistration);
        return View::make('website/company/confirm-register');
    }
    
    public function dashboard()
    {
		$contests = Contest::all();
		$companys = Company::all();
		return View::make('services/dashboard')->with('contests', $contests)->with('companys', $companys);
        if (CommonHelper::loggedInAsCompany())
        {
            $userId = UserHelper::getLoggedIn()->id;
               
            return View::make('services/company/dashboard');
        }
        else
        {
            return Redirect::action('Services_CompanyController@login', array('dashboard'));
        }
    }
    
    public function launchContest($id = null)
    {
        if (CommonHelper::loggedInAsCompany())
        {
            $error = false;
            $messages = false;
            if (Request::isMethod('post'))
            {
                
                $action = Input::get('submit');
                Input::flash();
                if($action == 'Submit')
                {
                    $rules = array(
                        'contestName' => 'required|max:30',
                        'startDate' => 'required|max:10',
                        'endDate' => 'required|max:10',
                        'appPicture' => 'required|image',
                        'webPicture' => 'required|image',
                        'briefRules' => 'required|max:200',
                        'judges' => 'required',
                        'entriesUser' => 'required'
                        
                    );
                    $rewardType = Input::get('rewardType');
                    if($rewardType == 'cash')
                    {
                        $rewardsRules = array(
                            '1st' => 'required|numeric',
                            '2nd' => 'required|numeric',
                            '3rd' => 'required|numeric',
                            
                        );
                        $rules = array_merge($rules, $rewardsRules);
                    }
                    $dataContest = Input::all();//var_dump($dataContest);exit;
                    $validator = Validator::make($dataContest, $rules);
                    if ($validator->fails())
                    {
                        $messages = $validator->messages();
                        return View::make('website/company/launch')->with('messages', $messages)->with('error', $error)->withInput(Input::all());
                    }
                    else
                    {
                        $startDate = Input::get('startDate');
                        $formatStartDate = ContestHelper::formatDate($startDate);
                        $startDate = $formatStartDate;
                        $formatStartDate->modify('+30 day');
                        $endDate = Input::get('endDate');
                        $formatEndDate = ContestHelper::formatDate($endDate);
                        if($formatStartDate > $formatEndDate)
                        {
                            $error = trans('messages.contest_date');
                            return View::make('website/company/launch')->with('messages', $messages)->with('error', $error)->withInput(Input::all());
                        }
                        else
                        {
                            $contestId = ContestHelper::SaveContestAsDraft($dataContest, $formatStartDate, $formatEndDate);
                            return Redirect::action('Website_CompanyController@launchContestStep2', array($contestId));
                        }
                    }
                    
                }
                if($action == 'Draft')
                {
                    $dataContest = Input::all();
                    ContestHelper::SaveContestAsDraft($dataContest);
                    return Redirect::action('Website_CompanyController@dashboard');
                }
                
            }
            return View::make('website/company/launch')->with('messages', $messages)->with('error', $error);
        }
        else
        {
            return Redirect::action('Website_CompanyController@login', array('launch-a-contest'));
        }
    }
    
    public function launchContestStep2($id)
    {
        if (CommonHelper::loggedInAsCompany())
        {
            if (Request::isMethod('post'))
            {
                $data = Input::all();
                ContestHelper::checkPlan($data, $id);
                return Redirect::action('Website_CompanyController@confirmPayment');
            }
            return View::make('website/company/launch-step-two')->with('id', $id);
        }
        else
        {
            return Redirect::action('Website_CompanyController@login', array('launch-a-contest'));
        }
    }
    
    public function confirmPayment()
    {
        return View::make('website/company/confirm-payment');
    }
    
    public function login($request)
    { 
        if (Request::isMethod('post'))
        {
            $userData = array(
                'username' => Input::get('company'),
                'password' => Input::get('password')
            );
            $user = CompanyHelper::loginCompany($userData);
            echo ($user ? 'true' : 'false');
        }
        else
        {
            return View::make('website/company/login')->with('request', $request);
        }
    }
    
    public function loginRequest($request)
    {
        return Redirect::to('website/company/'.$request);
    }
    
    public function logout()
    {
        if (Auth::check())
        {
            Auth::logout();
            return Redirect::to('website');
        }
        else
        {
            return Redirect::action('Website_CompanyController@wordpress', array('our-services'));
        }
    }
    
    public function description($request)
    {
        return View::make('website/description/'.$request);
    }
    
    public function functionDescription($request)
    {
        return View::make('website/functions/'.$request);
    }
    
    
    public function draftContest($id)
    {
        if (CommonHelper::loggedInAsCompany())
        {
            $error = false;
            $messages = false;
            $draftContest = ContestHelper::getDraftContestById($id);
            if (Request::isMethod('post'))
            {
                $action = Input::get('submit');
                if($action == 'Submit')
                {
                    $rules = array(
                        'contestName' => 'required|max:30',
                        'startDate' => 'required|max:10',
                        'endDate' => 'required|max:10',
                        'appPicture' => 'required|image',
                        'webPicture' => 'required|image',
                        'briefRules' => 'required|max:200',
                        'judges' => 'required',
                        'entriesUser' => 'required'
                        
                    );
                    $rewardType = Input::get('rewardType');
                    if($rewardType == 'cash')
                    {
                        $rewardsRules = array(
                            '1st' => 'required|numeric',
                            '2nd' => 'required|numeric',
                            '3rd' => 'required|numeric',
                            
                        );
                        $rules = array_merge($rules, $rewardsRules);
                    }
                    $dataContest = Input::all();
                    $validator = Validator::make($dataContest, $rules);
                    if ($validator->fails())
                    {
                        $messages = $validator->messages();
                    }
                    else
                    {
                        $startDate = Input::get('startDate');
                        $formatStartDate = ContestHelper::formatDate($startDate);
                        $startDate = $formatStartDate;
                        $formatStartDate->modify('+30 day');
                        $endDate = Input::get('endDate');
                        $formatEndDate = ContestHelper::formatDate($endDate);
                        if($formatStartDate > $formatEndDate)
                        {
                            $error = trans('messages.contest_date');
                        }
                        else
                        {
                            ContestHelper::UpdateContest($dataContest, $id);
                            return Redirect::action('Website_CompanyController@launchContestStep2', array($id));
                        }
                    }
                    
                }
                if($action == 'Draft')
                {
                    $dataContest = Input::all();
                    ContestHelper::UpdateContest($dataContest, $id);
                    return Redirect::action('Website_CompanyController@dashboard');
                }
                
            }
            return View::make('website/company/draft-contest')->with('contest', $draftContest)->with('messages', $messages)->with('id', $id)->with('error', $error);
        }
        else
        {
            return Redirect::action('Website_CompanyController@login', array('dashboard'));
        }
    }
    
    public function deleteAccount()
    {
        if (CommonHelper::loggedInAsCompany())
        {
            $userId = Auth::user()->id;
            CompanyHelper::deleteAccount($userId);
            return Redirect::action('Website_CompanyController@logout');
        }
        else
        {
            return Redirect::action('Website_CompanyController@login', array('dashboard'));
        }
    }
    
    /*
     * @TODO
     * check that company name is already used
     * @return string 'false' if is used, 
     */
    public function checkUsername()
    {
        
        $username = Input::get('username');
        //for example
        return 'true';
    }
    
    /*
     * check that company email is already used
     * @return string 'false' if is used, 
     */
    public function checkEmail()
    {
        $email = Input::get('email');
        $result = DB::table('companys')->where('email', $email)->first();
        if($result)
        {
            return 'false';
        }
        else
        {
            return 'true';
        }
        
    }
    
    public function printContest($id)
    {
        $contest = ContestHelper::getContestStats($id);
        $graph = ContestHelper::getGraph();
        return View::make('website/company/print')->with('id', $id)->with('contest', $contest)->with('graph', $graph);
    }
    
    public function team()
    {
        return View::make('website/company/team');
    }
    
    public function disconnectYouTubeAccount()
    {
    	$company = Company::find(CompanyHelper::getLoggedIn()->id);
    	$ch = CommonHelper::curl("https://accounts.google.com/o/oauth2/revoke?token=".$company->yt_access_token);
    	curl_exec($ch);
    	$info = curl_getinfo($ch);
    	Log::debug("YouTube token revoke curl info: ".print_r($info,true));
    	curl_close($ch);
    	 
    	$company->yt_access_token = null;
    	$company->save();
		return Redirect::action("Website_CompanyController@dashboard")->with("flash.message"," YouTube account unlinked")->with("flash.class","alert-info");
    }
    
    public function associateYouTubeAccount()
    {
    	return Redirect::to(YouTube::linkToYouTubeConsentPage(CompanyHelper::getLoggedIn()->id));
    }
    
    public function youTubeOAuth2Callback()
    {
    	if (CommonHelper::loggedInAsCompany())
    	{
	    	if (Input::has("code"))
	    	{
	    		$code = Input::get("code");
	    		YouTube::exchangeCodeForAccessToken($code, CompanyHelper::getLoggedIn()->id);
	    		return Redirect::action("Website_CompanyController@dashboard")->with("flash.message"," YouTube account linked")->with("flash.class","alert-success");
	    	}
	    	else
	    	{
	    		Log::warning("There is no code in oauth2 youtube callback. Either user denied access or it is internal error. URI: ".Request::path());
	    		return Redirect::action("Website_CompanyController@dashboard")->with("flash.message"," YouTube account not linked")->with("flash.class","alert-warning");
	    	}
    	}
    	else
    	{
    		return Redirect::action('Website_CompanyController@login', array('dashboard'));
    	}
    }
    
}
?>
