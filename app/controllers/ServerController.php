<?php
class ServerController extends BaseController {
 
    public function deploy() {
     
		 SSH::into('production')->run(array(
			'cd /var/www/html/laraproject',
			'git pull origin mynewbranch',
			'chown -R git:git .git'
	), function($line){
	 
		echo $line.PHP_EOL; // outputs server feedback
	});
	 
    }
}