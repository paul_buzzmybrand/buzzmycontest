<?php

use Laravel\Cashier\Customer;
//use Stripe\Customer;

class PaymentsController extends BaseController {

	function __construct()
	{
		// FIXME: un-comment this before going live
		$this->beforeFilter('auth_company', ['except' => 'sendExpirationWarnings']);
	}

    public function getInvoices()
    {
        print "[";
        $first = true;
        foreach(Invoice::orderBy('id', 'DESC')->where('user_id', Auth::user()->id)->get() as $invoice) {
            if(!$first) print ",";
            $invoice->contest_name = $invoice->contest['name'];
            print $invoice->toJson();
            $first = false;
        }
        print "]";
    }

    public function getInvoicePdf($id) 
    {
    	$invoice = Invoice::findOrFail($id);
        if($invoice->user_id == Auth::user()->id) {
            // return View::make('invoices.pdf', ['invoice' => $invoice]);
            return $invoice->getPdf()->stream();
        }

    	// Deny access to others
    	App::abort(404);
    }

    public function getCard()
    {
    	return Response::json(CreditCard::getCardByUser(Auth::user()));
    }

    public function postMakeCard()
    {
		$token = Input::get('paymillToken');

		if ($token) {
			try {
			    $card = CreditCard::makeCard($token, Auth::user());
			    return Response::json($card);
			} catch (PaymentException $err) {
				return Response::make($err->getMessage(), 400);
			}
		}
	}

	public function postMakeCustomer()
	{
		$token        = Input::get('stripeToken');
		$service_type = Input::get('service_type');
        //$coupon = str_replace(' ', '', Input::get('coupon'));
		$recaptcha    = Input::get('recaptcha');
		$customer_id  = Input::get('customer_id');

		if ($token) {
			try {
				Stripe::setApiKey("sk_test_OeVAPOsmdoAqkC4zkwzU0P5o");
				$user = Auth::user();
				$vat = $user->company()->getResults()->registration_number;

				// Create a Customer

				$customer_data = array(
					"source" => $token,
					"business_vat_id" => $vat,
					//"plan" => $plan,
					"description" => $user->name." ".$user->surname,
					"email" => $user->email);

                /*
				if (strlen($coupon) > 0)
				{
					$pushArr = array("coupon" => $coupon);
					$customer_data = array_merge($customer_data, $pushArr);
				}
                */


				if (empty($customer_id))
				{
					try
					{
						$customer = Customer::create($customer_data);

						$card = CreditCard::makeStripeCard($customer, $user, $token);
					}
					catch (Stripe_InvalidRequestError $err)
					{
						$error_message = $err->getMessage();
						return Response::make($error_message, 400);
					}
				}
				else
				{
					try
					{
						$customer = Customer::retrieve($customer_id);
						
                        $card = CreditCard::getCardByUser(Auth::user());
					}
					catch (Stripe_InvalidRequestError $err)
					{
						$error_message = $err->getMessage();
						return Response::make($error_message, 400);
					}
				}

				return Response::json($card);
			} catch (PaymentException $err) {
				return Response::make($err->getMessage(), 400);
			}
		}
	}

    public function postCheckout()
    {
		Log::debug("postCheckout() Request inputs: " . print_r( Input::all(), true ) );
        
        //mi serve customer_id, amount, description, se ho usato il coupon
        $contest_id = Input::get('contest_id');
		$auto_start = Input::get('contest_auto_start');
        $service_type = Input::get('service_type');
        $amount = Input::get('amount');
        $description = Input::get('description');
        $coupon = Input::get('coupon');
        $coupon_amount = Input::get('coupon_amount');
        $customer_id = Input::get('customer_id');
		$currency = Input::get('currency');
		$contest_amount = Input::get('contest_amount');
        $discountable = false;
        $invalid_coupon = false;
        $final_prize = null;

		Stripe::setApiKey("sk_test_OeVAPOsmdoAqkC4zkwzU0P5o");
		
        try {
            
			if (strlen($coupon) > 0)
            {
                //capire se il coupon inserito è valido                
                $coupon_object = Stripe_Coupon::retrieve($coupon);
                $discountable = true;
            }
            
        } catch (Stripe_InvalidRequestError $err) {            
            
            $discountable = false;
            $invalid_coupon = true;
        }
        

        try 
		{
            if (!$discountable) {
                $coupon = '';
            }
			
			$user = Auth::user();					

            $invoice = Invoice::where('contest_id', $contest_id)->first();
			
			if (!$invoice) {				
				
                $invoice = Invoice::generate($user, $customer_id, $contest_id, $amount, $currency, $description, $coupon, $coupon_amount, $contest_amount);
								
            }
			
			$contest = Contest::find($contest_id);
			//Log::debug("Print contest results: ".print_r($contest,true));
			
			if ($auto_start == 1) {
				
				if (isset($customer_id) && (!empty($customer_id))) {
					
					//creare invoice item               
					$invoiceItem = Stripe_InvoiceItem::create(array(
						"customer" => $invoice->customer_id,
						"amount" => intval($invoice->amount),
						"currency" => $currency,
						"description" => $invoice->description,
						"discountable" => $discountable
						//"invoice" => $invoice_id
					));

					//creare invoice
					$invoiceStripe = Stripe_Invoice::create(array(
						"customer" => $invoice->customer_id,
						"description" =>$contest->name
					));	

					//pay now
					$invoiceStripe->pay();					
					$invoice->invoice_stripe_id = $invoiceStripe->id;					
				}				
				
				$invoice->is_paid = 1;
				$invoice->contest_amount = $contest_amount;
				$invoice->save();
				
				
				$now = new DateTime();
				$isoFormat = 'Y-m-d H:i:s';				
				$start_time = date_format($now, $isoFormat);
                $contest->start_time = $start_time;
				$contest->service_type = $service_type;
				$contest->confirmed_by_customer = 1;
				$contest->status = 1;
				$contest->save();

				//mettere solo se contest ha pagina fb
				$fbPage = $contest->fbPages()->first();				
				Log::debug("Print fbPage results: ".print_r($fbPage,true));
				
				if ($fbPage) {
					
					try {
						$request = Request::create('/make-facebook-tab/' . $fbPage->id , 'GET', array());
						Request::replace($request->input());	
						Route::dispatch($request)->getContent();	
						/*
						$request = Request::create('/update-facebook-tab/' . $fbPage->id , 'GET', array());
						Request::replace($request->input());	
						Route::dispatch($request)->getContent();
						*/
					} catch (Exception $ex) {
						Log::error('facebookCustomerConnect: '.$ex);
					}
					
				}
				
				
				
				
				//Log::debug("Print contest results after save: ".print_r($contest,true));
								
				try {
					Log::info("Sending email");
					Mail::send('emails.contest_auto_start', array('user' => $user, 'contest' => $contest, 'contest_name' => $contest->name), function($message) use ($user) {
					$message->to($user->email, $user->name . " " . $user->surname)->bcc('support@buzzmybrand.com', 'Buzzmybrand')
							->subject(Lang::get('email.object_contest_start'));
					});
					
				}  catch (Exception $ex) {
					Log::error('Problem sending email: '.$ex);
				}
				
				
				
				
			}
			else {
				
				$contest->service_type = $service_type;
				$contest->confirmed_by_customer = 1;
				$contest->status = 3;
				$contest->save();
		
				try {
					Log::info("Sending email");
					//Send mail of scheduled contest only to clients that choose to postpone date of contest start
					Mail::send('emails.contest_edit_scheduled', array('user' => $user, 'contest' => $contest), function($message) use ($user) {
						$message->to($user->email, $user->name . " " . $user->surname)->bcc('support@buzzmybrand.com', 'Buzzmybrand')
							->subject(Lang::get('email.contest_edit_scheduled_subject'));
					});
					
				}  catch (Exception $ex) {
					Log::error('Problem sending email: '.$ex);
				}
				

				
			}
			
			
			//Mail to info@buzzmybrand.it for new contest
			/*
			try {
			
				Mail::send('emails.new_contest_alert', array(), function($message) use ($user) {
					
					$message->to("info@buzzmybrand.it", "Buzzmybrand")
							->subject("Notifica Checkout Nuovo Contest");
				
				});
			
			} catch (Exception $ex) {
				Log::error('Problem sending email: '.$ex);
			}
			*/
			
            
            if ($invalid_coupon) {
                $pushArr = array("invalid_coupon" => true);
                $invoice_data = array_merge($invoice->toArray(), $pushArr);
                return Response::json($invoice_data);
            }

            if (isset($coupon_object)) {
                //$pushArr = array("invalid_coupon" => true);
                if ($coupon_object->percent_off && $coupon_object->valid)
                {
                    $final_prize = round($amount * ((100-$coupon_object->percent_off) / 100), 2);
                    $final_prize = round($final_prize/100, 2);
                    
                    $pushArray = array("invalid_coupon" => false, "percent_off" => $coupon_object->percent_off, "final_prize" => $final_prize);
                }

                if ($coupon_object->amount_off && $coupon_object->valid)
                {
                    $final_prize = $amount - $coupon_object->amount_off;
                    $pushArray = array("invalid_coupon" => false, "percent_off" => $coupon_object->amount_off, "final_prize" => $final_prize);
                }

				if (!$coupon_object->valid)
				{
                    $pushArray = array("invalid_coupon" => true);
				}

                $invoice_coupon_data = array_merge($invoice->toArray(), $pushArray);
                return Response::json($invoice_coupon_data);
            }
            else return Response::json($invoice);

        } catch (Exception $ex) {

			$error_message = $ex->getMessage();
			Log::debug("Error in postCheckout function: ".$ex);
			return Response::make($error_message, 400);

        }
        
    }
        

	public function postChargeUser()
	{
		try {
		    $transaction = CreditCard::chargeUser(Auth::user(), 5.23, 'Test!');
		    return Response::make($transaction->toJSON());
		} catch (PaymentException $err) {
			return Response::make("Error: " . $err->getMessage());
		}
	}

    public function postRemoveCard()
    {
        $card = CreditCard::getCardByUser(Auth::user());
        $card->delete();
        return Response::make($card->toJSON());
    }

    public function getSendExpirationWarnings()
    {
    	if(Input::get('token') != Config::get('auth.api_token'))
    		App::abort(400);

    	$cards = CreditCard::getExpiringCards()->get();
    	if($cards->count() > 0) {
    		// Email admins
	        Mail::send("emails.cards_expiration_warning_admins", 
	            ['cards' => $cards], function($message) {
	                $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
	                $message->to(Config::get('ecommerce.send_alerts_to'))->subject('[WARNING] Carte in scadenza');
	        });

	        // Email users
	        foreach($cards as $card) {
	        	Mail::send("emails.cards_expiration_warning_user", 
		            ['card' => $card], function($message) use ($card) {
		                $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
		                $message->to($card->user->email)->subject(Lang::get('payments.email.expiration_subject'));
		        });
	        }

    	}
    }

    public function getPlans() {
        $plans = DB::table('service_fees')->select('id', 'service_type', 'fixed_fee', 'name')
            ->where('is_sellable', "1")
            ->orderBy('service_type')
            ->get();


        return $plans;
    }

    public function checkRecaptcha() {
        $recaptcha = Input::get('recaptcha', '');

        $client = new \GuzzleHttp\Client();
		/*
        $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded;charset=utf-8;',
            ],
            'body' => [
                'secret' => '6LdS5CQTAAAAAMxjpTOfWjhpdbpQj7OeybGpNtoI',
                'response' => $recaptcha
            ]
        ]);
        return $response->json();
		*/
		$response = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', [
			'form_params' => [
				'secret' => '6LdS5CQTAAAAAMxjpTOfWjhpdbpQj7OeybGpNtoI',
                'response' => $recaptcha
			]
		]);
		return json_decode($response->getBody(), true);
    }
}