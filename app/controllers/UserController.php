<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends BaseController {
        protected $fillable = array('username','password','email','image');
        function __construct() 
        {
            // ...
            $this->beforeFilter('oauth', array('except' => array('create', 'store')));
            // ...
        }
		

	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            //return View::make('users.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            return View::make('users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            $inputs = Input::all();
            $user = new User;
            $user ->username = $inputs['username'];
            if (User::where('username', '=', $inputs['username'])->count() > 0)
            {
                $statusCode=200;
                $description='Username Already exists';
                $response = Response::json(array('description'=>$description, 'errorCode'=>410),$statusCode);

                return $response;
            }
            $user ->password = Hash::make($inputs['password']);
            $user ->email = $inputs['email'];
            
            
            if (User::where('email', '=', $inputs['email'])->count() > 0)
            {
                $statusCode=200;
                $description='Email Already registered';
                $response = Response::json(array('description'=>$description, 'errorCode'=>411),$statusCode);
                return $response;
            }
            //return $inputs['file'];
            if(!isset($inputs['file']))
            {
                
                    //$statusCode=400;
                    //$description='Required Paramaters not Found';
                    //$response = Response::make($description, $statusCode);
                    //return $response;
                //$profileImageUploaded = 'default';
                //$extension = 'jpg';
                //$extension = strtolower($profileImageUploaded->getClientOriginalExtension());
                $user -> image = 'default.png';
                $uploadSuccess = true;
            }
            else
            {
                $profileImageUploaded = Input::file('file');
                $extension = strtolower($profileImageUploaded->getClientOriginalExtension());
                $imageFilePath=$profileImageUploaded->getRealPath();
                $videoFileName=md5_file($imageFilePath);
                $user -> image = $videoFileName.'.'.$extension;
                $destinationFileName = 'images/users/'.$user ->image;
            
                $uploadSuccess = File::move($inputs['file'], $destinationFileName);
            }
            
            
            if( $uploadSuccess ) 
            {
                $user -> save();
                $userSettings = new Setting;
                $userSettings -> user_id = $user->id;
                $userSettings -> facebook = 0;
                $userSettings -> twitter = 0;
                $userSettings -> youtube = 0;
                $userSettings -> save();
                $statusCode = 200;
                $response = Response::make($user, $statusCode);
                return $response;
            } 
            else 
            {
                $description = 'Error in File Upload';
                return Response::make($description, 400);
            }

	}
	

	public function getUserInformation()
	{
		$userId = ResourceServer::getOwnerId();
		$userDetails = User::findOrFail($userId);
		$userDetailsArray = $userDetails ->toArray();
		$statusCode = 200;
        $response = Response::json($userDetailsArray, $statusCode);    
        return $response;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
            try
            {
				
                if($id==0)
                {
                    $userId = ResourceServer::getOwnerId();
                }
                else
                {
                    $userId=$id;
                }
				Log::debug("userId: ".$userId);
				/*
                $userDetails = User::with(array('videos.contests' => function ($query)
                {
                    $query -> select('contests.name');
                }))->findOrFail($userId);
				*/
                $userDetails = User::with('company', 'contests')->findOrFail($userId);
								
                //order submission by rank, then score        
                //$userVideos = Video::where('user_id',$userId)->orderBy('created_at','DESC')->get()->toArray();
                $userVideosWithContests = Video::select("videos.*")
					->where('videos.user_id',$userId)
					->join("contests", "contests.id", "=", "videos.contest_id")
					->join("companys","companys.id", "=", "contests.company_id")->where("companys.type_id",Company::TYPE_INTERNAL)
					->orderBy('videos.created_at','DESC')->get()->toArray();
				
				$userPhotosWithContests = Photo::select("photos.*")
					->where('photos.user_id', $userId)
					->join("contests", "contests.id", "=", "photos.contest_id")
					->join("companys","companys.id", "=", "contests.company_id")->where("companys.type_id",Company::TYPE_INTERNAL)
					->orderBy('photos.created_at', 'DESC')->get()->toArray();
				
                $contestVideosArray = array();
                $i = 0;
				
                foreach($userVideosWithContests as $videos)
                {
                    //dd($videos);
                    //$contestJMTScore = $videos['score'] + $contestJMTScore;
                    $contestVideosArray[$i]['id'] = $videos['id'];
                    $contestVideosArray[$i]['name'] = $videos['name'];
                    $contestVideosArray[$i]['duration'] = $videos['duration'];
                    $contestVideosArray[$i]['image'] = $videos['image'];
                    $contestVideosArray[$i]['filename'] = $videos['filename'];
                    $contestVideosArray[$i]['score'] = $videos['score'];
					$contestVideosArray[$i]['rank_position'] = $videos['rank_position'];
                    $contestVideosArray[$i]['user_id'] = $videos['user_id'];
					
					$contestId = $videos['contest_id'];
					if ($contestId == 0)
					{
						$contestName = 'MyChannel';
						$contestVideosArray[$i]['contestName'] = $contestName;
					}
					else
					{
						$contest = Contest::find($contestId);	
						if (!empty($contest))
						{
							$contestPhotosArray[$i]['contestName'] = $contest->name;
						}
						else
						{
							$contestPhotosArray[$i]['contestName'] = "null";
						}
						
					}
					
					
                    /*if (isset($videos['contests'][0]))
                    {
                        $contestId = $videos['contests'][0]['id'];
                        //echo ($contestId);
                        $contestName = Contest::where('id',$contestId)->get(array('name'));
                        $contestVideosArray[$i]['contestName'] = $contestName[0]['name'];
                    }
                    else
                    {
                        $contestName = 'MyChannel';
                        $contestVideosArray[$i]['contestName'] = $contestName;
                    }*/
                    //return $contestId;

                    //return $contestName;

                    $i++;
                }
				
				
				//photo info for the user
				$contestPhotosArray = array();
                $i = 0;
                foreach($userPhotosWithContests as $photos)
                {
                    
                    $contestPhotosArray[$i]['id'] = $photos['id'];
                    $contestPhotosArray[$i]['name'] = $photos['name'];
                    $contestPhotosArray[$i]['image'] = $photos['image'];
                    $contestPhotosArray[$i]['filename'] = $photos['filename'];
                    $contestPhotosArray[$i]['score'] = $photos['score'];
					$contestPhotosArray[$i]['rank_position'] = $photos['rank_position'];
                    $contestPhotosArray[$i]['user_id'] = $photos['user_id'];
					
					$contestId = $photos['contest_id'];
					if ($contestId == 0)
					{
						$contestName = 'MyChannel';
						$contestPhotosArray[$i]['contestName'] = $contestName;
					}
					else
					{
						$contest = Contest::find($contestId);						
						if (!empty($contest))
						{
							$contestPhotosArray[$i]['contestName'] = $contest->name;
						}
						else
						{
							$contestPhotosArray[$i]['contestName'] = "null";
						}
					}
					

                    $i++;
                }
				
				
				
                
                if ($userDetails->company)
                {
	                //Assigning company contests
	                $companyContests = $userDetails->company->contests;
                }

                $userDetailsArray = $userDetails ->toArray();
                $userDetailsArray['userVideos'] = $contestVideosArray;
				$userDetailsArray['userPhotos'] = $contestPhotosArray;
                $userJMTScore = 0;
                foreach($userVideosWithContests as $videos)
                {
                    $userJMTScore = $videos['score'] + $userJMTScore;
                }
                
                foreach($userPhotosWithContests as $photos)
                {
                    $userJMTScore = $photos['score'] + $userJMTScore;
                }
				$userDetailsArray['userScore'] = $userJMTScore;
				
				Log::debug("Result for user details API: ".print_r($userDetailsArray,true));
				
                $statusCode = 200;
                $response = Response::json($userDetailsArray, $statusCode);    
                return $response;
				
				
            }
            catch(Exception $e)
            {
                $statusCode = 200;
                $description = 'User Not Found';
                $response = Response::json(array('description'=>$e->getMessage(), 'errorCode'=>404),$statusCode);
                return $response;
            }
			
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        //return View::make('users.edit');
	
            return 'here';
        }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
       
	public function update()
	{
		$inputs = Input::all();
                //return $inputs;
                $userId = ResourceServer::getOwnerId();
                //return array('user'=>$userId);
                $updateParameter = $inputs['type'];
                //$userId = ResourceServer::getOwnerId();
                
                //return array('user'=>$userId);
                switch ($updateParameter)
                {
                    case 'password':
                        $password = Hash::make($inputs['password']);
                        
                        //$userId = $id;
                        $user = User::find($userId);
                        $user->password = $password;
                        $user->save();
                        $statusCode = 200;
                        $description = 'Data updated successfully';
                        $returnResponse = array('description'=>'Data updated successfully','errorCode'=>200);
                        $response = Response::json($returnResponse,$statusCode);
                        return $response;
                        break;
                    case 'picture':
                        if(!isset($inputs['picture']))
                        {

                                $statusCode=400;
                                $description='Required Paramaters not Found';
                                $response = Response::make($description, $statusCode);
                                return $response;
                            
                        }
                        else
                        {
                            $profileImageUploaded = Input::file('picture');
                            $extension = strtolower($profileImageUploaded->getClientOriginalExtension());
                            //return $extension;
                            if (!isset($extension) or $extension=='')
                            {
                                $extension = 'png';
                            
                            }
                            $user = User::find($userId);
                            //return $user;
                            $imageFilePath=$profileImageUploaded->getRealPath();
                            $videoFileName=md5_file($imageFilePath);
                            $user -> image = $videoFileName.'.'.$extension;
                            $destinationFileName = 'images/users/'.$user ->image;

                            $uploadSuccess = File::move($inputs['picture'], $destinationFileName);
                        }


                        if( $uploadSuccess ) 
                        {
                            $user -> save();
                            $statusCode = 200;
                            $returnResponse = array('description'=>'Data updated successfully','errorCode'=>200);
                            $response = Response::json($returnResponse,$statusCode);
                            return $response;
                        } 
                        else 
                        {
                            $description = 'Error in File Upload';
                            $returnResponse = array('description'=>$description,'errorCode'=>400);
                            $response = Response::json($returnResponse,$statusCode);
                            return $response;
                        }
                        break;
                    default:
                        $statusCode = 400;
                        $description = 'Parameters not set correctly';
                        $returnResponse = array('description'=>$description,'errorCode'=>400);
                        $response = Response::json($returnResponse,$statusCode);
                        return $response;
                        break;
                
                }
                
	}
        public function put()
        {
            return 'here';
        }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		$userId = ResourceServer::getOwnerId();
                if ($userId == NULL)
                {
                    $statusCode = 400;
                    $description = 'Parameters not set properly';
                    $response = Response::make($description,$statusCode);
                    return $response;
                }
                User::destroy($userId);
                Video::where('user_id',$userId)-> delete();
                $contest = User::find($userId);
                
                //$contest->contest()->detach($userId);
                $statusCode = 200;
                $description = 'User Deletion successful';
                $returnResponse = array('description'=>$description,'errorCode'=>200);
                $response = Response::json($returnResponse,$statusCode);
                return $response;
	}
        
      

}
