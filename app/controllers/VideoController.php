<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class VideoController extends BaseController {

	public function getVideoInfo($id)
	{
		try
		{
			$videoList = Video::findOrFail($id);

			return $videoList;
		}
		catch (Exception $ex)
		{
			$statusCode = 400;
			$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
			return $json;
		}
	}

	public function getVideo($id)
	{
		$video = Video::find($id);
		$contest = $video->contest;

		$video = Video::with(
						array('user.facebookProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},
						'user.twitterProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},)
					)->where('id', $id)->first();

		return View::make('video')->with('video', $video);

	}


	public function likeVideo()
	{
		$videoId = Input::get('videoId');
		$userId = Input::get('userId');
		$companyId = Input::get('companyId');

		//$videoId = '1043';
		//$userId = '3825';

		$facebook_profile = FacebookProfile::where('user_id', $userId)->where('company_id', $companyId)->first();

		$video = Video::where('id', $videoId)->first();

		//$user = User::where('id', $userId)->first();

		//ricavare il facebook profile. E' necessario avere anche il company_id

		$contest=Contest::find($video->contest_id);

		$facebook = new Facebook\Facebook([
			'app_id' => $contest->company->fbApp_clientID,
			'app_secret' => $contest->company->fbApp_clientSecret,
			'default_graph_version' => 'v2.4',
		]);

		//$facebook->setAccessToken($facebook_profile->user_fb_token);

		foreach ($contest->fbPages as $page)
		{

			$videosOfPage = $page->videos()->where('fb_page_video.video_id', '=', $video->id)->get();

			foreach ($videosOfPage as $videoInPage)
			{
				try
				{
					Log::debug("Result for Contest detail API: ".$videoInPage->pivot->fb_idpost);
					$responseLike = $facebook->post("/".$videoInPage->pivot->fb_idpost."/likes", null, $facebook_profile->user_fb_token);
				}
				catch (Exception $ex)
				{
					Log::error("Error during liking video: ".$ex);
					$statusCode = 400;
					$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
					return $json;
				}

			}

		}


		Log::debug("Facebook response like Video: ".print_r($responseLike,true));

		$json = Response::json($responseLike, '200');
		return $json;

		//dare in uscita il responso dell'API per capire se è positivo o negativo

	}


	public function getVideoList($id)
	{
		try
		{
			$limit = Input::get('limit');
			if(!isset($limit))
			{
				$limit = 3;
			}

			$videoListTemp = Video::select("videos.*")
									->join("contests","contests.id", "=", "videos.contest_id")
									->where('contests.id','=',$id)
									->orderBy('score', 'desc');

			$videoList = $videoListTemp->paginate($limit)->toArray();


			$i = 0;
			$videoListArray = array();
			$videoListArray['total'] = $videoList['total'];
			$videoListArray['per_page'] = $videoList['per_page'];
			$videoListArray['current_page'] = $videoList['current_page'];
			$videoListArray['last_page'] = $videoList['last_page'];
			$videoListArray['from'] = $videoList['from'];
			$videoListArray['to'] = $videoList['to'];

			$contestVideosArray = array();
			foreach($videoList['data'] as $videos)
			{

				$contestVideosArray[$i]['id'] = $videos['id'];
				$contestVideosArray[$i]['name'] = $videos['name'];
				$contestVideosArray[$i]['duration'] = $videos['duration'];
				$contestVideosArray[$i]['image'] = $videos['image'];
				$contestVideosArray[$i]['filename'] = $videos['filename'];
				$contestVideosArray[$i]['score'] = $videos['score'];
				$contestVideosArray[$i]['user_id'] = $videos['user_id'];
				$contestVideosArray[$i]['location'] = $videos['location'];
				if (isset($videos['contest_id']))
				{
					$contestId = $videos['contest_id'];
					$contestVideosArray[$i]['contest_id'] = $contestId;
					$contest = DB::table('contests')->where('id', $contestId)->pluck('name');
					$contestVideosArray[$i]['contestName'] = $contest;

				}
				else
				{
					$contestName = 'MyChannel';
					$contestVideosArray[$i]['contestName'] = $contestName;
				}


				$i++;
			}
			$videoListArray['data'] = $contestVideosArray;

			$statusCode = 200;
			$response = Response::json($videoListArray, $statusCode);
            return $response;
			//return $videoListArray;
		}
		catch (Exception $ex)
		{
			$statusCode = 400;
			$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
			return $json;
		}



	}


		public function MultimediaElementsUser()
	{
		try
		{
			$limit = Input::get('limit');
			$page = Input::get('page', 1);

			$id = Input::get('userID');
			$companyID = Input::get('companyID');

			if(!isset($limit))
			{
				$limit = 6;
			}

			$combinedListTemp = DB::select('(select photos.id,
			photos.name,
			photos.image,
			photos.filename,
            photos.fb_source_url,
			photos.score as score,
			photos.rank_position as rank,
			facebook_profiles.name as user_fullname,
			facebook_profiles.image as user_image,
			contests.name as contestName,
			contests.type_id as type_id
			from photos
						inner join contests on contests.id = photos.contest_id
						inner join users on users.id = photos.user_id
						inner join facebook_profiles on facebook_profiles.user_id = users.id
						inner join companys on companys.id = contests.company_id
						where companys.type_id = 1 and photos.deleted_at is null
						and companys.id = '.$companyID.'
						and facebook_profiles.company_id = '.$companyID.'
						and photos.uploadOnFacebookDone = 1
						and photos.approval_step_id = 3 and users.id = '.$id.' )
						union
						(
			select videos.id,
			videos.name,
			videos.image,
			videos.filename,
			videos.fb_source_url,
			videos.score as score,
			videos.rank_position as rank,
			facebook_profiles.name as user_fullname,
			facebook_profiles.image as user_image,
			contests.name as contestName,
			contests.type_id as type_id
						from videos
						inner join contests on contests.id = videos.contest_id
						inner join users on users.id = videos.user_id
						inner join facebook_profiles on facebook_profiles.user_id = users.id
						inner join companys on companys.id = contests.company_id
						where companys.type_id = 1 and videos.deleted_at is null
						and companys.id = '.$companyID.'
						and facebook_profiles.company_id = '.$companyID.'
						and videos.uploadOnFacebookDone = 1
						and videos.approval_step_id = 3 and users.id = '.$id.')
						order by rank ASC');


			if (count($combinedListTemp)>0)
			{
				$currentPage = $page - 1;
				$pagedData = array_slice($combinedListTemp, $currentPage * $limit, $limit);
				$combinedListTemp = Paginator::make($pagedData, count($combinedListTemp), $limit);
				$statusCode = 200;
				$response = Response::json($combinedListTemp, $statusCode);
				return $response;
			}
			else
			{
				//retrieve only user information
				//$userDetails = User::select('name', 'image')->where('id',$id);
				$userDetails = FacebookProfile::where('user_id', $id)->where('company_id', $companyID)->select('name', 'image')->first();
				//$userDetails = User::select('name', 'image')->findOrFail($id);

				return $userDetails;
			}


		}
		catch (Exception $ex)
		{
			$statusCode = 400;
			$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
			return $json;
		}
	}



	public function FavouriteMediaElements($id)
	{

		//necessary fields: id, name, image, filename, score, created_at, user_fullname, user_image, contestName
		try
		{
			$limit = Input::get('limit');
			$page = Input::get('page', 1);
			$company_id = Input::get('company_id');

			if(!isset($limit))
			{
				$limit = 6;
			}
			/*
			$videoListTemp = DB::table('videos')
				->join("contests", "contests.id", "=", "videos.contest_id")
				->join("users", "users.id", "=", "videos.user_id")
				->join("companys","companys.id", "=", "contests.company_id")->where("companys.type_id",Company::TYPE_INTERNAL)
				->select("videos.id", "videos.name","videos.image","videos.filename","videos.score","videos.created_at","users.name as user_fullname","users.image as user_image", "contests.name as contestName")
				->whereNull('videos.deleted_at');
				//->orderBy('score', 'desc')->orderBy('created_at', 'desc');

			$photoListTemp = DB::table('photos')
				->join("contests", "contests.id", "=", "photos.contest_id")
				->join("users", "users.id", "=", "photos.user_id")
				->join("companys","companys.id", "=", "contests.company_id")->where("companys.type_id",Company::TYPE_INTERNAL)
				->select("photos.id", "photos.name","photos.image","photos.filename","photos.score","photos.created_at","users.name as user_fullname","users.image as user_image", "contests.name as contestName")
				->whereNull('photos.deleted_at');
				//->orderBy('score', 'desc')->orderBy('created_at', 'desc');
			*/
			$combinedListTemp = DB::select('
			(select photos.id,
	photos.name,
	photos.image,
	photos.filename,
	photos.fb_source_url,
	photos.score,
	photos.rank_position,
	photos.created_at,
	facebook_profiles.name as user_fullname,
	facebook_profiles.image as user_image,
	contests.name as contestName,
	contests.type_id as type_id
			from bmb_test2.photos
			inner join contests on contests.id = photos.contest_id
			inner join users on users.id = photos.user_id
			inner join facebook_profiles on facebook_profiles.user_id = users.id
			inner join companys on companys.id = contests.company_id
			where contests.id = '.$id.' and facebook_profiles.company_id = '.$company_id.'

			and companys.type_id = 1 and photos.deleted_at is null
			and photos.approval_step_id = 3 and photos.uploadOnFacebookDone = 1)
			union
			(select videos.id,
				videos.name,
				videos.image,
				videos.filename,
				videos.fb_source_url,
				videos.score,
				videos.rank_position,
				videos.created_at,
				facebook_profiles.name as user_fullname,
				facebook_profiles.image as user_image,
				contests.name as contestName,
				contests.type_id as type_id
			from bmb_test2.videos
			inner join contests on contests.id = videos.contest_id
			inner join users on users.id = videos.user_id
			inner join facebook_profiles on facebook_profiles.user_id = users.id
			inner join companys on companys.id = contests.company_id
			where contests.id = '.$id.' and facebook_profiles.company_id = '.$company_id.'

			and companys.type_id = 1 and videos.deleted_at is null
			and videos.approval_step_id = 3 and videos.uploadOnFacebookDone = 1)
			order by rank_position ASC');


			$currentPage = $page - 1;
			$pagedData = array_slice($combinedListTemp, $currentPage * $limit, $limit);
			$combinedListTemp = Paginator::make($pagedData, count($combinedListTemp), $limit);

			//Log::debug("Risultato: ".$combinedListTemp);
			$statusCode = 200;
			$response = Response::json($combinedListTemp, $statusCode);
            return $response;

		}
		catch (Exception $ex)
		{
			$statusCode = 400;
			$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
			return $json;
		}
	}



	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		try
		{


			$limit = Input::get('limit');
			$locationID = Input::get('locationID');
			$title = Input::get('title');

			$externalContest = CommonHelper::mandatoryInput("External_contest");
			$latitude = CommonHelper::optionalInput("Latitude");
			$longitude = CommonHelper::optionalInput("Longitude");

			if ((isset($latitude) && !isset($longitude)) || (!isset($latitude) && isset($longitude)))
			{
				throw new MissingMandatoryParametersException("You need to set both Latitude and Longitude parameters or none of them");
			}

			if(!isset($limit))
			{
				$limit = 12;
			}


			//$videoList = Video::where('location','like',$location.'%')->where('name','like',$title.'%')->orderBy('score','desc');
			//Log::debug("List of video screened by location and title: ".$videoList);

			if ($externalContest == "false")
			{

				//return $videoList[0];
				$videoListTemp = Video::select("videos.*")
				->where('videos.name','like','%'.$title.'%')
				->join("locations","locations.id", "=", "videos.location_id")
				->join("contests", "contests.id", "=", "videos.contest_id")
				->join("companys","companys.id", "=", "contests.company_id")->where("companys.type_id",Company::TYPE_INTERNAL)
				->orderBy('score', 'desc')->orderBy('created_at', 'desc');
				//$videoListTemp = Video::with('contests')->where('location','like',$location.'%')->where('name','like',$title.'%')->orderBy('score');

				$queries = DB::getQueryLog();
				Log::debug("Queries: ".print_r($queries,true));
				Log::debug("Number of videos returned: ".count($videoListTemp));

				//Order by distance if available
				if (isset($latitude))
				{
					$videoListTemp = $videoListTemp->orderBy(DB::raw("ACOS(COS(RADIANS(90-".$latitude."))*COS(RADIANS(90-latitude))+SIN(RADIANS(90-".$latitude."))*SIN(RADIANS(90-latitude))*COS(RADIANS(".$longitude."-longitude)))*6371"));
				}
				else if (isset($locationID))
				{
					$videoListTemp = $videoListTemp->where("videos.location_id", $locationID);
				}

				/*if (!isset($locationID))
				{
					//show only the videos that belong to the location present on the first row of the queryBuilder
					//In this way we can avoid visualization of foreign videos
					$nearestVideo = $videoListTemp->first();
					$locationID = $nearestVideo->location_id;
					$videoListTemp = $videoListTemp->where("videos.location_id", $locationID);
					//*************************************************************
					Log::debug("Now we have the locationID: ".$locationID);
				}
				else
				{
					//This condition is executed when the user is doing a Search
					$videoListTemp = $videoListTemp->where("videos.location_id", $locationID);
				}*/

				$videoList = $videoListTemp->paginate($limit)->toArray();

			}
			else if ($externalContest == "true")
			{

				$videoListTemp = Video::select("videos.*")
				->where('location_id',$locationID)
				->where('name','like','%'.$title.'%')
				->join("locations","locations.id", "=", "videos.location_id")
				->join("contests", "contests.id", "=", "videos.contest_id")
				->join("companys","companys.id", "=", "contests.company_id")->where("companys.type_id",Company::TYPE_EXTERNAL)
				->orderBy('score', 'desc')->orderBy('created_at', 'desc');
				//$videoListTemp = Video::with('contests')->where('location','like',$location.'%')->where('name','like',$title.'%')->orderBy('score');
				$videoList = $videoListTemp->paginate($limit)->toArray();

			}

			$queries = DB::getQueryLog();
			Log::debug("Queries: ".print_r($queries,true));
			Log::debug("Number of videos returned: ".count($videoList));


			/*$statusCode = 200;
			$response = Response::make($videoList,$statusCode);
	        return $response;*/

			$i = 0;
			$videoListArray = array();
			//return $videoListTemp[0]['contests'][0]['id'];
			//dd ($videoListTemp['data'][0]['contests']);
			$videoListArray['total'] = $videoList['total'];
			$videoListArray['per_page'] = $videoList['per_page'];
			$videoListArray['current_page'] = $videoList['current_page'];
			$videoListArray['last_page'] = $videoList['last_page'];
			$videoListArray['from'] = $videoList['from'];
			$videoListArray['to'] = $videoList['to'];

			$contestVideosArray = array();
			foreach($videoList['data'] as $videos)
			{
				//dd($videos);
				//$contestJMTScore = $videos['score'] + $contestJMTScore;
				$contestVideosArray[$i]['id'] = $videos['id'];
				$contestVideosArray[$i]['name'] = $videos['name'];
				$contestVideosArray[$i]['duration'] = $videos['duration'];
				$contestVideosArray[$i]['image'] = $videos['image'];
				$contestVideosArray[$i]['filename'] = $videos['filename'];
				$contestVideosArray[$i]['score'] = $videos['score'];
				$contestVideosArray[$i]['user_id'] = $videos['user_id'];
				$contestVideosArray[$i]['created_at'] = $videos['created_at'];
				$contestVideosArray[$i]['location_id'] = $videos['location_id'];
				$contestVideosArray[$i]['user_fullname'] = DB::table('users')->where('id', $videos['user_id'])->pluck('name');
				$contestVideosArray[$i]['user_image'] = DB::table('users')->where('id', $videos['user_id'])->pluck('image');
				if (isset($videos['contest_id']))
				{
					$contestId = $videos['contest_id'];
					$contestVideosArray[$i]['contest_id'] = $contestId;
					//echo ($contestId);
					//$contestName = Contest::where('id',$contestId)->get(array('name'));

					$contest = DB::table('contests')->where('id', $contestId)->pluck('name');

					//$contestBuilder = Contest::where('id',$contestId);
					//$contest = $contestBuilder->get();
					$contestVideosArray[$i]['contestName'] = $contest;

				}
				else
				{
					$contestName = 'MyChannel';
					$contestVideosArray[$i]['contestName'] = $contestName;
				}

				$i++;
			}
			$videoListArray['data'] = $contestVideosArray;
			//return $videoListArray;

			$statusCode = 200;
			$response = Response::json($videoListArray, $statusCode);
            return $response;

			/*$statusCode = 200;
			$response = Response::make($videoList, $statusCode);
			return $response;*/

		}
		catch (Exception $ex)
		{
			Log::error($ex);
			$statusCode = 400;
			$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
			return $json;
		}


	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            return View::make('videos.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            //Inputs contest,title,video,duration,image

            $inputs = Input::all();
            Log::debug("VideoController::store(): input parameters: ".print_r($inputs,true));

			$reqParamContest = isset($inputs['contest'])||isset($inputs['contest_id']);

			if (!$reqParamContest)
			{
				Log::debug("VideoController::store(): Required parameters were not found, returning 400");
                $description='Required Paramaters Not Found';
                $statusCode=400;
                return Response::make($description, $statusCode);
			}

			$object_title = '';
			if (isset($inputs['title']))
			{
				$object_title = utf8_encode($inputs['title']);
			}
			else
			{
				$object_title = $inputs['product'];
			}

            if(!isset($inputs['video']))
            {
            	Log::debug("VideoController::store(): Required parameters were not found, returning 400");
                $description='Required Paramaters Not Found';
                $statusCode=400;
                return Response::make($description, $statusCode);
            }

			//$user_Fb_token = CommonHelper::optionalInput("user_Fb_token");

            try
			{
				//$gcmRegistrationId = CommonHelper::optionalInput("gcmRegistrationId");
				$videoFile=Input::file('video');
				$videoFilePath=$videoFile->getRealPath();
				//$videoFileName=md5_file($videoFilePath);
				$videoFileName=md5(time());
				$videoExtension=strtolower($videoFile->getClientOriginalExtension());
				//$videoExtension=strtolower($videoFile->getMimeType());
				if (!isset($videoExtension) or $videoExtension=='')
				{
					$videoExtension = 'mp4';
				}

				if (isset($inputs['image']))
				{
					$imageFile=Input::file('image');
					$imageFilePath=$imageFile->getRealPath();
					//$imageFileName=md5_file($imageFilePath);
					//dd($videoFileName);
					$imageExtension=strtolower($imageFile->getClientOriginalExtension());
					Log::debug('File extention of video image is: '.$imageExtension);

					if (!isset($imageExtension) or $imageExtension=='')
					{
						$imageExtension = 'jpg';
					}
					File::move($imageFile,'images/videos/'."$videoFileName.$imageExtension");

				}



				/*Upload version to review
				try
				{
					$videoDestFile = $videoFile->move('videos/', $videoFileName.$videoExtension);
					$imageDestFile = $imageFile->move('images/videos/', $videoFileName.$imageExtension);
				}
				catch(Exception $e)
				{
					$statusCode = 400;
					$json = Response::json(array("error_message" => $e->getMessage()), $statusCode);
					return $json;
				}*/



				if (File::move($videoFile,'videos/'."$videoFileName.$videoExtension"))
				{


					//find the right contest for approval of content
					$contest = null;
					if (!isset($inputs['contest_id']))
					{
						$contestName=$inputs['contest'];
						$contest=Contest::where('name',$contestName)->first();
					}
					else
					{
						$contest=Contest::where('id',$inputs['contest_id'])->first();
					}

					//$ownerId = ResourceServer::getOwnerId();
					//$fb = $inputs['fbid'];


					//$fp = FacebookProfile::where("id_fb", $fb)->first();
					$user_id = $inputs['user_id'];

					$user = User::find($user_id);

					$ownerId = $user->id;
					//exec("sh ./GenerateFacebookShareHTMLForUploadedVideo.sh $videoFileName.$videoExtension", $output);
					//exec("sh ./GenerateThumbnails.sh $videoFileName.$videoExtension",$output);



					$video=new Video();
					$video->filename="$videoFileName.$videoExtension";
					$video->name=$object_title;
					if (isset($inputs['duration'])) {
						$video->duration=$inputs['duration'];
					}
					if (isset($inputs['image']))
					{
						$video->image="$videoFileName.$imageExtension";
					}
					$video->user_id=$ownerId;


					if (isset($inputs['sub_videopart']))
					{
						$video->sub_videopart=$inputs['sub_videopart'];
					}

					$facebookProfile = FacebookProfile::where('user_id', $ownerId)->where('company_id', $contest->company_id)->first();
					if ( $facebookProfile && !$facebookProfile->expired ) {
						$video->user_connected_to_facebook = 1;
					}

					$twitterProfile = TwitterProfile::where('user_id', $ownerId)->where('company_id', $contest->company_id)->first();
					if ( $twitterProfile && !$twitterProfile->expired) {
						$video->user_connected_to_twitter = 1;
					}

					///////////////////////////////////
					$video->approval_step_id=1;
					///////////////////////////////////

					///////////////////////////////////
					///////////////////////////////////
					exec("sh ./GenerateThumbnails.sh $videoFileName.$videoExtension", $output);
					///////////////////////////////////
					///////////////////////////////////


					$video->contest_id=$contest->id;
					$video->location_id=$contest->location_id;
					if (isset($inputs['description']))
					{
						$video->description=$inputs['description'];
					}
					//create thumbnail
					$thumbnail = ContestHelper::createThumbnailVideo(base_path().'/public/videos/'."$videoFileName.$videoExtension");
					$video->image = $thumbnail;

					$output = shell_exec("sh ".base_path()."/sh/GenerateThumbnails.sh ".$video->filename);
					Log::debug("Output GenerateThumbnails: ".$output);

					// ATTACH IF ROW DOESN' T EXIST
					/*
					$contest_2 = Contest::with(
									array(
										'users' => function($query) use ($ownerId) {
														$query->where('user_id', '=', $ownerId);
													}
										)
								)->where('id', $contest->id)->first();

					if ( $contest_2->users->count() == 0 ) {
						$contest_2->users->attach($ownerId);
					}
					*/
					//
					//join the contest
					//Contest::find($contest->id)->users()->attach($ownerId);

					//Dati bambino
					if (isset($inputs['sex_child'])) {
						$video->info_1=$inputs['sex_child'];
					}

					if (isset($inputs['age_child'])) {
						$video->info_2=$inputs['age_child'];
					}

					if (isset($inputs['character_child'])) {
						$video->info_3=$inputs['character_child'];
					}

					$video->save();
					Log::debug("Video saved on the db");


					/*
					$company_user = Company::find($contest->company_id)->belongsToManyUsers()->where('user_id', $user->id)->first();

					if ((isset($company_user->pivot->token) && $company_user->pivot->token !== ''))
					{
						$message = PushNotification::Message('Your video entitled "'.$video->name.'"  for '.$contest->name.' contest has been approved! Share on Facebook and make it viral!',array(
							'title' => $contest->name,
							'url' => 'https://www.buzzmybrand.it/video/'.$video->id));
						$app = '';
						if (strpos($company_user->pivot->token,'Android_') !== false)
						{
							$deviceToken = str_replace("Android_", "", $company_user->pivot->token);
							Log::debug("Device token to be used: ".$deviceToken);
							$app = $contest->company->name.'Android';
						}
						else if (strpos($company_user->pivot->token,'IOS_') !== false)
						{
							$deviceToken = str_replace("IOS_", "", $company_user->pivot->token);
							Log::debug("Device token to be used: ".$deviceToken);
							$app = $contest->company->name;
						}

						PushNotification::app($app)
							->to($deviceToken)
							->send("ciao");
					}
					*/

					//store and update the user access token
					//$user->user_fb_token = $user_Fb_token;
					//$user->save();

					/*
					if ($inputs['contest'] != "MyChannel")
					{
						//dd ($contestName);
						$contest=Contest::where('name',$contestName)->first();
						$video->contests()->attach($contest->id);
					}
					*/

					$statusCode=200;
					return Response::make($video,$statusCode);

				}
				else
				{
					return Response::json(array("error" => "Error in File Upload"),500);
					/*$description='Error in File Upload';
					$statusCode=400;
					return Response::make($description,$statusCode);*/
				}




			}
			catch(Exception $e)
			{
				Log::error($e);
				return Response::json(array("error" => "Internal error occured"),500);
			}




	}

	public function message2($data, $page_token)
    {
        // need token
        $data['access_token'] = $page_token;


        // init
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/me/feed");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // execute and close
        $return = curl_exec($ch);
		Log::debug("Facebook message2 JSON: ".$return);
        curl_close($ch);

        // end
        return $return;
    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try
		{
			$videoList = Video::findOrFail($id);

			return $videoList;
		}
		catch(ModelNotFoundException $e)
		{
			$statusCode = 200;
			$description = 'Video Not Found';
			$response = Response::json(array('description'=>$description, 'errorCode'=>404),$statusCode);
			return $response;
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            return View::make('videos.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function softDelete($id)
	{
		//
		try
		{
			$video = Video::findOrFail( $id );
			$video->delete();

			//delete the post from the FB page + update the ranking of the belonging contest
			Scheduler::deleteVideoPost($id);
			Scheduler::UpdateRanking($video->contest_id);

			$statusCode = 200;
			$description = 'Video Deletion successful';
			$returnResponse = array('description'=>$description,'errorCode'=>200);
			$response = Response::json($returnResponse,$statusCode);
			return $response;
		}
		catch (Exception $ex)
		{
			$statusCode = 400;
			$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
			return $json;
		}

	}

}
