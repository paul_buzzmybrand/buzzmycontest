<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;
class ContestController extends BaseController {


	public function fetchContestList($id)
	{
		
		//return Company::with('contests')->find($id);
		try
		{
							
			$contestList = Contest::with('company')->with('rewards')->where('id', $id)->first();//findOrFail($id);
			
			$contestWithApprOfCont = $contestList->contest_approval;
			$contestType = $contestList->type_id;
			Log::debug("Contest with approval of contents= ".$contestWithApprOfCont);
			
			$contestIsJoin = Contest::where('id',$id)->whereHas('users', function($q)
			{
				$ownerId = ResourceServer::getOwnerId();
				
			   $q->where('user_id', $ownerId);
			})->get();
			
			if ($contestType == 1) //method regarding VIDEOCONTEST
			{
				//$contestVideos = Contest::with('videos')->where('id',$id)->get()->toArray();
				
				$contestVideos = Video::where('contest_id', $id)->orderBy('created_at','desc')->get()->toArray();
				
				//Log::debug($contestVideos);
				
				$contestJMTScore = 0;
				$i = 0;
				$contestVideosArray = array();
				
				if ($contestWithApprOfCont)
				{
					
					
					foreach($contestVideos as $videos)
					{
					
						if (($videos['approval_step_id']=='3') && ($videos['uploadOnFacebookDone']=='1'))
						{
							$contestJMTScore = $videos['score'] + $contestJMTScore;
							$contestVideosArray[$i]['id'] = $videos['id'];
							$contestVideosArray[$i]['name'] = $videos['name'];
							$contestVideosArray[$i]['duration'] = $videos['duration'];
							$contestVideosArray[$i]['image'] = $videos['image'];
							$contestVideosArray[$i]['filename'] = $videos['filename'];
							$contestVideosArray[$i]['score'] = $videos['score'];
							$contestVideosArray[$i]['created_at'] = $videos['created_at'];
							$contestVideosArray[$i]['user_id'] = $videos['user_id'];
							$location = DB::table('locations')->where('id', $videos['location_id'])->first();						
							$locationName = $location->name;
							$contestVideosArray[$i]['location'] = $locationName;
							$contestVideosArray[$i]['sex_child'] = $videos['info_1'];
							$contestVideosArray[$i]['age_child'] = $videos['info_2'];
							$contestVideosArray[$i]['character_child'] = $videos['info_3'];
							$i++;
						}
						
						
					}
					
					
					
				}
				else
				{
					foreach($contestVideos as $videos)
					{
						$contestJMTScore = $videos['score'] + $contestJMTScore;
						$contestVideosArray[$i]['id'] = $videos['id'];
						$contestVideosArray[$i]['name'] = $videos['name'];
						$contestVideosArray[$i]['duration'] = $videos['duration'];
						$contestVideosArray[$i]['image'] = $videos['image'];
						$contestVideosArray[$i]['filename'] = $videos['filename'];
						$contestVideosArray[$i]['score'] = $videos['score'];
						$contestVideosArray[$i]['created_at'] = $videos['created_at'];
						$contestVideosArray[$i]['user_id'] = $videos['user_id'];
						$location = DB::table('locations')->where('id', $videos['location_id'])->first();						
						$locationName = $location->name;
						$contestVideosArray[$i]['location'] = $locationName;
						$contestVideosArray[$i]['sex_child'] = $videos['info_1'];
						$contestVideosArray[$i]['age_child'] = $videos['info_2'];
						$contestVideosArray[$i]['character_child'] = $videos['info_3'];
						$i++;
					}
					
				}
				
				$ownerId = ResourceServer::getOwnerId();
				//return $ownerId;
				if ( $contestIsJoin->count())
				{
					$userIsJoin = TRUE;
				}
				else
				{
					$userIsJoin = FALSE;
				}
				$statusCode = 200;
				$contestArray=$contestList->toArray();
				$contestArray['videos']=$contestVideosArray;
				$contestArray['contestJMTScore']=$contestJMTScore;
				$contestArray['userIsJoin']=$userIsJoin;
			}
			else if ($contestType == 2) //method regarding PHOTOCONTEST
			{
				//$contestPhotos = Contest::with('photos')->where('id',$id)->get()->toArray();
				
				$contestPhotos = Photo::where('contest_id', $id)->orderBy('created_at', 'desc')->get()->toArray();
				
				Log::debug($contestPhotos);
				
				$contestJMTScore = 0;
				$i = 0;
				$contestPhotosArray = array();
				
				if ($contestWithApprOfCont)
				{
					
					
					foreach($contestPhotos as $photos)
					{
					
						if (($photos['approval_step_id']=='3') && ($photos['uploadOnFacebookDone']=='1'))
						{
							$contestJMTScore = $photos['score'] + $contestJMTScore;
							$contestPhotosArray[$i]['id'] = $photos['id'];
							$contestPhotosArray[$i]['name'] = $photos['name'];
							$contestPhotosArray[$i]['image'] = $photos['image'];
							$contestPhotosArray[$i]['filename'] = $photos['filename'];
							$contestPhotosArray[$i]['fb_source_url'] = $photos['fb_source_url'];
							$contestPhotosArray[$i]['score'] = $photos['score'];
							$contestPhotosArray[$i]['created_at'] = $photos['created_at'];
							$contestPhotosArray[$i]['user_id'] = $photos['user_id'];
							$location = DB::table('locations')->where('id', $photos['location_id'])->first();						
							$locationName = $location->name;
							$contestPhotosArray[$i]['location'] = $locationName;
							$i++;
						}
						
						
					}
					
					
					
				}
				else
				{
					foreach($contestPhotos as $photos)
					{
						$contestJMTScore = $photos['score'] + $contestJMTScore;
						$contestPhotosArray[$i]['id'] = $photos['id'];
						$contestPhotosArray[$i]['name'] = $photos['name'];
						$contestPhotosArray[$i]['image'] = $photos['image'];
						$contestPhotosArray[$i]['filename'] = $photos['filename'];
						$contestPhotosArray[$i]['fb_source_url'] = $photos['fb_source_url'];
						$contestPhotosArray[$i]['score'] = $photos['score'];
						$contestPhotosArray[$i]['created_at'] = $photos['created_at'];
						$contestPhotosArray[$i]['user_id'] = $photos['user_id'];
						$location = DB::table('locations')->where('id', $photos['location_id'])->first();						
						$locationName = $location->name;
						$contestPhotosArray[$i]['location'] = $locationName;
						$i++;
					}
					
				}
				
				$ownerId = ResourceServer::getOwnerId();
				//return $ownerId;
				if ( $contestIsJoin->count())
				{
					$userIsJoin = TRUE;
				}
				else
				{
					$userIsJoin = FALSE;
				}
				$statusCode = 200;
				$contestArray=$contestList->toArray();
				$contestArray['photos']=$contestPhotosArray;
				$contestArray['contestJMTScore']=$contestJMTScore;
				$contestArray['userIsJoin']=$userIsJoin;

			}       
			
			
			Log::debug("Result for Contest detail API: ".print_r($contestArray,true));
			
			
			$response = Response::json($contestArray, $statusCode);    
			return $response;
			
		}
		catch(ModelNotFoundException $e)
		{
			$statusCode = 200;
			$description = 'Event Not Found';
			$returnResponse = array('description'=>$description,'errorCode'=>404);
			$response = Response::json($returnResponse,$statusCode);
			return $response;
		}
		
	
	
	
	
	
	
	
	
	
	
	
	
	
		/*
		try
		{
			Log::debug("Welcome to the service without Access Token for fetching the single contest Samsung!");
			$queryBuilder = null;
		
			$queryBuilder = Contest::select("contests.*")->with("Company")->where("contests.id",$id)
							->join("companys","companys.id", "=", "contests.company_id")
							->join("locations","locations.id", "=", "contests.location_id")
							->where("companys.type_id",Company::TYPE_EXTERNAL);
			
			$queries = DB::getQueryLog();
			Log::debug("Queries: ".print_r($queries,true));
			
			$contestList = $queryBuilder->paginate(1);
			$statusCode = 200;
			$response = Response::make($contestList,$statusCode);
			return $response;
		}
		catch (Exception $ex)
		{
			$statusCode = 400;
			$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
			return $json;
		}
		*/
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$id = 26;
		
		$limit = Input::get('limit');
		
		
		//return belonging company
		$companyID = Input::get('companyID');
		
		$externalContest = CommonHelper::mandatoryInput("External_contest");
		$latitude = CommonHelper::optionalInput("Latitude");
		$longitude = CommonHelper::optionalInput("Longitude");
		
		$contestListTemp = Contest::with('company')->with('rewards')->with('location')->where('company_id', $companyID);
		
				
		$contestList = $contestListTemp->paginate($limit)->toArray();
		/*
		$i=0;
		$contestRewardsArray = array();
		foreach($contestList['data']['rewards'] as $reward)
		{
			$contestRewardsArray[$i]['title'] = reward['title'];
			$contestRewardsArray[$i]['description'] = reward['description'];
			$contestRewardsArray[$i]['thumbnail'] = reward['thumbnail'];
			$contestRewardsArray[$i]['rank'] = reward['rank'];
			$i++;
		}
		*/
		$i = 0;
		$contestListArray = array();
		$contestListArray['total'] = $contestList['total'];
		$contestListArray['per_page'] = $contestList['per_page'];
		$contestListArray['current_page'] = $contestList['current_page'];
		$contestListArray['last_page'] = $contestList['last_page'];
		$contestListArray['from'] = $contestList['from'];
		$contestListArray['to'] = $contestList['to'];	

		$contestArray = array();
		foreach($contestList['data'] as $contest)
		{
			$contestArray[$i]['id'] = $contest['id'];
			$contestArray[$i]['name'] = $contest['name'];
			$contestArray[$i]['start_time'] = $contest['start_time'];
			$contestArray[$i]['end_time'] = $contest['end_time'];				
			$contestArray[$i]['concept'] = $contest['concept'];
			$contestArray[$i]['type_id'] = $contest['type_id'];
			$contestArray[$i]['image'] = $contest['image'];
			$location = DB::table('locations')->where('id', $contest['location_id'])->pluck('name');
			$contestArray[$i]['location'] = $location;
			
			$j = 0;
			$contestRewardsArray = array();
			foreach($contest['rewards'] as $reward)
			{
				$contestRewardsArray[$j]['title'] = $reward['title'];
				$contestRewardsArray[$j]['description'] = $reward['description'];
				$contestRewardsArray[$j]['thumbnail'] = $reward['thumbnail'];
				$contestRewardsArray[$j]['rank'] = $reward['rank'];
				$j++;
			}
			
			$contestArray[$i]['rewards'] = $contestRewardsArray;
			
			$i++;
		}
		$contestListArray['data'] = $contestArray;
		
		$statusCode = 200;
		$response = Response::json($contestListArray, $statusCode);    
		return $response;
		
		
		$queries = DB::getQueryLog();
		Log::debug("Queries: ".print_r($queries,true));
		Log::debug("Number of contests returned: ".count($contestList));
		$statusCode = 200;
		$response = Response::make($contestList,$statusCode);
		return $response;
		
		try
		{
							
			$contestList = Contest::with('company')->with('rewards')->where('id', $id)->first();//findOrFail($id);
			
			$contestWithApprOfCont = $contestList->contest_approval;
			$contestType = $contestList->type_id;
			Log::debug("Contest with approval of contents= ".$contestWithApprOfCont);
			
			$contestIsJoin = Contest::where('id',$id)->whereHas('users', function($q)
			{
				$ownerId = ResourceServer::getOwnerId();
				
			   $q->where('user_id', $ownerId);
			})->get();
			
			if ($contestType == 1) //method regarding VIDEOCONTEST
			{
				//$contestVideos = Contest::with('videos')->where('id',$id)->get()->toArray();
				
				$contestVideos = Video::where('contest_id', $id)->orderBy('created_at','desc')->get()->toArray();
				
				//Log::debug($contestVideos);
				
				$contestJMTScore = 0;
				$i = 0;
				$contestVideosArray = array();
				
				if ($contestWithApprOfCont)
				{
					
					
					foreach($contestVideos as $videos)
					{
					
						if (($videos['approval_step_id']=='3') && ($videos['uploadOnFacebookDone']=='1'))
						{
							$contestJMTScore = $videos['score'] + $contestJMTScore;
							$contestVideosArray[$i]['id'] = $videos['id'];
							$contestVideosArray[$i]['name'] = $videos['name'];
							$contestVideosArray[$i]['duration'] = $videos['duration'];
							$contestVideosArray[$i]['image'] = $videos['image'];
							$contestVideosArray[$i]['filename'] = $videos['filename'];
							$contestVideosArray[$i]['score'] = $videos['score'];
							$contestVideosArray[$i]['created_at'] = $videos['created_at'];
							$contestVideosArray[$i]['user_id'] = $videos['user_id'];
							$location = DB::table('locations')->where('id', $videos['location_id'])->first();						
							$locationName = $location->name;
							$contestVideosArray[$i]['location'] = $locationName;
							$i++;
						}
						
						
					}
					
					
					
				}
				else
				{
					foreach($contestVideos as $videos)
					{
						$contestJMTScore = $videos['score'] + $contestJMTScore;
						$contestVideosArray[$i]['id'] = $videos['id'];
						$contestVideosArray[$i]['name'] = $videos['name'];
						$contestVideosArray[$i]['duration'] = $videos['duration'];
						$contestVideosArray[$i]['image'] = $videos['image'];
						$contestVideosArray[$i]['filename'] = $videos['filename'];
						$contestVideosArray[$i]['score'] = $videos['score'];
						$contestVideosArray[$i]['created_at'] = $videos['created_at'];
						$contestVideosArray[$i]['user_id'] = $videos['user_id'];
						$location = DB::table('locations')->where('id', $videos['location_id'])->first();						
						$locationName = $location->name;
						$contestVideosArray[$i]['location'] = $locationName;
						$i++;
					}
					
				}
				
				$ownerId = ResourceServer::getOwnerId();
				//return $ownerId;
				if ( $contestIsJoin->count())
				{
					$userIsJoin = TRUE;
				}
				else
				{
					$userIsJoin = FALSE;
				}
				$statusCode = 200;
				$contestArray=$contestList->toArray();
				$contestArray['videos']=$contestVideosArray;
				$contestArray['contestJMTScore']=$contestJMTScore;
				$contestArray['userIsJoin']=$userIsJoin;
			}
			else if ($contestType == 2) //method regarding PHOTOCONTEST
			{
				//$contestPhotos = Contest::with('photos')->where('id',$id)->get()->toArray();
				
				$contestPhotos = Photo::where('contest_id', $id)->orderBy('created_at', 'desc')->get()->toArray();
				
				Log::debug($contestPhotos);
				
				$contestJMTScore = 0;
				$i = 0;
				$contestPhotosArray = array();
				
				if ($contestWithApprOfCont)
				{
					
					
					foreach($contestPhotos as $photos)
					{
					
						if (($photos['approval_step_id']=='3') && ($photos['uploadOnFacebookDone']=='1'))
						{
							$contestJMTScore = $photos['score'] + $contestJMTScore;
							$contestPhotosArray[$i]['id'] = $photos['id'];
							$contestPhotosArray[$i]['name'] = $photos['name'];
							$contestPhotosArray[$i]['image'] = $photos['image'];
							$contestPhotosArray[$i]['filename'] = $photos['filename'];
							$contestPhotosArray[$i]['fb_source_url'] = $photos['fb_source_url'];
							$contestPhotosArray[$i]['score'] = $photos['score'];
							$contestPhotosArray[$i]['created_at'] = $photos['created_at'];
							$contestPhotosArray[$i]['user_id'] = $photos['user_id'];
							$location = DB::table('locations')->where('id', $photos['location_id'])->first();						
							$locationName = $location->name;
							$contestPhotosArray[$i]['location'] = $locationName;
							$i++;
						}
						
						
					}
					
					
					
				}
				else
				{
					foreach($contestPhotos as $photos)
					{
						$contestJMTScore = $photos['score'] + $contestJMTScore;
						$contestPhotosArray[$i]['id'] = $photos['id'];
						$contestPhotosArray[$i]['name'] = $photos['name'];
						$contestPhotosArray[$i]['image'] = $photos['image'];
						$contestPhotosArray[$i]['filename'] = $photos['filename'];
						$contestPhotosArray[$i]['fb_source_url'] = $photos['fb_source_url'];
						$contestPhotosArray[$i]['score'] = $photos['score'];
						$contestPhotosArray[$i]['created_at'] = $photos['created_at'];
						$contestPhotosArray[$i]['user_id'] = $photos['user_id'];
						$location = DB::table('locations')->where('id', $photos['location_id'])->first();						
						$locationName = $location->name;
						$contestPhotosArray[$i]['location'] = $locationName;
						$i++;
					}
					
				}
				
				$ownerId = ResourceServer::getOwnerId();
				//return $ownerId;
				if ( $contestIsJoin->count())
				{
					$userIsJoin = TRUE;
				}
				else
				{
					$userIsJoin = FALSE;
				}
				$statusCode = 200;
				$contestArray=$contestList->toArray();
				$contestArray['photos']=$contestPhotosArray;
				$contestArray['contestJMTScore']=$contestJMTScore;
				$contestArray['userIsJoin']=$userIsJoin;

			}       
			
			
			Log::debug("Result for Contest detail API: ".print_r($contestArray,true));
			
			
			$response = Response::json($contestArray, $statusCode);    
			return $response;
			
		}
		catch(ModelNotFoundException $e)
		{
			$statusCode = 200;
			$description = 'Event Not Found';
			$returnResponse = array('description'=>$description,'errorCode'=>404);
			$response = Response::json($returnResponse,$statusCode);
			return $response;
		}
		
		
		
		
		
	
	
	
	
	
		/*
		try
		{
			//$location = Input::get('location');
			//$name = Input::get('name');
			$locationID = Input::get('locationID');
			//Reward is commented out because this column is deleted
			//$reward = Input::get('reward');
			
			//return $location;
			$limit = Input::get('limit');
			
			//return belonging company
			$companyID = Input::get('companyID');
			
			$externalContest = CommonHelper::mandatoryInput("External_contest");
			$latitude = CommonHelper::optionalInput("Latitude");
			$longitude = CommonHelper::optionalInput("Longitude");
			
			if ((isset($latitude) && !isset($longitude)) || (!isset($latitude) && isset($longitude)))
			{
				throw new MissingMandatoryParametersException("You need to set both Latitude and Longitude parameters or none of them");
			}
			
			$queryBuilder = null;
			if ($externalContest == "false")
			{
				$queryBuilder = Contest::with('rewards')->with('company')			
					->join("companys","companys.id", "=", "contests.company_id")						
					->join("locations","locations.id", "=", "contests.location_id")
					->where('companys.id', $companyID)
					->where("companys.type_id",Company::TYPE_INTERNAL);
					
				Log::debug("Response: ".print_r($queryBuilder->toArray(),true));
					
				//Order by distance if available
				if (isset($latitude))
				{
					$queryBuilder = $queryBuilder->orderBy(DB::raw("ACOS(COS(RADIANS(90-".$latitude."))*COS(RADIANS(90-latitude))+SIN(RADIANS(90-".$latitude."))*SIN(RADIANS(90-latitude))*COS(RADIANS(".$longitude."-longitude)))*6371"));
				}
				else if (isset($locationID))
				{
					$queryBuilder = $queryBuilder->where("contests.location_id", $locationID);
				}	
				
				
			}
			else if ($externalContest == "true")
			{
				$companyId = CommonHelper::mandatoryInput("Company_ID");
				$queryBuilder = Contest::select("contests.*")->with("Company")
					->where("company_id",$companyId)
					->join("companys","companys.id", "=", "contests.company_id")
					->join("locations","locations.id", "=", "contests.location_id")
					->where("companys.type_id",Company::TYPE_EXTERNAL);
			}
			else
			{
				throw new MissingMandatoryParametersException("External_contest=".$externalContest.", should be either true or false");
			}

			
			
			
			
			
			if (isset($limit)) {
				$contestList = $queryBuilder->orderBy('start_time','desc')->paginate($limit);
			} else {
				$contestList = $queryBuilder->orderBy('start_time','desc')->get();
			}
			
			$queries = DB::getQueryLog();
			Log::debug("Response: ".print_r($contestList,true));
			Log::debug("Queries: ".print_r($queries,true));
			Log::debug("Number of contests returned: ".count($contestList));
			$statusCode = 200;
			$response = Response::make($contestList,$statusCode);
			return $response;
		}
		catch(MissingMandatoryParametersException $e)
		{
			$statusCode = 400;
			Log::error($e);
			$json = Response::json(array("error_message" => $e->getMessage()),$statusCode);
			return $json;
		}
   
            */
            //return Contest::with('company','videos')->orderBy('created_at','ASC')->paginate($limit);
	}
        
        public function search()
	{
            $location = Input::get('location');
            $name = Input::get('name');
            $reward = Input::get('reward');
            
            //return $location;
            $limit = Input::get('limit');
            if(!isset($limit))
            {
                $limit = 10;
            }
          try
            {
                
                //$contestList = Contest::with('company','video_contests.videos')->where('name','like',$name.'%')->orWhere('location','like',$location.'%')->paginate($limit);
                $contestList = Contest::with('company','video_contests.videos')->where(function($query) use ($name, $location, $reward, $limit)
                {
                    $query->where('name', 'like', $name.'%')
                          ->where('location', 'like', $location.'%')
                          ->where('prize_money','like',$reward.'%');
                })->paginate($limit);
                return $contestList;
            }
            catch(ModelNotFoundException $e)
            {
                $errorArray=array(
                    'Error' => '404',
                    'Message' => 'Not Found'
                );
                return $errorArray;
            } 
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('contests.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
            //return Company::with('contests')->find($id);
            try
            {
								
                $contestList = Contest::with('fbPages')->with('rewards')->where('id', $id)->first();//findOrFail($id);
				
				$contestWithApprOfCont = $contestList->contest_approval;
				$contestType = $contestList->type_id;
				Log::debug("Contest with approval of contents= ".$contestWithApprOfCont);
				
                $contestIsJoin = Contest::where('id',$id)->whereHas('users', function($q)
                {
                    $ownerId = ResourceServer::getOwnerId();
                    
                   $q->where('user_id', $ownerId);
                })->get();
				
				if ($contestType == 1) //method regarding VIDEOCONTEST
				{
					//$contestVideos = Contest::with('videos')->where('id',$id)->get()->toArray();
					
					$contestVideos = Video::where('contest_id', $id)->orderBy('rank_position','asc')->get()->toArray();
					
					//Log::debug($contestVideos);
					
					$contestJMTScore = 0;
					$i = 0;
					$contestVideosArray = array();
					
					if ($contestWithApprOfCont)
					{
						
						
						foreach($contestVideos as $videos)
						{
						
							if (($videos['approval_step_id']=='3') && ($videos['uploadOnFacebookDone']=='1'))
							{
								$contestJMTScore = $videos['score'] + $contestJMTScore;
								$contestVideosArray[$i]['id'] = $videos['id'];
								$contestVideosArray[$i]['name'] = $videos['name'];
								$contestVideosArray[$i]['duration'] = $videos['duration'];
								$contestVideosArray[$i]['image'] = $videos['image'];
								$contestVideosArray[$i]['filename'] = $videos['filename'];
								$contestVideosArray[$i]['score'] = $videos['score'];
								$contestVideosArray[$i]['rank'] = $videos['rank_position'];
								$contestVideosArray[$i]['created_at'] = $videos['created_at'];
								$contestVideosArray[$i]['user_id'] = $videos['user_id'];
								$location = DB::table('locations')->where('id', $videos['location_id'])->first();						
								$locationName = $location->name;
								$contestVideosArray[$i]['location'] = $locationName;
								$i++;
							}
							
							
						}
						
						
						
					}
					else
					{
						foreach($contestVideos as $videos)
						{
							$contestJMTScore = $videos['score'] + $contestJMTScore;
							$contestVideosArray[$i]['id'] = $videos['id'];
							$contestVideosArray[$i]['name'] = $videos['name'];
							$contestVideosArray[$i]['duration'] = $videos['duration'];
							$contestVideosArray[$i]['image'] = $videos['image'];
							$contestVideosArray[$i]['filename'] = $videos['filename'];
							$contestVideosArray[$i]['score'] = $videos['score'];
							$contestVideosArray[$i]['rank'] = $videos['rank_position'];
							$contestVideosArray[$i]['created_at'] = $videos['created_at'];
							$contestVideosArray[$i]['user_id'] = $videos['user_id'];
							$location = DB::table('locations')->where('id', $videos['location_id'])->first();						
							$locationName = $location->name;
							$contestVideosArray[$i]['location'] = $locationName;
							$i++;
						}
						
					}
					
					$ownerId = ResourceServer::getOwnerId();
					//return $ownerId;
					if ( $contestIsJoin->count())
					{
						$userIsJoin = TRUE;
					}
					else
					{
						$userIsJoin = FALSE;
					}
					$statusCode = 200;
					$contestArray=$contestList->toArray();
					$contestArray['videos']=$contestVideosArray;
					$contestArray['contestJMTScore']=$contestJMTScore;
					$contestArray['userIsJoin']=$userIsJoin;
				}
				else if ($contestType == 2) //method regarding PHOTOCONTEST
				{
					//$contestPhotos = Contest::with('photos')->where('id',$id)->get()->toArray();
					
					$contestPhotos = Photo::where('contest_id', $id)->orderBy('rank_position', 'asc')->get()->toArray();
					
					Log::debug($contestPhotos);
					
					$contestJMTScore = 0;
					$i = 0;
					$contestPhotosArray = array();
					
					if ($contestWithApprOfCont)
					{
						
						
						foreach($contestPhotos as $photos)
						{
						
							if (($photos['approval_step_id']=='3') && ($photos['uploadOnFacebookDone']=='1'))
							{
								$contestJMTScore = $photos['score'] + $contestJMTScore;
								$contestPhotosArray[$i]['id'] = $photos['id'];
								$contestPhotosArray[$i]['name'] = $photos['name'];
								$contestPhotosArray[$i]['image'] = $photos['image'];
								$contestPhotosArray[$i]['filename'] = $photos['filename'];
								$contestPhotosArray[$i]['fb_source_url'] = $photos['fb_source_url'];
								$contestPhotosArray[$i]['score'] = $photos['score'];
								$contestPhotosArray[$i]['rank'] = $photos['rank_position'];
								$contestPhotosArray[$i]['created_at'] = $photos['created_at'];
								$contestPhotosArray[$i]['user_id'] = $photos['user_id'];
								$location = DB::table('locations')->where('id', $photos['location_id'])->first();						
								$locationName = $location->name;
								$contestPhotosArray[$i]['location'] = $locationName;
								$i++;
							}
							
							
						}
						
						
						
					}
					else
					{
						foreach($contestPhotos as $photos)
						{
							$contestJMTScore = $photos['score'] + $contestJMTScore;
							$contestPhotosArray[$i]['id'] = $photos['id'];
							$contestPhotosArray[$i]['name'] = $photos['name'];
							$contestPhotosArray[$i]['image'] = $photos['image'];
							$contestPhotosArray[$i]['filename'] = $photos['filename'];
							$contestPhotosArray[$i]['fb_source_url'] = $photos['fb_source_url'];
							$contestPhotosArray[$i]['score'] = $photos['score'];
							$contestPhotosArray[$i]['rank'] = $photos['rank_position'];
							$contestPhotosArray[$i]['created_at'] = $photos['created_at'];
							$contestPhotosArray[$i]['user_id'] = $photos['user_id'];
							$location = DB::table('locations')->where('id', $photos['location_id'])->first();						
							$locationName = $location->name;
							$contestPhotosArray[$i]['location'] = $locationName;
							$i++;
						}
						
					}
					
					$ownerId = ResourceServer::getOwnerId();
					//return $ownerId;
					if ( $contestIsJoin->count())
					{
						$userIsJoin = TRUE;
					}
					else
					{
						$userIsJoin = FALSE;
					}
					$statusCode = 200;
					$contestArray=$contestList->toArray();
					$contestArray['photos']=$contestPhotosArray;
					$contestArray['contestJMTScore']=$contestJMTScore;
					$contestArray['userIsJoin']=$userIsJoin;

				}       
				
                
                Log::debug("Result for Contest detail API: ".print_r($contestArray,true));
                
                
                $response = Response::json($contestArray, $statusCode);    
                return $response;
				
            }
            catch(ModelNotFoundException $e)
            {
                $statusCode = 200;
                $description = 'Event Not Found';
                $returnResponse = array('description'=>$description,'errorCode'=>404);
                $response = Response::json($returnResponse,$statusCode);
                return $response;
            }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('contests.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//Company::with('contests')->find($id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
        public function eventList()
		{
			try
			{
				$externalContest = CommonHelper::mandatoryInput("External_contest");
				 //$queryBuilder = Contest::all();
				$queryBuilder = null;
				/*$queryBuilder = Contest::whereHas('users', function($q)
				{
	                $ownerId = ResourceServer::getOwnerId();
					$q->where('user_id', $ownerId);					
				});*/
				if ($externalContest == "false")
				{
				
				/*Contest::select("contests.*")->with('company','videos')->where('contests.name','like',$name.'%')->where('location','like',$location.'%')->where('typology_rewards','like',$reward.'%')
	                	->join("companys","companys.id", "=", "contests.company_id")->where("companys.type_id",Company::TYPE_INTERNAL)
	                	->join("locations","locations.id", "=", "contests.location_id");*/
				
				
					$queryBuilder = Contest::select("contests.*")
						->join("companys","companys.id", "=", "contests.company_id")
						->where("companys.type_id",Company::TYPE_INTERNAL)
						->join("locations","locations.id", "=", "contests.location_id");
				}
				else if ($externalContest == "true")
				{
				
					$companyId = CommonHelper::mandatoryInput("Company_ID");
					
					
					$queryBuilder = Contest::select("contests.*")->with("Company")
						->where("company_id",$companyId)
	            		->join("companys","companys.id", "=", "contests.company_id")
	                	->join("locations","locations.id", "=", "contests.location_id")
	            		->where("companys.type_id",Company::TYPE_EXTERNAL);
					
					
					/*$queryBuilder = Contest::select("contests.*")
						->where("company_id",$companyId)
						->join("companys","companys.id", "=", "contests.company_id")
						->where("companys.type_id",Company::TYPE_EXTERNAL)
						->join("locations","locations.id", "=", "contests.location_id");*/
				}
				else
				{
					throw new MissingMandatoryParametersException("External_contest=".$externalContest.", should be either true or false");
				}
				
				$userWhatList = $queryBuilder->get()->toArray();
				Log::debug($userWhatList);
	            //$returnResponse = array("contests" => $userWhatList, "errorCode" => 200);
				//$response = Response::json($returnResponse,200);
	            //return $response;
				return $userWhatList;
			}
			catch(MissingMandatoryParametersException $e)
			{
				Log::error($e);
				return Response::json(array("error_message" => $e->getMessage()), 400);
			}
        }
        
        public function rewardList()
	{
            return Reward::all(['reward_type']);
	}
        
        public function join($id)
	{
			try
			{
				Log::debug("Join Function from controller");
				$ownerId = ResourceServer::getOwnerId();
				
				
				// ATTACH IF ROW DOESN' T EXIST
				$contest = Contest::with(
								array(
									'users' => function($query) use ($ownerId) {
													$query->where('user_id', '=', $ownerId);
												}
									)
							)->where('id', $id)->first();
				
				if ( $contest->users->count() == 0 ) {
					$contest->users->attach($ownerId);
				}
				//
				//Contest::find($id)->users()->attach($ownerId);
				
				
				$statusCode=200;
				$description='User Successfully joined Contest';
				$returnResponse = array('description'=>$description,'errorCode'=>200);
				$response = Response::json($returnResponse,$statusCode);
				return $response;
			}
			catch (Exception $ex)
			{
				$statusCode = 400;
				$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
				return $json;
			}
            
	}
       
        /*********  Controller method for Contest *******/
        /* web part*/
        /* web part*/
        /* web part*/
        
        public function postEvent()
        {
            $rules = array(
                'city'=>'required|min:4',
                'contest_name'=>'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            
            if ($validator->fails()) {
                
                Session::flash('message1', 'Error! please enter mandatory fields to add event!');
                return Redirect::to('company/profile');
            }
            else 
            {
                $contest = new Contest;

                $image = Input::file('event_image');
                if($image)
                {
                    $image_name = $image -> getClientOriginalName();
                    $destinationPath = 'images/events';
                    $contest->image = $image_name;
                    $upload_appImage = Input::file('event_image')->move($destinationPath, $image_name);
                }
                $contest -> company_id = Input::get('id');
                $contest -> name = Input::get('contest_name');
                $contest -> location = Input::get('city');
                //$contest -> score = Input::get('score');

                $contest -> save();



                return Redirect::to('company/profile');
            }
        }
        
        public function deleteEvent($id)
        {
            $event = Contest::find($id);
            $event -> delete();
            
            return Redirect::to('company/profile');
        }
        
        public function editEvent($id)
	{
            $event = Contest::find($id);
            return Redirect::to('company');
        }
        
        
        public function eventEdit()
        {
            $rules = array(
                'event_name'=>'required',
                'city'=>'required'  ,
                'email'=>'required|email',
                'phone'=>'numeric'
            );
            
            $validator = Validator::make(Input::all(), $rules);
            
            if($validator->passes())
            {    
                $event_id = Input::get('event_id');
                $contest = Contest::find($event_id);
                
                $image = Input::file('app_image');
                if($image)
                {
                    $image_name = $image -> getClientOriginalName();
                    $destinationPath = 'images/events';
                    $upload_appImage = Input::file('app_image')->move($destinationPath, $image_name);
                
                    $contest -> image = $image_name;
                }
                
                $contest -> name = Input::get('event_name');
                
                $contest->location = Input::get('city');
                $contest->concept = Input::get('concept');
                $contest->contact_name = Input::get('contact_name');
                $contest->phone = Input::get('phone');
                $contest->email = Input::get('email');
                $contest->rules = Input::get('rules');
                $contest->judges = Input::get('judges');
                
                $contest-> reward1_type = Input::get('reward_type1');
                $contest-> reward2_type = Input::get('reward_type2');
                $contest-> reward3_type = Input::get('reward_type3');
                
                $contest-> reward1_desc = Input::get('reward1');
                $contest-> reward2_desc = Input::get('reward2');
                $contest-> reward3_desc = Input::get('reward3');
                
                $contest-> save();
                
                Session::flash('message1', 'Event record successfully updated');
                return Redirect::to('company/profile');
                
            }
            else
            {
                Session::flash('message1', 'Error! please enter mandatory fields to edit event details!');
                return Redirect::to('company/profile');
            }
        }
        
        public function showContestPage()
        {
            $contests = Contest::all();
            $companys = Company::all();
            $reward_types = DB::table('reward_types')->get();
            //$contests = Company::with('contest')->all();
            //return $contests;
            if(Session::has('username'))
            {
                return View::make('contest')->with('contests',$contests)
                    ->with('reward_types',$reward_types)
                    ->with('companys',$companys);
            }
            else
            {
                return Redirect::to('admin');
            }
        }
        
        public function registerContest()
        {
           $rules = array(
                'event_name'=>'required',
                'companyName'=>'required',
                'city'=>'required',
                'email'=> 'required|email',
                'judges'=>'required',
                'reward_type1'=>'required',
                'rules'=>'required',
            );
            
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()) {
                return Redirect::to('contest')->with('ErrorMessage','Registration is failed due to:')->withErrors($validator)->withInput();
            }
            else
            {
                $contest = new Contest;
                
                $contest->company_id = Input::get('companyName');
                $contest->name = Input::get('event_name');
                $contest->location = Input::get('city');
                $contest->concept = Input::get('concept');
                $contest->contact_name = Input::get('contact_name');
               // $company->zip = Input::get('zip');
                $contest->email = Input::get('email');
                $contest->phone = Input::get('phone');
                $contest->judges = Input::get('judges');
                
                $contest->reward1_type = Input::get('reward_type1');
                $contest->reward2_type = Input::get('reward_type2');
                $contest->reward3_type = Input::get('reward_type3');
                
                $contest->reward1_desc = Input::get('reward1');
                $contest->reward2_desc = Input::get('reward2');
                $contest->reward3_desc = Input::get('reward3');
                
                $contest->rules = Input::get('rules');
                
                $image = Input::file('eventImage');
                if($image)
                {
                    $image_name = $image -> getClientOriginalName();
                    $destinationPath = 'images/events';
                    $contest->image = $image_name;
                    $upload_appImage = Input::file('eventImage')->move($destinationPath, $image_name);
                }
                else
                {
                    $contest->image = 'default.png';
                }
               
                $result = $contest->save();
                
                return Redirect::to('contest')->with('ErrorMessage', 'Registered successfully!');
            }
        }
        
        public function editContest()
        {
            $rules = array(
                'event_name'=>'required',
                'companyName'=>'required',
                'city'=>'required',
                'email'=> 'required|email',
                'judges'=>'required',
                'reward_type1'=>'required',
                'rules'=>'required',
            );
            
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()) {
                return Redirect::to('contest')->with('ErrorMessage','Registration is failed due to:')->withErrors($validator)->withInput();
            }
            else
            {
                $contestId = Input::get('eventid');
                
                $contest = Contest::find($contestId);
                
                $contest->company_id = Input::get('companyName');
                $contest->name = Input::get('event_name');
                $contest->location = Input::get('city');
                $contest->concept = Input::get('concept');
                $contest->contact_name = Input::get('contact_name');
               // $company->zip = Input::get('zip');
                $contest->email = Input::get('email');
                $contest->phone = Input::get('phone');
                $contest->judges = Input::get('judges');
                
                $contest->reward1_type = Input::get('reward_type1');
                $contest->reward2_type = Input::get('reward_type2');
                $contest->reward3_type = Input::get('reward_type3');
                
                $contest->reward1_desc = Input::get('reward1');
                $contest->reward2_desc = Input::get('reward2');
                $contest->reward3_desc = Input::get('reward3');
                
                $contest->rules = Input::get('rules');
                
                $image = Input::file('image');
                if($image)
                {
                    $image_name = $image -> getClientOriginalName();
                    $destinationPath = 'images/events';
                    $contest->image = $image_name;
                    $upload_appImage = Input::file('image')->move($destinationPath, $image_name);
                }
               
                $result = $contest->save();
                
                return Redirect::to('contest')->with('ErrorMessage', 'Event details updated successfully!');
            }
            
        }
        public function deleteContest($id)
        {
            $event = Contest::find($id);
            $event -> delete();
            
            return Redirect::to('contest')->with('ErrorMessage', 'Data successfully deleted!');
        }

        public function getScheduled() {
            $contests = Contest::where('company_id', Auth::user()->company->id)->where('status', 3)->get()->toArray();
            
            return json_encode($contests);
        }

        public function switchScheduledContests() {
			
			$contestsToswitch = Contest::where('company_id', Auth::user()->company->id)->where('status', 3)->get();
			
			Log::debug("Contest to switch in draft mode: ".$contestsToswitch);
			
			foreach($contestsToswitch as $contest)
			{
				$invoiceTodelete = Invoice::where('contest_id', $contest->id)->delete();				
			}
			
            $contestsUpdate = Contest::where('company_id', Auth::user()->company->id)->where('status', 3)->update(['status' => 4, 'confirmed_by_customer' => 0]);

            return json_encode($contestsUpdate);
        }
}
