<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BMBWebSiteController extends BaseController {
        
	//

	public function testApprovationEmail($id) {
	
		$video = Video::find($id);
		$contest = $video->contest;
		$user = $video->user;
		
		try {
		
			Mail::send('emails.test-approvation', array('contest' => $contest, 'video' => $video), function($message) use ($user) {
				
				$message->to('giuseppes7@libero.it', $user->name . " " . $user->surname)
						->subject('Your Video Has been approved!');
			
			});
		
		} catch (Exception $ex) {
			Log::error('Problem sending email: '.$ex);
		}
	
	}
	
		
	public function getInTouch() {
	
		
		try {
			
			$fullname = Input::get('name');
			$email = Input::get('email');
			$lang = Input::get('language');
				
			$contact = new Contact();
			$contact->name = Input::get('name');
			$contact->email = Input::get('email');
			$contact->company = Input::get('company');
			$contact->telephone = Input::get('telephone');
			$contact->message = Input::get('message');
			$contact->newsletter = Input::get('newsletter');
			$contact->save();
			
			// REGISTER NEWSLETTER ON MAILCHIMP
			if ($lang == 'it')
				$list_id = 'c37ced4e70';
			else
				$list_id = '5b5711f60c';
			
			try
			{
				MailchimpWrapper::lists()->subscribe($list_id, array('email'=>$email),  array('FirstName' => $fullname, 'LastName' => ''));
				Log::debug("Mail aggiunta in lista");
			}	
			catch (Exception $ex)
			{
				Log::debug("Exception on subscribe mailchimp: ". $ex);
			}		
			
			Mail::send('emails.messageWebsite', array('email' => print_r($contact->email,true), 
														'name' => print_r($contact->name,true),
														'company' => print_r($contact->company,true),
														'telephone' => print_r($contact->telephone,true),
														'text' => print_r($contact->message,true)), function($message) {
				
				$message->to('vin@buzzmybrand.it', 'BuzzMyBrand')->cc('pao@buzzmybrand.it')
						->subject('Message From WebSite BuzzMyBrand web site');
			
			});
		
			return json_encode(array("result" => "1"));
	
		} catch (Exception $ex) {
		
			Log::debug("Exception on getInTouch: ". $ex);
			return json_encode(array("result"=> "0", "exception" => $ex));
		}
	
	}
	
	public function subscribe() {
	
		
			
			
		$lang = Input::get('language');
		
		try {
				
			//$list_id = Config::get('config/packages/hugofirth/mailchimp/config.listid');
			if ($lang == 'it')
				$list_id = 'c37ced4e70';
			else
				$list_id = '5b5711f60c';
			
			$email = Input::get('email');
			
			//$lists = MailchimpWrapper::lists()->getList()['data'];
			
			//Log::debug("lists: ".print_r($lists,true));
			//MailChimpWrapper::lists()->memberInfo($list_id, array('email' => $email));
		
			//Log::debug("Return: ".print_r($ret,true));
		
			MailchimpWrapper::lists()->subscribe($list_id, array('email'=>$email));
			
			$contact = new Contact();
			$contact->email = Input::get('email');
			$contact->newsletter = 1;
			$contact->save();
		
		} catch (Exception $ex){
		
			Log::debug('mailchimp subscribe: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
		
		return json_encode(array("result" => "1"));
			
	
		
	
	}

}