	<?php

class tab_HomeController extends Controller {

	public static function format_contest_date($locationId, $date){
		//App::setlocale('it');

		if ( $locationId == 2 ) {

			$date = explode('-', $date);
			switch ( $date[1] ) {
				case '01': return explode(' ', $date[2])[0] .' '.Lang::get('tab.january');
					break;
				case '02': return explode(' ', $date[2])[0] .' '.Lang::get('tab.february');
					break;
				case '03': return explode(' ', $date[2])[0] .' '.Lang::get('tab.march');
					break;
				case '04': return explode(' ', $date[2])[0] .' '.Lang::get('tab.april');
					break;
				case '05': return explode(' ', $date[2])[0] .' '.Lang::get('tab.may');
					break;
				case '06': return explode(' ', $date[2])[0] .' '.Lang::get('tab.june');
					break;
				case '07': return explode(' ', $date[2])[0] .' '.Lang::get('tab.july');
					break;
				case '08': return explode(' ', $date[2])[0] .' '.Lang::get('tab.august');
					break;
				case '09': return explode(' ', $date[2])[0] .' '.Lang::get('tab.september');
					break;
				case '10': return explode(' ', $date[2])[0] .' '.Lang::get('tab.october');
					break;
				case '11': return explode(' ', $date[2])[0] .' '.Lang::get('tab.november');
					break;
				case '12': return explode(' ', $date[2])[0] .' '.Lang::get('tab.december');
					break;
				default : return " / ";
					break;
			}

		} else if ( $locationId == 1 ) {

			$date = explode('-', $date);

			$final = '';

			if ( explode(' ', $date[2])[0] == '01' || explode(' ', $date[2])[0] == '1' ) {
				$final = 'st';
			} else if (  explode(' ', $date[2])[0] == '02' || explode(' ', $date[2])[0] == '2'  ) {
				$final = 'nd';
			} else if (  explode(' ', $date[2])[0] == '03' || explode(' ', $date[2])[0] == '3'  ) {
				$final = 'rd';
			} else {
				$final = 'th';
			}

			switch ( $date[1] ) {
				case '01':
					return Lang::get('tab.january') . ' ' . explode(' ', $date[2])[0] . '<span style="vertical-align:super; font-size:85%;">' . $final . '</span>';
					break;
				case '02': return Lang::get('tab.february') . ' ' . explode(' ', $date[2])[0] . '<span style="vertical-align:super; font-size:85%;">' . $final . '</span>';
					break;
				case '03': return Lang::get('tab.march') . ' ' . explode(' ', $date[2])[0] . '<span style="vertical-align:super; font-size:85%;">' . $final . '</span>';
					break;
				case '04': return Lang::get('tab.april') . ' ' . explode(' ', $date[2])[0] . '<span style="vertical-align:super; font-size:85%;">' . $final . '</span>';
					break;
				case '05': return Lang::get('tab.may') . ' ' . explode(' ', $date[2])[0] . '<span style="vertical-align:super; font-size:85%;">' . $final . '</span>';
					break;
				case '06': return Lang::get('tab.june') . ' ' . explode(' ', $date[2])[0] . '<span style="vertical-align:super; font-size:85%;">' . $final . '</span>';
					break;
				case '07': return Lang::get('tab.july') . ' ' . explode(' ', $date[2])[0] . '<span style="vertical-align:super; font-size:85%;">' . $final . '</span>';
					break;
				case '08': return Lang::get('tab.august') . ' ' . explode(' ', $date[2])[0] . '<span style="vertical-align:super; font-size:85%;">' . $final . '</span>';
					break;
				case '09': return Lang::get('tab.september') . ' ' . explode(' ', $date[2])[0] . '<span style="vertical-align:super; font-size:85%;">' . $final . '</span>';
					break;
				case '10': return Lang::get('tab.october') . ' ' . explode(' ', $date[2])[0] . '<span style="vertical-align:super; font-size:85%;">' . $final . '</span>';
					break;
				case '11': return Lang::get('tab.november') . ' ' . explode(' ', $date[2])[0] . '<span style="vertical-align:super; font-size:85%;">' . $final . '</span>';
					break;
				case '12': return Lang::get('tab.december') . ' ' . explode(' ', $date[2])[0] . '<span style="vertical-align:super; font-size:85%;">' . $final . '</span>';
					break;
				default : return " / ";
					break;
			}
		}

	}


	public function makeFacebookTab($id) {


		$contest_details = DB::table('fbpage_contest')
			->join("contests", "contests.id", "=", "fbpage_contest.contest_id")
			->where("fbpage_contest.fbpage_id", "=", $id)
			->orderBy('contests.id', 'desc')->first();

		//Log::debug("Contest details: ".print_r($contest_details, true));
		$contest = Contest::find($contest_details->id);
		Log::debug("Contest object: ".print_r($contest, true));


		$facebook = new Facebook\Facebook([
			'app_id' => $contest->company->fbApp_clientID,
			'app_secret' => $contest->company->fbApp_clientSecret,
			'default_graph_version' => 'v2.4',
		]);

		$page = fbPage::find($id);



		$admin_access_token = $page->fb_page_admin_at;


		//get PAGE ACCESS TOKEN
		$info = array("access_token" => $admin_access_token);
		$fb_request = $facebook->get('/me/accounts', $admin_access_token);

		Log::debug("Facebook response: ".print_r($fb_request->getBody(),true));
		//scorrere l'array finche non trovi la pagina giusta
		//$pages_array = $fb_request['data'];
		$pages_array = $fb_request->getGraphEdge();

		//$pages_array = $fb_request['data'];
		$page_access_token = null;
		foreach ($pages_array as $pagefb)
		{
			if ($pagefb['id'] == $page->fb_page_id)
			{
				$page_access_token = $pagefb['access_token'];
				break;
			}
		}

		//$contest = $page->contests->first();



		$data = array(
			"app_id" => $contest->company->fbApp_clientID,
			"access_token" => $page_access_token,
			"position" => 2,
			"custom_name" => strip_tags($contest->name));



		$response = $facebook->post("/".$page->fb_page_id."/tabs", $data);
		$graphNodeResponse = $response->getGraphNode();
		Log::debug("Facebook response for makeFacebookTab: ".print_r($graphNodeResponse, true));

	}

	public function deleteFacebookTab($id) {

		$contest_details = DB::table('fbpage_contest')
			->join("contests", "contests.id", "=", "fbpage_contest.contest_id")
			->where("fbpage_contest.fbpage_id", "=", $id)
			->orderBy('contests.id', 'desc')->first();


		$contest = Contest::find($contest_details->id);

		$facebook = new Facebook\Facebook([
			'app_id' => $contest->company->fbApp_clientID,
			'app_secret' => $contest->company->fbApp_clientSecret,
			'default_graph_version' => 'v2.4',
		]);

		$page = fbPage::find($id);


		$admin_access_token = $page->fb_page_admin_at;

		//get PAGE ACCESS TOKEN
		$info = array("access_token" => $admin_access_token);
		$fb_request = $facebook->get('/me/accounts', $admin_access_token);
		Log::debug("Facebook response: ".print_r($fb_request->getBody(),true));
		//scorrere l'array finche non trovi la pagina giusta
		//$pages_array = $fb_request['data'];
		$pages_array = $fb_request->getGraphEdge();
		$page_access_token = null;
		foreach ($pages_array as $pagefb)
		{
			if ($pagefb['id'] == $page->fb_page_id)
			{
				$page_access_token = $pagefb['access_token'];
				break;
			}
		}

		$data = array(
			"tab" => "app_".$contest->company->fbApp_clientID);

		$response = $facebook->delete("/".$page->fb_page_id."/tabs", $data, $page_access_token);
		$graphNodeResponse = $response->getGraphNode();
		Log::debug("Facebook response for deleteFacebookTab: ".print_r($graphNodeResponse, true));

	}

	public function updateFacebookTab($id) {

		$contest_details = DB::table('fbpage_contest')
			->join("contests", "contests.id", "=", "fbpage_contest.contest_id")
			->where("fbpage_contest.fbpage_id", "=", $id)
			->orderBy('contests.id', 'desc')->first();

		$contest = Contest::find($contest_details->id);

		$facebook = new Facebook\Facebook([
			'app_id' => $contest->company->fbApp_clientID,
			'app_secret' => $contest->company->fbApp_clientSecret,
			'default_graph_version' => 'v2.4',
		]);

		$page = fbPage::find($id);


		$admin_access_token = $page->fb_page_admin_at;


		//get PAGE ACCESS TOKEN
		$info = array("access_token" => $admin_access_token);
		$fb_request = $facebook->get('/me/accounts', $admin_access_token);
		Log::debug("Facebook response: ".print_r($fb_request->getBody(),true));
		//scorrere l'array finche non trovi la pagina giusta
		//$pages_array = $fb_request['data'];
		$pages_array = $fb_request->getGraphEdge();
		$page_access_token = null;
		foreach ($pages_array as $pagefb)
		{
			if ($pagefb['id'] == $page->fb_page_id)
			{
				$page_access_token = $pagefb['access_token'];
				break;
			}
		}

		//$facebook->setAccessToken($page_access_token);


		$data = array(
			"position" => 2,
			"custom_name" => $contest->name);



		$response = $facebook->post("/".$page->fb_page_id."/tabs/app_".$contest->company->fbApp_clientID , $data, $page_access_token);
		$graphNodeResponse = $response->getGraphNode();
		Log::debug("Facebook response for updateFacebookTab: ".print_r($graphNodeResponse, true));

	}

	public function saveEssay(){

		Log::debug("saveEssay(): input parameters: ".print_r(Input::all(),true));
		$contestId = Input::get('id_contest');
		$sentence = Input::get('sentence');
		$ownerId = Cookie::get('user_id');

		$contest = Contest::find($contestId);



		try {

			$ownerId = Cookie::get('user_id');
			Log::debug("ownerId " . $ownerId);

			$essay = new Essay();

			$tag = explode('/', ContestHelper::createEssayPicture($sentence));

			$essay->image = $tag[count($tag) - 1];
			$essay->sentence = $sentence;
			$essay->user_id = $ownerId;

			$user = User::find($ownerId);

			$facebookProfile = FacebookProfile::where('user_id', $ownerId)->where('company_id', $contest->company_id)->first();
			if ( $facebookProfile && !$facebookProfile->expired ) {
				$essay->user_connected_to_facebook = 1;
			}

			$twitterProfile = TwitterProfile::where('user_id', $ownerId)->where('company_id', $contest->company_id)->first();
			if ( $twitterProfile && !$twitterProfile->expired ) {
				$essay->user_connected_to_twitter = 1;
			}

			$essay->approval_step_id = 1;

			$contest = Contest::find($contestId);
			$essay->contest_id = $contest->id;
			$essay->location_id = $contest->location_id;

			$essay->save();
			Log::debug("Essay saved on the db");

			$output = shell_exec("sh ".base_path()."/sh/GenerateEssayThumbnails.sh ". $tag[count($tag) - 1]);

			$step = $contest->needsUserRegistration() ? $contest->makeStepClass(4) : $contest->makeStepClass(3);
			App::setLocale($contest->location->language);
			return View::make('tab/confirm')->with('contest_route', $contest->contest_route)->with('contest', $contest)->with('step', $step)->with('essay_id', $essay->id);

		} catch(Exception $e) {
			Log::error($e);
			return Redirect::to("website/widget/connect/" . $contestId);
		}

	}

	public function savePhoto()
	{
		//Inputs id_contest,title,image for photo widget
		//Inputs photo, title, id_contest, input_type for import photo


		$inputs = Input::all();
		if(!array_key_exists('id_contest', $inputs))
			$inputs['id_contest'] = $inputs['contest_id'];
		Log::debug("savePhoto::store(): input parameters: ".print_r($inputs,true));

		//find the right contest for approval of content
		$contest = Contest::find($inputs['id_contest']);
		$step = $contest->makeStepClass(2);

		//$image=$_FILE['image'];
		$photoFileName=null;
		$photoExtension=null;
		$uploadPhoto_Success = null;
		try
		{
			App::setLocale($contest->location->language);

			if (!isset($inputs['input_type']))
			{
				$photoBase64String=$inputs['photo'];

				$base64File = tempnam(null,null).".base64";
				file_put_contents($base64File,$photoBase64String);

				$photoFileName=md5(time());
				$photoExtension='jpg';

				$command = "base64 -d ".$base64File." > ".base_path()."/public/photos/".$photoFileName.".".$photoExtension." 2>&1";
				Log::debug("command Base64: ".$command);
				$output = shell_exec($command);
				Log::debug("Output command Base64: ".$output);


				$uploadPhoto_Success = true;
			}
			else
			{
				//photo input is a string path
				//I have to move the tmp file saved in tmp folder from there to correct destination
				$pathPhoto=$inputs['photo'];
				//http://images.joinmethere.tv/photos/tmp/d7bfc1c0d7e0f5ff6c1b16689d2c93f5.jpg
				$value = explode('/', $pathPhoto);
				$photoFileNamewithExt=end($value);
				$array = explode('.', $photoFileNamewithExt);
				$photoFileName = $array[0];
				$photoExtension='jpg';


				$oldpathPhoto = public_path()."/images/photos/tmp/".$photoFileNamewithExt;
				$newpathPhoto = public_path()."/photos/".$photoFileNamewithExt;
				$uploadPhoto_Success = false;

				if (File::move($oldpathPhoto, $newpathPhoto))
				{
					//die("Couldn't rename file");
					$uploadPhoto_Success = true;
				}

				//$uploadPhoto_Success = $photoFile->move('photos/', $photoFileName.'.'.$photoExtension);
				//$converted_res = ($uploadPhoto_Success) ? 'true' : 'false';
				//Log::debug("uploadPhoto_Success: ".$converted_res);

			}


			//$uploadPhoto_Success = $photoFile->move('photos/', $photoFileName.'.'.$photoExtension);
			//$converted_res = ($uploadPhoto_Success) ? 'true' : 'false';
			//Log::debug("uploadPhoto_Success: ".$converted_res);

			if ($uploadPhoto_Success)
			{
				//caricare la foto nell'album della pagina Facebook di riferimento, con tanto di tag dell'utente che ha generato il file

				$title = utf8_encode($inputs['title']);

				//find the right contest for approval of content
				$contest = Contest::find($inputs['id_contest']);


				//$ownerId = Auth::user()->id;
				//$user=User::find($ownerId);
				$ownerId = Cookie::get('user_id');
				Log::debug("ownerId " . $ownerId);

				$photo=new Photo();
				$photo->filename="$photoFileName.$photoExtension";
				$photo->name=$title;
				$photo->user_id=$ownerId;
				$photo->image="$photoFileName.$photoExtension";
				$user=User::find($ownerId);

				$facebookProfile = FacebookProfile::where('user_id', $ownerId)->where('company_id', $contest->company_id)->first();
				if ( $facebookProfile && !$facebookProfile->expired ) {
					$photo->user_connected_to_facebook = 1;
				}

				$twitterProfile = TwitterProfile::where('user_id', $ownerId)->where('company_id', $contest->company_id)->first();
				if ( $twitterProfile && !$twitterProfile->expired ) {
					$photo->user_connected_to_twitter = 1;
				}

				/////////////////////////////////
				$photo->approval_step_id=1;
				/////////////////////////////////

				/////////////////////////////////////
				/////////////////////////////////////
				$output = shell_exec("sh ".base_path()."/sh/GeneratePhotoThumbnails.sh $photoFileName.$photoExtension");
				/////////////////////////////////////
				/////////////////////////////////////


				$photo->contest_id=$contest->id;
				$photo->location_id=$contest->location_id;


				// ATTACH IF ROW DOESN' T EXIST
				/*
				$contest_2 = Contest::with(
								array(
									'users' => function($query) use ($ownerId) {
													$query->where('user_id', '=', $ownerId);
												}
									)
							)->where('id', $contest->id)->first();

				if ( $contest_2->users->count() == 0 ) {
					$contest_2->users->attach($ownerId);
				}
				*/
				//

				//Contest::find($contest->id)->users()->attach($ownerId);




				$photo->save();
				Log::debug("Photo saved on the db");

				//store and update the user access token
				//$user->user_fb_token = $user_Fb_token;
				//$user->save();


				//$statusCode=200;
				//return Response::make($photo,$statusCode);
				//redirect to confirm.blade.php
				$step = $contest->needsUserRegistration() ? $contest->makeStepClass(4) : $contest->makeStepClass(3);



				/////////////// CUSTOMIZZAZIONE ////////////////////
				Log::debug("max_entries: " . $contest->max_entries . " num_entry: " . Session::get('num_entry'));
				if ( $contest->max_entries && Session::get('num_entry') < $contest->max_entries - 1 ) {

					//////////
					Session::put('num_entry', Session::get('num_entry') + 1 );
					if ( !Session::get('entry') ) {
						$array = array();
						array_push($array, $photo->id);
					} else {
						$array = Session::get('entry');
						array_push($array, $photo->id);
					}

					Session::put('entry', $array );
					//////////

					if ( $contest->type_id == 1 || $contest->type_id == 2 ) {

						$request = Request::create('website/widget/choose/' . $contest->id . '/' . $user->id, 'GET', array());
						Request::replace($request->input());
						return Route::dispatch($request)->getContent();

					} else if ( $contest->type_id == 3 ) {

						$request = Request::create('website/widget/insert_essay/' . $contest->id, 'GET', array());
						Request::replace($request->input());
						return Route::dispatch($request)->getContent();

					}

				} else {

					Session::forget('num_entry');

					return View::make('tab/confirm')->with('contest_route', $contest->contest_route)->with('contest', $contest)->with('step', $step)->with('photo_id', $photo->id);


				}
				///////////////////////////////////////////////////////////////////



			}
			else
			{
				//return Response::json(array("error" => "Error in File Upload"),500);
				return View::make('tab/choose')->with('contest_type', $contest->type_id)->with('contest_id', $inputs['id_contest'])->with('alert', 'Error in File Upload')->with('contest', $contest)->with('step', $step);
			}

		}
		catch(Exception $e)
		{
			//print_r($e);
			Log::error($e);
			//return Response::json(array("error" => "Internal error occurred"),500);
			return View::make('tab/choose')->with('contest_type', $contest->type_id)->with('contest_id', $inputs['id_contest'])->with('alert', 'Internal error occurred')->with('step', $step)->with('contest', $contest);
		}

	}

	public function sendMail()
	{
		//Inputs: name, addressee_name, recipient
		$inputs = Input::all();

		Log::debug("inviteMail::store(): input parameters: ".print_r($inputs,true));

		// input validator with its rules
		$validator = Validator::make(
			array(
				'name' => $inputs['name'],
				'addressee_name' => $inputs['addressee_name'],
				'recipient' => $inputs['recipient']
			),
			array(
				'name' => 'required|min:3',
				'addressee_name' => 'required|min:3',
				'recipient' => 'required|email'
			)
		);

		$contest_id = $inputs['id_contest'];

		$contest = Contest::find($contest_id);

		$contestRoute = "https://www.buzzmybrand.co/tab/" . $contest->id;
		if ( $contest->contest_route ) {
			$contestRoute = "http://bmb.buzzmybrand.co/" . $contest->contest_route;
		}

		$recipient = $inputs['recipient'];
		$sender_name = $inputs['name'];
		$addressee_name = $inputs['addressee_name'];
		$data = array("name"=>$sender_name,
					  "recipient"=>$recipient,
					  "contest_name" => $contest->name,
					  "name_friend"=> $addressee_name,
					  "contest_fb_link" => $contestRoute,
	    );

		if ($validator -> fails())
		{
			Log::debug("Validation fails!");
			// data is invalid
			//return View::make('tab/invite_final')->with('message', 'Verify the inserted information. Try again!');
			return View::make('tab/invite_error');
		}
		else
		{
			Log::debug("Validation ok!");
			try {
				App::setLocale($contest->location->language);
				Mail::send('emails.test', $data, function($message) use ($data)
				{
				  $message->to($data["recipient"], $data["name"])
				  ->subject(Lang::get('email.invite.subject'));
				});

			} catch (Exception $ex) {
				Log::error('Problem sending email: '.$ex);
			}

			//close popup
			return View::make('tab/invite_final');

		}

		Log::debug("No validator!");

	}

	public function getContest($name)
	{

		$contest = Contest::where('contest_route', $name)->first();

		if ( $contest->needsUserRegistration() ) {
			$url = action('tab_HomeController@login', $contest->id);
		} else {
			$url = action('Website_WidgetController@connect', $contest->id);
			$url .= "?first_step=connect";
		}

		$template = Template::find($contest->template_id);


		App::setLocale($contest->location->language);

		return View::make('tab/index')->with('id', $contest->id)->with('url', $url)->with('contest', $contest)->with('template', $template);
	}

	public function getCustomContest($name)
	{

		$contest = Contest::where('contest_route', $name)->where('service_type', 2)->whereNotNull('image_minisite_bg')->first();

		if ( $contest->needsUserRegistration() ) {
			$url = action('tab_HomeController@login', $contest->id);
		} else {
			$url = action('Website_WidgetController@connect', $contest->id);
			$url .= "?first_step=connect";
		}

		$template = Template::find($contest->template_id);


		App::setLocale($contest->location->language);

		return View::make('tab/index')->with('id', $contest->id)->with('url', $url)->with('contest', $contest)->with('template', $template);
	}

	public function showDemo($id) {

		//$template = Template::find($id);

		return View::make('tab/template_demo')->with('id', $id);

	}

	public function index($id)
	{

		$contest = Contest::find($id);

		if ( $contest->needsUserRegistration() ) {
			$url = action('tab_HomeController@login', $id);
		} else {
			$url = action('Website_WidgetController@connect', $id);
			$url .= "?first_step=connect";
		}

		App::setLocale($contest->location->language);

		return View::make('tab/index')->with('id', $id)->with('url', $url)->with('contest', $contest);
	}

	public function getTemplateSubmissions()
	{
		if(Request::ajax())
		{
			$page = Input::get('page');
			$contest_type = Input::get('contest_type');
			$order = Input::get('order');
			$template_id = Input::get('template');

			$template = Template::find($template_id);

			if($order == 'most_recent') {
				$order_clause = 'created_at';
				$o = 'desc';
			}
			else if ( $order = 'most_viral') {
				$order_clause = 'rank_position';
				$o = 'asc';
			}

			$thumbnails_path = '/images/photos/';
			$share_path = '/FacebookSharePhoto/';
			$content_path = '/photos/';


			$submissions = Photo::where(function ($submissions) use ($template)  {
				$submissions->where('contest_id', '=', 25)->where('approval_step_id', '3');
			});

			$submissions = $submissions->orderby($order_clause, $o)->paginate(12);

			return View::make('tab.submissions_grid')->with('submissions', $submissions)->with('thumbnails_path', $thumbnails_path)
			->with('share_path', $share_path)->with('content_path', $content_path)->with('tab', Input::get('tab'))->with('social_passive', true);
		}
	}

	public function getSubmissions()
	{
		if(Request::ajax())
		{
			$contest_id = Input::get('contest_id');
			$contest = Contest::find($contest_id);
			//$fb_page_link = $contest->fb_page_link;
			$page = Input::get('page');
			$contest_type = Input::get('contest_type');
			$order = Input::get('order');
			if ($contest->win_modality_id == '1')
			{
				$statistics = $this->getStatistics($contest_id);
			}
			else
			{
				$statistics = null;
			}

			if($order == 'most_recent') {
				$order_clause = 'created_at';
				$o = 'desc';
			}
			else if ( $order = 'most_viral') {
				$order_clause = 'rank_position';
				$o = 'asc';
			}
			if( $contest_type == 1)
			{
				$thumbnails_path = '/images/videos/';
				$content_path = '/videos/';
				$share_path = '/FacebookShare/';

				$submissions = Video::where('contest_id', '=', $contest_id)->where('approval_step_id', '3');

				if ( $contest->fbPages->count() > 0 ) {
					$submissions = $submissions->where('uploadOnFacebookDone', '1');
				}
				if ( $contest->twPages->count() > 0 ) {
					$submissions = $submissions->where('uploadOnTwitterDone', '1');
				}

				if ( $contest->ytPages->count() > 0 ) {
					$submissions = $submissions->where('uploadOnCompanyYouTubeDone', '1');
				}

				$submissions = $submissions->orderby($order_clause, $o)->paginate(12);
			}
			else if( $contest_type == 2)
			{
				$thumbnails_path = '/images/photos/';
				$share_path = '/FacebookSharePhoto/';
				$content_path = '/photos/';


				$submissions = Photo::where(function ($submissions) use ($contest) {
					$submissions->where('contest_id', '=', $contest->id)->where('approval_step_id', '3');
						    if ( $contest->fbPages->count() > 0 ) {
								$submissions->where('uploadOnFacebookDone', '1');
							}
							if ( $contest->twPages->count() > 0 ) {
								$submissions->where('uploadOnTwitterDone', '1');
							}
				})->orWhere(function ($submissions) use ($contest) {
							if ($contest->instagram) {
								$submissions->where('contest_id', '=', $contest->id)->where('approval_step_id', '3')->where('user_connected_to_instagram', '1');
							}
				});

				/*
				$submissions = Photo::where('contest_id', '=', $contest_id)->where('approval_step_id', '3');
				if ( $contest->fbPages->count() > 0 ) {
					$submissions = $submissions->where('uploadOnFacebookDone', '1');
				}
				if ( $contest->twPages->count() > 0 ) {
					$submissions = $submissions->where('uploadOnTwitterDone', '1');
				}
				if ($contest->instagram) {
					$submissions = $submissions->orwhere('user_connected_to_instagram', '1');
					//$submissions = Photo::where('contest_id', '=', $contest_id)->where('approval_step_id', '3');

				}
				*/
				$submissions = $submissions->orderby($order_clause, $o)->paginate(12);

			} else if( $contest_type == 3)
			{
				$thumbnails_path = '/images/photos/';
				$share_path = '/FacebookSharePhoto/';
				$content_path = '/photos/';

				$submissions = Essay::where('contest_id', '=', $contest_id)->where('approval_step_id', '3');
				if ( $contest->fbPages->count() > 0 ) {
					$submissions = $submissions->where('uploadOnFacebookDone', '1');
				}
				if ( $contest->twPages->count() > 0 ) {
					$submissions = $submissions->where('uploadOnTwitterDone', '1');
				}

				$submissions = $submissions->orderby($order_clause, $o)->paginate(12);
			}

			return View::make('tab.submissions_grid')->with('submissions', $submissions)->with('thumbnails_path', $thumbnails_path)
			->with('share_path', $share_path)->with('content_path', $content_path)->with('statistics', $statistics)->with('tab', Input::get('tab'))->with('social_passive', false);
		}

	}
	public function getSubmission()
	{
		if(Request::ajax())
		{

			$url = Input::get('url');
			$contest_type = Input::get('contest_type');
			$tab = Input::get('tab');

			if( $contest_type == 1)
				return '<video width="640" height="480" controls autoplay>
  				<source src="' . $url . '" type="video/mp4">
  				</video>';
			else if ( $contest_type == 2)
			{
                if ($tab == "true")
                {
                    $width_original = 640;
                    $height_original = 480;
                }
                else
                {
                    $width_original = 888;
                    $height_original = 500;
                }

				$size = getimagesize($url);
    			$width = $size[0];
    			$height = $size[1];
    			$ratio = $width/$height;
				$ratio_original = $width_original/$height_original;
    			$margin_top = 0;
				if ($ratio == $ratio_original)
				{
					$width = $width_original;
	    			$height = $height_original;
				}
				else //if ( $width != '640' && $height != '480')
				{
					if( $ratio > $ratio_original) {
	    				$width = $width_original;
	    				$height = $height_original*$ratio;
						if ($height > $height_original)
						{
							$height = $width_original/$ratio;
						}

						$margin_top = ($height_original - $height)/ 2 - 50;
						if( $margin_top < 0 )
							$margin_top = ($height_original - $height)/ 2;
					}
					else if ( $ratio < $ratio_original)
					{
						$width = $height_original*$ratio;
	    				$height = $height_original;

						$margin_top = ($height_original - $height)/ 2 - 50;
						if( $margin_top < 0 )
							$margin_top = (500 - $height)/ 2;
					}
					else
					{
						$width = $height_original;
						$height = $height_original;

						$margin_top = ($height_original - $height)/ 2 - 50;
						if( $margin_top < 0 )
							$margin_top = ($height_original - $height)/ 2;
					}


    			}
				return '<img src="' . $url . '" class=\'image_popup\'" width="' . $width . '" height="' . $height . '" / >';
				//return '<img src="' . $url . '" width="' . $width . '" height="' . $height . '" style="margin-top:' . $margin_top .'px;"/ >';
			} else if ( $contest_type == 3)
			{
				$size = getimagesize($url);
    			$width = $size[0];
    			$height = $size[1];
    			$ratio = $width/$height;
				$ratio_original = 640/480;
    			$margin_top = 0;
				if ($ratio == $ratio_original)
				{
					$width = 640;
	    			$height = 480;
				}
				else if( $width != '640' && $height != '480')
				{
					if( $ratio > $ratio_original) {
	    				$width = 640;
	    				$height = 480*$ratio;
						if ($height > 480)
						{
							$height = 640/$ratio;
						}

						$margin_top = (480 - $height)/ 2 - 50;
						if( $margin_top < 0 )
							$margin_top = (480 - $height)/ 2;
					}
					else if ( $ratio < $ratio_original)
					{
						$width = 480*$ratio;
	    				$height = 480;

						$margin_top = (480 - $height)/ 2 - 50;
						if( $margin_top < 0 )
							$margin_top = (480 - $height)/ 2;
					}
					else
					{
						$width = 480;
						$height = 480;

						$margin_top = (480 - $height)/ 2 - 50;
						if( $margin_top < 0 )
							$margin_top = (480 - $height)/ 2;
					}


    			}

				return '<img src="' . $url . '" width="' . $width . '" height="' . $height . '" style="margin-top:' . $margin_top .'px;"/ >';
			}
			else
				return "Error";
		}
	}
	public function uploadSubmission()
	{

		Log::debug("uploadSubmission method ");
		$contest_type = Input::get('contest_type');
		$contest_id = Input::get('contest_id');

		$contest = Contest::find($contest_id);
		App::setLocale($contest->location->language);
		//$fb_id = Input::get('fb_id');
		if( $contest_type == 1)
		{
			$data = Input::file('video');
			$dataFilePath=$data->getRealPath();
			Log::debug("dataFilePath: ".$dataFilePath);
			$videoExtension=strtolower($data->getClientOriginalExtension());
			if (!isset($videoExtension) or $videoExtension=='')
			{
				$videoExtension = 'mp4';
			}

			$dataFileName = md5(time());
			$dataFolderTemp = public_path().'/images/videos/tmp';

			if ($data->move($dataFolderTemp, $dataFileName.'.'.$videoExtension))
			{

				// Put video on wowza
				$oldpathVideo = public_path()."/images/videos/tmp/".$dataFileName.'.'.$videoExtension;
				$newpathVideo = public_path()."/videos/".$dataFileName.'.'.$videoExtension;
				File::copy($oldpathVideo, $newpathVideo);
				//

				$thumbnail = ContestHelper::createThumbnailVideo($newpathVideo);
				Log::debug("Thumbnail content: ".$thumbnail);


				$datapath = $dataFolderTemp.'/'.$dataFileName.'.'.$videoExtension;
				$step = $contest->makeStepClass(($contest->needsUserRegistration() ? 3 : 2));
				return View::make('tab/upload_submission')
				->with('contest_type', $contest_type)
				->with('contest_id',$contest_id)
				->with('data', 'http://www.buzzmybrand.co/images/videos/tmp/'.$dataFileName.'.'.$videoExtension)
				->with('image', 'http://www.buzzmybrand.co/website_public/hdfvr/snapshots/'.$dataFileName.'.jpg')
				->with('filename', $dataFileName.'.'.$videoExtension)
				->with('contest', $contest)
				->with('step', $step);
			}
		}
		else if( $contest_type == 2 )
		{
			$data = Input::file('photo');
			$dataFilePath=$data->getRealPath();
			Log::debug("dataFilePath: ".$dataFilePath);
			$imageExtension=strtolower($data->getClientOriginalExtension());
			if (!isset($imageExtension) or $imageExtension=='')
			{
				$imageExtension = 'jpg';
			}

			$dataFileName = md5(time());
			$dataFolderTemp = public_path().'/images/photos/tmp';


			if ($data->move($dataFolderTemp, $dataFileName.'.'.$imageExtension))
			{

				// create new Intervention Image
				$img = Image::make($dataFolderTemp."/".$dataFileName.'.'.$imageExtension);

				$orientation = $img->exif('Orientation');
				Log::debug("Image orientation: ".$orientation);

				if (!empty($orientation))
				{
					switch($orientation)
					{
						case 8:
							$img->rotate(90);
							$img->save($dataFolderTemp."/".$dataFileName.'.'.$imageExtension);
						break;
						case 3:
							$img->rotate(180);
							$img->save($dataFolderTemp."/".$dataFileName.'.'.$imageExtension);
						break;
						case 6:
							$img->rotate(-90);
							$img->save($dataFolderTemp."/".$dataFileName.'.'.$imageExtension);
						break;
						default:
					}

				}



				$datapath = $dataFolderTemp.'/'.$dataFileName.'.'.$imageExtension;


				/////////////// CUSTOMIZZAZIONE //////////////////////
				/*
				if (($contest->max_entries) && (!Session::get('num_entry')))
				{
					Session::put('num_entry', 1 );
				}
				*/

				if ( $contest->max_entries && Session::get('num_entry') && Session::get('num_entry') < $contest->max_entries ) {

					$photos = array();
					foreach(Session::get('entry') as $e) {
						$p = Photo::find($e);
						array_push($photos, $p);
					}
					Log::debug("Customizzazione comieco, valore item: ".Session::get('num_entry') + 1);
					return View::make('tab/upload_submission')->with('contest_type', $contest_type)->with('contest_id',$contest_id)->with('data', 'http://www.buzzmybrand.co/images/photos/tmp/'.$dataFileName.'.'.$imageExtension)->with('contest', $contest)->with('photos', $photos)->with('item', Session::get('num_entry') + 1);
				} elseif ($contest->max_entries) {
					Log::debug("Customizzazione comieco, importazione prima foto");
					return View::make('tab/upload_submission')->with('contest_type', $contest_type)->with('contest_id',$contest_id)->with('data', 'http://www.buzzmybrand.co/images/photos/tmp/'.$dataFileName.'.'.$imageExtension)->with('contest', $contest)->with('item', 1);
				} else {
					Log::debug("Nessuna customizzazione");
					return View::make('tab/upload_submission')->with('contest_type', $contest_type)->with('contest_id',$contest_id)->with('data', 'http://www.buzzmybrand.co/images/photos/tmp/'.$dataFileName.'.'.$imageExtension)->with('contest', $contest)->with('item', 0);
				}
				///////////////////////////////////////////////////////

			}
		}
    	return View::make('tab/upload_submission')->with('contest_type', $contest_type)->with('contest_id',$contest_id)->with('data', $data)->with('contest', $contest);
	}
	public function takePicture()
	{
		$contest_type = Input::get('contest_type');
		$contest_id = Input::get('contest_id');
		$contest = Contest::find($contest_id);
		App::setLocale($contest->location->language);

		/////////////// CUSTOMIZZAZIONE //////////////////////
		if ( $contest->max_entries && Session::get('num_entry') && Session::get('num_entry') < $contest->max_entries ) {

			$photos = array();
			foreach(Session::get('entry') as $e) {
				$p = Photo::find($e);
				array_push($photos, $p);
			}
			Log::debug("Customizzazione comieco, valore item: ".Session::get('num_entry') + 1);
			return View::make('tab/take_picture')->with('contest_type', $contest_type)->with('contest_id',$contest_id)->with('contest', $contest)->with('photos', $photos)->with('item', Session::get('num_entry') + 1);
		} else if ($contest->max_entries) {
			Log::debug("Customizzazione comieco, importazione prima foto");
			return View::make('tab/take_picture')->with('contest_type', $contest_type)->with('contest_id',$contest_id)->with('contest', $contest)->with('item', 1);
		} else {
			return View::make('tab/take_picture')->with('contest_type', $contest_type)->with('contest_id',$contest_id)->with('contest', $contest)->with('item', 0);
		}
		///////////////////////////////////////////////////////



	}

	public function login($id)
	{
		$contest = Contest::find($id);

		$imageLogin = public_path()."/images/companys/".$contest->company->image_login;

		/*
		$idFromCookie = Cookie::get('user_id');

		$user = null;

		if ( $idFromCookie !== '' && $idFromCookie !== 0 ) {

			$user = User::with(
						array('facebookProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						})
					)->where('id', $idFromCookie)->first();

		}
		*/

		App::setLocale($contest->location->language);
		return View::make('website/widget/login')
			->with('contest', $contest)
			->with('step', $contest->makeStepClass(1, 4));
		/*
		if (CommonHelper::loggedInAsUser())
		{
			$user_id = Auth::user()->id;
			Log::info('User (id: '.$user_id.') started to use widget.');
			$config = Config::get('hdfvr');
			$domain = $config['domain'];
			$contest = Contest::find($id);
			$contest_type = $contest->type_id;
			//return View::make('website/widget/record')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain);
			return View::make('tab/choose')->with('contest_id', $id)->with('user_id', $user_id)->with('domain', $domain)->with('contest_type', $contest_type);
		}
		else
		{
			$errors = null;
			$message = null;
			if (Request::isMethod('post'))
			{
				$userData = array(
						'username' => Input::get('username'),
						'password' => Input::get('password')
				);
				Log::debug("User credentials. username: ".$userData["username"].", password: ".$userData["password"]);
				//@TODO: create login for user with user role
				$user = UserHelper::loginUser($userData);

				if ($user)
				{
					$userId = $user->id;
					Log::info('User (id: '.$userId.') logged to system in widget');
					return Redirect::action('Website_WidgetController@record', array($id));
				}
				else
				{
					Log::info('Wrong data login (username: '.$userData['username'].')');
					$message =  trans('messages.wrong_data_login');
				}
			}
			return View::make('website/widget/login')->with('errors', $errors)->with('message', $message)->with('id', $id);
		}
		*/
	}

	public function invite($id)
	{
		$contest = Contest::find($id);
		App::setLocale($contest->location->language);
		return View::make('tab/invite')->with('contest_id', $id);
	}



	public function saveVideo()
	{

		//Inputs id_contest,title,video

		$inputs = Input::all();
		Log::debug("VideoController::store(): input parameters: ".print_r($inputs,true));



		$object_title = utf8_encode($inputs['title']);
		//$fb_id = $inputs['fb_id'];

		if(!isset($inputs['video']))
		{
			Log::debug("VideoController::store(): Required parameter video were not found, returning 400");
			$description='Required Paramater Video Not Found';
			//$statusCode=400;
			//return Response::make($description, $statusCode);
			return View::make('tab/choose')->with('contest_type', '1')->with('contest_id', $inputs['id_contest'])->with('alert', $description);
		}



		try
		{

			$pathVideo=$inputs['video'];

			$value = explode('/', $pathVideo);
			$videoFileNamewithExt=end($value);
			$array = explode('.', $videoFileNamewithExt);
			$videoFileName = $array[0];
			$videoExtension='mp4';


			$oldpathVideo = public_path()."/images/videos/tmp/".$videoFileNamewithExt;
			$newpathVideo = public_path()."/videos/".$videoFileNamewithExt;

			$uploadVideo_Success = false;


			$video_attributes = ContestHelper::get_video_attributes($oldpathVideo);
			Log::debug("Video attributes: ".print_r($video_attributes,true));

			if (File::move($oldpathVideo, $newpathVideo))
			{
				//die("Couldn't rename file");
				$uploadVideo_Success = true;
			}




			/*
			$videoFile=Input::file('video');
			$video_attributes = ContestHelper::get_video_attributes($videoFile);

			//create thumbnail
			$thumbnail = ContestHelper::createThumbnailVideo($videoFile);


			$videoFilePath=$videoFile->getRealPath();
			//$videoFileName=md5_file($videoFilePath);
			$videoFileName=md5(time());
			$videoExtension=strtolower($videoFile->getClientOriginalExtension());
			//$videoExtension=strtolower($videoFile->getMimeType());
			if (!isset($videoExtension) or $videoExtension=='')
			{
				$videoExtension = 'mp4';
			}
			*/
			if ($uploadVideo_Success)
			{
				//find the right contest for approval of content
				$contest = null;
				if (!isset($inputs['id_contest']))
				{
					$contestName=$inputs['contest'];
					$contest=Contest::where('name',$contestName)->first();
				}
				else
				{
					$contest=Contest::where('id',$inputs['id_contest'])->first();
				}

				App::setLocale($contest->location->language);



				//$facebookProfiles = FacebookProfile::where('id_fb', $fb_id)->first();

				//$ownerId = $facebookProfiles->user_id;
				//$ownerId = Auth::user()->id;
				$ownerId = Cookie::get('user_id');

				$video=new Video();
				$video->filename="$videoFileName.$videoExtension";
				$video->name=$object_title;
				$video->duration=$video_attributes['secs'];
				$video->image="$videoFileName.$videoExtension";
				$video->user_id=$ownerId;

				/*
				if (isset($inputs['sub_videopart']))
				{
					$video->sub_videopart=$inputs['sub_videopart'];
				}
				*/
				//$user=User::find($ownerId);

				$facebookProfile = FacebookProfile::where('user_id', $ownerId)->where('company_id', $contest->company_id)->first();
				if ( $facebookProfile && !$facebookProfile->expired ) {
					$video->user_connected_to_facebook = 1;
				}

				$twitterProfile = TwitterProfile::where('user_id', $ownerId)->where('company_id', $contest->company_id)->first();
				if ( $twitterProfile && !$twitterProfile->expired ) {
					$video->user_connected_to_twitter = 1;
				}

				////////////////////////////////////
				$video->approval_step_id=1;
				////////////////////////////////////

				////////////////////////////////////
				////////////////////////////////////
				exec("sh ".base_path()."/sh/GenerateThumbnails.sh $videoFileName.$videoExtension", $output);
				////////////////////////////////////
				////////////////////////////////////

				if (isset($inputs['product']))
				{
					/*
					$submissionInfo =new ExtraInfo();
					$submissionInfo->company=$inputs['company'];
					$submissionInfo->regnumber=$inputs['regnumber'];
					$submissionInfo->telephone=$inputs['telephone'];
					$submissionInfo->save();
					$video->extraInfo_id=$submissionInfo->id;
					*/
				}


				$video->contest_id=$contest->id;
				$video->location_id=$contest->location_id;

				//create thumbnail
				$thumbnail = ContestHelper::createThumbnailVideo(base_path().'/public/videos/'."$videoFileName.$videoExtension");
				$video->image = $thumbnail;

				$output = shell_exec("sh ".base_path()."/sh/GenerateThumbnails.sh ".$video->filename);
				Log::debug("Output GenerateThumbnails: ".$output);


				// ATTACH IF ROW DOESN' T EXIST
				/*
				$contest_2 = Contest::with(
								array(
									'users' => function($query) use ($ownerId) {
													$query->where('user_id', '=', $ownerId);
												}
									)
							)->where('id', $contest->id)->first();

				if ( $contest_2->users->count() == 0 ) {
					$contest_2->users->attach($ownerId);
				}
				*/
				//

				//join the contest
				//Contest::find($contest->id)->users()->attach($ownerId);


				$step = $contest->needsUserRegistration() ? $contest->makeStepClass(4) : $contest->makeStepClass(3);

				$video->save();
				Log::debug("Video saved on the db");


				//redirect to confirm.blade.php
				return View::make('tab/confirm')->with('contest', $contest)->with('video_id', $video->id)->with('step', $step)->with('contest_route', $contest->contest_route);

			}
			else
			{
				throw new Exception("Error during File Upload");
				/*$description='Error in File Upload';
				$statusCode=400;
				return Response::make($description,$statusCode);*/
			}




		}
		catch(Exception $e)
		{
			Log::error($e);
			//return Response::json(array("error" => "Internal error occured"),500);
			return View::make('tab/choose')->with('contest_type', $contest->type_id)->with('contest_id', $inputs['id_contest'])->with('alert', $e);
		}







	}
















	public function indexBS($id)
	{
		$url = action('tab_HomeController@loginBS', $id);
		$contest = Contest::find($id);
		$contestImage = $contest->image;
		$first_Answered = 0;
		$second_Answered = 0;

		//$contest = ContestHelper::get($id);
		//se il contest ha approval_content=1 devo visualizzare solo i video approvati
		$contest_approval=$contest->contest_approval;



		if ($contest_approval == 1)
		{

			$collection = $contest->videos;

			$videos = $collection->filter(function($video)
			{
				if ($video->approval_step_id == '3') {
					return true;
				}
			});

			//only approved videos
			/*$videos = $contest->videos;
				$videos = $videos->filter(function($video)
				{
						return $video->where('approval_step_id', '=','3');
						});*/
		}
		else
		{
			if ($contest->is_resume == '0')
			{
				$videos = $contest->videos;
			}
			else //trattasi di video resume
			{
				$collection = $contest->videos;

				//devo sapere se c'� il video della prima risposta

				$videosAnswer1 = $collection->filter(function($video)
				{
					if ($video->sub_videopart == 1) {
						return true;
					}
				});
				if (count($videosAnswer1) > 0)
					$first_Answered = 1;

				//devo sapere se c'� il video della seconda risposta

				$videosAnswer2 = $collection->filter(function($video)
				{
					if ($video->sub_videopart == 2) {
						return true;
					}
				});
				if (count($videosAnswer2) > 0)
					$second_Answered = 1;

				$videos = $collection->filter(function($video)
				{
					if (is_null($video->sub_videopart)) {
						return true;
					}
				});
			}

		}

		$contestData = ContestHelper::getContestData($id);
		//@TODO count videos for carousel
		//example
		$countVideos = count($videos);



		return View::make('tab/indexBS')->with('id', $id)->with('url', $url)->with('contestImage', $contestImage)->with('videos', $videos)->with('countVideos', $countVideos)->with('contestData', $contestData)->with('first_Answered', $first_Answered)->with('second_Answered', $second_Answered);
	}



	public function loginBS($id)
	{
		if (CommonHelper::loggedInAsUser())
		{
			$userId = Auth::user()->id;
			Log::info('User (id: '.$userId.') started to use widget.');
			$config = Config::get('hdfvr');
			$domain = $config['domain'];
			return View::make('website/widget/record')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain);
		}
		else
		{
			$errors = null;
			$message = null;
			if (Request::isMethod('post'))
			{
				$userData = array(
						'username' => Input::get('username'),
						'password' => Input::get('password')
				);
				Log::debug("User credentials. username: ".$userData["username"].", password: ".$userData["password"]);
				//@TODO: create login for user with user role
				$user = UserHelper::loginUser($userData);

				if ($user)
				{
					$userId = $user->id;
					Log::info('User (id: '.$userId.') logged to system in widget');
					return Redirect::action('Website_WidgetController@record', array($id));
				}
				else
				{
					Log::info('Wrong data login (username: '.$userData['username'].')');
					$message =  trans('messages.wrong_data_login');
				}
			}
			return View::make('website/widget/login')->with('errors', $errors)->with('message', $message)->with('id', $id);
		}

	}

	private function withCommas($int) {

		$str_int = "$int";
		$char = str_split($str_int);
		$result = array();
		for ($i = count($char) - 1; $i >= 0; $i--) {

			array_push($result,$char[$i]) ;

			if (($i + 1)% 3 == 0) {
				array_push($result,',') ;
			}

		}

		$str = '';
		for ($i = 0; $i <= count($char); $i++) {

			$str += array_pop($result) ;


		}


		return $str;

	}


	private function getStatistics($contestId) {

		$contest = Contest::find($contestId);
		$statistics = array();

		if ($contest->type_id == 1) {

			foreach ($contest->videos as $video) {

				// START Aggregazione statistiche per video //
				$statistics[$video->id] = array(
												'unique_likes' => 0,
												'unique_comments' => 0,
												'unique_shares' => 0,
												'unique_views' => 0,
												'reach' => 0,
												'retweet' => 0,
												'favourite' => 0,
												'yt_likes' => 0,
												'yt_views' => 0,
												);


				foreach ($video->fbPages as $vp) {

					$statistics[$video->id]['unique_likes'] += $vp->pivot->unique_likes;
					$statistics[$video->id]['unique_comments'] += $vp->pivot->unique_comments;
					$statistics[$video->id]['unique_shares'] += $vp->pivot->unique_shares;
					$statistics[$video->id]['unique_views'] += $vp->pivot->unique_views;
					$statistics[$video->id]['reach'] += $vp->pivot->reach;

				}

				foreach ($video->twPages as $vp) {

					$statistics[$video->id]['retweet'] += $vp->pivot->retweets_count;
					$statistics[$video->id]['favourite'] += $vp->pivot->favourite_count;
				}

				foreach ($video->ytPages as $vp) {

					$statistics[$video->id]['yt_likes'] += $vp->pivot->yt_like_count;
					$statistics[$video->id]['yt_views'] += $vp->pivot->yt_view_count;
				}

				$statistics[$video->id]['unique_likes'] = number_format($statistics[$video->id]['unique_likes']);
				$statistics[$video->id]['unique_comments'] = number_format($statistics[$video->id]['unique_comments']);
				$statistics[$video->id]['unique_shares'] = number_format($statistics[$video->id]['unique_shares']);
				$statistics[$video->id]['unique_views'] = number_format($statistics[$video->id]['unique_views']);
				$statistics[$video->id]['reach'] = number_format($statistics[$video->id]['reach']);
				$statistics[$video->id]['retweet'] = number_format($statistics[$video->id]['retweet'] + $video->retweets_count_post );
				$statistics[$video->id]['favourite'] = number_format($statistics[$video->id]['favourite'] + $video->favourite_count_post );
				$statistics[$video->id]['yt_likes'] = number_format($statistics[$video->id]['yt_likes']);
				$statistics[$video->id]['yt_views'] = number_format($statistics[$video->id]['yt_views']);

				// END Aggregazione statistiche per video //

			}

		} else if ($contest->type_id == 2) {

			foreach ($contest->photos as $photo) {

				// START Aggregazione statistiche per video //
				$statistics[$photo->id] = array(
												'unique_likes' => 0,
												'unique_comments' => 0,
												'unique_shares' => 0,
												'unique_views' => 0,
												'reach' => 0,
												'retweet' => 0,
												'favourite' => 0,
												);


				foreach ($photo->fbPages as $fp) {

					$statistics[$photo->id]['unique_likes'] += $fp->pivot->unique_likes;
					$statistics[$photo->id]['unique_comments'] += $fp->pivot->unique_comments;
					$statistics[$photo->id]['unique_shares'] += $fp->pivot->unique_shares;
					$statistics[$photo->id]['reach'] += $fp->pivot->reach;

				}

				foreach ($photo->twPages as $fp) {

					$statistics[$photo->id]['retweet'] += $fp->pivot->retweets_count;
					$statistics[$photo->id]['favourite'] += $fp->pivot->favourite_count;
				}

				$statistics[$photo->id]['unique_likes'] = number_format($statistics[$photo->id]['unique_likes'] + $photo->likes_count_post );
				$statistics[$photo->id]['unique_comments'] = number_format($statistics[$photo->id]['unique_comments'] + $photo->comments_count_post );
				$statistics[$photo->id]['unique_shares'] = number_format($statistics[$photo->id]['unique_shares']);
				$statistics[$photo->id]['unique_views'] = number_format($statistics[$photo->id]['unique_views']);
				$statistics[$photo->id]['reach'] = number_format($statistics[$photo->id]['reach']);
				$statistics[$photo->id]['retweet'] = number_format($statistics[$photo->id]['retweet'] + $photo->retweets_count_post );
				$statistics[$photo->id]['favourite'] = number_format($statistics[$photo->id]['favourite'] + $photo->favourite_count_post  );


				// END Aggregazione statistiche per video //
			}

		} else if ( $contest->type_id == 3 ) {


			foreach ($contest->essays as $essay) {

				// START Aggregazione statistiche per video //
				$statistics[$essay->id] = array(
												'unique_likes' => 0,
												'unique_comments' => 0,
												'unique_shares' => 0,
												'unique_views' => 0,
												'reach' => 0,
												'retweet' => 0,
												'favourite' => 0,
												);


				foreach ($essay->fbPages as $fp) {

					$statistics[$essay->id]['unique_likes'] += $fp->pivot->unique_likes;
					$statistics[$essay->id]['unique_comments'] += $fp->pivot->unique_comments;
					$statistics[$essay->id]['unique_shares'] += $fp->pivot->unique_shares;
					$statistics[$essay->id]['reach'] += $fp->pivot->reach;

				}

				foreach ($essay->twPages as $fp) {

					$statistics[$essay->id]['retweet'] += $fp->pivot->retweets_count;
					$statistics[$essay->id]['favourite'] += $fp->pivot->favourite_count;
				}

				$statistics[$essay->id]['unique_likes'] = number_format($statistics[$essay->id]['unique_likes'] + $essay->likes_count_post );
				$statistics[$essay->id]['unique_comments'] = number_format($statistics[$essay->id]['unique_comments'] + $essay->comments_count_post );
				$statistics[$essay->id]['unique_shares'] = number_format($statistics[$essay->id]['unique_shares']);
				$statistics[$essay->id]['unique_views'] = number_format($statistics[$essay->id]['unique_views']);
				$statistics[$essay->id]['reach'] = number_format($statistics[$essay->id]['reach']);
				$statistics[$essay->id]['retweet'] = number_format($statistics[$essay->id]['retweet'] + $essay->retweets_count_post );
				$statistics[$essay->id]['favourite'] = number_format($statistics[$essay->id]['favourite'] + $essay->favourite_count_post );


				// END Aggregazione statistiche per video //
			}

		}
		return $statistics;

	}

	public function tabs() {


		$signed_request = Input::get('signed_request');
		$signed_request_element = explode(".", $signed_request);
		$json_encoded = $signed_request_element[1];
		$json = base64_decode ( $json_encoded );
		$data = json_decode($json);


		$fbPage = fbPage::where("fb_page_id", $data->page->id)->first();

		//$contest = $fbPage->contests->first();

		$contest = DB::table('fbpage_contest')
				->join("contests", "contests.id", "=", "fbpage_contest.contest_id")
				->where("fbpage_contest.fbpage_id", "=", $fbPage->id)
				->orderBy('contests.id', 'desc')->first();

		$contestRoute = "https://www.buzzmybrand.com/minisite/" . $contest->id;


		return Redirect::to($contestRoute);

	}

	public function view_t_c($contestId)
	{
		$contest = Contest::find($contestId);

		$termsFile = $contest->terms_cond;

		$file= public_path(). "/T_and_C/".$termsFile;

		return Response::make(file_get_contents($file), 200, [
			'Content-Type' => 'application/pdf',
			'Content-Disposition' => 'inline; '.$termsFile,
		]);
	}

	public function getDownload($contestId)
	{

		$contest = Contest::find($contestId);

		$termsFile = $contest->terms_cond;

        //PDF file is stored under project/public/download/info.pdf
        $file= public_path(). "/T_and_C/".$termsFile;
		$headers = array(
		  'Content-Type' => 'application/pdf',
		);
        return Response::download($file, 't&c.pdf', $headers);

	}


	public function getPrivacy()
	{

        $file= public_path(). "/T_and_C/BMB Privacy Policy minisite.pdf";
		$headers = array(
		  'Content-Type' => 'application/pdf',
		);
        return Response::download($file, 'privacy.pdf', $headers);

	}

	public function computeSharingScore()
	{
		$data = Input::all();

		return $data['destination'];
	}

public function instantWin()
	{
		try
		{
			Log::info('instantWin function');
			//funzione per sapere se l'utente vince o meno
			if(Request::ajax())
			{
				$contest_id = Input::get('contest_id');
				$contest = Contest::find($contest_id);
				$photo_id = Input::get('photo_id');
				$photo = Photo::find($photo_id);
				$user = User::find($photo->user_id);

				//$model->problems()->where('phone_problem', $problem->id)->first()->pivot->price
				//$user->contests()->where('users.id', '=', $photo->user_id)->first()->pivot->instantwin_done


				if ($contest->prize_pool > 0)
				{

					//nuova parte
					if (isset($contest->users()->where('users.id', '=', $photo->user_id)->first()->pivot->instantwin_done)) {
						//return false;
						Log::info('instantWin function: utente ha gi� vinto');
						$json = Response::json(array("result" => false), 200);
						return $json;
					}
					else
					{
						$contest->prize_pool = $contest->prize_pool - 1;
						$contest->users()->updateExistingPivot($photo->user_id, array('instantwin_done' => new DateTime('now')), false);

						$contest->save();
						//devo salvare contest e user entity
						//return true;
						Log::info('instantWin function: utente ha vinto');
						$json = Response::json(array("result" => true), 200);
						return $json;
					}



					/*
					//devo verificare che l'utente non ha gi� vinto il premio instantwin
					if ($user->instantwin_done == 1) {
						//return false;
						Log::info('instantWin function: utente ha gi� vinto');
						$json = Response::json(array("result" => false), 200);
						return $json;
					}
					else
					{
						$contest->prize_pool = $contest->prize_pool - 1;
						$user->instantwin_done = 1;
						$contest->save();
						$user->save();
						//devo salvare contest e user entity
						//return true;
						Log::info('instantWin function: utente ha vinto');
						$json = Response::json(array("result" => true), 200);
						return $json;
					}
					*/
				}
				else {
					//return false;
					Log::info('instantWin function: premi esauriti per il momento');
					$json = Response::json(array("result" => false), 200);
					return $json;
				}



			}
			else {
				//return false;
				Log::info('instantWin function: richiesta non ajax');
				$json = Response::json(array("result" => false), 200);
				return $json;
			}




		}
		catch (Exception $ex)
		{
			Log::error($ex);
			$statusCode = 400;
			$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
			return $json;
		}


	}

}
?>
