<?php

use Symfony\Component\Filesystem\Exception\IOException;

class HomeController extends BaseController {


	

	/*public function loginWithTwitterToken()
	{
		try
		{
			$token = CommonHelper::mandatoryInput("token");
			$tokenSecret = CommonHelper::mandatoryInput("token_secret");
			Log::debug("Twitter access token: ".$token);
			
			$config = Config::get('twitter');
			$connection = new TwitterOAuth($config['consumer_key'], $config['consumer_secret'], $token, $tokenSecret);
			$params = array();
			$params['include_entities']='false';
			$content = $connection->get('account/verify_credentials',$params);
			Log::debug("verify_credentials returned: ".print_r($content,true));
			if (!$content || !isset($content->screen_name)) {
				throw new OAuthException("Twitter token verification failed");
			}
			Log::debug("User ".$content->screen_name." token verified successfully");
			$user = User::where("username",$content->screen_name)->first();
			if ($user == null) 
			{
				$user = new User();
				$user->email = $content->screen_name."@twitter.com";
				$user->password = Hash::make(Config::get("facebook")["secret_password"]);
				//$user->password = $config["secret_password"].time();
				$user->username = $content->screen_name;
				if (isset($content->location))
				{
					$user->country = $content->location;
				}
				if (isset($content->profile_image_url))
				{
					Log::debug("User with the following image: ".$content->profile_image_url." to be saved.");
					$imagefilename = CommonHelper::random_string(32).".png";
					$sourcefile = CommonHelper::GetImageFromUrl($content->profile_image_url);
					$savefile = fopen("images/users/".$imagefilename, 'w'); 
					fwrite($savefile, $sourcefile); 
					fclose($savefile);
					$user->image = $imagefilename;
				}
				$user->save();
				
				$cachedPassword = $user->password;
				$tmpPassword = $config["secret_password"].time();
				$user->password = Hash::make($tmpPassword);
				$user->save();
				$userData = array("username" => $user->email, "password" => $tmpPassword);
				$json = CommonHelper::getAccessToken($userData,true);
				Log::debug("Response for user ".$user->email.": ".json_encode($json, JSON_PRETTY_PRINT));
				$user->password = $cachedPassword;
				$user->save();
				Log::debug("Cached password recreated");
				
				return Response::json($json,200);
				//return Response::json("User ".$content->screen_name." not found", 404);
			}
			$cachedPassword = $user->password;
			$tmpPassword = $config["secret_password"].time();
			$user->password = Hash::make($tmpPassword);
			$user->save();
			Log::debug("Cached password: ".$cachedPassword.". Performing implicit login of user ".$user->email);
			$userData = array("username" => $user->email, "password" => $tmpPassword);
			$json = CommonHelper::getAccessToken($userData,true);
			Log::debug("Response for user ".$user->email.": ".json_encode($json, JSON_PRETTY_PRINT));
			$user->password = $cachedPassword;
			$user->save();
			Log::debug("Cached password recreated");
			return Response::json($json,200);			
		}
		catch(MissingMandatoryParametersException $e)
		{
			Log::error($e);
			return Response::json(array("error" => "Input parameter token is missing"),401);
		}
		catch(Exception $e)
		{
			Log::error($e);
			return Response::json(array("error" => "Internal error occured"),500);			
		}
	}*/
	
	
	/*public function loginWithFbToken()
	{
	
		try
		{
			$token = CommonHelper::mandatoryInput("token");
			
			Log::debug("Facebook access token: ".$token);
			$ch = CommonHelper::curl("https://graph.facebook.com/me");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$token));
			$json=curl_exec($ch);
			if(curl_errno($ch))
			{
				Log::error("curl error: ".curl_error($ch).", info: ".print_r(curl_getinfo($ch),true));
				curl_close($ch);
				throw new IOException("Error while accessing facebook");
			}
			curl_close($ch);
			Log::debug("Facebook JSON: ".$json);
			$decoded_json = json_decode($json);
			
			if (isset($decoded_json->error))
			{
				Log::warning("Token ".$token." is not correct. Rejecting it");
				return Response::json(array("error" => "Access token is not correct"),401);
			}
			
			if (isset($decoded_json->id))
			{
				$user = FacebookProfile::where("user_id",$decoded_json->id)->first();
			}
			else
			{
				//error because json response from FB does not give either username or email
				throw new Exception('FB token response with incorrect data');
				
			}
			
			if (!$user)
			{
				//user does not exist in the database. Create a new one
				Log::debug("User not found. Performing sign up");
				$user = new FacebookProfile();
				
				if (isset($decoded_json->username))
				{
					$user->username = $decoded_json->username;
				}			
				
				if (isset($decoded_json->email))
				{
					$user->email = $decoded_json->email;
				}			
				
				if (isset($decoded_json->first_name))
				{
					$user->first_name = $decoded_json->first_name;
				}
				
				if (isset($decoded_json->last_name))
				{
					$user->last_name = $decoded_json->last_name;
				}
				
				if (isset($decoded_json->name))
				{
					$user->full_name = $decoded_json->name;
				}
				
				if (isset($decoded_json->gender))
				{
					$user->gender = $decoded_json->gender;
				}
				
				if (isset($decoded_json->birthday))
				{
					$user->birthday = $decoded_json->birthday;
				}
				
				if (isset($decoded_json->hometown))
				{
					$user->hometown = $decoded_json->hometown;
				}
				
				if (isset($decoded_json->link_profile))
				{
					$user->link_profile = $decoded_json->link_profile;
				}
				
				if (isset($decoded_json->location))
				{
					$user->location = $decoded_json->location;
				}
				
				if (isset($decoded_json->relationship_status))
				{
					$user->relationship = $decoded_json->relationship_status;
				}
				
				if (isset($decoded_json->verified))
				{
					$user->verified = $decoded_json->verified;
				}
				
				//calculate number of friends
				$ch = CommonHelper::curl("https://graph.facebook.com/me/friends");
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$token));
				$json=curl_exec($ch);
				if(curl_errno($ch))
				{
					Log::error("curl error: ".curl_error($ch).", info: ".print_r(curl_getinfo($ch),true));
					curl_close($ch);
					throw new IOException("Error while accessing facebook friends API");
				}
				curl_close($ch);
				Log::debug("Facebook Friends JSON: ".$json);
				$decoded_jsonFriends = json_decode($json);
				$arrayFriends = $decoded_jsonFriends->data;
				$num_friends = count($arrayFriends);
				$user->friends_count=$num_friends;
				$user->save();
				Log::debug("User ".$user->email." saved");
			}
			else
			{
				//update database info regarding the user
				
			}
		}
		catch(MissingMandatoryParametersException $e)
		{
			Log::error($e);
			return Response::json(array("error" => "Input parameter token is missing"),401);
		}
		catch(Exception $e)
		{
			Log::error($e);
			return Response::json(array($e->getMessage()),500);			
		}
		
		
	}*/
	
	public function getUsersList()
	{
		try
		{
			$userList = User::select("users.*")->paginate(10)->toArray();

			$statusCode = 200;
			$response = Response::json($userList, $statusCode);    
            return $response;
		}
		catch (Exception $ex)
		{
			$statusCode = 400;
			$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
			return $json;
		}
	}	
	
	
	private function updateNumberOfFriends(&$facebookProfile, $accessToken) {
		
		$ch = CommonHelper::curl("https://graph.facebook.com/v2.1/me/friends?offset=5000&limit=5000");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
		$jsonfriends=curl_exec($ch);
		if(curl_errno($ch))
		{
			Log::error("curl error: ".curl_error($ch).", info: ".print_r(curl_getinfo($ch),true));
			curl_close($ch);
			throw new IOException("Error while accessing facebook");
		}
		curl_close($ch);
		Log::debug("Facebook friends JSON: ".$jsonfriends);
		$decoded_jsonfriends = json_decode($jsonfriends);
		
		if (isset($decoded_jsonfriends->summary))
		{
			$facebookProfile->num_friends = $decoded_jsonfriends->summary->total_count;
		}
		
	}
	
	private function downloadAndSaveFacebookProfileImage(&$facebookProfile, $accessToken) {
			
		Log::debug("Obtaining facebook profile image");
		$fp = tmpfile();
		$tmpFilename = stream_get_meta_data($fp)['uri'];
		Log::debug("Temporary filename: ".$tmpFilename);
		$ch = CommonHelper::curl("https://graph.facebook.com/me/picture");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$jsonimage = curl_exec($ch);
		if(curl_errno($ch))
		{
			Log::warning("Error downloading profile picture. Setting the default one");
			curl_close($ch);
			fclose($fp);
			$facebookProfile->image = 'default.png';
		}
		else
		{
			Log::debug("Picture downloaded");
			curl_close($ch);
			
			Log::debug("Facebook Image JSON: ".$jsonimage);
			
			$extension = "png";
			$facebookProfile->image = md5_file($tmpFilename).".".$extension;
			$destinationFileName = 'images/users/'.$facebookProfile->image;
			Log::debug("Destination file name: ".$destinationFileName);
			$uploadSuccess = rename($tmpFilename, $destinationFileName);
			Log::debug("rename() success: ".$uploadSuccess);
			fclose($fp);
		}
			
	
	}
	
private function updateFacebookProfile($facebook_account, $accesToken, $company_id, $user_id) {
		
		Log::debug("Update Facebook profile start");

		$facebook_profile = FacebookProfile::where("company_id", $company_id)->where("user_id", $user_id)->first();
		if ( $facebook_profile ) {
		
			Log::debug("Facebook profile founded: " . $facebook_profile->id_fb);
		
		} else {
		
			$facebook_profile = new FacebookProfile();
			Log::debug("New Facebook profile: " . $facebook_account['id']);
			
		}
		
		$facebook_profile->user_id = $user_id;
		if (isset($facebook_account['username']))
		{
			$facebook_profile->username = $facebook_account['username'];
		}
		$facebook_profile->company_id = $company_id;
		
		$facebook_profile->user_fb_token = $accesToken;
		$facebook_profile->id_fb = $facebook_account['id'];
		
		if (isset($facebook_account['name']))
		{
			$facebook_profile->name = $facebook_account['name'];
		}

		if (isset($facebook_account['first_name']))
		{
			$facebook_profile->first_name = $facebook_account['first_name'];
		}
	
		if (isset($facebook_account['middle_name']))
		{
			$facebook_profile->middle_name = $facebook_account['middle_name'];
		}
	
		if (isset($facebook_account['last_name']))
		{
			$facebook_profile->last_name = $facebook_account['last_name'];
		}
	
		if (isset($facebook_account['gender']))
		{
			$facebook_profile->gender = $facebook_account['gender'];
		}
	
		if (isset($facebook_account['email']))
		{
			$facebook_profile->email = $facebook_account['email'];
		}					
		else
		{
			$facebook_profile->email = $facebook_account['id']."@facebook.com";
		}
		
		if (isset($facebook_account['birthday']))
		{
			$facebook_profile->birthdate = $facebook_account['birthday'];
		}
	
		if (isset($facebook_account['verified']))
		{
			$facebook_profile->verified = $facebook_account['verified'];
		}
	
		if (isset($facebook_account['timezone']))
		{
			$facebook_profile->timezone = $facebook_account['timezone'];
		}
	
		if (isset($facebook_account['hometown']))
		{
			$facebook_profile->hometown = $facebook_account['hometown']['name'];
		}	
	
		if (isset($facebook_account['locale']))
		{
			$facebook_profile->locale = $facebook_account['locale'];
		}
		
		$facebook_profile->password = Hash::make(Config::get("facebook")["secret_password"]);
		
		$this->updateNumberOfFriends($facebook_profile, $accesToken);
			
		$this->downloadAndSaveFacebookProfileImage($facebook_profile, $accesToken);
		
		$facebook_profile->save();
		
		Log::debug("Update Facebook profile end");		
	
	}
	
	private function joinContestFromMobile($ownerId, $contestId) {
	
	
		Log::debug("joinContestFromWebsite()");
	
		// ATTACH IF ROW DOESN' T EXIST
		$contest_2 = Contest::with(
						array(
							'users' => function($query) use ($ownerId) {
											$query->where('user_id', '=', $ownerId);
										}
							)
					)->where('id', $contestId)->first();
		
		if ( $contest_2->users->count() == 0 ) {
			$contest_2->users()->attach($ownerId, array('login_mobile' => new DateTime('now')));
			Log::debug("joinContestFromWebsite() done");
		} else {
			$contest_2->users()->updateExistingPivot($ownerId, array('login_mobile' => new DateTime('now')), false);
			Log::debug("joinContestFromWebsite() updated");
		}
		//
	
	}
	
	public function loginWithFacebookToken() {
	
		$idFromCookie = Cookie::get('user_id');
	
		Log::debug("loginWithFacebookToken() Request inputs: " . print_r( Input::all(), true ) );
	
		$token = CommonHelper::mandatoryInput("token");	
		
		$client_id = CommonHelper::mandatoryInput("client_id");	
		
		$device_token = CommonHelper::optionalInput("device_token");	
		
		$user_name = CommonHelper::optionalInput("user_name");
		
		$user_surname = CommonHelper::optionalInput("user_surname");
		
		$user_email = CommonHelper::optionalInput("email");
		
		$user_phone = CommonHelper::optionalInput("phone_number");
		
		// Get company ID
		$company_id = CommonHelper::mandatoryInput("company_id");
		
		if (isset($device_token)) {
			Log::debug("Device token: ".$device_token);
		}
		
		$ch = CommonHelper::curl("https://graph.facebook.com/me");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$token));
		$json=curl_exec($ch); $accountResponse = $json;
		if(curl_errno($ch))
		{
			Log::error("curl error: ".curl_error($ch).", info: ".print_r(curl_getinfo($ch),true));
			curl_close($ch);
			throw new IOException("Error while accessing facebook");
		}
		curl_close($ch);
		Log::debug("Facebook JSON: ".$json);
		$decoded_json = json_decode($json);
		
		if (isset($decoded_json->error))
		{
			Log::warning("Token ".$token." is not correct. Rejecting it");
			return Response::json(array("error" => "Access token is not correct"),401);
		}
		
		if (isset($decoded_json->id))
		{
		
					
			if ( $idFromCookie ) {
		
				$user = User::where('id', $idFromCookie)->first();
				Log::debug("User retrieved from cookie: " . $user->email);
		
			} else {
				
				/*
				if (isset($decoded_json->email))
				{
					$user = User::where('email', $decoded_json->email)->first();
				}
				*/
				$user = User::where('email', $decoded_json->id . '@f.com')->first();
				
				// There is a user 
				if ($user) {
					
					$twitterProfileConnected = TwitterProfile::where('user_id', $user->id)->where('company_id', $company_id)->first();
					if ( $twitterProfileConnected ) {
						$twitterProfileConnected->expired = 1;
						$twitterProfileConnected->save();
					}
					
					Log::debug("User already present: " . $user->id);
				
				// Create new user
				} else {
				
					$user = new User();
					Log::debug("New user created: " . $user->id);
				}
/*				
					if ( isset($decoded_json->first_name) ){
						$user->name = $decoded_json->first_name;
					}
					if ( isset($decoded_json->last_name) ){
						$user->surname = $decoded_json->last_name;
					}
					if ( isset($decoded_json->email) ) {
						$user->email = $decoded_json->email;
					}					
					else {
						$user->email = $decoded_json->id."@facebook.com";
					}
				
					$user->password = Hash::make(Config::get("facebook")["secret_password"]);
				
					$user->save();
					Log::debug("New user created: " . $user->id);
					
				}*/
				
				$user->email = $decoded_json->id . '@f.com';
				
				$user->password = Hash::make(Config::get("facebook")["secret_password"]);
				
				if ( isset($user_name) ){
					$user->name = $user_name;
				} elseif ( isset($decoded_json->first_name) ) {
					$user->name = $decoded_json->first_name;
				}
				
				if ( isset($user_surname) ){
					$user->surname = $user_surname;
				} elseif ( isset($decoded_json->last_name) ) {
					$user->surname = $decoded_json->last_name;
				}
				
				if ( isset($user_email) ) {
					$user->survey_email = $user_email;
				}
				
				if ( isset($user_phone) ) {
					$user->survey_phone = $user_phone;
				}
				
				$user->save();				
				
				Cookie::queue('user_id', $user->id);
				Log::debug("Created new Cookie for USER ID: " . $user->id);
			
			
			}
			
			
			
			// Get contest ID
			$contest_id = CommonHelper::mandatoryInput("contest_id");
			
			// Update facebook profile with company ID 
			$this->updateFacebookProfile(json_decode($accountResponse, true), $token, $company_id, $user->id);
		
			// Update pivot table contest_user with user ID and contest ID
			$this->joinContestFromMobile($user->id, $contest_id);
		
			// Update pivot table company_user with user ID and company ID
			Log::debug("Attempting to attach company: " . $company_id . " with user: " . $user->id);
			$company_user = Company::find($company_id)->belongsToManyUsers()->where('user_id', $user->id)->first();

			if ( !$company_user) {
				Log::debug("Many to many relationship not found ... attaching");
				Company::find($company_id)->belongsToManyUsers()->attach($user->id, array('token' =>  $device_token));
				Log::debug("Attach done");
			} else {
				Log::debug("Many to many relashinship found ... updating");
				Company::find($company_id)->belongsToManyUsers()->updateExistingPivot($user->id, array('token' =>  $device_token), false);
				Log::debug("Update done");
			}
			
			$userData = array("username" => $user->email, "password" => Config::get("facebook")["secret_password"], "client" => $client_id);
			$json = CommonHelper::getAccessToken($userData,true);
			//Log::debug("Response for user ".$user->email.": ".json_encode($json, JSON_PRETTY_PRINT));
			$new = array_merge( json_decode(json_encode($json), true), array( "id_user" => $user->id ) );
			Log::debug("Response for user ".$user->id.": ".json_encode($new, JSON_PRETTY_PRINT));
			
			
			return Response::json($new,200);
						
		}
		else
		{
			//error because json response from FB does not give either username or email
			throw new Exception('FB token response with incorrect data');
			
		} 
		
	}
		
	public function loginWithFacebookToken_old()
	{
		try
		{
		
			Log::debug("loginWithFacebookToken() Request inputs: " . print_r( Input::all(), true ) );
			
			
			$token = CommonHelper::mandatoryInput("token");	
			
			$device_token = CommonHelper::optionalInput("device_token");
			
			$client_id = CommonHelper::mandatoryInput("client_id");
						
			if (isset($device_token))
			{
				Log::debug("Device token: ".$device_token);
			}
			
			//$ch = CommonHelper::curl("https://graph.facebook.com/me?fields=id,name,username,email,first_name,last_name,gender,birthday");
			$ch = CommonHelper::curl("https://graph.facebook.com/me");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$token));
			$json=curl_exec($ch); $accountResponse = $json;
			if(curl_errno($ch))
			{
				Log::error("curl error: ".curl_error($ch).", info: ".print_r(curl_getinfo($ch),true));
				curl_close($ch);
				throw new IOException("Error while accessing facebook");
			}
			curl_close($ch);
			Log::debug("Facebook JSON: ".$json);
			$decoded_json = json_decode($json);
			
			if (isset($decoded_json->error))
			{
				Log::warning("Token ".$token." is not correct. Rejecting it");
				return Response::json(array("error" => "Access token is not correct"),401);
			}
			
			if (isset($decoded_json->id))
			{
			
				
				$user = User::where('email', $decoded_json->email)->first();
				
				
				if (!$user)
				{
				
					$fp = FacebookProfile::where("id_fb", $decoded_json->id)->first();
					
					if ( $fp ) {
			
						//
					
					} else {
					
						//utente che si logga per la prima volta
						Log::debug("User not found. Performing sign up");
						$user = new User();

						
						/*
						$user->id_fb = $decoded_json->id;
						
						$user->user_fb_token = $token;
						
						if (isset($decoded_json->username))
						{
							$user->username = $decoded_json->username;
						}
						
						if (isset($decoded_json->name))
						{
							$user->name = $decoded_json->name;
						}
						
						if (isset($decoded_json->email))
						{
							$user->email = $decoded_json->email;
						}					
						else if (isset($decoded_json->id))
						{
							$user->email = $decoded_json->id."@facebook.com";
						}
						
						if (isset($decoded_json->first_name))
						{
							$user->first_name = $decoded_json->first_name;
						}
						
						if (isset($decoded_json->middle_name))
						{
							$user->middle_name = $decoded_json->middle_name;
						}
						
						if (isset($decoded_json->last_name))
						{
							$user->last_name = $decoded_json->last_name;
						}
						
						if (isset($decoded_json->gender))
						{
							$user->gender = $decoded_json->gender;
						}
						
						if (isset($decoded_json->birthday))
						{
							$user->birthdate = $decoded_json->birthday;
						}
						
						if (isset($decoded_json->verified))
						{
							$user->verified = $decoded_json->verified;
						}
											
						if (isset($decoded_json->timezone))
						{
							$user->timezone = $decoded_json->timezone;
						}
						
						if (isset($decoded_json->hometown))
						{
							
							$user->hometown = $decoded_json->hometown->name;
						}
						
						if (isset($decoded_json->locale))
						{
							$user->locale = $decoded_json->locale;
						}
						
						if (isset($device_token))
						{
							$user->token = $device_token;
						}
						
						$ch = CommonHelper::curl("https://graph.facebook.com/v2.1/me/friends?offset=5000&limit=5000");
						curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$token));
						$jsonfriends=curl_exec($ch);
						if(curl_errno($ch))
						{
							Log::error("curl error: ".curl_error($ch).", info: ".print_r(curl_getinfo($ch),true));
							curl_close($ch);
							throw new IOException("Error while accessing facebook");
						}
						curl_close($ch);
						Log::debug("Facebook friends JSON: ".$jsonfriends);
						$decoded_jsonfriends = json_decode($jsonfriends);
						
						if (isset($decoded_jsonfriends->summary))
						{
							$user->num_friends = $decoded_jsonfriends->summary->total_count;
						}
						
						$user->password = Hash::make(Config::get("facebook")["secret_password"]);
						
						Log::debug("Obtaining user profile image");
						$fp = tmpfile();
						$tmpFilename = stream_get_meta_data($fp)['uri'];
						Log::debug("Temporary filename: ".$tmpFilename);
						$ch = CommonHelper::curl("https://graph.facebook.com/me/picture");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
						curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$token));
						curl_setopt($ch, CURLOPT_FILE, $fp);
						curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
						$jsonimage = curl_exec($ch);
						if(curl_errno($ch))
						{
							Log::warning("Error downloading profile picture. Setting the default one");
							curl_close($ch);
							fclose($fp);
							$user->image = 'default.png';
						}
						else
						{
							Log::debug("Picture downloaded");
							curl_close($ch);
							
							Log::debug("Facebook Image JSON: ".$jsonimage);
							
							$extension = "png";
							$user->image = md5_file($tmpFilename).".".$extension;
							$destinationFileName = 'images/users/'.$user->image;
							Log::debug("Destination file name: ".$destinationFileName);
							$uploadSuccess = rename($tmpFilename, $destinationFileName);
							Log::debug("rename() success: ".$uploadSuccess);
							fclose($fp);
						}
						*/
						//$user->save();
						//Log::debug("User ".$user->email." saved");
					
					}
				}
				else
				{
					//utente esiste nel db. Aggiornare dati
					
					/*
					$user->user_fb_token = $token;
					
					if (isset($decoded_json->id))
					{
						$user->id_fb = $decoded_json->id;
					}
					
					if (isset($decoded_json->name))
					{
						$user->name = $decoded_json->name;
					}
					
					if (isset($decoded_json->first_name))
					{
						$user->first_name = $decoded_json->first_name;
					}
					
					if (isset($decoded_json->middle_name))
					{
						$user->middle_name = $decoded_json->middle_name;
					}
					
					if (isset($decoded_json->last_name))
					{
						$user->last_name = $decoded_json->last_name;
					}
					
					if (isset($decoded_json->gender))
					{
						$user->gender = $decoded_json->gender;
					}
					
					if (isset($decoded_json->email))
					{
						$user->email = $decoded_json->email;
					}
					
					if (isset($decoded_json->birthday))
					{
						$user->birthdate = $decoded_json->birthday;
					}
					
					if (isset($decoded_json->verified))
					{
						$user->verified = $decoded_json->verified;
					}
					
					if (isset($decoded_json->timezone))
					{
						$user->timezone = $decoded_json->timezone;
					}
					
					if (isset($decoded_json->hometown))
					{
						$user->hometown = $decoded_json->hometown->name;
					}
					
					if (isset($decoded_json->locale))
					{
						$user->locale = $decoded_json->locale;
					}
					
					if (isset($device_token))
					{
					$user->token = $device_token;
					}
					
					$ch = CommonHelper::curl("https://graph.facebook.com/v2.1/me/friends?offset=5000&limit=5000");
					curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$token));
					$jsonfriends=curl_exec($ch);
					if(curl_errno($ch))
					{
						Log::error("curl error: ".curl_error($ch).", info: ".print_r(curl_getinfo($ch),true));
						curl_close($ch);
						throw new IOException("Error while accessing facebook");
					}
					curl_close($ch);
					Log::debug("Facebook friends JSON: ".$jsonfriends);
					$decoded_jsonfriends = json_decode($jsonfriends);
					
					if (isset($decoded_jsonfriends->summary))
					{
						$user->num_friends = $decoded_jsonfriends->summary->total_count;
					}
					
					Log::debug("Obtaining user profile image");
					$fp = tmpfile();
					$tmpFilename = stream_get_meta_data($fp)['uri'];
					Log::debug("Temporary filename: ".$tmpFilename);
					$ch = CommonHelper::curl("https://graph.facebook.com/me/picture");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$token));
					curl_setopt($ch, CURLOPT_FILE, $fp);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
					$jsonimage = curl_exec($ch);
					if(curl_errno($ch))
					{
						Log::warning("Error downloading profile picture. Setting the default one");
						curl_close($ch);
						fclose($fp);
						$user->image = 'default.png';
					}
					else
					{
						Log::debug("Picture downloaded");
						curl_close($ch);
						
						Log::debug("Facebook Image JSON: ".$jsonimage);
						
						$extension = "png";
						$user->image = md5_file($tmpFilename).".".$extension;
						$destinationFileName = 'images/users/'.$user->image;
						Log::debug("Destination file name: ".$destinationFileName);
						$uploadSuccess = rename($tmpFilename, $destinationFileName);
						Log::debug("rename() success: ".$uploadSuccess);
						fclose($fp);
					}
					*/
				
				}
				
				
				/////JSON RESPONSE ONLY IF TOKEN FETCH ID OF THE USER
				//$cachedPassword = $user->password;
				//Log::debug("Cached password: ".$cachedPassword);
				$tmpPassword = Config::get("facebook")["secret_password"].time();
				//$user->password = Hash::make($tmpPassword);
				//Log::debug("Tmp password: ".$tmpPassword.". Performing implicit login of user ".$user->email);
				Log::debug("Tmp password: ".$tmpPassword.". Performing implicit login of user ");
				/*
				if (isset($device_token) && ($device_token != ''))
				{
					if (strpos($device_token,'Android_') !== false)
					{
						$userData = array("username" => $user->email, "password" => $tmpPassword, "client" => "Droid");
					}
					else if (strpos($device_token,'IOS_') !== false)
					{
						$userData = array("username" => $user->email, "password" => $tmpPassword, "client" => "IPhone");
					}
				}
				else
				{
					$userData = array("username" => $user->email, "password" => $tmpPassword);
				}
				*/
				$userData = array("username" => $decoded_json->email, "password" => $tmpPassword, "client" => $client_id);
				
				//$userData = array("username" => $user->username, "password" => $tmpPassword);
				$user->save();
				$json = CommonHelper::getAccessToken($userData,true);
				Log::debug("Response for user ".$decoded_json->email.": ".json_encode($json, JSON_PRETTY_PRINT));
				//$user->password = $cachedPassword;
				//$user->save();
				//Log::debug("Cached password recreated");
				
				//pass the user Id with the access token
				//$response = Response::json(array('description'=>$description, 'errorCode'=>404),$statusCode);
				//$arrne = array('id' => $user->id);
				//$json["user_id"] = $user->id;
				//array_push( $json, $arrne);
				//$arraymerge = $json + $arrne;
				$new = array_merge( json_decode(json_encode($json), true), array( "id_user" => $user->id ) );
				//$json['id'] = $user->id;
				Log::debug("Response for user ".$user->id.": ".json_encode($new, JSON_PRETTY_PRINT));
				
				
				
				// Get company ID
				$company_id = CommonHelper::mandatoryInput("company_id");
				
				// Get contest ID
				$contest_id = CommonHelper::mandatoryInput("contest_id");
				
				// Update facebook profile with company ID 
				$this->updateFacebookProfile(json_decode($accountResponse, true), $token, $company_id, $user->id);
			
				// Update pivot table contest_user with user ID and contest ID
				$this->joinContestFromMobile($user->id, contest_id);
			
				// Update pivot table company_user with user ID and company ID
				Log::debug("Attempting to attach company: " . $company_id . " with user: " . $user->id);
				$company_user = Company::find($company_id)->belongsToManyUsers()->where('user_id', $user->id)->first();
	
				if ( !$company_user) {
					Log::debug("Many to many relationship not found ... attaching");
					Company::find($company_id)->belongsToManyUsers()->attach($user->id, array('token' =>  $device_token));
					Log::debug("Attach done");
				} else {
					Log::debug("Many to many relashinship found ... does not attach");
				}
				
				
				
				
				
				return Response::json($new,200);
						
			}
			else
			{
				//error because json response from FB does not give either username or email
				throw new Exception('FB token response with incorrect data');
				
			}
			
			
			
			
			
		}
		catch(MissingMandatoryParametersException $e)
		{
			Log::error($e);
			return Response::json(array("error" => "Input parameter token is missing"),401);
		}
		catch(Exception $e)
		{
			Log::error($e);
			return Response::json(array($e->getMessage()),500);			
		}
	}
	
	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function loginWithFacebook()
	{
            //Inputs; username:email, realusername:username, password:password, & other oauth fields
                $inputs=Input::all();
                if(!isset($inputs['username'])||!isset($inputs['password']))
                {
                    $description='Required Params Not Found';
                    $statusCode = 200;
                    $returnResponse = array('description'=>$description,'errorCode'=>400);
                    $response = Response::json($returnResponse,$statusCode);
                    return $response;
                }
                $username = $inputs["username"];
                Log::debug("loginWithFacebook(username=".$username.")");
                if(User::where('email',$inputs['username'])->count()>0)
                {
                	Log::debug("User ".$username." found. Calling performAccessTokenFlow()");
                    return AuthorizationServer::performAccessTokenFlow();
                }
                

                if(User::onlyTrashed()->where('email',$inputs['username'])->count()>0)
                {
                    $description='User Deactivated';
                    $statusCode = 200;
                    $returnResponse = array('description'=>$description,'errorCode'=>410);
                    $response = Response::json($returnResponse,$statusCode);
                    return $response;
                }
                if(User::where('username',$inputs['realusername'])->count()>0)
                {
                    $description='Userid/Email already exists';
                    $statusCode = 200;
                    $returnResponse = array('description'=>$description,'errorCode'=>411);
                    $response = Response::json($returnResponse,$statusCode);
                    return $response;
                }
                $user=new User();
                $user->username=$inputs['realusername'];
                $user->email=$inputs['username'];
                $user->password=Hash::make($inputs['password']);
                $user->save();
                
                $userSettings = new Setting;
                $userSettings -> user_id = $user->id;
                $userSettings -> facebook = 0;
                $userSettings -> twitter = 0;
                $userSettings -> youtube = 0;
                $userSettings -> save();

                return AuthorizationServer::performAccessTokenFlow();
	}
    
        public function loginWithTwitter()
        {
                $inputs=Input::all();
                if(!isset($inputs['username'])||!isset($inputs['password']))
                {
                    $description='Required Params Not Found';
                    $statusCode = 200;
                    $returnResponse = array('description'=>$description,'errorCode'=>400);
                    $response = Response::json($returnResponse,$statusCode);
                    return $response;
                }

                if(User::where('email',$inputs['username'])->count()>0)
                {
                    return AuthorizationServer::performAccessTokenFlow();
                }
                
                if(User::onlyTrashed()->where('email',$inputs['username'])->count()>0)
                {
                    $description='User Deactivated';
                    $statusCode = 200;
                    $returnResponse = array('description'=>$description,'errorCode'=>410);
                    $response = Response::json($returnResponse,$statusCode);
                    return $response;
                }
                
                $user=new User();
                $user->username=$inputs['realusername'];
                $user->email=$inputs['username'];
                $user->password=Hash::make($inputs['password']);

                $user->save();
                
                $userSettings = new Setting;
                $userSettings -> user_id = $user->id;
                $userSettings -> facebook = 0;
                $userSettings -> twitter = 0;
                $userSettings -> youtube = 0;
                $userSettings -> save();

				Log::debug("Performing implicit login on email: ".$user->email);
				$userData = array("username" => $user->email, "password" => password);
				$json = CommonHelper::getAccessToken($userData,true);
				Log::debug("Response for user ".$user->email.": ".json_encode($json, JSON_PRETTY_PRINT));
				return Response::json($json,200);
				
                //return AuthorizationServer::performAccessTokenFlow();
        }
        public function termOfServices()
        {
                return View::make('termsOfServices');
        }
        public function privacyPolicy()
        {
                return View::make('privacyPolicy');
        }
        
        public function forgotPasswordAPI()
        {
            $rules = array(
              'email' => 'required|email',
            );
            
            $validator = Validator::make(Input::all(), $rules);
            //$password = str_random(15);
            if($validator -> fails())
            {                
                $description='Incorrect Email Format';
                $statusCode = 200;
                $returnResponse = array('description'=>$description,'errorCode'=>$statusCode);
                $response = Response::json($returnResponse,$statusCode);
                return $response;
            }
            else
            {
                $email = Input::get('email');
                $user = User::where('email', $email)->first();
                if($user)
                {
                    $password = str_random(15);
                    $token = str_random(32);
                    
                    $update = User::where('email', $email)
                        ->update(array('token' => $token ));
                    if($update)
                    {
                        Mail::send('emails.forgotPassword', array('email'=>$email,'token'=>$token), function($message){
                            $message->to(Input::get('email'))->subject('JMT FORGOT PASSWORD');
                         });
                        $description='Confirmation Email Sent. Check your inbox';
                        $statusCode = 200;
                        $returnResponse = array('description'=>$description,'errorCode'=>$statusCode);
                        $response = Response::json($returnResponse,$statusCode);
                        return $response;
                         //return Redirect::to('corporate')->with('message','Email has been sent to your email-address');
                    }
                    else
                    {
                        $description='Error in Password Reset. Please Try Again';
                        $statusCode = 200;
                        $returnResponse = array('description'=>$description,'errorCode'=>400);
                        $response = Response::json($returnResponse,$statusCode);
                        return $response;
                    }
                }
                else
                {
                    $description='Invalid Email';
                    $statusCode = 200;
                    $returnResponse = array('description'=>$description,'errorCode'=>410);
                    $response = Response::json($returnResponse,$statusCode);
                    return $response;
                }
            }
                
        }

/*****************************************************/

public function ResetPasswordForgotAPI()
        {   
            $input = Input::all();
            
            $email = $input['email'];
            $token = $input['token'];
            
            $password = str_random(15);
            
            $result = User::where('email', $email)->where('token', $token)->first();
            if($result)
            {
                $mail = Mail::send('emails.changePassword', array('password'=>$password), function($message){
                   $message->to(Input::get('email'))->subject('Password Reset: JMT');
                });
                
                if($mail)
                {
                    $update = User::where('email', $email)
                        ->update(array('password' => Hash::make($password) ));
                    
                    return Redirect::to('www.joinmethere.tv')->with('message','<script>alert("New Password has been sent to your email-address");</script>');
                }
                else
                {
                    return Redirect::to('www.joinmethere.tv/jmtvapi/')->with('message','<script>alert("Error: try again");</script>');
                }
            }
        }
		
		
		public function locationList()
		{
			try
			{
								
				
				$statusCode = 200;
				$response = Response::make(Location::all(),$statusCode);
				return $response;
			}
			catch (Exception $ex)
			{
				$statusCode = 400;
				$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
				return $json;
			}
		}
		
        
        /*************   Controller fnuction for web ************/
        public function index()
	{
            // load the view and pass the nerds
            
            return View::make('index');
            
         }
        
        public function getLogin()
	{
            // create our user data for the authentication
            $userdata = array(
                'username' 	=> Input::get('username'),
                'password' 	=> Input::get('password')
            );
            
            //Set different Model for admin login
            
            // attempt to do the login
            if (Auth::attempt($userdata)) 
            {
                $username = $userdata['username'];
                $user = DB::table('users')->where('username', $username)->first();
                $user_id = $user -> id;
                $company_id = $user -> company_id;
                
                Session::put('userid', $user_id);
                Session::put('companyid', $company_id);
                
                return Redirect::to('company/profile');
            } 
            else
            {
                // validation not successful, send back to form
                return Redirect::to('//')->with('message', 'Either username or password is wrong. please try again!');
            }
        }
        
        public function create()
	{
		return View::make('signup');
	}
        
        public function forget()
	{
		return View::make('forget');
	}
        
        public function getLogout() {
             Auth::logout();
             Session::flush();
             return Redirect::to('/');
        }
        
        
        public function forgotPassword()
        {
            $emailAdd = Input::get('emailAdd');
            $companyUser = DB::table('companys')->where('email', $emailAdd)->first();
            
            if($companyUser)
            {   
                Mail::send('emails.confirmEmail', array('email' => $emailAdd), function($message){
                    $message->to(Input::get('emailAdd'))->subject('Confirm password reset!');
                });
                return Redirect::to('/');
                //return Redirect::to('forget')->with('message', '<div class="Errormessage" style="background-color:#ccc;"><p style="color:#000;">Password is successfully changed</p></div>');
            }
            else
            {
                return Redirect::to('forget')->with('message', '<div class="Errormessage"><p>Email Address does not exist in the record</p></div>');
            }
        }
        
        public function showReset()
        {
            return View::make('passwordReset');
        }
        
        public function resetPassword()
        {
            $rules = array(
                'password'=>'required|alpha_num|min:6|confirmed',
                'password_confirmation'=>'required|alpha_num|min:6'
            );
            
            $validator = Validator::make(Input::all(), $rules);
            
            if($validator -> passes())
            {
                $password = Input::get('password');
                $email = Input::get('email');
                
                $companyUser = DB::table('companys')->where('email', $email)->first();
                if($companyUser)
                {
                    $name = $companyUser -> name;
                    
                    DB::table('companys')
                    ->where('email', $email)
                    ->update(array('password' => Hash::make($password) ));
                       
                    
                    Mail::send('emails.welcome', array('name' => $name , 'password' => $password), function($message){
                              $message->to(Input::get('email'))->subject('password reset!');
                    });
                
                    return Redirect::to('/');   
                }
                else
                {
                    return Redirect::to('company/passwordReset')->with('message', 'Email address does not exist in the record');
                }
                
            }
            else
            {
                return Redirect::to('company/passwordReset')->with('message', 'Following errors occurs:')->withErrors($validator)->withInput();
            }
        }
	
        
        public function showRegister()
        {
           return View::make('register'); 
        }
		
	public function buzzMyBrandLogin($id) {
	
		
		return View::make('buzzMyBrandLogin')->with('contest_id', $id);
	}
	
	public function buzzMyBrandPerformLogin($id) {
		
		$contest = Contest::find($id);
		
		$rules = array(
			'email'=>'required',
			'password'=>'required',
		);
		
		$validator = Validator::make(Input::all(), $rules);
		
		if ($validator->fails()) {
			return Redirect::to('buzz-my-brand-login/' . $id)->with('message', 'The following errors occurred')->withErrors($validator);
		}
            
		if (Request::isMethod('post')) {
		
			$userData = array(
				'username' => Input::get('email'),
				'password' => Input::get('password')
			);
			
			$user = UserHelper::loginUser($userData);

			if ($user) {
			
                Cookie::queue('user_id', $user->id);
				
				$fp = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
				
				if ( $fp ) {
					$fp->expired = 1;	
					$fp->save();
				}
				
				Cookie::queue('twitterLogin', 0);
				Cookie::queue('instagramLogin', 0);

				
				return Redirect::to('login/'. $id);
				
			} else {
			
				return Redirect::to('buzz-my-brand-login/' . $id)->with('message', 'Bad Credentials')->withErrors(null);
            }
        
		}

	}
	

		
}