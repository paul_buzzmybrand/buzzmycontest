<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AdminController extends BaseController {
        
	public function index()
	{
	   return View::make('admin'); 
	}
        
        public function getSignin()
        {
            $userdata = array(
                'email' => Input::get('username'),
                'password' => Input::get('password'),
            );

            $isAjax = Input::get('ajax') == "1";
            
            //Config::set('auth.model', 'Admin');
            // attempt to do the login
			//Log::debug("getSignin() Request inputs: " . print_r( Input::all(), true ) );
			$user = User::where('email', Input::get('username'))->whereNotNull("company_id")->first();
			Log::debug("User id: ".$user);
			
			if (!$user) {
				return Response::json(['success' => 0, 'message' => Lang::get('admin.signin_wrong_credentials')]);	
			}	
			
			try {
				if (Auth::attempt(['email' => Input::get('username'), 'password' => Input::get('password'), 'company_id' => $user->company_id ])) 
				{
					$UserName = Input::get('username');
					Session::put('username', $UserName);
					
					$admin = Auth::user();


					Log::debug("company id: " . $admin->company_id);
					Log::debug("isSuperAdmin: " . $admin->isSuperAdmin());
					
					if($isAjax) {
						if (Input::get('timezone_offset'))
						{
							$admin->timezone_offset =  Input::get('timezone_offset');
							$admin->save();
						}
						return Response::json(['success' => 1]);
						//creare funzione laravel per il login accessibile da https://www.buzzmybrand.co/dashboard/index
						//In questa funzione ci deve essere 
						//questa funzione deve rimandare a dashboard/index.html
						//return Redirect::to('https://www.buzzmybrand.co/dashboard/index.html');
					}
					
					if ( $admin->isSuperAdmin() )
					{
						return Redirect::to('/admin/external-contests');
					}
					
					$company_id = $admin->company_id;
					
					return Redirect::to('/admin/external-contests/'.$company_id);
				} 
				else
				{
					if($isAjax) {
						return Response::json(['success' => 0, 'message' => Lang::get('admin.signin_wrong_credentials')]);						
					}
				
					return Redirect::to('admin')->with('message', 'Error: Either Username or password is wrong');
				}
			}
			catch (Exception $ex)
			{
				Log::debug("Error during login:".$ex);
				return Redirect::to('admin')->with('message', 'Error: Either Username or password is wrong');
			}
			
            
        }
        
        public function showIntro()
        {
            if(Session::has('username'))
            {
                return View::make('view');
            }    
            else
            {
                return View::make('admin');
            }
        }
        
        public function logout()
        {
			Config::set('auth.model', 'Admin');
        	Auth::logout();
            Session::flush();
            return Redirect::to('/');
        }
}