<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Image;
class PhotoController extends BaseController {

	public function getPhotoInfo($id)
	{
		try
		{
			$photoList = Photo::findOrFail($id);

			return $photoList;
		}
		catch (Exception $ex)
		{
			$statusCode = 400;
			$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
			return $json;
		}
	}

	public function getPhoto($id)
	{
		/*
		$photo = Photo::where('id', $id)->first();
		// create new Intervention Image
		$width = Image::make(base_path()."/public/photos/".$photo->filename)->width();
		$height = Image::make(base_path()."/public/photos/".$photo->filename)->height();
		$heightPlayer = intval((640*$height)/$width);
		$widthPlayer = 640;
		return View::make('photo')->with('photo', $photo)->with('height', $heightPlayer)->with('width', $widthPlayer);
		*/

		$photo = Photo::find($id);
		$contest = $photo->contest;

		$width = Image::make(base_path()."/public/photos/".$photo->filename)->width();
		$height = Image::make(base_path()."/public/photos/".$photo->filename)->height();

		$photo = Photo::with(
						array('user.facebookProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},
						'user.twitterProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},)
					)->where('id', $id)->first();

		return View::make('photo')->with('photo', $photo)->with('height', $height)->with('width', $width);


	}

	public function getJuvePhoto($id)
	{
		$photo = Photo::where('id', $id)->first();
		// create new Intervention Image
		$width = Image::make(base_path()."/public/photos/".$photo->filename)->width();
		$height = Image::make(base_path()."/public/photos/".$photo->filename)->height();
		$heightPlayer = intval((640*$height)/$width);
		$widthPlayer = 640;
		return View::make('photojuve')->with('photo', $photo)->with('height', $heightPlayer)->with('width', $widthPlayer);

	}

	public function getPhotoList($id)
	{
		try
		{
			$limit = Input::get('limit');
			if(!isset($limit))
			{
				$limit = 12;
			}

			$photoListTemp = Photo::select("photos.*")
									->join("contests","contests.id", "=", "photos.contest_id")
									->where('contests.id','=',$id)
									->orderBy('score', 'desc');

			$photoList = $photoListTemp->paginate($limit)->toArray();


			$i = 0;
			$photoListArray = array();
			$photoListArray['total'] = $photoList['total'];
			$photoListArray['per_page'] = $photoList['per_page'];
			$photoListArray['current_page'] = $photoList['current_page'];
			$photoListArray['last_page'] = $photoList['last_page'];
			$photoListArray['from'] = $photoList['from'];
			$photoListArray['to'] = $photoList['to'];

			$contestPhotosArray = array();
			foreach($photoList['data'] as $photos)
			{

				$contestPhotosArray[$i]['id'] = $photos['id'];
				$contestPhotosArray[$i]['name'] = $photos['name'];
				$contestPhotosArray[$i]['filename'] = $photos['filename'];
				$contestPhotosArray[$i]['score'] = $photos['score'];
				$contestPhotosArray[$i]['user_id'] = $photos['user_id'];
				if (isset($photos['contest_id']))
				{
					$contestId = $photos['contest_id'];
					$contestPhotosArray[$i]['contest_id'] = $contestId;
					$contest = DB::table('contests')->where('id', $contestId)->pluck('name');
					$contestPhotosArray[$i]['contestName'] = $contest;

				}
				else
				{
					$contestName = 'MyChannel';
					$contestPhotosArray[$i]['contestName'] = $contestName;
				}


				$i++;
			}
			$photoListArray['data'] = $contestPhotosArray;

			$statusCode = 200;
			$response = Response::json($photoListArray, $statusCode);
            return $response;
			//return $videoListArray;
		}
		catch (Exception $ex)
		{
			$statusCode = 400;
			$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
			return $json;
		}



	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		try
		{


			$limit = Input::get('limit');
			$locationID = Input::get('locationID');
			$title = Input::get('title');

			$externalContest = CommonHelper::mandatoryInput("External_contest");
			$latitude = CommonHelper::optionalInput("Latitude");
			$longitude = CommonHelper::optionalInput("Longitude");

			if ((isset($latitude) && !isset($longitude)) || (!isset($latitude) && isset($longitude)))
			{
				throw new MissingMandatoryParametersException("You need to set both Latitude and Longitude parameters or none of them");
			}

			if(!isset($limit))
			{
				$limit = 12;
			}



			//$videoList = Video::where('location','like',$location.'%')->where('name','like',$title.'%')->orderBy('score','desc');
			//Log::debug("List of video screened by location and title: ".$videoList);

			if ($externalContest == "false")
			{

				//return $videoList[0];
				$photoListTemp = Photo::select("photos.*")
				->where('photos.name','like','%'.$title.'%')
				->join("locations","locations.id", "=", "photos.location_id")
				->join("contests", "contests.id", "=", "photos.contest_id")
				->join("companys","companys.id", "=", "contests.company_id")->where("companys.type_id",Company::TYPE_INTERNAL)
				->orderBy('score', 'desc')->orderBy('created_at', 'desc');
				//$videoListTemp = Video::with('contests')->where('location','like',$location.'%')->where('name','like',$title.'%')->orderBy('score');

				//Order by distance if available
				if (isset($latitude))
				{
					$photoListTemp = $photoListTemp->orderBy(DB::raw("ACOS(COS(RADIANS(90-".$latitude."))*COS(RADIANS(90-latitude))+SIN(RADIANS(90-".$latitude."))*SIN(RADIANS(90-latitude))*COS(RADIANS(".$longitude."-longitude)))*6371"));
				}

				if (!isset($locationID))
				{
					//show only the photos that belong to the location present on the first row of the queryBuilder
					//In this way we can avoid visualization of foreign photos
					$nearestPhoto = $photoListTemp->first();
					$locationID = $nearestPhoto->location_id;
					$photoListTemp = $photoListTemp->where("photos.location_id", $locationID);
					//*************************************************************
					Log::debug("Now we have the locationID: ".$locationID);
				}
				else
				{
					//This condition is executed when the user is doing a Search
					$photoListTemp = $photoListTemp->where("photos.location_id", $locationID);
				}

				$photoList = $photoListTemp->paginate($limit)->toArray();

			}
			else if ($externalContest == "true")
			{

				$photoListTemp = Video::select("photos.*")
				->where('location_id',$locationID)
				->where('name','like','%'.$title.'%')
				->join("locations","locations.id", "=", "photos.location_id")
				->join("contests", "contests.id", "=", "photos.contest_id")
				->join("companys","companys.id", "=", "contests.company_id")->where("companys.type_id",Company::TYPE_EXTERNAL)
				->orderBy('score', 'desc')->orderBy('created_at', 'desc');
				//$videoListTemp = Video::with('contests')->where('location','like',$location.'%')->where('name','like',$title.'%')->orderBy('score');
				$photoList = $photoListTemp->paginate($limit)->toArray();

			}

			$queries = DB::getQueryLog();
			Log::debug("Queries: ".print_r($queries,true));
			Log::debug("Number of photos returned: ".count($photoList));


			/*$statusCode = 200;
			$response = Response::make($videoList,$statusCode);
	        return $response;*/

			$i = 0;
			$photoListArray = array();
			//return $videoListTemp[0]['contests'][0]['id'];
			//dd ($videoListTemp['data'][0]['contests']);
			$photoListArray['total'] = $photoList['total'];
			$photoListArray['per_page'] = $photoList['per_page'];
			$photoListArray['current_page'] = $photoList['current_page'];
			$photoListArray['last_page'] = $photoList['last_page'];
			$photoListArray['from'] = $photoList['from'];
			$photoListArray['to'] = $photoList['to'];

			$contestPhotosArray = array();
			foreach($photoList['data'] as $photos)
			{
				//dd($videos);
				//$contestJMTScore = $videos['score'] + $contestJMTScore;
				$contestPhotosArray[$i]['id'] = $photos['id'];
				$contestPhotosArray[$i]['name'] = $photos['name'];
				$contestPhotosArray[$i]['image'] = $photos['image'];
				$contestPhotosArray[$i]['filename'] = $photos['filename'];
				$contestPhotosArray[$i]['source_fb_url'] = $photos['source_fb_url'];
				$contestPhotosArray[$i]['score'] = $photos['score'];
				$contestPhotosArray[$i]['user_id'] = $photos['user_id'];
				$contestPhotosArray[$i]['created_at'] = $photos['created_at'];
				$contestPhotosArray[$i]['location_id'] = $photos['location_id'];
				$contestPhotosArray[$i]['user_fullname'] = DB::table('users')->where('id', $photos['user_id'])->pluck('name');
				$contestPhotosArray[$i]['user_image'] = DB::table('users')->where('id', $photos['user_id'])->pluck('image');
				if (isset($photos['contest_id']))
				{
					$contestId = $photos['contest_id'];
					$contestPhotosArray[$i]['contest_id'] = $contestId;
					//echo ($contestId);
					//$contestName = Contest::where('id',$contestId)->get(array('name'));

					$contest = DB::table('contests')->where('id', $contestId)->pluck('name');

					//$contestBuilder = Contest::where('id',$contestId);
					//$contest = $contestBuilder->get();
					$contestPhotosArray[$i]['contestName'] = $contest;

				}
				else
				{
					$contestName = 'MyChannel';
					$contestPhotosArray[$i]['contestName'] = $contestName;
				}


				$i++;
			}
			$photoListArray['data'] = $contestPhotosArray;
			//return $videoListArray;

			$statusCode = 200;
			$response = Response::json($photoListArray, $statusCode);
            return $response;

			/*$statusCode = 200;
			$response = Response::make($videoList, $statusCode);
			return $response;*/

		}
		catch (Exception $ex)
		{
			$statusCode = 400;
			$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
			return $json;
		}


	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            return View::make('photos.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            //Inputs contest,title,photo,image, user_Fb_token

            $inputs = Input::all();
            Log::debug("PhotoController::store(): input parameters: ".print_r($inputs,true));

			$reqParamContest = isset($inputs['contest'])||isset($inputs['id_contest']);

			if (!$reqParamContest)
			{
				Log::debug("PhotoController::store(): Required parameters were not found, returning 400");
                $description='Required Paramaters Not Found';
                $statusCode=400;
                return Response::make($description, $statusCode);
			}

            if(!isset($inputs['title'])||!isset($inputs['photo']))
            {
            	Log::debug("PhotoController::store(): Required parameters were not found, returning 400");
                $description='Required Paramaters Not Found';
                $statusCode=400;
                return Response::make($description, $statusCode);
            }

			$user_Fb_token = CommonHelper::optionalInput("user_Fb_token");

            try
			{
				//$gcmRegistrationId = CommonHelper::optionalInput("gcmRegistrationId");
				$photoFile=Input::file('photo');
				$photoFilePath=$photoFile->getRealPath();
				//$videoFileName=md5_file($videoFilePath);
				$photoFileName=md5(time());
				$photoExtension=strtolower($photoFile->getClientOriginalExtension());
				//$videoExtension=strtolower($videoFile->getMimeType());
				if (!isset($photoExtension) or $photoExtension=='')
				{
					$photoExtension = 'jpg';
				}

				$imageFile=Input::file('image');
				$imageFilePath=$imageFile->getRealPath();
				Log::debug("ImageFilePath: ".$imageFilePath);
				$imageFileName=md5(time());
				//dd($videoFileName);
				$imageExtension=strtolower($imageFile->getClientOriginalExtension());
				if (!isset($imageExtension) or $imageExtension=='')
				{
					$imageExtension = 'jpg';
				}

				$uploadImage_Success = $imageFile->move('images/photos/', $imageFileName.'.'.$imageExtension);
				$converted_res = ($uploadImage_Success) ? 'true' : 'false';
				Log::debug("uploadImage_Success: ".$converted_res);
				$uploadPhoto_Success = $photoFile->move('photos/', $photoFileName.'.'.$photoExtension);
				$converted_res = ($uploadPhoto_Success) ? 'true' : 'false';
				Log::debug("uploadPhoto_Success: ".$converted_res);

				if (($uploadImage_Success) && ($uploadPhoto_Success))
				//if(File::move($photoFile,'photos/'."$photoFileName.$photoExtension")&& File::move($imageFile,'images/photos/'."$imageFileName.$imageExtension"))
				{
					//caricare la foto nell'album della pagina Facebook di riferimento, con tanto di tag dell'utente che ha generato il file



					//find the right contest for approval of content
					$contest = null;
					if (!isset($inputs['id_contest']))
					{
						$contestName=$inputs['contest'];
						$contest=Contest::where('name',$contestName)->first();
					}
					else
					{
						$contest=Contest::where('id',$inputs['id_contest'])->first();
					}


					$ownerId = ResourceServer::getOwnerId();
					//$user=User::find($ownerId);

					$photo=new Photo();
					$photo->filename="$photoFileName.$photoExtension";
					$photo->image="$imageFileName.$imageExtension";
					$photo->name=utf8_encode($inputs['title']);
					$photo->user_id=$ownerId;

					$user=User::find($ownerId);

					///////////////////////////////////
					$photo->approval_step_id=1;
					///////////////////////////////////

					//////////////////////////////////
					//////////////////////////////////
					exec("sh ".base_path()."/sh/GeneratePhotoThumbnails.sh $photoFileName.$photoExtension", $output);
					//////////////////////////////////
					//////////////////////////////////


					$photo->contest_id=$contest->id;
					$photo->location_id=$contest->location_id;


					Contest::find($contest->id)->users()->attach($ownerId);




					$photo->save();
					Log::debug("Photo saved on the db");


					/*
					Log::debug("Sending notification with Samsung method!!!!!!!");
					GCMHelper::notifyPhoto($photo->id);
					*/


					$company_user = Company::find($contest->company_id)->belongsToManyUsers()->where('user_id', $user->id)->first();

					if ((isset($company_user->pivot->token) && $company_user->pivot->token !== ''))
					{
						$message = PushNotification::Message('Your photo entitled "'.$photo->name.'"  for '.$contest->name.' contest has been approved! Share on Facebook and make it viral!',array(
							'title' => $contest->name,
							'url' => 'https://www.contestengine.net/photo/'.$photo->id));
						$app = '';
						if (strpos($company_user->pivot->token,'Android_') !== false)
						{
							$deviceToken = str_replace("Android_", "", $company_user->pivot->token);
							Log::debug("Device token to be used: ".$deviceToken);
							$app = 'jtmAndroid';
						}
						else if (strpos($company_user->pivot->token,'IOS_') !== false)
						{
							$deviceToken = str_replace("IOS_", "", $company_user->pivot->token);
							Log::debug("Device token to be used: ".$deviceToken);
							$app = 'jtmiOS';
						}

						PushNotification::app($app)
							->to($deviceToken)
							->send($message);
					}



					//store and update the user access token
					//$user->user_fb_token = $user_Fb_token;
					//$user->save();


					$statusCode=200;
					return Response::make($photo,$statusCode);

				}
				else
				{


					return Response::json(array("error" => "Error in File Upload"),500);
				}




			}
			catch(Exception $e)
			{
				Log::error($e);
				return Response::json(array("error" => "Internal error occurred"),500);
			}




	}






	/*
	public function message($data, $page_token, $page_id)
    {
        // need token
        $data['access_token'] = $page_token;


        // init
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/".$page_id."/feed");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // execute and close
        $return = curl_exec($ch);
		Log::debug("Facebook JSON: ".$return);
        curl_close($ch);

        // end
        return $return;
    }
	*/
	public function message2($data, $page_token)
    {
        // need token
        $data['access_token'] = $page_token;


        // init
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/me/feed");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // execute and close
        $return = curl_exec($ch);
		Log::debug("Facebook message2 JSON: ".$return);
        curl_close($ch);

        // end
        return $return;
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
            try
            {
                $photoList = Photo::findOrFail($id);

                return $photoList;
            }
            catch(ModelNotFoundException $e)
            {
                $statusCode = 200;
                $description = 'Photo Not Found';
                $response = Response::json(array('description'=>$description, 'errorCode'=>404),$statusCode);
                return $response;
            }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            return View::make('photos.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function softDelete($id)
	{
		//
		try
		{
			Log::info("softDelete photo with id ".$id);
			$photo = Photo::findOrFail( $id );
			$photo->delete();

			//delete the post from the FB page + update the ranking of the belonging contest
			Scheduler::deletePhotoPost($id);
			Scheduler::UpdateRanking($photo->contest_id);

			$statusCode = 200;
			$description = 'Photo Deletion successful';
			$returnResponse = array('description'=>$description,'errorCode'=>200);
			$response = Response::json($returnResponse,$statusCode);
			return $response;
		}
		catch (Exception $ex)
		{
			$statusCode = 400;
			$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
			return $json;
		}

	}


	public function saveExternalPhoto()
	{
            //Mandatory Inputs: fb_user_id, email, id_contest, title, photo,
			//Optional inputs: device_token

            $inputs = Input::all();
            Log::debug("saveExternalPhoto::store(): input parameters: ".print_r($inputs,true));

			$reqParamContest = isset($inputs['email'])||isset($inputs['id_contest']);

			if (!$reqParamContest)
			{
				Log::debug("saveExternalPhoto::store(): Required parameters were not found, returning 400");
                $description='Required Paramaters Not Found';
                $statusCode=400;
                return Response::make($description, $statusCode);
			}

            if(!isset($inputs['title'])||!isset($inputs['photo'])||!isset($inputs['fb_user_id']))
            {
            	Log::debug("saveExternalPhoto::store(): Required parameters were not found, returning 400");
                $description='Required Paramaters Not Found';
                $statusCode=400;
                return Response::make($description, $statusCode);
            }

			$device_token = CommonHelper::optionalInput("device_token");

            try
			{
				//$gcmRegistrationId = CommonHelper::optionalInput("gcmRegistrationId");
				$photoFile=Input::file('photo');
				$photoFilePath=$photoFile->getRealPath();
				//$videoFileName=md5_file($videoFilePath);
				$photoFileName=md5(time());
				$photoExtension=strtolower($photoFile->getClientOriginalExtension());
				//$videoExtension=strtolower($videoFile->getMimeType());
				if (!isset($photoExtension) or $photoExtension=='')
				{
					$photoExtension = 'jpg';
				}




				$uploadPhoto_Success = $photoFile->move('photos/', $photoFileName.'.'.$photoExtension);
				$converted_res = ($uploadPhoto_Success) ? 'true' : 'false';
				Log::debug("uploadPhoto_Success: ".$converted_res);

				if ($uploadPhoto_Success)
				//if(File::move($photoFile,'photos/'."$photoFileName.$photoExtension")&& File::move($imageFile,'images/photos/'."$imageFileName.$imageExtension"))
				{
					//caricare la foto nell'album della pagina Facebook di riferimento, con tanto di tag dell'utente che ha generato il file



					//find the right contest for approval of content
					$contest = null;
					$contest=Contest::where('id',$inputs['id_contest'])->first();


					$ownerId = ResourceServer::getOwnerId();
					//$user=User::find($ownerId);

					//Abbiamo solo questo dato: "fb_user_id". Devo crearmi l'utenza solo se
					//è la prima volta che l'utente scatta una foto
					//*************************************************************
					$client_id = "juveUser";
					$fb_user_id = $inputs['fb_user_id'];
					$email = $inputs['email'];

					$user = User::where("id_fb",$fb_user_id)->first();


					if (!$user)
					{
						//utente che scatta foto per la prima volta
						$user = new User();
						$user->id_fb = $fb_user_id;


						$user->password = Hash::make(Config::get("facebook")["secret_password"]);

					}

					try
					{
						$facebook = new Facebook(array(
							'appId' => $contest->company->fbApp_clientID,
							'secret' => $contest->company->fbApp_clientSecret
						));
						$facebook->setFileUploadSupport(true);
						//access token dell'admin della app juventus live
						$info = array("access_token" => "CAACIJdgrdQIBAMbcRbKpuFttoOAFeicUEZCW33K5KTmswgmfJ7EhsfGYu6ZCyAbsQ1EO8TzEcSu7ZBKr36i3ZCkCjCTDptbnoTi4saES9DXaZCHo9lY7Hhu2QvzoYh2b2Wfph6bNZB68NI0vZAbHQaBtMhRGMXycG2fdZAZApdr5O15PmK0jDCr6kkMZCHfNiefpVZAUX4ZBhlM5sOLnWiYZAB3bZC");
						$fb_request = $facebook->api('/'.$fb_user_id, 'GET', $info);
						//Log::debug("Facebook response for getting access token: ".print_r($fb_request,true));

						$user->first_name = $fb_request['first_name'];
						$user->last_name = $fb_request['last_name'];
						$user->name = $fb_request['name'];
					}
					catch (Exception $ex)
					{
						Log::error("Error during graph api user data request. Error details: ".$ex);
					}

					$user->email = $email;
					if (isset($device_token))
					{
						$user->token = $device_token;
					}
					$cachedPassword = $user->password;
					Log::debug("Cached password: ".$cachedPassword);
					$tmpPassword = Config::get("facebook")["secret_password"].time();
					$user->password = Hash::make($tmpPassword);
					Log::debug("Tmp password: ".$tmpPassword.". Performing implicit login of user ".$fb_user_id);

					$userData = array("username" => $user->email, "password" => $tmpPassword, "client" => $client_id);

					//$userData = array("username" => $user->username, "password" => $tmpPassword);
					$user->save();
					$json = CommonHelper::getAccessToken($userData,true);
					Log::debug("Response for user ".$user->email.": ".json_encode($json, JSON_PRETTY_PRINT));
					$user->password = $cachedPassword;
					$user->save();
					Log::debug("Cached password recreated");
					//*************************************************************

					$photo=new Photo();
					$photo->filename="$photoFileName.$photoExtension";
					$photo->image="$photoFileName.$photoExtension";
					$photo->name=utf8_encode($inputs['title']);
					$photo->user_id=$user->id;

					//It is not possible to upload the video on FACEBOOK page. It needs to be reviewed by the customer.
					$photo->approval_step_id=1;
					exec("sh ".base_path()."/sh/GeneratePhotoThumbnails.sh $photoFileName.$photoExtension", $output);

					$photo->contest_id=$contest->id;
					$photo->location_id=$contest->location_id;

					Contest::find($contest->id)->users()->attach($user->id);

					$photo->save();
					Log::debug("Photo saved on the db");

					$statusCode=200;
					return Response::make($photo,$statusCode);

				}
				else
				{


					return Response::json(array("error" => "Error in File Upload"),500);
				}




			}
			catch(Exception $e)
			{
				Log::error($e);
				return Response::json(array("error" => "Internal error occurred"),500);
			}




	}
}
