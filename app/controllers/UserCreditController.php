<?php

class UserCreditController extends BaseController {

	function __construct()
	{
		$this->beforeFilter('auth_company');
	}

    public function getCredits()
    {
        return Response::json(UserCredit::getCreditsForUser(Auth::user()));
    }

    public function getHasAvailableCredit($service_type)
    {
        return Response::json(UserCredit::userHasCredit($service_type, Auth::user()));
    }

    public function postChargeFee()
    {
        $fee = ServiceFee::findOrFail(Input::get('fee_id'));
        $description = $fee->getInvoiceDescription();
        $transaction = CreditCard::chargeUser(Auth::user(), $fee->fixed_fee, $description);
        $invoice = Invoice::generate(Auth::user(), $fee->fixed_fee, $description);
        $transaction->invoice_id = $invoice->id;
        $transaction->save();
        // $invoice = Invoice::find(4);

        UserCredit::loadCredit($fee, Auth::user());

        return Response::json(true);

        // return $invoice->getPdf()->download($invoice->getFormattedId() . ".pdf");
    }

	public function postBuyCredit()
	{
		$fee = ServiceFee::findOrFail(Input::get('fee_id'));
        return Response::make(UserCredit::loadCredit($fee, Auth::user()));
	}

	public function postChargeContest()
	{
        $contest = Contest::findOrFail(Input::get('cid'));
        return Response::make(UserCredit::chargeCredit(Auth::user(), $contest));
	}

    public function getAvailablePlans($service_type = null) {
        $fees = ServiceFee::where('is_sellable', true);

        if($service_type != null)
            $fees = $fees->where('service_type', $service_type);

        return View::make('service_fees.list')->with('fees', $fees);
    }

}