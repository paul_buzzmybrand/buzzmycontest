<?php

use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;

class Website_UserController extends Controller {

    
     //@TODO  adding search with reward type
    public function index() 
    {
        if (CommonHelper::loggedInAsUser())
        {
            return Redirect::action('Website_UserController@userContests');
        }
        else if (CommonHelper::loggedInAsCompany())
        {
        	return Redirect::action('Website_CompanyController@dashboard');
        }		
        else
        {
        	$keyword = Input::get('keyword');
            $location = Input::get('location');
            $reward = Input::get('reward');            
			if (isset($keyword) || isset($location) || isset($reward))
            {
            	//Search
                $keyword = Input::get('keyword');
                $location = Input::get('location');
                $reward = Input::get('reward');
                
                $contests = ContestHelper::search($keyword, $location, $reward);
                $countContest = count($contests);
            	Log::debug("Number of contests returned: ".$countContest); 
                return View::make('website/user/user')->with('contests', $contests)->with('countContest', $countContest);
            }
            $contests = UserHelper::getAllContest();
            $countContest = count($contests);
            Log::debug("Number of contests returned: ".$countContest); 
            return View::make('website/user/user')->with('contests', $contests)->with('countContest', $countContest);
        }    
        
    }
    
    public function contest($id)
    {
        $contest = ContestHelper::get($id);
        $videos = $contest->videos;
        $contestData = ContestHelper::getContestData($id);
        //@TODO count videos for carousel
        //example
        $countVideos = count($videos);
        return View::make('website/user/contest')->with('contest', $contest)->with('videos', $videos)->with('countVideos', $countVideos)->with('contestData', $contestData)->with('id', $id);
    }
    
    
    public function login()
    {
        $userData = array(
            'username' => Input::get('username'),
            'password' => Input::get('password')
        );
        Log::debug("User credentials. username: ".$userData["username"].", password: ".$userData["password"]);
        //@TODO: create login for user with user role
        $user = UserHelper::loginUser($userData);
        if ($user)
        {
        	return 'true';
        }
        else
        {
        	return 'false';
        }
    }
    
    public function register()
    {
        $dataToRegistration = array(
            'username' => Input::get('username'),
            'password' => Input::get('password'),
        	'email' => Input::get('email'),
        	'confirmPassword' => Input::get('confirmPassword'),
        	'images' => Input::file('images')
        );
        $userId = UserHelper::registerUser($dataToRegistration);
        UserHelper::sendEmail($dataToRegistration);
        Log::debug("ID of the new user: ".$userId);
        $data2Login = array(
        	"username" => $dataToRegistration["email"],
        	"password" => $dataToRegistration["password"]
        );
        UserHelper::loginUser($data2Login);
        return View::make('website/user/confirm-register');
    }
    
    public function changePhoto()
    {
        if (CommonHelper::loggedInAsUser() && (Input::hasFile('images')))
        {
            $image = Input::file("images");
        	Log::debug("New photo uploaded: ".$image);
            $userId = Auth::user()->id;
            UserHelper::updatePhoto($image, $userId);
            return Redirect::action('Website_UserController@index');
		}
        else
        {
        	Log::debug("User did not upload the photo");
            return Redirect::action('Website_UserController@index');
        }
    }
    
    public function deleteAccount()
    {
        if (CommonHelper::loggedInAsUser())
        {
            $userId = Auth::user()->id;
            UserHelper::deleteAccount($userId);
    		Session::flush();
            return Redirect::to('website');
        }
        else
        {
            return Redirect::action('Website_CompanyController@index');
        }
    }
    
    public function logout()
    {
        if (Auth::check())
        {
    		Session::flush();
        	Auth::logout();
            return Redirect::to('website');
        }
        else
        {
            return Redirect::action('Website_CompanyController@wordpress', array('our-services'));
        }
    }
    
    public function facebook()
    {
        if (CommonHelper::loggedInAsUser())
        {
            return Redirect::to('website/user');
        }
        else
        {
            Log::info('Attempt to login with Facebook.');
            //$facebook = new Facebook(Config::get('facebook'));
            $params = array(
                'redirect_uri' => url('website/user/facebook_login'),
                'scope' => 'email',
             );

            $permissions = 'public_profile,user_friends,email';

            $loginUrl = "https://www.facebook.com/dialog/oauth?"
                ."client_id=566800206740950"
                ."&scope=".$permissions
                ."&response_type=code&redirect_uri=".url('website/user/facebook_login');
            
            return Redirect::to($loginUrl);
        }
    }
    
    //@TODO save facebook profile
    public function facebookLogin()
    {
        $code = Input::get('code');
        if (strlen($code) == 0)
        { 
            Log::warning('Error while attempt to connect with Facebook.');
            $message = trans('messages.error_communicating_facebook');
            return View::make('website/user/social-login')->with('message', $message);
        }

        $facebook = new Facebook(Config::get('facebook'));
        $uid = $facebook->getUser();

        if ($uid == 0)
        {
            $message = trans('messages.facebook_error');
            Log::warning('Error while attempt to connect with Facebook.');
            return View::make('website/user/social-login')->with('message', $message);
        }

        $facebook_account = $facebook->api('/me');
        $ch = CommonHelper::curl("/oauth/facebook_token");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array("token" => $facebook->getAccessToken()));
        $json = curl_exec($ch);
        if (curl_errno($ch))
        {
        	Log::warning("Error while authentication facebook token: ".curl_errno($ch).",".curl_error($ch));
        	curl_close($ch);
        	return View::make("website/user/social-login")->with("message","messages.facebook_error");
        }
        curl_close($ch);
        Log::debug("curl returned: ".$json);
        $decoded_json = json_decode($json);
        if (isset($decoded_json->access_token))
        {
        	$accessToken = $decoded_json->access_token;
	        Log::debug("Storing access token in session: ".$accessToken);
	        Session::put("website.access_token",$accessToken);
        	$user = User::find(UserHelper::getByAccessToken($accessToken)->id);
        	Auth::login($user);
        	Log::debug("User ".$user->email." successfully authenticated with facebook");
        	return Redirect::action('Website_UserController@index');
        }
        else
        {
        	return View::make("website/user/social-login")->with("message","messages.facebook_error");
        }
        
        //@TODO - API should return additional user id
        
        //$accountId = $facebook_account['id'];
        //$user = DB::table('facebook_profiles')->where('uid', $accountId)->first();
        
        
        
        /*
        //@TODO - this is temporary because there is no integration with facebook
        //<!-- TEMPORARY STARTS -->
        $config = Config::get('website');
        $userJson = UserHelper::loginUser(array("username" => $config["general_username"], "password" => $config["general_password"]));
        Log::debug("Facebook user = ".$userJson);
        $user = User::find($userJson->id);
        //<!-- TEMPORARY ENDS -->
        if($user)
        {
            $userId = $user->id;
            Auth::login($user);
            Log::info('Login to system with facebook(facebook_account: '.json_encode($accountId,JSON_PRETTY_PRINT).', userId: '.$userId.').');
            return Redirect::action('Website_UserController@index');
        }
        else
        {
            UserHelper::addUserWithFacebook($facebook_account);
            return Redirect::action('Website_UsertController@index');
        }  */
    }
    
    public function twitter()
    {
        if (CommonHelper::loggedInAsUser())
        {
            return Redirect::to('website/user');
        }
        else
        {
            $config = Config::get('twitter');
            $connection = new TwitterOAuth($config['consumer_key'], $config['consumer_secret']);
            if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')
            {
            	$callback = $config['oauth_callback_user_https'];
            }
            else
            {
            	$callback = $config['oauth_callback_user_http'];
            }
            $request_token = $connection->getRequestToken($callback); //get Request Token     
            if( $request_token)
            {
            	Log::debug("request_token: ".print_r($request_token,true));
                switch ($connection->http_code) 
                {
                    case 200:
                        $url = $connection->getAuthorizeURL($request_token);
                        Session::put("twitter_oauth_token",$request_token["oauth_token"]);
                        Session::put("twitter_oauth_token_secret",$request_token["oauth_token_secret"]);
                        Log::info('Attempt to login with Twitter, url: '.$url);
                        return Redirect::to($url);
                    default:
                        $message = trans('messages.twitter_connection_error');
                        Log::warning('Error while attempt to connect with Twitter.');
                        return View::make('website/user/social-login')->with('message', $message);
                }
            }
            else //error receiving request token
            {
                $message = trans('messages.twitter_request_token_error');
                Log::warning('Twitter request token error.');
                return View::make('website/user/social-login')->with('message', $message);
            }
        }
        
    }
    
    public function twitterLogin()
    {
		try
		{
			$config = Config::get('twitter');
			$oauthVerifier = CommonHelper::mandatoryInput("oauth_verifier");
			Log::debug("twitterLogin(). oauth_verifier: ");
			$connection = new TwitterOAuth($config['consumer_key'], $config['consumer_secret'], Session::get('twitter_oauth_token'),Session::get('twitter_oauth_token_secret'));
			$tokenCredentials = $connection->getAccessToken($oauthVerifier);
			Log::debug("We obtained new token credentials: ".print_r($tokenCredentials,true));
			$params =array();
			$params['include_entities']='false';
			$content = $connection->get('account/verify_credentials',$params);
			Log::debug("Call to verify_credentials returned: ".json_encode($content, JSON_PRETTY_PRINT));
			if($content && isset($content->screen_name) && isset($content->name))
			{
				Log::debug("Successful call to verify_credentials. Returned content: ".json_encode($content,JSON_PRETTY_PRINT));
				Log::debug("Calling internal twitter login function to obtain token to access service oauth");
				$ch = CommonHelper::curl("/oauth/twitter_token");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, array("token" => $connection->token->key, "token_secret" => $connection->token->secret));
				$json = curl_exec($ch);
				if (curl_errno($ch))
				{
					Log::warning("Error while authentication twitter token: ".curl_errno($ch).",".curl_error($ch));
					curl_close($ch);
					return View::make("website/user/social-login")->with("message","messages.twitter_error");
				}
				curl_close($ch);
				Log::debug("curl returned: ".$json);
				$decoded_json = json_decode($json);
				if (isset($decoded_json->access_token))
				{
					$accessToken = $decoded_json->access_token;
					Log::debug("Storing access token in session: ".$accessToken);
					Session::put("website.access_token",$accessToken);
					$user = User::find(UserHelper::getByAccessToken($accessToken)->id);
					Auth::login($user);
					Log::debug("User ".$user->email." successfully authenticated with twitter");
					return Redirect::action('Website_UserController@index');
				}
				else
				{
					return View::make("website/user/social-login")->with("message","messages.twitter_error");
				}
				return Redirect::action('Website_UserController@index');
			}
			else
			{
				Log::debug("Call to verify_credentials not successful");
				return Redirect::to('website/user');
			}
		}
    	catch(MissingMandatoryParametersException $e)
    	{
    		Log::warning("Missing mandatory parameter. Probably user denied login");
    		return Redirect::action("Website_UserController@index");
    	}		
    }
    
    public function userContests()
    {
        if (CommonHelper::loggedInAsUser())
        {
            $userId = Auth::user()->id;
            $contests = UserHelper::getUserContests($userId);
            Log::debug("Checking if user contests collection is empty");
            if (isset($contests) && !empty($contests) && is_array($contests))
            {
            	$countContest = count($contests); 
            	Log::debug("There are ".$countContest." contests");
            	return View::make('website/user/user-contests')->with('contests', $contests)->with('countContest', $countContest);
            }
            else
            {
            	Log::debug("It is empty");
            	return View::make('website/user/user-contests')->with('contests', array())->with('countContest', 0);
            }
        }
        else
        {
            return Redirect::action('Website_UserController@index');
        }  
    }
    
    public function favouritedContests()
    {
        if (CommonHelper::loggedInAsUser())
        {
            $contests = UserHelper::getFeaturedContests();
            $countContest = count($contests); 
            return View::make('website/user/favourited-contests')->with('contests', $contests)->with('countContest', $countContest);
        }
        else
        {
            return Redirect::action('Website_UserController@index');
        }
    }
    
    //@TODO  adding search with reward type 
    public function searchContests()
    {
        if (CommonHelper::loggedInAsUser())
        {
            if (Request::isMethod('get'))
            {
                $keyword = Input::get('keyword');
                $localization = Input::get('location');
                $reward = Input::get('reward');
                if($keyword || $localization || $reward)
                {
                    $contests = DB::table('contests')
                            ->where('name', 'like', $keyword.'%')
                            ->where( function ( $query ) use($localization)
                            {
                                $query->where( 'location', 'like', $localization.'%' );
                            })
                            ->get();
                    $countContest = count($contests);
                    return View::make('website/user/search')->with('contests', $contests)->with('countContest', $countContest);
                }
            }
            $contests = UserHelper::getAllContest();
            $countContest = count($contests); 
            return View::make('website/user/search')->with('contests', $contests)->with('countContest', $countContest);
        }
        else
        {
            return Redirect::action('Website_UserController@index');
        }
    }
    
    public function checkUsername()
    {
        $username = Input::get('username');
        $user = DB::table('users')->where('username', $username)->first();
        if($user)
        {
            echo 'false';
        }
        else
        {
            echo 'true';
        }
    }
    
    /*
     * check that company email is already used
     * @return string 'false' if is used, 
     */
    public function checkEmail()
    {
        $email = Input::get('email');
        $result = DB::table('users')->where('email', $email)->first();
        if($result)
        {
            return 'false';
        }
        else
        {
            return 'true';
        }
        
    }
    
    public function joinContest($id)
    {
  		$result = UserHelper::joinTheContest($id);
    	return Redirect::action('Website_UserController@contest',array($id));
    }
    
    public function video($id)
    {
        $video = ContestHelper::getVideo($id);
        return View::make('website/user/video')->with('video', $video);
    }
}
?>
