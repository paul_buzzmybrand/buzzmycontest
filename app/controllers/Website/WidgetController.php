<?php

use MetzWeb\Instagram\Instagram;

class Website_WidgetController extends Controller {

	public function message($data, $page_token, $page_id)
    {
        // need token
        $data['access_token'] = $page_token;


        // init
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/".$page_id."/feed");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // execute and close
        $return = curl_exec($ch);
		Log::debug("Facebook JSON: ".$return);
        curl_close($ch);

        // end
        return $return;
    }

	public function facebook($id)
    {
        /*if (CommonHelper::loggedInAsUser())
        {
            $userId = Auth::user()->id;
            Log::info('User (id: '.$userId.') started to use widget.');
            $config = Config::get('hdfvr');
            $domain = $config['domain'];
            return View::make('website/widget/record')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain);
        }
        else
        {*/
		//require_once __DIR__ . '/../../../vendor/facebook/php-sdk-v4/src/Facebook/autoload.php';
		//session_start();

		Log::debug('Attempt to login with Facebook.');
		$contest = Contest::find($id);


		$redirect_url = '';
		$device = Input::get('device');



		if ( $device == 'mobile') {
			$redirect_url = 'website/login/facebook_login/'.$id . '?device=mobile';
		} else {
			$redirect_url = 'website/login/facebook_login/'.$id . '?device=desktop';
		}

		if ($contest->score_from_userwall)
		{
			$permissions = 'public_profile,user_friends,email,publish_actions';
		}
		else
		{
			$permissions = 'public_profile,user_friends,email';
		}

		$cookie = Cookie::forget('redirect_url');

		$loginUrl = "https://www.facebook.com/dialog/oauth?"
		."client_id=566800206740950"
		."&scope=".$permissions
		."&response_type=code&redirect_uri=http://bmb.buzzmybrand.co/".$redirect_url;

		Log::debug("LoginUrl: ".$loginUrl);

		Cookie::queue('redirect_url', $redirect_url);

		return Redirect::to($loginUrl);

    }

		public function facebookLogin($id) {
		//session_start();
		//require_once __DIR__ . '/../../../vendor/facebook/php-sdk-v4/src/Facebook/autoload.php';

		//var_dump(Session::all());
		//exit;

		//var_dump(Session::all());
		//$FBRLH_state = Cookie::get('FBRLH_state');
		//$FBRLH_state = $_GET['state'];

		//$_SESSION['FBRLH_state'] = $FBRLH_state;
		//var_dump($_SESSION);
		//exit;
		//var_dump($_COOKIE);
		/*
		foreach ($_COOKIE as $k=>$v) {
			if(strpos($k, "FBRLH_")!==FALSE) {
				//$_SESSION[$k]=$v;
				Session::put($k, $v);

			}
		}
		*/
		//var_dump($_SESSION);
		//exit;
		//Session::put('FBRLH_' . 'state',Cookie::get('statecross'));

		//var_dump(Session::all());

		//var_dump(Session::all());
		//exit;
		Log::info('function facebookLogin');
        $code = Input::get('code');

		/*
		if (strlen($code) == 0) {
            Log::warning('Error while attempt to connect with Facebook. Code from facebook is empty');
            $message = trans('messages.error_communicating_facebook');
            return Redirect::to('/website/widget/login/'.$id)->with('message', $message);
        }*/

		$redirect_url = Cookie::get('redirect_url');
		Log::debug("Redirect url: ".$redirect_url);
		//var_dump($redirect_url);
		//exit;
		$redirect_uri = "http://bmb.buzzmybrand.co/".$redirect_url;
		// Get cURL resource
		$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => 'https://graph.facebook.com/oauth/access_token?client_id=566800206740950&client_secret=283eeba22d1bb4503086e259d9cfa34c&redirect_uri='.$redirect_uri.'&code='.$code
		));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);

		// Close request to clear up some resources
		curl_close($curl);

		parse_str($resp,$ar);
		Log::debug("Result from curl: ".$resp);
		$access_token=$ar['access_token'];

		//$access_token = str_replace('access_token=', '', $resp);
		//var_dump($access_token);
		//exit;

		$contest = Contest::find($id);


		//die($_SESSION['FBRLH_' . 'state']);

			$facebook = new Facebook\Facebook([
				'app_id' => $contest->company->fbApp_clientID,
				'app_secret' => $contest->company->fbApp_clientSecret,
				'default_graph_version' => 'v2.4',
			]);

		//$helper = $facebook->getRedirectLoginHelper();
		//var_dump($helper);
		//exit;

		/*
		try {
			$accessToken = $helper->getAccessToken();
			Log::debug("User access token: ".$helper->getAccessToken());
		}
		catch (Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  var_dump($e);
		  exit;
		} catch (Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  //echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  var_dump($e);
		  exit;
		}
		catch (Exception $e) {
			echo 'Facebook SDK returned un error: ' . $e->getMessage();
		  exit;
		}

		if (! isset($accessToken)) {
		  if ($helper->getError()) {
			header('HTTP/1.0 401 Unauthorized');
			echo "Error: " . $helper->getError() . "\n";
			echo "Error Code: " . $helper->getErrorCode() . "\n";
			echo "Error Reason: " . $helper->getErrorReason() . "\n";
			echo "Error Description: " . $helper->getErrorDescription() . "\n";
		  } else {
			header('HTTP/1.0 400 Bad Request');
			echo 'Bad request';
		  }
		  exit;
		}
		*/

		$facebook_account = $facebook->get('me?fields=email,birthday,first_name,gender,locale,last_name,middle_name,timezone', $access_token);
		$facebook_account = $facebook_account->getGraphUser();
		Log::debug("Result from facebook: ".$facebook_account);
		//exit;

		$idFromCookie = Cookie::get('user_id');

		if ( $idFromCookie ) {

			$user = User::where('id', $idFromCookie)->first();
			Log::debug("User retrieved from cookie: " . $user->email);

		} else {
			$user = User::where('email', $facebook_account['id'] . '@f.com')->first();

			// There is a user
			if ($user) {

				$twitterProfileConnected = TwitterProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
				if ( $twitterProfileConnected ) {
					$twitterProfileConnected->expired = 1;
					$twitterProfileConnected->save();
				}
				Log::debug("User already present: " . $user->id);

			// Create new user
			} else {

				$user = new User();
				Log::debug("New user created: " . $user->id);
			}

			$user->email = $facebook_account['id'] . '@f.com';
			$user->password = Hash::make(Config::get("facebook")["secret_password"]);

			if ( isset($facebook_account['first_name']) ){
				$user->name = $facebook_account['first_name'];
			}
			if ( isset($facebook_account['last_name']) ){
				$user->surname = $facebook_account['last_name'];
			}

			$user->save();

			Cookie::queue('user_id', $user->id);
			Log::debug("Created new Cookie for USER ID: " . $user->id);
		}

		//devo cercare il facebook profile avente company_id del contest, fb_id del profilo loggante. Una volta trovato,
		//devo verificare che lo/gli user_id associati non abbiano partecipato al contest con qualche submission.
		$facebook_profiles = FacebookProfile::where("company_id", $contest->company_id)->where("id_fb", $facebook_account['id'])->get();


		if ($contest->unique_participation)
		{

			foreach ($facebook_profiles as $fb_profile)
			{
				$user_id = $fb_profile['user_id'];
				//devo verificare se questo user ha gi� partecipato al contest


				if ($contest->type_id == "2")
				{


					$userPhotosWithContests=Photo::select("photos.*")
							->where("photos.user_id","=", $user_id)
							->where("photos.contest_id", "=", $id)
							->get();

					Log::debug("Number of photos returned per user: ".count($userPhotosWithContests));

					//$queries = DB::getQueryLog();
					//Log::debug("Queries: ".print_r($queries,true));

					if (count($userPhotosWithContests) > 0)
					{
						if ($contest->max_entries)
						{
							$entries = $contest->max_entries;

							if (count($userPhotosWithContests) > $entries - 1)
							{
								Log::debug("Yes photos with user_id ".$user_id." and max_entries ".$entries);

								$message = trans('messages.fbaccount_already_participate');

								return Redirect::to('login/'.$id)->with('message', $message);
							}

						}
						Log::debug("Yes photos with user_id ".$user_id." and fb_id ".$fb_profile['id_fb']);

						//$message = "Questo account ha gi� partecipato";
						$message = trans('messages.fbaccount_already_participate');

						return Redirect::to('login/'.$id)->with('message', $message);
						//break;
						//return false;
						//in tal caso, devo costringere l'utente a non farlo partecipare poich�
						//ha gi� usato il suddetto profilo facebook


					}
					else
					{
						Log::debug("No photos with user_id ".$user_id." and fb_id ".$fb_profile['id_fb']);
						//return true;
					}


				}
				else if ($contest->type_id == "1")
				{
					$userVideosWithContests=Photo::select("videos.*")
							->where("videos.user_id","=", $user_id)
							->where("videos.contest_id", "=", $id)
							->get();

					Log::debug("Number of videos returned per user: ".count($userVideosWithContests));

					//$queries = DB::getQueryLog();
					//Log::debug("Queries: ".print_r($queries,true));

					if (count($userVideosWithContests) > 0)
					{

						Log::debug("Yes videos with user_id ".$user_id." and fb_id ".$fb_profile['id_fb']);

						//$message = "Questo account ha gi� partecipato";
						$message = trans('messages.fbaccount_already_participate');


						return Redirect::to('login/'.$id)->with('message', $message);
						//break;
						//return false;
						//in tal caso, devo costringere l'utente a non farlo partecipare poich�
						//ha gi� usato il suddetto profilo facebook


					}
					else
					{
						Log::debug("No videos with user_id ".$user_id." and fb_id ".$fb_profile['id_fb']);
						//return true;
					}
				}
				else if ($contest->type_id == "3")
				{
					$userEssaysWithContests=Essay::select("essays.*")
							->where("essays.user_id","=", $user_id)
							->where("essays.contest_id", "=", $id)
							->get();

					Log::debug("Number of essays returned per user: ".count($userEssaysWithContests));

					//$queries = DB::getQueryLog();
					//Log::debug("Queries: ".print_r($queries,true));

					if (count($userEssaysWithContests) > 0)
					{

						Log::debug("Yes essays with user_id ".$user_id." and fb_id ".$fb_profile['id_fb']);

						//$message = "Questo account ha gi� partecipato";
						$message = trans('messages.fbaccount_already_participate');


						return Redirect::to('login/'.$id)->with('message', $message);
						//break;
						//return false;
						//in tal caso, devo costringere l'utente a non farlo partecipare poich�
						//ha gi� usato il suddetto profilo facebook


					}
					else
					{
						Log::debug("No essays with user_id ".$user_id." and fb_id ".$fb_profile['id_fb']);
						//return true;
					}
				}



			}

		}



		////////
		$this->updateFacebookProfile($facebook_account, $access_token, $contest->company->id, $user->id);
		/////////

		Cookie::queue('facebookLogin', 1);


		////////


		if ( isset($_GET['device']) && $_GET['device'] == 'mobile' ) {
			$this->joinContestFromMobile($user->id, $contest->id);
		} else {
			$this->joinContestFromWebsite($user->id, $contest->id);
		}
		////////

		$facebook_account_firstname = $facebook->get('/me?fields=first_name', $access_token);
		$facebook_account_firstname = json_decode($facebook_account_firstname->getBody());
		$facebook_account_lastname = $facebook->get('/me?fields=last_name', $access_token);
		$facebook_account_lastname = json_decode($facebook_account_lastname->getBody());


		$user->name = $facebook_account_firstname->first_name;
		$user->surname = $facebook_account_lastname->last_name;

		$cachedPassword = $user->password;
		$user->password = Hash::make(Config::get("facebook")["secret_password"]);
		$user->save();

		$userJson = UserHelper::loginUser(array("username" => $user->email, "password" => Config::get("facebook")["secret_password"]));
		Log::debug("Facebook user = ".$userJson);
		Auth::login(User::find($userJson->id));

		Log::info('Login to system with facebook(facebook_account: '.json_encode($facebook_account,JSON_PRETTY_PRINT).', user: '.$facebook_account['id'].').');

		$user->password = $cachedPassword;
		$user->save();

		$contest = Contest::find($id);
		$contest_type = $contest->type_id;
		return Redirect::to('website/widget/connect/'. $id);


	}

	private function updateNumberOfFriends(&$facebookProfile, $accessToken) {

		$ch = CommonHelper::curl("https://graph.facebook.com/v2.1/me/friends?offset=5000&limit=5000");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
		$jsonfriends=curl_exec($ch);
		if(curl_errno($ch))
		{
			Log::error("curl error: ".curl_error($ch).", info: ".print_r(curl_getinfo($ch),true));
			curl_close($ch);
			throw new IOException("Error while accessing facebook");
		}
		curl_close($ch);
		Log::debug("Facebook friends JSON: ".$jsonfriends);
		$decoded_jsonfriends = json_decode($jsonfriends);

		if (isset($decoded_jsonfriends->summary))
		{
			$facebookProfile->num_friends = $decoded_jsonfriends->summary->total_count;
		}

	}

	public static function downloadAndSaveFacebookProfileImage(&$facebookProfile, $accessToken) {

        $response = 'default.png';
        // Noonic debug
        if (true) {
    		Log::debug("Obtaining facebook profile image");
    		$fp = tmpfile();
    		$tmpFilename = stream_get_meta_data($fp)['uri'];
    		Log::debug("Temporary filename: ".$tmpFilename);
    		$ch = CommonHelper::curl("https://graph.facebook.com/me/picture");
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
    		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
    		curl_setopt($ch, CURLOPT_FILE, $fp);
    		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    		$jsonimage = curl_exec($ch);
    		if(curl_errno($ch))
    		{
    			Log::warning("Error downloading profile picture. Setting the default one");
    			curl_close($ch);
    			fclose($fp);
    			$facebookProfile->image = 'default.png';
    		} else {
    			Log::debug("Picture downloaded");
    			curl_close($ch);

    			Log::debug("Facebook Image JSON: ".$jsonimage);

    			$extension = "png";
    			$facebookProfile->image = md5_file($tmpFilename).".".$extension;
    			$destinationFileName = 'images/users/'.$facebookProfile->image;
    			Log::debug("Destination file name: ".$destinationFileName);
    			$uploadSuccess = rename($tmpFilename, $destinationFileName);
    			Log::debug("rename() success: ".$uploadSuccess);
    			fclose($fp);
    		}
            $response = $facebookProfile->image;
        }
        return $response;


	}

	private function updateFacebookProfile($facebook_account, $accesToken, $company_id, $user_id) {

		Log::debug("Update Facebook profile start");

		//$user = User::find($user_id);

		$facebook_profile = FacebookProfile::where("company_id", $company_id)->where("user_id", $user_id)->first();
		if ( $facebook_profile ) {

			Log::debug("Facebook profile founded: " . $facebook_profile->id_fb);

		} else {

			$facebook_profile = new FacebookProfile();
			Log::debug("New Facebook profile: " . $facebook_account['id']);

		}

		$facebook_profile->user_id = $user_id;
		if (isset($facebook_account['username']))
		{
			$facebook_profile->username = $facebook_account['username'];
		}
		$facebook_profile->company_id = $company_id;

		$facebook_profile->user_fb_token = $accesToken;
		$facebook_profile->id_fb = $facebook_account['id'];

		if (isset($facebook_account['name']))
		{
			$facebook_profile->name = $facebook_account['name'];
		}

		if (isset($facebook_account['first_name']))
		{
			$facebook_profile->first_name = $facebook_account['first_name'];
			//$user->name = $facebook_account['first_name'];
		}

		if (isset($facebook_account['middle_name']))
		{
			$facebook_profile->middle_name = $facebook_account['middle_name'];
		}

		if (isset($facebook_account['last_name']))
		{
			$facebook_profile->last_name = $facebook_account['last_name'];
			//$user->surname = $facebook_account['last_name'];
		}

		if (isset($facebook_account['gender']))
		{
			$facebook_profile->gender = $facebook_account['gender'];
		}

		if (isset($facebook_account['email']))
		{
			$facebook_profile->email = $facebook_account['email'];
		}
		else
		{
			$facebook_profile->email = $facebook_account['id']."@facebook.com";
		}

		if (isset($facebook_account['birthday']))
		{
			$facebook_profile->birthdate = $facebook_account['birthday'];
		}

		if (isset($facebook_account['verified']))
		{
			$facebook_profile->verified = $facebook_account['verified'];
		}

		if (isset($facebook_account['timezone']))
		{
			$facebook_profile->timezone = $facebook_account['timezone'];
		}

		if (isset($facebook_account['hometown']))
		{
			$facebook_profile->hometown = $facebook_account['hometown']['name'];
		}

		if (isset($facebook_account['locale']))
		{
			$facebook_profile->locale = $facebook_account['locale'];
		}

		$facebook_profile->password = Hash::make(Config::get("facebook")["secret_password"]);

		$this->updateNumberOfFriends($facebook_profile, $accesToken);

		self::downloadAndSaveFacebookProfileImage($facebook_profile, $accesToken);

		$facebook_profile->expired = 0;

		$facebook_profile->save();

		//$user->save();

		Log::debug("Update Facebook profile end");

	}

	private function joinContestFromWebsite($ownerId, $contestId) {

		Log::debug("joinContestFromWebsite()");

		// ATTACH IF ROW DOESN' T EXIST
		$contest_2 = Contest::with(
						array(
							'users' => function($query) use ($ownerId) {
											$query->where('user_id', '=', $ownerId);
										}
							)
					)->where('id', $contestId)->first();

		if ( $contest_2->users->count() == 0 )
		{
			$contest_2->users()->attach($ownerId, array('login_website' => new DateTime('now')));
			Log::debug("joinContestFromWebsite() done");
			return false;
		}
		else
		{
			$contest_2->users()->updateExistingPivot($ownerId, array('login_website' => new DateTime('now')), false);
			Log::debug("joinContestFromWebsite() updated");

			if ($contest_2->type_id == "2")
			{

				$userPhotosWithContests=Photo::select("photos.*")
					->where("photos.user_id","=", $ownerId)
					->where("photos.contest_id", "=", $contestId)
					->get();

				Log::debug("Number of photos returned per user: ".count($userPhotosWithContests));

				Log::debug("joinContestFromWebsite() counting photos array");

				//$queries = DB::getQueryLog();
				//Log::debug("Queries: ".print_r($queries,true));



				if (count($userPhotosWithContests) > 0)
				{
					if ($contest_2->max_entries)
					{
						$entries = $contest_2->max_entries;

						if (count($userPhotosWithContests) > $entries - 1)
						{
							Log::debug("joinContestFromWebsite() yes photos with user_id ".$ownerId." and max_entries ".$entries);
							return true;
						}

					}

					Log::debug("joinContestFromWebsite() yes photos with user_id ".$ownerId);
					return true;
				}
				else
				{
					Log::debug("joinContestFromWebsite() no photos with user_id ".$ownerId);
					return false;
				}

			}
			else if ($contest_2->type_id == "1")
			{
				$userVideosWithContests=Video::select("videos.*")
					->where("videos.user_id","=", $ownerId)
					->where("videos.contest_id", "=", $contestId)
					->get();

				Log::debug("Number of videos returned per user: ".count($userVideosWithContests));

				Log::debug("joinContestFromWebsite() counting videos array");

				//$queries = DB::getQueryLog();
				//Log::debug("Queries: ".print_r($queries,true));

				if (count($userVideosWithContests) > 0)
				{
					Log::debug("joinContestFromWebsite() yes videos with user_id ".$ownerId);
					return true;
				}
				else
				{
					Log::debug("joinContestFromWebsite() no videos with user_id ".$ownerId);
					return false;
				}
			}
			else if ($contest_2->type_id == "3")
			{
				$userEssaysWithContests=Essay::select("essays.*")
					->where("essays.user_id","=", $ownerId)
					->where("essays.contest_id", "=", $contestId)
					->get();

				Log::debug("Number of essays returned per user: ".count($userEssaysWithContests));

				Log::debug("joinContestFromWebsite() counting essays array");

				//$queries = DB::getQueryLog();
				//Log::debug("Queries: ".print_r($queries,true));

				if (count($userEssaysWithContests) > 0)
				{
					Log::debug("joinContestFromWebsite() yes essays with user_id ".$ownerId);
					return true;
				}
				else
				{
					Log::debug("joinContestFromWebsite() no essays with user_id ".$ownerId);
					return false;
				}
			}




		}
		//
		return false;
	}

	private function joinContestFromMobile($ownerId, $contestId) {

		Log::debug("joinContestFromMobile()");

		// ATTACH IF ROW DOESN' T EXIST
		$contest_2 = Contest::with(
						array(
							'users' => function($query) use ($ownerId) {
											$query->where('user_id', '=', $ownerId);
										}
							)
					)->where('id', $contestId)->first();

		if ( $contest_2->users->count() == 0 )
		{
			$contest_2->users()->attach($ownerId, array('login_mobile' => new DateTime('now')));
			Log::debug("joinContestFromMobile() done");
		}
		else
		{
			$contest_2->users()->updateExistingPivot($ownerId, array('login_mobile' => new DateTime('now')), false);
			Log::debug("joinContestFromMobile() updated");


			if ($contest_2->type_id == "2")
			{

				$userPhotosWithContests=Photo::select("photos.*")
					->where("photos.user_id","=", $ownerId)
					->where("photos.contest_id", "=", $contestId)
					->get();

				Log::debug("Number of photos returned per user: ".count($userPhotosWithContests));

				Log::debug("joinContestFromMobile() counting photos array");

				//$queries = DB::getQueryLog();
				//Log::debug("Queries: ".print_r($queries,true));

				if (count($userPhotosWithContests) > 0)
				{
					Log::debug("joinContestFromMobile() yes photos with user_id ".$ownerId);
					return true;
				}
				else
				{
					Log::debug("joinContestFromMobile() no photos with user_id ".$ownerId);
					return false;
				}

			}
			else if ($contest_2->type_id == "1")
			{
				$userVideosWithContests=Photo::select("videos.*")
					->where("videos.user_id","=", $ownerId)
					->where("videos.contest_id", "=", $contestId)
					->get();

				Log::debug("Number of videos returned per user: ".count($userVideosWithContests));

				Log::debug("joinContestFromMobile() counting videos array");

				//$queries = DB::getQueryLog();
				//Log::debug("Queries: ".print_r($queries,true));

				if (count($userVideosWithContests) > 0)
				{
					Log::debug("joinContestFromMobile() yes videos with user_id ".$ownerId);
					return true;
				}
				else
				{
					Log::debug("joinContestFromMobile() no videos with user_id ".$ownerId);
					return false;
				}
			}
			else if ($contest_2->type_id == "3")
			{
				$userEssaysWithContests=Photo::select("essays.*")
					->where("essays.user_id","=", $ownerId)
					->where("essays.contest_id", "=", $contestId)
					->get();

				Log::debug("Number of essays returned per user: ".count($userEssaysWithContests));

				Log::debug("joinContestFromMobile() counting essays array");

				//$queries = DB::getQueryLog();
				//Log::debug("Queries: ".print_r($queries,true));

				if (count($userEssaysWithContests) > 0)
				{
					Log::debug("joinContestFromMobile() yes essays with user_id ".$ownerId);
					return true;
				}
				else
				{
					Log::debug("joinContestFromMobile() no essays with user_id ".$ownerId);
					return false;
				}
			}


		}
		//
		return false;
	}

	public static function joinContestFromInstagram($ownerId, $contestId) {


	Log::debug("joinContestFromInstagram()");

	// ATTACH IF ROW DOESN' T EXIST
	$contest_2 = Contest::with(
					array(
						'users' => function($query) use ($ownerId) {
										$query->where('user_id', '=', $ownerId);
									}
						)
				)->where('id', $contestId)->first();

	if ( $contest_2->users->count() == 0 ) {
		$contest_2->users()->attach($ownerId, array('login_instagram' => new DateTime('now')));
		Log::debug("joinContestFromInstagram() done");
	} else {
		$contest_2->users()->updateExistingPivot($ownerId, array('login_instagram' => new DateTime('now')), false);
		Log::debug("joinContestFromInstagram() updated");
	}
	//

	}



	public function registerUser($id, $come_from) {

		$contest = Contest::find($id);

		$rules = array();


		if ( $contest->objectives()->first()->pivot->name )
		{
			array_push($rules, "'name'=>'required'");
		}

		if ( $contest->objectives()->first()->pivot->surname )
		{
			array_push($rules, "'surname'=>'required'");
		}

		if ( $contest->objectives()->first()->pivot->birthdate )
		{
			array_push($rules, "'birthdate' => 'required|date_format:d/m/Y'");
		}

		if ( $contest->objectives()->first()->pivot->email )
		{
			array_push($rules, "'email'=>'required|email'");
		}

		if ( $contest->objectives()->first()->pivot->phone )
		{
			array_push($rules, "'phone' => 'required|digits_between:1,45'");
		}

		if ( $contest->objectives()->first()->pivot->city )
		{
			array_push($rules, "'city' => 'required'");
		}

		if ( $contest->objectives()->first()->pivot->cap )
		{
			array_push($rules, "'cap'=>'required'");
		}





		$validator = Validator::make(Input::all(), $rules);

		$inputs = Input::all();

		//customizzazione per numero di telefono gi� inserito
		if ($contest->id == '259')
		{

			$phone = Input::get('phone');

			$contest_2 = Contest::with(
				array(
					'users' => function($query) use ($phone) {
									$query->where('survey_surname', '=', $phone);
								}
					)
			)->where('id', $contest->id)->first();
			/*
			$queries = DB::getQueryLog();
			Log::debug("Queries phone: ".print_r($queries,true));

			Log::debug("Response phone: ".print_r($contest_2,true));
			*/

			Log::debug("Count Response phone: ".$contest_2->users->count());

			if ($contest_2->users->count() > 0) {
				$message = trans('messages.phone_already_inserted');
				return Redirect::to('login/'.$id)->with('message', $message);
			}

		}


		if ($validator->fails()) {


			return Redirect::to('login/' . $id)->with('message', 'The following errors occurred')->withErrors($validator);
		}
		else
		{

			$user = User::where('email', Input::get('email'))->first();

			if ( $user ) {

				if ( $user->company_id != null) {
					return Redirect::to('login/' . $id)->with('message', 'The following errors occurred')->withErrors(array(Lang::get("widget.signup_step.deny_access")));
				}




			} else {

				$user = new User();

			}

			//$user->name = Input::get('name');
			//$user->surname = Input::get('surname');
			$user->email = Input::get('email');
			$user->password = Hash::make(Config::get("facebook")["secret_password"]);



			if (isset($inputs['name']))
			{
				$user->survey_name = Input::get('name');
			}


			if (isset($inputs['surname']))
			{
				$user->survey_surname = Input::get('surname');
			}


			if (isset($inputs['birthdate']))
			{
				$user->survey_birthdate = DateTime::createFromFormat('d/m/Y', Input::get('birthdate'));
			}

			if (isset($inputs['email']))
			{
				$user->survey_email = Input::get('email');
			}

			if (isset($inputs['phone']))
			{
				//da correggere
				$user->survey_surname = Input::get('phone');
			}

			if (isset($inputs['city']))
			{
				$user->survey_city = Input::get('city');
			}

			if (isset($inputs['cap']))
			{
				$user->survey_cap = Input::get('cap');
			}


			try {

				$user->save();

			} catch (Exception $ex){

				Redirect::to('login/' . $id)->with('message', 'Error registering user');
				Log::error("Error during login " . $ex);
			}

			$userData = array(
				'username' => $user->email,
				'password' => Config::get("facebook")["secret_password"]
			);
			$user = UserHelper::loginUser($userData);


			Cookie::queue('user_id', $user->id);
			Cookie::queue('facebookLogin', 0);
			Cookie::queue('twitterLogin', 0);
			Cookie::queue('instagramLogin', 0);


			$fp = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();

			if ( $fp ) {
				$fp->expired = 1;
				$fp->save();
			}

			$tp = TwitterProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();

			if ( $tp ) {
				$tp->expired = 1;
				$tp->save();
			}

			if ($come_from == 'is_mobile') {
				$already_participate = $this->joinContestFromMobile($user->id, $contest->id);
			}
			else {
				$already_participate = $this->joinContestFromWebsite($user->id, $contest->id);
			}

			if (($already_participate) && ($contest->unique_participation))
			{
				//$message = "Questa e-mail ha gi� partecipato";
				$message = trans('messages.mail_already_participate');
				return Redirect::to('login/'.$id)->with('message', $message);
			}
			else //non ha mai partecipato facendo foto o non � un contest da unique participation
			{
				return Redirect::to('website/widget/connect/'. $id);
			}

		}

	}



	public function chooseAction($id)
	{
		//id � l'id del contest
		$contest = Contest::find($id);
		$contest_type = $contest->type_id;
		return View::make('tab/choose')->with('contest_id', $id)->with('contest_type', $contest_type)->with('user_id', $user_id);
	}

	public function getLogout()
	{
		Log::info('getLogut Function');
        Auth::logout();
		$id = Session::get('widget.id_contest');
        Session::flush();
		//$facebook = new Facebook(Config::get('facebook'));
		$contest = Contest::find($id);
		$facebook = new Facebook\Facebook([
			'app_id' => $contest->company->fbApp_clientID,
			'app_secret' => $contest->company->fbApp_clientSecret,
			'default_graph_version' => 'v2.4',
		]);

		$helper = $facebook->getRedirectLoginHelper();
		//TODO: trovare modo per passare access token alla successiva funzione
		$logouturl = $helper->getLogoutUrl($accessToken, $next);

		//$logouturl = $facebook->getLogoutUrl();
		//$facebook->destroySession();
		Session::put("ask_user_to_login", true);
		$errors = null;
        $message = null;
        return View::make('website/widget/login')->with('errors', $errors)->with('message', $message)->with('id', $id);
	}

    public function index($id)
    {

        //$url = action('Website_WidgetController@login', $id);
		$contest_id = Input::get('contest_id');
        $contest = Contest::find($contest_id);
        $contestImage = $contest->image;
        return View::make('website/widget/index')->with('id', $id)->with('url', $url)->with('contestImage', $contestImage);


    }

    public function login($id)
    {


		//memorize id contest in the session
		Session::put('widget.id_contest', $id);
        //if (CommonHelper::loggedInAsUser() && (Session::get('ask_user_to_login') == false))
		if (CommonHelper::loggedInAsUser())
        {
            $userId = Auth::user()->id;
            Log::info('User (id: '.$userId.') started to use widget.');
            $config = Config::get('hdfvr');
            $domain = $config['domain'];
            return View::make('website/widget/record')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain);
        }
        else
        {
			//Session::put("ask_user_to_login", false);
            $errors = null;
            $message = null;
            if (Request::isMethod('post'))
            {
                $userData = array(
			    	'username' => Input::get('username'),
			        'password' => Input::get('password')
			    );
				Log::debug("User credentials. username: ".$userData["username"].", password: ".$userData["password"]);
			    //@TODO: create login for user with user role
			    $user = UserHelper::loginUser($userData);

			    if ($user)
			    {
					$userId = $user->id;
                    Log::info('User (id: '.$userId.') logged to system in widget');
                    return Redirect::action('Website_WidgetController@record', array($id));
				}
                else
                {
					Log::info('Wrong data login (username: '.$userData['username'].')');
                    $message =  trans('messages.wrong_data_login');
                }
            }
            return View::make('website/widget/login')->with('errors', $errors)->with('message', $message)->with('id', $id);
        }

    }


	public function record()
    {
    	Log::info('record Function');
		$contest_type = Input::get('contest_type');
		$contest_id = Input::get('contest_id');
		//$fb_id = Input::get('fb_id');
		$user_id = Input::get('user_id');
		//$userId = Auth::user()->id;
		$contest = Contest::find($contest_id);
		//$facebookProfile = FacebookProfile::where('id_fb', $fb_id)->where('company_id', $contest->company_id)->first();
		//$user=User::where('id_fb',$fb_id)->first();
		Log::info('User logged as id= '.$user_id);
		$config = Config::get('hdfvr');
		$domain = $config['domain'];
		//App::setLocale($contest->location->language);
		return View::make('website/widget/recording')->with('contestId', $contest_id)->with('userId', $user_id)->with('contest_type', $contest_type)->with('domain', $domain)->with('contest', $contest);


		/*
        if (CommonHelper::loggedInAsUser())
        {
        	Log::info('user logged in as user');
            $userId = Auth::user()->id;
			Log::info('User logged as id= '.$userId);
            $config = Config::get('hdfvr');
            $domain = $config['domain'];
            return View::make('website/widget/record')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain);
        }
        else
        {
        	Log::info('user logged in not as user');
            return Redirect::to('website/widget/login/'.$id);
			/*$userId = Auth::user()->id;
			Log::info('User logged as id= '.$userId);
            $config = Config::get('hdfvr');
            $domain = $config['domain'];
            return View::make('website/widget/record')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain);
        }
        */
    }

    public function saveVideo($videoName, $duration, $contestId, $userId, $title)
    {
        //@todo: save to database
        //$lol  = Input::upload('picture', 'C:/xampp/htdocs/jmtv/public/videos/', $videoName.'.mp4'); var_dump($lol);exit;
        Log::debug("saveVideo(videoName=".$videoName.",duration=".$duration.",contestId=".$contestId.",userId=".$userId.")");
        $config = Config::get('hdfvr');
        $error = ContestHelper::convertFLV2MP4($videoName, $config['pathToVideoCommandLine']);
		$contest = Contest::find($contestId);

        if($error)
        {
        	Log::error("Error occured while converting ".$videoName." to MP4. Returning with error message");
            $config = Config::get('hdfvr');
            $domain = $config['domain'];
            App::setLocale($contest->location->language);
			return View::make('website/widget/record')->with('contestId', $contestId)->with('userId', $userId)->with('domain', $domain)->with('error', $error)->with('contest', $contest);
        }
        else
        {
        	Log::debug("Successfully converted ".$videoName." to MP4. Proceeding with save.");
        	//Log::debug("Joining the contest ".$contestId);
        	//UserHelper::joinTheContest($contestId);
        	Log::debug("Saving the video ".$videoName);
            $videoId = ContestHelper::saveVideo($videoName, $duration, $contestId, $userId, $title);
        	App::setLocale($contest->location->language);
			return View::make('tab/confirm')->with('contest', $contest)->with('video_id', $videoId);
        }

    }

    //paolo code
    public function loginResume($id, $videopart)
    {
    	Log::info('function loginResume');
    	if (CommonHelper::loggedInAsUser())
    	{
    		$userId = Auth::user()->id;
    		Log::info('User (id: '.$userId.') started to use widget with Resume.');
    		$config = Config::get('hdfvr');
    		$domain = $config['domain'];
    		return View::make('website/widget/recordResume')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain)->with('videopart', $videopart);
    	}
    	else
    	{
    		$errors = null;
    		$message = null;
    		if (Request::isMethod('post'))
    		{
    			$userData = array(
    					'username' => Input::get('username'),
    					'password' => Input::get('password')
    			);
    			Log::debug("User credentials. username: ".$userData["username"].", password: ".$userData["password"]);
    			//@TODO: create login for user with user role
    			$user = UserHelper::loginUser($userData);

    			if ($user)
    			{
    				$userId = $user->id;
    				Log::info('User (id: '.$userId.') logged to system in widget');
    				return Redirect::action('Website_WidgetController@record', array($id, $videopart));
    			}
    			else
    			{
    				Log::info('Wrong data login (username: '.$userData['username'].')');
    				$message =  trans('messages.wrong_data_login');
    			}
    		}
    		return View::make('website/widget/loginResume')->with('errors', $errors)->with('message', $message)->with('id', $id)->with('videopart',$videopart);
    	}

    }


    //paolo code
    public function facebookResume($id, $videopart)
    {
    	Log::info('facebookResume function');
    	if (CommonHelper::loggedInAsUser())
    	{
    		$userId = Auth::user()->id;
    		Log::info('User (id: '.$userId.') started to use widget.');
    		$config = Config::get('hdfvr');
    		$domain = $config['domain'];
    		return View::make('website/widget/recordResume')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain)->with('videopart', $videopart);
    	}
    	else
    	{
    		//Log::info('Attempt to login with Facebook.');
    		$facebook = new Facebook(Config::get('facebook'));
    		$params = array(
    				'redirect_uri' => url('website/login/facebook_login/'.$id.'/'.$videopart),
    				'display' => 'popup',
    				'scope' => 'email',
    		);

    		return Redirect::to($facebook->getLoginUrl($params));
    	}
    }

    public function getTag($friends_tag_array)
	{
		for($i=0 ;$i<count($friends_tag_array);$i++)
		{
			$tag = array('tag_uid' => $friends_tag_array[$i]['uid'],'x' =>$friends_tag_array[$i]['x'],'y' =>$friends_tag_array[$i]['y']);
			$tag_data[]=$tag;
		}
	   return $tag_data;
	}






    public function facebookLoginResume($id, $videopart)
    {
    	$code = Input::get('code');
    	if (strlen($code) == 0)
    	{
    		Log::warning('Error while attempt to connect with Facebook.');
    		$message = trans('messages.error_communicating_facebook');
    		return Redirect::to('/website/widget/login/'.$id.'/'.$videopart)->with('message', $message);
    	}

    	$facebook = new Facebook(Config::get('facebook'));
    	$uid = $facebook->getUser();

    	if ($uid == 0)
    	{
    		$message = trans('messages.facebook_error');
    		Log::warning('Error while attempt to connect with Facebook.');
    		return Redirect::to('/website/widget/login/'.$id.'/'.$videopart)->with('message', $message);
    	}

    	$facebook_account = $facebook->api('/me');
    	$accountId = $facebook_account['id'];
    	$user = DB::table('facebook_profiles')->where('uid', $accountId)->first();
    	//@TODO - this is temporary because there is no integration with facebook
    	//<!-- TEMPORARY STARTS -->
    	$config = Config::get('website');
    	$userJson = UserHelper::loginUser(array("username" => $config["general_username"], "password" => $config["general_password"]));
    	Log::debug("Facebook user = ".$userJson);
    	$user = User::find($userJson->id);
    	//<!-- TEMPORARY ENDS -->
    	if($user)
    	{
    		$userId = $user->id;
    		$user = User::find($userId);
    		Auth::login($user);
    		Log::info('Login to system with facebook(facebook_account: '.json_encode($facebook_account,JSON_PRETTY_PRINT).', userId: '.$userId.').');
    		return Redirect::action('Website_WidgetController@recordResume', array($id, $videopart));

    	}
    	else
    	{
    		CompanyHelper::addCompanyUserWithFacebook($facebook_account);
    		return Redirect::action('Website_WidgetController@recordResume', array($id, $videopart));
    	}
    }



    public function twitter_old($id)
    {
        if (CommonHelper::loggedInAsUser())
        {
            $userId = Auth::user()->id;
            Log::info('User (id: '.$userId.') started to use widget.');
            $config = Config::get('hdfvr');
            $domain = $config['domain'];
            return View::make('website/widget/record')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain);
        }
        else
        {
            $config = Config::get('twitter');
            $connection = new TwitterOAuth($config['consumer_key'], $config['consumer_secret']);
            if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')
            {
            	$callback = $config['oauth_callback_https'].$id;
            }
            else
            {
            	$callback = $config['oauth_callback_http'].$id;
            }
            $request_token = $connection->getRequestToken($callback); //get Request Token
            if( $request_token)
            {
                $token = $request_token['oauth_token'];
                switch ($connection->http_code)
                {
                    case 200:
                        $url = $connection->getAuthorizeURL($token);
                        Log::info('Attempt to login with Twitter.');
                        return Redirect::to($url);
                    default:
                        $message = trans('messages.twitter_connection_error');
                        Log::warning('Error while attempt to connect with Twitter.');
                        return Redirect::to('/website/widget/login/'.$id)->with('message', $message);
                }
            }
            else //error receiving request token
            {
                $message = trans('messages.twitter_request_token_error');
                Log::warning('Twitter request token error.');
                return Redirect::to('/website/widget/login/'.$id)->with('message', $message);
            }
        }

    }


    public function twitterResume($id, $videopart)
    {
    	if (CommonHelper::loggedInAsUser())
    	{
    		$userId = Auth::user()->id;
    		Log::info('User (id: '.$userId.') started to use widget.');
    		$config = Config::get('hdfvr');
    		$domain = $config['domain'];
    		return View::make('website/widget/record')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain);
    	}
    	else
    	{
    		$config = Config::get('twitter');
    		$connection = new TwitterOAuth($config['consumer_key'], $config['consumer_secret']);
    		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')
    		{
    			$callback = $config['oauth_callback_https'].$id;
    		}
    		else
    		{
    			$callback = $config['oauth_callback_http'].$id;
    		}
    		$request_token = $connection->getRequestToken($callback); //get Request Token
    		if( $request_token)
    		{
    			$token = $request_token['oauth_token'];
    			switch ($connection->http_code)
    			{
    				case 200:
    					$url = $connection->getAuthorizeURL($token);
    					Log::info('Attempt to login with Twitter.');
    					return Redirect::to($url);
    				default:
    					$message = trans('messages.twitter_connection_error');
    					Log::warning('Error while attempt to connect with Twitter.');
    					return Redirect::to('/website/widget/login/'.$id)->with('message', $message);
    			}
    		}
    		else //error receiving request token
    		{
    			$message = trans('messages.twitter_request_token_error');
    			Log::warning('Twitter request token error.');
    			return Redirect::to('/website/widget/login/'.$id)->with('message', $message);
    		}
    	}

    }

    public function twitterLogin_old($id)
    {
        if(isset($_GET['oauth_token']))
        {
            $config = Config::get('twitter');
            $connection = new TwitterOAuth($config['consumer_key'], $config['consumer_secret'], $config['oauth_token'], $config['oauth_token_secret']);
            $params =array();
            $params['include_entities']='false';
            $content = $connection->get('account/verify_credentials',$params);
            if($content && isset($content->screen_name) && isset($content->name))
            {
                //@TODO:login with current account or save to database
                //$content->name;
               // $content->profile_image_url;
               // $content->screen_name;

                UserHelper::signUpWithTwitter(null);
                $user = User::find(1);
                Auth::login($user);
                return Redirect::action('Website_WidgetController@record', array($id));

            }
            else
            {
                return Redirect::to('website/widget/login');
            }
        }

    }

    public function recordResume($id, $videopart)
    {
    	Log::info('recordResume Function');
    	if (CommonHelper::loggedInAsUser())
    	{
    		Log::info('user logged in as user');
    		$userId = Auth::user()->id;
    		$config = Config::get('hdfvr');
    		$domain = $config['domain'];
    		return View::make('website/widget/recordResume')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain)->with('videopart', $videopart);
    	}
    	else
    	{
    		Log::info('user logged in not as user');
    		return Redirect::to('website/widget/login');
    	}

    }


    public function saveVideopart($videoName, $duration, $contestId, $userId, $videopart)
    {
    	//@todo: save to database
    	//$lol  = Input::upload('picture', 'C:/xampp/htdocs/jmtv/public/videos/', $videoName.'.mp4'); var_dump($lol);exit;
    	Log::debug("saveVideopart(videoName=".$videoName.",duration=".$duration.",contestId=".$contestId.",userId=".$userId.")");
    	$config = Config::get('hdfvr');
    	$error = ContestHelper::convertFLV2MP4($videoName, $config['pathToVideoCommandLine']);
    	if($error)
    	{
    		Log::error("Error occured while converting ".$videoName." to MP4. Returning with error message");
    		$config = Config::get('hdfvr');
    		$domain = $config['domain'];
    		return View::make('website/widget/record')->with('contestId', $contestId)->with('userId', $userId)->with('domain', $domain)->with('error', $error);
    	}
    	else
    	{
    		Log::debug("Successfully converted ".$videoName." to MP4. Proceeding with save.");
    		Log::debug("Joining the contest ".$contestId);
    		UserHelper::joinTheContest($contestId);
    		Log::debug("Saving the video ".$videoName);
    		$videoId = ContestHelper::saveVideoPart($videoName, $duration, $contestId, $userId, $videopart);
    		return View::make('website/widget/confirmResume')->with('videoId', $videoId)->with("contestId",$contestId);
    	}

    }


    public function saveResume($id)
    {
    	Log::debug("Save Total Resume function");
    	//catturare i due video in base al contest, all'id dell'utente (per il momento no) e
    	//alle due videopart
    	$config = Config::get('admin');
    	$contest = Contest::find($id);

    	$collection = $contest->videos;

    	$collection = $collection->filter(function($video)
    	{
    		if ($video->name == 'video_part') {
    			return true;
    		}
    	});

    	//collect list of videos to be joined
    	$videoparts = array();

    	foreach ($collection as $video)
    	{

    		$inputVideoFilename = $config['videos_folder_path']."/".$video->filename;

    		$videopart = $video->sub_videopart;

    		//unire ogni singolo video con una slide, alla fine unire i due video

    		//ottenere video di 5 secondi
    		//ffmpeg -s 640x480 -r 30 -loop 1  -i /temp/WHY.png -t 00:00:05 -c:v libx264 -y -pix_fmt yuv420p /temp/slide.mp4

    		//mettere fade out dal 120esimo frame fino alla fine
    		//ffmpeg -i /temp/slide.mp4 -y -vf fade=out:120:30 /temp/slide_fade_out.mp4

    		//join dei 2 video
    		//ffmpeg -i /temp/slide_fade_out.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts /temp/intermediate1.ts
    		//ffmpeg -i /temp/samsungvideo.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts /temp/intermediate2.ts
    		//ffmpeg -i "concat:intermediate1.ts|intermediate2.ts" -c copy -bsf:a aac_adtstoasc /temp/outputResume.mp4
    		$videoFileName=md5(time());
    		if ($videopart == 1)
    		{
    			$filenameSlide = "/temp/WHY.png";
    			//$filenameOut = "/temp/".$videoFileName.".mp4";
    			$filenameOut = tempnam(null,null).".mp4";
    			//$cmdCreateVideoSlide = "/usr/bin/ffmpeg -s 640x480 -r 30 -loop 1  -i /temp/WHY.png -t 00:00:05 -c:v libx264 -y -pix_fmt yuv420p /temp/".$videoFileName.".mp4";
    			$cmdCreateVideoSlide = sprintf($config['ffmpeg.createVideoSlide'],$filenameSlide,$filenameOut);
    			$cmdCreateVideoSlide = str_replace("\\", "/", $cmdCreateVideoSlide);
    		}
    		else if ($videopart == 2)
    		{
    			$filenameSlide = "/temp/DESCRIBE.png";
    			//$filenameOut = "/temp/".$videoFileName.".mp4";
    			$filenameOut = tempnam(null,null).".mp4";
    			//$cmdCreateVideoSlide = "/usr/bin/ffmpeg -s 640x480 -r 30 -loop 1  -i /temp/DESCRIBE.png -t 00:00:05 -c:v libx264 -y -pix_fmt yuv420p /temp/".$videoFileName.".mp4";
    			$cmdCreateVideoSlide = sprintf($config['ffmpeg.createVideoSlide'],$filenameSlide,$filenameOut);
    			$cmdCreateVideoSlide = str_replace("\\", "/", $cmdCreateVideoSlide);
    		}
    		else
    		{
    			throw new Exception("Error while reading videoparts!");
    		}


    		Log::debug("FFMPEG Create Video Slide cmdline: ".$cmdCreateVideoSlide);
    		$retVal = null;
    		system($cmdCreateVideoSlide,$retVal);
    		Log::debug("ffmpeg return value: ".$retVal);
    		if ($retVal != 0)
    		{
    			throw new IOException("Error while creating video slide, cmdline: ".$cmdCreateVideoSlide);
    		}

    		$filenameOutwithFade = tempnam(null,null).".mp4";
    		//$cmdCreateFadeOutSlide = "/usr/bin/ffmpeg -i /temp/".$videoFileName.".mp4 -y -vf fade=out:120:30 /temp/".$videoFileNameSlide.".mp4";
    		$cmdCreateFadeOutSlide = sprintf($config['ffmpeg.createSlideFadeOut'],$filenameOut,$filenameOutwithFade);
    		$cmdCreateFadeOutSlide = str_replace("\\", "/", $cmdCreateFadeOutSlide);

    		Log::debug("FFMPEG Create Video Slide Fade out cmdline: ".$cmdCreateFadeOutSlide);
    		$retVal = null;
    		system($cmdCreateFadeOutSlide,$retVal);
    		Log::debug("ffmpeg return value: ".$retVal);
    		if ($retVal != 0)
    		{
    			throw new IOException("Error while creating fade out for slide, cmdline: ".$cmdCreateFadeOutSlide);
    		}

    		//$cmd1 = "/usr/bin/ffmpeg -i /temp/".$videoFileNameSlide.".mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts /temp/".$videoFileNameSlide.".ts";
    		$filenameOutwithFadeConv = tempnam(null,null).".ts";
    		$cmd1 = sprintf($config['ffmpeg.createFirstIntermediate'],$filenameOutwithFade,$filenameOutwithFadeConv);
    		$cmd1 = str_replace("\\", "/", $cmd1);

    		Log::debug("FFMPEG Create Video Intermediate for Slide cmdline: ".$cmd1);
    		$retVal = null;
    		system($cmd1,$retVal);
    		Log::debug("ffmpeg return value: ".$retVal);
    		if ($retVal != 0)
    		{
    			throw new IOException("Error while creating conversion video slide, cmdline: ".$cmd1);
    		}


    		//$cmd2 = "/usr/bin/ffmpeg -i ".$inputVideoFilename." -c copy -bsf:v h264_mp4toannexb -f mpegts /temp/".$videoPartIntermediate.".ts";
    		$filenameOutwithResumeConv = tempnam(null,null).".ts";
    		$inputVideoFilename = "/var/www/html/jmtvapi/shared/".$inputVideoFilename;
    		$cmd2 = sprintf($config['ffmpeg.createSecondIntermediate'],$inputVideoFilename,$filenameOutwithResumeConv);
    		$cmd2 = str_replace("\\", "/", $cmd2);

    		Log::debug("FFMPEG Create Video Intermediate for Video Part cmdline: ".$cmd2);
    		$retVal = null;
    		system($cmd2,$retVal);
    		Log::debug("ffmpeg return value: ".$retVal);
    		if ($retVal != 0)
    		{
    			throw new IOException("Error while creating conversion video Resume, cmdline: ".$cmd2);
    		}

    		$videoFinalWithSlide=tempnam(null,null).".mp4";
    		//$cmdFinal = "/usr/bin/ffmpeg -i 'concat:/temp/".$videoFileNameSlide.".ts|/temp/".$videoPartIntermediate.".ts' -c copy -bsf:a aac_adtstoasc /temp/".$videoFinalWithSlide.".mp4";
    		$cmdFinal = sprintf($config['ffmpeg.ConcatMp4Videos'],$filenameOutwithFadeConv,$filenameOutwithResumeConv,$videoFinalWithSlide);
    		$cmdFinal = str_replace("\\", "/", $cmdFinal);

    		Log::debug("FFMPEG Join Video with Slide cmdline: ".$cmdFinal);
    		$retVal = null;
    		system($cmdFinal,$retVal);
    		Log::debug("ffmpeg return value: ".$retVal);
    		if ($retVal != 0)
    		{
    			throw new IOException("Error while creating video with Slide, cmdline: ".$cmdFinal);
    		}

    		//replace the old video part with the new one, this: $videoFinalWithSlide
    		Log::debug("Video creared correctly. Renaming new video file with slide and creating backup of existing one");
    		if (rename($inputVideoFilename,$inputVideoFilename."-backup-".time()) == false)
    		{
    			throw new IOException("Could not rename ".$inputVideoFilename);
    		}
    		if (rename($videoFinalWithSlide,$inputVideoFilename) == false)
    		{
    			throw new IOException("Could not rename ".$videoFinalWithSlide." to ".$inputVideoFilename);
    		}


    		array_push($videoparts, $inputVideoFilename);

    	}

    	//ciclare sulle nuove videopart e unirle in un unico video creando un nuovo record nella tab Video

    		$inputVideoFilename = $videoparts[0];


    		$filenameFirstConv = tempnam(null,null).".ts";
    		$cmd1 = sprintf($config['ffmpeg.createFirstIntermediate'],$inputVideoFilename,$filenameFirstConv);
    		$cmd1 = str_replace("\\", "/", $cmd1);

    		Log::debug("FFMPEG Create Video Intermediate number 1: ".$cmd1);
    		$retVal = null;
    		system($cmd1,$retVal);
    		Log::debug("ffmpeg return value: ".$retVal);
    		if ($retVal != 0)
    		{
    			throw new IOException("Error while creating conversion video slide, cmdline: ".$cmd1);
    		}

    		$inputVideoFilename = $videoparts[1];

    		$filenameSecondConv = tempnam(null,null).".ts";
    		$cmd2 = sprintf($config['ffmpeg.createSecondIntermediate'],$inputVideoFilename,$filenameSecondConv);
    		$cmd2 = str_replace("\\", "/", $cmd2);

    		Log::debug("FFMPEG Create Video Intermediate for Video Part cmdline: ".$cmd2);
    		$retVal = null;
    		system($cmd2,$retVal);
    		Log::debug("ffmpeg return value: ".$retVal);
    		if ($retVal != 0)
    		{
    			throw new IOException("Error while creating conversion video Resume, cmdline: ".$cmd2);
    		}

    		$videoFinal=tempnam(null,null).".mp4";
    		//$cmdFinal = "/usr/bin/ffmpeg -i 'concat:/temp/".$videoFileNameSlide.".ts|/temp/".$videoPartIntermediate.".ts' -c copy -bsf:a aac_adtstoasc /temp/".$videoFinalWithSlide.".mp4";
    		$cmdFinal = sprintf($config['ffmpeg.ConcatMp4Videos'],$filenameFirstConv,$filenameSecondConv,$videoFinal);
    		$cmdFinal = str_replace("\\", "/", $cmdFinal);

    		Log::debug("FFMPEG Join Videos cmdline: ".$cmdFinal);
    		$retVal = null;
    		system($cmdFinal,$retVal);
    		Log::debug("ffmpeg return value: ".$retVal);
    		if ($retVal != 0)
    		{
    			throw new IOException("Error while creating joined video, cmdline: ".$cmdFinal);
    		}

    	$videoId = ContestHelper::saveVideo($cmdFinal, "0.00", $id, "216", "BlackShape Interview");
    	return View::make('website/widget/confirm')->with('videoId', $videoId)->with("contestId",$contestId);

    }

    public function resetResume($id)
    {
    	//cancellare in maniera soft tutti i video part per ripetere la procedura
    	//prima della cancellazione � opportuno far uscire un alert

    }

	public function signup($id) {
		return View::make('website/widget/signup')->with('contest_id', $id);
	}


	public function resetCookie($id, $userId) {

		$idFromCookie = Cookie::get('user_id');
		$contest = Contest::find($id);

		if ( $idFromCookie ) {
			Cookie::queue('user_id', 0);
			$fp = FacebookProfile::where('user_id', $userId)->where('company_id', $contest->company_id)->first();
			if ( $fp ) {
				$fp->expired = 1;
				$fp->save();
			}

			Cookie::queue('facebookLogin', 0);
			Cookie::queue('twitterLogin', 0);
			Cookie::queue('instagramLogin', 0);

		}

		return Redirect::to('login/'. $id);

	}


	public function choose($id, $user_id)
	{
		if ( !$user_id ) {
			return Redirect::to('website/widget/connect/'. $id . '?first_step=connect&error=social');
		}

		$contest = Contest::find($id);
		Log::debug("needsUserRegistration: " . $contest->needsUserRegistration());
		if($contest->needsUserRegistration())
			$step = $contest->makeStepClass(3,4, $contest->location->language);
		else
			$step = $contest->makeStepClass(2,3, $contest->location->language);
		$contest_type = $contest->type_id;
		App::setLocale($contest->location->language);


		/////////////// CUSTOMIZZAZIONE //////////////////////
		if ( $contest->max_entries && Session::get('num_entry') && Session::get('num_entry') < $contest->max_entries ) {

			$photos = array();
			foreach(Session::get('entry') as $e) {
				$p = Photo::find($e);
				array_push($photos, $p);
			}

			return View::make('tab/choose')
			->with('contest_id', $id)
			->with('contest_type', $contest_type)
			->with('user_id', $user_id)
			->with('contest', $contest)
			->with('step', $step)
			->with('photos', $photos);
		} else {
			return View::make('tab/choose')
			->with('contest_id', $id)
			->with('contest_type', $contest_type)
			->with('user_id', $user_id)
			->with('contest', $contest)
			->with('step', $step);
		}
		///////////////////////////////////////////////////////


	}


	public function twitterLogin($id) {

		$oauthVerifier = Input::get('oauth_verifier');
		$twitteroauth = new TwitterOAuth('tY2MCEnw0hwTr1ZpYLkHo7Afv', 'dXihbXKGQ6VDYmeoC7GzoFGqjLN99aOZ9heECfTuKIoloJrBEe',  Session::get('twitter_oauth_token'), Session::get('twitter_oauth_token_secret'));
		$access_token = $twitteroauth->getAccessToken($oauthVerifier);
		$user_info = $twitteroauth->get('account/verify_credentials');

		$contest = Contest::find($id);

		$idFromCookie = Cookie::get('user_id');

		if ( $idFromCookie ) {

			$user = User::where('id', $idFromCookie)->first();
			Log::debug("User retrieved from cookie: " . $user->email);

		} else {

			$user = User::where('email', $user_info->id . '@t.com')->first();

			// There is a user
			if ($user) {

				$facebookProfileConnected = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
				if ( $facebookProfileConnected ) {
					$facebookProfileConnected->expired = 1;
					$facebookProfileConnected->save();
				}


				Log::debug("User already present: " . $user->id);

			// Create new user
			} else {

				$user = new User();
				Log::debug("New user created: " . $user->id);

			}

			$user->email = $user_info->id . '@t.com';
			$user->password = Hash::make(Config::get("facebook")["secret_password"]);
			$user->name = $user_info->name;
			$user->save();

			Cookie::queue('user_id', $user->id);
			Log::debug("Created new Cookie for USER ID: " . $user->id);

		}

		Cookie::queue('twitterLogin', 1);

		Log::debug("Twitter profile start");

		$twitterProfile = TwitterProfile::where("company_id", $contest->company_id)->where("user_id", $user->id)->first();
		if ( $twitterProfile ) {

			Log::debug("Twitter profile founded: " . $twitterProfile->tw_id);

		} else {

			$twitterProfile = new TwitterProfile();
			Log::debug("New Twitter profile: " . $user_info->id);

		}

		$twitterProfile->user_id = $user->id;
		$twitterProfile->tw_id = $user_info->id;
		$twitterProfile->name = $user_info->name;
		$twitterProfile->screen_name = $user_info->screen_name;
		$twitterProfile->oauth_token = $access_token['oauth_token'];
		$twitterProfile->oauth_token_secret = $access_token['oauth_token_secret'];
		$twitterProfile->company_id = $contest->company_id;
		$twitterProfile->expired = 0;

		$twitterProfile->save();

		Log::debug("Update Twitter profile end");


		////////


		if ( isset($_GET['device']) && $_GET['device'] == 'mobile' ) {
			$this->joinContestFromMobile($user->id, $contest->id);
		} else {
			$this->joinContestFromWebsite($user->id, $contest->id);
		}
		////////


		$cachedPassword = $user->password;
		$user->password = Hash::make(Config::get("facebook")["secret_password"]);
		$user->save();

		$userJson = UserHelper::loginUser(array("username" => $user->email, "password" => Config::get("facebook")["secret_password"]));
		Log::debug("Twitter user = ".$userJson);
		Auth::login(User::find($userJson->id));
		Log::info('Login to system with twitter(twitter_account: '.json_encode($user_info,JSON_PRETTY_PRINT).', user: '.$user_info->id.').');

		$user->password = $cachedPassword;
		$user->save();


		return Redirect::to('website/widget/connect/'. $id);

	}

	public function instagramLogin($id) {

		$contest = Contest::find($id);

		$idFromCookie = Cookie::get('user_id');

		if ( $idFromCookie ) {

			$user = User::where('id', $idFromCookie)->first();

		} else {

			$user = User::where('name', 'Utente Instagram di Test')->first();

			if ( $user ) {

			} else {

				$user = new User();
				$user->name = "Utente Instagram di Test";
				$user->email = "testInstagram@test.it";
				$user->password = Hash::make(Config::get("facebook")["secret_password"]) ;
				$user->save();

			}

			Cookie::queue('user_id', $user->id);

			Cookie::queue('twitterLogin', 0);

			$fp = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();

			if ( $fp ) {
				$fp->expired = 1;
				$fp->save();
			}

		}

		Cookie::queue('instagramLogin', 1);


		return Redirect::to('website/widget/connect/'. $id);
	}

	public function twitter($id) {

		$redirect_url = '';
		$device = Input::get('device');
		if ( $device == 'mobile') {
			$redirect_url = '/website/login/twitter-login/'.$id . '?device=mobile';
		} else {
			$redirect_url = '/website/login/twitter-login/'.$id . '?device=desktop';
		}


		// The TwitterOAuth instance
		$twitteroauth = new TwitterOAuth('tY2MCEnw0hwTr1ZpYLkHo7Afv', 'dXihbXKGQ6VDYmeoC7GzoFGqjLN99aOZ9heECfTuKIoloJrBEe');
		// Requesting authentication tokens, the parameter is the URL we will be redirected to
		$request_token = $twitteroauth->getRequestToken(asset($redirect_url));
		//$request_token = $twitteroauth->getRequestToken(asset('/website/widget/twitter-test/'.$id));

		// Saving them into the session
		Session::put('twitter_oauth_token', $request_token['oauth_token']);
		Session::put('twitter_oauth_token_secret', $request_token['oauth_token_secret']);



		// If everything goes well..
		if($twitteroauth->http_code==200){
			// Let's generate the URL and redirect
			$url = $twitteroauth->getAuthorizeURL($request_token['oauth_token']);
			return Redirect::to($url);
		} else {
			// It's a bad idea to kill the script, but we've got to know when there's an error.
			die('Something wrong happened.');
		}


	}

	public function twitterTest($id) {

		$oauthVerifier = Input::get('oauth_verifier');

		// TwitterOAuth instance, with two new parameters we got in twitter_login.php
		$twitteroauth = new TwitterOAuth('tY2MCEnw0hwTr1ZpYLkHo7Afv', 'dXihbXKGQ6VDYmeoC7GzoFGqjLN99aOZ9heECfTuKIoloJrBEe',  Session::get('twitter_oauth_token'), Session::get('twitter_oauth_token_secret'));
		// Let's request the access token
		$access_token = $twitteroauth->getAccessToken($oauthVerifier);
		// Save it in a session var
		//$_SESSION['access_token'] = $access_token;
		// Let's get the user's info
		$user_info = $twitteroauth->get('account/verify_credentials');
		// Print user's info
		print_r($access_token);

	}

	public function connect($id) {

		//////////////// CUSTOMIZZAZIONE /////////////////////////
		Session::forget('num_entry');
		Session::forget('entry');
		//////////////////////////////////////////////////////////

		$contest = Contest::find($id);

		$user_id = 0;
		$facebookConnected = null;
		$twitterConnected = null;

		if ( isset($_GET['first_step']) &&  $_GET['first_step'] == "connect" ) {

			Cookie::queue('user_id', 0);
			Cookie::queue('facebookLogin', 0);
			Cookie::queue('twitterLogin', 0);

		} else {

			$user = User::find(Cookie::get('user_id'));
			$user_id = $user->id;
			$facebookConnected = Cookie::get('facebookLogin');
			$twitterConnected = Cookie::get('twitterLogin');
		}

		if ( $contest->needsUserRegistration() ) {
			$step = $contest->makeStepClass(2, 4, $contest->location->language);
		} else {
			$step = $contest->makeStepClass(1, 3, $contest->location->language);
		}


		Log::debug("twitterConnected: " . $twitterConnected);
		Log::debug("facebookConnected: " . $facebookConnected);
		Log::debug("idFromCookie: " . Cookie::get('user_id'));
		Log::debug("user_id: " . $user_id);

		$alert = null;
		if ( isset($_GET['error']) &&  $_GET['error'] == "social" ) {
			$alert = 'Connect at least to one social';
		}

		$redirectToChoose = true;
		if ( $contest->fbPages->count() > 0 && !$facebookConnected ) {
			$redirectToChoose = false;
		}

		if ( $contest->twPages->count() > 0 && !$twitterConnected ) {
			$redirectToChoose = false;
		}

		Log::debug("redirectToChoose: " . $redirectToChoose);

		if ( !$redirectToChoose ) {

			App::setLocale($contest->location->language);
			return View::make('website/widget/connect')
						->with('contest', $contest)
						->with('user_id', $user_id)
						->with('checkFacebook', $facebookConnected)
						->with('checkTwitter', $twitterConnected)
						->with('checkInstagram', Cookie::get('instagramLogin'))
						->with('alert', $alert)
						->with('step', $step);

		} else {

			if ( $contest->type_id == 1 || $contest->type_id == 2 ) {
				$request = Request::create('/website/widget/choose/' . $contest->id . "/" . $user_id, 'GET', array());
				return Route::dispatch($request)->getContent();
			} else if ( $contest->type_id == 3 ) {
				$request = Request::create('/website/widget/insert_essay/' . $contest->id, 'GET', array());
				return Route::dispatch($request)->getContent();
			}



		}

	}

	public function instagram($id) {

		$instagram = new Instagram(array(
			'apiKey' => 'b1d3c4324fbe4e4c917e1add4771a74a',
			'apiSecret' => '8dc01d3d8ff344808504c9f3ed3edaa2',
			'apiCallback' => asset('/website/widget/instagram-test') // Callback URL
		));

		$loginUrl = $instagram->getLoginUrl();
		return Redirect::to($loginUrl);

	}

	public function instagramTest() {

		$code = $_GET['code'];

		$instagram = new Instagram(array(
			'apiKey' => 'b1d3c4324fbe4e4c917e1add4771a74a',
			'apiSecret' => '8dc01d3d8ff344808504c9f3ed3edaa2',
			'apiCallback' => asset('/website/widget/instagram-test') // Callback URL
		));

		if (true === isset($code)) {

			$data = $instagram->getOAuthToken($code);


		}



	}

	public function insertEssay($id) {

		$contest = Contest::find($id);
		App::setLocale($contest->location->language);

		if ( $contest->needsUserRegistration() ) {
			$step = $contest->makeStepClass(3, 4, $contest->location->language);
		} else {
			$step = $contest->makeStepClass(2, 3, $contest->location->language);
		}

		return View::make('website/widget/insertEssay')->with('contest', $contest)->with("step", $step);

	}

}
?>
