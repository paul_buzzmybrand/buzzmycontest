<?php


class Website_CBCWidgetController extends Controller {

   
    public function index($id) 
    {
        $url = action('Website_CBCWidgetController@login', $id);
        $contest = Contest::find($id);
        $contestImage = $contest->image;
        return View::make('website/widget_CBC_Thailand/index')->with('id', $id)->with('url', $url)->with('contestImage', $contestImage);
    }
    
    public function login()
    {
        /*if (CommonHelper::loggedInAsUser())
        {
            $userId = Auth::user()->id;
            Log::info('User (id: '.$userId.') started to use widget.');
            $config = Config::get('hdfvr');
            $domain = $config['domain'];
            return View::make('website/widget_CBC_Thailand/record')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain);
        }
        else
        {
			//provide standard access token (test1@test1.com), then automated login (for CBC Thailand)
			$access_token = CommonHelper::needAnyAccessToken();
			$json = UserHelper::getByAccessToken($access_token);
			$userId = $json->id;
			return Redirect::action('Website_CBCWidgetController@record', array($id));*/
			
		
            $errors = null;
            $message = null;
            if (Request::isMethod('post'))
            {
                $userData = array(
			    	'username' => 'test1@test1.com',
			        'password' => 'test1'
			    );
				$project = Input::get('project');
				if ($project == "")
				{
					Log::info('Project not inserted!');
                    $message =  "Please enter the Project Name";
					return View::make('website/widget_CBC_Thailand/login')->with('errors', $errors)->with('message', $message)->with('id', 2);
				}
				Log::debug("User credentials. username: ".$userData["username"].", password: ".$userData["password"]);
			    //@TODO: create login for user with user role
			    $user = UserHelper::loginUser($userData);

			    if ($user)
			    {
					$userId = 2;
                    Log::info('User (id: '.$userId.') logged to system in widget');
                    return Redirect::action('Website_CBCWidgetController@record', array(2, 'project' => $project));
				} 
                else
                {
					Log::info('Wrong data login (username: '.$userData['username'].')');
                    $message =  trans('messages.wrong_data_login');
					
                }
            }
            return View::make('website/widget_CBC_Thailand/login')->with('errors', $errors)->with('message', $message)->with('id', 2);
        
        
    }
    
    public function facebook($id)
    {
        if (CommonHelper::loggedInAsUser())
        {
            $userId = Auth::user()->id;
            Log::info('User (id: '.$userId.') started to use widget.');
            $config = Config::get('hdfvr');
            $domain = $config['domain'];
            return View::make('website/widget_CBC_Thailand/record')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain);
        }
        else
        {
            //Log::info('Attempt to login with Facebook.');
            $facebook = new Facebook(Config::get('facebook'));
            $params = array(
                'redirect_uri' => url('website/login/facebook_login/'.$id),
                'display' => 'popup',
                'scope' => 'email',
             );
            
            return Redirect::to($facebook->getLoginUrl($params));
        }
    }
    
    public function facebookLogin($id)
    {
        $code = Input::get('code');
        if (strlen($code) == 0)
        { 
            Log::warning('Error while attempt to connect with Facebook.');
            $message = trans('messages.error_communicating_facebook');
            return Redirect::to('/website/widget_CBC_Thailand/login/'.$id)->with('message', $message);
        }

        $facebook = new Facebook(Config::get('facebook'));
        $uid = $facebook->getUser();

        if ($uid == 0)
        {
            $message = trans('messages.facebook_error');
            Log::warning('Error while attempt to connect with Facebook.');
            return Redirect::to('/website/widget_CBC_Thailand/login/'.$id)->with('message', $message);
        }

        $facebook_account = $facebook->api('/me');
        $accountId = $facebook_account['id'];
        $user = DB::table('facebook_profiles')->where('uid', $accountId)->first();
        //@TODO - this is temporary because there is no integration with facebook
        //<!-- TEMPORARY STARTS -->
        $config = Config::get('website');
        $userJson = UserHelper::loginUser(array("username" => $config["general_username"], "password" => $config["general_password"]));
        Log::debug("Facebook user = ".$userJson);
        $user = User::find($userJson->id);
        //<!-- TEMPORARY ENDS -->
        if($user)
        {
            $userId = $user->id;
            $user = User::find($userId);
            Auth::login($user);
            Log::info('Login to system with facebook(facebook_account: '.json_encode($facebook_account,JSON_PRETTY_PRINT).', userId: '.$userId.').');
            return Redirect::action('Website_CBCWidgetController@record', array($id));
            
        }
        else
        {
            CompanyHelper::addCompanyUserWithFacebook($facebook_account);
            return Redirect::action('Website_CBCWidgetController@record', array($id));
        }  
    }
    
    public function twitter($id)
    {
        if (CommonHelper::loggedInAsUser())
        {
            $userId = Auth::user()->id;
            Log::info('User (id: '.$userId.') started to use widget.');
            $config = Config::get('hdfvr');
            $domain = $config['domain'];
            return View::make('website/widget_CBC_Thailand/record')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain);
        }
        else
        {
            $config = Config::get('twitter');
            $connection = new TwitterOAuth($config['consumer_key'], $config['consumer_secret']);
            if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')
            {
            	$callback = $config['oauth_callback_https'].$id;
            }
            else
            {
            	$callback = $config['oauth_callback_http'].$id;
            }
            $request_token = $connection->getRequestToken($callback); //get Request Token     
            if( $request_token)
            {
                $token = $request_token['oauth_token'];
                switch ($connection->http_code) 
                {
                    case 200:
                        $url = $connection->getAuthorizeURL($token);
                        Log::info('Attempt to login with Twitter.');
                        return Redirect::to($url);
                    default:
                        $message = trans('messages.twitter_connection_error');
                        Log::warning('Error while attempt to connect with Twitter.');
                        return Redirect::to('/website/widget_CBC_Thailand/login/'.$id)->with('message', $message);
                }
            }
            else //error receiving request token
            {
                $message = trans('messages.twitter_request_token_error');
                Log::warning('Twitter request token error.');
                return Redirect::to('/website/widget_CBC_Thailand/login/'.$id)->with('message', $message);
            }
        }
        
    }
    
    public function twitterLogin($id)
    {
        if(isset($_GET['oauth_token']))
        {
            $config = Config::get('twitter');
            $connection = new TwitterOAuth($config['consumer_key'], $config['consumer_secret'], $config['oauth_token'], $config['oauth_token_secret']);
            $params =array();
            $params['include_entities']='false';
            $content = $connection->get('account/verify_credentials',$params);
            if($content && isset($content->screen_name) && isset($content->name))
            {
                //@TODO:login with current account or save to database
                //$content->name;
               // $content->profile_image_url;
               // $content->screen_name;
              
                UserHelper::signUpWithTwitter(null);
                $user = User::find(1);
                Auth::login($user);
                return Redirect::action('Website_CBCWidgetController@record', array($id));

            }
            else
            {
                return Redirect::to('website/widget_CBC_Thailand/login');
            }
        }
    
    }
    
    public function record($id)
    {
        if (CommonHelper::loggedInAsUser())
        {
            $userId = Auth::user()->id;
            $config = Config::get('hdfvr');
            $domain = $config['domain'];
            return View::make('website/widget_CBC_Thailand/record')->with('contestId', $id)->with('userId', $userId)->with('domain', $domain);
        }
        else
        {
            return Redirect::to('website/widget_CBC_Thailand/login');
        }
        
    }
    
    public function saveVideo($videoName, $duration, $contestId, $userId, $title)
    {
        //@todo: save to database
        //$lol  = Input::upload('picture', 'C:/xampp/htdocs/jmtv/public/videos/', $videoName.'.mp4'); var_dump($lol);exit;
        Log::debug("saveVideo(videoName=".$videoName.",duration=".$duration.",contestId=".$contestId.",userId=".$userId.")");
        $config = Config::get('hdfvr');
        $error = ContestHelper::convertFLV2MP4($videoName, $config['pathToVideoCommandLine']);
        if($error)
        {
        	Log::error("Error occured while converting ".$videoName." to MP4. Returning with error message");
            $config = Config::get('hdfvr');
            $domain = $config['domain'];
            return View::make('website/widget_CBC_Thailand/record')->with('contestId', $contestId)->with('userId', $userId)->with('domain', $domain)->with('error', $error);
        }
        else
        {
        	Log::debug("Successfully converted ".$videoName." to MP4. Proceeding with save.");
        	Log::debug("Joining the contest ".$contestId);
        	UserHelper::joinTheContest($contestId);
        	Log::debug("Saving the video ".$videoName);
            $videoId = ContestHelper::saveVideo($videoName, $duration, $contestId, $userId, $title);  
        	return View::make('website/widget_CBC_Thailand/confirm')->with('videoId', $videoId)->with("contestId",$contestId);
        }
        
    }
    
}
?>
