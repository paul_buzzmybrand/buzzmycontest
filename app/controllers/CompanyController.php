<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CompanyController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
            try
            {
                $company = Company::with('contests')->findOrFail($id);
                $statusCode = 200;
                $response = Response::make($company, $statusCode);    
                return $response;
            }
            catch(ModelNotFoundException $e)
            {
                $statusCode = 200;
                $description = 'Company Not Found';
                $response = Response::json(array('description'=>$description, 'errorCode'=>404),$statusCode);
                return $response;
            }
	}
        
        public function showCompanyVideos($id)
	{
            try
            {
                $company = Company::with('contests')->findOrFail($id);
                $contestList = Contest::where('company_id',$company->id)->get(array('id','name'));
              //return $contestList;
                //$videos = array();
                $companyVideosArray = array();
                $companyJMTScore = 0;
                $i = 0;
                foreach($contestList as $contest)
                {
                    //echo($contest->id);
                    //echo ('and');
                    $videosTemp=Video::whereHas('contests',function($query) use ($contest){
                        $query->where('contests.id',$contest->id);
                    })->get()->toArray();
                   // dd($videosTemp);
                    //return $videosTemp;
                    if (empty($videosTemp))
                    {
                        continue;
                    }
                    else
                    {  
                        
                        foreach($videosTemp as $videos)
                        {
                            $companyJMTScore = $videos['score'] + $companyJMTScore;
                            $companyVideosArray[$i]['id'] = $videos['id'];
                            $companyVideosArray[$i]['name'] = $videos['name'];
                            $companyVideosArray[$i]['duration'] = $videos['duration'];
                            $companyVideosArray[$i]['image'] = $videos['image'];
                            $companyVideosArray[$i]['filename'] = $videos['filename'];
                            $companyVideosArray[$i]['score'] = $videos['score'];
                            $companyVideosArray[$i]['user_id'] = $videos['user_id'];
                            $companyVideosArray[$i]['location'] = $videos['location'];
                            //$contestId = $videos[0]['videos'][$i]['pivot']['contest_id'];
                            $contestName = $contest['name'];//Contest::where('id',$contestId)->get(array('name'));
                            //return $contestName;
                            $companyVideosArray[$i]['contestName'] = $contestName;
                            $i++;
                        }
                        /*
                        foreach ($videosTemp as $videosList)
                        {
                            $videos[] = $videosList;
                        }
                         * 
                         */
                    }
                }
                //return $videos;
                //var_dump($videos);
                $statusCode = 200;
                
                $response = Response::json(array('companyJMTScore'=>$companyJMTScore,'companyVideos'=>$companyVideosArray), $statusCode);    
                return $response;
            }
            catch(ModelNotFoundException $e)
            {
                $statusCode = 404;
                $description = 'Not Found';
                $response = Response::make($description,$statusCode);
                return $response;
            }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
        
        /****** Company Controller for web ********/
        /* web part */
        
        public function postRegister()
        {
            $rules = array(
                'username'=>'required|min:2|unique:users',
                'email'=>'required|email|unique:companys',
                'password'=>'required|alpha_num|min:6|confirmed',
                'password_confirmation'=>'required|alpha_num|min:6'
            );
            $validator = Validator::make(Input::all(), $rules);
            
            if ($validator->fails()) {
                return Redirect::to('create')->with('message', 'The following errors occurred')->withErrors($validator)->withInput();
            }
            else 
            {
                $company = new Company;
                $company->name = Input::get('company_name');
                $company->website = Input::get('website');
                $company->address = Input::get('address');
                $company->phone = Input::get('phone');
                $company->city = Input::get('city');
                $company->email = Input::get('email');
                
                $result = $company->save();
                
                if($result)
                {
                    $email = Input::get('email');    
                    $companyUser = DB::table('companys')->where('email', $email)->first();
                    $id = $companyUser -> id;
                    /* user data*/
                    $user = new User;

                    $user->username = Input::get('username');
                    $user->first_name = Input::get('firstname');
                    $user->last_name = Input::get('lastname');

                    $user->address = Input::get('address');

                    $user->email = Input::get('email');
                    $user->gender = Input::get('sex');
                    $user->country = Input::get('country');
                    $user->city = Input::get('city');
                    $user->phone = Input::get('phone');
                    $user->company_id = $id;

                    $user->password = Hash::make(Input::get('password'));

                    $user->save();
                }
                else
                {
                    DB::table('companys')->where('name',$name)->delete();
                }
                
                return Redirect::to('/');
            }
        }
        
        
        public function getProfile()
        {
            //return View::make('company');
            $company_id = Session::get('companyid');
            
            //$contest = Company::with('contest')->get();
            if (Session::has('companyid'))
            {
                $reward_types = DB::table('reward_types')->get();
                $contest = Company::with('contests')->find($company_id);
                
                return View::make('company')
                    ->with('companyWithContests', $contest)
                    ->with('reward_types',$reward_types);
                
                //View::share('reward_types',$reward_types);
                
            //return $contest;
            }
            else
            {
                return Redirect::to('/');
            }    
            
        }
        
        public function postCompanyrecord()
        {
            $rules = array(
                'email'=>'required|email',
                'name'=>'required'
            );
            $validator = Validator::make(Input::all(), $rules);
            
            if ($validator->fails()) {
                Session::flash('message1', 'Error! please enter mandatory fields to edit company record!');
                return Redirect::to('company/profile');
            }
            else 
            {
                $company_id = Input::get('id');
               
                $company = Company::find($company_id);
                
                $image = Input::file('company_image');
                if($image)
                {
                    $image_name = $image -> getClientOriginalName();
                    $destinationPath = 'images/companys';
                    $company->image = $image_name;
                    $upload_appImage = Input::file('company_image')->move($destinationPath, $image_name);
                }
                
                $company->name = Input::get('name');
                $company->description = Input::get('description');
                $company->address = Input::get('address');
                $company->contact_name = Input::get('contact_name');
                $company->email = Input::get('email');
                $company->website = Input::get('website');
                $company->phone = Input::get('phone');
                $company->fax = Input::get('fax');
                
                $company->save();
                
                Session::flash('message1', 'Record successfully updated!');
                return Redirect::to('company/profile');
            }
            
        }
        
        public function showCompanyPage()
        {
            $companys = Company::all();
            if(Session::has('username'))
            {
                return View::make('companyAdmin')->with('companys',$companys); 
            }
            else
            {
               return Redirect::to('admin'); 
            }
                
        }
        
        public function deleteCompany($id)
        {
            $event = Company::find($id);
            $event -> delete();
            
            return Redirect::to('company')->with('ErrorMessage', 'Data successfully deleted!');
        }
        
        public function registerNewCompany()
        {
            $rules = array(
                'companyName'=>'required',
                'email'=> 'required|email|unique:companys',
                'website'=>'required',
            );
            
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()) {
                return Redirect::to('company')->with('ErrorMessage','Registration is failed due to:')->withErrors($validator)->withInput();
            }
            else
            {
                $company = new Company;
                
                $company->name = Input::get('companyName');
                $company->contact_name = Input::get('contactName');
                $company->address = Input::get('address');
                $company->email = Input::get('email');
                //$company->zip = Input::get('zip');
                $company->city = Input::get('city');
                $company->phone = Input::get('phone');
                $company->fax = Input::get('fax');
                $company->website = Input::get('website');
              //  $company->email = Input::get('email');
                $company->facebook_link = Input::get('facebookPage');
                $company->twitter_link = Input::get('twitterPage');
                
                $image = Input::file('company_image');
                if($image)
                {
                    $image_name = $image -> getClientOriginalName();
                    $destinationPath = 'images/companys';
                    $company->image = $image_name;
                    $upload_appImage = Input::file('company_image')->move($destinationPath, $image_name);
                }
                else
                {
                    $company->image = 'default.png';
                }
               
                $result = $company->save();
                
                return Redirect::to('company')->with('ErrorMessage', 'Registered successfully!');
                
            }
        }
        
        public function updateCompany()
        {
            $rules = array(
                'companyName'=>'required',
                'email'=> 'required|email',
                'website'=>'required',
               
            );
            
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()) {
                return Redirect::to('company')->with('ErrorMessage','Unable to update due to following errors:')->withErrors($validator)->withInput();
            }
            else
            {
                $companyId = Input::get('company_id');
                
                $company = Company::find($companyId);
                
                $company->name = Input::get('companyName');
                $company->contact_name = Input::get('contactName');
                $company->address = Input::get('address');
                $company->email = Input::get('email');
               // $company->zip = Input::get('zip');
                $company->city = Input::get('city');
                $company->phone = Input::get('phone');
                $company->fax = Input::get('fax');
                $company->website = Input::get('website');
                $company->email = Input::get('email');
                $company->facebook_link = Input::get('facebookPage');
                $company->twitter_link = Input::get('twitterPage');
                
                $image = Input::file('company_image');
                if($image)
                {
                    $image_name = $image -> getClientOriginalName();
                    $destinationPath = 'images/companys';
                    $company->image = $image_name;
                    $upload_appImage = Input::file('company_image')->move($destinationPath, $image_name);
                }
               
                $result = $company->save();
                
                return Redirect::to('company')->with('ErrorMessage', 'Data successfully updated!');
            
            }
        }
}