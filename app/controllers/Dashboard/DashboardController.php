<?php

class Dashboard_DashboardController extends BaseController {	


	function __construct() 
	{
		$this->beforeFilter('auth_company', ['except' => ['getSettings']]);
	}

	public function getCurrentUser()
	{
		// return Response::make('OCCRISTO', 400);

        $user = Auth::user();
		
		//Log::debug("GETCURRENTUSER: ".($user->isSuperAdmin() ? 'true' : 'false'));

        return Response::json([
            'user_id' => $user->id,
            'company_name' => ($user->company ? $user->company->name : ''),
            'user_name' => $user->name,
            'user_surname' => $user->surname,
            'user_job_title' => $user->job_title,
            'user_country' => ($user->country ? Countries::getOne($user->country, App::getLocale(), 'cldr') : 'IT'),
            'user_phone' => $user->telephone,
            'user_email' => $user->email,
            'is_admin' => $user->isSuperAdmin(),
            'is_email_confirmed' => (bool) $user->email_confirmed,
            'profile_image_url' => $user->company->dashboard_image,
            'company_id' => $user->company_id,
        ]);
        // For debugging
		return Response::json([
			'company_name' => 'Yourapp',
			'user_name' => 'Cristiano',
			'user_surname' => 'Valente',
			'user_job_title' => 'developer',
			'user_country' => 'Italy',
			'user_phone' => '328 8631333',
			'user_email' => 'cristiano.valente@gmail.com',
			'is_admin' => true,
			'is_email_confirmed' => true,
			'company_id' => 27,
			'profile_image' => ''
		]);
	}

	public function getTranslationStrings($lang)
	{
		if(!in_array($lang, ['en', 'it']))
			return Response::json(['error' => "Invalid language code: $lang"], 404);

        App::setlocale($lang);
		return Response::json(Lang::get('dashboard'));
	}

	public function getSettings()
	{
		return Response::view('dashboard/settings');
	}
}