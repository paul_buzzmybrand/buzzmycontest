<?php

class SettingController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$ownerId = ResourceServer::getOwnerId();
                $userSettings = Setting::where('user_id',$ownerId)->get();
                $statusCode = 200;
                
                $response = Response::make($userSettings, $statusCode);    
                return $response;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
                $inputs = Input::all();
                if(!isset($inputs['facebook']) || !isset($inputs['twitter']) || !isset($inputs['youtube']))
                {
                        $statusCode=200;
                        $description='Required Paramaters not Found';
                        $response = Response::make(array('description'=>$description,'statusCode'=>400), $statusCode);
                        return $response;
                }
                $ownerId = ResourceServer::getOwnerId();
                $userSettingsData = Setting::where('user_id',$ownerId)->first();
                if(!isset($userSettingsData) || empty($userSettingsData))
                {
                    $userSettings = new Setting;
                    $userSettings -> user_id = $ownerId;
                    $userSettings -> facebook = $inputs['facebook'];
                    $userSettings -> twitter = $inputs['twitter'];
                    $userSettings -> youtube = $inputs['youtube'];
                    $userSettings -> save();
                }
                else
                {
                    $userSettings = Setting::where('user_id',$ownerId)->first();
                    $userSettings -> facebook = $inputs['facebook'];
                    $userSettings -> twitter = $inputs['twitter'];
                    $userSettings -> youtube = $inputs['youtube'];
                    $userSettings -> save();
                }
                $statusCode = 200;
                
                $response = Response::make($userSettings, $statusCode);    
                return $response;
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}