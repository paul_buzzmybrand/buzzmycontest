<?php
use Guzzle\Http\Message\Response;
class Admin_ContestController extends BaseController {

	/*function __construct() 
	{
		$this->beforeFilter('auth_company');
	}
	*/

	/**
	 * Display a listing of all external contests
	 *
	 * @return Response
	 */
	public function externalIndex($id)
	{
	
		Log::debug("in externalIndex");
	

			Log::debug(" externalIndex true ");
			//$contests = Contest::select("contests.*")->with("company","videos")->join("companys","companys.id","=","contests.company_id")->where("companys.type_id",2)->get();
			
			$contests = Contest::select("contests.*")->with("company","videos", "photos")->join("companys","companys.id","=","contests.company_id")->where("companys.id",$id)->get();
			
			$queries = DB::getQueryLog();
            Log::debug("Queries: ".print_r($queries,true));
			Log::debug("Contests about to be rendered: ".print_r($contests,true));
			$company = Company::where('id', $id)->first();
            return View::make("admin/external-contests-list")->with("contests",$contests)->with("company_name", $company->name);

		
	}
	
	public function externalAdminIndex()
	{


			//$contests = Contest::select("contests.*")->with("company","videos")->join("companys","companys.id","=","contests.company_id")->where("companys.type_id",2)->get();
			$contests = Contest::select("contests.*")->with("company","videos")->join("companys","companys.id","=","contests.company_id")->get();
			$queries = DB::getQueryLog();
            Log::debug("Queries: ".print_r($queries,true));
			Log::debug("Contests about to be rendered: ".print_r($contests,true));
            return View::make("admin/external-contests-list")->with("contests",$contests)->with("company_name", "BuzzMyBrand Dashboard");


	}
	
	public function contestManagement()
	{


			$contests = Contest::select("contests.*")->join("companys","companys.id","=","contests.company_id")->get();
			$companys = Company::select("companys.*")->join("contests","contests.company_id","=","companys.id")->distinct()->get(array('companys.*'));
			$locations = Location::select("locations.*")->get();
			return View::make("admin/contestManagement")->with("contests",$contests)->with("companys",$companys)->with("locations", $locations);

		
	}
	
	
	

	
	/**
	 * Show information about a specific content
	 * @param unknown $id
	 */
	public function contest($id)
	{

			//ContestHelper::checkContestShiftWatermark2YouTube($id); 
			
			$contest = Contest::with("videosForApproval", "videosForApproval.approvalStep", "photosForApproval", "photosForApproval.approvalStep", "videos.fbPages", "photos.fbPages", "videos.user", "photos.user")->find($id);

			
			//calculating number of videos awaiting approval
			$awaitingApprovalCount = 0;
			$videosWatermarkedCount = 0;
			$photosWatermarkedCount = 0;
			$videosAlreadyApprovedCount = 0;
			$photosAlreadyApprovedCount = 0;
			$statistics = array();
			if ($contest->type_id == 1)
			{
				
				$statistics_global = array(
										'reach' => 0, 
										'impressions' => 0, 
										'engaged_users' => 0, 
										'unique_views' => 0,
										'views' => 0,
										'unique_likes' => 0,
										'unique_comments' => 0,
										'unique_shares' => 0,
										);
				
				
				foreach ($contest->videos as $video)
				{
				
				
					if ($video->approval_step_id == 1)
					{
						$awaitingApprovalCount++;
					}
					if ($video->watermarkInsDone)
					{
						$videosWatermarkedCount++;
					}
					if ($video->approval_step_id == 3)
					{
						$videosAlreadyApprovedCount++;
					}
					
					// START Aggregazione statistiche per video //
					$statistics[$video->id] = array(
													'unique_likes' => 0, 
													'unique_comments' => 0, 
													'unique_shares' => 0, 
													'unique_views' => 0,
													'reach' => 0,
													);
					
										
					foreach ($video->fbPages as $vp) {
						
						$statistics[$video->id]['unique_likes'] += $vp->pivot->unique_likes;
						$statistics[$video->id]['unique_comments'] += $vp->pivot->unique_comments;
						$statistics[$video->id]['unique_shares'] += $vp->pivot->unique_shares;
						$statistics[$video->id]['unique_views'] += $vp->pivot->unique_views;
						$statistics[$video->id]['reach'] += $vp->pivot->reach;
						
						$statistics_global['reach'] += $vp->pivot->reach;
						$statistics_global['impressions'] += $vp->pivot->impressions;
						$statistics_global['engaged_users'] += $vp->pivot->engaged_users;
						$statistics_global['unique_views'] += $vp->pivot->unique_views;
						$statistics_global['views'] += $vp->pivot->views;
						$statistics_global['unique_likes'] += $vp->pivot->unique_likes;
						$statistics_global['unique_comments'] += $vp->pivot->unique_comments;
						$statistics_global['unique_shares'] += $vp->pivot->unique_shares;
					
					}
					// END Aggregazione statistiche per video //
					
				}
				$contest->awaitingApprovalCount = $awaitingApprovalCount;
				$contest->videosWatermarkedCount = $videosWatermarkedCount;
				$contest->videosAlreadyApprovedCount = $videosAlreadyApprovedCount;
				if ($contest->awaitingApprovalCount > 0)
				{
					$contest->mayFinishApproval = true;
				}
				if ($contest->videosAlreadyApprovedCount > 0)
				{
					$contest->mayResetApproval = true;
				}
				//if ($contest->contentApprovalDone && $contest->watermarkInsDone == 0)
				if ($contest->watermarkInsDone == 0)
				{
					$contest->mayInsertWatermark = true;
				}
				//if ($contest->watermarkInsDone && $contest->uploadOnYoutubeDone == 0)
				if ($contest->uploadOnYoutubeDone == 0 && ($contest->company->yt_access_token) && $contest->should_upload_to_company_youtube == 1)
				{
					$contest->mayUploadYouTube = true;
				}
				
			}
			else if ($contest->type_id == 2)
			{
				
				$statistics_global = array(
										'reach' => 0, 
										'impressions' => 0, 
										'engaged_users' => 0, 
										'unique_views' => 0,
										'views' => 0,
										'unique_likes' => 0,
										'unique_comments' => 0,
										'unique_shares' => 0,
										);
				

				foreach ($contest->photos as $photo)
				{ 
					if ($photo->approval_step_id == 1)
					{
						$awaitingApprovalCount++;
					}
					if ($photo->watermarkInsDone)
					{
						$photosWatermarkedCount++;
					}
					if ($photo->approval_step_id == 3)
					{
						$photosAlreadyApprovedCount++;
					}
					
					
					// START Aggregazione statistiche per video //
					$statistics[$photo->id] = array(
													'unique_likes' => 0, 
													'unique_comments' => 0, 
													'unique_shares' => 0, 
													'unique_views' => 0,
													'reach' => 0,
													);
					
										
					foreach ($photo->fbPages as $fp) {
						
						$statistics[$photo->id]['unique_likes'] += $fp->pivot->unique_likes;
						$statistics[$photo->id]['unique_comments'] += $fp->pivot->unique_comments;
						$statistics[$photo->id]['unique_shares'] += $fp->pivot->unique_shares;
						$statistics[$photo->id]['reach'] += $fp->pivot->reach;
						
						$statistics_global['reach'] += $fp->pivot->reach;
						$statistics_global['impressions'] += $fp->pivot->impressions;
						$statistics_global['engaged_users'] += $fp->pivot->engaged_users;
						$statistics_global['unique_likes'] += $fp->pivot->unique_likes;
						$statistics_global['unique_comments'] += $fp->pivot->unique_comments;
						$statistics_global['unique_shares'] += $fp->pivot->unique_shares;
					
					}
					// END Aggregazione statistiche per video //
					
					$statistics[$photo->id]['unique_likes'] += $photo->likes_count_post;
					$statistics[$photo->id]['unique_comments'] += $photo->comments_count_post;
					
					$statistics_global['unique_likes'] += $photo->likes_count_post;
					$statistics_global['unique_comments'] += $photo->comments_count_post;
					
				}
				$contest->awaitingApprovalCount = $awaitingApprovalCount;
				$contest->photosWatermarkedCount = $photosWatermarkedCount;
				$contest->photosAlreadyApprovedCount = $photosAlreadyApprovedCount;
				if ($contest->awaitingApprovalCount > 0)
				{
					$contest->mayFinishApproval = true;
				}
				if ($contest->photosAlreadyApprovedCount > 0)
				{
					$contest->mayResetApproval = true;
				}
				//if ($contest->contentApprovalDone && $contest->watermarkInsDone == 0)
				if ($contest->watermarkInsDone == 0)
				{
					$contest->mayInsertWatermark = true;
				}
				
				$contest->mayUploadYouTube = false;
				
			}
			
			$resultForLoggedFromWebsite = DB::table('contest_user')
											->select(DB::raw('contest_id, count(*) as logged_from_website'))
											->where('contest_id', '=', $id)
											->whereNotNull('login_website')
											->whereNull('login_mobile')
											->groupBy('contest_id')
											->get();
											
			$resultForLoggedFromMobile = DB::table('contest_user')
											->select(DB::raw('contest_id, count(*) as logged_from_mobile'))
											->where('contest_id', '=', $id)
											->whereNotNull('login_mobile')
											->whereNull('login_website')
											->groupBy('contest_id')
											->get();
											
			$resultForLoggedFromWebsiteAndMobile = DB::table('contest_user')
													->select(DB::raw('contest_id, count(*) as logged_from_website_and_mobile'))
													->where('contest_id', '=', $id)
													->whereNotNull('login_mobile')
													->whereNotNull('login_website')
													->groupBy('contest_id')
													->get();
			
			$logged_from_website = 0;
			if ( count($resultForLoggedFromWebsite) > 0 ) {
				$logged_from_website = $resultForLoggedFromWebsite[0]->logged_from_website;
			}
			
			$logged_from_mobile = 0;
			if ( count($resultForLoggedFromMobile) > 0 ) {
				$logged_from_mobile = $resultForLoggedFromMobile[0]->logged_from_mobile;
			}
			
			$logged_from_website_and_mobile = 0; 
			if ( count($resultForLoggedFromWebsiteAndMobile) > 0 ) {
				$logged_from_website_and_mobile = $resultForLoggedFromWebsiteAndMobile[0]->logged_from_website_and_mobile;
			}
			
			$queries = DB::getQueryLog();			
			
			Log::debug("Queries: ".print_r($queries,true));
			Log::debug("Contest about to be rendered: ".print_r($contest,true));
			$usernameLogin = UserHelper::getAdminUserLoggedIn();
			
			
			return View::make("admin/contest")
						->with("contest",$contest)
						->with("username", $usernameLogin)
						->with('statistics', $statistics)
						->with('statistics_global', $statistics_global)
						->with('logged_from_website', $logged_from_website)
						->with('logged_from_mobile', $logged_from_mobile)
						->with('logged_from_website_and_mobile', $logged_from_website_and_mobile);
		
			

		
	}
	
	/**
	 * Presents information about the specific video
	 * @param unknown $id
	 */
	public function video($id)
	{

			$ffmpeg = "/usr/bin/ffmpeg";
			$rotation = "0";
			$video = Video::with("Contest")->find($id);
			
			// get rotation of the video
			ob_start();
			passthru($ffmpeg . " -i " .base_path()."/public/videos/".$video->filename . " 2>&1");
			$duration_output = ob_get_contents();
			ob_end_clean();

			// rotate?
			if (preg_match('/rotate *: (.*?)\n/', $duration_output, $matches))
			{
				$rotation = $matches[1];
				//rotare il file e sovrascriverlo se la rotazione corrisponde a 90
				if ($rotation == "90")
				{
					$extension = pathinfo(base_path()."/public/videos/".$video->filename)["extension"];
					$filenameRotate = tempnam(null,null).".".$extension;
					
					$cmdLine = sprintf($ffmpeg.' -i '.base_path()."/public/videos/".$video->filename.' -metadata:s:v:0 rotate=0 -vf "transpose=1" '.$filenameRotate);
					
					$cmdLine = str_replace("\\", "/", $cmdLine);
					Log::debug("FFMPEG rotate operation cmdline: ".$cmdLine);
					$retVal = null;
					system($cmdLine,$retVal);
					Log::debug("ffmpeg rotate operation return value: ".$retVal);
					if ($retVal != 0)
					{
						throw new IOException("Error while rotate operation, cmdline: ".$cmdLine);
					}
					
					if (rename($filenameRotate, base_path()."/public/videos/".$video->filename) == false)
					{
						throw new IOException("Could not rename ".$filenameRotate." to ".base_path()."/public/videos/".$video->filename);				
					}
					Log::info("Video ".$video->id." successfully rotated");
				}
				else if ($rotation == "180")
				{
					$extension = pathinfo(base_path()."/public/videos/".$video->filename)["extension"];
					$filenameRotate1 = tempnam(null,null).".".$extension;
					//fare 2 rotazioni
					$cmdLine = sprintf($ffmpeg.' -i '.base_path()."/public/videos/".$video->filename.' -metadata:s:v:0 rotate=0 -vf "transpose=1" '.$filenameRotate1);
					
					$cmdLine = str_replace("\\", "/", $cmdLine);
					Log::debug("FFMPEG first rotate operation cmdline: ".$cmdLine);
					$retVal = null;
					system($cmdLine,$retVal);
					Log::debug("ffmpeg first rotate operation return value: ".$retVal);
					if ($retVal != 0)
					{
						throw new IOException("Error while first rotate operation, cmdline: ".$cmdLine);
					}
					$filenameRotate2 = tempnam(null,null).".".$extension;
					$cmdLine = sprintf($ffmpeg.' -i '.$filenameRotate1.' -metadata:s:v:0 rotate=0 -vf "transpose=1" '.$filenameRotate2);
					$retVal = null;
					system($cmdLine,$retVal);
					Log::debug("ffmpeg second rotate operation return value: ".$retVal);
					if ($retVal != 0)
					{
						throw new IOException("Error while second rotate operation, cmdline: ".$cmdLine);
					}
					
					if (rename($filenameRotate2, base_path()."/public/videos/".$video->filename) == false)
					{
						throw new IOException("Could not rename ".$filenameRotate2." to ".base_path()."/public/videos/".$video->filename);				
					}
					Log::info("Video ".$video->id." successfully rotated");
				}
				else if ($rotation == "270")
				{
					$extension = pathinfo(base_path()."/public/videos/".$video->filename)["extension"];
					$filenameRotate = tempnam(null,null).".".$extension;
					//fare una rotazione di valore 2
					$cmdLine = sprintf($ffmpeg.' -i '.base_path()."/public/videos/".$video->filename.' -metadata:s:v:0 rotate=0 -vf "transpose=2" '.$filenameRotate);
					
					$cmdLine = str_replace("\\", "/", $cmdLine);
					Log::debug("FFMPEG rotate operation cmdline: ".$cmdLine);
					$retVal = null;
					system($cmdLine,$retVal);
					Log::debug("ffmpeg rotate operation return value: ".$retVal);
					if ($retVal != 0)
					{
						throw new IOException("Error while inserting watermark, cmdline: ".$cmdLine);
					}
					
					if (rename($filenameRotate, base_path()."/public/videos/".$video->filename) == false)
					{
						throw new IOException("Could not rename ".$filenameRotate." to ".base_path()."/public/videos/".$video->filename);				
					}
					Log::info("Video ".$video->id." successfully rotated");
				}
				
			}
			
			$queries = DB::getQueryLog();
			Log::debug("Queries: ".print_r($queries,true));
			Log::debug("Video about to be rendered: ".print_r($video,true));
			$usernameLogin = UserHelper::getAdminUserLoggedIn();
			return View::make("admin/video")->with("video",$video)->with("username", $usernameLogin)->with("rotation", $rotation);

	}
	
		/**
	 * Presents information about the specific photo
	 * @param unknown $id
	 */
	public function photo($id)
	{

			
			$photo = Photo::with("Contest")->find($id);
			$width = Image::make(base_path()."/public/photos/".$photo->filename)->width();
			$height = Image::make(base_path()."/public/photos/".$photo->filename)->height();
			//$heightPlayer = intval((640*$height)/$width);
			//$widthPlayer = 640;
			$heightPlayer = 480;
			$widthPlayer = intval((480*$width)/$height);
			$queries = DB::getQueryLog();
			Log::debug("Queries: ".print_r($queries,true));
			Log::debug("Photo about to be rendered: ".print_r($photo,true));
			$usernameLogin = UserHelper::getAdminUserLoggedIn();
			return View::make("admin/photo")->with("photo",$photo)->with('height', $heightPlayer)->with('width', $widthPlayer)->with("username", $usernameLogin);


	}
	
	/**
	 * Changes approval status of the video
	 */
	public function approveVideo($id)
	{
		$approvalStepId = CommonHelper::mandatoryInput("approvalStep");
				
		$video = Video::find($id);
		$contest = $video->contest;
			
		$sendNotification = false;
		if ( $video->approval_step_id == 1 &&  $approvalStepId == 2 ) {
			$sendNotification = true;
		} else if ( $video->approval_step_id == 3 &&  $approvalStepId == 2 ) {
			$sendNotification = true;
		} else if ( $video->approval_step_id == 3 &&  $approvalStepId == 1 ) {
			$sendNotification = true;
		}
		
		$video = Video::with(
						array('user.facebookProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},
						'user.twitterProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},)
					)->where('id', $id)->first();
				
		Log::debug("Changing video(".$id.") approval step to ".$approvalStepId);
		$approvalStep = ApprovalStep::find($approvalStepId);
		Log::debug("Approval step name: ".$approvalStep->name);
		
		/*if ($approvalStepId <> 3)
		{
			GCMHelper::schedule($video);
		}*/
		if (($approvalStepId == 1) || ($approvalStepId == 2))
		{
			
			foreach ($video->contest->fbPages as $page)					
			{
				$videosOfPage = $page->videos()->where('fb_page_video.video_id', '=', $video->id)->get();
				
				foreach ($videosOfPage as $video)
				{
					$video->fbPages()->detach($page->id);
					
					//richiamare funzione per cancellare post da fb 
					Scheduler::deleteVideoPost($id, $page, $video->pivot->fb_idpost);
				}
				
				
			}
			
			foreach ($video->contest->twPages as $page)	{
			
			
				$videosOfPage = $page->videos()->where('tw_page_video.video_id', '=', $video->id)->get();
				
				foreach ($videosOfPage as $video){
					$video->twPages()->detach($page->id);
					Scheduler::deletePostTwitter($page, $video->pivot->tw_idvideo);
				}
				
			}
			
			$video->uploadOnTwitterDone = 0;
			$video->save();
			
			foreach ($video->contest->ytPages as $page)	{
			
			
				$videosOfPage = $page->videos()->where('yt_page_video.video_id', '=', $video->id)->get();
				
				foreach ($videosOfPage as $video){
					$video->ytPages()->detach($page->id);
					YouTube::deleteVideo($page, $video->pivot->playlist_item, $video->pivot->yt_idvideo);
				}
				
			}
			
			$video->uploadOnCompanyYouTubeDone  = 0;
			$video->save();
			
			
			$page = ytPage::find(1);
			if ( $video->youtube_id) {
				YouTube::deleteVideo($page, $video->playlist_item, $video->youtube_id );
			}
			$video->uploadOnOwnYouTubeDone = 0;
			$video->save();			
			
			
			
			
			$video->score = 0.00;
			$video->rank_position = 0;
			$video->uploadOnFacebookDone = 0;
			$video->uploadOnOwnYouTubeDone = 0;
			$video->uploadOnCompanyYouTubeDone = 0;
			$video->fb_idpost_usr = '';
			$video->fb_source_url = '';
			$video->approval_step_id = $approvalStepId;
			$video->tw_idpost_usr = '';
			$video->youtube_id = '';
			$video->playlist_item = '';
			$video->total_count_link = 0;
			$video->save();
			
			//self::sendNegativeNotificationPhoto($id);
			self::sendNegativeNotificationVideo($id);
			Scheduler::UpdateRanking($video->contest_id);
			
			
			$contest = $video->contest;
			$user = $video->user;
			
			if ( ($contest->needsUserRegistration() ) && $sendNotification ) {
			
				try {
				
					Mail::send('emails.video-approvation', array('contest' => $contest, 'video' => $video), function($message) use ($user) {
						
						$message->to($user->email, $user->name . " " . $user->surname)
								->subject(Lang::get('email.object_video_contest_not_approved'));
					
					});
					
					$video->emailNotificationDone = 0;
				
				} catch (Exception $ex) {
					Log::error('Problem sending email: '.$ex);
				}
			
			}
						
			$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);
			
			$facebookProfile = null;
			if ( $video->user->facebookProfiles->count() > 0 ) {
				$facebookProfile = $video->user->facebookProfiles->first();
			}
			
			
			if ( $facebookProfile && $video->user_connected_to_facebook && $sendNotification ) {
			
				//Facebook desktop notify
				try
				{										
					//$acc_tokenResponse = $facebook->api("/oauth/access_token?client_id=".$contest->company->fbApp_clientID."&client_secret=".$contest->company->fbApp_clientSecret."&grant_type=client_credentials");
					//$app_access_token = $acc_tokenResponse["access_token"];
					App::setlocale($contest->location->language);
					$messageNotif = Lang::get('messages.video_not_approved', array('contestname' => $contest->name));
					
					$responseFBAPP = $facebook->post("/".$facebookProfile->id_fb."/notifications", array(
						"template" => $messageNotif,// and ask your friends to like, share, comment to increase your chances to win! The more your video is viral the more chances you have to win!
						"href" => "video/".$video->id,
					),  $contest->company->app_accesstoken);
					Log::debug("Desktop notification done!");
				}
				catch (Exception $ex)
				{
					Log::error('Error during sending notification: '.$ex);
				}
			
			}
			
			$twitterProfile = null;
			if ( $video->user->twitterProfiles->count() > 0 ) {
				$twitterProfile = $video->user->twitterProfiles->first();
			}
			
			if ( $twitterProfile && $video->user_connected_to_twitter && $sendNotification ) {
			
				$twPage = null;
				if ( $video->contest->twPages->count() > 0 ) {
					$twPage = $video->contest->twPages->first();
				}
			
				try {
				
					App::setlocale($contest->location->language);
					$message = Lang::get('messages.video_not_approved');
					
					$parameters = array(
						'user_id' => $twitterProfile->tw_id,
						'text' => $message,
					);
					
					$twitteroauth = new TwitterOAuth('tY2MCEnw0hwTr1ZpYLkHo7Afv', 'dXihbXKGQ6VDYmeoC7GzoFGqjLN99aOZ9heECfTuKIoloJrBEe',  $twPage->tw_page_admin_at , $twPage->tw_page_secret_token );
					$tweet_data = $twitteroauth->post('https://api.twitter.com/1.1/direct_messages/new.json', $parameters);			
				
					Log::debug("Twitter response for notification" . print_r($tweet_data, true));
				
					Log::debug("Twitter notification done!");
				
				} catch (Exception $ex){
					Log::error('Error during sending twitter direct message: '.$ex);
				}
			
			}

			$video->save();
			
		}		
		else if ($approvalStepId == 3)
		{
			
			$video->approval_step_id = $approvalStepId;
			$video->save();
			

			
		}
		
		if ( Input::get("api_call") ) {
			return json_encode(array("result"=> "1"));	
		} else {
			return Redirect::action("Admin_ContestController@contest",array("id" => $video->contest->id))->with("flash.message","Approval status changed")->with("flash.class","alert-success");
		}
		
	}
	
	public function sendNegativeNotificationVideo($id)
	{
		$video = Video::find($id);
		
		$app = null;
		$deviceToken = null;
		$message = null;
		
		$contest = Contest::find($video->contest_id);
		$company_user = Company::find($contest->company_id)->belongsToManyUsers()->where('user_id', $video->user->id)->first();
		
		if ( isset($company_user->pivot->token) && strpos($company_user->pivot->token,'Android_' ) !== false)
		{
			
			App::setlocale($video->contest->location->language);
			$nativeNotifString = Lang::get('messages.video_not_approved_native', array('videoname' => $video->name, 'contestdescription'=>$video->contest->description));
			
			$message = PushNotification::Message($nativeNotifString,array(
				'title' => $video->contest->name,
				'url' => 'https://www.buzzmybrand.it/video/'.$video->id));
			
			$deviceToken = str_replace("Android_", "", $company_user->pivot->token);
			Log::debug("Device token to be used: ".$deviceToken);
			$app = $video->contest->company->name.'Android';
			
			
		}
		else if (isset($company_user->pivot->token) && strpos($company_user->pivot->token,'IOS_') !== false)
		{
			App::setlocale($video->contest->location->language);
			$nativeNotifString = Lang::get('messages.video_not_approved_native', array('videoname' => $video->name, 'contestdescription'=>$video->contest->description));
			$message = PushNotification::Message($nativeNotifString, array('custom' => array('url' => 'https://www.buzzmybrand.it/video/'.$video->id)));
			
			$deviceToken = str_replace("IOS_", "", $company_user->pivot->token);
			Log::debug("Device token to be used: ".$deviceToken);
			$app = $video->contest->company->name;
			
			
		}
				
		if ( (isset($company_user->pivot->token) && $company_user->pivot->token != '' && $company_user->pivot->token != null) )
		{
			PushNotification::app($app)
				->to($deviceToken)
				->send($message);
		}
	}
	
	public function sendNegativeNotificationPhoto($id)
	{
		$photo = Photo::find($id);
		
		$contest = Contest::find($photo->contest_id);
		$company_user = Company::find($contest->company_id)->belongsToManyUsers()->where('user_id', $photo->user->id)->first();
		
		if (isset($company_user->pivot->token) && strpos($company_user->pivot->token,'Android_') !== false)
		{
			$message = PushNotification::Message('Your photo entitled "'.$photo->name.'"  for '.$photo->contest->name.' contest has not been approved! Follow the guidelines and try again!',array(
			'title' => $photo->contest->name,
			'url' => 'http://www.buzzmybrand.it/photo/'.$photo->id));
			$app = '';
			$deviceToken = str_replace("Android_", "", $company_user->pivot->token);
			Log::debug("Device token to be used: ".$deviceToken);
			$app = 'jtmAndroid';
		}
		else if (isset($company_user->pivot->token) && strpos($company_user->pivot->token,'IOS_') !== false)
		{
			$message = PushNotification::Message('Your photo entitled "'.$photo->name.'" for '.$photo->contest->name.' contest has not been approved! Follow the guidelines and try again!', array('custom' => array('url' => 'https://www.buzzmybrand.it/photo/'.$photo->id)));
			$app = '';
			$deviceToken = str_replace("IOS_", "", $company_user->pivot->token);
			Log::debug("Device token to be used: ".$deviceToken);
			if ($photo->contest->company->name == 'THAIGOVIRAL')
			{
				$app = 'TGViOS';
			}
			else if ($photo->contest->company->name == '2014 AFF SUZUKI CUP')
			{
				$app = 'jtmiOS';
			}
			
		}
		
		if ( (isset($company_user->pivot->token) && $company_user->pivot->token != '' && $company_user->pivot->token != null) )
		{
			PushNotification::app($app)
				->to($deviceToken)
				->send($message);
		}
	}
	
	/**
	 * Changes approval status of the video
	 */
	public function approvePhoto($id)
	{
	
		$approvalStepId = CommonHelper::mandatoryInput("approvalStep");
		
		$photo = Photo::find($id);
		$contest = $photo->contest;
		
		$sendNotification = false;
		if ( $photo->approval_step_id == 1 &&  $approvalStepId == 2 ) {
			$sendNotification = true;
		} else if ( $photo->approval_step_id == 3 &&  $approvalStepId == 2 ) {
			$sendNotification = true;
		} else if ( $photo->approval_step_id == 3 &&  $approvalStepId == 1 ) {
			$sendNotification = true;
		}
		
		$photo = Photo::with(
				array('user.facebookProfiles' => function($query) use ($contest) {
					$query->where('company_id', '=', $contest->company_id);
				},
				'user.twitterProfiles' => function($query) use ($contest) {
					$query->where('company_id', '=', $contest->company_id);
				},)
			)->where('id', $id)->first();
		
		Log::debug("Changing photo (".$id.") approval step to ".$approvalStepId);
		$approvalStep = ApprovalStep::find($approvalStepId);
		Log::debug("Approval step name: ".$approvalStep->name);
		//$photo->approval_step_id = $approvalStepId;
		//$photo->save();
		/*if ($approvalStepId <> 3)
		{
			GCMHelper::schedule($photo);
		}*/
		if (($approvalStepId == 1) || ($approvalStepId == 2))
		{
			//cancellare il post da facebook, cancellare la riga del post, resettare lo score
			
			
			foreach ($photo->contest->fbPages as $page)					
			{
				$photosOfPage = $page->photos()->where('fb_page_photo.photo_id', '=', $photo->id)->get();
				
				foreach ($photosOfPage as $photo)
				{
					
					//richiamare funzione per cancellare post da fb 
					Scheduler::deletePhotoPost($id, $page, $photo->pivot->fb_idphoto);
					
					$photo->fbPages()->detach($page->id);
					
					
				}
				
				
			}
			
			foreach ($photo->contest->twPages as $page)	{
			
			
				$photosOfPage = $page->photos()->where('tw_page_photo.photo_id', '=', $photo->id)->get();
				
				foreach ($photosOfPage as $photo){
					$photo->twPages()->detach($page->id);
					Scheduler::deletePostTwitter($page, $photo->pivot->tw_idphoto);
				}
				
			}
			
			$photo->uploadOnTwitterDone = 0;
			$photo->save();
			
						
			$photo->score = 0.00;
			$photo->rank_position = 0;
			$photo->uploadOnFacebookDone = 0;
			$photo->uploadOnTwitterDone = 0;
			$photo->uploadOnInstagramDone = 0;
			$photo->fb_idpost_usr = '';
			$photo->fb_source_url = '';
			$photo->approval_step_id = $approvalStepId;
			$photo->tw_idpost_usr = '';

			$photo->save();
			
			self::sendNegativeNotificationPhoto($id);
			Scheduler::UpdateRanking($photo->contest_id);
			
			
			$contest = $photo->contest;
			$user = $photo->user;
			
			if ( ($contest->needsUserRegistration() ) && $sendNotification ) {
			
				try {
				
					Mail::send('emails.photo-approvation', array('contest' => $contest, 'photo' => $photo), function($message) use ($user) {
						
						$message->to($user->email, $user->name . " " . $user->surname)
								->subject(Lang::get('email.object_photo_contest_not_approved'));
					
					});
					
					$photo->emailNotificationDone = 0;
				
				} catch (Exception $ex) {
					Log::error('Problem sending email: '.$ex);
				}

			}
			
			$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);
			
			$facebookProfile = null;
			if ( $photo->user->facebookProfiles->count() > 0 ) {
				$facebookProfile = $photo->user->facebookProfiles->first();
			}
			
			if ( $facebookProfile && $photo->user_connected_to_facebook && $sendNotification ) {
			
				//Facebook desktop notify
				try
				{		
					
					App::setlocale($contest->location->language);
					$message = Lang::get("messages.photo_not_approved");

					//$acc_tokenResponse = $facebook->api("/oauth/access_token?client_id=".$contest->company->fbApp_clientID."&client_secret=".$contest->company->fbApp_clientSecret."&grant_type=client_credentials");
					//$app_access_token = $acc_tokenResponse["access_token"];
					$responseFBAPP = $facebook->post("/".$facebookProfile->id_fb."/notifications", array(
						"template" => $message,
						"href" => "photo/".$photo->id,
					), $contest->company->app_accesstoken );
					Log::debug("Desktop notification done!");
				}
				catch (Exception $ex)
				{
					Log::error('Error during sending notification: '.$ex);
				}
			
			}
			
			$twitterProfile = null;
			if ( $photo->user->twitterProfiles->count() > 0 ) {
				$twitterProfile = $photo->user->twitterProfiles->first();
			}
			
			if ( $twitterProfile && $photo->user_connected_to_twitter && $sendNotification ) {
			
				$twPage = null;
				if ( $photo->contest->twPages->count() > 0 ) {
					$twPage = $photo->contest->twPages->first();
				}
			
				try {
				
					App::setlocale($contest->location->language);
					$message = Lang::get('messages.photo_not_approved');
					
					$parameters = array(
						'user_id' => $twitterProfile->tw_id,
						'text' => $message,
					);
					
					$twitteroauth = new TwitterOAuth('tY2MCEnw0hwTr1ZpYLkHo7Afv', 'dXihbXKGQ6VDYmeoC7GzoFGqjLN99aOZ9heECfTuKIoloJrBEe',  $twPage->tw_page_admin_at, $twPage->tw_page_secret_token );
					$tweet_data = $twitteroauth->post('https://api.twitter.com/1.1/direct_messages/new.json', $parameters);			
				
					Log::debug("Twitter response for notification" . print_r($tweet_data, true));
				
					Log::debug("Twitter notification done!");
				
				} catch (Exception $ex) {
					Log::error('Error during sending twitter direct message: '.$ex);
				}
			
			}
			
			$photo->save();
			
		}		
		else if ($approvalStepId == 3)
		{
			
			$photo->approval_step_id = $approvalStepId;
			$photo->save();
		}
		
		if ( Input::get("api_call") ) {
			return json_encode(array("result"=> "1"));
				
		} else {
			return Redirect::action("Admin_ContestController@contest",array("id" => $photo->contest->id))->with("flash.message","Approval status changed")->with("flash.class","alert-success");

		} 
		
	}
	
	
	public function approveEssay($id)
	{
		Log::debug("id essay element: ".$id);
		$approvalStepId = CommonHelper::mandatoryInput("approvalStep");
		
		$essay = Essay::find($id);
		$contest = $essay->contest;
		
		$sendNotification = false;
		if ( $essay->approval_step_id == 1 &&  $approvalStepId == 2 ) {
			$sendNotification = true;
		} else if ( $essay->approval_step_id == 3 &&  $approvalStepId == 2 ) {
			$sendNotification = true;
		} else if ( $essay->approval_step_id == 3 &&  $approvalStepId == 1 ) {
			$sendNotification = true;
		}
		
		$essay = Essay::with(
				array('user.facebookProfiles' => function($query) use ($contest) {
					$query->where('company_id', '=', $contest->company_id);
				},
				'user.twitterProfiles' => function($query) use ($contest) {
					$query->where('company_id', '=', $contest->company_id);
				},)
			)->where('id', $id)->first();
		
		Log::debug("Changing essay (".$id.") approval step to ".$approvalStepId);
		$approvalStep = ApprovalStep::find($approvalStepId);
		Log::debug("Approval step name: ".$approvalStep->name);
		//$photo->approval_step_id = $approvalStepId;
		//$photo->save();
		/*if ($approvalStepId <> 3)
		{
			GCMHelper::schedule($photo);
		}*/
		if (($approvalStepId == 1) || ($approvalStepId == 2))
		{
			//cancellare il post da facebook, cancellare la riga del post, resettare lo score
			
			
			foreach ($essay->contest->fbPages as $page)					
			{
				$essaysOfPage = $page->essays()->where('fb_page_essay.essay_id', '=', $essay->id)->get();
				
				foreach ($essaysOfPage as $essay)
				{
					
					//richiamare funzione per cancellare post da fb 
					Scheduler::deleteEssayPost($id, $page, $essay->pivot->fb_idessay);
					
					$essay->fbPages()->detach($page->id);
					
					
				}
				
				
			}
			
			foreach ($essay->contest->twPages as $page)	{
			
			
				$essaysOfPage = $page->essays()->where('tw_page_essay.essay_id', '=', $essay->id)->get();
				
				foreach ($essaysOfPage as $essay){
					$essay->twPages()->detach($page->id);
					Scheduler::deletePostTwitter($page, $essay->pivot->tw_idessay);
				}
				
			}
			
			$essay->uploadOnTwitterDone = 0;
			$essay->save();
			
						
			$essay->score = 0.00;
			$essay->rank_position = 0;
			$essay->uploadOnFacebookDone = 0;
			$essay->uploadOnTwitterDone = 0;
			$essay->fb_idpost_usr = '';
			$essay->fb_source_url = '';
			$essay->approval_step_id = $approvalStepId;
			$essay->tw_idpost_usr = '';
			$essay->save();
			
			//self::sendNegativeNotificationPhoto($id);
			Scheduler::UpdateRanking($essay->contest_id);
			
			
			$contest = $essay->contest;
			$user = $essay->user;
			
			if ( ($contest->needsUserRegistration() ) && $sendNotification ) {
			
				try {
				
					Mail::send('emails.essay-approvation', array('contest' => $contest, 'essay' => $essay), function($message) use ($user) {
						
						$message->to($user->email, $user->name . " " . $user->surname)
								->subject(Lang::get('email.object_essay_contest_not_approved'));
					
					});
					
					$essay->emailNotificationDone = 0;
				
				} catch (Exception $ex) {
					Log::error('Problem sending email: '.$ex);
				}

			}
			
			$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);
			
			$facebookProfile = null;
			if ( $essay->user->facebookProfiles->count() > 0 ) {
				$facebookProfile = $essay->user->facebookProfiles->first();
			}
			
			if ( $facebookProfile && $essay->user_connected_to_facebook && $sendNotification ) {
			
				//Facebook desktop notify
				try
				{		
					
					App::setlocale($contest->location->language);
					$message = Lang::get("messages.essay_not_approved");

					//$acc_tokenResponse = $facebook->api("/oauth/access_token?client_id=".$contest->company->fbApp_clientID."&client_secret=".$contest->company->fbApp_clientSecret."&grant_type=client_credentials");
					//$app_access_token = $acc_tokenResponse["access_token"];
					$responseFBAPP = $facebook->post("/".$facebookProfile->id_fb."/notifications", array(
						"template" => $message,
						"href" => "essay/".$essay->id,
					), $contest->company->app_accesstoken );
					Log::debug("Desktop notification done!");
				}
				catch (Exception $ex)
				{
					Log::error('Error during sending notification: '.$ex);
				}
			
			}
			
			$twitterProfile = null;
			if ( $essay->user->twitterProfiles->count() > 0 ) {
				$twitterProfile = $essay->user->twitterProfiles->first();
			}
			
			if ( $twitterProfile && $essay->user_connected_to_twitter && $sendNotification ) {
			
				$twPage = null;
				if ( $essay->contest->twPages->count() > 0 ) {
					$twPage = $essay->contest->twPages->first();
				}
			
				try {
				
					App::setlocale($contest->location->language);
					$message = Lang::get('messages.essay_not_approved');
					
					$parameters = array(
						'user_id' => $twitterProfile->tw_id,
						'text' => $message,
					);
					
					$twitteroauth = new TwitterOAuth('tY2MCEnw0hwTr1ZpYLkHo7Afv', 'dXihbXKGQ6VDYmeoC7GzoFGqjLN99aOZ9heECfTuKIoloJrBEe',  $twPage->tw_page_admin_at, $twPage->tw_page_secret_token );
					$tweet_data = $twitteroauth->post('https://api.twitter.com/1.1/direct_messages/new.json', $parameters);			
				
					Log::debug("Twitter response for notification" . print_r($tweet_data, true));
				
					Log::debug("Twitter notification done!");
				
				} catch (Exception $ex) {
					Log::error('Error during sending twitter direct message: '.$ex);
				}
			
			}
			
			$essay->save();
			
		}		
		else if ($approvalStepId == 3)
		{
			
			$essay->approval_step_id = $approvalStepId;
			$essay->save();
		}
		
		if ( Input::get("api_call") ) {
			return json_encode(array("result"=> "1"));
				
		} else {
			return Redirect::action("Admin_ContestController@contest",array("id" => $essay->contest->id))->with("flash.message","Approval status changed")->with("flash.class","alert-success");

		} 
		
	}
	
	public function finishApproval($id)
	{
		Log::debug("finishApproval(".$id.")");
		$contest = Contest::find($id);
		
		if ($contest->type_id == 1)
		{
			foreach ($contest->videos as $video)
			{
				$video->approval_step_id = 3;
				$video->save();
			}
		}
		else if ($contest->type_id == 2)
		{
			foreach ($contest->photos as $photo)
			{
				
				if ($photo->approval_step_id == '1')
				{
					$photo->approval_step_id = 3;
					$photo->save();
				}
				
				
			}
		}
		
		
		//$contest->contentApprovalDone = 1;
		$contest->save();
		return Redirect::action("Admin_ContestController@contest",array("id" => $id))->with("flash.message","Finished approval")->with("flash.class","alert-success");
	}
	
	public function uploadToInstagram($id) {
	
		Scheduler::uploadPhotoInstagram($id, 1);
	
	}
	
	public function uploadPhotoToInstagram($id) {
	
		
		Scheduler::uploadPhotoInstagram($id, 2);
	}
	
	public function startServerProcess($id)
	{	
	
		//Scheduler::getTrends();
		//Scheduler::updateTrendsTest();
		//Scheduler::updateTrends();
		//Scheduler::updateLocations();
		//Scheduler::checkPromotionsToConfirm();
		
		
		$contest = Contest::find($id);
		
		App::setLocale($contest->location->language);
		
		$tz_object = new DateTimeZone($contest->location->timezoneDesc);
		$datetime = new DateTime();
		$datetime->setTimezone($tz_object);
		$today = $datetime->format("Y-m-d H:i:s");
		
		//calculate score only for active contests
		Log::debug("Actual server time: ".Scheduler::now($contest->location->timezoneDesc));
		
		if (($today >= $contest->score_start_time ) && ($today <= $contest->score_end_time))
		{
			//if contest need watermark insertion, check all the videos of the contest and insert watermark to videos without it
			if ((($contest->should_insert_contest_watermark) || ($contest->should_insert_company_watermark)))
			{
				Scheduler::InsertWatermarkContest($contest->id);
			}
			Scheduler::uploadOnYoutubeBMBPage($contest->id);
			Scheduler::uploadOnFacebookPage($contest->id);
			Scheduler::uploadOnYoutubeCorporatePage($contest->id);
			Scheduler::uploadOnTwitterPage($contest->id);
			Scheduler::emailNotification($contest->id);
			Scheduler::UpdateScorenew($contest->id);
			Scheduler::UpdateRanking($contest->id);
		}
		
		
		/*
		// Changing status, email notification
		$contestStartTime2 = DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time);
		$contestStartTime = DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time);
		$contestStartTime2->add(DateInterval::createFromDateString('15 mins'));
		
		$contestEndTime2 = DateTime::createFromFormat('Y-m-d H:i:s', $contest->end_time);
		$contestEndTime = DateTime::createFromFormat('Y-m-d H:i:s', $contest->end_time);
		$contestEndTime2->add(DateInterval::createFromDateString('15 mins'));
		
		
		if ( $datetime >= $contestStartTime && $datetime <= $contestStartTime2 ) {
			
			$contest->status = 1;
			$user = User::where("company_id", "=", $contest->company_id)->first();
		
			
			try {
			
				Mail::send('emails.contest_start', array(), function($message) use ($user) {
					
					$message->to($user->email, $user->name . " " . $user->surname)
							->subject(Lang::get('email.object_contest_start'));
				
				});
			
			} catch (Exception $ex) {
				Log::error('Problem sending email: '.$ex);
			}
			
			
			
			Auth::login($user);
			try {
				$request = Request::create('/admin/insert-notification/' . $id, 'POST', array(
																		'text' => Lang::get('notifications.contest_start'), 
																	'redirect' => asset("/dasboard/index.html"),));
																	

				Request::replace($request->input());
					
				Route::dispatch($request)->getContent();
			
			} catch (Exception $ex) {
				Log::error('Problem sending notification: '.$ex);
			}
		
		}
		
		if ( $datetime >= $contestEndTime && $datetime <= $contestEndTime2 ) {
			$contest->status = 2;
			$user = User::where("company_id", "=", $contest->company_id)->first();
		
			try {
			
				Mail::send('emails.contest_end', array(), function($message) use ($user) {
					
					$message->to($user->email, $user->name . " " . $user->surname)
							->subject(Lang::get('email.object_contest_end'));
				
				});
			
			} catch (Exception $ex) {
				Log::error('Problem sending email: '.$ex);
			}
			
			Auth::login($user);
			try {
				$request = Request::create('/admin/insert-notification/' . $id, 'POST', array(
																		'text' => Lang::get('notifications.contest_end'), 
																	'redirect' => asset("/dasboard/index.html"),));
																	

				Request::replace($request->input());
					
				Route::dispatch($request)->getContent();
			
			} catch (Exception $ex) {
				Log::error('Problem sending notification: '.$ex);
			}
		
		}
		
		$contest->save();
		/////////////////////////////////////////
		*/
		
		/*
		$facebook = new Facebook(array(
					'appId' => '291972127598452',
					'secret' => 'b5b151f0024cb6f93711084ad892e421'
				));
				
		$facebook->setFileUploadSupport(true);
				
				
		//upload video to facebook page
		$data = array(
			"source" => new CURLFile(public_path()."/videos/65554256b66ecc05db981b2fd3ed083f.mp4"),
			"title" => "test",
			"no_story" => "true",
			"access_token" => "CAAEJjA7zJ3QBAHZAZB97hJW4hw23iC9bEFc09YpibpuduZAZAXYVUhumo6PSE5AYbV2CEgwSXqjgDYhCYhkdjppt8ZBPqK3cBUpa8G5KZCM6vtgwxqKU8BvtmX3pxhQ3ZBXbLY84lojLZARqVdHEY0ZAFMgFx0O9Boosl6H7TXZButf3scfg2ZBCExmZBdFFsszlmuAZD",
			"description" => "sisisisisi");
					

		$response = $facebook->api("/1510417125914114/videos", "POST", $data);
		$id_post = $response['id'];		
		Log::debug("Id post in the Facebook page for the video: ".$response['id']);	
		*/
				
		//return Redirect::action("Admin_ContestController@contest",array("id" => $id))->with("flash.message","Server Process started")->with("flash.class","alert-success");
		
		
	}
	
	public function resetApproval($id)
	{
			
		
		
		Log::debug("resetApproval(".$id.")");
		$contest = Contest::find($id);
		
		if ($contest->type_id == 1)
		{
			foreach ($contest->videos as $video)
			{
				//richiamare funzione per cancellare post da fb 
				if ($video->id_fb_post != 0)
				{
					Scheduler::deleteVideoPost($video->id);
				}
			
				$video->approval_step_id = 1;
				$video->id_fb_post = 0;
				$video->score = 0.00;
				$video->rank_position = 0;
				$video->uploadOnFacebookDone = 0;
				$video->save();
				
			}
		}
		else if ($contest->type_id == 2)
		{
			foreach ($contest->photos as $photo)
			{
				
				try
				{
					
					if (($photo->approval_step_id == '3') && ($photo->uploadOnFacebookDone == '1'))
					{
						
						foreach ($contest->fbPages as $page)					
						{
							$photosOfPage = $page->photos()->where('fb_page_photo.photo_id', '=', $photo->id)->get();
							
							foreach ($photosOfPage as $photoToDelete)
							{
								
								//richiamare funzione per cancellare post da fb 
								Scheduler::deletePhotoPost($photo->id, $page, $photoToDelete->pivot->fb_idphoto);
								
								$photoToDelete->fbPages()->detach($page->id);
								
								
							}
							
							
						}
						
						
					}
								
					$photo->score = 0.00;
					$photo->rank_position = 0;
					$photo->uploadOnFacebookDone = 0;
					$photo->uploadOnTwitterDone = 0;
					$photo->uploadOnInstagramDone = 0;
					$photo->fb_idpost_usr = '';
					$photo->fb_source_url = '';
					$photo->approval_step_id = 1;
					$photo->save();
					
				}
				catch (Exception $ex)
				{
					Log::error("Error during deleting photo with id ".$photo->id." with error: ".$ex);
				}
					
				
				
			}
		}
		
		
		//$contest->contentApprovalDone = 1;
		//$contest->save();
		return Redirect::action("Admin_ContestController@contest",array("id" => $id))->with("flash.message","Approval reset finished")->with("flash.class","alert-success");
	}
	
	public function insertWatermarks($id)
	{
		Log::debug("insertWatermarks(contestId=".$id.")");
		$contest = Contest::find($id);
		foreach ($contest->videos as $video)
		{
			Log::debug("Testing video ".$video->id." if should be scheduled for watermark insertion");
			if ($video->watermarkInsDone == false)
			{
				Scheduler::schedule("insertWatermark;".$video->id);
			}
		}
		return Redirect::action("Admin_ContestController@contest",array("id" => $id))->with("flash.message","Inserting watermarks started")->with("flash.class","alert-success");
	}
	
	public function upload2YouTube($id)
	{
		Log::debug("upload2YouTube(contestId=".$id.")");
		$contest = Contest::find($id);
		foreach ($contest->videos as $video)
		{
			Log::debug("Testing video ".$video->id." if should be scheduled for YouTube upload");
			if (($video->uploadOnCompanyYouTubeDone == 0) || ($video->uploadOnOwnYouTubeDone == 0))
			{
				Scheduler::schedule("upload2YouTube;".$video->id);
				
				$approvalStepId = $video->approval_step_id;
				$approvalStep = ApprovalStep::find($approvalStepId);
				Log::debug("Approval step name: ".$approvalStep->name);
				if ($approvalStepId == 3)
				{
					GCMHelper::schedule($video);
				}
			}
		}
		return Redirect::action("Admin_ContestController@contest",array("id" => $id))->with("flash.message","Upload to YouTube started")->with("flash.class","alert-success");
	}
	
	public function uploadToInstagram_old($id)
	{
		Log::debug("uploadToInstagram(contestId=".$id.")");
		$contest = Contest::find($id);
		foreach ($contest->photosAlreadyApproved as $photo)
		{
			Log::debug("Testing photo ".$photo->id." if should be scheduled for Instagram upload");
			if ($photo->uploadOnInstagramDone == 0)
			{
				Scheduler::schedule("uploadToInstagram;".$photo->id);
				//Scheduler::uploadPhotoInstagram($photo);
			}
			
		}
		
		
		return Redirect::action("Admin_ContestController@contest",array("id" => $id))->with("flash.message","Upload to Instagram started")->with("flash.class","alert-success");
	}
	
	public function sendNotification($id) 
	{
		
		$video = Video::find($id);
		
		$user = User::find($video->user_id);
		$contest = Contest::find($video->contest_id);
		$facebookProfile = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
		$fbUserID = $facebookProfile->id_fb;
		
		
		//Facebook desktop notify
		try
		{		
		
			$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);

			//$acc_tokenResponse = $facebook->api("/oauth/access_token?client_id=".$contest->company->fbApp_clientID."&client_secret=".$contest->company->fbApp_clientSecret."&grant_type=client_credentials");
			//$app_access_token = $acc_tokenResponse["access_token"];
			$responseFBAPP = $facebook->post("/".$fbUserID."/notifications", array(
				"template" => Input::get("message"),
				"href" => "video/".$video->id,
			), $contest->company->app_accesstoken );
			Log::debug("Desktop notification done!");
			
			
		}
		catch (Exception $ex)
		{
			Log::error('Error during sending notification: '.$ex);
		}
		
		$app = null;
		$deviceToken = null;
		$message = null;
		
		$company_user = Company::find($contest->company_id)->belongsToManyUsers()->where('user_id', $user->id)->first();
		
		if (isset($company_user->pivot->token) && strpos($company_user->pivot->token,'Android_') !== false)
		{
			
			App::setlocale($video->contest->location->language);
			$nativeNotifString = Input::get("message");
			
			$message = PushNotification::Message($nativeNotifString,array(
				'title' => $video->contest->name,
				'url' => 'https://www.buzzmybrand.it/video/'.$video->id));
			
			$deviceToken = str_replace("Android_", "", $$company_user->pivot->token);
			Log::debug("Device token to be used: ".$deviceToken);
			$app = $video->contest->company->name.'Android';
			
			
		}
		else if (isset($company_user->pivot->token) && strpos($company_user->pivot->token,'IOS_') !== false)
		{
		
		
			App::setlocale($video->contest->location->language);
			$nativeNotifString = Input::get("message");
			$message = PushNotification::Message($nativeNotifString, array('custom' => array('url' => 'https://www.buzzmybrand.it/video/'.$video->id)));
			
			$deviceToken = str_replace("IOS_", "", $$company_user->pivot->token);
			Log::debug("Device token to be used: ".$deviceToken);
			$app = $video->contest->company->name;
			
			
		}
				
		if ( (isset($company_user->pivot->token) && $company_user->pivot->token != '' && $company_user->pivot->token != null) )
		{
			PushNotification::app($app)
				->to($deviceToken)
				->send($message);
				
			
		}
		
		return Redirect::action("Admin_ContestController@contest",array("id" => $video->contest->id))->with("flash.message","Notification sended")->with("flash.class","alert-success");
	
	}
	
	
	public function sendNotificationPhoto($id)
	{
		$photo = Photo::find($id);
		
		$user = User::find($photo->user_id);
		$contest = Contest::find($photo->contest_id);		
		$facebookProfile = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
		$fbUserID = $facebookProfile->id_fb;
		
		//Facebook desktop notify
		try
		{		
		
			$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);

			//$acc_tokenResponse = $facebook->api("/oauth/access_token?client_id=".$contest->company->fbApp_clientID."&client_secret=".$contest->company->fbApp_clientSecret."&grant_type=client_credentials");
			//$app_access_token = $acc_tokenResponse["access_token"];
			$responseFBAPP = $facebook->post("/".$fbUserID."/notifications", array(
				"template" => Input::get("message"),
				"href" => "photo/".$photo->id,
			), $contest->company->app_accesstoken );
			Log::debug("Desktop notification done!");
			
			
		}
		catch (Exception $ex)
		{
			Log::error('Error during sending notification: '.$ex);
		}
		
		$company_user = Company::find($contest->company_id)->belongsToManyUsers()->where('user_id', $user->id)->first();
		
		if (isset($company_user->pivot->token) && strpos($company_user->pivot->token,'Android_') !== false)
		{
			$message = PushNotification::Message(Input::get("message"),
				array(
					'title' => $photo->contest->name,
					'url' => 'http://www.buzzmybrand.it/photo/'.$photo->id));
			$app = '';
			$deviceToken = str_replace("Android_", "", $company_user->pivot->token);
			Log::debug("Device token to be used: ".$deviceToken);
			$app = $photo->contest->company->name.'Android';
		}
		else if (isset($company_user->pivot->token) && strpos($company_user->pivot->token,'IOS_') !== false)
		{
			$message = PushNotification::Message(Input::get("message"), 
				array('custom' => array('url' => 'https://www.buzzmybrand.it/photo/'.$photo->id)));
			$app = '';
			$deviceToken = str_replace("IOS_", "", $company_user->pivot->token);
			Log::debug("Device token to be used: ".$deviceToken);
			$app = $photo->contest->company->name;

			
		}
		
		if ( (isset($company_user->pivot->token) && $company_user->pivot->token != '' && $company_user->pivot->token != null) )
		{
			PushNotification::app($app)
				->to($deviceToken)
				->send($message);
				
			
		}
		
		return Redirect::action("Admin_ContestController@contest",array("id" => $photo->contest->id))->with("flash.message","Notification sended")->with("flash.class","alert-success");
		
	}
	
	
	
	public function sendMassNotification($contestId) {
		
		Log::debug("Send Mass Notification");
	
		
			$type_logged = Input::get('mass_type_notification_logged');
			$type_applyer = Input::get('mass_type_notification_applyer');
			$message = Input::get("message");
			$contestType = Input::get("contestType");
			$loggedUsers = null;
			$usersApplyers = null;
			
			$returnMessage = '';
			if ($type_logged == '' && $type_applyer == '') {
				$returnMessage = trans('messages.Type notification missing');
				return Redirect::to('/admin/contest/' . $contestId)->with("flash.message", $returnMessage)->with("flash.class","alert-warning");;
			}
			
			$users = null;
			
			if ( $type_logged == 'notify' ) {
				
				$contest = Contest::with(
								array('users' => function($query) use ($contestId, $contestType) {
								
									if ( $contestType == '1' ) {
										$query->whereNotIn('users.id', Video::where('contest_id', $contestId)->lists('user_id'))->distinct();
									} else if ( $contestType == '2' ) {
										$query->whereNotIn('users.id', Photo::where('contest_id', $contestId)->lists('user_id'))->distinct();
									}
									
								})
							)->where('id', $contestId)->first();
					
				$loggedUsers = $contest->users;
				
		
				$queries = DB::getQueryLog();
				Log::debug("Queries: ".print_r($queries,true));
			
			} 
			
			if ( $type_applyer == 'notify' ) {
			
				$contest = Contest::with(
								array('users' => function($query) use ($contestId, $contestType) {
								
									if ( $contestType == '1' ) {
										$query->whereIn('users.id', Video::where('contest_id', $contestId)->lists('user_id'))->distinct();
									} else if ( $contestType == '2' ) {
										$query->whereIn('users.id', Photo::where('contest_id', $contestId)->lists('user_id'))->distinct();
									}
									
								})
							)->where('id', $contestId)->first();
					
				$usersApplyers = $contest->users;
				
				$queries = DB::getQueryLog();
				Log::debug("Queries: ".print_r($queries,true));
			
			}
			
			$loggedUsersArray = array();
			$usersApplyersArray = array();
			
			if ( $loggedUsers != null) {
				$loggedUsersArray = $loggedUsers->toArray();
			}
			
			if ( $usersApplyers != null) {
				$usersApplyersArray = $usersApplyers->toArray();
			}
			
			$users = array_merge($loggedUsersArray, $usersApplyersArray);

			
			$successFacebookNotification = 0;
			$errorFacebookNotification = 0;
			
			$successMobileNotification = 0;
			$errorMobileNotification = 0;
			
			$contestRoute = "https://www.buzzmybrand.it/tab/" . $contest->id;
			if ( $contest->contest_route ) {
				$contestRoute = "https://app.buzzmybrand.it/" . $contest->contest_route;
			}
			
			foreach ( $users as $user ) {
			
				
				$facebookProfile = FacebookProfile::where('user_id', $user['id'])->where('company_id', $contest->company_id)->first();
				
				if ( $facebookProfile ) {
					
					try
					{		

						$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
						]);
						$responseFBAPP = $facebook->post("/" . $facebookProfile->id_fb . "/notifications", array(
							"template" => $message,
							"href" => $contestRoute,
						), $contest->company->app_accesstoken );
						
						Log::debug("Desktop notification done! User: " . $user['email']);
						$successFacebookNotification++;
						
					}
					catch (Exception $ex)
					{
						Log::error('Error during sending notification User: '. $user['email'] . ' ex ' .$ex);
						$errorFacebookNotification++;
						
					}
				
				}
				
				
				$company_user = Company::find($contest->company_id)->belongsToManyUsers()->where('user_id', $user['id'])->first();

				if ( $company_user) {
					
					if (strpos($company_user->pivot->token, 'Android_') !== false)
					{
						$messageToPush = PushNotification::Message($message,
											array(
												'title' => $contest->name,
												'url' => $contestRoute
											)
										);

						$deviceToken = str_replace("Android_", "", $company_user->pivot->token);
						$app = $contest->company->name.'Android';
					}
					else if (strpos($company_user->pivot->token, 'IOS_') !== false)
					{
						$messageToPush = PushNotification::Message($message, 
											array(
												'custom' => array('url' => $contestRoute)
											)
										);

						$deviceToken = str_replace("IOS_", "", $company_user->pivot->token);
						$app = $contest->company->name.'IOS';

					}
					
					if ( ($company_user->pivot->token != '' && $company_user->pivot->token != null) )
					{
						PushNotification::app($app)
							->to($deviceToken)
							->send($messageToPush);
						
						Log::debug("Mobile notification done! User: " . $user['email']);
						$successMobileNotification++;
						
					} else {
					
						Log::debug("Mobile notification failed! User: " . $user['email']);
						$errorMobileNotification++;
					
					}
					
				
				} 
				
				
			
			}
			
			$returnMessage = trans('messages.Notifications statistics', array(
																			'successFacebookNotification' => $successFacebookNotification,
																			'errorFacebookNotification' => $errorFacebookNotification,
																			'successMobileNotification' => $successMobileNotification,
																			'errorMobileNotification' => $errorMobileNotification,
																		)
									);
			
			return Redirect::to('/admin/contest/' . $contestId)->with("flash.message", $returnMessage)->with("flash.class","alert-warning");;
	

	
	}
	
	private function getStatistics($contestId) {
	
		$contest = Contest::find($contestId);
		$statistics = array();
	
		if ($contest->type_id == 1) {
				
			foreach ($contest->videos as $video) {
				
				// START Aggregazione statistiche per video //
				$statistics[$video->id] = array(
												'unique_likes' => 0, 
												'unique_comments' => 0, 
												'unique_shares' => 0, 
												'unique_views' => 0,
												'reach' => 0,
												);
				
									
				foreach ($video->fbPages as $vp) {
					
					$statistics[$video->id]['unique_likes'] += $vp->pivot->unique_likes;
					$statistics[$video->id]['unique_comments'] += $vp->pivot->unique_comments;
					$statistics[$video->id]['unique_shares'] += $vp->pivot->unique_shares;
					$statistics[$video->id]['unique_views'] += $vp->pivot->unique_views;
					$statistics[$video->id]['reach'] += $vp->pivot->reach;
				
				}
				// END Aggregazione statistiche per video //
				
			}
				
		} else if ($contest->type_id == 2) {

			foreach ($contest->photos as $photo) { 
				
				// START Aggregazione statistiche per video //
				$statistics[$photo->id] = array(
												'unique_likes' => 0, 
												'unique_comments' => 0, 
												'unique_shares' => 0, 
												'unique_views' => 0,
												'reach' => 0,
												);
				
									
				foreach ($photo->fbPages as $fp) {
					
					$statistics[$photo->id]['unique_likes'] += $fp->pivot->unique_likes;
					$statistics[$photo->id]['unique_comments'] += $fp->pivot->unique_comments;
					$statistics[$photo->id]['unique_shares'] += $fp->pivot->unique_shares;
					$statistics[$photo->id]['reach'] += $fp->pivot->reach;
				
				}
				// END Aggregazione statistiche per video //
			}
			
		}
		return $statistics;			
					
	}
	
	public function getAllVideos() {
	
		$contest_id = Input::get('contest_id');
		$cols = Input::get('cols');
		$page = Input::get('page');
		$statistics = $this->getStatistics($contest_id);
		$contest = Contest::find($contest_id);
		$videos = Video::with(
						array('user.facebookProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},
						'user.twitterProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						}
						)
					)->where('contest_id', '=', $contest_id)->orderBy('approval_step_id','asc')->orderBy('rank_position','asc')->paginate($cols);
		return View::make('admin.ajax-all-videos')->with('videos', $videos)->with('cols', $cols)->with('page', $page)->with('statistics', $statistics);
		
	}
	
	public function getAllPhotos() {
	
		$contest_id = Input::get('contest_id');
		$cols = Input::get('cols');
		$page = Input::get('page');
		$statistics = $this->getStatistics($contest_id);
		$contest = Contest::find($contest_id);
		$photos = Photo::with(
						array('user.facebookProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},
						'user.twitterProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						})
					)->where('contest_id', '=', $contest_id)->orderBy('approval_step_id','asc')->orderBy('rank_position','asc')->paginate($cols);
		
		return View::make('admin.ajax-all-photos')->with('photos', $photos)->with('cols', $cols)->with('page', $page)->with('statistics', $statistics);
		
	}
	
	public function getNotApprovedVideos() {
	
		$contest_id = Input::get('contest_id');
		$cols = Input::get('cols');
		$page = Input::get('page');
		$contest = Contest::find($contest_id);
		$videos = Video::with(
						array('user.facebookProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},
						'user.twitterProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						})
					)->where('contest_id', '=', $contest_id)->where('approval_step_id', '2')->orderBy('created_at','desc')->paginate($cols);
		
		return View::make('admin.ajax-not-approved-videos')->with('videos', $videos)->with('cols', $cols)->with('page', $page);
	
	}
	
	public function getNotApprovedPhotos() {
	
		$contest_id = Input::get('contest_id');
		$cols = Input::get('cols');
		$page = Input::get('page');
		$contest = Contest::find($contest_id);
		$photos = Photo::with(
						array('user.facebookProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},
						'user.twitterProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						})
					)->where('contest_id', '=', $contest_id)->where('approval_step_id', '2')->orderBy('created_at','desc')->paginate($cols);
		return View::make('admin.ajax-not-approved-photos')->with('photos', $photos)->with('cols', $cols)->with('page', $page);
		
	}
	
	public function getApprovedVideos() {
	
		$contest_id = Input::get('contest_id');
		$cols = Input::get('cols');
		$page = Input::get('page');
		$statistics = $this->getStatistics($contest_id);
		$contest = Contest::find($contest_id);
		$videos = Video::with(
						array('user.facebookProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},
						'user.twitterProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						})
					)->where('contest_id', '=', $contest_id)->where('approval_step_id', '3')->orderBy('rank_position', 'asc')->paginate($cols);
		return View::make('admin.ajax-approved-videos')->with('videos', $videos)->with('cols', $cols)->with('page', $page)->with('statistics', $statistics);
	
	}
	
	public function getApprovedPhotos() {
	
		$contest_id = Input::get('contest_id');
		$cols = Input::get('cols');
		$page = Input::get('page');
		$statistics = $this->getStatistics($contest_id);
		$contest = Contest::find($contest_id);
		$photos = Photo::with(
						array('user.facebookProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},
						'user.twitterProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						})
					)->where('contest_id', '=', $contest_id)->where('approval_step_id', '3')->orderBy('rank_position', 'asc')->paginate($cols);
		return View::make('admin.ajax-approved-photos')->with('photos', $photos)->with('cols', $cols)->with('page', $page)->with('statistics', $statistics);
		
	}
	
	public function getToApproveVideos() {
	
		$contest_id = Input::get('contest_id');
		$cols = Input::get('cols');
		$page = Input::get('page');
		$contest = Contest::find($contest_id);
		$videos = Video::with(
						array('user.facebookProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},
						'user.twitterProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						})
					)->where('contest_id', '=', $contest_id)->where('approval_step_id', '1')->orderBy('created_at','desc')->paginate($cols);
		return View::make('admin.ajax-to-approve-videos')->with('videos', $videos)->with('cols', $cols)->with('page', $page);
	
	}
	
	public function getToApprovePhotos() {
	
		$contest_id = Input::get('contest_id');
		$cols = Input::get('cols');
		$page = Input::get('page');
		$contest = Contest::find($contest_id);
		$photos = Photo::with(
						array('user.facebookProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},
						'user.twitterProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						})
					)->where('contest_id', '=', $contest_id)->where('approval_step_id', '1')->orderBy('created_at','desc')->paginate($cols);
	/*		
foreach ($photos as $photo) {			
		print_r($photo->user->facebookProfiles->count()); 
			}		return;
			*/
		return View::make('admin.ajax-to-approve-photos')->with('photos', $photos)->with('cols', $cols)->with('page', $page);
		
	}
	
	public function deleteVideo($video_id) {
	
		$video = Video::find($video_id);
			
		foreach ($video->contest->fbPages as $page)	{
		
		
			$videosOfPage = $page->videos()->where('fb_page_video.video_id', '=', $video->id)->get();
			
			foreach ($videosOfPage as $video){
				$video->fbPages()->detach($page->id);
				Scheduler::deleteVideoPost($video_id, $page, $video->pivot->fb_idpost);
			}
			
		}
		
		foreach ($video->contest->twPages as $page)	{
		
		
			$videosOfPage = $page->videos()->where('tw_page_video.video_id', '=', $video->id)->get();
			
			foreach ($videosOfPage as $video){
				$video->twPages()->detach($page->id);
				Scheduler::deletePostTwitter($page, $video->pivot->tw_idvideo);
			}
			
		}
		
		$video->uploadOnTwitterDone = 0;
		$video->save();
		
		
		foreach ($video->contest->ytPages as $page)	{
		
		
			$videosOfPage = $page->videos()->where('yt_page_video.video_id', '=', $video->id)->get();
			
			foreach ($videosOfPage as $video){
				$video->ytPages()->detach($page->id);
				YouTube::deleteVideo($page, $video->pivot->playlist_item, $video->pivot->yt_idvideo);
			}
			
		}
		
		$video->uploadOnCompanyYouTubeDone  = 0;
		$video->save();
		
		
		$page = ytPage::find(1);
		if ( $video->youtube_id ) {
			YouTube::deleteVideo($page, $video->playlist_item, $video->youtube_id );
		}
		$video->uploadOnOwnYouTubeDone = 0;
		$video->save();
		
		
		$video->deleted_at = new DateTime('now');
						
		$video->score = 0.00;
		$video->rank_position = 0;
		$video->uploadOnFacebookDone = 0;
		$video->uploadOnOwnYouTubeDone = 0;
		$video->uploadOnCompanyYouTubeDone = 0;
		$video->fb_idpost_usr = '';
		$video->fb_source_url = '';
		$video->approval_step_id = 1;
		$video->tw_idpost_usr = '';
		$video->youtube_id  = '';
		$video->playlist_item = ''; 
		$video->emailNotificationDone = 0;
		$video->total_count_link = 0;
		
		Scheduler::UpdateRanking($video->contest_id);
		
		$video->save();
		
		
			
		$message = trans("messages.Delete Done");
		
		if ( Input::get("api_call") ) {
			return json_encode(array("result"=> "1"));	
		} else {
			return Redirect::action("Admin_ContestController@contest",array("id" => $video->contest->id))->with("flash.message", $message)->with("flash.class","alert-success");
		}
	
	}
	
	public function deletePhoto($photo_id) {
	
		$photo = Photo::find($photo_id);			
			
		foreach ($photo->contest->fbPages as $page)	{
			
			$photosOfPage = $page->photos()->where('fb_page_photo.photo_id', '=', $photo->id)->get();
			
			foreach ($photosOfPage as $photo) {
				
				Scheduler::deletePhotoPost($photo_id, $page, $photo->pivot->fb_idphoto);
				$photo->fbPages()->detach($page->id);
				
			}
			
			
		}
		
		foreach ($photo->contest->twPages as $page)	{
		
		
			$photosOfPage = $page->photos()->where('tw_page_photo.photo_id', '=', $photo->id)->get();
			
			foreach ($photosOfPage as $photo){
				$photo->twPages()->detach($page->id);
				Scheduler::deletePostTwitter($page, $photo->pivot->tw_idphoto);
			}
			
		}
		
		$photo->uploadOnTwitterDone = 0;
		$photo->save();
		
			
		$photo->deleted_at = new DateTime('now');
						
		$photo->score = 0.00;
		$photo->rank_position = 0;
		$photo->uploadOnFacebookDone = 0;
		$photo->uploadOnTwitterDone = 0;
		$photo->uploadOnInstagramDone = 0;
		$photo->fb_idpost_usr = '';
		$photo->fb_source_url = '';
		$photo->approval_step_id = 1;
		$photo->tw_idpost_usr = '';

		$photo->emailNotificationDone = 0;
		
		
		Scheduler::UpdateRanking($photo->contest_id);
		
		$photo->save();
		
		
		$message = trans("messages.Delete Done");
		if ( Input::get("api_call") ) {
			return json_encode(array("result"=> "1"));	
		} else {
			return Redirect::action("Admin_ContestController@contest",array("id" => $photo->contest->id))->with("flash.message", $message)->with("flash.class","alert-success");
		}
	}
	
	
	public function deleteEssay($essay_id) {
	
		
		$essay = Essay::find($essay_id);			
		
		foreach ($essay->contest->fbPages as $page)	{
			
			$essaysOfPage = $page->essays()->where('fb_page_essay.essay_id', '=', $essay->id)->get();
			
			foreach ($essaysOfPage as $essay) {
				
				Scheduler::deleteEssayPost($essay_id, $page, $essay->pivot->fb_idessay);
				$essay->fbPages()->detach($page->id);
				
			}
			
			
		}
		
		
		foreach ($essay->contest->twPages as $page)	{
		
		
			$essaysOfPage = $page->essays()->where('tw_page_essay.essay_id', '=', $essay->id)->get();
			
			foreach ($essaysOfPage as $essay){
				
				Scheduler::deletePostTwitter($page, $essay->pivot->tw_idessay);
				$essay->twPages()->detach($page->id);
			}
			
		}
		
		$essay->uploadOnTwitterDone = 0;
		$essay->save();
		
			
		$essay->deleted_at = new DateTime('now');
						
		$essay->score = 0.00;
		$essay->rank_position = 0;
		$essay->uploadOnFacebookDone = 0;
		$essay->uploadOnTwitterDone = 0;
		$essay->fb_idpost_usr = '';
		$essay->fb_source_url = '';
		$essay->approval_step_id = 1;
		$essay->emailNotificationDone = 0;
		$essay->tw_idpost_usr = '';
		
		
		Scheduler::UpdateRanking($essay->contest_id);
		
		$essay->save();
		
		
		$message = trans("messages.Delete Done");
		if ( Input::get("api_call") ) {
			return json_encode(array("result"=> "1"));	
		} else {
			return Redirect::action("Admin_ContestController@contest",array("id" => $essay->contest->id))->with("flash.message", $message)->with("flash.class","alert-success");
		}
	}
	
	
	public function newContest() {
	

			$contests = Contest::select("contests.*")->join("companys","companys.id","=","contests.company_id")->get();
			$companys = Company::select("companys.*")->join("contests","contests.company_id","=","companys.id")->distinct()->get(array('companys.*'));
			$locations = Location::select("locations.*")->get();
			return View::make("admin/new-contest")->with("contests",$contests)->with("companys",$companys)->with("locations", $locations);


	}
	
	public function contestList($id) {
	
		$query = Input::get("search");
		$status = Input::get("status");
	
		$queryBuilder = DB::table('companys')
						->join("contests", "contests.company_id", "=", "companys.id")
						->leftJoin('videos', function($join)
							{
								$join->on('videos.contest_id', '=', 'contests.id')
									 ->where('videos.approval_step_id', '=', 1);
									 
							})
						->leftJoin('photos', function($join)
							{
								$join->on('photos.contest_id', '=', 'contests.id')
									 ->where('photos.approval_step_id', '=', 1);
							})
						->where("companys.id", $id)
						->where('contests.name', 'like', '%' . $query . '%')
						->whereNull('videos.deleted_at')
						->whereNull('photos.deleted_at');
		if ( $status ) {
			$queryBuilder = $queryBuilder->where("contests.status", $status);
		}
			
		$queryBuilder = $queryBuilder->groupBy('companys.id', 'contests.id');
		
		
		$queryBuilder->select(DB::raw('
			companys.id as company_id,
			companys.name as company_name, 
			contests.name as contest_name, 
			contests.id as contes_id,
			contests.type_id as contest_type,
			contests.score_start_time as start_date, 
			contests.score_end_time as end_date,
			contests.status as contest_status,
			count(videos.approval_step_id) as video_to_approve,
			count(photos.approval_step_id) as photo_to_approve
			'));
		
		$result = $queryBuilder->get();
		$queries = DB::getQueryLog();
        Log::debug("contestList queries: ".print_r($queries,true));
		
		return json_encode($result);
	
	}

	
	public function getEssay($id)
	{
		
		$essay = Essay::find($id);
		$contest = $essay->contest;
		
		$essay = Essay::with(
						array('user.facebookProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},
						'user.twitterProfiles' => function($query) use ($contest) {
							$query->where('company_id', '=', $contest->company_id);
						},)
					)->where('id', $id)->first();
		
		return View::make('essay')->with('essay', $essay);
		
		
	}
	
}
