<?php

set_include_path(get_include_path().PATH_SEPARATOR.app_path().'/classes');
require_once 'Google/Client.php';
require_once 'Google/Service/Books.php';

use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Guzzle\Http\Message\Response;
class Admin_DashboardController extends BaseController {	


	function __construct() 
	{
		$this->beforeFilter('auth_company', array('except' => array('newCompany', 'getTermsIt', 'getTermsEn', 'verifyEmail', 'resendPassword', 'getTec', 'checkAvEmail', 'phpInfo')));
	}
	
	
	// CHECKs FOR ACCESS
	private function checkAccessForCompany($companyId) {
	//$companyId = intval($companyId);
		if ( !preg_match("/\d/", $companyId) ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		} 
	
		if ( !Auth::user()->isSuperAdmin() && $companyId !== (string)Auth::user()->company_id ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
	}
	
	private function checkAccessContestList($companyId) {
	//$companyId = intval($companyId);
		if ( !preg_match("/\d/", $companyId) ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		} 
	
		if ( !Auth::user()->isSuperAdmin() && $companyId !== (string)Auth::user()->company_id ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
	}
	
	private function checkAccessContest($contestId) {
	
		Log::debug("contest in questione: ".$contestId );
	
		if ( !preg_match("/\d/", $contestId) ) {
			Log::debug("Errore matching in contestId");
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		} 
		
		
		
		$contestForCheck = Contest::find($contestId); 
		
		//Log::debug("Risultato contest: ".print_r($contestForCheck,true));
		
		if ( !$contestForCheck ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
		
		if ($contestForCheck->company_id !== (string)Auth::user()->company_id)
		{
			Log::debug("Different");
		}
		else
		{
			Log::debug("Equal");
		}
		if ( !Auth::user()->isSuperAdmin() && $contestForCheck->company_id !== (string)Auth::user()->company_id ) {
			Log::debug("Errore");
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		} 
	
	}
	
	private function checkAccesVideo($videoId) {
			
		if ( !preg_match("/\d/", $videoId) ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
			
		$entryForCheck = Video::find($videoId);
		
		if ( !$entryForCheck ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
		
		$contestForCheck = Contest::find($entryForCheck->contest_id); 
		if ( !Auth::user()->isSuperAdmin() && $contestForCheck->company_id !== Auth::user()->company_id ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		} 
	}
	
	private function checkAccesPhoto($photoId) {
	
		if ( !preg_match("/\d/", $photoId) ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
			
		$entryForCheck = Photo::find($photoId);
		
		if ( !$entryForCheck ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
		
		$contestForCheck = Contest::find($entryForCheck->contest_id); 
		if ( !Auth::user()->isSuperAdmin() && $contestForCheck->company_id !== Auth::user()->company_id ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		} 
		
	}
	
	private function checkAccesEssay($essayId) {
	
		if ( !preg_match("/\d/", $essayId) ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
			
		$entryForCheck = Essay::find($essayId);
		
		if ( !$entryForCheck ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
		
		$contestForCheck = Contest::find($entryForCheck->contest_id); 
		if ( !Auth::user()->isSuperAdmin() && $contestForCheck->company_id !== Auth::user()->company_id ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}  
	
	}
	
	private function checkAccesVideoOrPhotoOrEssay($entryId, $contestType) {
	
		if ( !preg_match("/\d/", $entryId) ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
		
		if ( $contestType == "1" ) {
			$entryForCheck = Video::find($entryId);
		} else if ( $contestType == "2" ) { 
			$entryForCheck = Photo::find($entryId);
		} else if ( $contestType == "3" ) {
			$entryForCheck = Essay::find($entryId);
		}
		
		if ( !$entryForCheck ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
		
		$contestForCheck = Contest::find($entryForCheck->contest_id);
		if ( !Auth::user()->isSuperAdmin() && $contestForCheck->company_id !== Auth::user()->company_id ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		} 

	
	}
	
	private function checkAccessUser($userId) {
	
		if ( !preg_match("/\d/", $userId) ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
		
		$userForCheck = User::find($userId); 
		
		if ( !$userForCheck ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
		
		if ( !Auth::user()->isSuperAdmin() && $userForCheck->company_id !== Auth::user()->company_id ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		} 
	
	}
	
	private function checkAccessNotification($notificationId) {
		
		if ( !preg_match("/\d/", $notificationId) ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
			
		$notificationForCheck = Notification::find($notificationId); 
		
		if ( !$notificationForCheck ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}
		
		$contestForCheck = Contest::find($notificationForCheck->contest_id); 
		if ( !Auth::user()->isSuperAdmin() && $contestForCheck->company_id !== Auth::user()->company_id ) {
			return json_encode(array("result"=> "0", "message" => "You have not access permission"));
		}  
		
	
	}
	
	private function checkForContestApprovation() {
	
		
	}
	// END CHECKs FOR ACCESS
	
	
	
	
	
	
	
	public function phpInfo() {
		phpinfo();
		
	}
	

	public function contestList($id = null) {

        $orderStatus = [1, 3, 4, 2];
		
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContestList($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		
		$query = Input::get("search");
		$status = Input::get("status");
	
		$queryBuilder = DB::table('companys')
                        ->join("contests", "contests.company_id", "=", "companys.id")
						->leftJoin("templates", "contests.template_id", "=", "templates.id")
						->whereRaw('(contests.name like \'%' . $query . '%\' or companys.name like \'%' . $query . '%\')')
						->whereNull('contests.deleted_at');
					
		if ($id == 225) //if user is admin
		{			
			$querybuilder = $queryBuilder->where("contests.company_name", "Buzzmybrand")->orwhere("companys.id", $id)->whereNull('contests.deleted_at')->orderBy('contests.status', 'desc');
		} else if ( $id ) {
			$queryBuilder = $queryBuilder->where("companys.id", $id);
		}
					
					
		if (( $status ) && ( $id )) {
			$queryBuilder = $queryBuilder->where("contests.status", $status);
		}
		/*
		if (( $status ) && ( !$id )) {//if user is admin
			$queryBuilder = $queryBuilder->where("contests.status", $status);
		}
		*/	
		$queryBuilder = $queryBuilder->groupBy('companys.id', 'contests.id');
		
		
		$queryBuilder->select(DB::raw('
			companys.id as company_id,
			companys.name as company_name, 
			companys.fbApp_clientID as fbApp_id,
			contests.name as contest_name, 
            contests.id as contest_id,
			contests.confirmed_by_customer as confirmed_by_customer,
			contests.currency as contest_currency,
            contests.location_id as location_id,
			contests.contest_approval as contest_approval,
			contests.service_type as service_type,
            contests.hashtag as contest_hashtag,
            contests.template_id as template_id,
            contests.sponsor_logo as contest_watermark,
            contests.type_id as contest_type,
            contests.concept as contest_cta,
            contests.how_to_win as contest_how_to_win,
			templates.contest_flyer as template_image,
			DATE_FORMAT(contests.start_time, \'%Y-%m-%dT%TZ\') as start_date, 
			DATE_FORMAT(contests.end_time, \'%Y-%m-%dT%TZ\') as end_date,
			contests.status as contest_status,
			contests.contest_route as contest_route,
			contests.image as contest_image,
			contests.use_template as use_template,
            contests.template_id as contest_template_id,
			contests.image_minisite_sq as contest_image_minisite_sq,
			contests.image_minisite_bg as contest_image_minisite_bg,
			contests.promotion_budget as contest_promotion
			'));
		
        $result = $queryBuilder->get();

		$queries = DB::getQueryLog();
		Log::debug("contest queries: ".print_r($queries,true));
        
        foreach ($result as $r) {

            $contest = Contest::find($r->contest_id);
            // Social
            $facebook_data = [];
            $twitter_data = [];
            $instagram_data = [];
            $youtube_data = [];
            if (count($contest->fbPages) > 0) {
                $facebook_data = $contest->fbPages->toArray();
            }
            if (count($contest->twPages) > 0) {
                $twitter_data = $contest->twPages->toArray();
            }
            if (count($contest->ytPages) > 0) {
                $youtube_data = $contest->ytPages->toArray();
            }
            if (!is_null($contest->instagram)) {
                $instagram_data = $contest->instagram;
            }
            $r->facebook_data = $facebook_data;
            $r->twitter_data = $twitter_data;
            $r->youtube_data = $youtube_data;
            $r->instagram_data = $instagram_data;

            // Objective
            $objectivesArray = array();
            foreach ( $contest->objectives->toArray() as $o ) {
                array_push( $objectivesArray, $o['id'] );
            }
            $r->contest_objectives = $objectivesArray;
            
            // Reward
            $rewardsArray = array();
            foreach ( $contest->rewards->toArray() as $reward ) {
                array_push( $rewardsArray, array("rank" => $reward['rank'], "title" => $reward['title'], "description" => $reward['description']) );
            }
            $r->rewards = $rewardsArray;

            
            // Service type
            $r->contest_service_type = $contest->service_type == 1 ? 'BUZZ PACKAGE' : 'SUPERBUZZ PACKAGE';

            // Template
			if ($contest->use_template == 1)
				$r->template = $contest->template['name'];
			else
				$r->template = "CUSTOM TEMPLATE";
		
			if ( $r->contest_type == 1 ) {
				$toApprove = DB::table('videos')
					->select(DB::raw('
						count(*) as videos_to_approve
						'))
					->where('videos.approval_step_id', '=', 1)
					->where('videos.contest_id', '=', $r->contest_id)
					->whereNull('videos.deleted_at')
					->groupBy('videos.contest_id')->first();
					
				if( count($toApprove) > 0 ) {
					$r->videos_to_approve = $toApprove->videos_to_approve;
				}
					
			} else if ( $r->contest_type == 2 ) {
				$toApprove = DB::table('photos')
					->select(DB::raw('
						count(*) as photos_to_approve
						'))
					->where('photos.approval_step_id', '=', 1)
					->where('photos.contest_id', '=', $r->contest_id)
					->whereNull('photos.deleted_at')
					->groupBy('photos.contest_id')->first();
					
				if( count($toApprove) > 0 ) {
					$r->photos_to_approve = $toApprove->photos_to_approve;
				}
					
			} else if ( $r->contest_type == 3 ) {
				$toApprove = DB::table('essays')
					->select(DB::raw('
						count(*) as essays_to_approve
						'))
					->where('essays.approval_step_id', '=', 1)
					->where('essays.contest_id', '=', $r->contest_id)
					->whereNull('essays.deleted_at')
					->groupBy('essays.contest_id')->first();
					
				if( count($toApprove) > 0 ) {
					$r->essays_to_approve = $toApprove->essays_to_approve;
				}
			}
			
			if ( $r->template_id == null ) {
			
				
				if($r->contest_image) {
					$tag = explode('.', $r->contest_image);
					$r->contest_image = asset('/images/events/' . $tag[0] . "." . $tag[1]);
				}

			
			} else {
				
				$r->contest_image = asset('/templates/mini-squared/' . $r->template_image );
			
			}
			
			$invoice = Invoice::where('contest_id', $contest->id)->first();

			
			if (($invoice) && ($contest->confirmed_by_customer == 1) && ($invoice->coupon != "")) {
				
				$r->coupon = $invoice->coupon;
				
				$package = ServiceFee::where('service_type', $contest->service_type)->first();
				
				if ($invoice->amount == $package->fixed_fee)
					$r->coupon_discount = "FREE";
				else {
					$coupon_discount = round($invoice->coupon_amount/100, 2);
					$r->coupon_discount = "-".$coupon_discount." ".$r->contest_currency;
				}
					
			}
					
			
		}
        
        $resultOrdered = [];
        if($result) {
            foreach ($orderStatus as $status) {
                foreach ($result as $contest) {
                    if ($contest->contest_status == $status) {
                        $resultOrdered[] = $contest;
                    }
                }
            }
        }

		$queries = DB::getQueryLog();
        Log::debug("contestList queries: ".print_r($queries,true));
		Log::debug("Result contestList: ".print_r($result,true));


		return json_encode($resultOrdered);
	
	}
	
	public function contest($id) {
	
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		
		$facebook_data = 0;
		$twitter_data = 0;
		$instagram_data = 0;
		$youtube_data = 0;
		
		$contest = Contest::find($id);
		if (count($contest->fbPages) > 0)
		{
			$facebook_data = 1;
		}
		
		if (count($contest->twPages) > 0)
		{
			$twitter_data = 1;
		}
		
		if (count($contest->ytPages) > 0)
		{
			$youtube_data = 1;
		}
		
		if (!is_null($contest->instagram))
		{
			$instagram_data = 1;
		}
		
		$queryBuilder = DB::table('companys')
							->join("contests", "contests.company_id", "=", "companys.id")
							->where("contests.id", "=", $id)
							->whereNull('contests.deleted_at');
							
		$queryBuilder->select(DB::raw('
			companys.id as company_id,
			companys.name as company_name, 
			contests.id as contest_id,
			contests.name as contest_name, 
			contests.type_id as contest_type,
			contests.status as contest_status,
			contests.image as contest_image,
			contests.contest_route as contest_route,
			contests.win_modality_id as win_modality_id
			'));
			
		$result = $queryBuilder->first();
		
		
		$result->facebook_data = $facebook_data;
		$result->twitter_data = $twitter_data;
		$result->youtube_data = $youtube_data;
		$result->instagram_data = $instagram_data;
		
		$queries = DB::getQueryLog();
        Log::debug("contest queries: ".print_r($queries,true));
		
		Log::debug("Result contest: ".print_r($result,true));
		//json_encode(array("result"=> "0", "message" => "You don't have read permission"));
		return json_encode($result);
	
	}
	
	public function videosOfContest($id) {
	
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		
		$col = Input::get('col');
		$approvationId = Input::get('approvation_id');
		
		if ( $col )  {
		
			$queryBuilder = DB::table('videos')
								->join("contests", "contests.id", "=", "videos.contest_id")
								->join("users", "users.id", "=", "videos.user_id")
								->where("videos.contest_id", "=", $id)
								->whereNull('contests.deleted_at')
								->whereNull('videos.deleted_at');
			
			if ( $approvationId ) {
				$queryBuilder = $queryBuilder->where("videos.approval_step_id", "=", $approvationId);
			}
			
			$queryBuilder->select(DB::raw('
				videos.id as post_id,
				videos.name as post_name,
				users.name as user_name,
				users.surname as user_surname,
				videos.rank_position as post_rank_position,
				videos.score as post_score,
				videos.image as post_image,
				videos.filename as post_mp4,
				videos.user_connected_to_facebook as user_connected_to_facebook,
				videos.user_connected_to_twitter as user_connected_to_twitter,
				videos.approval_step_id as approval_step
				'));
				
			$queryBuilder->orderBy('videos.score', 'desc')->orderBy('videos.created_at', 'desc')->paginate($col);
				
			$result = $queryBuilder->get();
			
			foreach ( $result as $r ) {
				$tag = explode(".", $r->post_image);
				$r->post_image = $tag[0] . "-sqthumbnail." . $tag[1];
				
				
				/*
				navigator.userAgent.match(/webOS/i)
					 || navigator.userAgent.match(/iPhone/i)
					 || navigator.userAgent.match(/iPad/i)
					 || navigator.userAgent.match(/iPod/i)
					 
				*/
				$r->streaming_url_ios = "http://www.buzzmybrand.com:1935/vod/mp4:" . $r->post_mp4 . "/playlist.m3u8";
				
				
				
				
				/*
				navigator.userAgent.match(/Android/i)
				*/
				$r->streaming_url_android = "rtsp://www.buzzmybrand.com:1935/vod/" . $r->post_mp4;
				
				
				
				/*
				other agents
				*/
				$r->streaming_url = "rtmp://www.buzzmybrand.com:1935/vod/mp4:" . $r->post_mp4;
				
				
			}
			
			$queries = DB::getQueryLog();
			Log::debug("videosOfContest queries: ".print_r($queries,true));
			
			return json_encode($result);
		
		} else {
		
			return json_encode(array("result"=> "0", "message" => "Missing parameters"));
		}
		
	}
	
	public function photosOfContest($id) {
		
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		
		$col = Input::get('col');
		$approvationId = Input::get('approvation_id');
		
		if ( $col ) {
		
			$queryBuilder = DB::table('photos')
								->join("contests", "contests.id", "=", "photos.contest_id")
								->join("users", "users.id", "=", "photos.user_id")
								->where("photos.contest_id", "=", $id)
								->whereNull('photos.deleted_at')
								->whereNull('contests.deleted_at');
								
			if ( $approvationId ) {
				$queryBuilder = $queryBuilder->where("photos.approval_step_id", "=", $approvationId);
			}
								
			$queryBuilder->select(DB::raw('
				photos.id as post_id,
				photos.name as post_name,
				users.name as user_name,
				users.surname as user_surname,
				photos.rank_position as post_rank_position,
				photos.score as post_score,
				photos.image as post_image,
				photos.user_connected_to_facebook as user_connected_to_facebook,
				photos.user_connected_to_twitter as user_connected_to_twitter,
				photos.user_connected_to_instagram as user_connected_to_instagram,
				photos.approval_step_id as approval_step
				'));
				
			$queryBuilder->orderBy('photos.score', 'desc')->orderBy('photos.created_at', 'desc')->paginate($col);
				
			$result = $queryBuilder->get();
			
			foreach ( $result as $r ) {
				if (!$r->user_connected_to_instagram)
				{
					$tag = explode(".", $r->post_image);
					$r->post_image = $tag[0] . "-sqthumbnail." . $tag[1];
				}
			}
			
			$queries = DB::getQueryLog();
			Log::debug("photosOfContest queries: ".print_r($queries,true));
			
			return json_encode($result);
		
		} else {
		
			return json_encode(array("result"=> "0", "message" => "Missing parameters"));
		}
		
	}
	
	public function essaysOfContest($id) {
		
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		
		$col = Input::get('col');
		$approvationId = Input::get('approvation_id');
		
		if ( $col ) {
		
			$queryBuilder = DB::table('essays')
								->join("contests", "contests.id", "=", "essays.contest_id")
								->join("users", "users.id", "=", "essays.user_id")
								->where("essays.contest_id", "=", $id)
								->whereNull('essays.deleted_at')
								->whereNull('contests.deleted_at');
								
			if ( $approvationId ) {
				$queryBuilder = $queryBuilder->where("essays.approval_step_id", "=", $approvationId);
			}
								
			$queryBuilder->select(DB::raw('
				essays.id as post_id,
				essays.sentence as post_sentence,
				users.name as user_name,
				users.surname as user_surname,
				essays.rank_position as post_rank_position,
				essays.score as post_score,
				essays.image as post_image,
				essays.user_connected_to_facebook as user_connected_to_facebook,
				essays.user_connected_to_twitter as user_connected_to_twitter,
				essays.approval_step_id as approval_step
				'));
				
			$queryBuilder->orderBy('essays.score', 'desc')->orderBy('essays.created_at', 'desc')->paginate($col);
				
			$result = $queryBuilder->get();
			
			foreach ( $result as $r ) {
				$tag = explode(".", $r->post_image);
				$r->post_image = $tag[0] . "-sqthumbnail." . $tag[1];
			}
			
			$queries = DB::getQueryLog();
			Log::debug("essaysOfContest queries: ".print_r($queries,true));
			
			return json_encode($result);
		
		} else {
		
			return json_encode(array("result"=> "0", "message" => "Missing parameters"));
		}
		
	}
	
	public function sendFacebookNotification($id) {
	
		////////// Security Check ////////////////////////////////////
		if ( Input::get("content_type") && $negativeResponse = $this->checkAccesVideoOrPhotoOrEssay($id, Input::get("content_type") ) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
	
		// MANDATORY PARAMETER
		$contentType = Input::get("content_type");
	
		// OPTIONAL PARAMETER
		$message = Input::get("message");
		$href = Input::get("href");
		
	
		if ( $contentType ) {
		
			if ( $contentType == "1" ) {
			
				$video = Video::find($id);
				
				$user = User::find($video->user_id);
				$contest = Contest::find($video->contest_id);
				$facebookProfile = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
			
			} else if ( $contentType == "2" ) {
			
				$photo = Photo::find($id);
				
				$user = User::find($photo->user_id);
				$contest = Contest::find($photo->contest_id);
				$facebookProfile = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
			
			} else if ( $contentType == "3" ) {
			
				$essay = Essay::find($id);
				
				$user = User::find($essay->user_id);
				$contest = Contest::find($essay->contest_id);
				$facebookProfile = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
			
			}
			
			
			try
			{		
			
				$facebook = new Facebook\Facebook([
					'app_id' => $contest->company->fbApp_clientID,
					'app_secret' => $contest->company->fbApp_clientSecret,
					'default_graph_version' => 'v2.4',
				]);
		
				Log::debug("sendFacebookNotification /" . $facebookProfile->id_fb . "/notifications");
				$responseFBAPP = $facebook->post("/" . $facebookProfile->id_fb . "/notifications", array(
					"template" => $message,
					"href" => $href,
				), $contest->company->app_accesstoken );
				Log::debug("sendFacebookNotification done!");
				
				return json_encode(array("result" => "1"));
				
			}
			catch (Exception $ex)
			{
				Log::debug('Error during sendFacebookNotification: '.$ex);
				return json_encode(array("result"=> "0", "message" => "".$ex));
			}
		
		} else {
		
			return json_encode(array("result" => "0", "message" => "Missing required parameter"));
			
		}
		
	
	}
	
	
	public function sendTwitterNotification($id) {
		
		////////// Security Check ////////////////////////////////////
		if ( Input::get("content_type") && $negativeResponse = $this->checkAccesVideoOrPhotoOrEssay($id, Input::get("content_type") ) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		
		// MANDATORY PARAMETER
		$contentType = Input::get("content_type");
		
		// OPTIONAL PARAMETER
		$message = Input::get("message");
		
		if ( $contentType ) {
		
			if ( $contentType == "1" ) {
			
				$video = Video::find($id);
				
				$user = User::find($video->user_id);
				$contest = Contest::find($video->contest_id);
				$twitterProfile = TwitterProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
				
				$twPage = null;
				if ( $video->contest->twPages->count() > 0 ) {
					$twPage = $video->contest->twPages->first();
				}
				
			
			} else if ( $contentType == "2" ) {
			
				$photo = Photo::find($id);
				
				$user = User::find($photo->user_id);
				$contest = Contest::find($photo->contest_id);
				$twitterProfile = TwitterProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
			
				$twPage = null;
				if ( $photo->contest->twPages->count() > 0 ) {
					$twPage = $photo->contest->twPages->first();
				}
			
			} else if ( $contentType == "3" ) {
			
				$essay = Essay::find($id);
				
				$user = User::find($essay->user_id);
				$contest = Contest::find($essay->contest_id);
				$twitterProfile = TwitterProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
			
				$twPage = null;
				if ( $essay->contest->twPages->count() > 0 ) {
					$twPage = $essay->contest->twPages->first();
				}
			
			}
		

		
			try {
				
				$parameters = array(
					'user_id' => $twitterProfile->tw_id,
					'text' => $message,
				);
				
				$twitterConfig = Config::get('twitter');
				
				
				$twitteroauth = new TwitterOAuth($twitterConfig['consumer_key'], $twitterConfig['consumer_secret'],  $twPage->tw_page_admin_at , $twPage->tw_page_secret_token );
				$tweet_data = $twitteroauth->post('https://api.twitter.com/1.1/direct_messages/new.json', $parameters);			
			
				Log::debug("sendTwitterNotification done!" . print_r($tweet_data, true));
			
				return json_encode(array("result" => "1"));
			
			} catch (Exception $ex){
			
				Log::debug('Error during sendTwitterNotification: '.$ex);
				return json_encode(array("result"=> "0", "message" => "".$ex));
				
			}
		
		} else {
		
			return json_encode(array("result" => "0", "message" => "Missing required parameter"));
		
		}
		
	}
	
	public function sendMassNotification($id) {
	
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		$type_logged = Input::get('mass_type_notification_logged');
		$type_applyer = Input::get('mass_type_notification_applyer');
		$message = Input::get("message");
		$href = Input::get("href");
		
		$loggedUsers = null;
		$usersApplyers = null;
		
		try {
		
			$contest = Contest::find($id);
			$contestType = $contest->type_id;
			
			$users = null;
			
			if ( $type_logged == 'notify' ) {
			
				$contest = Contest::with(
					array('users' => function($query) use ($id, $contestType) {
					
						if ( $contestType == '1' ) {
						
							if ( !empty (Video::where('contest_id', $id)->lists('user_id')) ) {
								$query->whereNotIn('users.id', Video::where('contest_id', $id)->lists('user_id'))->distinct();
							}

							
						} else if ( $contestType == '2' ) {
						
							if ( !empty (Photo::where('contest_id', $id)->lists('user_id')) ) {
								$query->whereNotIn('users.id', Photo::where('contest_id', $id)->lists('user_id'))->distinct();
							}
						
							
						} else if ( $contestType == '3' ) {
						
							if ( !empty (Essay::where('contest_id', $id)->lists('user_id')) ) {
								$query->whereNotIn('users.id', Essay::where('contest_id', $id)->lists('user_id'))->distinct();
							}
						
							
						}
						
					})
				)->where('id', $id)->first();
				
				$loggedUsers = $contest->users;
				
		
				$queries = DB::getQueryLog();
				Log::debug("Queries: ".print_r($queries,true));
			
			}
			
			if ( $type_applyer == 'notify' ) {
			
				$contest = Contest::with(
					array('users' => function($query) use ($id, $contestType) {
					
						if ( $contestType == '1' ) {
							$query->whereIn('users.id', Video::where('contest_id', $id)->lists('user_id'))->distinct();
						} else if ( $contestType == '2' ) {
							$query->whereIn('users.id', Photo::where('contest_id', $id)->lists('user_id'))->distinct();
						}
						
					})
				)->where('id', $id)->first();
				
				$usersApplyers = $contest->users;
				
				$queries = DB::getQueryLog();
				Log::debug("Queries: ".print_r($queries,true));
			
			}
			
			$loggedUsersArray = array();
			$usersApplyersArray = array();
			
			if ( $loggedUsers != null) {
				$loggedUsersArray = $loggedUsers->toArray();
			}
			
			if ( $usersApplyers != null) {
				$usersApplyersArray = $usersApplyers->toArray();
			}
			
			$users = array_merge($loggedUsersArray, $usersApplyersArray);
		
			
			foreach( $users as $user ) {
			
				$facebookProfile = FacebookProfile::where('user_id', $user['id'])->where('company_id', $contest->company_id)->first();
			
				if ( $facebookProfile ) {
				
					$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);
			
					Log::debug("sendFacebookNotification /" . $facebookProfile->id_fb . "/notifications");
					$responseFBAPP = $facebook->post("/" . $facebookProfile->id_fb . "/notifications", array(
						"template" => $message,
						"href" => $href,
					), $contest->company->app_accesstoken );
					Log::debug("sendFacebookNotification done!");
				
				}
				
				$twitterProfile = TwitterProfile::where('user_id', $user['id'])->where('company_id', $contest->company_id)->first();
				
				if ( $twitterProfile ) {
				
					$twPage = null;
					if ( $contest->twPages->count() > 0 ) {
						$twPage = $contest->twPages->first();
					}
					
					$parameters = array(
						'user_id' => $twitterProfile->tw_id,
						'text' => $message,
					);
					
					$twitterConfig = Config::get('twitter');
					
					
					$twitteroauth = new TwitterOAuth($twitterConfig['consumer_key'], $twitterConfig['consumer_secret'],  $twPage->tw_page_admin_at , $twPage->tw_page_secret_token );
					$tweet_data = $twitteroauth->post('https://api.twitter.com/1.1/direct_messages/new.json', $parameters);			
				
					Log::debug("sendTwitterNotification done!");
				
				}
				
			
			}
			
			return json_encode(array("result" => "1"));

		
		} catch (Exception $ex){
		
			Log::debug('Error during sendMassNotification: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
	
	}
	
		public function contestData($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		
		$resultArray = array(
			'total_impressions' => 0,
			'reach' => 0,
			'total_impressions_entries' => 0,
			'reach_entries' => 0,
			'total_views' => 0,
			'unique_views' => 0,
			'unique_likes' => 0,
			'unique_comments' => 0,
			'unique_shares' => 0,
			'total_engaged_users' => 0,
			'retweets' => 0,
			'favourite' => 0,
			'logged_from_website' => 0,
			'logged_from_mobile' => 0,			
			'logged_from_instagram' => 0,
			'daily_new_likes' => 0,
			'page_engaged_users' =>  0,
			'new_followers' => 0,
			'youtube_view_count' => 0,
			'youtube_like_count' => 0,
			'instagram_entries' => 0,
			'instagram_total_likes' => 0,
			'instagram_total_comments' => 0,
			
			'num_friends' => 0,
			'logins_facebook' => 0,
			'logins_twitter' => 0,
			'num_entries_fb' => 0,
			'num_entries_tw' => 0,
			'' => 0,
			'' => 0,
		);
		
		$contest = Contest::find($id);
		
				
		if (count($contest->fbPages) > 0)
		{
			$facebookProfiles = FacebookProfile::where('company_id', $contest->company_id)->get();
			$resultArray['logins_facebook'] = count($facebookProfiles);
			foreach ($facebookProfiles as $fp)
			{
				$resultArray['num_friends'] += $fp->num_friends;
			}
		}
		
		
		if (count($contest->twPages) > 0)
		{
			$twitterProfiles = TwitterProfile::where('company_id', $contest->company_id)->get();	
			$resultArray['logins_twitter'] = count($twitterProfiles);
			foreach ($twitterProfiles as $tp)
			{
				$resultArray['num_friends'] += $tp->followers;
			}
		}
		
		if ( $contest->type_id == 1 ) {
			
			$totalEntries = Video::where("contest_id", "=", $id)->get();
			$resultArray['total_entries'] = count($totalEntries);
			
			$approvedVideos = Video::where("contest_id", "=", $id)->where("approval_step_id", "=", "3")->get();
			$resultArray['approved_videos'] = count($approvedVideos);
			
			//devo calcolarmi le entries provenienti da facebook
			$approvedVideosFB = Video::where("contest_id", "=", $id)->where("approval_step_id", "=", "3")->where("user_connected_to_facebook", "=", "1")->get();
			$resultArray['num_entries_fb'] = count($approvedVideosFB);
			
			//devo calcolarmi le entries provenienti da twitter
			$approvedVideosTW = Video::where("contest_id", "=", $id)->where("approval_step_id", "=", "3")->where("user_connected_to_twitter", "=", "1")->get();
			$resultArray['num_entries_tw'] = count($approvedVideosTW);
			
			$notApprovedVideos = Video::where("contest_id", "=", $id)->where("approval_step_id", "=", "2")->get();
			$resultArray['not_approved_videos'] = count($notApprovedVideos);
			
			$videosToBeApproved = Video::where("contest_id", "=", $id)->where("approval_step_id", "=", "1")->get();
			$resultArray['videos_to_be_approved'] = count($videosToBeApproved);
			
			foreach($approvedVideos as $video) {
			
				foreach($video->fbPages as $vp) {
					
					$resultArray['total_impressions_entries'] += $vp->pivot->impressions;
					$resultArray['reach_entries'] += $vp->pivot->reach;
					$resultArray['total_views'] += $vp->pivot->views;
					$resultArray['unique_views'] += $vp->pivot->unique_views;
					
					$resultArray['unique_likes'] += $vp->pivot->unique_likes;
					$resultArray['unique_comments'] += $vp->pivot->unique_comments;
					$resultArray['unique_shares'] += $vp->pivot->unique_shares;
					
					$resultArray['total_engaged_users'] += $vp->pivot->engaged_users;
					
					
				}
				
				$resultArray['unique_likes'] += $video->likes_count_post;
				$resultArray['unique_comments'] += $video->comments_count_post;

				
			
				foreach($video->twPages as $vp) {
					
					$resultArray['retweets'] += $vp->pivot->retweets_count;
					$resultArray['favourite'] += $vp->pivot->favourite_count ;

				}
				

				$resultArray['retweets'] += $video->retweets_count_post;
				$resultArray['favourite'] += $video->favourite_count_post;

			
				foreach($video->ytPages as $vp) {
					
					$resultArray['youtube_view_count'] += $vp->pivot->yt_view_count;
					$resultArray['youtube_like_count'] += $vp->pivot->yt_like_count;

				}
			
			}
			
			//add social action to impression entries
			$resultArray['total_impressions_entries'] = $resultArray['total_impressions_entries'] + $resultArray['unique_likes'] + $resultArray['unique_comments'] + $resultArray['unique_shares'] + $resultArray['retweets'] + $resultArray['favourite'] + $resultArray['youtube_view_count'] + $resultArray['youtube_like_count'];
			
			//add social action to reach entries
			$resultArray['reach_entries'] = $resultArray['reach_entries'] + $resultArray['unique_likes'] + $resultArray['unique_comments'] + $resultArray['unique_shares'] + $resultArray['retweets'] + $resultArray['favourite'] + $resultArray['youtube_view_count'] + $resultArray['youtube_like_count'];
			
		} else if ( $contest->type_id == 2 ) {
		
			$totalEntries = Photo::where("contest_id", "=", $id)->get();
			$resultArray['total_entries'] = count($totalEntries);
			
			$approvedPhotos = Photo::where("contest_id", "=", $id)->where("approval_step_id", "=", "3")->get();
			$resultArray['approved_photos'] = count($approvedPhotos);
			
			//devo calcolarmi le entries provenienti da facebook
			$approvedPhotosFB = Photo::where("contest_id", "=", $id)->where("approval_step_id", "=", "3")->where("user_connected_to_facebook", "=", "1")->get();
			$resultArray['num_entries_fb'] = count($approvedPhotosFB);
			
			//devo calcolarmi le entries provenienti da twitter
			$approvedPhotosTW = Photo::where("contest_id", "=", $id)->where("approval_step_id", "=", "3")->where("user_connected_to_twitter", "=", "1")->get();
			$resultArray['num_entries_tw'] = count($approvedPhotosTW);
			
			$notApprovedPhotos = Photo::where("contest_id", "=", $id)->where("approval_step_id", "=", "2")->get();
			$resultArray['not_approved_photos'] = count($notApprovedPhotos);
			
			$photosToBeApproved = Photo::where("contest_id", "=", $id)->where("approval_step_id", "=", "1")->get();
			$resultArray['photos_to_be_approved'] = count($photosToBeApproved);
			
			$photosFromInstagram = Photo::where("contest_id", "=", $id)->where("user_connected_to_instagram", "=", "1")->get();
			$resultArray['instagram_entries'] = count($photosFromInstagram);
			
			foreach ($photosFromInstagram as $photo)
			{
				$resultArray['instagram_total_likes'] += $photo->likes_count_post;
				$resultArray['instagram_total_comments'] += $photo->comments_count_post;
			}
			
			foreach($approvedPhotos as $photo) {
			
				foreach($photo->fbPages as $fp) {
					
					//impressions totali comprensive di azioni social di ogni singolo post
					$resultArray['total_impressions_entries'] += $fp->pivot->impressions;
					$resultArray['reach_entries'] += $fp->pivot->reach;
					
					$resultArray['unique_likes'] += $fp->pivot->unique_likes;
					$resultArray['unique_comments'] += $fp->pivot->unique_comments;
					$resultArray['unique_shares'] += $fp->pivot->unique_shares;
					
					$resultArray['total_engaged_users'] += $fp->pivot->engaged_users;
					
				}
				
								
				$resultArray['unique_likes'] += $photo->likes_count_post;
				$resultArray['unique_comments'] += $photo->comments_count_post;

				
			
				foreach($photo->twPages as $fp) {
					
					$resultArray['retweets'] += $fp->pivot->retweets_count;
					$resultArray['favourite'] += $fp->pivot->favourite_count ;

				}
				
				$resultArray['retweets'] += $photo->retweets_count_post;
				$resultArray['favourite'] += $photo->favourite_count_post;
			
			}
			

			//add social action to impression entries
			$resultArray['total_impressions_entries'] = $resultArray['total_impressions_entries'] + $resultArray['unique_likes'] + $resultArray['unique_comments'] + $resultArray['unique_shares'] + $resultArray['retweets'] + $resultArray['favourite'] + $resultArray['instagram_total_likes'] + $resultArray['instagram_total_comments'];
			
			//add social action to reach entries
			$resultArray['reach_entries'] = $resultArray['reach_entries'] + $resultArray['unique_likes'] + $resultArray['unique_comments'] + $resultArray['unique_shares'] + $resultArray['retweets'] + $resultArray['favourite'] + $resultArray['instagram_total_likes'] + $resultArray['instagram_total_comments'];
		
		} else if ( $contest->type_id == 3 ) {
		
			$totalEntries = Essay::where("contest_id", "=", $id)->get();
			$resultArray['total_entries'] = count($totalEntries);
			
			$approvedEssays = Essay::where("contest_id", "=", $id)->where("approval_step_id", "=", "3")->get();
			$resultArray['approved_essays'] = count($approvedEssays);
			
			//devo calcolarmi le entries provenienti da facebook
			$approvedEssaysFB = Essay::where("contest_id", "=", $id)->where("approval_step_id", "=", "3")->where("user_connected_to_facebook", "=", "1")->get();
			$resultArray['num_entries_fb'] = count($approvedEssaysFB);
			
			//devo calcolarmi le entries provenienti da twitter
			$approvedEssaysTW = Essay::where("contest_id", "=", $id)->where("approval_step_id", "=", "3")->where("user_connected_to_twitter", "=", "1")->get();
			$resultArray['num_entries_tw'] = count($approvedEssaysTW);
			
			$notApprovedEssays = Essay::where("contest_id", "=", $id)->where("approval_step_id", "=", "2")->get();
			$resultArray['not_approved_essays'] = count($notApprovedEssays);
			
			$essaysToBeApproved = Essay::where("contest_id", "=", $id)->where("approval_step_id", "=", "1")->get();
			$resultArray['essays_to_be_approved'] = count($essaysToBeApproved);
			
			foreach($approvedEssays as $essay) {
			
				foreach($essay->fbPages as $fp) {
					
					$resultArray['total_impressions_entries'] += $fp->pivot->impressions;
					$resultArray['reach_entries'] += $fp->pivot->reach;
					
					$resultArray['unique_likes'] += $fp->pivot->unique_likes;
					$resultArray['unique_comments'] += $fp->pivot->unique_comments;
					$resultArray['unique_shares'] += $fp->pivot->unique_shares;
					
					$resultArray['total_engaged_users'] += $fp->pivot->engaged_users;
					
				}
				
				$resultArray['unique_likes'] += $essay->likes_count_post;
				$resultArray['unique_comments'] += $essay->comments_count_post;
			
				
			
				foreach($essay->twPages as $fp) {
					
					$resultArray['retweets'] += $fp->pivot->retweets_count;
					$resultArray['favourite'] += $fp->pivot->favourite_count ;

				}
				
				$resultArray['retweets'] += $essay->retweets_count_post;
				$resultArray['favourite'] += $essay->favourite_count_post;
			
			}
			
			//add social action to impression entries
			$resultArray['total_impressions_entries'] = $resultArray['total_impressions_entries'] + $resultArray['unique_likes'] + $resultArray['unique_comments'] + $resultArray['unique_shares'] + $resultArray['retweets'] + $resultArray['favourite'];
			
			//add social action to reach entries
			$resultArray['reach_entries'] = $resultArray['reach_entries'] + $resultArray['unique_likes'] + $resultArray['unique_comments'] + $resultArray['unique_shares'] + $resultArray['retweets'] + $resultArray['favourite'];
		
		}
		
		
		$resultForLoggedFromWebsite = DB::table('contest_user')
										->select(DB::raw('contest_id, count(*) as logged_from_website'))
										->where('contest_id', '=', $id)
										->whereNotNull('login_website')
										->whereNull('login_mobile')
										->groupBy('contest_id')
										->get();
										
		$resultForLoggedFromMobile = DB::table('contest_user')
										->select(DB::raw('contest_id, count(*) as logged_from_mobile'))
										->where('contest_id', '=', $id)
										->whereNotNull('login_mobile')
										->whereNull('login_website')
										->groupBy('contest_id')
										->get();
										
										
		$resultForLoggedFromInstagram = DB::table('contest_user')
										->select(DB::raw('contest_id, count(*) as logged_from_instagram'))
										->where('contest_id', '=', $id)
										->whereNotNull('login_instagram')
										->whereNull('login_website')
										->whereNull('login_mobile')
										->groupBy('contest_id')
										->get();
										
										
		
		$logged_from_website = 0;
		if ( count($resultForLoggedFromWebsite) > 0 ) {
			$resultArray['logged_from_website'] = $resultForLoggedFromWebsite[0]->logged_from_website;
		}
		
		$logged_from_mobile = 0;
		if ( count($resultForLoggedFromMobile) > 0 ) {
			$resultArray['logged_from_mobile'] = $resultForLoggedFromMobile[0]->logged_from_mobile;
		}
		
		$logged_from_instagram = 0;
		if ( count($resultForLoggedFromInstagram) > 0 ) {
			$resultArray['logged_from_instagram'] = $resultForLoggedFromInstagram[0]->logged_from_instagram;
		}
		
		$contestId = $contest->id;
		$contestType = $contest->type_id;
		if ((count(Video::where('contest_id', $contestId)->lists('user_id')) > 0) || (count(Photo::where('contest_id', $contestId)->lists('user_id')) > 0))
		{
			$contest = Contest::with(
				array('users' => function($query) use ($contestId, $contestType) {
				
					if ( $contestType == '1' ) {
						$query->whereIn('users.id', Video::where('contest_id', $contestId)->lists('user_id'))->distinct();
					} else if ( $contestType == '2' ) {
						$query->whereIn('users.id', Photo::where('contest_id', $contestId)->lists('user_id'))->distinct();
					}
					
				})
			)->where('id', $contestId)->first();
		
			$resultArray['unique_participants'] = count($contest->users);
		}
		else
			$resultArray['unique_participants'] = 0;
		//$resultArray['unique_participants'] = count($contest->users);
		//***************
		//$resultArray['daily_new_likes'] = $contest->daily_new_likes;
		//$resultArray['page_engaged_users'] = $contest->page_engaged_users;
		
		foreach ($contest->fbPages as $page) {
			
			$resultArray['total_impressions'] += $page->pivot->impressions;
			$resultArray['reach'] += $page->pivot->reach;
			$resultArray['daily_new_likes'] += $page->pivot->new_likes;
			$resultArray['page_engaged_users'] += $page->pivot->engaged_users;
		}
			
		$followers_list_count = 0;
		foreach ($contest->twPages as $page) {
			$followers_list_count += $page->pivot->follower_count_2 - $page->pivot->follower_count_1;
				
		}
		$resultArray['new_followers'] = $followers_list_count;

		$subscriptions_list_count = 0;
		foreach ($contest->ytPages as $page) {
			$subscriptions_list_count += $page->pivot->subscriptions_count_2 - $page->pivot->subscriptions_count_1;
		}
		$resultArray['new_subscribers'] = $subscriptions_list_count;
				//***************
		//$resultArray['new_followers'] = $contest->follower_count_2 - $contest->follower_count_1;
		//$resultArray['new_subscribers'] = $contest->subscribers_list_count2 - $contest->subscribers_list_count1;
		
		
		$resultArray["reach_company_pages"] = $resultArray['total_impressions_entries'];
		$resultArray["reach_user_profile_pages"] = round($resultArray['num_friends'] * 0.45, 0);
		
			//$resultArray['unique_likes'] + $resultArray['unique_comments'] + $resultArray['unique_shares'] + $resultArray['retweets'] + $resultArray['favourite'] + $resultArray['instagram_total_likes'] + $resultArray['instagram_total_comments'] + $resultArray['youtube_view_count'] + $resultArray['youtube_like_count'];
			$resultArray["viral_reach_from_users"] = round(($resultArray['unique_shares'] + $resultArray['unique_likes'] + $resultArray['unique_comments'] +  + $resultArray['retweets'] + $resultArray['favourite'] + $resultArray['instagram_total_likes'] + $resultArray['instagram_total_comments'] + $resultArray['youtube_view_count'] + $resultArray['youtube_like_count'])*0.45, 0);
		
		
		$resultArray["total_reach"] = $resultArray["reach_company_pages"] + $resultArray["reach_user_profile_pages"] + $resultArray["viral_reach_from_users"];
		
		if ($contest->budget == 0)
		{
			$resultArray["budget"] = 1;
		}
		else
		{
			$resultArray["budget"] = $contest->budget;
		}		
				
		$resultArray["earned_media"] = round((5*$resultArray["total_reach"])/1000, 0);		
		
		
		$resultArray["roi"] =  round($resultArray["earned_media"]/$resultArray["budget"], 3);
		
		Log::debug("Contest details result: ".json_encode($resultArray));
		return json_encode($resultArray); 
		
		
	}
	
	
	public function singlePostDataForVideos($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		$col = Input::get('col');
		
		if ( $col ) {
	
			$queryBuilder = DB::table("videos")
								->select(DB::raw('
									videos.id as post_id,
									videos.name as post_name,
									users.name as user_name,
									users.surname as user_surname,
									videos.rank_position as post_rank_position,
									videos.score as post_score,
									videos.image as post_image,
									videos.filename as post_mp4,
									videos.uploadOnFacebookDone as upload_on_facebook_done,
									videos.uploadOnTwitterDone as upload_on_twitter_done,
									videos.uploadOnOwnYouTubeDone as upload_on_own_youtube_done,
									videos.uploadOnCompanyYoutubeDone as upload_on_company_youtube_done,
									videos.user_connected_to_facebook as user_connected_to_facebook,
									videos.user_connected_to_twitter as user_connected_to_twitter,
									videos.user_connected_to_instagram as user_connected_to_instagram,
									videos.total_count_link as video_total_count_link,
									coalesce(sum(fb_page_video.unique_likes), 0) + coalesce(videos.likes_count_post, 0) as unique_like, 
									coalesce(sum(fb_page_video.unique_comments), 0) + coalesce(videos.comments_count_post, 0) as unique_comments,
									coalesce(sum(fb_page_video.unique_shares), 0) as unique_shares,
									coalesce(sum(fb_page_video.unique_views), 0) as unique_views,
									coalesce(sum(fb_page_video.reach), 0) as reach,
									coalesce(sum(fb_page_video.impressions), 0) + coalesce(sum(fb_page_video.unique_likes), 0) + coalesce(videos.likes_count_post, 0) + coalesce(sum(fb_page_video.unique_comments), 0) + coalesce(videos.comments_count_post, 0) + coalesce(sum(fb_page_video.unique_shares), 0)  as impressions,
									coalesce(sum(tw_page_video.retweets_count), 0) + coalesce(videos.retweets_count_post, 0) as retweets_count,
									coalesce(sum(tw_page_video.favourite_count), 0) + coalesce(videos.favourite_count_post, 0) as favourite_count,
									coalesce(sum(yt_page_video.yt_view_count), 0) as youtube_view_count,
									coalesce(sum(yt_page_video.yt_like_count), 0) as youtube_like_count
								'))->leftJoin("users", "users.id", "=", "videos.user_id")
								->leftJoin("fb_page_video", "fb_page_video.video_id", "=", "videos.id")
								->leftJoin("tw_page_video", "tw_page_video.video_id", "=", "videos.id")
								->leftJoin("yt_page_video", "yt_page_video.video_id", "=", "videos.id")
								->where("videos.contest_id", "=", $id)
								->where("videos.rank_position", ">", 0)
								->whereNull('videos.deleted_at')
								->groupBy('videos.id')
								->orderBy('videos.score', 'desc')->paginate($col);
			
		

			
			
			$result = $queryBuilder;
			
			foreach ( $result->toArray()['data'] as $r ) {
                //$r->post_name = utf8_decode($r->post_name);
				$tag = explode(".", $r->post_image);
				$r->post_image = $tag[0] . "-sqthumbnail." . $tag[1];
				
				
				
				
				/*
				navigator.userAgent.match(/webOS/i)
					 || navigator.userAgent.match(/iPhone/i)
					 || navigator.userAgent.match(/iPad/i)
					 || navigator.userAgent.match(/iPod/i)
					 
				*/
				$r->streaming_url_ios = "http://www.buzzmybrand.it:1935/vod/mp4:" . $r->post_mp4 . "/playlist.m3u8";
				
				
				
				
				/*
				navigator.userAgent.match(/Android/i)
				*/
				$r->streaming_url_android = "rtsp://www.buzzmybrand.it:1935/vod/" . $r->post_mp4;
				
				
				
				/*
				other agents
				*/
				$r->streaming_url = "rtmp://www.buzzmybrand.it:1935/vod/mp4:" . $r->post_mp4;
				
				
				
				
				
				
				
				
			}
				
			$queries = DB::getQueryLog();
			Log::debug("singlePostDataForVideos queries: ".print_r($queries,true));
			
			return json_encode($result->toArray()['data']);
			
		} else {
		
			return json_encode(array("result" => "0", "message" => "Missing required parameter"));
			
		}
	
	}
	
	
	public function singlePostDataForPhotos($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		$col = Input::get('col');
		
		if ( $col ) {
	
			$queryBuilder = DB::table("photos")
								->select(DB::raw('
									photos.id as post_id,
									photos.name as post_name,
									users.name as user_name,
									users.surname as user_surname,
									photos.rank_position as photo_rank_position,
									photos.score as post_score,
									photos.image as post_image,
									photos.uploadOnFacebookDone as upload_on_facebook_done,
									photos.uploadOnTwitterDone as upload_on_twitter_done,
									photos.user_connected_to_facebook as user_connected_to_facebook,
									photos.user_connected_to_twitter as user_connected_to_twitter,
									photos.user_connected_to_instagram as user_connected_to_instagram,
									photos.total_count_link as photo_total_count_link,
									coalesce(sum(fb_page_photo.unique_likes), 0) + coalesce(photos.likes_count_post, 0) as unique_like, 
									coalesce(sum(fb_page_photo.unique_comments), 0) + coalesce(photos.comments_count_post, 0) as unique_comments,
									coalesce(sum(fb_page_photo.unique_shares), 0) as unique_shares,
									coalesce(sum(fb_page_photo.reach), 0) as reach,
									coalesce(sum(fb_page_photo.impressions), 0) + coalesce(sum(fb_page_photo.unique_likes), 0) + coalesce(photos.likes_count_post, 0) + coalesce(sum(fb_page_photo.unique_comments), 0) + coalesce(photos.comments_count_post, 0) + coalesce(sum(fb_page_photo.unique_shares), 0)  as impressions,
									coalesce(sum(tw_page_photo.retweets_count), 0) + coalesce(photos.retweets_count_post, 0) as retweets_count,
									coalesce(sum(tw_page_photo.favourite_count), 0) + coalesce(photos.favourite_count_post, 0) as favourite_count
								'))->leftJoin("users", "users.id", "=", "photos.user_id")
								->leftJoin("fb_page_photo", "fb_page_photo.photo_id", "=", "photos.id")
								->leftJoin("tw_page_photo", "tw_page_photo.photo_id", "=", "photos.id")
								->where("photos.contest_id", "=", $id)
								->where("photos.rank_position", ">", 0)
								->whereNull('photos.deleted_at')
								->groupBy('photos.id')
								->orderBy('photos.score', 'desc')->paginate($col);
			
		

			
			
			$result = $queryBuilder;
			
					
			
			foreach ( $result->toArray()['data'] as $r ) {
				//$r->post_name = utf8_decode($r->post_name);
                if (!$r->user_connected_to_instagram)
                {
                    $tag = explode(".", $r->post_image);
                    $r->post_image = $tag[0] . "-sqthumbnail." . $tag[1];
                }
                else if ($r->user_connected_to_instagram)
                {
                    $photo = Photo::find($r->post_id);
                    $r->instagram_likes = $photo->likes_count_post;
                    $r->instagram_comments = $photo->comments_count_post;
					
				}
				
				
			}
			
			$queries = DB::getQueryLog();
			Log::debug("singlePostDataForPhotos queries: ".print_r($queries,true));
			//Log::debug("Result Photos for analytics: ".print_r($result->toArray()['data'],true));
			return json_encode($result->toArray()['data']);
		
		} else {
		
			return json_encode(array("result" => "0", "message" => "Missing required parameter"));
			
		}
	
	}
	
	public function singlePostDataForEssays($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		$col = Input::get('col');
		
		if ( $col ) {
	
			$queryBuilder = DB::table("essays")
								->select(DB::raw('
									essays.id as essay_id,
									essays.sentence as post_sentence,
									users.name as user_name,
									users.surname as user_surname,
									essays.rank_position as essay_rank_position,
									essays.image as post_image,
									essays.rank_position as post_rank_position,
									essays.score as post_score,
									essays.user_connected_to_facebook as user_connected_to_facebook,
									essays.user_connected_to_twitter as user_connected_to_twitter,
									essays.total_count_link as post_total_count_link,
									coalesce(sum(fb_page_essay.unique_likes), 0) + coalesce(essays.likes_count_post, 0) as unique_like, 
									coalesce(sum(fb_page_essay.unique_comments), 0) + coalesce(essays.comments_count_post, 0) as unique_comments,
									coalesce(sum(fb_page_essay.unique_shares), 0) as unique_shares,
									coalesce(sum(fb_page_essay.reach), 0) as reach,
									coalesce(sum(fb_page_essay.impressions), 0) + coalesce(sum(fb_page_essay.unique_likes), 0) + coalesce(essays.likes_count_post, 0) + coalesce(sum(fb_page_essay.unique_comments), 0) + coalesce(essays.comments_count_post, 0) + coalesce(sum(fb_page_essay.unique_shares), 0)  as impressions,
									coalesce(sum(tw_page_essay.retweets_count), 0) + coalesce(essays.retweets_count_post, 0) as retweets_count,
									coalesce(sum(tw_page_essay.favourite_count), 0) + coalesce(essays.favourite_count_post, 0) as favourite_count
								'))->leftJoin("users", "users.id", "=", "essays.user_id")
								->leftJoin("fb_page_essay", "fb_page_essay.essay_id", "=", "essays.id")
								->leftJoin("tw_page_essay", "tw_page_essay.essay_id", "=", "essays.id")
								->where("essays.contest_id", "=", $id)
								->where("essays.rank_position", ">", 0)
								->whereNull('essays.deleted_at')
								->groupBy('essays.id')
								->orderBy('essays.score', 'desc')->paginate($col);
			
		
			
			
			
			$result = $queryBuilder;
			
			foreach ( $result->toArray()['data'] as $r ) {
                //$r->post_sentence = utf8_decode($r->post_sentence);
				$tag = explode(".", $r->post_image);
				$r->post_image = $tag[0] . "-sqthumbnail." . $tag[1];
			}
			
			$queries = DB::getQueryLog();
			Log::debug("singlePostDataForEssays queries: ".print_r($queries,true));
			
			return json_encode($result->toArray()['data']);
		
		} else {
		
			return json_encode(array("result" => "0", "message" => "Missing required parameter"));
			
		}
	
	}
	
	public function getTermsEn($company) {
	

		$pdf = PDF::loadView('terms_en', array('company_sponsoring' => $company));
		

		
		return $pdf->stream('t&c.pdf');
		//return $pdf->download('t&c.pdf');
		


	}
	
	public function getTermsIt($company) {
	
		$pdf = PDF::loadView('terms_ita', array('company_sponsoring' => $company));
		
		return $pdf->stream('t&c.pdf');
        //return $pdf->download('t&c.pdf');
		

	}
	
	public function getTec() {

		/*
        //PDF file is stored under project/public/download/info.pdf
        $file= public_path(). "/T_and_C/T&C buzzmybrand.it.pdf";

		//header("Content-type:application/pdf");

		// It will be called downloaded.pdf
		header("Content-Disposition:attachment;filename='downloaded.pdf'");

		// The PDF source is in original.pdf
		return readfile($file);
		*/
		
		return Redirect::to('/T_and_C/T&C buzzmybrand.it.pdf');

		
	}
	
	
	public function linkify($str)
	{
	$find=array('`((?:https?|ftp)://\S+[[:alnum:]]/?)`si','`((?<!//)(www\.\S+[[:alnum:]]/?))`si');

	$replace=array('<a href="$1" target="_blank">$1</a>', '<a href="http://$1" target="_blank">$1</a>');

	return preg_replace($find,$replace,$str);
	}
	
	public function editContest() {

	
		Log::debug("editContest() Request inputs: " . print_r( Input::all(), true ) );
	
		////////// Security Check ////////////////////////////////////
		if ( Input::get('contest_id') && $negativeResponse = $this->checkAccessContest( Input::get('contest_id') ) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
	
		try {
	
			$contestId = Input::get('contest_id');
			$contest   = null;
			$user      = Auth::user();
            
            if ( $contestId )
                $contest = Contest::find($contestId);
            else 
                $contest = new Contest();
                
            
            if ( !$contestId ) {
				$contest->company_id = $user->company_id;
			}

			$isoFormat = 'Y-m-d\TH:i:s+';
				
			Log::debug("Value of confirmed_by_customer: " . Input::get('confirmed_by_customer'));
			$confirmed_by_customer = Input::get('confirmed_by_customer');
			
			// FIRST STEP
			
			if ( Input::get('contest_service_type') ) {
				$contest->service_type  = Input::get('contest_service_type');
			}
            if ( Input::get('contest_currency') ) {
                $contest->currency  = Input::get('contest_currency');
            }
			if ( Input::get('contest_type') ) {
				$contest->type_id = Input::get('contest_type');
			}
			if ( Input::get('contest_location') ) {
				$contest->location_id = Input::get('contest_location');
			}
            if ( Input::get('use_template') ) {
                $contest->use_template = Input::get('use_template');
            }
			// Objectives
			if ( Input::get('contest_name') ) {
				$contest->name = Input::get('contest_name');
			}
			if ( Input::get('contest_hashtag') ) {		
				$contest->hashtag = Input::get('contest_hashtag');
			}
			Log::debug("contest_auto_start: ".Input::get('contest_auto_start'));
			if ( Input::get('contest_auto_start') == 0 ) {				
				$contest->auto_start = 0;
			} else if ( Input::get('contest_auto_start') == 1 ) {
				$contest->auto_start = 1;
			}
            
			if ( Input::get('contest_start_date') ) {
				$contest->start_time = DateTime::createFromFormat($isoFormat, Input::get('contest_start_date'));
			}
			if ( Input::get('contest_end_date') ) {
				$contest->end_time = DateTime::createFromFormat($isoFormat, Input::get('contest_end_date'));
			}
			/*
			if ( Input::get('contest_score_start_date') ) {
				$contest->score_start_time = DateTime::createFromFormat($isoFormat, Input::get('contest_score_start_date'));
			}
			if ( Input::get('contest_score_end_date') ) {
				$contest->score_end_time = DateTime::createFromFormat($isoFormat, Input::get('contest_score_end_date'));
			}
			*/
			if ( Input::get('contest_concept') ) {
				$contest->concept = nl2br($this->linkify(Input::get('contest_concept')));
			}
			
			// FIRST SAVE
			$contest->save();
			
			///////////////////////////////////
			// Insert objectives after contest save
			$contest_objectives = json_decode(Input::get('contest_objectives'));
			if ( $contest_objectives ) {
							
                foreach ( $contest_objectives as $i => $v ) {
                 $contest_objectives[$i] = intval($v);
                }
                $contest->objectives()->sync($contest_objectives);
			}
			/*
			if ( Input::file('contest_term_cond_on_line_sales') ) {
				$file = Input::file('contest_term_cond_on_line_sales');
				$contest->term_cond_on_line_sales = "ON_" . $contest->id . "_" . $file->getClientOriginalName();
				$file->move(public_path() . '/T_and_C/', $contest->term_cond_on_line_sales);
			}
			if ( Input::file('contest_term_cond_off_line_sales') ) {
				$file = Input::file('contest_term_cond_off_line_sales');
				$contest->term_cond_off_line_sales = "OFF_" . $contest->id . "_" . $file->getClientOriginalName();
				$file->move(public_path() . '/T_and_C/', $contest->term_cond_off_line_sales);
			}
			*/
			/////////////////////////////////////

			if ( Input::get('contest_rewards') ) {

				foreach ( Reward::where("contest_id", $contestId)->get() as $reward ) {
					
					$reward->forceDelete();
				}
				
				$rewards = json_decode( Input::get('contest_rewards') );

				foreach ( $rewards as $v ) {
				
					$reward = new Reward();
					$reward->rank = $v->rank;
					$reward->title = $v->title;
					$reward->description = isset($v->description)? nl2br($this->linkify($v->description)) : "";
					$reward->contest_id = $contest->id;
					
					$reward->save();
				
				}

			}
            // Check no rewards
            if ( Input::get('contest_no_rewards') ) {
                $contest->no_rewards = intval(Input::get('contest_no_rewards'));
            }
			if ( Input::get('contest_how_to_win') ) {
				$contest->how_to_win = nl2br($this->linkify(Input::get('contest_how_to_win')));
			}
			if ( Input::get('contest_win_modality') == 'null') {
				//$contest->win_modality_id = Input::get('contest_win_modality');
			}
			else {
				$contest->win_modality_id = Input::get('contest_win_modality');
			}
			if ( Input::get('contest_template_id')  ) {
				if (Input::get('contest_template_id') == 'null') {
					$contest->template_id = null;
				}
				else
					$contest->template_id = Input::get('contest_template_id');
			}
			
            
			if ( Input::file('contest_watermark') ) {
				
				if ((($contest->sponsor_logo !=null) || ($contest->sponsor_logo != "")) && (file_exists(public_path() . '/images/events/'.$contest->sponsor_logo))) {
					unlink(public_path() . '/images/events/'.$contest->sponsor_logo);
				}
				
				$file = Input::file('contest_watermark');
				//$tag = explode(".", $file->getClientOriginalName());
				$r = rand(1,999);
				$sponsor_logo = md5(time().$r) . ".png";
				$file->move(public_path() . '/images/events/', $sponsor_logo);
			
				$imageFile = public_path() . '/images/events/' . $sponsor_logo;
				$imageObject = imagecreatefrompng($imageFile);
				
				// Scale
				$imageObject = imagescale($imageObject, 100, 100,  IMG_BICUBIC_FIXED);
				
				// New image
				imagepng($imageObject, $imageFile);
				
				/*				
				if ( $tag[1] == "jpg" ) {
					$imageFile = public_path() . '/images/events/' . $sponsor_logo;
					$imageObject = imagecreatefromjpeg($imageFile);
					
					// Scale
					// $imageObject = imagescale($imageObject, 100, 100,  IMG_BICUBIC_FIXED);
					
					// New image
					$sponsor_logo = $tag[0] . ".png";
					$imagePng = public_path() . '/images/events/' . $sponsor_logo;
					imagepng($imageObject, $imagePng);
				} elseif ( $tag[1] == "png" )  {
					$imageFile = public_path() . '/images/events/' . $sponsor_logo;
					$imageObject = imagecreatefrompng($imageFile);
					
					// Scale
					// $imageObject = imagescale($imageObject, 100, 100,  IMG_BICUBIC_FIXED);
					
					// New image
					imagepng($imageObject, $imageFile);
				} elseif ( $tag[1] == "gif" ) {
					$imageFile = public_path() . '/images/events/' . $sponsor_logo;
					$imageObject = imagecreatefromgif($imageFile);
					
					// Scale
					// $imageObject = imagescale($imageObject, 100, 100,  IMG_BICUBIC_FIXED);
					
					// New image
					$sponsor_logo = $tag[0] . ".png";
					$imagePng = public_path() . '/images/events/' . $sponsor_logo;
					imagepng($imageObject, $imagePng);
				}
				*/
				$contest->sponsor_logo = $sponsor_logo;
			
			}
			
			
			/*
			if ( Input::file('contest_image_minisite_sq') ) {
				$file = Input::file('contest_image_minisite_sq');
				
				$tag = explode(".", $file->getClientOriginalName());
				$file->move(public_path() . '/images/events/', $contest->id . "_" . $tag[0]. "_sq." . $tag[1]);
				$contest->image_minisite_sq = $contest->id . "_" . $tag[0] . "_sq." . $tag[1];
				$imageFile = public_path() . '/images/events/' . $contest->image_minisite_sq;
				
				switch ($tag[1]) {
					case "jpg":
						$imageObject = imagecreatefromjpeg($imageFile);
						break;
					case "png":
						$imageObject = imagecreatefrompng($imageFile);
						break;
					case "gif":
						$imageObject = imagecreatefromgif($imageFile);
						break;
				}
				
				// New image
				$imageJpeg = public_path() . '/images/events/' . $tag[0] . ".jpg";
				imagejpeg($imageObject, $imageJpeg);
			} else {
				//$file = file_get_contents(url('/dashboard/assets/images/logo_per_draft_contest.jpg'));
				//$contest->image_minisite_sq = $contest->id . "_flyer_sq.jpg";
				//file_put_contents(public_path() . '/images/events/' . $contest->image_minisite_sq, $file);
			}
			
			$contest->image = $contest->image_minisite_sq;
			*/
			
			
			if ( Input::file('contest_image_minisite_bg') ) {
				
				if ((($contest->image_minisite_bg !=null) || ($contest->image_minisite_bg != "")) && (file_exists(public_path() . '/images/events/'.$contest->image_minisite_bg))) {
					unlink(public_path() . '/images/events/'.$contest->image_minisite_bg);
				}
				
				$file = Input::file('contest_image_minisite_bg');
				$r = rand(1000,2000);				
				$filename=md5(time().$r);				
				$tag = explode(".", $file->getClientOriginalName());
				$file->move(public_path() . '/images/events/', $filename . "." . $tag[1]);
				
				
				
				
				switch ($tag[1]) {
					case "jpg":
						$imageFile = public_path() . '/images/events/' . $filename . "." . $tag[1];
						$imageObject = imagecreatefromjpeg($imageFile);
						break;
					case "png":
						$imageFile = public_path() . '/images/events/' . $filename . "." . $tag[1];
						$imageObject = imagecreatefrompng($imageFile);
						break;
					case "gif":
						$imageFile = public_path() . '/images/events/' . $filename . "." . $tag[1];
						$imageObject = imagecreatefromgif($imageFile);
						break;
				}
				
				// New image
				$imageJpeg = public_path() . '/images/events/' . $filename . "." . $tag[1];
				imagejpeg($imageObject, $imageJpeg);
				
				$contest->image_minisite_bg = $filename . "." . $tag[1];
			}
            
                
			if ( Input::get('template_title_color') ) {
				$contest->title_color = Input::get('template_title_color');
			}
			if ( Input::get('contest_route') && Input::get('contest_route') != 'null' ) {
				$contest->contest_route = Input::get('contest_route');
			}
			if ( Input::get('contest_term_cond_company_sponsoring') )  {
				$contest->term_cond_company_sponsoring = Input::get('contest_term_cond_company_sponsoring');
			}
			
			
			if ( Input::file('contest_term_conditions') ) {
				$file = Input::file('contest_term_conditions');
				$contest->terms_cond = $contest->id . "_" . $file->getClientOriginalName();
				$file->move(public_path() . '/T_and_C/', $contest->terms_cond);
			} else if (Input::get('contest_term_conditions') == '') {
				$contest->terms_cond = '';
			}
			
			
			// SECONDO STEP
			
			if ( intval(Input::get('contest_approval')) > 0 ) {
				$contest->contest_approval  = Input::get('contest_approval');
			}
			
			if ( Input::get('contest_promotion') !== null && Input::get('contest_promotion') !== 'null' ) {
				$contest->promotion_budget = Input::get('contest_promotion');
			}
			/*
			if ( Input::get('contest_influencers_promotion') !== null && Input::get('contest_influencers_promotion') !== 'null'  ) {
				$contest->influencers_promotion = Input::get('contest_influencers_promotion');
			}
			
			if ( Input::get('contest_display_promotion')  !== null && Input::get('contest_display_promotion')  !== 'null' ) {
				$contest->display_promotion  = Input::get('contest_display_promotion');
			}
			
			if ( Input::get('contest_video_seeding_promotion') !== null && Input::get('contest_video_seeding_promotion') !== 'null' ) {
				$contest->video_seeding_promotion = Input::get('contest_video_seeding_promotion');
			}
			
			if ( Input::get('contest_video_lenght') ) {
				$contest->video_length = Input::get('contest_video_lenght');
			}
			
			if ( Input::get('should_insert_contest_watermark') ) {
				$contest->should_insert_contest_watermark  = Input::get('should_insert_contest_watermark');
			}
			if ( Input::get('should_insert_company_watermark') ) {
				$contest->should_insert_company_watermark = Input::get('should_insert_company_watermark');
			}
			*/
								
			
			if ( Input::get('post_visibility') && $fbPage = $contest->fbPages->first()) {
				$fbPage = $contest->fbPages->first();
				
				if (Input::get('post_visibility') == 'YES')
					$fbPage->fb_no_story = 'NO';
				else
					$fbPage->fb_no_story = 'YES';
				
				
				$fbPage->save();
				
				/*
				//update facebook tab
				try 
				{
					
					$request = Request::create('/make-facebook-tab/' . $fbPage->id , 'GET', array());
					Request::replace($request->input());	
					Route::dispatch($request)->getContent();
					*/				

					/*
					$request = Request::create('/update-facebook-tab/' . $fbPage->id , 'GET', array());
					Request::replace($request->input());	
					Route::dispatch($request)->getContent();
					
				} 
				catch (Exception $ex) 
				{
					Log::error('facebookCreationTab in editContest(): '.$ex);
				}
				*/
			}

			$contest->like_value = 1;
			$contest->comment_value = 2;
			$contest->share_value = 2;
			$contest->unique_impression_value = 0.2;	
			$contest->unique_visualization_video = 0.2;	
			$contest->views_youtube = 0.2;				
			
						
			if ( $confirmed_by_customer == 1 ) {
				$contest->confirmed_by_customer = 1;
				$contest->status = 3;
			}
			else {
				$contest->confirmed_by_customer = 0;				
				$contest->status = 4;
			}
			
			$contest->save();

            			
			
			/*
                // Contest Draft/Scheduled
            if ($contest->status == 3 || $contest->status == 4) {
                Mail::send('emails.contest_edit_draft', array('user' => $user, 'contest' => $contest), function($message) use ($user) {
                    $message->to($user->email, $user->name . " " . $user->surname)
                            ->subject(Lang::get('email.contest_edit_draft_subject'));
                });
            }
			*/
        
			return json_encode(array("id" => $contest->id));
		
		} catch (Exception $ex) {
			
			Log::debug("Error on editContest: " . $ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
		
	}
	
	public function contestDescription($id) {
	
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////

	
		$queryBuilder = DB::table('companys')
							->join("contests", "contests.company_id", "=", "companys.id")
							->where("contests.id", "=", $id)
							->whereNull('contests.deleted_at');
							
		$queryBuilder->select(DB::raw('
			companys.id as company_id,
			companys.name as company_name, 
			contests.id as contest_id,
			contests.name as contest_name, 
			contests.image as contest_image,
			contests.type_id as contest_type,
			contests.status as contest_status,
			contests.concept as contest_concept,
			DATE_FORMAT(contests.score_start_time, \'%Y-%m-%dT%TZ\') as contest_start_date,
			DATE_FORMAT(contests.score_end_time, \'%Y-%m-%dT%TZ\') as contest_end_date,
			contests.how_to_win as contest_how_to_win
			'));
			

			
		$result = $queryBuilder->first();
		
		$queryBuilder2 = DB::table('rewards')
			->where("rewards.contest_id", "=", $id)
			->orderBy('rewards.rank', 'asc');
			
		$queryBuilder2->select(DB::raw('
			rewards.rank as reward_rank,
			rewards.title as reward_title,
			rewards.description as reward_description
			'));
		
		$result2 = $queryBuilder2->get();

		
		
		$result->rewards = $result2;
		
		$queries = DB::getQueryLog();
        Log::debug("contest queries: ".print_r($queries,true));
		
		return json_encode($result);
	
	}
	
	public function deleteContest($id) {
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		try {
			$contest = Contest::find($id);
			
			if ( $contest ) {
				$contest->deleted_at = new DateTime('now');
				$contest->save();
				return json_encode(array("result"=> "1"));
			} else {
				return json_encode(array("result"=> "0", "message" => "Contest not found"));
			}
		} catch (Exception $ex){
			Log::debug('Error during deleteContest: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
		}
	}
	
	public function deletePermanentlyContest($id) {
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		try {
			$contest = Contest::find($id);
			
			if ($contest) {
				$twpage_contest = DB::table('twpage_contest')
							->where("contest_id", "=", $id)
							->delete();
				$ytpage_contest = DB::table('ytpage_contest')
							->where("contest_id", "=", $id)
							->delete();
				$fbpage_contest = DB::table('fbpage_contest')
							->where("contest_id", "=", $id)
							->delete();
				$rewards = DB::table('rewards')
							->where("contest_id", "=", $id)
							->delete();
				$contest_objective = DB::table('contest_objective')
							->where("contest_id", "=", $id)
							->delete();
				
				$contest->forceDelete();
				return json_encode(array("result"=> "1"));
			} else {
				return json_encode(array("result"=> "0", "message" => "Contest not found"));
			}
		} catch (Exception $ex){
			Log::debug('Error during deleteContest: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
		}
	}
	
	public function getCompany($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessForCompany($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////

		try {
		
			$queryBuilder = DB::table('companys')
					->join("users", "companys.id", "=", "users.company_id")
					->where("companys.id", "=", $id)
					->whereNull('companys.deleted_at');
					
			$queryBuilder->select(DB::raw('
				companys.id as company_id,
				companys.name as company_name, 
				companys.registration_number as company_registration_number,
				companys.company_link as company_website,
				users.id as user_id,
				users.name as user_name,
				users.surname as user_surname,
				users.job_title as user_job_title,
				users.country as user_country,
				users.phone as user_phone,
				users.email as user_email,
				users.address1 as user_address1,
				users.address2 as user_address2,
				users.district as user_district,
				users.cap as user_cap,
				users.fiscal_code as user_fiscalcode
			'));
			
			$result = $queryBuilder->first();
			
			$queries = DB::getQueryLog();
			Log::debug("contest queries: ".print_r($queries,true));
			
			return json_encode($result);
		
		} catch (Exception $ex){
			
			Log::debug('Error during getCompany: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
				
		}
		
	}
	
	public function resendPassword() {
	
		$userEmail = Input::get("email");
		$language = Input::get("language");
		
		if ( $userEmail && $language ) {
		
			try {
			
				$passwordTemp = $this->generateRandomString();
				Log::debug('Temp pwd: '.$passwordTemp);
				$user = User::where('email', $userEmail)->first();
				$user->password = Hash::make($passwordTemp);
				$user->save();

			} catch (Exception $ex){
					
				Log::debug('user save: '.$ex);
				return json_encode(array("result"=> "0", "message" => "Error resending password"));
						
			}	
			
			App::setLocale($language);
			
			try {
			
				Mail::send('emails.resend_password', array('user' => $user, 'password_temp' => $passwordTemp), function($message) use ($user) {
					
					$message->to($user->email, $user->name . " " . $user->surname)
							->subject(Lang::get('email.resend_password_object'));
				
				});
			
			} catch (Exception $ex) {
				Log::error('Problem sending email: '.$ex);
			}
			
			return json_encode(array("result"=> "1"));
		
		} else {
			
			return json_encode(array("result"=> "0", "message" => "Missing parameters"));
			
		}
	
	}
	
	

	public function newCompany() {

		$language = Input::get("language");
		$company_name = Input::get("company_name");
		
		try {
			
			$email_available = json_decode($this->checkAvEmail(false, Input::get('user_email')), true);	
			
			if ($email_available['result']) {
				//$user->email = Input::get('user_email');
			} else {
				return json_encode($email_available);
			} 
			
		} catch (Exception $ex) {
			Log::error('newCompany-User: '.$ex);
			return json_encode(array("result"=> "0", "message" => "ERROR DURING REGISTRATION " . $ex)); 
		}
		if ( $language &&  $company_name  ) {
		
			DB::beginTransaction();
			
			try {
		
				// SAVE COMPANY
				$company = new Company();
				$company->fbApp_clientID = Config::get("facebook")["appId"];
				$company->fbApp_clientSecret = Config::get("facebook")["secret"];
				$company->app_accesstoken = Config::get("facebook")["appToken"];
				//$company->fbApp_clientID = "1722374491338195";
				//$company->fbApp_clientSecret = "87707f5b70d19c25dbdd841f681d6fa5";
				//$company->app_accesstoken = "1722374491338195|dZiqn6FuxVYm7M9IErUbcIl-Y10"; 
				$company->type_id = Company::TYPE_INTERNAL;
				if ( Input::get('company_name') ) {
					$company->name = Input::get('company_name');
				}
				$company->save();
				
				
				// SAVE USER
				$user = new User();
				$user->rand_key = $this->generateRandomString();
				$user->company_id = $company->id;
				if ( Input::get('user_name') ) {
					$user->name = Input::get('user_name');
				}
				if ( Input::get('user_surname') ) {
					$user->surname = Input::get('user_surname');
				}
				if ( Input::get('user_job_title') ) {
					$user->job_title = Input::get('user_job_title');
				}
				if ( Input::get('user_country') ) {
					$user->country = Input::get('user_country');
				}
				if ( Input::get('user_phone') ) {
					$user->phone = Input::get('user_phone');
				}
				if ( Input::get('user_email') ) {
					$user->email = Input::get('user_email');
				}
				if ( Input::get('user_password') ) {
					$user->password = Hash::make(Input::get('user_password'));
				}
				if ( Input::get('timezone_offset') ) {
					$user->timezone_offset = Input::get('timezone_offset');
				}
				$user->save();
				
			} catch (Exception $ex) {
				
				DB::rollback();
				Log::debug('Error during newCompany: '.$ex);
				
				if ( strpos($ex, "1062 Duplicate entry") && strpos($ex, "unique_email") ) {
					return json_encode(array("result"=> "0", "message" => "THERE IS ALREADY A USER WITH THIS E-MAIL ADDRESS. PLEASE USE ANOTHER E-MAIL TO REGISTER"));
				}
				
				return json_encode(array("result"=> "0", "message" => "ERROR DURING REGISTRATION " . $ex)); 
					
			}
			
			DB::commit();
			
			try {
			
				if ( Input::get('newsletter') != 'false' ) {
				
					// REGISTER NEWSLETTER ON CONTACTS
					$name = "";
					$email = "";
					$telephone = "";
			
					if ( Input::get('user_name') ) {
						$name .= Input::get('user_name');
					}
					if ( Input::get('user_surname') ) {
						
						if ( $name == "") {
							$name .= Input::get('user_surname');
						} else {
							$name .= " " . Input::get('user_surname');
						}
						
					}
					if( Input::get('user_email') ) {
						$email = Input::get('user_email');
					}
					
					if ( Input::get('user_phone') ) {
						$telephone = Input::get('user_phone');
					}
					
					$contact = new Contact();
					$contact->name = $name;
					$contact->email = $email;
					$contact->telephone = $telephone;
					$contact->newsletter = 1;
					$contact->save();
					
					// REGISTER NEWSLETTER ON MAILCHIMP
					
					
					if ($language == 'it')
						$list_id = 'c37ced4e70';
					else
						$list_id = '5b5711f60c';
					
					//MailchimpWrapper::lists()->subscribe($list_id, array('email'=>$email));
					MailchimpWrapper::lists()->subscribe($list_id, array('email'=>$email),  array('FirstName' => Input::get('user_name'), 'LastName' => Input::get('user_surname')));
					Log::debug("Mail aggiunta in lista");
				}
			
			} catch (Exception $ex) {
				
				Log::debug('Error during newCompany: '.$ex);
					
			}
			
			
			// LOGIN
			try {
				Auth::login($user);
			} catch (Exception $ex){
				Log::debug('Error during login: '.$ex);	
			}
			
			
			// SEND WELCOME EMAIL
			try {
				App::setLocale($language);
				Mail::send('emails.welcome-email', array('user' => $user, 'company' => $company), function($message) use ($user) {
					
					$message->to($user->email, $user->name . " " . $user->surname)
							->subject(Lang::get('email.welcome_email_object'));
				
				});
				
				Mail::send('emails.admin-notify-email', array('user' => $user, 'company' => $company), function($message) use ($user) {
					$message->to("support@buzzmybrand.com", $user->name . " " . $user->surname)
									->subject(Lang::get('email.admin_notify_email_object'));
				});
			} catch (Exception $ex) {
				Log::error('Problem sending email: '.$ex);
			}
			
			
			return json_encode(array("result" => "1"));
		
		
		} else {
		
			return json_encode(array("result"=> "0", "message" => "Missing parameters"));
		} 

		
		
	}
	
	
	
	
	
	
	
    
    public function editCompany() {
    
        ////////// Security Check ////////////////////////////////////
        if ( Input::get("company_id") && $negativeResponse = $this->checkAccessForCompany( Input::get("company_id") ) ) {
            return $negativeResponse;
        }
        //////////////////////////////////////////////////////////////
    
        try {
            
            $id = Input::get("company_id");

            if ( $id ) {
                $company = Company::find($id);
                $user = User::where("company_id", "=", $id)->first();
                /*
                if ( $company->id != Auth::user()->company_id ) {
                    return json_encode(array("result"=> "0", "message" => "You have not edit permission"));
                }
                */

            } elseif ($id == 0) {
                $user = User::where("email", "=", "info@buzzmybrand.it")->first();

            } else {
                return json_encode(array("result"=> "0", "message" => "id required"));
            }
            
            if ($id != 0) {
                if ( Input::get('company_name') ) {
                    $company->name = Input::get('company_name');
                }
                if ( Input::get('company_registration_number') ) {
                    $company->registration_number = Input::get('company_registration_number');
                }
                if ( Input::get('company_website') ) {
                    $company->company_link = Input::get('company_website');
                }
                
                $company->save();
                $user->company_id = $company->id;
            }
            
            if ( Input::get('user_name') ) {
                $user->name = Input::get('user_name');
            }
            if ( Input::get('user_surname') ) {
                $user->surname = Input::get('user_surname');
            }
            if ( Input::get('user_job_title') ) {
                $user->job_title = Input::get('user_job_title');
            }
            if ( Input::get('user_country') ) {
                $user->country = Input::get('user_country');
            }
            if ( Input::get('user_phone') ) {
                $user->phone = Input::get('user_phone');
            }


			if (( Input::get('user_email') ) && ($id != 0)) {
				$email_available = json_decode($this->checkAvEmail($user->id, Input::get('user_email')), true);
				
				if ($email_available['result']) {
					$user->email = Input::get('user_email');
				} else {
					return json_encode($email_available);
				}
			}
			else
			{
				$user->email = "info@buzzmybrand.it";
			}
			if ( Input::get('user_password') ) {
				if ( Hash::check(Input::get('actual_user_password'), $user->password) ) {
					$user->password = Hash::make(Input::get('user_password'));
				} else {
					return json_encode(array("result"=> "0", "message" => "Wrong actual password"));
				}
			}
			if ( Input::get('user_address1') ) {
				$user->address1 = Input::get('user_address1');
			}
			if ( Input::get('user_address2') ) {
				$user->address2 = Input::get('user_address2');
			}
			if ( Input::get('user_district') ) {
				$user->district = Input::get('user_district');
			}
			if ( Input::get('user_cap') ) {
				$user->cap = Input::get('user_cap');
			}
			if ( Input::get('user_fiscalcode') ) {
				$user->fiscal_code = Input::get('user_fiscalcode');
			}

			$user->save();
			

				
				
			return json_encode(array("result" => "1"));
			
		
		} catch (Exception $ex){
			
			Log::debug('Error during editCompany: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
				
		}
		
	}
	
	private function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	
	public function sendWelcomeEmail($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessUser( $id ) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		Log::debug("sendWelcomeEmail");
	
		try {
		
			$language = Input::get('language');
		
			if ( $language ) {
		
				$user = User::find($id);
				$user->rand_key = $this->generateRandomString();
				$user->save();

			
				$company = Company::find($user->company_id);
				
				App::setLocale($language);
			
				try {
			
					Mail::send('emails.welcome-email', array('user' => $user, 'company' => $company), function($message) use ($user) {
						
						$message->to($user->email, $user->name . " " . $user->surname)
								->subject((string) Lang::get('email.welcome_email_object'));
					
					});
				
				} catch (Exception $ex) {
					Log::error('Error during sendWelcomeEmail: '.$ex);
					return json_encode(array("result"=> "0", "message" => "".$ex));
				}
				
				return json_encode(array("result" => "1"));
			
			} else {
			
				return json_encode(array("result"=> "0", "message" => "Missing parameters"));
			
			}
		
		} catch (Exception $ex) {
			Log::error('Error during sendWelcomeEmail: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
		}
	
	}
	
	public function notifyEmailBudget() {
		try {
			$user = User::find($_POST['id']);
			$company = Company::find($user->company_id);
			
			Mail::send('emails.admin-notify-email-budget', array('user' => $user, 'company' => $company), function($message) use ($user) {
				$message->to("support@buzzmybrand.com", $user->name . " " . $user->surname)
						->subject(Lang::get('email.admin_notify_email_budget_object'));
				// vin@buzzmybrand.it is admin
			});
			
		} catch (Exception $ex) {
			Log::error('Problem sending email: '.$ex);
		}
	}
	
	public function verifyEmail($id, $rndString) {
	
		try {
		
			$user = User::find($id);
			
			if ( $user->rand_key === $rndString ) {
				$user->email_confirmed = 1;
				$user->save();
				
				
				try {
					Auth::login($user);
				} catch (Exception $ex){
					
					Log::debug('Error during login: '.$ex);
					return json_encode(array("result"=> "0", "message" => "".$ex));
						
				}
				
				
				return Redirect::to('/dashboard/index.html#/launch?welcome');
			}
			
			
			//return json_encode(array("result" => "1"));
			
		} catch (Exception $ex) {
			Log::error('verifyEmail: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
		}
	
	}
	
    public function facebookCustomerLogin($id) {
		
		Log::debug('facebookCustomerLogin start()');
	
        ////////// Security Check ////////////////////////////////////
        if ( $negativeResponse = $this->checkAccessContest($id) ) {
            return $negativeResponse;
        }
        //////////////////////////////////////////////////////////////
    
        
        
        $contest = Contest::find($id);
        /*
        $facebook = new Facebook(array(
             'appId' => $contest->company->fbApp_clientID,
             'secret' => $contest->company->fbApp_clientSecret
            //'appId' => "1722374491338195",
            //'secret' => "87707f5b70d19c25dbdd841f681d6fa5"
        ));
        */
        
        $facebook = new Facebook\Facebook([
                'app_id' => $contest->company->fbApp_clientID,
                'app_secret' => $contest->company->fbApp_clientSecret,
                'default_graph_version' => 'v2.4',
            ]);
    
        $redirect_url = asset('/admin/customer-facebook-pages/' . $id);
        $permissions = 'user_friends,email,manage_pages,publish_actions,public_profile,publish_pages';
    
        /*
        $params = array(
            'redirect_uri' => $redirect_url,
            'display' => 'popup',
            'scope' => $permissions,
         );
         */
         
         $loginUrl = "https://www.facebook.com/dialog/oauth?"
        ."client_id=".$contest->company->fbApp_clientID
        ."&scope=".$permissions
        ."&response_type=code&redirect_uri=".$redirect_url;
        
        return Redirect::to($loginUrl);
        
        //return Redirect::to($facebook->getLoginUrl($params));
    
    }
    
	public function facebookCustomerLogout($id) {

        ////////// Security Check ////////////////////////////////////
        if ($negativeResponse = $this->checkAccessContest($id)) {
            return $negativeResponse;
        }
        //////////////////////////////////////////////////////////////


        
    }

    public function customerFacebookPages($id) {

        ////////// Security Check ////////////////////////////////////
        if ($negativeResponse = $this->checkAccessContest($id)) {
            return $negativeResponse;
        }
        //////////////////////////////////////////////////////////////
        
        try {
			$redirect_url = asset('/admin/customer-facebook-pages/' . $id);
			
			$code = Input::get('code');
		
			$contest = Contest::find($id);

			$facebook = new Facebook\Facebook([
                'app_id' => $contest->company->fbApp_clientID,
                'app_secret' => $contest->company->fbApp_clientSecret,
                'default_graph_version' => 'v2.4',
            ]);
			
			$curl = curl_init();
			// Set some options - we are passing in a useragent too here
			curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => 'https://graph.facebook.com/oauth/access_token?client_id='
					.$contest->company->fbApp_clientID.'&client_secret='
					.$contest->company->fbApp_clientSecret.'&redirect_uri='
					.$redirect_url.'&code='
					.$code
			));
			// Send the request & save response to $resp
			$resp = curl_exec($curl);
					
			// Close request to clear up some resources
			curl_close($curl);
			
			parse_str($resp,$ar);
			$accessToken=$ar['access_token'];
			
			//TODO: devo avere anche l'access token 
			//$accessToken = $facebook->getDefaultAccessToken();

            $requestProfile = $facebook->get('/me', $accessToken);
            $profile = $requestProfile->getGraphUser();

            $requestPicture = $facebook->get('/me/picture?redirect=false&height=30', $accessToken);
            $picture = $requestPicture->getGraphUser();

            $logoutUrl = 'https://www.facebook.com/logout.php?next='.url('/').'/admin/facebook-customer-login/'.$id.'&access_token='.$accessToken;

            $requestAccounts = $facebook->get('/me/accounts', $accessToken);
            $pages = $requestAccounts->getGraphEdge();
            
            $user['name'] = $profile->getName();
            $user['profile_image'] = $picture['url'];
            
            $accounts = array(); 
            foreach( $pages as $page ) {
                $array_perm = json_decode($page['perms'], true);             
                if ( in_array('ADMINISTER', $array_perm) ) {
                    $requestPagePicture = $facebook->get('/'.$page['id'].'/picture?redirect=0&type=large', $accessToken);
                    $pagePicture        = $requestPagePicture->getGraphObject();

                    $accounts[$page['id']]['name']    = $page['name'];
					$accounts[$page['id']]['picture'] = $pagePicture['url'];
				}
			}
			
			return View::make('admin.customer-facebook-pages', ['user' => $user, 'accounts' => $accounts, 'access_token' => $accessToken, 'logoutUrl' => $logoutUrl])->with('contest_id', $id) ;
		
		} catch (Exception $ex) {
		
			Log::error('customerFacebookPages: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
		
	}
	
	public function facebookCustomerConnect($id) {
	
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		try {
	
			$contest = Contest::find($id);
			
			$adminToken = Input::get("admin_token");
			$pageId = Input::get("page_id");
			$pageImageUrl = Input::get("page_image");
			
			if ( $adminToken && $pageId ) {
			
			
				// delete for all istances of fb_page_contest
				$contest->fbPages()->detach();



				$facebook = new Facebook\Facebook([
					'app_id' => $contest->company->fbApp_clientID,
					'app_secret' => $contest->company->fbApp_clientSecret,
					'default_graph_version' => 'v2.4',
				]);
				
				//$params = array("access_token" => $adminToken);
				$response = $facebook->get("/" . $pageId ."?fields=name,link", $adminToken);
				$graphNodeResponse = $response->getGraphNode();
				//Log::debug("Facebook response: ".print_r($response->getBody(),true));
				//var_dump($graphNodeResponse);
				//exit;
				
				// Update or create of fbPage
				$fbPage = fbPage::where("fb_page_id", "=", $pageId)->first();
				if ( $fbPage ) {
					
				} else {
					$fbPage = new fbPage();
					$fbPage->fb_page_id = $graphNodeResponse['id'];
				}
				
				$fbPage->fb_page_name = $graphNodeResponse['name'];
				$fbPage->fb_page_link = $graphNodeResponse['link'];
				$fbPage->fb_page_admin_at = $adminToken;
				
				//$imageFilePath = Website_WidgetController::downloadAndSaveFacebookProfileImage($fbPage, $adminToken);
				//Scheduler::downloadFile($pageImageUrl, asset('/images/users/'.$graphNodeResponse['id']));
				$image = file_get_contents($pageImageUrl);
				file_put_contents(public_path('/images/users/'.$graphNodeResponse['id']), $image); //Where to save the image on your server
				
				$fbPage->save();

				//Create new relationship fb_page_contest
				$contest->fbPages()->attach($fbPage->id); 
				
				
				/*
				try {
					$request = Request::create('/make-facebook-tab/' . $fbPage->id , 'GET', array());
					Request::replace($request->input());	
					Route::dispatch($request)->getContent();	

					$request = Request::create('/update-facebook-tab/' . $fbPage->id , 'GET', array());
					Request::replace($request->input());	
					Route::dispatch($request)->getContent();
				} catch (Exception $ex) {
					Log::error('facebookCustomerConnect: '.$ex);
				}
				*/
				
				
                Cookie::make($id . "_facebook", 1, 5);
				return json_encode(array("result"=> "1", "page" => $graphNodeResponse["name"], "image" => $graphNodeResponse['id']));
			
			} else {
				return json_encode(array("result"=> "0", "message" => "Missing required parameters"));
			}
		
		} catch (Exception $ex) {
		
			Log::error('facebookCustomerConnect: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
	
	}
	
	public function facebookCustomerDisconnect($id) {
		
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////

		//Log::debug("facebookCustomerDisconnect ");
	
		try {
		
			$contest = Contest::find($id);
			//ora devo trovare la pagina fb del contest dato in input
			$fbpage_details = DB::table('fbpage_contest')
			->join("contests", "contests.id", "=", "fbpage_contest.contest_id")
			->where("fbpage_contest.contest_id", "=", $id)
			->orderBy('contests.id', 'desc')->first();
			
			$fbPage = fbPage::find($fbpage_details->fbpage_id);
						
			//devo eliminare il tab dalla pagina FB
			try {
				
				$request = Request::create('/delete-facebook-tab/' . $fbPage->id , 'GET', array());
				Request::replace($request->input());	
				Route::dispatch($request)->getContent();
				
			} catch (Exception $ex) {
				Log::error('facebookCustomerDisconnect: '.$ex);
			}
		
			// delete for all istances of fb_page_contest
			$contest->fbPages()->detach();	
			
			return json_encode(array("result"=> "1"));
		
		} catch (Exception $ex) {
		
			Log::error('facebookCustomerDisconnect: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
	
	}
	
	
	public function twitterCustomerLogin($id) {
	
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////

		Log::debug('twitterCustomerLogin start()');
	
		$redirect_url = '/admin/twitter-customer-connect/' . $id;
		
		// The TwitterOAuth instance
		$twitteroauth = new TwitterOAuth(Config::get("twitter")["consumer_key"], Config::get("twitter")["consumer_secret"]);
		// Requesting authentication tokens, the parameter is the URL we will be redirected to
		$request_token = $twitteroauth->getRequestToken(asset($redirect_url));
		//$request_token = $twitteroauth->getRequestToken(asset('/website/widget/twitter-test/'.$id));
		 
		// Saving them into the session
		Session::put('twitter_oauth_token', $request_token['oauth_token']);
		Session::put('twitter_oauth_token_secret', $request_token['oauth_token_secret']);

		
		
		// If everything goes well..
		if($twitteroauth->http_code==200){
			// Let's generate the URL and redirect
			$url = $twitteroauth->getAuthorizeURL($request_token['oauth_token'], true);
			
			return Redirect::to($url);
		} else {
			// It's a bad idea to kill the script, but we've got to know when there's an error.
			die('Something wrong happened.');
		}
		
	
	}
	
	public function twitterCustomerConnect($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		try {
	
			$contest = Contest::find($id);
	
			// delete for all istances of fb_page_contest
			$contest->twPages()->detach();
		
			$oauthVerifier = Input::get('oauth_verifier');
			$twitteroauth = new TwitterOAuth(Config::get("twitter")["consumer_key"], Config::get("twitter")["consumer_secret"],  Session::get('twitter_oauth_token'), Session::get('twitter_oauth_token_secret'));
			$access_token = $twitteroauth->getAccessToken($oauthVerifier);
			$user_info = $twitteroauth->get('account/verify_credentials');

			//print_r($user_info); return;
			
			
			// Update or create of twPage
			$twPage = twPage::where("tw_page_id", "=", $user_info->id)->first();
			if ( $twPage ) {
				
			} else {
				$twPage = new twPage();
				$twPage->tw_page_id  = $user_info->id;
			}
			
			$twPage->tw_page_name = $user_info->name;
			$twPage->tw_screen_name = $user_info->screen_name;
			$twPage->tw_page_link = "https://twitter.com/" . $user_info->screen_name;
			$twPage->tw_page_admin_at = $access_token['oauth_token'];
			$twPage->tw_page_secret_token = $access_token['oauth_token_secret'];
			
			$fp = tmpfile();
			$tmpFilename = stream_get_meta_data($fp)['uri'];
			$tmpFilenameName = md5($tmpFilename) . ".jpg";
			
            //Noonic debug
			$content = file_get_contents($user_info->profile_image_url);
			file_put_contents('images/users/'.$tmpFilenameName,  $content);
			
			$twPage->image = $tmpFilenameName ;
			$twPage->save();
			
			//Create new relationship fb_page_contest
			$contest->twPages()->attach($twPage->id); 

            Cookie::make($id . "_twitter", 1, 5);

            return View::make('admin.customer-social-connect', [
                'social'  => 'twitter',
                'result'  => 1,
                'message' => '',
                'page'    => $user_info->name,
                'image'   => $tmpFilenameName
            ]);

			return json_encode(array("result"=> "1", "page" => $user_info->name, "image" => $tmpFilenameName));
		
		} catch (Exception $ex) {
		
			Log::error('twitterCustomerConnect: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}

	
	}
	
	
	public function twitterCustomerDisconnect($id) {
		
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		try {
		
			$contest = Contest::find($id);
		
			// delete for all istances of fb_page_contest
			$contest->twPages()->detach();	
			
			return json_encode(array("result"=> "1"));
		
		} catch (Exception $ex) {
		
			Log::error('twitterCustomerDisconnect: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
	
	}
	
	public function facebookGhostPost($id) {
	
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		
		try {
		
			$value = Input::get("value");

		
			if ( isset($value) ) {
				
				$contest = Contest::find($id);
			
				if ( $value == 1 ) {

			
					foreach ($contest->fbPages as $fbPage) {
						
						$fbPage->fb_no_story = "YES";
						$fbPage->save();
					}
				
				} else if ( $value == 0 ) {

				
					foreach ($contest->fbPages as $fbPage) {

						$fbPage->fb_no_story = "NO";
						$fbPage->save();
					}
						
				}
				
			
				return json_encode(array("result"=> "1"));
		
			} else {
				
				return json_encode(array("result"=> "0", "message" => "Missing required parameters"));
				
			}
		
		} catch (Exception $ex) {
		
			Log::error('facebookGhostPost: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
		
	}
	
	public function youtubeCustomerLogin($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		$client = new Google_Client();
		if (Request::root() == "https://www.buzzmybrand.com")
			$client->setAuthConfigFile('../app/config/client_secret_460613892338.apps.googleusercontent.com.json');
		//$client->setAuthConfigFile('../app/config/client_secret_86087114127-a75sqeo6hoshb2dfrpgsut08a4moas6n.apps.googleusercontent.com.json');
		else
			$client->setAuthConfigFile('../app/config/client_secret_460613892338.apps.googleusercontent.com_co.json');
		$client->addScope('https://www.googleapis.com/auth/youtube');
		$client->addScope('https://www.googleapis.com/auth/plus.login');
		//$client->setRedirectUri(Request::root().'/admin/youtube-customer-connect');
		$client->setAccessType('offline');
		$client->setApprovalPrompt('force');
		$client->setState(strtr(base64_encode("".$id), '+/=', '-_,'));
		
		return Redirect::to($client->createAuthUrl());
		
	}
	
	public function youtubeCustomerConnect() {
	
		////////// Security Check ////////////////////////////////////
		if ( isset($_GET['state']) ) {
			$contestId = base64_decode(strtr($_GET['state'], '-_,', '+/='));
			if ( $contestId && $negativeResponse = $this->checkAccessContest($contestId) ) {
				return $negativeResponse;
			}
		}
		//////////////////////////////////////////////////////////////
	
		try {
	
	
			$client = new Google_Client();
			if (Request::root() == "https://www.buzzmybrand.com")
				$client->setAuthConfigFile('../app/config/client_secret_460613892338.apps.googleusercontent.com.json');
			//$client->setAuthConfigFile('../app/config/client_secret_86087114127-a75sqeo6hoshb2dfrpgsut08a4moas6n.apps.googleusercontent.com.json');
			else if (Request::root() == "https://www.buzzmybrand.co")
				$client->setAuthConfigFile('../app/config/client_secret_460613892338.apps.googleusercontent.com_co.json');
			else
				$client->setAuthConfigFile('../app/config/client_secret_460613892338.apps.googleusercontent.com_it.json');
			//$client->setAuthConfigFile('../app/config/client_secret_86087114127-a75sqeo6hoshb2dfrpgsut08a4moas6n.apps.googleusercontent.com.json');
			$client->addScope('https://www.googleapis.com/auth/youtube');
			$client->addScope('https://www.googleapis.com/auth/plus.login');
			$client->setAccessType('offline');
			$client->setApprovalPrompt('force');
			if (isset($_GET['code'])) {
				$client->authenticate($_GET['code']);
				$token = $client->getAccessToken();
				$authObj = json_decode($token);
				
				//print_r($authObj);
				$plus = new \Google_Service_Plus($client);
				$person = $plus->people->get('me');
				//print_r($person);
				//return;
				//echo base64_decode(strtr($_GET['state'], '-_,', '+/='));
				
				$contest = Contest::find(base64_decode(strtr($_GET['state'], '-_,', '+/=')));
				$contest->ytPages()->detach();
				
				// Update or create of ytPage
				$ytPage = ytPage::where("person_id", "=", $person->id)->first();
				if ( $ytPage ) {
					
				} else {
					$ytPage = new ytPage();
					$ytPage->person_id  = $person->id;					
				}
				$ytPage->yt_display_name = $person->displayName;
				$ytPage->yt_access_token = $authObj->access_token;
				$ytPage->yt_token_type = $authObj->token_type;
				$ytPage->yt_token_expires_ts = new DateTime();
				$ytPage->yt_token_expires_ts->add(new DateInterval("PT".$authObj->expires_in."S"));
				$ytPage->yt_refresh_token = $authObj->refresh_token;

				$fp = tmpfile();
				$tmpFilename = stream_get_meta_data($fp)['uri'];
				$tmpFilenameName = md5($tmpFilename) . ".jpg";
				
				$content = file_get_contents($person->image['url']);
				file_put_contents('images/users/'.$tmpFilenameName,  $content);
				
				$ytPage->image = $tmpFilenameName ;
				
				$ytPage->save();
				
				//Create new relationship fb_page_contest
				$contest->ytPages()->attach($ytPage->id); 

                Cookie::make($contestId . "_youtube", 1, 5);

				return View::make('admin.customer-social-connect', [
               				 'social' => 'youtube',
			                'result' => 1,
			                'message' => '',
							'page'    => $person->displayName,
							'image'   => $tmpFilenameName
		            	]);
				
				return json_encode(array("result"=> "1"));
			} else {
				return View::make('admin.customer-social-connect', [
               				 'social' => 'youtube',
			                'result' => 0,
			                'message' => ''
		            	]);
			}
		
		} catch (Exception $ex) {
		
			Log::error('youtubeCustomerConnect: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
		

	}
	
	
	public function youtubeCustomerDisconnect($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		try {
		
			$contest = Contest::find($id);
		
			// delete for all istances of fb_page_contest
			$contest->ytPages()->detach();	
			
			return json_encode(array("result"=> "1"));
		
		} catch (Exception $ex) {
		
			Log::error('youtubeCustomerDisconnect: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
	
	}
	
	
	public function saveDashboardImage() {
	
		////////// Security Check ////////////////////////////////////
		if ( Input::get("company_id") && $negativeResponse = $this->checkAccessForCompany( Input::get("company_id") ) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		$file = Input::file('image');
		$companyId = Input::get('company_id');
		
		if ( $file && $companyId ) {
		
			try {
		
				$company = Company::find($companyId);	
				$company->dashboard_image = $company->id . "_" . $file->getClientOriginalName();
				$file->move(public_path() . '/images/users/', $company->dashboard_image);
				$company->save();
		
			} catch (Exception $ex) {
		
				Log::error('saveDashboardImage: '.$ex);
				return json_encode(array("result"=> "0", "message" => "".$ex));
			
			}
		
			return json_encode(array("result"=> "1"));
		
		} else {
				
			return json_encode(array("result"=> "0", "message" => "Missing required parameters"));
				
		}
	
	}
	
	public function approvePhoto($id) {
	
		
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccesPhoto($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		
	
		$request = Request::create('/admin/photo/' . $id . '/approve', 'POST', array(
																					'approvalStep' => Input::get('approvalStep'), 
																				'api_call' => 1,));
		Request::replace($request->input());
		return Route::dispatch($request)->getContent();
		
	}
	
	public function approveVideo($id) {
			
		
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccesVideo($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		
		$request = Request::create('/admin/video/' . $id . '/approve', 'POST', array(
																		'approvalStep' => Input::get('approvalStep'), 
																		'api_call' => 1,));
		Request::replace($request->input());
		return Route::dispatch($request)->getContent();
	
	}
	
	public function approveEssay($id) {
			
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccesEssay($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
			
		$request = Request::create('/admin/essay/' . $id . '/approve', 'POST', array(
																		'approvalStep' => Input::get('approvalStep'), 
																		'api_call' => 1,));
		Request::replace($request->input());
		return Route::dispatch($request)->getContent();
	
	}

	
	
	public function deletePhoto($id) {
	
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccesPhoto($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		$request = Request::create('/admin/delete-photo/' . $id, 'POST', array('api_call' => 1));
		Request::replace($request->input());
		return Route::dispatch($request)->getContent();
	
	}
	
	public function deleteVideo($id) {

	
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccesVideo($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		
		$request = Request::create('/admin/delete-video/' . $id, 'POST', array('api_call' => 1));
		Request::replace($request->input());
		return Route::dispatch($request)->getContent();
	}
	
	public function deleteEssay($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccesEssay($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		
		$request = Request::create('/admin/delete-essay/' . $id, 'POST', array('api_call' => 1));
		Request::replace($request->input());
		return Route::dispatch($request)->getContent();
	}
	
	public function choosePromotion($id) {
	
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		$influencersPromotion = Input::get('influencers_promotion');
		$displayPromotion = Input::get('display_promotion');
		$videoSeedingPromotion = Input::get('video_seeding_promotion');
		$promotionBudget = Input::get('promotion_budget');
		
		$contest = Contest::find($id);
		if ( !$influencersPromotion && !$displayPromotion && !$videoSeedingPromotion ) {
			return json_encode(array("result"=> "0", "message" => "Choose at least one promotion"));
		}
		
		if ( $promotionBudget <= 0 ) {
			return json_encode(array("result"=> "0", "message" => "Budget 0"));
		}
		
		$startDate = DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time);
		$now = new DateTime('now');
		
		if ( $now >= $startDate ) {
			return json_encode(array("result"=> "0", "message" => "Edit not permitted"));
		}
		
		try {
		
			
		
			if ( $influencersPromotion ) {
				$contest->influencers_promotion = $influencersPromotion;
			}
			
			if ( $displayPromotion ) {
				$contest->display_promotion = $displayPromotion;
			}
			
			if ( $videoSeedingPromotion ) {
				$contest->video_seeding_promotion = $videoSeedingPromotion;
			}
			
			$contest->promotion_budget = $promotionBudget;
			
			
			$contest->save();
			
			try {
			
				App::setLocale($contest->location->language);
				Mail::send('emails.promotion_change_notification', array('contest' => $contest), function($message) {
					
					$message->to("info@buzzmybrand.it", "Buzzmybrand")
							->subject(Lang::get('email.promotion_change_notification_object'));
				
				});
			
			} catch (Exception $ex) {
				Log::error('Problem sending email: '.$ex);
			}
			
			return json_encode(array("result"=> "1"));
		
				
		} catch (Exception $ex) {
		
			Log::error('choosePromotion: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
		
	}
	
	public function changePromotionProposal($id) {
	
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		
		Log::debug("changePromotionProposal()");
		
		try {
		
			if ( Auth::user()->isSuperAdmin() ) {
			
				$activatedInfluencer = Input::get('activated_influencer');
				$estimatedPostReach = Input::get('estimated_post_reach ');
				$estimatedNewLeads = Input::get('estimated_new_leads');
				$estimatedDownloads = Input::get('estimated_downloads');
				
				
				
				$contest = Contest::find($id);
				$company = Company::find($contest->company_id);
				$customer = User::where('company_id', '=', $company->id)->first();

				
				if ( $activatedInfluencer ) {
					
					if ( $contest->influencers_promotion ) {
						$contest->activated_influencer = $activatedInfluencer;
					} else {
						return json_encode(array("result"=> "0", "message" => "You can't edit activated influencers"));
					}
					
				}
				
				if ( $estimatedPostReach ) {
					
					$contest->estimated_post_reach = $estimatedPostReach;
					
				}
				
				if ( $estimatedNewLeads ) {
					
					if ( $contest->objective_id == 4 || $contest->objective_id == 6 ) {
						$contest->estimated_new_leads = $estimatedNewLeads;
					} else {
						return json_encode(array("result"=> "0", "message" => "You can't edit estimated new leads"));
					}
					
				}
				
				if ( $estimatedDownloads ) {
					
					if ( $contest->objective_id == 7 ) {
						$contest->estimated_downloads = $estimatedDownloads;
					} else {
						return json_encode(array("result"=> "0", "message" => "You can't edit estimated downloads"));
					}
					
				}
				
				if ( $activatedInfluencer || $estimatedPostReach || $estimatedNewLeads || $estimatedDownloads ) {

				
					App::setLocale($contest->location->language);
				
					/*
					try {
					
						
						Mail::send('emails.proposal_change_notification', array(
																				'customer' => $customer,
																				'contest' => $contest, 
																				'activated_influencer' => $activatedInfluencer,
																				'estimated_post_reach' => $estimatedPostReach,
																				'estimated_new_leads' => $estimatedNewLeads,
																				'estimated_downloads' => $estimatedDownloads), function($message)use ($customer){
							
							$message->to($customer->email, $customer->name . " " . $customer->surname )
									->subject(Lang::get('email.proposal_change_notification_object'));
						
						});
					
					} catch (Exception $ex) {
						Log::error('Problem sending email: '.$ex);
					}
					*/
					
					try {
					
						$this->insertNotificationPrivate($id, Lang::get('notifications.change_promotion_proposal.text'));
					
					} catch (Exception $ex) {
						Log::error('Problem sending notification: '.$ex);
					}
				
				}
				
				return json_encode(array("result"=> "1"));
				
			 
			} else {
				return json_encode(array("result"=> "0", "message" => "You can't have admin permission"));
			}
		
		} catch (Exception $ex) {
		
			Log::error('changePromotionProposal: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
		
		
	}
	
	public function acceptProposal($id) {
		
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		
		$contest = Contest::find($id);
		$company = Company::find($contest->company_id);
		$customer = User::where('company_id', '=', $company->id)->first();
		
		try {
				
			$contest->proposal_accepted = 1;
			$contest->save();
			
			/*
			try {
			
				App::setLocale($contest->location->language);
				Mail::send('emails.confirm_proposal_notification', array(
																		'customer' => $customer,
																		'contest' => $contest), function($message) {
					
					$message->to("info@buzzmybrand.it", "Buzzmybrand")
							->subject(Lang::get('email.confirm_proposal_notification_object'));
				
				});
			
			} catch (Exception $ex) {
				Log::error('Problem sending email: '.$ex);
			}
			*/
			
			return json_encode(array("result"=> "1"));
		
		} catch (Exception $ex) {
		
			Log::error('acceptProposal: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
		
	}
	
	public function refuseProposal($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
		
		$contest = Contest::find($id);
		$company = Company::find($contest->company_id);
		$customer = User::where('company_id', '=', $company->id)->first();
		
		try {
				
			$contest->proposal_accepted = 0; 
			$contest->save();
		
			/*
			try {
			
				App::setLocale($contest->location->language);
				Mail::send('emails.refuse_proposal_notification', array(
																		'customer' => $customer,
																		'contest' => $contest), function($message) {
					
					$message->to("info@buzzmybrand.it", "Buzzmybrand")
							->subject(Lang::get('email.refuse_proposal_notification_object'));
				
				});
			
			} catch (Exception $ex) {
				Log::error('Problem sending email: '.$ex);
			}
			*/
			
			return json_encode(array("result"=> "1"));
		
		
		} catch (Exception $ex) {
		
			Log::error('refuseProposal: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
		
	}
	
	public function uploadBanner($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		$contest = Contest::find($id);


		try {
		
			if ( Input::file('banner_file') ) {
					
				if ( $contest->promotionalBanners->count() <= 11 ) {	
					
					if ( Input::file('banner_file')->getSize() / 1000 <= 80 ) {
					
					
						$promotionalBanner = new PromotionalBanner();
				
						$file = Input::file('banner_file');
						$promotionalBanner->filename = $file->getClientOriginalName();	
						
						$tag = explode('.', $promotionalBanner->filename);
						
						
						
						if ( $tag[count($tag) - 1] == 'jpg' || $tag[count($tag) - 1] == 'gif' || $tag[count($tag) - 1] == 'swf' ) {
						
							$file->move(public_path() . '/images/promotional_banners/', $promotionalBanner->filename);
							
							$image = Image::make(public_path() . '/images/promotional_banners/' . $promotionalBanner->filename);
							
							if ( 
								($image->width() == 120 && $image->height() == 120) ||
								($image->width() == 120 && $image->height() == 60) ||
								($image->width() == 320 && $image->height() == 50) ||
								($image->width() == 468 && $image->height() == 68) ||
								($image->width() == 300 && $image->height() == 250) ||
								($image->width() == 320 && $image->height() == 480) ||
								($image->width() == 480 && $image->height() == 320) ||
								($image->width() == 640 && $image->height() == 100) ||
								($image->width() == 728 && $image->height() == 90) ||
								($image->width() == 1024 && $image->height() == 768) ||
								($image->width() == 768 && $image->height() == 1024)	
							) {
							
								$promotionalBanner->contest_id = $id;
								$promotionalBanner->save();
							
							} else {
							
								unlink (public_path() . '/images/promotional_banners/' . $promotionalBanner->filename);
								return json_encode(array("result"=> "0", "message" => "unspupported dimensions"));
							}
							
							

						
						} else {
						
							return json_encode(array("result"=> "0", "message" => "wrong extensions"));
					
						}
							

				
					} else {
					
						return json_encode(array("result"=> "0", "message" => "max limit kb"));
					
					}
				
				} else {
				
					return json_encode(array("result"=> "0", "message" => "max limit banners"));
				
				}
				
				
				
			}
			
			return json_encode(array("result"=> "1"));
		
		} catch (Exception $ex) {
		
			Log::error('uploadBanner: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
	
	}
	
	public function uploadVideo($id) {
	
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		$contest = Contest::find($id);
		
		try {
		
			if ( Input::file('video_file') ) {
					
				if ( $contest->promotionalVideos->count() == 0 ) {	
					
					
					
					$promotionalVideo = new PromotionalVideo();
			
					$file = Input::file('video_file');
					$promotionalVideo->filename = $file->getClientOriginalName();	
					
					$tag = explode('.', $promotionalVideo->filename);
				
					
					if ( $tag[count($tag) - 1] == 'mp4' ) {
					
						$videoFileName = md5(time()); 	
						File::move($file, public_path() . '/images/promotional_videos/' . $videoFileName. '.' . 'mp4');
						
						$command = '/root/bin/ffmpeg -i ' . public_path() . '/images/promotional_videos/' . $videoFileName . '.mp4' . ' -vstats 2>&1';
						$output = shell_exec($command);
	
						$regex_sizes = "/Video: h264/";
						preg_match($regex_sizes, $output, $regsVideo);
						
						$regex_sizes = "/Audio: aac/";
						preg_match($regex_sizes, $output, $regsAudio);
							
						$command = '/root/bin/ffprobe -print_format json -show_streams ' . public_path() . '/images/promotional_videos/' . $videoFileName . '.mp4';
						$output = shell_exec($command);
						$resultArray = json_decode($output);
						
						
						if ( $regsVideo[0] != 'Video: h264' ) {
							unlink (public_path() . '/images/promotional_videos/' . $videoFileName . '.mp4');
							return json_encode(array("result"=> "0", "message" => "wrong video codec"));
						}
						
						if ( $regsAudio[0] != 'Audio: aac' ) {
							unlink (public_path() . '/images/promotional_videos/' . $videoFileName . '.mp4');
							return json_encode(array("result"=> "0", "message" => "wrong audio codec"));
						}
						
						if ( isset($resultArray->streams[1]->width) ) { 
						
							if (
								($resultArray->streams[1]->width == 300 && $resultArray->streams[1]->height == 250) ||
								($resultArray->streams[1]->width == 336 && $resultArray->streams[1]->height == 280)) {
							
								$promotionalVideo->contest_id = $id;
								$promotionalVideo->save();
							
							} else {
								unlink (public_path() . '/images/promotional_videos/' . $videoFileName . '.mp4');
								return json_encode(array("result"=> "0", "message" => "wrong dimesions"));
							
							}
							
						} elseif ( isset($resultArray->streams[0]->width) ) {
						
							if (
								($resultArray->streams[0]->width == 300 && $resultArray->streams[0]->height == 250) ||
								($resultArray->streams[0]->width == 336 && $resultArray->streams[0]->height == 280)) {
								
								$promotionalVideo->contest_id = $id;
								$promotionalVideo->save();
							
							} else {
								unlink (public_path() . '/images/promotional_videos/' . $videoFileName . '.mp4');
								return json_encode(array("result"=> "0", "message" => "wrong dimesions"));
							}
						

						
						} 
						
					} else {
				
						return json_encode(array("result"=> "0", "message" => "wrong extension"));
				
					}		
					

				
				} else {
				
					return json_encode(array("result"=> "0", "message" => "max limit videos"));
				
				}
				
				
				
			}
			
			return json_encode(array("result"=> "1"));
		
		} catch (Exception $ex) {
		
			Log::error('uploadVideos: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
	
	}
	
	public function getPromotion($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		try {
	
			$contest = Contest::find($id);
			
			$result = array();
			
			$result['influencers_promotion'] = $contest->influencers_promotion;
			$result['display_promotion'] = $contest->display_promotion;
			$result['video_seeding_promotion'] = $contest->video_seeding_promotion;
			$result['promotion_budget'] = $contest->promotion_budget;
			$result['activated_influencers'] = $contest->activated_influencers;
			$result['estimated_post_reach'] = $contest->estimated_post_reach;
			$result['estimated_new_leads'] = $contest->estimated_new_leads;
			$result['estimated_downloads'] = $contest->estimated_downloads;
			
			
			if ( $contest->display_promotion ) {
				
				$result['promotional_banners'] = $contest->promotionalBanners()->select("filename")->get()->toArray();
			}
			
			$displayPromotions = array();
			if ( $contest->video_seeding_promotion ) {
				$result['promotional_videos'] = $contest->promotionalVideos()->select("filename")->get()->toArray();
			}
			
			return json_encode($result);
		
		} catch (Exception $ex) {
		
			Log::error('getPromotion: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
		
	
	}
	
	private function insertNotificationPrivate($contest_id, $text, $redirect = null) {
	
		$contest = Contest::find($contest_id);

		$notification = new Notification();
		$notification->contest_id = $contest_id;
		
		if ( $text ) {
			$notification->text = $text;
		}
		
		if ( $redirect ) {
			$notification->redirect = $redirect;
		}

		$tz_object = new DateTimeZone($contest->location->timezoneDesc);
		$datetime = new DateTime();
		$datetime->setTimezone($tz_object);
		$today = $datetime->format("Y-m-d H:i:s");

		$notification->time = $today;
		
		$notification->save();
	
	}
	
	public function insertNotification($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
	
		try {
		
			$text = '';
			$redirect = '';
			
			if ( Input::get('text') ) {
				$text = Input::get('text');
			}
			
			if ( Input::get('redirect') ) {
				$redirect = Input::get('redirect');
			}
			
			$this->insertNotificationPrivate($id, $text, $redirect);
			
			return json_encode(array("result"=> "1"));
		
		} catch (Exception $ex) {
		
			Log::error('insertNotification: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
	}
	
	public function getNotifications() {

	
		////////// Security Check ////////////////////////////////////
		if ( Input::get("contest_id") && $negativeResponse = $this->checkAccessContest( Input::get("contest_id") ) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////

		try {
		
			$contest_id = Input::get("contest_id");
		
			$queryBuilder = DB::table('notifications')
					->whereNull("notifications.deleted_at");
			
			if ( $contest_id ) {
				$queryBuilder = $queryBuilder->where("notifications.contest_id", "=", $contest_id);
			} else {
				
				$contests = Contest::where('company_id', Auth::user()->company_id)->get();
				if ( count($contests) ) {
					$queryBuilder = $queryBuilder->whereIn('notifications.contest_id', Contest::where('company_id', Auth::user()->company_id)->lists('id'));
				} else {
					$queryBuilder = $queryBuilder->where(DB::raw( "false" ));	
				}
			
				
			}	
			
			$queryBuilder->select(DB::raw('
				notifications.id as notification_id,
				notifications.text as notification_text,
				notifications.time as notification_time,
				notifications.readed as notification_readed
			'));
			
			$result = $queryBuilder->get();
			
			return json_encode($result);
		
		} catch (Exception $ex) {
		
			Log::error('getNotifications: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
	
	}
	
	public function updateNotification($id) {

		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessNotification($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////

	
		try {
	
			$notification = Notification::find($id);
			$contest = Contest::find($notification->contest_id);
		
			$notification->readed = 1;

			$tz_object = new DateTimeZone($contest->location->timezoneDesc);
			$datetime = new DateTime();
			$datetime->setTimezone($tz_object);
			$today = $datetime->format("Y-m-d H:i:s");
	
			$notification->time = $today;
			
			$notification->save();
			
			return json_encode(array("result"=> "1"));
		
		} catch (Exception $ex) {
		
			Log::error('insertNotification: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
	
	}
	
	public function checkAvContestRoute() {
	
		try {
	
			$contestRoute = Input::get("contest_route");
            $contestId = Input::get('contest_id');
		
			if ( $contestRoute ) {
		
				$contests = Contest::where("contest_route", "=", $contestRoute);
                if($contestId)
                    $contests = $contests->where('id', '!=', $contestId);

                $contests = $contests->get();
				
				if ( count($contests) > 0 ) {
					return json_encode(array("result"=> "0", "message" => "Contest route not available"));
				} else {
					return json_encode(array("result"=> "1"));
				}
			
			} else {
			
				return json_encode(array("result"=> "0", "message" => "Contest route required"));
			}
			
			
		
		} catch (Exception $ex) {
		
			Log::error('checkAvContestRoute: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
	
	}
	
	public function htmlToText($text) {
		$text = str_replace('<br />','',$text);
		return strip_tags($text);
	}
	
	public function getContest($id) {
			
		////////// Security Check ////////////////////////////////////
		if ( $negativeResponse = $this->checkAccessContest($id) ) {
			return $negativeResponse;
		}
		//////////////////////////////////////////////////////////////
			
		try {

			$contest = Contest::find($id);
			
			/*
			if ( $contest->company_id != Auth::user()->company_id ) {
				return json_encode(array("result"=> "0", "message" => "You have not read permission"));
			}

			DATE_FORMAT(contests.start_time, '%Y-%m-%dT%TZ') as contest_start_date,
				DATE_FORMAT(contests.end_time, '%Y-%m-%dT%TZ') as contest_end_date,
				DATE_FORMAT(contests.score_start_time, '%Y-%m-%dT%TZ') as contest_score_start_date,
				DATE_FORMAT(contests.score_end_time, '%Y-%m-%dT%TZ') as contest_score_end_date,
			*/
	
			$queryBuilder = DB::table('contests')
					->leftJoin("templates", "contests.template_id", "=", "templates.id")
					->where("contests.id", "=", $id)
					->whereNull("contests.deleted_at");
					
			$queryBuilder->select(DB::raw('
				contests.id as contest_id,
                contests.service_type as contest_service_type,
				contests.currency as contest_currency,
				contests.company_id as contest_company_id,
				contests.type_id as contest_type,
				contests.status as contest_status,
				contests.location_id as contest_location,
				contests.term_cond_on_line_sales as contest_term_cond_on_line_sales,
				contests.term_cond_off_line_sales as contest_term_cond_off_line_sales,
				contests.name as contest_name,
				contests.hashtag as contest_hashtag,	
				contests.auto_start as contest_auto_start,				
				DATE_FORMAT(contests.start_time, \'%Y-%m-%dT%TZ\') as contest_start_date,
				DATE_FORMAT(contests.end_time, \'%Y-%m-%dT%TZ\') as contest_end_date,
				DATE_FORMAT(contests.score_start_time, \'%Y-%m-%dT%TZ\') as contest_score_start_date,
				DATE_FORMAT(contests.score_end_time, \'%Y-%m-%dT%TZ\') as contest_score_end_date,
				contests.concept as contest_concept,
                contests.how_to_win as contest_how_to_win,
				contests.no_rewards as contest_no_rewards,
				contests.win_modality_id as contest_win_modality,
                contests.template_id as contest_template_id,
				contests.use_template as use_template,
				contests.sponsor_logo as contest_watermark,
				contests.title_color as template_title_color,
				contests.contest_route as contest_route,
				contests.term_cond_company_sponsoring as contest_term_cond_company_sponsoring,
				contests.terms_cond as contest_term_conditions,
				contests.contest_approval as contest_approval,
				contests.influencers_promotion as contest_influencers_promotion,
				contests.display_promotion as contest_display_promotion,
				contests.video_seeding_promotion as contest_video_seeding_promotion,
				contests.promotion_budget as contest_promotion,
				contests.admin_approval  as admin_approval,
				contests.confirmed_by_customer  as confirmed_by_customer,
				contests.image as image,
				contests.image_minisite_bg as contest_image_minisite_bg,
				contests.image_minisite_sq as contest_image_minisite_sq,
				templates.background_image as template_image
			'));
			
			$result = $queryBuilder->first();
			
			$objectivesArray = array();
			
			foreach ( $contest->objectives->toArray() as $o ) {
			
				array_push( $objectivesArray, $o['id'] );
			
			}
			
			$result->contest_objectives = $objectivesArray;
			
			$rewardsArray = array();
			
			foreach ( $contest->rewards->toArray() as $r ) {
			
				array_push( $rewardsArray, array("rank" => $r['rank'], "title" => $r['title'], "description" => $this->htmlToText($r['description'])) );
			
			}

			$result->rewards = $rewardsArray;
			//'Y-m-d\TH:i:s\Z'
			if ($result->contest_start_date)
            {
                $contest->contest_start_date = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $result->contest_start_date);
            }
            if ($result->contest_end_date)
            {
                $contest->contest_end_date = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $result->contest_end_date);
            }
			/*
            if ($result->contest_score_start_date)
            {
                $contest->contest_score_start_date = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $result->contest_score_start_date);
            }
            if ($result->contest_score_end_date)
            {
                $contest->contest_score_end_date = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $result->contest_score_end_date);
            }
			*/
			
			$result->contest_concept = $this->htmlToText($result->contest_concept);
			
			$result->contest_how_to_win = $this->htmlToText($result->contest_how_to_win);
			
			if ( $result->contest_template_id == null ) {
			
				
				if($result->image) {
					$tag = explode('.', $result->image);
					$result->image = asset('/images/events/' . $tag[0] . "." . $tag[1]);
				}

			
			} else {
				
				$result->image = asset('/templates/squared/' . $result->template_image );
			
			}
			
			
			$fbPage = $contest->fbPages->first();
			
			if ( $fbPage ) {
				$result->post_visibility = $fbPage->fb_no_story; 
                $result->customer_connected_to_facebook       = 1;
                $result->customer_connected_to_facebook_page  = $fbPage->fb_page_name;
				$result->customer_connected_to_facebook_image = $fbPage->fb_page_id;
			}
			else
			{
				$result->customer_connected_to_facebook = 0;
			}
			
			$twPage = $contest->twPages->first();

			if ( $twPage ) {
                $result->customer_connected_to_twitter       = 1;
                $result->customer_connected_to_twitter_page  = $twPage->tw_page_name;
				$result->customer_connected_to_twitter_image = $twPage->image;
			}
			else
			{
				$result->customer_connected_to_twitter = 0;
			}
			
			$ytPage = $contest->ytPages->first();
			
			if ( $ytPage ) {
				$result->customer_connected_to_youtube = 1;
                $result->customer_connected_to_youtube_page  = $ytPage->yt_page_name;
                $result->customer_connected_to_youtube_image = $ytPage->image;
			}
			else
			{
				$result->customer_connected_to_youtube = 0;
			}
			
			$queryBuilderTemplate = DB::table('templates');
			
			$queryBuilderTemplate->select(DB::raw('
				templates.id as template_id,
				templates.name as template_name,
				templates.description as template_description,
				templates.background_image as template_background_image,
				templates.contest_flyer as template_contest_flyer,
				templates.preview as template_preview
			'));
			
			$result->templateslist = $queryBuilderTemplate->get();
			
			$contest_hashtag = explode(";", $result->contest_hashtag);
			
			$hashtagArray = [];			
						
			foreach ($contest_hashtag as $hashtag) {
				
				array_push($hashtagArray,$hashtag);
			}
			
			$result->contest_hashtag = $hashtagArray;
			
			return json_encode($result);
			
			
		
		} catch (Exception $ex) {
		
			Log::error('getContest: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
			
	}
	
	public function getTemplateList() {
		try {
			$queryBuilder = DB::table('templates');
			
			$queryBuilder->select(DB::raw('
				templates.id as template_id,
				templates.name as template_name,
				templates.description as template_description,
				templates.background_image as template_background_image,
				templates.contest_flyer as template_contest_flyer,
				templates.preview as template_preview
			'));
			
			$result = $queryBuilder->get();
			
			return json_encode($result);
		} catch (Exception $ex) {
			Log::error('getTemplateList: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
		}
	}
	
	public function getFonts() {
		try {
			$queryBuilder = DB::table('fonts');
			
			$queryBuilder->select(DB::raw('
				fonts.id as font_id,
				fonts.name as font_name,
				fonts.link as font_link
			'));
			
			$result = $queryBuilder->get();
			
			return json_encode($result);
		} catch (Exception $ex) {
			Log::error('getFonts: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
		}
	}
	
	public function checkAvEmail($user_id = false, $user_email = false) { 
	
		try {
	
			$email = !$user_email? Input::get("email"): $user_email;
		
			if ($email) {
		
				if ($user_id)
					$user = User::where("email", "=", $email)->where("id", "<>", $user_id)->get();
				else
					$user = User::where("email", "=", $email)->whereNotNull("company_id")->get();
				
				if ( count($user) > 0 ) {
					return json_encode(array("result"=> "0", "message" => "E-mail already registered"));
				} else {
					return json_encode(array("result"=> "1"));
				}
			
			} else {
			
				return json_encode(array("result"=> "0", "message" => "Email required"));
			}
			
			
		
		} catch (Exception $ex) {
		
			Log::error('checkAvEmail: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
		
	
	}
	
	public function getTwitterLocationsTrendsAvailable($name = null) {
	
		try {
	
			//$activeTrends = ActiveTrend::find(1);
			
			//if ( $activeTrends->value == 1 ) {
				
				//$locations = Table1TrendLocation::all();
				$locations = DB::table('table1_trend_locations')
							->whereNull('table1_trend_locations.deleted_at');
							
				
				
				if ($name) {
					Log::debug('getTwitterLocationsTrendsAvailable with name: '.$name);
					$locations->select(DB::raw('
					table1_trend_locations.id as id,
					table1_trend_locations.woeid as woeid, 
					table1_trend_locations.name as name,
					table1_trend_locations.processed as processed
				'))->where('table1_trend_locations.name', $name);
				
				} else {
					Log::debug('getTwitterLocationsTrendsAvailable without name');
					$locations->select(DB::raw('
						table1_trend_locations.woeid as woeid, 
						table1_trend_locations.name as name
					'));
					
				}
				
				$result = $locations->get();
				
			//}
			/*else if ( $activeTrends->value == 2 ) {
				
				//$locations = Table2TrendLocation::all();
				
				$locations = DB::table('table2_trend_locations')
							->whereNull('table2_trend_locations.deleted_at');
							
				$locations->select(DB::raw('
					table2_trend_locations.id as id,
					table2_trend_locations.woeid as woeid, 
					table2_trend_locations.name as name,
					table2_trend_locations.processed as processed
				'));
				
				$result = $locations->get();
			}
			*/
							
			return json_encode($result);
		
		} catch (Exception $ex) {
		
			Log::error('getTwitterLocationsTrendsAvailable: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
	
	}
	
	public function getTwitterTrendsAvailable($woeid) {
	
		try {
			
			$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  Config::get('twitter')['oauth_token'], Config::get('twitter')['oauth_token_secret']);
			$tweet_data = $twitteroauth->get('https://api.twitter.com/1.1/trends/place.json?id=' . $woeid);
			
			//Log::debug("tweet_data: ".print_r($tweet_data,true));
			
			$trends = [];
			
			//$trends = array("#اخر_هديه_وصلت_لك_متي", "#اسباب_سعادتك_هي", "#ALDUB49thWeeksary", "#تشيلي", "Amjad Sabri", "Ireland", "Deputy Crown Prince", "حرم علي", "حمد الرضا", "كره القدم");
			
			
			foreach ($tweet_data[0]->trends as $t) {
				
				$trend = stripos($t->name, '#') === false ? '#'.$t->name : $t->name;
				
				$trend = str_replace(' ', '', $trend);
				
				array_push($trends, $trend);
				
			}
			
			
			//Log::debug("Result trends: ".$trends);
			
			return json_encode($trends);
			
		} catch (Exception $ex) {
			Log::error('getTwitterTrendsAvailable: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
		}
		
	
	
	
		/* try {
	
			$activeTrends = ActiveTrend::find(1);
			
			if ( $activeTrends->value == 1 ) {
				
				$trends = DB::table('table1_trend_locations')
							->join("table1_trends", "table1_trends.table1_trend_location_id", "=", "table1_trend_locations.id")
							->whereNull('table1_trend_locations.deleted_at')
							->whereNull('table1_trends.deleted_at')
							->where("woeid", "=", $woeid);
							
											
				$trends->select(DB::raw('
					table1_trends.name as name
				'));
				
				$result = $trends->get();
				
				foreach ($result as $k => $row) {
					if (strpos($row->name, "#") === false) {
						$result[$k]->name = "#{$row->name}";
					}
				}
				
				//$location = Table1TrendLocation::where("woeid", "=", $woeid)->first();
				
			} else if ( $activeTrends->value == 2 ) {
				
				$trends = DB::table('table2_trend_locations')
							->join("table2_trends", "table2_trends.table2_trend_location_id", "=", "table2_trend_locations.id")
							->whereNull('table2_trend_locations.deleted_at')
							->whereNull('table2_trends.deleted_at')
							->where("woeid", "=", $woeid);
							
											
				$trends->select(DB::raw('
					table2_trends.name as name
				'));
				
				$result = $trends->get();
				
				foreach ($result as $k => $row) {
					if (strpos($row->name, "#") === false) {
						$result[$k]->name = "#{$row->name}";
					}
				}
				
				//$location = Table2TrendLocation::where("woeid", "=", $woeid)->first();
				
			}
			
			return json_encode($result);
			//return json_encode($location->trends->toArray());
		
		// } catch (Exception $ex) {
		
			// Log::error('getTwitterTrendsAvailable: '.$ex);
			// return json_encode(array("result"=> "0", "message" => "".$ex));
			
		 }*/
			
	}

    public function checkCoupon() {
        $code          = Input::get('code');
        $amount        = Input::get('amount');
        $invalidCoupon = 1;
        $percentOff    = 0;
        $amountOff     = 0;
        $discount      = 0;
        $finalPrize    = 0;

        try {
            if (strlen($code) > 0)
            {
                //capire se il coupon inserito è valido
                Stripe::setApiKey("sk_test_OeVAPOsmdoAqkC4zkwzU0P5o");
                $coupon_object = Stripe_Coupon::retrieve($code);
            }
            
        } catch (Stripe_InvalidRequestError $err) {}

        if (isset($coupon_object)) {
            $percentOff = $coupon_object->percent_off;
            $amountOff  = $coupon_object->amount_off;

            if ($coupon_object->percent_off && $coupon_object->valid)
            {
                $new_prize = round($amount * ((100-$coupon_object->percent_off) / 100), 2);

                $invalidCoupon = 0;
                $finalPrize    = round($new_prize/100, 2);
				
            }
            if ($coupon_object->amount_off && $coupon_object->valid)
            {
                $invalidCoupon = 0;
                $finalPrize    = $amount - $coupon_object->amount_off;
            }

            $discount = round(($amount - ($finalPrize*100))/100, 2);
        }

        return json_encode(array("invalid_coupon" => $invalidCoupon, "percent_off" => $percentOff, 'amount_off' => $amountOff, 'discount' => $discount, "final_prize" => $finalPrize));
    }
	
	
	public function approveContest() {

		return false;
	
		Log::debug("approveContest");
	
		try {
	
			// Security check
			if (Input::get('approval_step') !== 0 && !Auth::user()->isSuperAdmin() ) {
				return json_encode(array("result"=> "0", "message" => "You don't have read permission"));
			}
			//////////////////
			
			$contestId = Input::get("contest_id");
			$approvalStep = Input::get("approval_step");
		
			if ( $contestId && isset($approvalStep)) {
			
				$contest = Contest::find($contestId);
				$user = User::where("company_id", "=", $contest->company_id)->first();

				
				if ( $approvalStep == 0 ) {
					$contest->admin_approval = 0;
				} else if ( $approvalStep == 1 ) {
					$contest->admin_approval = 1;
					$contest->status = 6; // NOT APPROVED
					
					if ( $contest->service_type == 1 ) {
						$email = 'emails.contest_standard_not_approved';
						$object = 'email.object_contest_standard_not_approved';
						$notification = 'notifications.contest_not_approved';
					} else if ( $contest->service_type == 2 ) {
						$email = 'emails.contest_instant_not_approved';
						$object = 'email.object_contest_instant_not_approved';
						$notification = 'notifications.contest_not_approved';
					}
					
					App::setLocale($contest->location->language);
					
					try {
						Log::debug("send mail ");
						Mail::send($email, array('contest' => $contest), function($message) use ($user, $object) {
							
							$message->to("info@buzzmybrand.it", $user->name . " " . $user->surname)
									->subject(Lang::get($object));
						
						});	
					
					} catch (Exception $ex) {
		
						Log::error($ex);
						
					}
					
					
					try {
						$request = Request::create('/admin/insert-notification/' . $contestId, 'POST', array(
																				'text' => Lang::get($notification), 
																			'redirect' => asset("/dasboard/index.html"),));
																			

						Request::replace($request->input());
							
						Route::dispatch($request)->getContent();
					
					} catch (Exception $ex) {
						Log::error('Problem sending notification: '.$ex);
					}
				
				
				} else if ( $approvalStep == 2 ) {
					$contest->admin_approval = 2;
					$contest->status = 3; // SCHEDULED
					
					App::setLocale($contest->location->language);
					
					if ( $contest->service_type == 1 ) {
						$email = 'emails.contest_standard_approved';
						$object = 'email.object_contest_standard_approved';
						$notification = 'notifications.contest_approved';
						
						$notificationFull = Lang::get($notification) . DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time)->format('d/m/Y H:i:s');
					} else if ( $contest->service_type == 2 ) {
						$email = 'emails.contest_instant_approved';
						$object = 'email.object_contest_instant_approved';
						$notification = 'notifications.contest_instant_approved';
						
						$notificationFull = Lang::get($notification);
					}
					
					
					
					try {
					
						Mail::send($email, array('contest' => $contest), function($message) use ($user, $object) {
							
							$message->to("info@buzzmybrand.it", $user->name . " " . $user->surname)
									->subject(Lang::get($object));
						
						});	
					
					} catch (Exception $ex) {
		
						Log::error($ex);
						
					}
					
					try {
						$request = Request::create('/admin/insert-notification/' . $contestId, 'POST', array(
																				'text' => $notificationFull, 
																			'redirect' => asset("/dasboard/index.html"),));
																			

						Request::replace($request->input());
							
						Route::dispatch($request)->getContent();
					
					} catch (Exception $ex) {
						Log::error('Problem sending notification: '.$ex);
					}
					
					
				}
			
				$contest->save();
				
				return json_encode(array("result"=> "1"));
			
			} else {
				return json_encode(array("result"=> "0", "message" => "Missing parameters"));
			
			}
		
		} catch (Exception $ex) {
		
			Log::error('approveContest: '.$ex);
			return json_encode(array("result"=> "0", "message" => "".$ex));
			
		}
		
	
	}
	
	public function savePreview() {
		
		Log::debug("savePreview() Request inputs: " . print_r( Input::all(), true ) );
		
		if ( Input::file('contest_watermark') && ( Input::file('contest_image_minisite_bg') )) {
				
						
			$file = Input::file('contest_watermark');
			$sponsor_logo = $file->getClientOriginalName();
			$file->move(public_path() . '/images/events/tmp/', $sponsor_logo);
		
			
			$imageFile = public_path() . '/images/events/tmp/' . $sponsor_logo;
			$imageObject = imagecreatefrompng($imageFile);
			
			// Scale
			$imageObject = imagescale($imageObject, 100, 100,  IMG_BICUBIC_FIXED);
			
			// New image
			$imagePng = public_path() . '/images/events/' . $sponsor_logo;
			$imagePng = basename($imagePng);
			imagepng($imageObject, $imageFile);
			
			//BACKGROUND MINISITO
			$file = Input::file('contest_image_minisite_bg');
			$image_minisite_bg = $file->getClientOriginalName();
			$tag = explode(".", $image_minisite_bg);
			$file->move(public_path() . '/images/events/tmp/', $image_minisite_bg);
			
			$imageFile = public_path() . '/images/events/tmp/' . $image_minisite_bg;
			
			switch ($tag[1]) {
				case "jpg":
					$imageObject = imagecreatefromjpeg($imageFile);
					break;
				case "png":
					$imageObject = imagecreatefrompng($imageFile);
					break;
				case "gif":
					$imageObject = imagecreatefromgif($imageFile);
					break;
			}
			
			// New image
			$imageJpeg = public_path() . '/images/events/' . $tag[0] . ".jpg";
			imagejpeg($imageObject, $imageJpeg);
			$imageJpeg = basename($imageJpeg);
		
			return json_encode(array("result"=> "1", "contest_watermark" => $imagePng, "contest_image_minisite_bg" => $imageJpeg));
		} else
			return json_encode(array("result"=> "0"));
		
		
			
			
			
		
		
		
		
	}
	
	
}
