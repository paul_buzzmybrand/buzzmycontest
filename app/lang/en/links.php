<?php

return [

	'social_buzz_title' => 'The Social Buzz',
	'social_buzz_description' => ':before BuzzMyBrand :after conceived and developed the :before Social Buzz ® :after, an algorithm that indicates the success of a single on-line post (photo or video) calculating how viral and influential it is. The Social Buzz is used to understand who\'s the winner of a contest without using a jury and, moreover, making the contest viral. :br In order to win a contest launched using :before BuzzMyBrand :after, users must get the highest number of likes, shares, comments and views on the major social media like :before Facebook :after, :before Twitter :after and :before YouTube :after. Our software will evaluate the success of a photo or a video on any social media and quantify it, ranking content in real time for everyone to view. By doing so, users with the highest Social Buzz ® will become your brand\'s main ambassadors.',

	'about_title' => 'About',
	'about_description' => ':before BuzzMyBrand :after is a platform to launch instant video & photo contests on Facebook, Twitter and Youtube. :br With BuzzMyBrand you can launch instant contest according to the hot topics on the net thanks to our technology that matches the trends of the moment with your needs and targets. :br Social media pages are the nowadays websites and brands are always looking for :before new ways to engage customers and create brand loyalty on their social media official pages :after. Furthermore, on the occasion of public events that catch the attention of million of users and consequentially fill the social media with tons of user generated content related to such events, companies don’t know how :before to get into this buzz :after and :before “brand” :after such contents. :br :before BuzzMyBrand :after brings novelty and efficacy to companies: we offer a solution :before simple to use :after from concept to delivery, competitive in price (cheaper than competitors but with a better value proposition) and very :before effective :after in results. :br',
	
	'compliance_title' => 'Compliance',
	
	'faqs_title' => 'FAQs',
	
	'careers_title' => 'WE ARE ALWAYS LOOKING FOR MOTIVATED AND DEDICATED PEOPLE',
	'careers_description' => 'At BuzzMyBrand we are always looking for a wide range of software developers, skilled graphic designers. We know that our success depend on all our employees\' competence, drive and motivation. :br Hence, we see the full benefit of offering exiting tasks in a first class working environment, competitive terms and good opportunities for a career development. :br Please send your CV at info@buzzmybrand.it',
	
	'contacts_title' => 'Contacts',
	'contacts_enquiries' => 'General enquiries',
	'contacts_callus' => 'Call us',
	'contacts_offices' => 'Offices',
	'contacts_reg_office' => 'Registered Office',
	'cancellationpolicy_title' => 'Cancellation Policy',
	'invoices_title' => 'Billing and Refund Policies',
	
	'awards' => 'awards',
	'terms' => 'terms',
	'contacts' => 'contacts',
	'careers' => 'careers',
	'about' => 'about-us',
	'compliance' => 'compliance',
	'our_flow' => 'our-flow',
	'our_flow_title' => 'Our Flow',
	'cancellationpolicy' => 'cancellationpolicy',
	'invoices' => 'invoices',
	];