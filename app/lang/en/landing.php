<?php

return [

	'do_not_wait' => '',
	'head_cta' => ':before Launch :after Your Contest Now!',
	'head_sub' => 'Photo, Video and Text Contests to reach out new users and customers and achieve your marketing & sales objectives',
	'instant_contest' => 'INSTANT CONTEST',
	'48_hours' => '48 HOURS',
	'standard_contest' => 'STANDARD CONTEST',
	'1_day_up' => '1 DAY UP',
	'start_now' => 'START NOW',
	'get_in_touch' => 'GET IN TOUCH',
	'launch_contest' => 'LAUNCH CONTEST',
	'get_coupon' => 'FREE COUPON',

	// GET IN TOUCH FORM
	'form_signup_title' => 'SIGN UP TO START',
	'form_title' => 'GET IN :before TOUCH :after',
	'form_name' => 'name',
	'form_surname' => 'surname',
	'form_email' => 'e-mail',
	'form_company' => 'company',
	'form_job_title' => 'job title',
	'form_country' => 'country',
	'form_tel' => 'telephone',
    'form_tel_placeholder' => 'include your country code (e.g. +39)',
	'form_message' => 'message',
	'form_newsletter' => 'subscribe to our newsletter',
	'form_send' => 'send',
	'form_sent' => 'Message sent!',
	'form_not_sent' => 'Message not sent',
	'form_password' => 'Password',
	'form_password_repeat' => 'Password confirmation',
	'signup_newsletter' => 'Subscribe to our Newsletter',
	'terms_and_conditions_desc' => 'I agree to BuzzMyBrand ',
	'terms_and_conditions' => 'Terms & Conditions',
	'get_started' => 'GET STARTED',
	'email_not_available' => 'E-mail already registered. Please login or signup with a different e-mail address.',

	// LAUNCH
	'launch_instant' => 'OUR :before CONTESTS :after',
	'launch_instant_description' => 'The brand-new platform that allows brands and on-line marketing agencies to manage contests on social networks, websites and mobile APPs. You will be able to engage your followers and community on your Facebook, Twitter, Instagram, YouTube, WeChat and Snapchat profiles and manage influencers and trending hashtags to make your social network campaign popular and successful.',
	'instant_feature_1' => 'BRAND AWARENESS',
	'instant_feature_2' => 'INSTANT TREND CONTESTS',
	'instant_feature_3' => 'ENGAGEMENT',
	'instant_feature_4' => 'REACH',

	'launch_standard' => 'STANDARD :before CONTEST :after',
	'launch_standard_description' => 'Launch a Contest that is focused on the specific achievement of a corporate objective such as :before brand loyalty, on-line sales, conversion. Full customization. :after',
	'standard_feature_1' => 'USERS\' DATA',
	'standard_feature_2' => 'ON-LINE SALES CONVERSION',
	'standard_feature_3' => 'LEAD GENERATION',
	'standard_feature_4' => 'BRAND LOYALTY',

	// HOW IT WORKS
	'how_it_works' => 'HOW IT :before WORKS :after',
	'how_it_works_sub' => 'With BuzzMyBrand contests you will convert the social actions of your profiles into useful data to be used for future marketing & sales campaigns. Make your community and followers ambassadors of your brand and collect their data through challenging and  engaging contests.',

	// ENGAGE
	'engage_title' => 'INFORM YOUR COMMUNITY AND ADVERTISE THE CONTEST',
	'engage_description' => 'Invite the users to participate to the contest. All contents, once approved, will be shared on the social networks and voted by other users, friends and on-line communities. Thanks to the Social Buzz® ranking system your users will challenge themselves to make the most popular content. The most popular content will win the contest in a very transparent and certified way. With our custom solutions we can help you to amplify the contest reach activating bloggers and influencers that will activate word of mouth on the net increasing the contest participation and visibility.',
	'engage_app' => 'CUSTOMIZED APP :br on iOS and Android',
	'engage_facebook' => 'FACEBOOK TAB',
	'engage_twitter' => 'TWITTER',
	'engage_youtube' => 'YOUTUBE',
	'engage_website' => 'WEBSITE',
	'engage_promotion' => 'PROMOTION :br APP downloads :br ADV campaigns :br Influencers buzz',

	// CONVERT
	'convert_title' => 'GATHER DATA AND CONTROL THE PERFORMANCE',
	'convert_description' => 'For each contest you will receive detailed real-time analytics: total contents, approved/not approved contents, total/unique reach, total/unique impressions, point of entry efficacy (Mobile, Web, Social Network), new followers increase, engagement rate increase, social actions and emotions. At the end of the contest we will send you a report with all the contest details and with the data collected from the participants. With our custom solution we can help you to collect more data and leads from all the participants.',
	'convert_roi' => 'ROI :br APP downloads :br ENGAGEMENT',
	'convert_analytics' => 'ANALYTICS DEMOGRAPHIC',
	'convert_channels' => 'CHANNELS EFFICIENCY :br Facebook, Twitter, Instagram, APPs',

	// CONNECT
	'connect_title' => 'LAUNCH A CONTEST',
 	'connect_description' => 'Linking your social network/s profile/s to BuzzMyBrand platform you will create a mobile-friendly website where participants can upload their photos, video or texts in order to win the products/services sponsored by you. The contest website is automatically embedded in a TAB on your Facebook page and on your website with an iFrame link. Launching a contest with BuzzMyBrand you will be allowed to choose the contest title, the hashtags, the layouts, the dates, the prizes, the creative brief, the approval processes and launch a contest in few steps. If you need a customized solution you can contact us and a sales account will answer to all your questions related to T&C and provide you with creative solutions in order to reach you needs and objectives. ',
	'connect_photo' => 'PHOTO OR VIDEO CONTEST',
	'connect_dates' => 'CONTEST DATES',
	'connect_brief' => 'CONTEST BRIEF',
	'connect_rewards' => 'REWARDS',

	// HOW ARE WE DIFFERENT
	'diff_title' => 'OUR :before PLUS :after',
	'diff_sub' => 'What makes us unique is that we know our customers’ needs and we make their objectives reachable through and engaging and easy to use contest experience.',

	'diff_1_title' => 'LOCATION AND TREND BASED CONTEST',
	'diff_1_description' => 'With BuzzMyBrand you can promote location and trend based contest thanks to our instant trends detector that will allow you to check the hot topics and hashtag in a particular location. Increase leads and participation launching a contest following the hottest topic on the net.',

	'diff_2_title' => 'THE SOCIAL BUZZ ®',
	'diff_2_description' => 'The Social Buzz ® is our proprietary technology that identifies the most viral contents from your contest participants. The contest winners are the participants with the highest Social Buzz ®. The more people make their content popular and viral, the more you get brand awareness and word of mouth on social media.',

	'diff_3_title' => 'MULTIPLATFORM CONTESTS',
	'diff_3_description' => 'With BuzzMyBrand you can launch and manage contests on Facebook, Twitter, Instagram, Youtube, WeChat, SnapChat, Dedicated Website and Mobile APP simultaneously.',

	'diff_4_title' => 'CUSTOMIZED SOLUTIONS',
	'diff_4_description' => 'We have a team that loves creativity and that helps our customer to create tailored solutions in order to reach the company objectives on social media. We will help you with the custom layout, we will provide you creative ideas, we will assist you with the contest T&C. We are here to help you, your success is our success.',

	'diff_5_title' => 'PERFORMANCE BASED PRICING',
	'diff_5_description' => 'We are able to optimize towards the success and popularity of the contest. This is only with a small set up fee and a performance based pricing.',

	// LAUNCH CALL TO ACTION BLUE BOX
	'lcta_title' => ':before LAUNCH :after YOUR CONTEST NOW',
	'lcta_sub' => 'Photo, Video and Text Contests to reach out new users and customers and achieve your marketing & sales objectives',

	// PRICING
	'pricing_title' => 'PRICING',
	'pricing_sub' => 'BuzzMyBrand is simple. :before Your Contest success is our reward :after',
	'pricing_launch_btn' => 'LAUNCH NOW',
	'pricing_advanced_billing' => '(billed in advance)',
	'pricing_based_on' => 'BASED ON :beforeREAL TIME HASHTAGS:after',
	'pricing_all_included' => 'ALL INCLUDED',
	'pricing_tailored' => 'TAILORED TO YOUR NEEDS',
	'pricing_onsocial' => 'ON SOCIAL, WEB, MOBILE APP',
	'pricing_firstrow' => 'CUSTOMIZED',
	'pricing_secondrow' => 'PRIZE',

	// - BUZZ
	'pricing_1_title' => ':beforeBUZZ :after PACKAGE',
	'pricing_1_1' => 'PHOTO, VIDEO, TEXT CONTEST',
	'pricing_1_2' => 'CUSTOM CONTEST LANDING PAGE',
	'pricing_1_3' => 'UNLIMITED LEADS',
	'pricing_1_4' => 'DASHBOARD WITH BASIC ANALYTICS',
	'pricing_1_5' => 'SOCIAL MEDIA INTEGRATION (Fb, Tw, Yt)',
	'pricing_1_6' => 'CONTENTS\' APPROVAL',

	// - SUPERBUZ
	'pricing_2_title' => ':beforeSUPERBUZZ:after PACKAGE',
	'pricing_2_1' => 'BUZZ PACKAGE FEATURES',
	'pricing_2_2' => 'CONTEST URL WHITELABELING',
	'pricing_2_3' => 'PARTICIPANTS LEADS AND MAIL ADDRESSES',
	'pricing_2_4' => 'CONTEST REPORT IN PDF',
	'pricing_2_5' => 'ADVANCED ANALYTICS',
	'pricing_2_6' => 'CORPORATE WEBSITE INTEGRATION',

	// - BUZZMYCONTEST
	'pricing_3_title' => ':beforeBUZZMYCONTEST :after PACKAGE',
	'pricing_3_1' => 'SUPERBUZZ PACKAGE FEATURES',	
	'pricing_3_2' => 'CUSTOM CONTEST LANDING PAGE',
	'pricing_3_3' => 'WATERMARK ON EACH ENTRY',
	'pricing_3_4' => 'API/WEBSITE/APP INTEGRATIONS',
	'pricing_3_5' => 'CUSTOM LAYOUTS AND DESIGNS',
	'pricing_3_6' => 'TOTAL CONTEST FLOW CUSTOMIZATION',	
	'pricing_3_btn' => 'CONTACT US',
	'price_spec' => 'CUSTOM PRICE',

	// NEWSLETTER
	'newsletter_title' => ':before STAY :after TUNED',
	'newsletter_sub' => 'subscribe to our newsletter',
	'newsletter_placeholder' => 'your email here',
	'newsletter_btn' => 'SUBSCRIBE',
	
	
];

