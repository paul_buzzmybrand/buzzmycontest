<?php

return array(
	
	'win_modality_instructions_buzz' => 'The winners of the contest are the users with the highest social buzz. The Social Buzz calculates the number of likes, comments, shares and views. Get your entry the most popular',
	'colors' => array(
		'black' => 'black',
		'white' => 'white',
	),
);