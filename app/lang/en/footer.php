<?php

return [

	'social_buzz' => 'the social buzz ®',
	'awards' => 'awards',
	'terms' => 'terms',
	'privacy' => 'privacy',
	'contact' => 'imprint',
	'careers' => 'careers',
	'about' => 'about',
	'blog' => 'blog',
	'media_kit' => 'media kit',
	'compliance' => 'compliance',
	'our_flow' => 'our flow',
	'faqs' => 'FAQs',
	'invoices' => 'billing & refunds',
	'cancellationpolicy' => 'cancellation policy',
	'sitemap' => 'site map',
];