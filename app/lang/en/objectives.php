<?php

return array(
	
	'community_engagement' => array (
		'label' => 'COMMUNITY ENGAGEMENT',
		'description' => 'Share the contest with your audience and create a strong bond between your brand and your fans: BuzzMyBrand is the best solution to make exciting challenges on social media in a battle of likes.',
	),
	'brand_awareness' => array (
		'label' => 'BRAND AWARENESS',
		'description' => 'Creating awareness of your brand with potential customers is our primary focus in the initial stage of the life cycle of your product. The contest will create awareness and consideration, expanding your target market.',
	),
	'brand_loyalty' => array (
		'label' => 'BRAND LOYALTY',
		'description' => 'Build trust in your brand: let the users recommend your brand to their friends, leading to a word of mouth process. The operations of "loyalty" can be directed to your brand (brand loyalty) or to your store (store loyalty).',
	),
	'sales' => array (
		'label' => 'SALES',
		'description' => 'Increase your sales with purchase call-to-actions and providing vouchers, launch a contest to increase your sales, creating new channels and routes to take your followers to the final purchase.',
	),
	'offline_sales' => array (
		'label' => 'OFFLINE SALES',
		'description' => 'Bring your on-line fans in your physical stores with campaigns based on coupons, discounts and offers.',
	),
	'online_sales' => array (
		'label' => 'ONLINE SALES',
		'description' => 'Convert the participants of the contest in users of your e-commerce platform.',
	),
	
);