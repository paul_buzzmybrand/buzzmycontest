<?php

return [
    'duplicate_initial_payment' => 'You have already registered a credit card',
    'user_has_no_active_card' => 'You have no active credit card',
    'generic_error_message' => 'We could not complete the transaction. Please try again in some minutes.',
    'could_not_remove_credit_card' => 'Error removing your credit card. Please contact our staff.',

    'email' => [
    	'expiration_subject' => 'Your credit card is about to expire',
    ]
    
];