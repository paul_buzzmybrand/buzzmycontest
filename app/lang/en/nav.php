<?php

return [

	'toggle_navigation' => 'Toggle navigation',
	'home' => 'Home',
	'how_it_works' => 'How it works',
	'how_we_are_different' => 'Our Plus',
	'pricing' => 'Pricing',
	'launch_campaign' => 'Launch contest',
	'login' => 'LOG IN',
	'signup' => 'SIGN UP',
	'password_forgotten' => 'Password forgotten?',
	'back_to_login' => 'Back',
	"recover_your_password" => "Recover your password",
	"contact_us" => "CONTACT US",
	'recovery_email_sent' => 'Recovery e-mail sent',
	
];
