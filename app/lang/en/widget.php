<?php

return array(
	
	"signup" => "signup",
	"connect" => "connect",
	"actions" => "actions",
	"create" => "create",
	"instant_win" => "instant win",
	"end" => "end",
	"share" => "SHARE THE CONTEST!",
	"share_second_sentence" => "You will gain one point more for your entry",


	"signup_step" => array(
	
		"register_now" => "REGISTER NOW",
		"name" => "name",
		"surname" => "surname",
		"birthdate" => "birthdate",
		"email_address" => "email address",
		"phone" => "phone",
		"city" => "city",
		"cap" => "postal code",
		"sex" => "sex",
		"t_e_c" => "T&C Apply",
		"name_required" => "Name is required",
		"surname_required" => "Surname is required",
		"birthdate_required" => "Birthdate is required in format dd/mm/yyyy",
		"email_required" => "A correct email is required",
		"phone_required" => "A valid phone is required",
		"city_required" => "City is required",
		"t_e_c_required" => "Terms and conditions need to be applyed",
		"privacy_required" => "Privacy must me approved",
		"sex_required" => "Sex is required",
		"cap_required" => "A valid postal code is required",
		"privacycheck_required" => "Please click on the checkbox",
		"checkbox_info" => "I authorize the treatment of my personal data in order to participate to the contest. Data will not be sent to third parties.",
		
		"deny_access" => "You can't register with this email",
	
		"selectsex" => "select the sex",
		"male" => "male",
		"female" => "female",
	),
	
	"connect_step" => array(
	
		"sentence" => "The contest will run on ",
		"sentence_conj" => " and ",
		"sentence_2" => "Connect your accounts to increase your chances of victory ",
		"sentence_2_s" => "Connect your account to increase your chances of victory ",
		"proceed" => "proceed",
		"social_alert" => "You must connect at least with one social",
		"proceed_sentence" => "Click on Proceed to continue",
	),
	
	
	"creation_step" => array(
	
		"title" => "Insert title",
		"essay_placeholder" => "Insert your text here",
		"placeholder" => "Insert the title of your entry + the hashtag ",
		"placeholder2" => "and ask your friends to make it popular to win!",
		"policy" => "Connecting your accounts, you agree to post your entry on your timelines",
	),
	
	"end_step" => array(
	
		"sentence1" => "CONGRATULATIONS",
		"sentence2" => "YOUR CONTENT HAS BEEN UPLOADED",
		"sentence3" => "YOU WILL BE NOTIFIED ONCE APPROVED",
		"share" => "SHARE",
		"share_second_sentence" => "to gain more points!"
	),
	


);

?>