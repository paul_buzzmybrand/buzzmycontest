<?php

return array(
    'type'       => 'type',
    'language'   => 'language',
    'objectives' => 'objectives',
    'social'     => 'social',
    'title'      => 'title',
    'hashtag'    => 'hashtag',
    'brief'      => 'brief',
    'how_to_win' => 'how to win',
    'rewards' => 'rewards',
    'dates' => 'dates',
    'flyer' => 'flyer',
    'filter' => 'filter',
    'post_visibility' => 'post visibility',
    'url' => 'url',
    'url_facebook_tab' => 'url Facebook tab',
    'promotion_budget' => 'promotion budget',
    'package' => 'package',
    'discount' => 'discount applied',

);