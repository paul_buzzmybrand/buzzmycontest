<?php

return [
    'YOUR_CONTESTS' => 'Your contests',
    'CHOOSE_CONTESTS_TO_MANAGE' => 'Choose the contests you want to manage',

    'FILTER' => [
    	'IN_PROGRESS' => 'In progress',
    	'OVER' => 'Over',
    	'SCHEDULED' => 'Scheduled',
    	'DRAFT' => 'Draft',
    	'ALL' => 'All'
    ],
    'choose_a_facebook_page' => 'Choose the facebook page where you want to add the contest TAB',
    'logout' => 'Logout',
];