	<?php

return [
	
	'change_promotion_proposal' => array(
		'text' => 'You have a Pending Promotion Proposal. Check it out!',
	),
	
	'contest_start' => 'Hi Buzzer! Your contest has just started. Check how is it going in the Analytics page.',
	'contest_end' => 'Hi Buzzer, your contest is over! Get the analytics of your campaign.',
	'contest_approved' => 'Hi Buzzer! Your contest has been approved and will start on ... ',
	'contest_instant_approved' => 'Hi Buzzer! Your contest has been approved.',
	'contest_not_approved' => 'YOUR CONTEST HAS NOT BEEN APPROVED',
];