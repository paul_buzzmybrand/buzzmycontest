<?php

return [

	'object_video_contest_approved' => 'Your Video Has been approved!',
	'object_photo_contest_approved' => 'Your Photo Has been approved!',
	'object_essay_contest_approved' => 'Your Text Has been approved!',
	
	'object_video_contest_not_approved' => 'Your Video Has not been approved!',
	'object_photo_contest_not_approved' => 'Your Photo Has not been approved!',
	'object_essay_contest_not_approved' => 'Your Text Has not been approved!',
	
	"invite" => array(
		"subject" => "You have received an invitation for a contest!",
		"sentence1" => "Hi",
		"sentence2" => "Your friend",
		"sentence3" => "invited you to",
		"contest_en" => "Contest",
		"sentence4" => "To know more, click",
		"sentence5" => "Here",
	),
	
	'admin_notify_email_object' => 'New user has been successfully registered.',
	
	'admin_notify_email_1' => 'User',
	'admin_notify_email_2' => 'has been successfully registered',
	'admin_notify_email_3' => 'Email',
	'admin_notify_email_4' => 'Company',
	'admin_notify_email_5' => 'Job Title',
	'admin_notify_email_6' => 'Telephone',
	'admin_notify_email_7' => 'Country',
	
	'admin_notify_email_budget_object' => 'User has been successfully entered budget.',
	'admin_notify_email_budget_1' => 'Data regarding user that wants promotion for his contest',
	
	'welcome_email_object' => 'Welcome to BuzzMyBrand',

	'welcome_email_1' => 'Hello',
	'welcome_email_2' => 'Welcome to BuzzMyBrand!',
	'welcome_email_3' => 'With our social contest platform you will be able to launch your contest quickly and easily.',
	'welcome_email_4' => 'Click ',
	'welcome_email_9' => 'here',
	'welcome_email_8' => 'to verify your account or copy and paste this link in your browser:',
	'welcome_email_5' => 'If you need assistance contact us replying directly to this e-mail',
	'welcome_email_6' => 'Regards',
	'welcome_email_7' => 'BuzzMyBrand Team',	
	
	
	'promotion_change_notification_object' => 'Promotion change',
	'promotion_change_notification' => array (
		'sentence' => 'There is a new promotion request for contest',
		'type' => 'Type',
	),
	
	'proposal_change_notification_object' => 'Proposal change',
	'proposal_change_notification' => array (
		'hi' => 'Hi',
		'sentence1' => 'Here ours proposal for contest',
		'activated_influencers' => 'Activated influencer',
		'estimated_post_reach' => 'Estimated post reach',
		'estimated_new_leads' => 'Estimated new leads',
		'estimated_downloads' => 'Estimated downloads',
	),
	'confirm_proposal_notification_object' => 'Proposal confirmed',
	'confirm_proposal_notification' => array (
		'sentence' => 'Promotion proposal confirmed for the contest',
	),
	'refuse_proposal_notification_object' => 'Proposal refused',
	'refuse_proposal_notification' => array (
		'sentence' => 'Promotion proposal refused for the contest',
	),
	'object_request_confirmation' => 'Promotion confirmation',
	'request_confirmation' => array (
		'sentence1' => 'You must confirm the promotion for the contest',
		'sentence2' => 'starting',
	),
	'object_contest_start' => 'Your Contest with BuzzMyBrand has started!',
	'contest_start_sentence_1' => 'Hi',
	'contest_start_sentence_2' => '! The contest',
	'contest_start_sentence_3' => 'has started. Check how it is going clicking the link below',
	'contest_start_sentence_4' => 'We wish you a pleasant contest experience!',
	
	'object_contest_end' => 'Your Contest with BuzzMyBrand is over!',
	'contest_end_sentence_1' => 'Hi',
	'contest_end_sentence_2' => '! The contest',
	'contest_end_sentence_3' => 'has ended. Check out the data and analytics at the link below',
	'contest_end_sentence_4' => 'Thanks for using BuzzMyBrand for your social media marketing campaigns.',
	
	'object_contest_standard_approved' => 'Your Contest has been approved!',
	'contest_standard_approved_sentence' => 'Hi Buzzer! Your contest has been approved and will start on ... ',
	
	'object_contest_standard_not_approved' => 'Your Contest has not been approved!',
	'contest_standard_not_approved_sentence1' => 'Hi Buzzer! Your contest has not been approved because it doesn’t follow our T&C.',
	'contest_standard_not_approved_sentence2' => 'Our team will get in touch with you to settle the issue within the next 24 hours.',
	'contest_standard_not_approved_sentence3' => 'Regards,',
	'contest_standard_not_approved_sentence4' => 'The BuzzMyBrand team',
	
	'object_contest_instant_not_approved' => 'Your contest has not been approved.',
	'contest_instant_not_approved_sentence1' => 'Hi Buzzer! Your contest has not been approved.',
	
	'object_contest_instant_approved' => 'Your contest has been approved.',
	'contest_instant_approved_sentence' => 'Hi Buzzer! Your contest has been approved.',
	
	'resend_password_object' => 'Password forgotten',
	'resend_password' => 'You can use this temporary passsword',
	
	'contest_edit_scheduled_subject'    => 'Contest scheduled',
	
    'contest_edit_scheduled_sentence_1' => 'Hello',
    'contest_edit_scheduled_sentence_2' => 'your contest',
    'contest_edit_scheduled_sentence_3' => 'has been scheduled and will start at',
    'contest_edit_scheduled_sentence_4' => 'on',
    'contest_edit_scheduled_sentence_5' => 'You can still modify it in the dashboard until it starts.',
    'contest_edit_scheduled_sentence_6' => 'Wish you a pleasent Contest experience!',
    'contest_edit_scheduled_sentence_7' => 'Here are listed the details of your contest:',
	'contest_start_sentence_no_rewards' => 'No rewards',
	'contest_start_sentence_custom_template' => 'Custom Template',

    'contest_edit_draft_subject'          => 'Contest saved',
    'contest_edit_draft_sentence_1'       => 'Hello',
    'contest_edit_draft_sentence_2'       => 'your contest',
    'contest_edit_draft_sentence_3'       => 'has been modified and in status',
    'contest_edit_draft_sentence_4'       => 'You can still modify it in the dashboard until it starts.',
    'contest_edit_draft_sentence_5'       => 'Wish you a pleasent Contest experience!',
    'contest_edit_draft_status_scheduled' => 'scheduled',
    'contest_edit_draft_status_draft'     => 'draft',
	
	'approve_content_1' => 'Hello',
	'approve_content_2' => 'you have',
	'approve_content_7' => 'entries to approve for your contest',
	'approve_content_3' => 'Click',
	'approve_content_4' => 'here',
	'approve_content_5' => 'to check them out!',
	'approve_content_6' => 'Regards',
	'object_approve-content' => 'BuzzMyBrand: contents to approve',
];

