<?php

return [
    'invoice' => 'Invoice',
    'invoice_date' => 'Invoice date',
    'items' => 'Items',
    'description' => 'Description',
    'total' => 'Total',
    'vat' => 'VAT',
    'total_te' => 'Total (VAT excl.)',
    'transactions' => 'Transactions',
    'transaction_date' => 'Transaction date',
    'payment_gateway' => 'Payment gateway',
    'transaction_id' => 'Transaction ID',
    'amount' => 'Amount',
    'to_be_paid' => 'To be paid',
	'credit_card_used'=> 'Credit Card used',
	'coupon_code' => 'Coupon Code'
];