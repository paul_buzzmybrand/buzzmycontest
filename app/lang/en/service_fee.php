<?php

return [
    'invoice_description' => 'Credits for :credits :service_type (:varying_fee/CPM)',
    'buy_now' => 'BUY NOW',
    '1st_contest_on_us' => '1st one is on us!',
    'performance_based_price' => 'Performance based',
];