<?php

return [

	'homepage' => 'Make your brand viral on social network',
	'social_buzz' => 'Social Buzz: the new social marketing tool',
	'contacts' => 'Contact us or pay us a visit',
	'work_with_us' => 'Work with us, bring value to our project',
	'about_us' => 'Why BuzzMyBrand is a must for your social marketing',
	'blog' => 'Our Blog: update on social contest',
	'media_kit' => 'For social marketing focused reviews',
	'faq' => 'FAQ',
	'careers' => 'Careers',
	'cancellationpolicy' => 'Cancellation Policy',
	'invoices' => 'Invoices',

	//metadata description
	'homepage_d' => 'BuzzMyBrand is a platform for social marketing to launch contests on social networks. LAUNCH YOUR CONTEST and engage your social media community',
	'social_buzz_d' => 'Social Buzz is a technology for social marketing that measures the virality of single post on social networks generating the ranking of the contest',
	'contacts_d' => 'For info and contacts send an email to info@buzzmybrand.it or pay us a visit at Via Chiossetto 12 in Milan',
	'work_with_us_d' => 'At BuzzMyBrand we are always looking for motiveted and skilled people interested in social marketing and social networks',
	'about_us_d' => 'On BuzzMyBrand you can launch instant contest on the most popular social networks and exploit the best technology for your social marketing',
	'blog_d' => 'Follow us on our blog! We post stories and opinions about the world of contest and social marketing',
	'media_kit_d' => 'At this link find our press releases. For more details contact us, we are at your disposal',
	'faq_d' => 'BuzzMyBrand is a platform providing management services for social networks. With BuzzMyBrand you can launch multiplaform photo/video contest',
];
