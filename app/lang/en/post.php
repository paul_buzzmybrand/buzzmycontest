<?php

return array(
	'community_engagement_sentence_post' => '%contestname% %type% Contest! Can you do better? Visit %companylink%  and participate to win amazing prizes with %companyname%! %hashtag%',
	'community_engagement_sentence_userpost' => 'Here is my entry for %contestname% %type% Contest. Can you do better? Visit %companylink%  and participate to win amazing prizes with %companyname%! %hashtag%',
	'brand_awareness_sentence_post' => '%companyname% is now launching %contestname% %type% Contest! Visit %companylink% and participate to win amazing prizes! %hashtag%',
	'brand_awareness_sentence_userpost' => '%companyname% presents %contestname% %type% Contest. Visit %companylink% and participate to win amazing prizes with %companyname%! %hashtag%',
	'brand_loyalty_sentence_post' => '%contestname% %type% Contest! Can you do better? Visit %companylink% and participate to win amazing prizes with %companyname%!%hashtag%',
	'brand_loyalty_sentence_userpost' => 'Loving this %contestname% %type% Contest. Visit %companylink%  and participate to win amazing prizes with %companyname%! %hashtag%',
	'on-line_sales_sentence_post' => '%contestname% %type% Contest! Participate and Redeem your Discount Voucher with %companyname%! %hashtag%',
	'on-line_sales_sentence_userpost' => '%contestname% %type% Contest! This is my entry to Redeem my Discount Voucher with %companyname%! %hashtag%',
	'off-line_sales_sentence_post' => 'Live from %companyname% Shop for the %contestname% %type% Contest! Participate and Redeem your Discount Voucher now! %hashtag%',
	'off-line_sales_sentence_userpost' => 'Live from %companyname% Shop for the %contestname% %type% Contest! Participate and Redeem your Discount Voucher now! %hashtag%',
	'leads_generation_sentence_post' => '%contestname% %type% Contest! Can you do better? Visit %companylink%  and participate to win amazing prizes with %companyname%! %hashtag%',
	'leads_generation_sentence_userpost' => 'Here is my entry for %contestname% %type% Contest. Can you do better? Visit %companylink%  and participate to win amazing prizes with %companyname%! %hashtag%',
	'app_installs_sentence_post' => '%contestname% %type% Contest! Can you do better? Download %storelinks% to participate and win amazing prizes with %companyname%! %hashtag%',
	'app_installs_sentence_userpost' => 'Here is my entry for %contestname% %type% Contest. Can you do better? Download %storelink% to participate and win amazing prizes with %companyname%! %hashtag%',
	'tw_community_engagement_sentence_post' => '%contestname% type Contest! Can you do better? Visit %companylink% %hashtag%',
	'tw_community_engagement_sentence_userpost' => 'Can you do better? Visit %companylink% %hashtag%',
	'tw_brand_awareness_sentence_post' => '%companyname% is now launching %contestname% %type% Contest! %companylink% %hashtag%',
	'tw_brand_awareness_sentence_userpost' => '%companyname% presents %contestname% %type% Contest. %companylink%', 
	'tw_brand_loyalty_sentence_post' => '%contestname% %type% Contest! Can you do better? %companylink%', 
	'tw_brand_loyalty_sentence_userpost' => 'Loving this %contestname% %type% Contest. %companylink%', 
	'tw_on-line_sales_sentence_post' => '%contestname% %type% Contest! Redeem your Discount! %companylink%',
	'tw_on-line_sales_sentence_userpost' => '%contestname% %type% Contest! Redeem your Discount Voucher with %companyname%! %companylink%',
	'tw_off-line_sales_sentence_post' => 'Live from %companyname% Shop for the %contestname% %type% Contest! %hashtag% %companylink%',
	'tw_off-line_sales_sentence_userpost' => 'Live from %companyname% Shop for the %contestname% %type% Contest! %hashtag% %companylink%', 
	'tw_leads_generation_sentence_post' => '%contestname% %type% Contest! Can you do better? %companylink% %hashtag%',
	'tw_leads_generation_sentence_userpost' => 'My entry for %contestname% %type% Contest. Can you do better? %companylink% %hashtag%',
	'tw_app_installs_sentence_post' => '%contestname% %type% Contest! Can you do better? %storelinks% to participate %hashtag%',
	'tw_app_installs_sentence_userpost' => 'My entry for %contestname% %type% Contest. Can you do better? Download %storelink% to participate %hashtag%',
	'yt_bmb_community_engagement_sentence_post' => 'Enjoy with us',
	'yt_bmb_community_engagement_sentence_userpost' => 'Enjoy with us',
	'yt_bmb_brand_awareness_sentence_post' => 'Enjoy with us',
	'yt_bmb_brand_awareness_sentence_userpost' => 'Enjoy with us',
	'yt_bmb_brand_loyalty_sentence_post' => 'Enjoy with us',
	'yt_bmb_brand_loyalty_sentence_userpost' => 'Enjoy with us',
	'yt_bmb_on-line_sales_sentence_post' => 'Enjoy with us',
	'yt_bmb_on-line_sales_sentence_userpost' => 'Enjoy with us',
	'yt_bmb_off-line_sales_sentence_post' => 'Enjoy with us',
	'yt_bmb_off-line_sales_sentence_userpost' => 'Enjoy with us',
	'yt_bmb_leads_generation_sentence_post' => 'Enjoy with us',
	'yt_bmb_leads_generation_sentence_userpost' => 'Enjoy with us',
	'yt_bmb_app_installs_sentence_post' => 'Enjoy with us',
	'yt_bmb_app_installs_sentence_userpost' => 'Enjoy with us',
	'yt_cmp_community_engagement_sentence_post' => 'Enjoy with us',
	'yt_cmp_community_engagement_sentence_userpost' => 'Enjoy with us',
	'yt_cmp_brand_awareness_sentence_post' => 'Enjoy with us',
	'yt_cmp_brand_awareness_sentence_userpost' => 'Enjoy with us',
	'yt_cmp_brand_loyalty_sentence_post' => 'Enjoy with us',
	'yt_cmp_brand_loyalty_sentence_userpost' => 'Enjoy with us',
	'yt_cmp_on-line_sales_sentence_post' => 'Enjoy with us',
	'yt_cmp_on-line_sales_sentence_userpost' => 'Enjoy with us',
	'yt_cmp_off-line_sales_sentence_post' => 'Enjoy with us',
	'yt_cmp_off-line_sales_sentence_userpost' => 'Enjoy with us',
	'yt_cmp_leads_generation_sentence_post' => 'Enjoy with us',
	'yt_cmp_leads_generation_sentence_userpost' => 'Enjoy with us',
	'yt_cmp_app_installs_sentence_post' => 'Enjoy with us',
	'yt_cmp_app_installs_sentence_userpost' => 'Enjoy with us',

);

?>


