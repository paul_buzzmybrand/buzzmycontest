<?php

return array(
    
    
    //widget
    'widget_signup_text' => 'You need to sign in before using recording widget',
    'widget_record' => 'Record',
    'confirm_video' => 'You have successfully uploaded your video. Thank you!',
    'start_record' => 'Record',
    'stop_record' => 'Stop',
    'play_video' => 'Preview',
    'stop_video' => 'Pause',
    'save_video' => 'Upload',
    'record_again_video' => 'Do you want record again?',
	'confirm_video_CBC' => 'You have successfully uploaded your video. Close this window and go back to the form',
	'alert_miss_title' => 'Please insert a title before saving video!',
		
	//widget CBC
	'project_name' => 'Project Name',
	'email_CBC' => 'Email (same used for submission)',
    
    //common
    
    'error_communicating_facebook' => 'There was an error communicating with Facebook',
	'mail_already_participate' => 'Questa e-mail ha già partecipato',
	'phone_already_inserted' => 'Questo numero di telefono è stato già usato',
	'fbaccount_already_participate' => 'Questo account ha già partecipato',
    'facebook_error' => 'There was an error',
    'twitter_request_token_error' => 'Error receiving request token',
    'twitter_connection_error' => 'Coonection with twitter Failed',
    'Join_the_contest' => 'Join the contest',
    'sign_up' => 'Sign up',
    'sign_in' => 'Sign in',
    'log_in' => 'Log in',
    'log_out' => 'Log out',
    'log_in_with_facebook' => 'Log in with Facebook',
    'log_in_with_twitter' => 'Log in with Twitter',
    'form_username' => 'username',
    'form_password' => 'password',
    'form_password_r' => 'confirm password',
    'form_company' => 'company name',
    'form_contact' => 'contact name',
    'form_address' => 'address',
    'form_zip' => 'zip code',
    'form_city' => 'city',
    'form_email' => 'email',
    'form_phone' => 'telephone',
    'form_fax' => 'fax number',
    'form_website' => 'website address',
    'form_accept' => 'Signing up you accept JMT',
    'close' => 'Close',
    'error_occurred' => 'An error occurred, please try again',
    'uploading' => 'Uploading',
    'social' => 'Social',
    'yes' => 'Yes',
    'no' => 'No',
    'delete' => 'Delete',
    'your_account' => 'Your account',
    'change_picture' => 'Change Picture',
    'language' => 'Language',
	'associate_yt_account' => 'Link with YouTube',
	'disconnect_yt_account' => 'Unlink YouTube',
	'company' => 'Company',
    'choose_new_photo' => 'New Photo',
    'print' => 'Print',
    'logged' => 'You are logged',
    'not_logged' => 'Not logged in',
    'all_contests' => 'All Contests',
    'shoot' => 'Shoot',
    'back' => 'back',
    
    //form message
    'required' => 'This field is required',
    'min_length' => 'Please enter at least :number characters',
    'max_length' => 'Please enter no more than :number characters',
    'wrong_data_login' => 'Either username or password is wrong. Please try again.',
    'comapny_name_registered'  => 'This company name is already registered',
    'contest_date' => 'Contest must to last longer than 30 days',
    'confirm_register' => 'Thank You! Check your e-mail.',
    'confirm_register_company' => 'Welcome in system!',
    'username_registered' => 'This username is already registered',
    'email_registered' => 'This email is already registered',
    
    //company menu
    'our_services' => 'Our Services',
    'client_partners' => 'Clients & Partners',
    'success_stories' => 'Success Stories',
    'team' => 'Team',
    'launch_contest' => 'Launch a contest',
    'dashboard' => 'Dashboard',
    
    //footer
    'app_store' => 'App Store',
    'google_play' => 'Google Play',
    'terms_service' => 'Terms of Service',
    'privacy_police' => 'Privacy Police',
    'contact' => 'Contact',
    
    //new contest form
    'contest_name' => 'Contest name:',
    'start_date' => 'Start Date:',
    'end_date' => 'End Date:',
    'app_picture' => 'Upload App Picture:',
    'web_picture' => 'Upload Web Picture:',
    'brief_rules' => 'Brief and Rules:',
	'start time' => 'Start time:',
	'end time' => 'End time:',
	'facebook page' => 'Click here for the Facebook page',
    'judges' => 'Judges:',
	'judgesCriteria' => 'Judging Criteria:',
    'entries_user' => 'Entries/User:',
    'location' => 'Location:',
    'reward_type' => 'Reward Type:',
    'rewards' => 'Rewards:',
    '1st' => '1st:',
    '2nd' => '2nd:',
    '3rd' => '3rd:',
    'internal_judging' => 'Internal Judging',
    'jmt_score' => 'JMT Score',
    'one_per_user' => 'One per user',
    'unlimited_per_user' => 'Unlimited per user',
    'submit' => 'Submit',
    'draft' => 'Draft',
    'cash' => 'Cash',
    'vouchers' => 'Vouchers',
    'products_services' => 'Products/Services',
    'others' => 'Others',
    'voucher_description' => 'Voucher Description',
    'other_description' => 'Description',
    'products_services_desc' => 'Products/Services Description',
    'choose_photo' => 'Select Photo',
    'free_plan' => 'Free Plan',
    'plan_1' => 'Plan 1',
    'plan_2' => 'Plan 2',
    'function_1' => 'Function 1',
    'function_2' => 'Function 2',
    'function_3' => 'Function 3',
    'function_4' => 'Function 4',
    'function_5' => 'Function 5',
    'function_6' => 'Function 6',
    'function_7' => 'Function 7',
    'function_8' => 'Function 8',
    
    
    
    
    
    //titles
    'new_contest_step_one' => 'Step 1 - General Info',
    'new_contest_step_two' => 'Step 2 - Adds On',
    'payment_confirm' => 'Thanks for your submission! We are reviewing it and come back to you!',
    'dashboard' => 'Dashboard',
    'choose_event' => 'Choose Your Contest/Event :',
    'analytics' => 'Analytics',
    'engagement_details' => 'Engagement Details',
    'delete_account' => 'Delete account',
    'delete_account_desc' => 'Do you want delete your account?',
    'contest_list' => 'Contests/Events List',
    'my_contests' => 'My Contests',
    'favourited_contests' => 'Featured Contests',
    'search_contests' => 'Search Contests',
    'team' => 'Team',
   
    
    
    
    //dashboard stats
    'video_collected' => 'Number od videos collected:',
    'user_engaged' => 'User engaged:',
    'average_video' => 'Average Views/Video :',
    'total_views' => 'Total Views:',
    'induced_views' => 'Induced Virality Views:',
    'campaign_score' => 'Campaign JMT Score:',
    'video_highest_score' => 'Video with highest scores:',
    'facebook_likes' => 'Facebook Likes:',
    'facebook_coments' => 'Facebook Comments:',
    'facebook_shares' => 'Facebook Shares:',
    'twitter_favouriters' => 'Twitter Favourites:',
    'twitter_replies' => 'Twitter Replies:',
    'twitter_retweets' => 'Twitter Retweets:',
    'youtube_likes' => 'Youtube Likes:',
    'youtube_comments' => 'Youtube Comments:',
    'youtube_shares' => 'Youtube Shares',

    //description
    'desc_contest_name' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
    'desc_start_date' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
    'desc_end_date' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
    'desc_upload_web' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
    'desc_upload_app' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
    'desc_brief_rules' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
    'desc_judges' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
    'desc_entries' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
    'desc_location' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
    'desc_reward_type' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
    'desc_rewards' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
    
    //contest
    'copy_link' => 'Copy Link',
    'search' => 'Search',
    'keyword' => 'keyword',
    'start' => 'Start:',
    'end' => 'End:',
    'no_results' => 'No results',
    
	//photo after approval
	'photo_not_approved' => 'YOUR PHOTO HAS NOT BEEN APPROVED',
    'photo_approved' => 'YOUR PHOTO HAS BEEN APPROVED',
    'facebook_page' => 'Facebook Page',
    'click_increase_score' => 'CLICK HERE TO INCREASE THE SCORE!',
    'tag_your_friends' => 'Tag your friends directly from your post',
    'photo_needs_approval' => 'The photo needs approval',
	
	//native video notification
	'video_approved_native' => 'Your video entitled :videoname for :contestdescription has been approved!',
	'video_not_approved_native' => 'Your video entitled :videoname for :contestdescription has not been approved!',
	
	//description own facebook wall
	'descriptionOwnWall' => "Tag your friends, increase the score and win!! :hashtags Download the APP Android :GooglePlayLinkApp, iOS :iTunesStoreLinkApp and win amazing prizes!",
	
	//facebook notification
	'video_approved' => 'YOUR VIDEO HAS BEEN APPROVED',
	'video_not_approved' => 'YOUR VIDEO HAS NOT BEEN APPROVED',
	'share_to_improve_1' => 'SHARE',
	'share_to_improve_2' => 'TO IMPROVE YOUR SCORE',
	'invite' => 'INVITE',
	'links' => 'LINKS',
	
	
	//mass notifications
	'Type notification missing' => 'Type notification missing',
	'Exception occurred' => 'Exception occurred',
	'Facebook Notification Done' => 'Facebook Notification Done',
	'Notifications statistics' => 'Facebook Notification Succeded: :successFacebookNotification, Facebook notification failed :errorFacebookNotification, Mobile Notification Succeded: :successMobileNotification, Mobile Notification Failed :errorMobileNotification',
	
	// Delete
	'Delete Done' => 'Delete Done',
	
	
	//essay after approval
	'essay_not_approved' => 'YOUR ESSAY HAS NOT BEEN APPROVED',
    'essay_approved' => 'YOUR ESSAY HAS BEEN APPROVED',
	
);

?>


