<?php

return [
    'YOUR_CONTESTS' => 'I tuoi contest',
    'CHOOSE_CONTESTS_TO_MANAGE' => 'Scegli i contest che vuoi gestire',

    'FILTER' => [
    	'IN_PROGRESS' => 'In corso',
    	'OVER' => 'Conclusi',
    	'SCHEDULED' => 'Programmati',
    	'DRAFT' => 'Bozze',
    	'ALL' => 'Tutti'
    ],

    'choose_a_facebook_page' => 'Seleziona la pagina Facebook sulla quale inserire il TAB del contest',
    'logout' => 'Esci',
];