<?php

return [

	'homepage' => 'Rendi il tuo Brand virale con un Social Contest - BuzzMyBrand',
	'social_buzz' => 'Social Buzz: the new social marketing tool',
	'contacts' => 'Contact us or pay us a visit',
	'work_with_us' => 'Work with us, bring value to our project',
	'about_us' => 'Why BuzzMyBrand is a must for your social marketing',
	'blog' => 'Our Blog: update on social contest',
	'media_kit' => 'For social marketing focused reviews',
	'faq' => 'FAQ',
	'careers' => 'Lavora con noi',
	'cancellationpolicy' => 'Politiche di rimborso',
	'invoices' => 'Fatturazione',
	'sitemap' => 'Mappa del sito',
	'about' => 'Su di noi',

	//metadata description
	'homepage_d' => 'BuzzMyBrand è una piattaforma di social marketing per lanciare contest sui social network. LANCIA IL TUO CONTEST e coinvolgi la tua social media community',
	'social_buzz_d' => 'Social Buzz è una tecnologia per il social marketing che misura la viralità dei singoli post sui social network producendo la classifica del contest',
	'contacts_d' => 'Per info ed contatti scrivici all’indirizzo info@buzzmybrand.it oppure vieni a prenderti un caffè da noi in Via Chiossetto 12 a Milano',
	'work_with_us_d' => 'A BuzzMyBrand siamo sempre alla ricerca di collaboratori interessati al social marketing e che conoscano il mondo dei social network',
	'about_us_d' => 'Su BuzzMyBrand puoi lanciare instant contest sui social network più diffusi e sfruttare la migliore tecnologia per il tuo social marketing',
	'blog_d' => 'Seguici sul nostro blog! Pubblichiamo storie ed opinioni sul mondo dei contest e del social marketing',
	'media_kit_d' => 'A questo link trovi i nostri comunicati stampa. Per ulteriori dettagli contattaci e saremo a tua disposizione',
	'faq_d' => 'BuzzMyBrand è un portale che offre tecnologie gestionali per social network. Con BuzzMyBrand puoi lanciare foto/video contest in maniera integrata e simultanea',
	'sitemap_d' => 'La mappa del sito',
];
