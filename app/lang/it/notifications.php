<?php

return [
	
	'change_promotion_proposal' => array(
		'text' => 'Ti abbiamo inviato la nostra proposta promozionale. Controlla ora!',
	),
	
	'contest_start' => 'Ciao Buzzer! Il tuo contest è appena iniziato. Controlla la sezione Analytics per misurarne la performance.',
	'contest_end' => 'Ciao Buzzer! Il tuo contest è terminato. Controlla la sezione Analytics per misurarne la performance.',
	'contest_approved' => 'Ciao Buzzer! Il tuo contest è stato approvato ed inizierà il ... ',
	'contest_instant_approved' => 'Ciao Buzzer! Il tuo contest è stato approvato.',
	'contest_not_approved' => 'IL TUO CONTEST NON È STATO APPROVATO',
];