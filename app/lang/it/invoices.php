<?php

return [
    'invoice' => 'Fattura',
    'invoice_date' => 'Data fattura',
    'items' => 'Articoli',
    'description' => 'Descrizione',
    'total' => 'Totale',
    'vat' => 'IVA',
    'total_te' => 'Totale (IVA escl.)',
    'transactions' => 'Transazioni',
    'transaction_date' => 'Data transazione',
    'payment_gateway' => 'Gateway di pagamento',
    'transaction_id' => 'ID Transazione',
    'amount' => 'Importo',
    'to_be_paid' => 'Saldo',
	'credit_card_used'=> 'Carta di Credito',
	'coupon_code' => 'Codice Coupon'
];