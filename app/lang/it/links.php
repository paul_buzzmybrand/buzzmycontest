<?php

return [

	'social_buzz_title' => 'Il Social Buzz',
	'social_buzz_description' => 'La grande innovazione portata sul mercato da :before BuzzMyBrand :after è il :before Social Buzz ® :after, una tecnologia proprietaria in grado di misurare il successo sui social network dei contenuti generati dagli utenti e di creare una classifica in tempo reale dei contest in corso. Questo rivoluziona i meccanismi attuali per la decisione dei vincitori e riesce ad inserirsi nelle strettissime guidelines di Facebook oltre che nella normativa italiana. :br Per vincere i contest lanciati dalle aziende con :before BuzzMyBrand :after gli utenti devono ricevere il maggior numero possibile di like, condivisioni e commenti sui maggiori social media come :before Facebook, Twitter e Instagram :after. Il nostro software valuterà il successo di una foto o di un video su ogni social media e fornirà un numero che ne quantifica il valore. E quindi gli utenti diventeranno i principali sponsor del brand che ha lanciato il contest.',
								 
	'about_title' => 'Su di noi',
	'about_description' => ':before BuzzMyBrand :after è una piattaforma che consente di lanciare video e foto contest (anche istantanei) su Facebook, Twitter e Youtube. :br Inoltre con :before BuzzMyBrand :after è possibile lanciare instant contest in base ai temi più caldi della rete grazie alla nostra tecnologia che mette in contatto le tendenze del momento con le tue esigenze ed I tuoi obiettivi aziendali. :brI social media sono oramai divenuti gli odierni siti web ed i brand sono alla continua ricerca di nuovi modi per coinvolgere i clienti e creare fedeltà al loro marchio sia sulle loro pagine ufficiali nei social media. Inoltre, in occasione di eventi pubblici che catturano l\'attenzione di milioni di utenti e di conseguenza riempiono i social media con migliaia di contenuti generati dagli utenti relativi a tali eventi, le aziende non sanno come penetrare questo “buzz” e "brandizzare" tali contenuti. :br :before BuzzMyBrand :after porta una proposta nuova ed efficace per le aziende: una soluzione semplice da usare dal concept fino alla valutazione della campagna, competitiva nel prezzo (più economica rispetto ai concorrenti, ma con maggiore valore) e efficacia misurabile nei risultati.:br',
	                       
	'compliance_title' => 'Note legali',
	
	'faqs_title' => 'FAQ',
	                   
	'careers_title' => 'Siamo sempre alla ricerca di persone motivate e competenti',
	'careers_description' => 'A BuzzMyBrand siamo sempre alla ricerca di un ampio ventaglio di tipologie di programmatori e competenti grafici e designer. Siamo consapevoli che il nostro successo dipende dalla qualità dei nostri collaboratori e dalla loro competenza e motivazione. :brSe interessati inviate il vostro CV a info@buzzmybrand.it',
	                         
	'contacts_title' => 'Contatti',
	'contacts_enquiries' => 'Richieste generiche',
	'contacts_callus' => 'Chiamaci',
	'contacts_offices' => 'Uffici',
	'contacts_reg_office' => 'Sede Legale',
	'cancellationpolicy_title' => 'Politica di cancellazione account',
	'invoices_title' => 'Fatturazione e politiche di rimborso',
	
	'awards' => 'premi',
	'terms' => 'termini',
	'contacts' => 'contacts',
	'careers' => 'lavoro',
	'about' => 'su-di-noi',
	'compliance' => 'note-legali',
	'our_flow' => 'nostro-flow',
	'our_flow_title' => 'IL NOSTRO FLOW',
	'cancellationpolicy' => 'cancellationpolicy',
	'invoices' => 'fatturazione',
	];