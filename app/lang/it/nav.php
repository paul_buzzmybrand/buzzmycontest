<?php

return [

	'toggle_navigation' => 'Toggle navigation',
	'home' => 'Home',
	'how_it_works' => 'Come funziona',
	'how_we_are_different' => 'Il nostro valore aggiunto',
	'pricing' => 'Tariffe',
	'launch_campaign' => 'Lancia contest',
	'login' => 'ENTRA',
	'signup' => 'REGISTRATI',
	'password_forgotten' => 'Password dimenticata?',
	'back_to_login' => 'Indietro',
	"recover_your_password" => "Recupera la password",
	"contact_us" => "CONTATTACI",
	'recovery_email_sent' => 'Controlla la tua posta',
];
