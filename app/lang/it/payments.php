<?php

return [
    'duplicate_initial_payment' => 'Hai già una carta di credito registrata',
    'user_has_no_active_card' => 'Non hai nessuna carta di credito registrata',
    'generic_error_message' => 'Impossibile effettuare la transazione. Per favore, riprova tra qualche minuto.',
    'could_not_remove_credit_card' => 'Impossibile rimuovere la carta di credito. Contatta il nostro staff.',

    'email' => [
    	'expiration_subject' => 'La tua carta di credito sta per scadere',
    ]
];