<?php

return [
    'invoice_description' => ':credits crediti per :service_type (:varying_fee/CPM)',
    'buy_now' => 'ACQUISTA',
    '1st_contest_on_us' => 'Il primo è gratis',
    'performance_based_price' => 'In base alle performance',
];