<?php

return [

	'object_video_contest_approved' => 'Il Tuo Video è stato approvato!',
	'object_photo_contest_approved' => 'La Tua Foto è stata approvata!',
	'object_essay_contest_approved' => 'Il Tuo Post è stato approvato!',
	
	'object_video_contest_not_approved' => 'Il Tuo Video non è stato approvato!',
	'object_photo_contest_not_approved' => 'La Tua Foto non è stata approvata!',
	'object_essay_contest_not_approved' => 'La Tuo Post non è stato approvato!',
	
	"invite" => array(
		"subject" => "Hai ricevuto un invito per un contest!",
		"sentence1" => "Ciao",
		"sentence2" => "Il tuo amico",
		"sentence3" => "ti ha invitato a partecipare al Contest",
		"contest_en" => "",
		"sentence4" => "Per saperne di più clicca",
		"sentence5" => "qui",
	),
	
	'admin_notify_email_object' => 'New user has been successfully registered.',
	
	'admin_notify_email_1' => 'User',
	'admin_notify_email_2' => 'has been successfully registered',
	'admin_notify_email_3' => 'Email',
	'admin_notify_email_4' => 'Company',
	'admin_notify_email_5' => 'Job Title',
	'admin_notify_email_6' => 'Telephone',
	'admin_notify_email_7' => 'Country',
	
	'admin_notify_email_budget_object' => 'User has been successfully entered budget.',
	'admin_notify_email_budget_1' => 'Data regarding user that wants promotion for his contest',
	
	'welcome_email_object' => 'Benvenuto in BuzzMyBrand',
	
	'welcome_email_1' => 'Ciao',
	'welcome_email_2' => 'Benvenuto in BuzzMyBrand!',
	'welcome_email_3' => 'Con la nostra piattaforma di social contest, tu sarai in grado di lanciare il tuo contest velocemente e facilmente.',
	'welcome_email_4' => 'Clicca ',
	'welcome_email_9' => 'qui',
	'welcome_email_8' => 'per verificare il tuo account o copia e incolla il seguente link nel tuo browser:',
	'welcome_email_5' => 'Se hai bisogno di assistenza contattaci rispondendo direttamente a questa e-mail',
	'welcome_email_6' => 'Cordialmente',
	'welcome_email_7' => 'BuzzMyBrand Team',
	
	'promotion_change_notification_object' => 'Promozione aggiornata',
	'promotion_change_notification' => array (
		'sentence' => 'C\'è una richiesta di promozione per il contest',
		'type' => 'Tipo',
	),
	
	'proposal_change_notification_object' => 'Proposta effettuata',
	'proposal_change_notification' => array (
		'hi' => 'Ciao',
		'sentence1' => 'Ciao la nostra proposta per il contest',
		'activated_influencers' => 'Activated influencer',
		'estimated_post_reach' => 'Estimated post reach',
		'estimated_new_leads' => 'Estimated new leads',
		'estimated_downloads' => 'Estimated downloads',
	),
	'confirm_proposal_notification_object' => 'Proposta confermata',
	'confirm_proposal_notification' => array (
		'sentence' => 'Proposta confermata per il contest',
	),
	'refuse_proposal_notification_object' => 'Proposta rifiutata',
	'refuse_proposal_notification' => array (
		'sentence' => 'Proposta della promozione rifiutata per il contest',
	),
	'object_request_confirmation' => 'Conferma promozione',
	'request_confirmation' => array (
		'sentence1' => 'E\' necessaria la tua conferma per la promozione del contest',
		'sentence2' => 'che inizia',
	),
	'object_contest_start' => 'Il tuo contest con BuzzMyBrand è iniziato!',
	'contest_start_sentence_1' => 'Ciao',
	'contest_start_sentence_2' => '! Il contest',
	'contest_start_sentence_3' => 'è iniziato. Per controllare l\'andamento della campagna, collegati alla nostra dashboard cliccando qui sotto',
	'contest_start_sentence_4' => 'Vi auguriamo un contest di successo!',
	
	'object_contest_end' => 'Il tuo contest con BuzzMyBrand è terminato',
	'contest_end_sentence_1' => 'Ciao',
	'contest_end_sentence_2' => '! Il contest',
	'contest_end_sentence_3' => 'è terminato. Puoi controllare i risultati cliccando sul link sottostante',
	'contest_end_sentence_4' => 'Ti ringraziamo per aver utilizzato BuzzMyBrand per le tue campagne di social-media marketing.',
	
	
	'object_contest_standard_approved' => 'Il tuo contest è stato approvato!',
	'contest_standard_approved_sentence' => 'Ciao Buzzer! Il tuo contest è stato approvato ed inizierà il ... ',
	
	'object_contest_standard_not_approved' => 'Il tuo contest non è stato approvato!',
	'contest_standard_not_approved_sentence1' => 'Ciao Buzzer! Il tuo contest non è stato approvato perché non in linea con le nostre T&C.',
	'contest_standard_not_approved_sentence2' => 'Il nostro team ti contatterà entro le prossime 24 ore per risolvere il problema.',
	'contest_standard_not_approved_sentence3' => '',
	'contest_standard_not_approved_sentence4' => 'Saluti dal team di BuzzMyBrand',
	
	'object_contest_instant_not_approved' => 'Il tuo contest non è stato approvato.',
	'contest_instant_not_approved_sentence1' => 'Ciao Buzzaer! Il tuo contest non è stato approvato.',
	
	'object_contest_instant_approved' => 'Il tuo contest è stato approvato.',
	'contest_instant_approved_sentence' => 'Ciao Buzzaer! Il tuo contest è stato approvato.',
	
	'resend_password_object' => 'Password dimenticata',
	'resend_password' => 'Puoi utilizzare questa password temporanea',
	
	'contest_edit_scheduled_subject' => 'Contest programmato',

    'contest_edit_scheduled_sentence_1' => 'Ciao',
    'contest_edit_scheduled_sentence_2' => 'il tuo contest',
    'contest_edit_scheduled_sentence_3' => 'è stato lanciato ed inizierà alle',
    'contest_edit_scheduled_sentence_4' => 'il giorno',
    'contest_edit_scheduled_sentence_5' => 'Puoi modificarlo fino alla data e ora di inizio del contest dalla dashboard.',
    'contest_edit_scheduled_sentence_6' => 'Ti auguriamo un contest di successo!',
	'contest_edit_scheduled_sentence_7' => 'Ecco il riepilogo del tuo contest:',
	'contest_start_sentence_no_rewards' => 'No premi',
	'contest_start_sentence_custom_template' => 'Template personalizzato',

    'contest_edit_draft_subject'          => 'Contest salvato',
    'contest_edit_draft_sentence_1'       => 'Ciao',
    'contest_edit_draft_sentence_2'       => 'il tuo contest',
    'contest_edit_draft_sentence_3'       => 'è stato modificato ed è in stato',
    'contest_edit_draft_sentence_4'       => 'Puoi continuare a modificarlo fino alla data di inizio.',
    'contest_edit_draft_sentence_5'       => 'Ti auguriamo il meglio con il tuo contest!',
    'contest_edit_draft_status_scheduled' => 'programmato',
    'contest_edit_draft_status_draft'     => 'bozza',
	
	'approve_content_1' => 'Ciao',
	'approve_content_2' => 'hai',
	'approve_content_7' => 'entries da approvare per il tuo contest',
	'approve_content_3' => 'Clicca',
	'approve_content_4' => 'qui',
	'approve_content_5' => 'per approvarli!',
	'approve_content_6' => 'Cordialmente',
	'object_approve-content' => 'BuzzMyBrand: contenuti da approvare',
];

