<?php

return array(
	
	'community_engagement' => array (
		'label' => 'ENGAGEMENT',
		'description' => 'Condividi la campagna con il tuo pubblico e crea un legame forte tra il tuo brand ed i tuoi fan: BuzzMyBrand è la soluzione migliore per rendere avvincenti le sfide sui social a colpi di “like”.',
	),
	'brand_awareness' => array (
		'label' => 'BRAND AWARENESS',
		'description' => 'La creazione di consapevolezza del tuo brand nella massa è l’obiettivo primario nella fase iniziale del ciclo di vita del tuo prodotto. Questo processo avrà  una grossa influenza sul comportamento d’acquisto dei potenziali clienti e allargherà il tuo target di mercato.',
	),
	'brand_loyalty' => array (
		'label' => 'LOYALTY',
		'description' => 'Crea fiducia nel tuo brand: porta l’utente a consigliare il tuo brand ai suoi amici, attiva un processo di passaparola. Le operazioni di “fidelizzazione” possono essere dirette verso la tua marca (brand loyalty) o verso un tuo punto vendita (store loyalty).',
	),
	'sales' => array (
		'label' => 'VENDITE',
		'description' => 'Aumenta le tue vendite con call-to-action finalizzate all’acquisto, usa il marketing sui social per incrementare le tue vendite, crea nuovi canali e percorsi per portare i tuoi follower all’acquisto finale.',
	),
	'offline_sales' => array (
		'label' => 'VENDITE OFFLINE',
		'description' => 'Porta i tuoi fan online nei tuoi punti vendita fisici con campagne basate su coupon, scontistica ed offerte.',
	),
	'online_sales' => array (
		'label' => 'VENDITE ONLINE',
		'description' => 'Converti i partecipanti del tuo contest in utenti della tua piattaforma e-commerce.',
	),
	
);