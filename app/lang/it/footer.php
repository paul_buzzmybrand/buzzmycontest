<?php

return [

	'social_buzz' => 'il social buzz ®',
	'awards' => 'premi',
	'terms' => 'termini',
	'privacy' => 'privacy',
	'contact' => 'imprint',
	'careers' => 'lavora con noi',
	'about' => 'su di noi',
	'blog' => 'blog',
	'media_kit' => 'media kit',
	'compliance' => 'note legali',
	'our_flow' => 'il nostro flow',
	'faqs' => 'FAQ',
	'invoices' => 'fatturazione & pagamenti',
	'cancellationpolicy' => 'cancellazione account',
	'sitemap' => 'mappa del sito',
];

