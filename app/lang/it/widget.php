<?php

return array(
	
	"signup" => "signup",
	"connect" => "login",
	"actions" => "upload",
	"create" => "preview",
	"end" => "instant win",
	"share" => "CONDIVIDI IL CONTEST!",
	"share_second_sentence" => "Otterrai un punto in più per la tua entry",
	
	"signup_step" => array(
	
		"register_now" => "REGISTRATI",
		"name" => "nome",
		"surname" => "cognome",
		"birthdate" => "data di nascita",
		"email_address" => "email",
		"phone" => "telefono",
		"city" => "città",
		"cap" => "Inserisci il tuo CAP",
		"sex" => "sesso",
		"t_e_c" => "Termini d'uso",
		"name_required" => "Inserire il nome",
		"surname_required" => "Inserire il cognome",
		"birthdate_required" => "Inserire la data di nascita nel formato gg/mm/aaaa",
		"email_required" => "Inseire una email valida",
		"phone_required" => "Inserire un numero di telefono valido",
		"city_required" => "Inserire una città",
		"sex_required" => "Inserire il sesso",
		"cap_required" => "Inserire un CAP valido",
		"privacycheck_required" => "Prego cliccare sulla casella di controllo",
		"t_e_c_required" => "I termini d'uso devono essere approvati",
		"privacy_required" => "La privacy deve essere approvata",
		"checkbox_info" => "Autorizzo il trattamento dei dati e delle immagini personali ai sensi del D.lgs. 196/2003 per le finalita' del concorso. I dati forniti non saranno ceduti a terzi.",
	
		"deny_access" => "Non puoi registrarti con questa email",
		
		"selectsex" => "seleziona il sesso",
		"male" => "maschio",
		"female" => "femmina",
	),
	
	"connect_step" => array(
	
		"sentence" => "Il contest sarà lanciato su ",
		"sentence_conj" => " e ",
		"sentence_2" => "Connetti i tuoi accounts per aumentare le tue possibilità di vittoria ",
		"sentence_2_s" => "Per partecipare connetti il tuo account Facebook ",
		"proceed" => "procedi",
		"social_alert" => "Devi connetterti con almeno un social",
		"proceed_sentence" => "Clicca su procedi per continuare",
	),
	
	
	"creation_step" => array(
	
		"title" => "Inserisci titolo",
		"essay_placeholder" => "Inserisci il tuo testo qui",
		"placeholder" => "Inserisci il titolo + l'hashtag ",
		"placeholder2" => "e chiedi a tuoi amici di renderlo popolare!",
		"policy" => "Collegando il tuo account facebook acconsenti alla pubblicazione della tua foto sulla tua bacheca",
	),
	
	"end_step" => array(
	
		"sentence1" => "GRAZIE",
		"sentence2" => "LA TUA FOTO E' IN FASE DI APPROVAZIONE",
		"sentence3" => "Riceverai una notifica ad approvazione avvenuta",
		"share" => "CONDIVIDI",
		"share_second_sentence" => "per aumentare le tue chance di successo!"
	),
	


);

?>