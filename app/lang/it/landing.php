<?php

return [

	'do_not_wait' => '',
	'head_cta' => ':before Lancia :after ora il tuo contest!',
	'head_sub' => 'Contest Foto, Video e di Testo per portare effettivo valore al tuo business on-line',
	'instant_contest' => 'INSTANT CONTEST',
	'48_hours' => '48 ore',
	'standard_contest' => 'STANDARD CONTEST',
	'1_day_up' => 'Più di 2 giorni',
	'start_now' => 'INIZIA ORA',
	'get_in_touch' => 'CONTATTACI',

	// GET IN TOUCH FORM
	'form_signup_title' => 'REGISTRATI PER INIZIARE',
	'form_title' => ':before CONTATTACI :after',
	'form_name' => 'nome',
	'form_surname' => 'cognome',
	'form_email' => 'e-mail',
	'form_company' => 'azienda',
	'form_job_title' => 'posizione',
	'form_country' => 'nazione',
	'form_tel' => 'telefono',
    'form_tel_placeholder' => 'includi il prefisso +39',
	'form_message' => 'messaggio',
	'form_newsletter' => 'newsletter',
	'form_send' => 'inoltra',
	'form_sent' => 'Messaggio inoltrato!',
	'form_not_sent' => 'Messaggio non inoltrato',
	'form_password' => 'Password',
	'form_password_repeat' => 'Conferma password',
	'signup_newsletter' => 'Iscriviti alla nostra Newsletter',
	'terms_and_conditions_desc' => 'Ho letto i ',
	'terms_and_conditions' => 'Termini del Servizio e Condizio d\'uso',
	'get_started' => 'INIZIA',
	'email_not_available' => 'Email non disponibile',
	
	//COUPON
	'get_coupon' => 'COUPON GRATUITO',

	// LAUNCH
	'launch_contest' => 'LANCIA CONTEST',
	
	'launch_instant' => 'I NOSTRI :before CONTEST :after',
	'launch_instant_description' => 'La prima piattaforma in Italia che ti permette di gestire concorsi a premio sui social network, siti web ed applicazioni mobile per raggiungere i tuoi obiettivi aziendali. Potrai coinvolgere i tuoi followers e community sui tuoi profili Facebook, Twitter, Instagram, Youtube, WeChat e Snapchat ed utilizzare influencers ed hashtag del momento per rendere la tua campagna davvero popolare ed ottenere i risultati prefissati. :after',
	'instant_feature_1' => 'VISIBILITA\'',
	'instant_feature_2' => 'INSTANT TREND CONTEST',
	'instant_feature_3' => 'ENGAGEMENT',
	'instant_feature_4' => 'REACH',

	'launch_standard' => 'STANDARD :before CONTEST :after',
	'launch_standard_description' => 'Puoi lanciare Contest per raggiungere i tuoi obiettivi aziendali: :before registrati e ti contatteremo per una soluzione interamente customizzata. :after',
	'standard_feature_1' => 'DATI DEGLI UTENTI',
	'standard_feature_2' => 'ON-LINE SALES CONVERSION',
	'standard_feature_3' => 'LEAD GENERATION',
	'standard_feature_4' => 'FEDELTA\' AL BRAND',

	// HOW IT WORKDS
	'how_it_works' => 'COME :before FUNZIONA :after',
	'how_it_works_sub' => 'Trasforma i like e i follower in testimonial del tuo brand. Raccogli i dati della tua community di :before Facebook, Twitter, Instagram, Youtube, WeChat :after e :before Snapchat :after',
                          
	// ENGAGE
	'engage_title' => 'INFORMA LA COMMUNITY E PUBBLICIZZA IL CONCORSO',
	'engage_description' => 'Invita gli utenti a partecipare al contest cliccando sul tab creato nella tua pagina Facebook, tramite il link del sito web del contest o sul tuo sito. Tutti i contenuti, una volta approvati, saranno ricondivisi sulle pagine social dove il contest è attivo e votate da utenti, amici e dalle community on-line. Con il software per il punteggio Social Buzz ® i tuoi utenti entreranno in una fantastica sfida dove il vincitore sarà chi avrà inviato il contenuto divenuto più popolare in rete. Non hai bisogno di altri antiquati sistemi di voto, il contenuto più virale vincerà il contest con una procedura del tutto trasparente e certificata per il corretto funzionamento del concorso. Con le nostre soluzioni customizzate possiamo aiutarti ad ampliare la portata ed il buzz del contest attivando bloggers ed influencers che parleranno del tuo brand ed aumenteranno la partecipazione e la visibilità.',
	'engage_app' => 'APP PERSONALIZZATE :br su iOS e Android',
	'engage_facebook' => 'FACEBOOK TAB',
	'engage_twitter' => 'TWITTER',
	'engage_youtube' => 'YOUTUBE',
	'engage_website' => 'SITO WEB',
	'engage_promotion' => 'PROMOZIONE SU :br downloads delle APP :br campagne pubblicitarie :br Uso Influencers',

	// CONVERT
	'convert_title' => 'RACCOGLI I DATI E CONTROLLA LE PERFORMANCE',
	'convert_description' => 'Per ogni contest avrai la possibilità di controllare il numero totale di post ricevuti, quelli approvati e non, i canali di partecipazione (Mobile, Sito, Social Network), i dati di reach, impressions, nuovi followers dall’inizio del concorso, % di engagement rate, mi piace, commenti, condivisioni. Tutto in tempo reale e per tutti i social sui quali hai lanciato il contest. Avrai la possibilità di comunicare con tutti o per singolo partecipante tramite social network ed e-mail ed a fine campagna ti forniremo tutti i dati degli utenti che potrai riutilizzare per perfezionare le tue strategie di vendita e di marketing. Con le nostre soluzioni customizzate possiamo aiutarti ad ampliare il dettaglio dei dati di cui hai bisogno (CAP, numero di telefono, preferenze ed interessi, questionari, etc.) per rendere l’esperienza del contest ancora più coinvolgente ed utile per i tuoi clienti ed il tuo brand.',
	'convert_roi' => 'COINVOLGIMENTO di :br APP downloads :br per ROI',
	'convert_analytics' => 'STATISTICHE DEMOGRAFICHE',
	'convert_channels' => 'RENDIMENTO CANALI :br Facebook, Twitter, Instagram, APPs',

	// CONNECT
	'connect_title' => 'CREA IL CONTEST',
 	'connect_description' => 'Connettendo il/i social network dove vuoi lanciare il concorso, BuzzMyBrand creerà un sito web mobile-friendly del contest sul quale i partecipanti potranno postare le foto, i video, i testi per vincere i premi messi in palio. Il sito del contest viene automaticamente inserito in un TAB nella tua pagina Facebook ed all’interno del tuo sito aziendale con link iFrame. In fase di creazione potrai scegliere il titolo del contest, gli hashtag, il design, le date, i premi, la creatività, le modalità di approvazione/condivisione dei post degli utenti e lanciare il contest in pochi e semplici step. Se hai bisogno di una soluzione customizzata puoi contattarci ed un nostro agente di vendita sarà pronto a rispondere alle tue domande riguardanti il regolamento e fornirti soluzioni creative adatte alle tue necessità ed obiettivi.',
	'connect_photo' => 'FOTO O VIDEO CONTEST',
	'connect_dates' => 'DATE DEL CONTEST',
	'connect_brief' => 'DESCRIZIONE DEL CONTEST',
	'connect_rewards' => 'PREMI',

	// HOW ARE WE DIFFERENT
	'diff_title' => 'IL NOSTRO :before VALORE AGGIUNTO :after',
	'diff_sub' => 'Ciò che rende :before BuzzMyBrand :after unico è il focus sull’assecondare le necessità dei brand e delle agenzie con una soluzione facile e divertente da usare',
				  
	'diff_1_title' => 'CONTEST SU HASHTAG DEL MOMENTO',
	'diff_1_description' => 'Con BuzzMyBrand puoi promuovere contest basati sugli hashtag del momento. Grazie al nostro software rilevi in tempo reale hashtag geo localizzati ed in pochi secondi puoi lanciare un contest sul tema del momento aumentando la visibilità e la possibilità di ricevere più post sull’argomento hot, associando l’hashtag alla tua brand image.',

	'diff_2_title' => 'IL SOCIAL BUZZ',
	'diff_2_description' => 'Il Social Buzz ® è la nostra tecnologia proprietaria basata su un algoritmo che indica il successo di un singolo post (foto, video o testo) sui social network calcolando quanto virale e popolare esso sia. Il vincitore del contest è l’utente che ottiene il Social Buzz ® più alto.',

	'diff_3_title' => 'LANCIO CONTEST MULTIPIATTAFORMA',
	'diff_3_description' => 'Con BuzzMyBrand, nella versione automatica, puoi lanciare e gestire un contest su Facebook, Twitter, Youtube e Mini-sito mobile-friendly, in simultanea sfruttando dunque tutti i canali in entrata ed uscita. Possiamo anche lanciare contest customizzati su WeChat, SnapChat ed Instagram, Mobile APP, Sito web aziendale, iFrame.',

	'diff_4_title' => 'SOLUZIONI PERSONALIZZATE',
	'diff_4_description' => 'Abbiamo un team dedicato ai clienti che vogliono ricevere soluzioni personalizzate. In particolare offriamo assistenza legale in Italia per la stesura del regolamento del concorso e per la gestione delle pratiche amministrative, forniamo proposte di brief altamente creative e design personalizzato, customizziamo il flow utente (instant-win, doppia foto, questionari, form di contatto dedicate). ',

	'diff_5_title' => 'TARIFFE IN BASE AL RISULTATO',
	'diff_5_description' => 'Una piccola commissione iniziale ed un tariffario basato sul risultato del contest di modo che il pagamento sia in base al successo ed al risultato effettivo del tuo contest.',

	// LAUNCH CALL TO ACTION BLUE BOX
	'lcta_title' => ':before LANCIA :after ORA IL TUO CONTEST',
	'lcta_sub' => 'Contest Foto, Video e di Testo per portare effettivo valore al tuo business on-line',

	// PRICING
	'pricing_title' => 'TARIFFE',
	'pricing_sub' => 'BuzzMyBrand è semplice. :before Il tuo successo è la nostra ricompensa :after',
	'pricing_launch_btn' => 'LANCIA ORA',
	'pricing_advanced_billing' => '(alla conferma)',
	'pricing_based_on' => 'BASATO SU :beforeHASHTAGS IN TEMPO REALE:after',
	'pricing_all_included' => 'TUTTO INCLUSO',
	'pricing_tailored' => 'FATTO SU MISURA',
	'pricing_onsocial' => 'SU SOCIAL, WEB, MOBILE APP',
	'pricing_firstrow' => 'PREZZO',
	'pricing_secondrow' => 'CUSTOM',

	// - BUZZ
	'pricing_1_title' => 'PACCHETTO :before BUZZ :after',
	'pricing_1_1' => 'CONTEST FOTO, VIDEO O TESTO',
	'pricing_1_2' => 'PAGINA DEL CONTEST PERSONALIZZATA ',
	'pricing_1_3' => 'PARTECIPANTI  ILLIMITATI ',
	'pricing_1_4' => 'MENU\' CON I DATI BASE DEL CONTEST',
	'pricing_1_5' => 'INTEGRAZIONE CON I SOCIAL MEDIA (Fb, Tw, Yt) ',
	'pricing_1_6' => ' MODERAZIONE CONTENUTI',
	

	// - SUPERBUZ
	'pricing_2_title' => 'PACCHETTO :before SUPERBUZZ  :after',
	'pricing_2_1' => 'FUNZIONALITA\' DEL PACCHETTO BUZZ',
	'pricing_2_2' => 'SITO DEL CONTEST CON  DOMINIO PERSONALIZZATO',
	'pricing_2_3' => 'INDIRIZZI  E-MAIL E CONTATTI DEI PARTECIPANTI',
	'pricing_2_4' => 'REPORT DEL CONTEST A FINE CAMPAGNA',
	'pricing_2_5' => 'ANALITICHE AVANZATE',
	'pricing_2_6' => ' INTEGRAZIONE DEL CONTEST ALL\' INTERNO DEL SITO WEB',

	// - BUZZMYCONTEST
	'pricing_3_title' => 'PACCHETTO :before BUZZMYCONTEST  :after',
	'pricing_3_1' => 'FUNZIONALITA\' DEL  PACCHETTO SUPERBUZZ',
	'pricing_3_2' => 'SITO DEL CONTEST DOMINIO PERSONALIZZATO',
	'pricing_3_3' => 'LOGO DELLA TUA AZIENDA SU OGNI POST DEGLI UTENTI',
	'pricing_3_4' => 'INTEGRAZIONE CON SITO AZIENDALE ED APP TRAMITE API',
	'pricing_3_5' => 'DESIGN ED IMMAGINI  PERSONALIZZATE',
	'pricing_3_6' => 'ASSISTENZA PER LA PROCEDURA AMMINISTRATIVA IN ITALIA',
	'pricing_3_btn' => 'CONTATTACI',
	'price_spec' => 'PREZZO SU MISURA',

	
	// NEWSLETTER
	'newsletter_title' => ':before RIMANI :after SINTONIZZATO',
	'newsletter_sub' => 'iscriviti alla nostra newsletter',
	'newsletter_placeholder' => 'la tua email qui',
	'newsletter_btn' => 'ISCRIVITI'
];

