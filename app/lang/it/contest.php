<?php

return array(
    'type'       => 'tipologia',
    'language'   => 'lingua',
    'objectives' => 'obiettivi',
    'social'     => 'social',
    'title'      => 'titolo',
    'hashtag'    => 'hashtag',
    'brief'      => 'brief',
    'how_to_win' => 'come vincere',
    'rewards' => 'premi',
    'dates' => 'date',
    'flyer' => 'locandina',
    'filter' => 'filtro',
    'post_visibility' => 'visualizza sulla timeline',
    'url' => 'url',
    'url_facebook_tab' => 'url tab Facebook',
    'promotion_budget' => 'budget per la promozione',
    'package' => 'pacchetto',
    'discount' => 'sconto applicato',

);