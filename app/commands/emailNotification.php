<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class emailNotification extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'scheduler:emailNotification';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send email notification to the owner of the entries';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->line('Welcome to the Email notification generator');
		
		$contest_id = $this->argument('contest_id');
		
		Scheduler::emailNotification($contest_id);
		
		$this->line('Done');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
            array('contest_id', InputArgument::REQUIRED, 'Id of the contest'),
        );
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(			
		);
	}

}
