<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RunMakeTabCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'scheduler:MakeTab';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->line('Welcome to the Facebook Tab Generator');
		
		$id = $this->argument('FacebookPage_id');
		
		$contest_id = $this->argument('contest_id');
		
		Scheduler::makeFacebookTab($id, $contest_id);
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		/*
		return array(
		);
		*/
		return array(
            array('FacebookPage_id', InputArgument::REQUIRED, 'Id of the Facebook Page from DB'),
			array('contest_id', InputArgument::REQUIRED, 'Id of the Contest from DB'),
        );
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		);
	}

}
