<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UpdateSocialScoresCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'social:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Updates the Social Scores of All Videos';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//
            $videos=Video::all();
            foreach($videos as $video)
            {
                $filenameFull=$video->filename;
                $filenameParts=explode('.',$filenameFull);
                $fileExt=array_pop($filenameParts);
                $filename=implode('.',$filenameParts);
                
                $facebook = new Facebook(Config::get('facebook'));
                $command = array(
                    'method' => 'fql.query',
                    'query' => "select like_count, comment_count, share_count from link_stat where url='http://www.joinmethere.tv/jmtvapi/FacebookShare/$filename.html'",
                );
                $scores=$facebook->api($command);
                var_dump($scores);
                $jmtvScoreFacebook=($scores[0]['like_count'])*2+($scores[0]['comment_count'])*3+($scores[0]['share_count'])*5;
                $video->score=$jmtvScoreFacebook;
                $video->save();
                
            }
            
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			
		);
	}

}
