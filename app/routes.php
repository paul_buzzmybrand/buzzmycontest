<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::post('/bug-notification', function() {
	$inputs = Input::all();
	Log::debug("BugNotification: input parameters: ".print_r($inputs,true));
	$bug = $inputs['bug'];

	Mail::send('emails.bug_notification', ['bug' => $bug], function($message) {

						$message->to("pao@buzzmybrand.it")->cc('vin@buzzmybrand.it')
								->subject("BUZZMYBRAND BUG NOTIFICATION");

					});

	return Redirect::to('https://www.buzzmybrand.com');
}
);

if (file_exists(__DIR__.'/controllers/ServerController.php')) {
    Route::get('/deploy', 'ServerController@deploy');
};

Route::get('/t&c/{id}', 'tab_HomeController@getDownload');
Route::get('/view_t_c/{id}', 'tab_HomeController@view_t_c');
Route::get('/get-privacy-minisite', 'tab_HomeController@getPrivacy');
Route::get('/make-facebook-tab/{id}', 'tab_HomeController@makeFacebookTab');
Route::get('/update-facebook-tab/{id}', 'tab_HomeController@updateFacebookTab');
Route::get('/delete-facebook-tab/{id}', 'tab_HomeController@deleteFacebookTab');


Route::get('/makeEssayPic', function()
{
	try
	{
		$filename = ContestHelper::createEssayPicture("Frase di prova");
		$statusCode = 200;
		$json = Response::json(array("response" => $filename), $statusCode);
		return $json;
	}
	catch (Exception $ex)
	{
		$statusCode = 400;
		$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
		return $json;
	}

});

Route::get('/makeExcelFile', function()
{
	try
	{
		$data = array(
			array('data1', 'data2'),
			array('data3', 'data4')
		);
		$filename = ContestHelper::getExcelFile($data);
		$json = Response::json(array("response" => $filename), '200');
		return $json;
	}
	catch (Exception $ex)
	{
		$statusCode = 400;
		$json = Response::json(array("error_message" => $ex->getMessage()), $statusCode);
		return $json;
	}

});


Route::group(array('domain' => 'bmb.buzzmybrand.com'), function()
{
	Route::get('/{name}', 'Minisite\Controllers\IndexController@getContest');

  Route::group(array('prefix' => 'msapi/'), function() {
		Route::get('/{name}', 'ApiMinisite\Controllers\apiController@getContest');
		Route::post('/auth/twitter/{name}', 'ApiMinisite\Controllers\AuthController@twitter');
		Route::post('/auth/facebook/{name}', 'ApiMinisite\Controllers\AuthController@facebook');
		Route::post('/inviteFriend/{name}', 'ApiMinisite\Controllers\apiController@inviteFriend');
		Route::get('/objectives/{name}', 'ApiMinisite\Controllers\apiController@objectives');
		Route::get('/social/{name}', 'ApiMinisite\Controllers\apiController@getSocial');
		Route::get('/rewards/{name}', 'ApiMinisite\Controllers\apiController@getRewards');
		Route::get('/entries/{name}', 'ApiMinisite\Controllers\apiController@getEntries');
		Route::get('auth/test/{name}/{comefrom?}', 'ApiMinisite\Controllers\AuthController@test');
		Route::group(array('before' => 'tokencheck'), function(){
  		Route::get('/profile/{name}', 'ApiMinisite\Controllers\apiController@getProfile');
			Route::get('/profile/entries/{name}', 'ApiMinisite\Controllers\apiController@getEntries');
			Route::post('/savePhoto/{name}', 'ApiMinisite\Controllers\apiController@savePhoto');
			Route::post('/saveVideo/{name}', 'ApiMinisite\Controllers\apiController@saveVideo');
			Route::post('/saveEssay/{name}', 'ApiMinisite\Controllers\apiController@saveEssay');
			Route::post('/signUp/{name}/{comefrom?}', 'ApiMinisite\Controllers\apiController@signUp');
		});
	});

});

Route::group(array('domain' => 'buzzmybrand.com'), function()
{
	Route::get('/minisite/{name}', 'Minisite\Controllers\IndexController@getContest');
});

Route::group(array('domain' => 'www.contestengine.net'), function()
{
	Route::get('/{name}', 'Minisite\Controllers\IndexController@getContest');

  Route::group(array('prefix' => 'msapi/'), function() {
		Route::get('/{name}', 'ApiMinisite\Controllers\apiController@getContest');
		Route::post('/auth/twitter/{name}', 'ApiMinisite\Controllers\AuthController@twitter');
		Route::post('/auth/facebook/{name}', 'ApiMinisite\Controllers\AuthController@facebook');
		Route::post('/inviteFriend/{name}', 'ApiMinisite\Controllers\apiController@inviteFriend');
		Route::get('/objectives/{name}', 'ApiMinisite\Controllers\apiController@objectives');
		Route::get('/social/{name}', 'ApiMinisite\Controllers\apiController@getSocial');
		Route::get('/rewards/{name}', 'ApiMinisite\Controllers\apiController@getRewards');
		Route::get('/entries/{name}', 'ApiMinisite\Controllers\apiController@getEntries');
		Route::get('auth/test/{name}/{comefrom?}', 'ApiMinisite\Controllers\AuthController@test');
		Route::group(array('before' => 'tokencheck'), function(){
  		Route::get('/profile/{name}', 'ApiMinisite\Controllers\apiController@getProfile');
			Route::get('/profile/entries/{name}', 'ApiMinisite\Controllers\apiController@getEntries');
			Route::post('/savePhoto/{name}', 'ApiMinisite\Controllers\apiController@savePhoto');
			Route::post('/saveVideo/{name}', 'ApiMinisite\Controllers\apiController@saveVideo');
			Route::post('/saveEssay/{name}', 'ApiMinisite\Controllers\apiController@saveEssay');
			Route::post('/signUp/{name}/{comefrom?}', 'ApiMinisite\Controllers\apiController@signUp');
		});
	});

});

Route::group(array('domain' => 'www.buzzmybrand.com'), function()
{
	//Route::get('/{name}', 'Minisite\Controllers\IndexController@getContest');
	Route::get('/minisite/{name}', 'Minisite\Controllers\IndexController@getContest');
	Route::post('/minisite/{name}', 'Minisite\Controllers\IndexController@getContest');

	Route::get('take_picture', 'tab_HomeController@takePicture');
	Route::post('tab/getSubmissions','tab_HomeController@getSubmissions');
	Route::post('tab/getSubmission','tab_HomeController@getSubmission');
  Route::group(array('prefix' => 'msapi/'), function() {
		Route::get('/{name}', 'ApiMinisite\Controllers\apiController@getContest');
		Route::post('/auth/twitter/{name}', 'ApiMinisite\Controllers\AuthController@twitter');
		Route::post('/auth/facebook/{name}', 'ApiMinisite\Controllers\AuthController@facebook');
		Route::post('/inviteFriend/{name}', 'ApiMinisite\Controllers\apiController@inviteFriend');
		Route::get('/objectives/{name}', 'ApiMinisite\Controllers\apiController@objectives');
		Route::get('/social/{name}', 'ApiMinisite\Controllers\apiController@getSocial');
		Route::get('/rewards/{name}', 'ApiMinisite\Controllers\apiController@getRewards');
		Route::get('/entries/{name}', 'ApiMinisite\Controllers\apiController@getEntries');
		Route::get('auth/test/{name}/{comefrom?}', 'ApiMinisite\Controllers\AuthController@test');
		Route::group(array('before' => 'tokencheck'), function(){
  		Route::get('/profile/{name}', 'ApiMinisite\Controllers\apiController@getProfile');
			Route::get('/profile/entries/{name}', 'ApiMinisite\Controllers\apiController@getEntries');
			Route::post('/savePhoto/{name}', 'ApiMinisite\Controllers\apiController@savePhoto');
			Route::post('/saveVideo/{name}', 'ApiMinisite\Controllers\apiController@saveVideo');
			Route::post('/saveEssay/{name}', 'ApiMinisite\Controllers\apiController@saveEssay');
			Route::post('/signUp/{name}/{comefrom?}', 'ApiMinisite\Controllers\apiController@signUp');
		});
	});

});








Route::match(array('GET', 'POST'), '/', function()
{
	if (strpos(Request::root(), 'buzzmybrand.it'))
	{
		Session::put('my.locale', 'it');
		return View::make('landing_it');
	}
	else
	{
		Session::put('my.locale', 'en');
		return View::make('landing_en');
	}
});



//switching language
Route::get('lang/{id}','LanguageController@select');

Route::get('/socialBuzz', function(){return View::make('links/social_buzz');});
Route::get('/sitemap', function(){return View::make('links/sitemap');});

//Route::get('{awards}', function(){return View::make('links/awards');})->where('awards', 'awards|premi');
//Route::get('{terms}', function(){return View::make('links/terms');})->where('terms', 'terms|termini');
//Route::get('{contacts}', function(){return View::make('links/contact');})->where('contacts', 'contacts|contatti');
//Route::get('{careers}', function(){return View::make('links/careers');})->where('careers', 'careers|lavoro');
//Route::get('{about}', function(){return View::make('links/about');})->where('about', 'about-us|su-di-noi');
//Route::get('{compliance}', function(){return View::make('links/compliance');})->where('compliance', 'compliance|note-legali');
//Route::get('{our_flow}', function(){return View::make('links/our_flow');})->where('our_flow', 'our-flow|nostro-flow');
//Route::get('{cancellationpolicy}', function(){return View::make('links/cancellationpolicy');})->where('cancellationpolicy', 'cancellationpolicy|cancellationpolicy');
//Route::get('{invoices}', function(){return View::make('links/invoices');})->where('invoices', 'invoices|fatturazione');

Route::get('/awards', function(){return View::make('links/awards');});
Route::get('/terms', function(){return View::make('links/terms');});
Route::get('/privacy', function(){return View::make('links/privacy');});
Route::get('/cookies', function(){return View::make('links/cookies');});
Route::get('/contact', function(){return View::make('links/contact');});
Route::get('/careers', function(){return View::make('links/careers');});
Route::get('/about', function(){return View::make('links/about');});
Route::get('/blog', function(){return Redirect::to('/blog/');});
Route::get('/media_kit', function(){return View::make('links/media_kit');});
Route::get('/compliance', function(){return View::make('links/compliance');});
Route::get('/our_flow', function(){return View::make('links/our_flow');});
Route::get('/faqs', function(){return View::make('links/faqs');});
Route::get('/cancellationpolicy', function(){return View::make('links/cancellationpolicy');});
Route::get('/invoices', function(){return View::make('links/invoices');});

/////////////////////juve routes///////////////////////////////////
Route::post('/juve', function()
{
	return Redirect::to('https://www.tok.tv/lp/juventuslive2/');
});
Route::get('/juve', function()
{
	return Redirect::to('https://www.tok.tv/lp/juventuslive2/');
});
Route::get('juve/photo/{id}', "PhotoController@getJuvePhoto");
Route::get('juve/video/{id}', "VideoController@getVideo");
Route::post('juve/photo/{id}', "PhotoController@getJuvePhoto");
Route::post('juve/video/{id}', "VideoController@getVideo");

Route::post('tab/savePhoto/{id}','tab_HomeController@savePhoto');
Route::post('tab/computeSharingScore','tab_HomeController@computeSharingScore');
Route::post("photoUpload", "PhotoController@saveExternalPhoto");
///////////////////////////////////////////////////////////////////
Route::post('oauth/access_token', function()
{
    return AuthorizationServer::performAccessTokenFlow();
});

Route::post('oauth/facebook_token',"HomeController@loginWithFacebookToken");

//new API for retrieving information from the FB login
Route::post('oauth/fb_token',"HomeController@loginWithFbToken");

Route::get('video/{id}', "VideoController@getVideo");
Route::get('photo/{id}', "PhotoController@getPhoto");
Route::get('essay/{id}', "Admin_ContestController@getEssay");
Route::post('video/{id}', "VideoController@getVideo");
Route::post('photo/{id}', "PhotoController@getPhoto");
Route::post('essay/{id}', "Admin_ContestController@getEssay");

Route::get('getUsersList', 'HomeController@getUsersList');

//API without access token
Route::get('fetchContestList/{id}', "ContestController@fetchContestList");
Route::get('getVideoInfo/{id}', "VideoController@getVideoInfo");
Route::get('getVideoList/{id}', "VideoController@getVideoList");
Route::get('getPhotoInfo/{id}', "PhotoController@getPhotoInfo");
Route::get('getPhotoList/{id}', "PhotoController@getPhotoList");
Route::post('deleteVideo/{id}',"VideoController@softDelete");
Route::post('likeVideo', 'VideoController@likeVideo');

Route::post('get-in-touch', 'BMBWebSiteController@getInTouch');
Route::post('subscribe', 'BMBWebSiteController@subscribe');

Route::get('test-approvation-email/{id}', 'BMBWebSiteController@testApprovationEmail');



Route::group(array('prefix' => 'v1'), function()
{

});

Route::group(array('prefix' => 'v1', 'before' => 'oauth'), function()
{
    Route::resource('contests', 'ContestController');
	Route::resource('contests', 'ContestController');
    Route::resource('videos', 'VideoController');
	Route::resource('photos', 'PhotoController');
    Route::resource('companys', 'CompanyController');
    Route::resource('settings', 'SettingController');
    Route::post('settings/update','SettingController@update');
    Route::get('eventList', 'ContestController@eventList');
    Route::get('rewardList', 'ContestController@rewardList');
    Route::post('contests/join/{id}', 'ContestController@join');
    Route::get('companys/videos/{id}', 'CompanyController@showCompanyVideos');
    Route::post('video/join', 'VideoController@store');
	Route::post('photo/join', 'PhotoController@store');
	Route::get('locationList', 'HomeController@locationList');
	Route::post('deleteVideo/{id}',"VideoController@softDelete");
	Route::post('deletePhoto/{id}', "PhotoController@softDelete");
	//Route::get('MultimediaElementsUser/{id}', "VideoController@MultimediaElementsUser");
	Route::get('MultimediaElementsUser','VideoController@MultimediaElementsUser');
	Route::get('FavouriteMediaElements/{id}', "VideoController@FavouriteMediaElements");

});
Route::get('admin', 'AdminController@index');
//Route::get('v1/search/{contest?}','ContestController@search');

Route::group(array('prefix' => 'v1'), function()
{
    Route::resource('users', 'UserController');
    Route::post('users/update','UserController@update');
    Route::post('users/delete','UserController@destroy');
	Route::post('users/getUserInformation', 'UserController@getUserInformation');
    Route::post('loginWithFacebook','HomeController@loginWithFacebook');
    Route::post('loginWithTwitter','HomeController@loginWithTwitter');

    Route::post('forgotPassword','HomeController@forgotPasswordAPI');
    Route::get('resetPassword','HomeController@ResetPasswordForgotAPI');
});

/** web routes 12-2-14 **/

//Route::resource('/', 'HomeController');
Route::get('buzz-my-brand-login/{id}', 'HomeController@buzzMyBrandLogin');
Route::post('buzz-my-brand-perform-login/{id}', 'HomeController@buzzMyBrandPerformLogin');


Route::get('forget', 'HomeController@forget');

Route::post('users/login', 'HomeController@getLogin');

Route::get('forgot/password', 'HomeController@forgotPassword');
Route::post('/resetPassword', 'HomeController@resetPassword');
Route::get('company/passwordReset', 'HomeController@showReset');

Route::get('register','HomeController@showRegister');
Route::get('logout', 'HomeController@getLogout');
Route::get('login', 'HomeController@getLogin');

Route::get('company/profile', 'CompanyController@getProfile');
Route::post('company/edit', 'CompanyController@postCompanyrecord');
Route::post('company/getRegistered', 'CompanyController@getRegistered');
Route::post('registeration', 'CompanyController@postRegister');

Route::post('create/event', 'ContestController@postEvent');
Route::get('company/event/{any}', 'ContestController@deleteEvent');
Route::post('company/event/edit', 'ContestController@editEvent');
Route::post('company/event/edit', 'ContestController@eventEdit');

Route::post('company/getSignin', 'AdminController@getSignin');
Route::get('introduction','AdminController@showIntro');
Route::get('company','CompanyController@showCompanyPage');
Route::get('company/deleteCompany/{any}','CompanyController@deleteCompany');
Route::post('company/addNewCompany','CompanyController@registerNewCompany');
Route::post('company/editCompany','CompanyController@updateCompany');
Route::get('company/logout','AdminController@logout');


Route::get('contest','ContestController@showContestPage');
Route::post('event/addEvent','ContestController@registerContest');
Route::post('event/editEvent','ContestController@editContest');
Route::get('contest/deleteContest/{any}','ContestController@deleteContest');

//Routing for website
Route::get('website','Website_HomeController@index');
Route::get('website/login/facebook_login/{id}', 'Website_WidgetController@facebookLogin');


Route::get('website/login/twitter-login/{id}', 'Website_WidgetController@twitterLogin');
Route::get('website/login/instagram-login/{id}', 'Website_WidgetController@instagramLogin');
Route::get('website/login/facebook_login/{id}', 'Website_WidgetController@facebookLogin');

//Routing Paolo per BS
Route::get('website/login/facebook_login/{id}/{videopart}', 'Website_WidgetController@facebookLoginResume');
//
Route::get('website/login/twitter_login/{id}', 'Website_WidgetController@twitterLogin_old');
Route::get('website/form/description/{input}','Website_CompanyController@description');
Route::get('website/function/description/{input}','Website_CompanyController@functionDescription');
Route::get('website/footer/{resource}','Website_CompanyController@footer');

//Routing for tab
Route::post('minisites','tab_HomeController@tabs');
Route::get('tab/{id}','tab_HomeController@index');
Route::post('tab/{id}','tab_HomeController@index');
Route::get('login/{id}','tab_HomeController@login');
Route::get('choose', function(){
    return View::make('tab/choose');
});
Route::get('invite/{id}', 'tab_HomeController@invite');
Route::post('upload_submission', 'tab_HomeController@uploadSubmission');
Route::get('take_picture', 'tab_HomeController@takePicture');

Route::get('confirm', function(){
    return View::make('tab/confirm');
});
Route::get('invite_final', function(){
    return View::make('tab/invite_final');
});
Route::get('invite_error', function(){
    return View::make('tab/invite_error');
});
Route::get('record_video', function(){
    return View::make('tab/record_video');
});
Route::post('tab/getSubmissions','tab_HomeController@getSubmissions');
Route::post('tab/instantWin', 'tab_HomeController@instantWin');
Route::post('tab/getSubmission','tab_HomeController@getSubmission');
Route::post('tab/savePhoto/{id}','tab_HomeController@savePhoto');
Route::post('tab/saveVideo/{id}','tab_HomeController@saveVideo');
Route::post('tab/saveEssay/{id}','tab_HomeController@saveEssay');
Route::post('tab/sendMail/{id}', 'tab_HomeController@sendMail');
Route::post('tab/getTemplateSubmissions','tab_HomeController@getTemplateSubmissions');
// END
//Routing for Blackshape tab
Route::get('tabBS/{id}','tab_HomeController@indexBS');
Route::get('loginBS/{id}','tab_HomeController@loginBS');

Route::group(array('prefix' => 'website/user'), function()
{
     Route::get('/','Website_UserController@index');
     Route::post('change-photo','Website_UserController@changePhoto');
     Route::get('/delete-account','Website_UserController@deleteAccount');
     Route::get('logout','Website_UserController@logout');
     Route::post('my-contests','Website_UserController@userContests');
     Route::get('my-contests','Website_UserController@userContests');
     Route::get('favourited-contests','Website_UserController@favouritedContests');
     Route::get('search-contests','Website_UserController@searchContests');
     Route::get('video/{id}','Website_UserController@video');
     Route::post('check-username','Website_UserController@checkUsername');
     Route::post('check-email','Website_UserController@checkEmail');
     Route::post('login','Website_UserController@login');
     Route::get('facebook','Website_UserController@facebook');
     Route::get('twitter','Website_UserController@twitter');
     Route::get('facebook_login', 'Website_UserController@facebookLogin');
     Route::get('twitter_login', 'Website_UserController@twitterLogin');
     Route::post('register','Website_UserController@register');
     Route::get('contest/{id}','Website_UserController@contest');
     Route::post('contest/{id}','Website_UserController@contest');
     Route::get('contest/{id}/join', 'Website_UserController@joinContest');
});
//STANDARD WIDGET
Route::group(array('prefix' => 'website/widget'), function()
{
	Route::get('insert_essay/{id}', 'Website_WidgetController@insertEssay');
	Route::get('/instagram/{id}','Website_WidgetController@instagram');
	Route::get('/instagram-test', 'Website_WidgetController@instagramTest');

	Route::get('/getLogout', 'Website_WidgetController@getLogout');
	Route::get('/{id}','Website_WidgetController@index');
	Route::get('login/{id}','Website_WidgetController@login');
	//routing Paolo BlackShape
	Route::get('loginResume/{id}/{videopart}','Website_WidgetController@loginResume');
	Route::post('loginResume/{id}/{videopart}','Website_WidgetController@loginResume');
	Route::get('facebook/{id}/{videopart}','Website_WidgetController@facebookResume');
	Route::get('twitter/{id}/{videopart}','Website_WidgetController@twitterResume');
	Route::get('recordResume/{id}/{videopart}','Website_WidgetController@recordResume');
	//Route::get('save_videopart/{videoId}/{duration}/{companyId}/{userId}/{videopart}','Website_WidgetController@saveVideopart');
	Route::get('saveResume/{id}', 'Website_WidgetController@saveResume');
	Route::get('resetResume', 'Website_WidgetController@resetResume');
	//
	Route::post('login/{id}','Website_WidgetController@login');
	Route::get('chooseAction/{id}', 'Website_WidgetController@chooseAction');
	Route::get('record/{id}','Website_WidgetController@record');
	Route::get('facebook/{id}','Website_WidgetController@facebook');
	Route::get('/twitter/{id}','Website_WidgetController@twitter');
	Route::get('/twitter-test/{id}', 'Website_WidgetController@twitterTest');
	//Route::get('save_video/{videoId}/{duration}/{companyId}/{userId}/{title}','Website_WidgetController@saveVideo');


	Route::get('signup/{id}', 'Website_WidgetController@signup');
	Route::post('register-user/{id}/{come_from}', 'Website_WidgetController@registerUser');
	Route::get('disconnect/{id}/{user_id}', 'Website_WidgetController@resetCookie');

	Route::get('choose/{id}/{user_id}','Website_WidgetController@choose');


	Route::get('connect/{id}', 'Website_WidgetController@connect');
	//Route::post('connect2/{id}', array('before' => 'oauth', 'uses' => 'Website_WidgetController@connect2'));

	Route::get('uploadPicture/{id}', 'Website_WidgetController@uploadPicture');
	Route::get('shootPicture/{id}', 'Website_WidgetController@shootPicture');








});
/*
//WIDGET FOR CBC THAILAND
Route::group(array('prefix' => 'website/widget_CBC_Thailand'), function()
{
     Route::get('/{id}','Website_CBCWidgetController@index');
     Route::get('login/{id}','Website_CBCWidgetController@login');
     Route::post('login/{id}','Website_CBCWidgetController@login');
     Route::get('record/{id}/{project}','Website_CBCWidgetController@record');
     Route::get('facebook/{id}','Website_CBCWidgetController@facebook');
     Route::get('twitter/{id}','Website_CBCWidgetController@twitter');
     Route::get('save_video/{videoId}/{duration}/{companyId}/{userId}/{title}','Website_CBCWidgetController@saveVideo');
});
*/

Route::get('CBC', "Website_CBCWidgetController@login");
Route::post('CBC', "Website_CBCWidgetController@login");
//***********************

Route::group(array('prefix' => 'website/company'), function()
{
     Route::get('/associate-youtube-account', 'Website_CompanyController@associateYouTubeAccount');
     Route::get('/disconnect-youtube-account', 'Website_CompanyController@disconnectYouTubeAccount');
     Route::get('/youtube-oauth2callback', 'Website_CompanyController@youTubeOAuth2Callback');
	 Route::post('/register','Website_CompanyController@register');
     Route::get('/confirm-payment','Website_CompanyController@confirmPayment');
     Route::get('/dashboard/{id?}','Website_CompanyController@dashboard');
     Route::get('/launch-a-contest/{id?}','Website_CompanyController@launchContest');
     Route::get('/team','Website_CompanyController@team');
     Route::post('/launch-a-contest','Website_CompanyController@launchContest');
     Route::post('check-username','Website_CompanyController@checkUsername');
     Route::post('check-email','Website_CompanyController@checkEmail');
     Route::get('/launch-a-contest-step-two/{id}','Website_CompanyController@launchContestStep2');
     Route::post('/launch-a-contest-step-two/{id}','Website_CompanyController@launchContestStep2');
     Route::get('/draft-contest/{id}','Website_CompanyController@draftContest');
     Route::get('/delete-account','Website_CompanyController@deleteAccount');
     Route::post('/draft-contest/{id}','Website_CompanyController@draftContest');
     Route::post('/dashboard','Website_CompanyController@dashboard');
     Route::get('/login/{request}','Website_CompanyController@login');
     Route::get('/print/{id}','Website_CompanyController@printContest');
     Route::post('/login/{request}','Website_CompanyController@login');
     Route::post('/login-request/{request}','Website_CompanyController@loginRequest');
     Route::get('/logout','Website_CompanyController@logout');
     Route::get('/{resource}','Website_CompanyController@wordpress');
});

Route::group(array('prefix' => 'website/video'), function()
{
    Route::get('/{id}','Website_VideoController@video');
});

Route::group(array('prefix' => 'template_demo'), function()
{
    Route::get('/{id}','tab_HomeController@showDemo');
});

Route::group(array('prefix' => 'admin'), function()
{

	Route::get("php-info", "Admin_DashboardController@phpInfo");

	Route::get("/external-contests", "Admin_ContestController@externalAdminIndex");
	Route::get("/external-contests/{id}", "Admin_ContestController@externalIndex");

	/*Route::post("/contest-list/{id}", "Admin_ContestController@contestList");*/
	Route::post("/savepreview", "Admin_DashboardController@savePreview");
	Route::get("/contest-list/{id}", "Admin_DashboardController@contestList");
	Route::get("/contest-detail/{id}", "Admin_DashboardController@contest");
	Route::get("/videos-of-contest/{id}", "Admin_DashboardController@videosOfContest");
	Route::get("/photos-of-contest/{id}", "Admin_DashboardController@photosOfContest");
	Route::get("/essays-of-contest/{id}", "Admin_DashboardController@essaysOfContest");
	Route::post("/send-facebook-notification/{id}", "Admin_DashboardController@sendFacebookNotification");
	Route::post("/send-twitter-notification/{id}", "Admin_DashboardController@sendTwitterNotification");
	Route::post("/send-mass-notification-ft/{id}", "Admin_DashboardController@sendMassNotification");
	Route::get("/contest-data/{id}", "Admin_DashboardController@contestData");
	Route::get("/single-post-data-for-videos/{id}", "Admin_DashboardController@singlePostDataForVideos");
	Route::get("/single-post-data-for-photos/{id}", "Admin_DashboardController@singlePostDataForPhotos");
	Route::get("/single-post-data-for-essays/{id}", "Admin_DashboardController@singlePostDataForEssays");
	Route::post("/edit-contest", "Admin_DashboardController@editContest");
	Route::get("/contest-description/{id}", "Admin_DashboardController@contestDescription");
	Route::post("/delete-contest/{id}", "Admin_DashboardController@deleteContest");
	Route::post("/delete-permanently-contest/{id}", "Admin_DashboardController@deletePermanentlyContest");
	Route::get("/get-company/{id}", "Admin_DashboardController@getCompany");
	Route::post("/edit-company", "Admin_DashboardController@editCompany");
	Route::post("/send-welcome-email/{id}", "Admin_DashboardController@sendWelcomeEmail");
	Route::get("/verify-email/{id}/{rnd_key}", "Admin_DashboardController@verifyEmail");
	Route::post("/notify-email-budget", "Admin_DashboardController@notifyEmailBudget");
	Route::get("/facebook-customer-login/{id}", "Admin_DashboardController@facebookCustomerLogin");
	Route::get("/customer-facebook-pages/{id}", "Admin_DashboardController@customerFacebookPages");
	Route::post("/facebook-customer-connect/{id}", "Admin_DashboardController@facebookCustomerConnect");
	Route::get("/facebook-customer-disconnect/{id}", "Admin_DashboardController@facebookCustomerDisconnect");
	Route::get("/twitter-customer-login/{id}", "Admin_DashboardController@twitterCustomerLogin");
	Route::get("/twitter-customer-connect/{id}", "Admin_DashboardController@twitterCustomerConnect");
	Route::get("/twitter-customer-disconnect/{id}", "Admin_DashboardController@twitterCustomerDisconnect");
	Route::post("/facebook-ghost-post/{id}", "Admin_DashboardController@facebookGhostPost");
	Route::get("/youtube-customer-login/{id}", "Admin_DashboardController@youtubeCustomerLogin");
	Route::get("/youtube-customer-connect", "Admin_DashboardController@youtubeCustomerConnect");
	Route::get("/youtube-customer-disconnect/{id}", "Admin_DashboardController@youtubeCustomerDisconnect");
	Route::post("/approve-photo/{id}", "Admin_DashboardController@approvePhoto");
	Route::post("/approve-video/{id}", "Admin_DashboardController@approveVideo");
	Route::post("/approve-essay/{id}", "Admin_DashboardController@approveEssay");
	Route::post("/choose-promotion/{id}", "Admin_DashboardController@choosePromotion");
	Route::post("/change-promotion-proposal/{id}", "Admin_DashboardController@changePromotionProposal");
	Route::post("/accept-proposal/{id}", "Admin_DashboardController@acceptProposal");
	Route::post("/refuse-proposal/{id}", "Admin_DashboardController@refuseProposal");
	Route::post("/upload-banner/{id}", "Admin_DashboardController@uploadBanner");
	Route::post("/upload-video/{id}", "Admin_DashboardController@uploadVideo");
	Route::get("/get-promotion/{id}", "Admin_DashboardController@getPromotion");
	Route::get("/check-av-contest-route", "Admin_DashboardController@checkAvContestRoute");
	Route::get("/get-contest/{id}", "Admin_DashboardController@getContest");
	Route::get("/get-template-list", "Admin_DashboardController@getTemplateList");
    Route::get("/get-fonts", "Admin_DashboardController@getFonts");

	Route::post("/check-coupon", "Admin_DashboardController@checkCoupon");

	Route::post("/delete-photo-from-contest/{id}", "Admin_DashboardController@deletePhoto");
	Route::post("/delete-video-from-contest/{id}", "Admin_DashboardController@deleteVideo");
	Route::post("/delete-essay-from-contest/{id}", "Admin_DashboardController@deleteEssay");
	Route::post("/new-company", "Admin_DashboardController@newCompany");

	Route::post("/insert-notification/{id}", "Admin_DashboardController@insertNotification");
	Route::post("/update-notification/{id}", "Admin_DashboardController@updateNotification");
	Route::post("/get-notifications", "Admin_DashboardController@getNotifications");
	Route::post("/check-av-email", "Admin_DashboardController@checkAvEmail");
	Route::get("/get-terms-en/{company}", "Admin_DashboardController@getTermsEn");
	Route::get("/get-terms-it/{company}", "Admin_DashboardController@getTermsIt");
	Route::get("/get-twitter-locations-trends-available/{name?}", "Admin_DashboardController@getTwitterLocationsTrendsAvailable");
	Route::get("/get-twitter-trends-available/{woeid}", "Admin_DashboardController@getTwitterTrendsAvailable");
	Route::post("/resend-password", "Admin_DashboardController@resendPassword");
	Route::post("/save-dashboard-image", "Admin_DashboardController@saveDashboardImage");
	Route::get("/get-tec", "Admin_DashboardController@getTec");

	//Route:post("/approve-contest", "Admin_DashboardController@approveContest");

	Route::get("/contest", "Admin_ContestController@contestManagement");
	Route::get("/company", function(){
		return View::make('admin/company');
	});
	Route::get("/contest/{id}", "Admin_ContestController@contest");
	Route::get("/video/{id}", "Admin_ContestController@video");
	Route::get("/photo/{id}", "Admin_ContestController@photo");
	Route::post("/video/{id}/approve", "Admin_ContestController@approveVideo");
	Route::post("/photo/{id}/approve", "Admin_ContestController@approvePhoto");
	Route::post("/essay/{id}/approve", "Admin_ContestController@approveEssay");
	Route::post("/contest/{id}/finish-approval", "Admin_ContestController@finishApproval");
	Route::post("/contest/{id}/reset-approval", "Admin_ContestController@resetApproval");
	Route::post("/contest/{id}/start-server-process", "Admin_ContestController@startServerProcess");
	Route::post("/contest/{id}/insert-watermarks", "Admin_ContestController@insertWatermarks");
	Route::post("/contest/{id}/upload-to-youtube", "Admin_ContestController@upload2YouTube");
	Route::post("/contest/{id}/upload-to-instagram", "Admin_ContestController@uploadToInstagram");

	Route::post("/get-all-videos", "Admin_ContestController@getAllVideos");
	Route::post("/get-all-photos", "Admin_ContestController@getAllPhotos");
	Route::post("/get-not-approved-videos", "Admin_ContestController@getNotApprovedVideos");
	Route::post("/get-not-approved-photos", "Admin_ContestController@getNotApprovedPhotos");
	Route::post("/get-approved-videos", "Admin_ContestController@getApprovedVideos");
	Route::post("/get-approved-photos", "Admin_ContestController@getApprovedPhotos");
	Route::post("/get-to-approve-videos", "Admin_ContestController@getToApproveVideos");
	Route::post("/get-to-approve-photos", "Admin_ContestController@getToApprovePhotos");

	Route::post("/send-notification/{id}", "Admin_ContestController@sendNotification");
	Route::post("/send-notificationPhoto/{id}", "Admin_ContestController@sendNotificationPhoto");
	Route::post("/send-mass-notification/{id}", "Admin_ContestController@sendMassNotification");

	Route::post("/delete-video/{id}", "Admin_ContestController@deleteVideo");
	Route::post("/delete-photo/{id}", "Admin_ContestController@deletePhoto");
	Route::post("/delete-essay/{id}", "Admin_ContestController@deleteEssay");

	Route::get("/new-contest", "Admin_ContestController@newContest");

	Route::get("/contest/upload-photo-instagram/{id}", "Admin_ContestController@uploadPhotoToInstagram");
	Route::get("/contest/upload-video-instagram/{id}", "Admin_ContestController@uploadToInstagram");

});

//Routing for services
Route::get('services','Services_HomeController@index');

Route::group(array('prefix' => 'services/user'), function()
{
     Route::get('/','Services_UserController@index');
});

Route::group(array('prefix' => 'services'), function()
{
	Route::post('/dashboard','Services_CompanyController@dashboard');
});

// Invoice download
Route::get('invoices/{id}', 'PaymentsController@getInvoicePdf');

Route::get('dashboard-api/payments/get-plans', 'PaymentsController@getPlans');
Route::post('dashboard-api/payments/make-customer', 'PaymentsController@postMakeCustomer');
Route::post('dashboard-api/payments/update-customer', 'PaymentsController@postUpdateCustomer');
Route::post('dashboard-api/payments/make-checkout', 'PaymentsController@postCheckout');
Route::post('dashboard-api/payments/check-recaptcha', 'PaymentsController@checkRecaptcha');
Route::post('dashboard-api/payments/remove-card', 'PaymentsController@postRemoveCard');
Route::get('dashboard-api/contests/get-scheduled', 'ContestController@getScheduled');
Route::get('dashboard-api/contests/switch-scheduled-contests', 'ContestController@switchScheduledContests');

Route::group(['prefix' => 'dashboard-api'], function()
{
    Route::controller('utils', 'Dashboard_DashboardController');
    Route::controller('payments', 'PaymentsController');
    Route::controller('credits', 'UserCreditController');
});
