<?php

class Wordpress extends WordpressJsonClient
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getPageContent($name, $locale)
    {
        if (Cache::has($name))
        {
            $page_content = Cache::get($name);
        }
        else
        {
            $page_content = $this->getPage("/".$name,$locale)->page->content;
            Cache::add($name, $page_content, 60); 
        }
        
        //next step is to strip of wordpress URL prefix (hard-coded absolute URL) to images
        //unless there is no wp_url or no gs_images_url (so no URL to fix)
        if ($this->wordpressContentUrl != null)
        {
            $fixed_content = str_replace($this->wordpressContentUrl, $this->jmtvContentUrl, $page_content);	
            Log::info("Fixed content: ".$fixed_content);
            return $fixed_content;
        }
        else
        {
            return $page_content;
        }
    }
}

?>
