<?php

use Paymill\Services\PaymillException;

class PaymillHelper {
    
    /**
     * Formats the amount as required by Paymill. Eg: 10.22 => "1022"
     *
     * @param $amount: int, float or string
     * @return string
     */
    public static function getFormattedAmount($amount)
    {
        // Return as-is if already formatted
        if(is_string($amount) && strpos($amount, '.') === false)
            return $amount;

        // Just cast to string
        if(is_integer($amount))
            return (string) $amount;

        // Round to two decimals, push decimals to int, round and cast to string
        return (string) round(round($amount, 2) * 100);
    }

    public static function getDecimalAmount($amount)
    {
        return round((int) $amount / 100, 2);
    }

    /**
     * Creates a new payment card with a token.
     * 
     * @return Paymill\Response object
     **/
    public static function makePaymentCard($token)
    {
        $request = new Paymill\Request(Config::get('paymill.api_keys.private'));
        $payment = new Paymill\Models\Request\Payment();
        $payment->setToken($token);

        $response = $request->create($payment);
        return $response;
    }

    /**
     * Attempts a transaction with either a token (new card) or a payment (existing card).
     * 
     * @return Paymill\Response object
     **/
    public static function makeTransaction($amount, $token=null, $payment=null, $description=null)
    {
        if($token == null && $payment == null)
            throw new PaymillException('Provide a token or a payment.');
        if($token && $payment)
            throw new PaymillException('Provide either token or a payment.');

        $amount = static::getFormattedAmount($amount);

        $request = new Paymill\Request(Config::get('paymill.api_keys.private'));
        $transaction = new Paymill\Models\Request\Transaction($amount);
        $transaction->setAmount($amount)
                    ->setCurrency('EUR');
        
        if($token)
            $transaction->setToken($token);
        if($payment)
            $transaction->setPayment($payment);

        if($description)
            $transaction->setDescription($description);

        try {
            $response = $request->create($transaction);
            return $response;
        } catch (Exception $err) {
            if($err instanceof PaymillException) {
                Log::error($err);
                throw new PaymentException($err->getMessage());
            }

            Log::error($err);
            throw new PaymentException(trans('payments.generic_error_message'));
        }
    }

    public static function removePayment($token)
    {
        $request = new Paymill\Request(Config::get('paymill.api_keys.private'));
        
        try {
            $payment = new Paymill\Models\Request\Payment();
            $payment->setId($token);
            $response = $request->delete($payment);
        } catch (PaymillException $err) {
            Log::error($err);
            throw new PaymentException(trans('payments.could_not_remove_credit_card'));
        }
    }

}