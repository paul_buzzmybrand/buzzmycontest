<?php


use \Swift_SmtpTransport as SmtpTransport;

class Scheduler
{

	public static function downloadFile ($url, $path) {

		$newfname = $path;
		$file = fopen ($url, "rb");
		if ($file) {
			$newf = fopen ($newfname, "wb");

			if ($newf)
			while(!feof($file)) {
			  fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
			}
		}

		if ($file) {
			fclose($file);
		}

		if ($newf) {
			fclose($newf);
		}

	}

	public static function sendInstantWinUsers()
	{
		Log::debug("sendInstantWinUsers");

		$contests = Contest::all();

		foreach ($contests as $contest)
		{

			if (isset($contest->instantwin_timeslot))
			{


				$users = $contest->users()->whereNotNull('contest_user.instantwin_done')->orderBy('contest_user.instantwin_done', 'asc')->get();

				Log::debug("User number:".$users->count());

				try {

					Mail::send('emails.instantwin_mail', array('contest' => $contest, 'users' => $users), function($message) {

						$message->to('pao@buzzmybrand.it', 'Paolo Cappelluti')->to('info@iorompolescatole.com', '#iorompolescatole')
								->to('andrea@tumitalia.com', 'Andrea Vitrotti')->to('paula@tumitalia.com', 'Paula Torrell')
								->subject("Notifica quotidiana instant win");

					});



				} catch (Exception $ex) {
					Log::error('Problem sending email: '.$ex);
				}



			}

		}
	}

	public static function updateMediaFromInstagram()
	{
		Log::debug("updateMediaFromInstagram");

		$contests = Contest::all();
		//$contests = Contest::find('228');

		//$queries = DB::getQueryLog();
		//Log::debug("Queries: ".print_r($queries,true));



		foreach ($contests as $contest)
		{

			//$tz_object = new DateTimeZone($contest->location->timezoneDesc);
			$datetime = new DateTime();
			//$datetime->setTimezone($tz_object);
			$today = $datetime->format("Y-m-d H:i:s");


			if (!is_null($contest->instagram) && ($today >= $contest->start_time) && ($today <= $contest->end_time) && $contest->status == 1)
			{


				$hashtag = substr($contest->hashtag, 1);

				//$hashtag = "molfetta";

				$instagram = new Instagram(array(
					'apiKey'      => Config::get('instagram')['app_key'],
					'apiSecret'   => Config::get('instagram')['app_secret'],
					'apiCallback' => Config::get('instagram')['app_callback']
				));

				$instagram->setAccessToken(Config::get('instagram')['app_access_token']);

				$result = $instagram->getTagMedia($hashtag);

				//Log::debug("Response from updateMediaFromInstagram: ".print_r($result,true));

				foreach ($result->data as $media)
				{
					Log::debug("Response of the single media: ".print_r($media,true));



					//devo salvare la foto ma devo verificare se già c'è
					//se la foto è già presente, non devo reinserirla
					$photo = Photo::where('image', $media->images->low_resolution->url)->first();

					if (!$photo)
					{

						Log::debug("Upload instagram photo with thumbnail ".$media->images->low_resolution->url);

						//controllo se c'è già l'utente
						$user = User::where('email', $media->user->username)->first();

						if ( !$user )
						{
							//l'utente non è presente


							//devo prima creare l'utente
							$user = new User();
							if ($media->user->full_name)
							{
								$user->name = utf8_encode($media->user->full_name);
							}
							$user->email = utf8_encode($media->user->username);
							$user->save();

							Website_WidgetController::joinContestFromInstagram($user->id, $contest->id);

						}




						$photo=new Photo();
						$photo->contest_id = $contest->id;
						$photo->user_id = $user->id;
						if ($contest->contest_approval)
						{
							$photo->approval_step_id = 1;
						}
						else
						{
							$photo->approval_step_id = 3;
							//TODO: caricare direttamente le foto su pagina FB qualora esista
						}


						$title = (strlen($media->caption->text) > 80) ? substr($media->caption->text,0,77).'...' : $media->caption->text;

						$photoFileName = md5(time());

						$photo->name = utf8_encode($title);
						$photo->image = $media->images->low_resolution->url;

						Scheduler::downloadFile($media->images->standard_resolution->url, public_path()."/photos/".$photoFileName.".jpg");

						$photo->filename = $photoFileName.".jpg";
						$photo->ig_source_url = $media->link;
						$photo->location_id = $contest->location_id;
						$photo->likes_count_post = $media->likes->count;
						$photo->comments_count_post = $media->comments->count;
						$photo->user_connected_to_instagram = 1;

						//$contest->users()->attach($user->id);
						$photo->save();



					}



					//break;
				}


			}





		}


	}


	public static function uploadOnFacebookPageFromInstagram($contest_id) {

		Log::debug("uploadOnFacebookPageFromInstagram()");

		$contest = Contest::findOrFail($contest_id);

		//devo trovare le foto che appartengono al contest, hanno user_connecte_to_instagram = 1 e approval_step_id = 3

		$photos = Photo::where("photos.approval_step_id", "=", 3)
				->where("photos.watermarkInsDone", "=", 0)
				->where("photos.uploadOnFacebookDone", "=", 0)
				->where("photos.contest_id", "=", $contest_id)
				->where("photos.user_connected_to_instagram", "=", 1)
				->get();

		Log::debug("Number of photos returned for upload on Facebook with Instagram: ".count($photos));


		foreach ($photos as $photo)	{

			//watermarkare la foto + creare html per sharing
			if ((($contest->should_insert_contest_watermark) || ($contest->should_insert_company_watermark)) && $contest->type_id != 3 )
			{
				ContestHelper::insertWatermarkPhoto($photo->id, 2);
				Scheduler::replace_in_file($photo, 2);
			}




			//caricare la foto su facebook
			//*******************************************************************

			Log::debug("Photo that correspond to the correct requisites for upload on FB: ".$photo->name);

			$facebook = new Facebook\Facebook([
				'app_id' => $contest->company->fbApp_clientID,
				'app_secret' => $contest->company->fbApp_clientSecret,
				'default_graph_version' => 'v2.4',
			]);

			//$facebook->setFileUploadSupport(true);

			$pages_array = $contest->fbPages;

			$source_fb = '';

			//Qui devo ciclare per tutte le pagine FB connesse al contest
			//Quindi devo costruirmi l'array per individuare le pagine FB connesse al contest
			foreach ($contest->fbPages as $page)
			{

				$admin_access_token = $page->fb_page_admin_at;
				$fb_page_id = $page->fb_page_id;

				//get PAGE ACCESS TOKEN
				$info = array("access_token" => $admin_access_token);
				$fb_request = $facebook->get('/me/accounts', $admin_access_token);

				Log::debug("Facebook response for getting access token: ".print_r($fb_request->getBody(),true));
				//scorrere l'array finche non trovi la pagina giusta
				//$pages_array = $fb_request['data'];
				$pages_array = $fb_request->getGraphEdge();


				$page_access_token = null;
				foreach ($pages_array as $pagefb)
				{
					if ($pagefb['id'] == $page->fb_page_id)
					{
						$page_access_token = $pagefb['access_token'];
						break;
					}
				}

				Log::debug("Facebook page access token: ".$page_access_token);

				$message = $photo->name;

				Log::debug("Message for page post: ".$message);


				$fileToUpload = public_path()."/photos/".$photo->filename;

				//upload photo to facebook page
				$data = array(
					"source" => $facebook->fileToUpload($videoToUpload),
					"message" => $message,
					"access_token" => $page_access_token);

				if ($page->fb_no_story == "YES")
				{
					Log::debug("Contest with unpublished posts");
					$new_data = array("no_story" => "true");
					$data = array_merge($data, $new_data);

				}


				if (isset($page->fb_id_album)) {
					$response = $facebook->post("/".$page->fb_id_album."/photos", $data, $page_access_token);
				} else {
					$album_details = array(
						'name' => $contest->name,
					);

					$create_album = $facebook->post('/'.$page->fb_page_id.'/albums', $album_details, $page_access_token);

					$graphNodealbum = $create_album->getGraphNode();

					$page->fb_id_album = $graphNodealbum['id'];
					$page->save();

					$response = $facebook->post("/".$page->fb_id_album."/photos", $data, $page_access_token);
				}

				$graphNode = $response->getGraphNode();

				Log::debug("Facebook response for new Photo Upload: ".print_r($graphNode,true));
				$id_photo = $graphNode['id'];
				$id_post_photo = null;
				if (array_key_exists("post_id", $graphNode))
				{
					$id_post_photo = $response['post_id'];
				}
				else
				{
					$id_post_photo = $page->fb_page_id."_".$id_photo;
				}


				//Creare record nella pivot table tra photo e fbpage array("receiver_id"=>$u2->id, "status"=>1)
				$photo->fbPages()->attach($page->id, array('fb_idphoto'=>$id_photo, 'fb_idpost'=>$id_post_photo));


				try {

					$data = array("access_token" => $page_access_token);
					$responsePhoto = $facebook->get("/".$id_photo, $page_access_token);
					$graphNodePhoto = $responsePhoto->getGraphNode();
					Log::debug("Facebook response for Photo Data: ".print_r($graphNodePhoto,true));
					$picture_post = $graphNodePhoto['picture'];


					if ($page->custom_logo == '' || is_null($page->custom_logo))
					{
						//$source_fb = $responsePhoto['source'];
						$source_fb = $graphNodePhoto['images'][0]['source'];
					}

					$photo->fb_source_url = $source_fb;
					$photo->save();

				}
				catch (Exception $ex)
				{
					Log::error($ex);
				}


			}


			$photo->uploadOnFacebookDone = '1';
			$photo->save();










			//******************************************************************
			Log::debug("Photo saved on the db with name: ".$photo->filename);








		}



	}


	public static function makeFacebookTab($id, $contest_id) {

		Log::debug("makeFacebookTab");

		Log::debug("makeFacebookTab: input parameter: ".print_r($id,true));

		/*
		$contest = DB::table('fbpage_contest')
				->join("contests", "contests.id", "=", "fbpage_contest.contest_id")
				->where("fbpage_contest.fbpage_id", "=", $id)
				->orderBy('contests.id', 'desc')->first();
		*/

		$contest = Contest::findOrFail($contest_id);

		Log::debug("contest result: ".print_r($contest,true));

		//$queries = DB::getQueryLog();
		//Log::debug("Queries: ".print_r($queries,true));

		$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);

		$page = fbPage::find($id);


		$admin_access_token = $page->fb_page_admin_at;


		//get PAGE ACCESS TOKEN
		$info = array("access_token" => $admin_access_token);
		$fb_request = $facebook->get('/me/accounts', $admin_access_token);

		Log::debug("Facebook response: ".print_r($fb_request->getBody(),true));
		//scorrere l'array finche non trovi la pagina giusta
		//$pages_array = $fb_request['data'];
		$pages_array = $fb_request->getGraphEdge();


		$page_access_token = null;
		foreach ($pages_array as $pagefb)
		{
			if ($pagefb['id'] == $page->fb_page_id)
			{
				$page_access_token = $pagefb['access_token'];
				break;
			}
		}

		//$contest = $page->contests->first();



		$data = array(
			"app_id" => $contest->company->fbApp_clientID,
			"position" => 2,
			"custom_name" => strip_tags($contest->name));



		$response = $facebook->post("/".$page->fb_page_id."/tabs", $data, $page_access_token);
		$graphNodeResponse = $response->getGraphNode();
		Log::debug("Facebook response for makeFacebookTab: ".print_r($graphNodeResponse, true));

	}

	/* server timezone */
	//define('CONST_SERVER_TIMEZONE', 'UTC');

	/* server dateformat */
	//define('CONST_SERVER_DATEFORMAT', 'Y-m-d H:i:s');

	public static function now($str_user_timezone)
	{

		$str_server_timezone = 'UTC';
		$str_server_dateformat = 'Y-m-d H:i:sP';
		// set timezone to user timezone
		date_default_timezone_set($str_user_timezone);

		$date = new DateTime('now', new DateTimeZone($str_user_timezone));
		$str_server_now = $date->format('Y-m-d H:i:sP');


		// return timezone to server default
		date_default_timezone_set($str_server_timezone);

		return $str_server_now;
	}


	public static function schedule($funcName)
	{
		$task = new Task();
		$task->func_name = $funcName;
		$task->status = Task::SCHEDULED;
		$task->save();
		Log::debug("Job ".$funcName." scheduled.");
	}

	public static function approval()
	{
		Log::debug("scheduler.approval() started");

		//cercare contest attivi

		$contests = Contest::where('contest_approval', '2')->get();

		foreach ($contests as $contest)
		{

			//$tz_object = new DateTimeZone($contest->location->timezoneDesc);
			$datetime = new DateTime();
			//$datetime->setTimezone($tz_object);
			$today = $datetime->format("Y-m-d H:i:s");

			Log::debug("Actual server time: ".$today);
			if (($today >= $contest->start_time ) && ($today <= $contest->end_time) && $contest->status = 1)
			{
				//cercare video in stato approval step = 1
				$videos=Video::where("videos.approval_step_id", "=", 1)
						->where("videos.contest_id", "=", $contest->id)->get();

				foreach ($videos as $video)
				{
					$video->approval_step_id = 3;
					$video->save();
				}

				//cercare foto in stato approval step = 1
				$photos=Photo::where("photos.approval_step_id", "=", 1)
						->where("photos.contest_id", "=", $contest->id)->get();

				foreach ($photos as $photo)
				{
					$photo->approval_step_id = 3;
					$photo->save();
				}

				//cercare essays in stato approval step = 1
				$essays=Essay::where("essays.approval_step_id", "=", 1)
						->where("essays.contest_id", "=", $contest->id)->get();

				foreach ($essays as $essay)
				{
					$essay->approval_step_id = 3;
					$essay->save();
				}

			}
		}

		Log::debug("scheduler.approval() finished");
	}

	public static function updateLocations() {

		Log::debug("updateLocations");

		//$activeTrend = ActiveTrend::find(1);

		//if ( $activeTrend->value == 1 ) {

			//DB::table('table2_trends')->delete();
			//DB::table('table2_trend_locations')->delete();
			//DB::table('table1_trends')->delete();
			DB::table('table1_trend_locations')->delete();

			try {

				$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  Config::get('twitter')['oauth_token'], Config::get('twitter')['oauth_token_secret']);
				$tweet_data = $twitteroauth->get('https://api.twitter.com/1.1/trends/available.json');

				Log::debug("locations founded: " . count($tweet_data) . " processing ...");

				foreach ( $tweet_data as $l ) {

					try {

						$table1TrendLocation = new Table1TrendLocation();
						$table1TrendLocation->woeid = $l->woeid;
						$table1TrendLocation->name = $l->name;
						$table1TrendLocation->processed = 0;
						$table1TrendLocation->save();

					} catch (Exception $ex) {

						Log::error('save table1TrendLocation: '.$ex);


					}

				}

			} catch (Exception $ex) {

				Log::error('getTwitterLocationsTrendsAvailable: '.$ex);
				return json_encode(array("result"=> "0", "message" => "".$ex));

			}



		/*} else if ( $activeTrend->value == 2 ) {

			DB::table('table1_trends')->delete();
			DB::table('table1_trend_locations')->delete();

			try {

				$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  Config::get('twitter')['oauth_token'], Config::get('twitter')['oauth_token_secret']);
				$tweet_data = $twitteroauth->get('https://api.twitter.com/1.1/trends/available.json');

				Log::debug("locations founded: " . count($tweet_data) . " processing ...");

				foreach ( $tweet_data as $l ) {

					try {

						$table1trendLocation = new Table1TrendLocation();
						$table1trendLocation->woeid = $l->woeid;
						$table1trendLocation->name = $l->name;
						$table1trendLocation->processed = 0;
						$table1trendLocation->save();

					} catch (Exception $ex) {

						Log::error('save Table1TrendLocation: '.$ex);


					}

				}

			} catch (Exception $ex) {

				Log::error('getTwitterLocationsTrendsAvailable: '.$ex);
				return json_encode(array("result"=> "0", "message" => "".$ex));

			}

		}
	*/
	}

	public static function getTrends() {


		print_r(Table2Trend::all()); return;

	}

	public static function updateTrendsTest() {

		/*
						$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  "3130600163-e3g7CNERQkC7ReyzxNptB03ISyWNEUiK6dTFMbO", "zRowDFdIwJfJyVHpkJ2D1vscccKTEqwurEo6ZrOUDCYUQ");
				$tweet_data = $twitteroauth->get('https://api.twitter.com/1.1/trends/available.json');

				print_r($tweet_data); return;
	*/
		$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  "", "");
		$tweet_data = $twitteroauth->get('https://api.twitter.com/1.1/trends/place.json?id=1');


		foreach ($tweet_data[0]->trends as $t) {

			try {

				$table2Trend = new Table2Trend();
				$table2Trend->name = $t->name;
				$table2Trend->save();

			} catch (Exception $ex) {

				Log::error('save Table2Trend: '.$ex);


			}

		}


	}

	public static function updateTrends() {

		Log::debug("updateTrends");

		Scheduler::updateLocations();

		/*

		$error_rateLimit = false;

		$activeTrend = ActiveTrend::find(1);

		if ( $activeTrend->value == 1 ) {

			Log::debug("active trends 1");

			//$locations = DB::table('table2_trend_locations')->where('processed', '=', 0)->get();
			$locations = DB::table('table2_trend_locations')->where('woeid', '=', '23424738')->get();

			//$id_uad = '23424738';

			try {

				foreach ( $locations as $l ) {



					$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  Config::get('twitter')['oauth_token'], Config::get('twitter')['oauth_token_secret']);
					$tweet_data = $twitteroauth->get('https://api.twitter.com/1.1/trends/place.json?id=' . $l->woeid);


					Log::debug("tweet_data: ".print_r($tweet_data,true));

					try {

						Log::debug("############");
						Log::debug("############");
						Log::debug("############");
						Log::debug("Processing trends 2 woeid: " . $l->woeid);
						Log::debug("############");
						Log::debug("############");
						Log::debug("############");


						foreach ($tweet_data[0]->trends as $t) {

							Log::debug('Processing trend '.$t->name);
							try {

								$table2Trend = new Table2Trend();
								$table2Trend->name = $t->name;
								$table2Trend->table2_trend_location_id = $l->id;
								$table2Trend->save();

							} catch (Exception $ex) {

								Log::error('save Table2Trend: '.$ex);


							}

						}

						try {

							$table2TrendLocation = Table2TrendLocation::find($l->id);
							$table2TrendLocation->processed = 1;
							$table2TrendLocation->save();

						} catch (Exception $ex) {

							Log::error('save Table2Trend: '.$ex);

						}

					} catch (Symfony\Component\Debug\Exception\FatalErrorException $ex)
					{
						break;
						$error_rateLimit = true;
						Log::debug('Error during cycling locations2');
					}

				}

				if (!$error_rateLimit)
				{
					//DEVO FARE IN MODO CHE SE C'è L'ERRORE RATE LIMIT, NON DEVE MAI AGGIORNARE LA TABELLA ACTIVE TRENDS
					Log::debug('E ENTRATO');
					$activeTrend->value = 2;
					$activeTrend->save();

					Scheduler::updateLocations();
				}



			} catch (Exception $ex) {

				Log::error('getTwitterTrendsAvailable: '.$ex);


			}




		} else if ( $activeTrend->value == 2 ) {

			Log::debug("active trends 2");

			//$locations = DB::table('table1_trend_locations')->where('processed', '=', 0)->get();
			$locations = DB::table('table1_trend_locations')->where('woeid', '=', '23424738')->get();

			//$id_uad = '23424738';

			try {

				foreach ( $locations as $l ) {



					$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  Config::get('twitter')['oauth_token'], Config::get('twitter')['oauth_token_secret']);
					$tweet_data = $twitteroauth->get('https://api.twitter.com/1.1/trends/place.json?id=' . $l->woeid);


					Log::debug("tweet_data: ".print_r($tweet_data,true));

					try {

						Log::debug("############");
						Log::debug("############");
						Log::debug("############");
						Log::debug("Processing trends 1 woeid: " . $l->woeid);
						Log::debug("############");
						Log::debug("############");
						Log::debug("############");

						foreach ($tweet_data[0]->trends as $t) {
							Log::debug('Processing trend '.$t->name);
							try {

								$table1Trend = new Table1Trend();
								$table1Trend->name = $t->name;
								$table1Trend->table1_trend_location_id = $l->id;
								$table1Trend->save();

							} catch (Exception $ex) {

								Log::error('save Table1Trend: '.$ex);


							}



						}

						try {

							$table1TrendLocation = Table1TrendLocation::find($l->id);
							$table1TrendLocation->processed = 1;
							$table1TrendLocation->save();

							Log::debug('Location processed. '.$t->name);

						} catch (Exception $ex) {

							Log::error('save Table1TrendLocation: '.$ex);

						}

					} catch (Symfony\Component\Debug\Exception\FatalErrorException $ex)
					{
						//QUI NON VA MAI, CONTROLLARE
						break;
						$error_rateLimit = true;
						Log::debug('Error during cycling locations1');
					}




				}

				if (!$error_rateLimit)
				{
					Log::debug('E ENTRATO');
					$activeTrend->value = 1;
					$activeTrend->save();

					Scheduler::updateLocations();
				}


			} catch (Exception $ex) {

				Log::error('getTwitterTrendsAvailable: '.$ex);


			}




		}

	*/

	}
	//executed every hour
	public static function runScore()
	{
		Log::debug("scheduler.runScore()");

		$contests = Contest::all();

		foreach ($contests as $contest)
		{

			//$tz_object = new DateTimeZone($contest->location->timezoneDesc);
			$datetime = new DateTime();
			//$datetime->setTimezone($tz_object);
			$today = $datetime->format("Y-m-d H:i:s");

			//App::setLocale($contest->location->language);

			//calculate score only for active contests
			Log::debug("Analyzing contest ".$contest->name);
			//Log::debug("Actual date time for contest ".$contest->name.": ".self::now($contest->location->timezoneDesc));

			if (($today >= $contest->start_time ) && ($today <= $contest->end_time) && $contest->status == 1 )
			{
				self::UpdateScorenew($contest->id);
				self::UpdateRanking($contest->id);
			}
		}
	}

	public static function run()
	{
		Log::debug("scheduler.run()");


		/*
		$tasks = Task::where("status",Task::SCHEDULED)->get();
		foreach ($tasks as $task2)
		{
			$timeStart = time();
			DB::transaction(function() use ($task2,$timeStart) {
				Log::debug("Task ".$task2->id." SELECT FOR UPDATE");
				$task = Task::where("id",$task2->id)->lockForUpdate()->firstOrFail();
				Log::debug("Task: ".print_r($task,true));
				if ($task->status == Task::SCHEDULED)
				{
					$task->last_run_at = new DateTime();
					$task->status = Task::IN_PROGRESS;
					$task->save();

					//here is task running code
					$args = explode(";", $task->func_name);
					switch ($args[0])
					{
						case "insertWatermark":
							ContestHelper::insertWatermarkVideo($args[1]);
							break;
						case "testRun":
							self::testRun($args[1]);
							break;
						case "upload2YouTube":
							YouTube::upload2YouTube($args[1]);
							break;
						case "gcmNotify":
							GCMHelper::notify($args[1]);
							break;
						case "uploadToInstagram":
							self::uploadPhotoInstagram($args[1], null);
							break;
					}

					$task->duration = time()-$timeStart;
					$task->status = Task::SUCCESS;
					$task->save();
				}
				else
				{
					Log::info("Task ".$task->id." already executed. Skipping");
				}
			});
		}

		*/

		$contests = Contest::all();


		//put function to upload the multimedia element on Facebook. If approval_step is 3 (approved) and the field id_fb_post is 0,
		//provide to upload the photo/video in the FACEBOOK Page indicated by the field "fb_page_link" of the contests table.
		//self::uploadOnFacebookPage();

		//self::sendNegativeNotify();

		foreach ($contests as $contest)
		{

			//$tz_object = new DateTimeZone($contest->location->timezoneDesc);
			//$datetime = new DateTime()->format("Y-m-d H:i:s");
			$datetime = new DateTime();
			//$datetime->setTimezone($tz_object);
			$today = $datetime->format("Y-m-d H:i:s");

			//App::setLocale($contest->location->language);

			//calculate score only for active contests
			Log::debug("Analyzing contest ".$contest->name);
			//Log::debug("Actual date time for contest ".$contest->name.": ".self::now($contest->location->timezoneDesc));




			if (($today >= $contest->start_time ) && ($today <= $contest->end_time) && $contest->status == 1 )
			{
				//instant win solo per contest in running
				//INTEGRAZIONE INSTANT WIN
				//Verrà usato il cronjob RUN che, in più, controllerà tutti i contest INSTANTWIN. Sarà ogni 15 minuti
				//Innanzitutto il contest INSTANTWIN deve contenere la durata dello slot di tempo in minuti. Questo verrà calcolato a mano.
				//Verranno analizzati solo i contest aventi attributo INSTANTWIN valorizzato (not null) e che sono già in running.
				//Si deve calcolare il range di tempo in minuti tra la data di inizio e la data attuale.
				//Se questa quantità di minuti supera il multiplo dello slot per meno di 15 minuti (range di tempo tra esecuzione job e l'altro),
				//allora bisogna fare +1 nel pool dei premi (prize_pool). Questa è una variabile-attributo dell'entità contest.
				//
				//
				$time_slot = $contest->instantwin_timeslot;
				if (isset($time_slot))
				{

					$start_date = DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time);
					$end_date = DateTime::createFromFormat('Y-m-d H:i:s', $contest->end_time);

					$date_now = new DateTime('now');
					Log::debug("Start Date contest: ".$start_date->format('Y-m-d H:i:s'));
					Log::debug("Date now: ".$date_now->format('Y-m-d H:i:s'));

					$interval = $start_date->diff($date_now);

					$minutes = $interval->days * 24 * 60;
					$minutes += $interval->h * 60;
					$minutes += $interval->i;

					Log::debug("Difference between contest start date and date_now in minutes: ".$minutes);


					if ($minutes % $time_slot < 15)
					{
						Log::debug("+1 prize pool");
						$contest->prize_pool += 1;
					}

				}


				//if contest need watermark insertion, check all the videos of the contest and insert watermark to videos without it
				if ((($contest->should_insert_contest_watermark) || ($contest->should_insert_company_watermark)) && $contest->type_id != 3 )
				{
					self::InsertWatermarkContest($contest->id);
				}

				self::CreateFacebookShareFile($contest->id);

				if (count($contest->fbPages) > 0)
				{
					self::uploadOnFacebookPage($contest->id);
				}

				if ((count($contest->fbPages) > 0) && (!is_null($contest->instagram)))
				{
					self::uploadOnFacebookPageFromInstagram($contest->id);
				}

				if ($contest->type_id == 1)
				{
					self::uploadOnYoutubeBMBPage($contest->id);
				}
				if (count($contest->ytPages) > 0)
				{
					self::uploadOnYoutubeCorporatePage($contest->id);
				}

				if (count($contest->twPages) > 0)
				{
					self::uploadOnTwitterPage($contest->id);
				}


				self::emailNotification($contest->id);
				//self::UpdateScorenew($contest->id);
				//self::UpdateRanking($contest->id);
			}




			// Changing status, email notification
			$contestStartTime2 = DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time);
			$contestStartTime = DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time);
			$contestStartTime2->add(DateInterval::createFromDateString('30 mins'));

			$contestEndTime2 = DateTime::createFromFormat('Y-m-d H:i:s', $contest->end_time);
			$contestEndTime = DateTime::createFromFormat('Y-m-d H:i:s', $contest->end_time);
			$contestEndTime2->add(DateInterval::createFromDateString('30 mins'));


			//$date = new DateTime('now', new DateTimeZone($str_user_timezone));
			//$str_server_now = $date->format('Y-m-d H:i:sP');






			//$tz_object = new DateTimeZone($contest->location->timezoneDesc);
			$datetime = new DateTime('now');
			//$datetime->setTimezone($tz_object);
			//$today = $datetime->format("Y-m-d H:i:sP");
			$today = $datetime->format("Y-m-d H:i:s");


			Log::debug("now: " . $today . " start time 1: " . $contestStartTime->format("Y-m-d H:i:s") . " start time 2: " . $contestStartTime2->format("Y-m-d H:i:s") );
			if ( $datetime >= $contestStartTime && $datetime <= $contestStartTime2 && $contest->status == 3 && $contest->confirmed_by_customer == 1 ) {

				$contest->status = 1;
				$user = User::where("company_id", "=", $contest->company_id)->first();

				self::ChargeCustomer($contest->id);

				$fbPage = $contest->fbPages()->first();

				//$fbPage = fbPage::where("fb_page_id", "=", $pageId)->first();

				try {
					$request = Request::create('/make-facebook-tab/' . $fbPage->id , 'GET', array());
					Request::replace($request->input());
					Route::dispatch($request)->getContent();
					/*
					$request = Request::create('/update-facebook-tab/' . $fbPage->id , 'GET', array());
					Request::replace($request->input());
					Route::dispatch($request)->getContent();
					*/
				} catch (Exception $ex) {
					Log::error('facebookCustomerConnect: '.$ex);
				}

				try {
					Log::debug('Sending email to the customer for contest start');
					Mail::send('emails.contest_start', array('user' => $user, 'contest'=> $contest), function($message) use ($user) {

						$message->to($user->email, $user->name . " " . $user->surname)
								->subject(Lang::get('email.object_contest_start'));

					});

				} catch (Exception $ex) {
					Log::error('Problem sending email: '.$ex);
				}


				/*
				Auth::login($user);
				try {
					$request = Request::create('/admin/insert-notification/' . $contest->id, 'POST', array(
																			'text' => Lang::get('notifications.contest_start'),
																		'redirect' => asset("/dasboard/index.html"),));


					Request::replace($request->input());

					Route::dispatch($request)->getContent();

				} catch (Exception $ex) {
					Log::error('Problem sending notification: '.$ex);
				}
				*/
			}

			Log::debug("now: " . $datetime->format("Y-m-d H:i:s") . " end time 1: " . $contestEndTime->format("Y-m-d H:i:s") . " end time 2: " . $contestEndTime2->format("Y-m-d H:i:s") );
			if ( $datetime >= $contestEndTime && $datetime <= $contestEndTime2  && $contest->status == 1) {
				$contest->status = 2;
				$user = User::where("company_id", "=", $contest->company_id)->first();

				try {
					Log::debug('Sending email to the customer for contest end');
					Mail::send('emails.contest_end', array('name' => $user->name, 'contest_name' => $contest->name), function($message) use ($user) {

						$message->to($user->email, $user->name . " " . $user->surname)
								->subject(Lang::get('email.object_contest_end'));

					});

				} catch (Exception $ex) {
					Log::error('Problem sending email: '.$ex);
				}
				/*
				Auth::login($user);
				try {
					$request = Request::create('/admin/insert-notification/' . $contest->id, 'POST', array(
																			'text' => Lang::get('notifications.contest_end'),
																		'redirect' => asset("/dasboard/index.html"),));


					Request::replace($request->input());

					Route::dispatch($request)->getContent();

				} catch (Exception $ex) {
					Log::error('Problem sending notification: '.$ex);
				}
				*/
			}

			$contest->save();
			/////////////////////////////////////////


		}



		//self::likesfromLogin();

		//self::uploadPhotoInstagram();

		Log::debug("scheduler.run() finished");

	}

	public static function ChargeCustomer($contest_id)
	{
		Log::debug('Charging customer');
		$contest = Contest::findOrFail($contest_id);
		$user = User::where("company_id", $contest->company_id)->first();

		if ($contest->service_type != '0')
		{

			$fee = ServiceFee::findOrFail($contest->service_type);
            $invoice = Invoice::where('contest_id', $contest_id)->first();

            $discountable = false;
            if (strlen($invoice->coupon) > 0)
            {
                $discountable = true;
            }

			if ((isset($invoice->customer_id)) && (!empty($invoice->customer_id)))
			{
				Stripe::setApiKey("sk_test_OeVAPOsmdoAqkC4zkwzU0P5o");
				//creare invoice item
				$invoiceItem = Stripe_InvoiceItem::create(array(
					"customer" => $invoice->customer_id,
					"amount" => intval($invoice->amount),
					"currency" => $contest->currency,
					"description" => $invoice->description,
					"discountable" => $discountable
					//"invoice" => $invoice_id
				));

				//creare invoice
				$invoiceStripe = Stripe_Invoice::create(array(
					"customer" => $invoice->customer_id,
					"description" =>$contest->name
				));

				//pay now
				$invoiceStripe->pay();

				$invoice->invoice_stripe_id = $invoiceStripe->id;
				$invoice->is_paid = 1;
				$invoice->save();
			}
			else
			{
				//contest gratis
				$invoice->is_paid = 1;
				$invoice->save();
			}



			/*
			$description = $fee->getInvoiceDescription();
			$transaction = CreditCard::chargeUser($user, $fee->fixed_fee, $description);
			$invoice = Invoice::generate($user, $fee->fixed_fee, $description);
			$transaction->invoice_id = $invoice->id;
			$transaction->save();

			UserCredit::loadCredit($fee, $user);

			$idCredit = UserCredit::chargeCredit($user, $contest);
			*/

		}
	}

	public static function emailApproveContentNotification()
	{

		$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);

		$contests = Contest::all();


		foreach ($contests as $contest)
		{


			$datetime = new DateTime();
			$today = $datetime->format("Y-m-d H:i:s");

			Log::debug("Analyzing contest for notification ".$contest->name);


			if (($today >= $contest->start_time ) && ($today <= $contest->end_time) && $contest->status == 1 )
			{

				try {

					$videos=Video::select("videos.*")
						->join("contests", "contests.id", "=", "videos.contest_id")
						->where("videos.approval_step_id", "=", 1)
						->where("videos.contest_id", "=", $contest_id)->get();

					Log::debug("Number of videos returned for send email: ".count($videos));

					$user = User::where("company_id", $contest->company_id)->first();

					if ((count($videos) > 0) && ($user)) {
						try {

							Mail::send('emails.approve-content', array('contest' => $contest, 'entries_to_approve' => count($videos)), function($message) use ($user) {

								$message->to($user->email, $user->name . " " . $user->surname)
										->subject(Lang::get('email.object_approve-content'));

							});

						} catch (Exception $ex) {
							Log::error('Problem sending email: '.$ex);
						}


						App::setlocale($contest->location->language);
						$messageNotif = Lang::get('messages.approve-content', array('contestname' => $contest->name));

						$facebookProfile = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();

						$responseFBAPP = $facebook->post("/".$facebookProfile->id_fb."/notifications", array(
							"template" => $messageNotif,// and ask your friends to like, share, comment to increase your chances to win! The more your video is viral the more chances you have to win!
							"href" => "/",
						), $contest->company->app_accesstoken );

					}


					$photos=Video::select("photos.*")
						->join("contests", "contests.id", "=", "photos.contest_id")
						->where("photos.approval_step_id", "=", 1)
						->where("photos.contest_id", "=", $contest_id)->get();

					Log::debug("Number of photos returned for send email: ".count($photos));

					$user = User::where("company_id", $contest->company_id)->first();

					if ((count($photos) > 0) && ($user)) {
						try {

							Mail::send('emails.approve-content', array('contest' => $contest, 'entries_to_approve' => count($photos)), function($message) use ($user) {

								$message->to($user->email, $user->name . " " . $user->surname)
										->subject(Lang::get('email.object_approve-content'));

							});


						} catch (Exception $ex) {
							Log::error('Problem sending email: '.$ex);
						}

						App::setlocale($contest->location->language);
						$messageNotif = Lang::get('messages.approve-content', array('contestname' => $contest->name));

						$facebookProfile = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();

						$responseFBAPP = $facebook->post("/".$facebookProfile->id_fb."/notifications", array(
							"template" => $messageNotif,// and ask your friends to like, share, comment to increase your chances to win! The more your video is viral the more chances you have to win!
							"href" => "/",
						), $contest->company->app_accesstoken );

					}


					$essays=Essay::select("essays.*")
						->join("contests", "contests.id", "=", "essays.contest_id")
						->where("essays.approval_step_id", "=", 1)
						->where("essays.contest_id", "=", $contest_id)->get();

					Log::debug("Number of essays returned for send email: ".count($essays));

					$user = User::where("company_id", $contest->company_id)->first();

					if ((count($essays) > 0) && ($user)) {
						try {

							Mail::send('emails.approve-content', array('contest' => $contest, 'entries_to_approve' => count($essays)), function($message) use ($user) {

								$message->to($user->email, $user->name . " " . $user->surname)
										->subject(Lang::get('email.object_approve-content'));

							});



						} catch (Exception $ex) {
							Log::error('Problem sending email: '.$ex);
						}

						App::setlocale($contest->location->language);
						$messageNotif = Lang::get('messages.approve-content', array('contestname' => $contest->name));

						$facebookProfile = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();

						$responseFBAPP = $facebook->post("/".$facebookProfile->id_fb."/notifications", array(
							"template" => $messageNotif,// and ask your friends to like, share, comment to increase your chances to win! The more your video is viral the more chances you have to win!
							"href" => "/",
						), $contest->company->app_accesstoken );

					}

				}
				catch (Exception $ex) {
					Log::error('Problem sending notification approve content email: '.$ex);
				}


			}
		}


	}

	public static function emailNotification($contest_id)
	{


		$contest = Contest::find($contest_id);

		if ($contest_id == "259")
		{
			Log::debug("Email notification for Comieco contest");

			// Backup your default mailer
			$backup = Mail::getSwiftMailer();

			// Setup your gmail mailer
			$transport = SmtpTransport::newInstance('host32.registrar-servers.com', 465, 'ssl');
			$transport->setUsername('info@iorompolescatole.com');
			$transport->setPassword('comieco2015');
			// Any other mailer configuration stuff needed...

			$comiecomail = new Swift_Mailer($transport);

			// Set the mailer as gmail
			Mail::setSwiftMailer($comiecomail);

			// Send your message
			//Mail::send();

			try
			{

				$photos=Photo::select("photos.*")
					->join("contests", "contests.id", "=", "photos.contest_id")
					->where("photos.approval_step_id", "=", 3)
					->where("photos.emailNotificationDone", "=", 0)
					->where("photos.contest_id", "=", $contest_id)
					->get();

				Log::debug("Number of photos returned for send email: ".count($photos));

				foreach($photos as $photo) {

					$contest = $photo->contest;
					$user = $photo->user;

					try {

						Mail::send('emails.photo-approvation', array('contest' => $contest, 'photo' => $photo), function($message) use ($user) {

							$message->to($user->email, $user->name . " " . $user->surname)
									->subject(Lang::get('email.object_photo_contest_approved'));

						});

						$photo->emailNotificationDone = 1;

					} catch (Exception $ex) {
						Log::error('Problem sending email: '.$ex);
					}

					$photo->save();

				}

			}
			catch (Exception $ex)
			{
				Log::debug("Exception occurred during emailNotification " . $ex);
			}

			// Restore your original mailer
			Mail::setSwiftMailer($backup);


		}
		else if ($contest_id == "628") {

			try {

				//$videos=Video::select("videos.*")
				//	->join("contests", "contests.id", "=", "videos.contest_id")
				//	->where("videos.approval_step_id", "!=", 1)
				//	->where("videos.emailNotificationDone", "=", 0)
				//	->where("videos.contest_id", "=", $contest_id)
				//	->get();

				$videos=Video::select("videos.*")
					->where("videos.id", "=", 1926)->get();

				Log::debug("Number of Toys Center videos returned for send email: ".count($videos));

				foreach($videos as $video) {
					Log::debug('Video id:'.$video->id);
					$contest = $video->contest;
					$user = $video->user;
					Log::debug('User associated:'.print_r($user, true));
					$approved = 'no';
					$customized_email = null;

					try {
						if ($video->approval_step_id == '3')
							$approved = 'yes';
						else if ($video->approval_step_id == '2')
							$approved = 'no';

						if ($user) {

							//$customized_email = str_replace('@', '+approved@', $user->survey_email);

							$queryString = 'title='.$video->name.'&name=paolo&lastname=cappelluti&email=paolo.cappelluti+approved@gmail.com&url=test&approved='.$approved;
							//$queryString = 'title='.$video->name.'&name='.$user->name.'&lastname='.$user->surname.'&email='.$customized_email.'&url='.$video->fb_source_url.'&approved='.$approved;

							Log::debug("Query String generated: ".$queryString);

							$myInstanceEncrypt = new ToysCenterEncrypt();

							$encryptedquerystring = $myInstanceEncrypt->encrypt($queryString);

							Log::debug("Enripted Query String generated: ".$encryptedquerystring);

							$cryptQueryBase64 = base64_encode($encryptedquerystring);

							Log::debug("Base64 ofEnripted Query String: ".$cryptQueryBase64);

							$ch = curl_init();
							curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
							$params = array(
								"q"=>$cryptQueryBase64,
							);
							curl_setopt($ch,CURLOPT_URL,"http://www.toyscenter.it/emailapprove/index/send");
							curl_setopt($ch,CURLOPT_POST,true);
							curl_setopt($ch,CURLOPT_POSTFIELDS,http_build_query($params));
							$result = curl_exec($ch);
							Log::debug("Response from toys center: ".$result);

							if ($result == "EMAIL CORRECTLY SENT") {
								$video->emailNotificationDone = 1;
							}
							else {
								$video->emailNotificationDone = 0;
							}


							$video->save();

						}



					} catch (Exception $ex) {
						Log::error('Problem sending email: '.$ex);
					}

					//$video->save();

				}


			} catch (Exception $ex) {

				Log::debug("Exception occurred during emailNotification " . $ex);

			}

		}
		else if ( $contest->needsUserRegistration() ) {

			try
			{


				$videos=Video::select("videos.*")
					->join("contests", "contests.id", "=", "videos.contest_id")
					->where("videos.approval_step_id", "=", 3)
					->where("videos.watermarkInsDone", "=", 1)
					->where("videos.emailNotificationDone", "=", 0)
					->where("videos.contest_id", "=", $contest_id)->get();

				Log::debug("Number of videos returned for send email: ".count($videos));

				foreach($videos as $video) {

					$contest = $video->contest;
					$user = $video->user;

					try {

						Mail::send('emails.video-approvation', array('contest' => $contest, 'video' => $video), function($message) use ($user) {

							$message->to($user->email, $user->name . " " . $user->surname)
									->subject(Lang::get('email.object_video_contest_approved'));

						});

						$video->emailNotificationDone = 1;

					} catch (Exception $ex) {
						Log::error('Problem sending email: '.$ex);
					}

					$video->save();

				}


				$photos=Photo::select("photos.*")
					->join("contests", "contests.id", "=", "photos.contest_id")
					->where("photos.approval_step_id", "=", 3)
					->where("photos.emailNotificationDone", "=", 0)
					->where("photos.contest_id", "=", $contest_id)
					->get();

				Log::debug("Number of photos returned for send email: ".count($photos));

				foreach($photos as $photo) {

					$contest = $photo->contest;
					$user = $photo->user;

					try {

						Mail::send('emails.photo-approvation', array('contest' => $contest, 'photo' => $photo), function($message) use ($user) {

							$message->to($user->email, $user->name . " " . $user->surname)
									->subject(Lang::get('email.object_photo_contest_approved'));

						});

						$photo->emailNotificationDone = 1;

					} catch (Exception $ex) {
						Log::error('Problem sending email: '.$ex);
					}

					$photo->save();

				}

				$essays = Essay::select("essays.*")
					->join("contests", "contests.id", "=", "essays.contest_id")
					->where("essays.approval_step_id", "=", 3)
					->where("essays.emailNotificationDone", "=", 0)
					->where("essays.contest_id", "=", $contest_id)
					->get();

				Log::debug("Number of essays returned for send email: ".count($essays));

				foreach($essays as $essay) {

					$contest = $essay->contest;
					$user = $essay->user;

					try {

						Mail::send('emails.essay-approvation', array('contest' => $contest, 'essay' => $essay), function($message) use ($user) {

							$message->to($user->email, $user->name . " " . $user->surname)
									->subject(Lang::get('email.object_essay_contest_approved'));

						});

						$essay->emailNotificationDone = 1;

					} catch (Exception $ex) {
						Log::error('Problem sending email: '.$ex);
					}

					$essay->save();

				}



			} catch (Exception $ex) {

				Log::debug("Exception occurred during emailNotification " . $ex);

			}

		}



	}



	public static function deletePostTwitter($page, $postId) {

		Log::debug("Deleting from twitter with post id: " . $postId);

		try {

			$parameters = array();

			$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $page->tw_page_admin_at, $page->tw_page_secret_token);
			$tweet_data = $twitteroauth->post('https://api.twitter.com/1.1/statuses/destroy/'. $postId . '.json', $parameters);

			Log::debug('Response for deletion post: '.print_r($tweet_data, true));

		} catch(Exception $ex) {

			Log::error($ex);

		}

	}

	public static function deleteVideoPost($id, $page, $idvideo)
	{

		Log::debug("Deleting video with id: ".$id);
		try
		{
			$video = Video::where('id', $id)->first();

			$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);

			$admin_access_token = $page->fb_page_admin_at;


			$info = array("access_token" => $admin_access_token);
			$fb_request = $facebook->get('/me/accounts', $admin_access_token);

			Log::debug("Facebook response for getting access token: ".print_r($fb_request->getBody(),true));
			//scorrere l'array finche non trovi la pagina giusta
			//$pages_array = $fb_request['data'];
			$pages_array = $fb_request->getGraphEdge();

			//$pages_array = $fb_request['data'];
			$real_page_access_token = null;
			foreach ($pages_array as $fbpage)
			{
				if ($fbpage['id'] == $page->fb_page_id)
				{
					$real_page_access_token = $fbpage['access_token'];
					break;
				}
			}

			Log::debug("Facebook real page access token: ".$real_page_access_token);

			$data = array(
					"access_token" => $real_page_access_token,
					);

			$response = $facebook->delete("/".$idvideo, null, $real_page_access_token);
			//Log::debug("Response for deletion post: ".$response['id']);


		}
		catch (Exception $ex)
		{
			Log::error($ex);
		}

	}

	public static function deletePhotoPost($id, $page, $idphoto)
	{
		Log::debug("Deleting photo with id: ".$id);
		try
		{
			$photo = Photo::where('id', $id)->first();

			$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);


			//Delete photo from the wall Facebook Page
			$admin_access_token = $page->fb_page_admin_at;

			//get REAL PAGE ACCESS TOKEN
			$info = array("access_token" => $admin_access_token);
			$fb_request = $facebook->get('/me/accounts', $admin_access_token);
			//scorrere l'array finche non trovi la pagina giusta

			Log::debug("Facebook response for getting access token: ".print_r($fb_request->getBody(),true));
			//scorrere l'array finche non trovi la pagina giusta
			//$pages_array = $fb_request['data'];
			$pages_array = $fb_request->getGraphEdge();

			//$pages_array = $fb_request['data'];
			$real_page_access_token = null;
			foreach ($pages_array as $fbpage)
			{
				if ($fbpage['id'] == $page->fb_page_id)
				{
					$real_page_access_token = $fbpage['access_token'];
					break;
				}
			}
			//$real_page_access_token = $fb_request['data']['0']['access_token'];
			Log::debug("Facebook real page access token: ".$real_page_access_token);
			Log::debug("Id photo post: ".$idphoto);

			$data = array(
					"access_token" => $real_page_access_token,
					);

			$response = $facebook->delete("/".$idphoto, null, $real_page_access_token);
			//Log::debug("Response for deletion photo post: ".$response);


		}
		catch (Exception $ex)
		{
			Log::error($ex);

			try
			{

				$data = array(
						"access_token" => $admin_access_token,
						);

				$response = $facebook->delete("/".$idphoto, null, $admin_access_token);

			} catch (Exception $ex){

				Log::error($ex);
			}

		}

	}

	public static function deleteEssayPost($id, $page, $idessay)
	{
		Log::debug("Deleting essay with id: ".$id);
		try
		{
			$essay = Essay::where('id', $id)->first();

			$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);

			//Delete photo from the wall Facebook Page
			$admin_access_token = $page->fb_page_admin_at;

			//get REAL PAGE ACCESS TOKEN
			$info = array("access_token" => $admin_access_token);
			$fb_request = $facebook->get('/me/accounts', $admin_access_token);
			//scorrere l'array finche non trovi la pagina giusta

			Log::debug("Facebook response for getting access token: ".print_r($fb_request->getBody(),true));
			//scorrere l'array finche non trovi la pagina giusta
			//$pages_array = $fb_request['data'];
			$pages_array = $fb_request->getGraphEdge();

			//$pages_array = $fb_request['data'];
			$real_page_access_token = null;
			foreach ($pages_array as $fbpage)
			{
				if ($fbpage['id'] == $page->fb_page_id)
				{
					$real_page_access_token = $fbpage['access_token'];
					break;
				}
			}
			//$real_page_access_token = $fb_request['data']['0']['access_token'];
			Log::debug("Facebook real page access token: ".$real_page_access_token);
			Log::debug("Id essay post: ".$idessay);

			$data = array(
					"access_token" => $real_page_access_token,
					);

			$response = $facebook->delete("/".$idessay, null, $real_page_access_token);
			//Log::debug("Response for deletion photo post: ".$response);


		}
		catch (Exception $ex)
		{
			Log::error($ex);

			try
			{

				$data = array(
						"access_token" => $admin_access_token,
						);

				$response = $facebook->delete("/".$idessay, null, $admin_access_token);

			} catch (Exception $ex){

				Log::error($ex);
			}

		}

	}

	public static function CreateFacebookShareFile($contest_id)
	{
		$contest=Contest::find($contest_id);

		if ($contest->type_id == 1) //video contest
		{
			$videos=Video::select("videos.*")
						->join("contests", "contests.id", "=", "videos.contest_id")
						->join("companys","companys.id", "=", "contests.company_id")
						->where("videos.contest_id", $contest_id)
						->where("videos.approval_step_id", 3)->get();

			foreach ($videos as $video)
			{
				try {
					self::replace_in_file($video, $contest->type_id);
				}
				catch (Exception $ex) {
					Log::error($ex);
				}

			}

		}
		else if ($contest->type_id == 2)
		{
			$photos=Photo::select("photos.*")
						->join("contests", "contests.id", "=", "photos.contest_id")
						->join("companys","companys.id", "=", "contests.company_id")
						->where("photos.contest_id", $contest_id)
						->whereNull("photos.user_connected_to_instagram")
						->where("photos.approval_step_id", 3)->get();

			foreach ($photos as $photo)
			{

				try {
					self::replace_in_file($photo, $contest->type_id);
				}
				catch (Exception $ex) {
					Log::error($ex);
				}

			}
		}
		else if ($contest->type_id == 3)
		{
			$essays=Essay::select("essays.*")
						->join("contests", "contests.id", "=", "essays.contest_id")
						->join("companys","companys.id", "=", "contests.company_id")
						->where("essays.contest_id", $contest_id)
						->where("essays.approval_step_id", 3)->get();

			foreach ($essays as $essay)
			{
				try {
					self::replace_in_file($essay, $contest->type_id);
				}
				catch (Exception $ex) {
					Log::error($ex);
				}
			}
		}
	}

	public static function InsertWatermarkContest($contest_id)
	{

		try
		{

			//find kind of contest
			$contest=Contest::find($contest_id);

			if ($contest->type_id == 1) //video contest
			{
				Log::debug("Watermark insertion for videos");
				//analyze all the videos of the related contest, check if the watermark is already inserted. If no, insert it.
				$videos=Video::select("videos.*")
						->join("contests", "contests.id", "=", "videos.contest_id")
						->join("companys","companys.id", "=", "contests.company_id")
						//->where("companys.type_id",Company::TYPE_INTERNAL)
						->where("videos.contest_id", $contest_id)
						->where("videos.watermarkInsDone", 0)
						->where("videos.approval_step_id", 3)->get();

				foreach ($videos as $video)
				{


					//$output = shell_exec("sh ".base_path()."/sh/GenerateFacebookShareHTMLForUploadedVideo.sh ".$video->filename);
					//Log::debug("Output GenerateFacebookShareHTMLForUploadedVideo: ".$output);
					//$output = shell_exec("sh ".base_path()."/public/GenerateThumbnails.sh ".$video->filename);
					//Log::debug("Output GenerateThumbnails: ".$output);

					try {

						ContestHelper::insertWatermarkVideo($video->id);
						//self::replace_in_file($video, 1);

					} catch (Exception $ex) {
						Log::error($ex);
					}
				}
			}
			else if ($contest->type_id == 2) //photo contest
			{
				//insert the watermark also for photos!!!!!!!
				Log::debug("Watermark insertion for photos");
				$photos=Photo::select("photos.*")
						->join("contests", "contests.id", "=", "photos.contest_id")
						->join("companys","companys.id", "=", "contests.company_id")
						//->where("companys.type_id",Company::TYPE_INTERNAL)
						->where("photos.contest_id", $contest_id)
						->where("photos.watermarkInsDone", 0)
						->whereNull("photos.user_connected_to_instagram")
						->where("photos.approval_step_id", 3)->get();

				foreach ($photos as $photo)
				{

					//$output = shell_exec("sh ".base_path()."/sh/GenerateFacebookShareHTMLForUploadedPhoto.sh ".$photo->filename);
					//Log::debug("Output GenerateFacebookShareHTMLForUploadedPhoto: ".$output);
					//$output = shell_exec("sh ".base_path()."/sh/GeneratePhotoThumbnails.sh ".$photo->filename);
					//Log::debug("Output GeneratePhotoThumbnails: ".$output);

					try {

						ContestHelper::insertWatermarkPhoto($photo->id, 2);
						//self::replace_in_file($photo, 2);

					} catch (Exception $ex) {
						Log::error($ex);
					}
				}
			}
			/*
			else if ($contest->type_id == 3) //essay contest
			{
				//insert the watermark also for photos!!!!!!!
				Log::debug("Watermark insertion for essays");
				$essays=Essay::select("essays.*")
						->join("contests", "contests.id", "=", "essays.contest_id")
						->join("companys","companys.id", "=", "contests.company_id")
						->where("companys.type_id",Company::TYPE_INTERNAL)
						->where("essays.contest_id", $contest_id)
						->where("essays.watermarkInsDone", 0)
						->where("essays.approval_step_id", 3)->get();


				foreach ($essays as $essay)
				{

					//$output = shell_exec("sh ".base_path()."/sh/GenerateFacebookShareHTMLForUploadedPhoto.sh ".$photo->filename);
					//Log::debug("Output GenerateFacebookShareHTMLForUploadedPhoto: ".$output);
					//$output = shell_exec("sh ".base_path()."/sh/GeneratePhotoThumbnails.sh ".$photo->filename);
					//Log::debug("Output GeneratePhotoThumbnails: ".$output);



					try {

						ContestHelper::insertWatermarkPhoto($essay->id, 3);
						self::replace_in_file($essay, 3);

					} catch (Exception $ex) {
						Log::error($ex);
					}


				}
			}
			*/





		}
		catch (Exception $ex)
		{
			Log::error($ex);
		}


	}

	/*
	public static function sendNegativeNotify()
	{
		try
		{
			//select all the videos with approval_step_id = 2 (video not approved)
			$videos=Video::select("videos.*")
				->join("contests", "contests.id", "=", "videos.contest_id")
				->join("companys","companys.id", "=", "contests.company_id")
				->where("companys.type_id",Company::TYPE_INTERNAL)
				->where("videos.id_fb_post", "=", 0)
				->where("videos.approval_step_id", "=", 2)
				->where("contests.contest_approval", "=", 1)->get();

			Log::debug("Number of videos with negative approval returned: ".count($videos));

			foreach ($videos as $video)
			{
				Log::debug("Video that correspond to the correct requisites for sendNegativeNotify(): ".$video->name);

				$message = PushNotification::Message('Your video entitled "'.$video->name.'"  for '.$video->contest->name.' contest has been not approved! Follow the guidelines and try again!',array(
					'title' => $video->contest->name,
					'url' => ''));
				$app = '';
				if (strpos($video->user->token,'Android_') !== false)
				{
					$deviceToken = str_replace("Android_", "", $video->user->token);
					Log::debug("Device token to be used: ".$deviceToken);
					$app = 'jtmAndroid';
				}
				else if (strpos($video->user->token,'IOS_') !== false)
				{
					$deviceToken = str_replace("IOS_", "", $video->user->token);
					Log::debug("Device token to be used: ".$deviceToken);
					$app = 'jtmiOS';

				}

				PushNotification::app($app)
					->to($deviceToken)
					->send($message);


				//soft delete the video
				//$video->delete();
			}

			//select all the photos with approval_step_id = 2 (photo not approved)
			$photos=Photo::select("photos.*")
				->join("contests", "contests.id", "=", "photos.contest_id")
				->join("companys","companys.id", "=", "contests.company_id")
				->where("companys.type_id",Company::TYPE_INTERNAL)
				->where("photos.id_fb_post", "=", 0)
				->where("photos.approval_step_id", "=", 2)
				->where("contests.contest_approval", "=", 1)->get();

			Log::debug("Number of photos with negative approval returned: ".count($photos));

			foreach ($photos as $photo)
			{
				Log::debug("Photo that correspond to the correct requisites for sendNegativeNotify(): ".$photo->name);

				$message = PushNotification::Message('Your photo entitled "'.$photo->name.'"  for '.$photo->contest->name.' contest has been not approved! Follow the guidelines and try again!',array(
					'title' => $photo->contest->name,
					'url' => ''));
				$app = '';
				if (strpos($photo->user->token,'Android_') !== false)
				{
					$deviceToken = str_replace("Android_", "", $photo->user->token);
					Log::debug("Device token to be used: ".$deviceToken);
					$app = 'jtmAndroid';
				}
				else if (strpos($photo->user->token,'IOS_') !== false)
				{
					$deviceToken = str_replace("IOS_", "", $photo->user->token);
					Log::debug("Device token to be used: ".$deviceToken);
					$app = 'jtmiOS';

				}

				PushNotification::app($app)
					->to($deviceToken)
					->send($message);


				//soft delete the photo
				//$photo->delete();
			}
		}
		catch (Exception $ex)
		{
			Log::error($ex);
		}
	}
	*/
	public static function uploadOnFacebookPage($contest_id)
	{

		try
		{
			//select all the videos with contests that need approval of content, with approval_step=3 and id_fb_post=0
			//manca la gestione eventuale dei video da instagram

			$videos=Video::select("videos.*")
				->join("contests", "contests.id", "=", "videos.contest_id")
				->where("videos.approval_step_id", "=", 3)
				->where("videos.watermarkInsDone", "=", 1)
				->where("videos.uploadOnFacebookDone", "=", 0)
				->where("videos.contest_id", "=", $contest_id)->get();

			/*$roles = (string) User::find(1)->roles;*/
			Log::debug("Number of videos returned for upload on Facebook: ".count($videos));

			foreach ($videos as $video)
			{

				try {

					/*
					$output = shell_exec("sh ".base_path()."/public/GenerateThumbnails.sh ".$video->filename);
					Log::debug("Output GenerateThumbnails: ".$output);
					*/

					//here upload the video on FACEBOOK page because the contest does not need approval of content

					Log::debug("Video that correspond to the correct requisites for upload on FB: ".$video->name);
					$user=User::find($video->user_id);
					$contest=Contest::find($video->contest_id);

					$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);

					//$facebook->setFileUploadSupport(true);
					$facebookProfile = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
					Log::debug("FacebookProfile user id :" . $user->id . "company id " . $contest->company_id);


					if ( $facebookProfile && $video->user_connected_to_facebook) {
						$fbUserID = $facebookProfile->id_fb;
					}

					$pages_array = $contest->fbPages;

					$source_fb = '';

					//Qui devo ciclare per tutte le pagine FB connesse al contest
					//Quindi devo costruirmi l'array per individuare le pagine FB connesse al contest
					foreach ($contest->fbPages as $page)
					{


						//vedere http://www.developed.be/2013/08/30/laravel-4-pivot-table-example-attach-and-detach/
						$admin_access_token = $page->fb_page_admin_at;
						$fb_page_id = $page->fb_page_id;
						$fb_page_link = $page->fb_page_link;

						//get PAGE ACCESS TOKEN
						//$info = array("access_token" => $admin_access_token);
						$fb_request = $facebook->get('/me/accounts', $admin_access_token);

						Log::debug("Facebook response for getting access token: ".print_r($fb_request->getBody(),true));
						//scorrere l'array finche non trovi la pagina giusta
						$pages_array = $fb_request->getGraphEdge();
						$page_access_token = null;
						foreach ($pages_array as $pagefb)
						{
							if ($pagefb['id'] == $page->fb_page_id)
							{
								$page_access_token = $pagefb['access_token'];
								break;
							}
						}
						//$real_page_access_token = $fb_request['data']['0']['access_token'];
						Log::debug("Facebook page access token: ".$page_access_token);

						//Fare replace sul testo "message"

						/*
						$todoReplacement = array("%contestname%", "%type%", "%companylink%", "%companyname%", "%hashtag%");
						$newWords = array($contest->name, $contest->contestType, "http://app.buzzmybrand.it/" . $contest->contest_route, $contest->company_name, $contest->hashtag);
						$message = str_replace($todoReplacement, $newWords, $contest->objective['sentence_post']);
						*/
						$message = utf8_decode($video->name);
						Log::debug("Message for page post: ".$message);


						/*
						if ($page->should_insert_custom_wt)
						{
							$videoToUpload = public_path()."/videos/".$video->filename;
							$inputVideo = explode('.', $videoToUpload);
							$fileToUpload = $inputVideo[0]."_".$page->id.".".$inputVideo[1];
						}
						else
						{
							$fileToUpload = public_path()."/videos/".$video->filename;
						}
						*/
						$fileToUpload = public_path()."/videos/".$video->filename;

						//upload video to facebook page
						$data = array(
							"source" => $facebook->fileToUpload($fileToUpload),
							"title" => $message,
							"description" => $message);


						if ($page->fb_no_story == "YES")
						{
							Log::debug("Contest with unpublished posts");
							$new_data = array("no_story" => "true");
							$data = array_merge($data, $new_data);
						}

						//vedere se esiste album avente nome corrispondente a quello del contest
						$albums =  $facebook->get('/'.$page->fb_page_id.'/video_lists', $page_access_token);

						$album_array = $albums->getGraphEdge();

						Log::debug("VideoList array: ".print_r($album_array,true));

						$album_id = null;
						foreach ($album_array as $album)
						{
							if ($album['title'] == $contest->name)
							{
								$album_id = $album['id'];
								break;
							}
						}

						Log::debug("VideoList where entry will be uploaded: ".$album_id);

						if ( $album_id == null ) {

							$album_details = array(
								'title' => $contest->name,
							);

							$create_album = $facebook->post('/'.$page->fb_page_id.'/video_lists', $album_details,  $page_access_token);

							$graphNodealbum = $create_album->getGraphNode();
							Log::debug("CreateAlbum response: ".print_r($graphNodealbum,true));


							//vedere se esiste album avente nome corrispondente a quello del contest
							$albums =  $facebook->get('/'.$page->fb_page_id.'/video_lists', $page_access_token);

							$album_array = $albums->getGraphEdge();

							Log::debug("VideoList array: ".print_r($album_array,true));

							$album_id = null;
							foreach ($album_array as $album)
							{
								if ($album['title'] == $contest->name)
								{
									$album_id = $album['id'];
									break;
								}
							}
							$page->fb_id_album = $album_id;
							$page->save();

						}



						$response = $facebook->post("/".$page->fb_page_id."/videos", $data, $page_access_token);




						$graphNode = $response->getGraphNode();
						Log::debug("Facebook response for new Video Upload: ".print_r($graphNode,true));
						$id_post = $graphNode['id'];





						//$id_post = $response['id'];
						Log::debug("Id post in the Facebook page for the video: ".$id_post);

						$params = array(
							"video_ids" => array($id_post));

						$response = $facebook->post("/".$page->fb_id_album."/videos", $params, $page_access_token);
						/*
						if ( $facebookProfile && $video->user_connected_to_facebook) {

							//tag all'utente, se ha messo like a JMT si vede il post nella timeline, altrimenti nada.
							//monitorare il tag al video durante post-approvazione, non sempre lo fa.
							$count = 0;
							$maxTries = 3;
							while($count<$maxTries)
							{
								try
								{

									$facebook->api('/v2.4/' .$id_post. '/tags', 'POST', array(
										'access_token' => $admin_access_token,
										'tag_uid'      => $fbUserID
									));
									$count=3;
								} catch (Exception $e)
								{
									// handle exception
									if ($e->getCode() == 210)
									{
										$count=3;
										Log::debug("User not taggable because he is invisible");
									}
									else
									{
										Log::error("Error during tagging, error: ".$e);
										$count++;
										if ($count == $maxTries) {
											break;
										}
									}

								}
							}

						}
						*/


						//Creare record nella pivot table tra photo e fbpage array("receiver_id"=>$u2->id, "status"=>1)
						$video->fbPages()->attach($page->id, array('fb_idpost'=>$id_post));


						try {

							$data = array("access_token" => $page_access_token);
							$responseVideo = $facebook->get("/".$id_post, $page_access_token);

							$graphNodeVideo = $responseVideo->getGraphNode();
							Log::debug("Facebook response for Video Data: ".print_r($graphNodeVideo,true));

							if ($page->custom_logo == '' || is_null($page->custom_logo))
							{
								$source_fb = "https://www.facebook.com/video/embed?video_id=".$graphNodeVideo['id'];
							}

							$video->fb_source_url = $source_fb;
							$video->save();

						}
						catch (Exception $ex)
						{
							Log::error($ex);
						}

					}

					$video->uploadOnFacebookDone = '1';
					$video->save();

					//Create link to share
					//self::replace_in_file($video, 1);

					if ( $facebookProfile && $video->user_connected_to_facebook ) {

						//Publish video to the timeline of the user
						try
						{
							if (isset($facebookProfile->user_fb_token))
							{
								Log::debug('Publishing video to the user wall');

								App::setlocale($contest->location->language);
								$descriptionOwnWall = $video->name;

								$videoToUpload = public_path()."/videos/".$video->filename;
								//upload photo to facebook page. Message: Tagga i tuoi amici, incrementa lo score e vinci!! #INSTAPULIA #CIMEDIAPP #PUGLIA.
								//Scarica l'APP InstApulia #LINK APP# e vinci fantastici premi Pugliesi!
								$data = array(
									"source" => $facebook->fileToUpload($videoToUpload),
									"title" => $video->name,
									"description" => $video->name);


								$responseUsrWall = $facebook->post("/me/videos", $data, $facebookProfile->user_fb_token);
								$graphNodeUsrWall = $responseUsrWall->getGraphNode();

								Log::debug("Facebook response for new Video Upload to user wall: ".print_r($graphNodeUsrWall,true));
								$id_videoUsr = $graphNodeUsrWall['id'];


								$video->fb_idpost_usr = $id_videoUsr;



								//App::setlocale($contest->location->language);
								//$descriptionOwnWall = Lang::get('messages.descriptionOwnWall', array('contestname' => $contest->name, 'GooglePlayLinkApp'=>$contest->company->googlePlay_linkApp, 'iTunesStoreLinkApp'=>$contest->company->iTunesStore_linkApp));
								//Fare replace sul testo "message"

								/*
								$todoReplacement = array("%contestname%", "%type%", "%companylink%", "%companyname%", "%hashtag%");
								$newWords = array($contest->name, $contest->contestType, "http://app.buzzmtbrand.it/" . $contest->contest_route, $contest->company_name, $contest->hashtag);
								$descriptionOwnWall = str_replace($todoReplacement, $newWords, $contest->objective['sentence_userpost']);
								*/

								/*
								$descriptionOwnWall = $video->name;

								Log::debug("Message for user post: ".$descriptionOwnWall);

								$filenameFull=$video->filename;
								$filenameParts=explode('.',$filenameFull);
								$fileExt=array_pop($filenameParts);
								$filename=implode('.',$filenameParts);


								$response3 = self::message2(array(
										  'message'     => $descriptionOwnWall,
										  'link'        => 'http://www.buzzmybrand.it/FacebookShare/'.$filename.'.html',
										  'picture'     => 'http://www.buzzmybrand.it/images/videos/'.$filename.'-facebook.jpg',
										  'name'        => $video->name,
										  'caption'     => $contest->name,
										  'description' => $contest->concept ), $facebookProfile->user_fb_token);

								Log::debug("Facebook response for response of publication to user wall: ".print_r($response3,true));
								*/
							}
							else
							{
								Log::warning('User access token not present! It is not possible to publish on behalf of user');
							}
						}
						catch (Exception $ex)
						{
							Log::error('Problem during video post publication with user access token: '.$ex);
						}

					}

					if ( $facebookProfile && $video->user_connected_to_facebook ) {

						//Facebook desktop notify
						try
						{
							//$acc_tokenResponse = $facebook->api("/oauth/access_token?client_id=".$contest->company->fbApp_clientID."&client_secret=".$contest->company->fbApp_clientSecret."&grant_type=client_credentials");
							//$app_access_token = $acc_tokenResponse["access_token"];
							App::setlocale($contest->location->language);
							$messageNotif = Lang::get('messages.video_approved', array('contestname' => $contest->name));

							$responseFBAPP = $facebook->post("/".$fbUserID."/notifications", array(
								"template" => $messageNotif,// and ask your friends to like, share, comment to increase your chances to win! The more your video is viral the more chances you have to win!
								"href" => "video/".$video->id,
							), $contest->company->app_accesstoken );

							Log::debug("Desktop notification done!");
						}
						catch (Exception $ex)
						{
							Log::error('Error during sending notification: '.$ex);
						}

					}


					$company_user = Company::find($contest->company_id)->belongsToManyUsers()->where('user_id', $user->id)->first();

					//Sending native notification
					if (isset($company_user->pivot->token) && $company_user->pivot->token !== '')
					{

						Log::debug("Sending native notification");

						if (strpos($company_user->pivot->token,'Android_') !== false)
						{

							App::setlocale($contest->location->language);
							$nativeNotifString = Lang::get('messages.video_approved_native', array('videoname' => $video->name, 'contestdescription'=> $contest->name));

							$message = PushNotification::Message($nativeNotifString,array(
								'title' => $contest->name,
								'url' => Config::get('app.url').'/video/'.$video->id));
							$app = '';
							$deviceToken = str_replace("Android_", "", $company_user->pivot->token);
							Log::debug("Device token to be used: ".$deviceToken);
							$app = $contest->company->name.'Android';

							PushNotification::app($app)
							->to($deviceToken)
							->send($message);
						}
						else if (strpos($company_user->pivot->token,'IOS_') !== false)
						{
							App::setlocale($contest->location->language);
							$nativeNotifString = Lang::get('messages.video_approved_native', array('videoname' => $video->name, 'contestdescription'=> $contest->name));
							$message = PushNotification::Message($nativeNotifString, array('custom' => array('url' => 'http://www.contestengine.net/video/'.$video->id)));
							$app = '';
							$deviceToken = str_replace("IOS_", "", $company_user->pivot->token);
							Log::debug("Device token to be used: ".$deviceToken);
							$app = $contest->company->name;

							PushNotification::app($app)
							->to($deviceToken)
							->send($message);
						}



					}


				}
				catch (Exception $ex)
				{
					Log::error($ex);
				}

				$video->save();

			}

			$photos=Photo::select("photos.*")
				->join("contests", "contests.id", "=", "photos.contest_id")
				->where("photos.approval_step_id", "=", 3)
				->where("photos.watermarkInsDone", "=", 1)
				->where("photos.uploadOnFacebookDone", "=", 0)
				->where("photos.contest_id", "=", $contest_id)
				->whereNull("photos.user_connected_to_instagram")
				->get();



			/*$roles = (string) User::find(1)->roles;*/
			Log::debug("Number of photos returned for upload on Facebook: ".count($photos));


			foreach ($photos as $photo)
			{

				try {

					//Generate link + thumbnails
					//$output = shell_exec("sh ".base_path()."/sh/GenerateFacebookShareHTMLForUploadedPhoto.sh ".$photo->filename);
					//Log::debug("Output GenerateFacebookShareHTMLForUploadedPhoto: ".$output);

					/*
					$output = shell_exec("sh ".base_path()."/sh/GeneratePhotoThumbnails.sh ".$photo->filename);
					Log::debug("Output GeneratePhotoThumbnails: ".$output);
					*/

					//here upload the photo on FACEBOOK page because the contest does not need approval of content

					Log::debug("Photo that correspond to the correct requisites for upload on FB: ".$photo->name);
					$user=User::find($photo->user_id);
					$contest=Contest::find($photo->contest_id);

					$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);

					//$facebook->setFileUploadSupport(true);
					$facebookProfile = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
					Log::debug("FacebookProfile user id :" . $user->id . "company id " . $contest->company_id);

					if ( $facebookProfile && $photo->user_connected_to_facebook ) {
						$fbUserID = $facebookProfile->id_fb;
					}

					$pages_array = $contest->fbPages;

					$source_fb = '';

					//Qui devo ciclare per tutte le pagine FB connesse al contest
					//Quindi devo costruirmi l'array per individuare le pagine FB connesse al contest
					foreach ($contest->fbPages as $page)
					{


						//vedere http://www.developed.be/2013/08/30/laravel-4-pivot-table-example-attach-and-detach/
						$admin_access_token = $page->fb_page_admin_at;
						$fb_page_id = $page->fb_page_id;
						$fb_page_link = $page->fb_page_link;

						//get PAGE ACCESS TOKEN
						$info = array("access_token" => $admin_access_token);
						$fb_request = $facebook->get('/me/accounts', $admin_access_token);

						Log::debug("Facebook response for getting access token: ".print_r($fb_request->getBody(),true));
						//scorrere l'array finche non trovi la pagina giusta
						//$pages_array = $fb_request['data'];
						$pages_array = $fb_request->getGraphEdge();
						$page_access_token = null;
						foreach ($pages_array as $pagefb)
						{
							if ($pagefb['id'] == $page->fb_page_id)
							{
								$page_access_token = $pagefb['access_token'];
								break;
							}
						}

						if (isset($page_access_token))
						{




							Log::debug("Facebook page access token: ".$page_access_token);

							//Fare replace sul testo "message"
							/*
							$todoReplacement = array("%contestname%", "%type%", "%companylink%", "%companyname%", "%hashtag%");
							$newWords = array($contest->name, $contest->contestType, "http://app.buzzmybrand.it/" . $contest->contest_route, $contest->company_name, $contest->hashtag);
							$message = str_replace($todoReplacement, $newWords, Lang::get('post.' . $contest->objective['sentence_post_key']));
							*/

							$message = utf8_decode($photo->name);

							Log::debug("Message for page post: ".$message);

							/*
							$fileToUpload = '';
							if (($contest->should_insert_contest_watermark) || ($contest->should_insert_company_watermark))
							{
								$photoToUpload = public_path()."/photos/".$photo->filename;
								$inputPhoto = explode('.', $photoToUpload);
								$fileToUpload = $inputPhoto[0]."_".$page->id.".".$inputPhoto[1];
							}
							else
							{
								$fileToUpload = public_path()."/photos/".$photo->filename;
							}
							*/
							$fileToUpload = public_path()."/photos/".$photo->filename;

							//upload photo to facebook page
							$data = array(
								"source" => $facebook->fileToUpload($fileToUpload),
								"message" => $message);

							if ($page->fb_no_story == "YES")
							{
								Log::debug("Contest with unpublished posts");
								$new_data = array("no_story" => "true");
								$data = array_merge($data, $new_data);

							}

							if (isset($page->fb_id_album) && (!empty($page->fb_id_album))) {
								$response = $facebook->post("/".$page->fb_id_album."/photos", $data, $page_access_token);
							} else {
								$album_details = array(
									'name' => $contest->name,
								);

								$create_album = $facebook->post('/'.$page->fb_page_id.'/albums', $album_details, $page_access_token);

								$graphNodealbum = $create_album->getGraphNode();

								$page->fb_id_album = $graphNodealbum['id'];
								$page->save();

								$response = $facebook->post("/".$page->fb_id_album."/photos", $data, $page_access_token);
							}

							$graphNode = $response->getGraphNode();

							Log::debug("Facebook response for new Photo Upload: ".print_r($graphNode,true));
							$id_photo = $graphNode['id'];
							$id_post_photo = null;
							if (array_key_exists("post_id", $graphNode))
							{
								$id_post_photo = $graphNode['post_id'];
							}
							else
							{
								$id_post_photo = $page->fb_page_id."_".$id_photo;
							}


							//Creare record nella pivot table tra photo e fbpage array("receiver_id"=>$u2->id, "status"=>1)
							$photo->fbPages()->attach($page->id, array('fb_idphoto'=>$id_photo, 'fb_idpost'=>$id_post_photo));


							try {

								$data = array("access_token" => $page_access_token);
								$responsePhoto = $facebook->get("/".$id_photo."?fields=images", $page_access_token);
								$graphNodePhoto = $responsePhoto->getGraphNode();
								Log::debug("Facebook response for Photo Data: ".print_r($graphNodePhoto,true));



								if ($page->custom_logo == '' || is_null($page->custom_logo))
								{
									//$source_fb = $responsePhoto['source'];
									$source_fb = $graphNodePhoto['images'][0]['source'];
								}

								$photo->fb_source_url = $source_fb;
								$photo->save();

							}
							catch (Exception $ex)
							{
								Log::error($ex);
							}






						}




					}


					$photo->uploadOnFacebookDone = '1';
					$photo->save();


					if ( $facebookProfile && $photo->user_connected_to_facebook ) {

						//Publish photo to the timeline of the user
						try
						{
							if (isset($facebookProfile->user_fb_token))
							{


								Log::debug('Publishing post to the user wall');

								/*
								$todoReplacement = array("%contestname%", "%type%", "%companylink%", "%companyname%", "%hashtag%");
								$newWords = array($contest->name, $contest->contestType, "http://app.buzzmybrand.it/" . $contest->contest_route, $contest->company_name, $contest->hashtag);
								$descriptionOwnWall = str_replace($todoReplacement, $newWords, Lang::get('post.' . $contest->objective['sentence_userpost_key']));
								*/

								$descriptionOwnWall = utf8_decode($photo->name);

								Log::debug("Message for user post: ".$descriptionOwnWall);

								$photoToUpload = public_path()."/photos/".$photo->filename;
								//upload photo to facebook page. Message: Tagga i tuoi amici, incrementa lo score e vinci!! #INSTAPULIA #CIMEDIAPP #PUGLIA.
								//Scarica l'APP InstApulia #LINK APP# e vinci fantastici premi Pugliesi!
								$data = array(
									"source" => $facebook->fileToUpload($photoToUpload),
									"message" => $descriptionOwnWall);


								$responseUsrWall = $facebook->post("/me/photos", $data, $facebookProfile->user_fb_token);
								$graphNodeUsrWall = $responseUsrWall->getGraphNode();

								Log::debug("Facebook response for new Photo Upload to user wall: ".print_r($graphNodeUsrWall,true));
								$id_photoUsr = $graphNodeUsrWall['id'];
								$id_post_photoUsr = null;
								if (array_key_exists("post_id", $graphNodeUsrWall))
								{
									$id_post_photoUsr = $graphNodeUsrWall['post_id'];
								}

								$photo->fb_idpost_usr = $id_photoUsr;





								/*
								$filenameFull=$photo->filename;
								$filenameParts=explode('.',$filenameFull);
								$fileExt=array_pop($filenameParts);
								$filename=implode('.',$filenameParts);


								$response3 = self::message2(array(
										  'message'     => 'This is my entry for the '.$contest->description.' Contest! Like, Share and Comment to increase my chance to win! Follow @'.$page->fb_page_link.' to know more! '.$contest->hashtags,
										  'link'        => 'http://www.joinmethere.tv/FacebookSharePhoto/'.$filename.'.html',
										  'picture'     => $picture_post,
										  'name'        => $photo->name,
										  'caption'     => $contest->description,
										  'description' => $contest->name ), $user->user_fb_token);

								Log::debug("Facebook response for response of publication to user wall: ".print_r($response3,true));
								*/

							}
							else
							{
								Log::warning('User access token not present! It is not possible to publish on behalf of user');
							}

						}
						catch (Exception $ex)
						{
							Log::error('Problem during photo post publication with user access token: '.$ex);
						}

					}





					//Create link to share
					//self::replace_in_file($photo, 2);

					if ( $facebookProfile && $photo->user_connected_to_facebook ) {

						//Facebook desktop notify
						try
						{

							App::setlocale($contest->location->language);
							$messageNotif = Lang::get('messages.photo_approved', array('contestname' => $contest->name));

							//$acc_tokenResponse = $facebook->api("/oauth/access_token?client_id=".$contest->company->fbApp_clientID."&client_secret=".$contest->company->fbApp_clientSecret."&grant_type=client_credentials");
							//$app_access_token = $acc_tokenResponse["access_token"];
							$responseFBAPP = $facebook->post("/".$fbUserID."/notifications", array(
								"template" => $messageNotif,// and ask your friends to like, share, comment to increase your chances to win! The more your video is viral the more chances you have to win!
								"href" => "photo/".$photo->id,
							), $contest->company->app_accesstoken );

							Log::debug("Desktop notification done!");
						}
						catch (Exception $ex)
						{
							Log::error('Error during sending notification: '.$ex);
						}

					}


					$company_user = Company::find($contest->company_id)->belongsToManyUsers()->where('user_id', $user->id)->first();

					//Sending native notification
					if (isset($company_user->pivot->token) && $company_user->pivot->token !== '')
					{

						Log::debug("Sending native notification");

						if (strpos($company_user->pivot->token,'Android_') !== false)
						{
							$message = PushNotification::Message('Your photo entitled "'.$photo->name.'"  for (contest_description) has been approved! Share it on Facebook to win!',array(
								'title' => $contest->name,
								'url' => Config::get('app.url').'/photo/'.$photo->id));
							$app = '';
							$deviceToken = str_replace("Android_", "", $company_user->pivot->token);
							Log::debug("Device token to be used: ".$deviceToken);
							$app = $contest->company->name.'Android';

							PushNotification::app($app)
							->to($deviceToken)
							->send($message);
						}
						else if (strpos($company_user->pivot->token,'IOS_') !== false)
						{
							$message = PushNotification::Message('Your photo entitled "'.$photo->name.'" for (contest_description) has been approved! Share it on Facebook to win!', array('custom' => array('url' => 'https://www.contestengine.net/photo/'.$photo->id)));
							$app = '';
							$deviceToken = str_replace("IOS_", "", $company_user->pivot->token);
							Log::debug("Device token to be used: ".$deviceToken);
							$app = $contest->company->name;
							/*
							if ($contest->company->name == 'THAIGOVIRAL')
							{
								$app = 'TGViOS';
							}
							else if ($contest->company->name == '2014 AFF SUZUKI CUP')
							{
								$app = 'jtmiOS';
							}
							*/
							PushNotification::app($app)
							->to($deviceToken)
							->send($message);
						}



					}


				}
				catch (Exception $ex)
				{
					Log::error($ex);
				}


				$photo->save();

			}



			//Da notare che per gli essay non è previsto inserimento watermark

			$essays=Essay::select("essays.*")
				->join("contests", "contests.id", "=", "essays.contest_id")
				->where("essays.approval_step_id", "=", 3)
				->where("essays.uploadOnFacebookDone", "=", 0)
				->where("essays.contest_id", "=", $contest_id)
				->get();


			/*$roles = (string) User::find(1)->roles;*/
			Log::debug("Number of essays returned for upload on Facebook: ".count($essays));


			foreach ($essays as $essay)
			{

				try {

					//Generate link + thumbnails
					//$output = shell_exec("sh ".base_path()."/sh/GenerateFacebookShareHTMLForUploadedPhoto.sh ".$photo->filename);
					//Log::debug("Output GenerateFacebookShareHTMLForUploadedPhoto: ".$output);

					/*
					$output = shell_exec("sh ".base_path()."/sh/GeneratePhotoThumbnails.sh ".$photo->filename);
					Log::debug("Output GeneratePhotoThumbnails: ".$output);
					*/

					//here upload the photo on FACEBOOK page because the contest does not need approval of content

					Log::debug("Essay that correspond to the correct requisites for upload on FB: ".$essay->name);
					$user=User::find($essay->user_id);
					$contest=Contest::find($essay->contest_id);

					$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);

					//$facebook->setFileUploadSupport(true);
					$facebookProfile = FacebookProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();
					Log::debug("FacebookProfile user id :" . $user->id . "company id " . $contest->company_id);

					if ( $facebookProfile && $essay->user_connected_to_facebook ) {
						$fbUserID = $facebookProfile->id_fb;
					}

					$pages_array = $contest->fbPages;

					$source_fb = '';

					//Qui devo ciclare per tutte le pagine FB connesse al contest
					//Quindi devo costruirmi l'array per individuare le pagine FB connesse al contest
					foreach ($contest->fbPages as $page)
					{


						//vedere http://www.developed.be/2013/08/30/laravel-4-pivot-table-example-attach-and-detach/
						$admin_access_token = $page->fb_page_admin_at;
						$fb_page_id = $page->fb_page_id;
						$fb_page_link = $page->fb_page_link;


						//get PAGE ACCESS TOKEN
						$info = array("access_token" => $admin_access_token);
						$fb_request = $facebook->get('/me/accounts', $admin_access_token);
						Log::debug("Facebook response for getting access token: ".print_r($fb_request->getBody(),true));
						//scorrere l'array finche non trovi la pagina giusta
						//$pages_array = $fb_request['data'];
						$pages_array = $fb_request->getGraphEdge();
						$page_access_token = null;
						foreach ($pages_array as $pagefb)
						{
							if ($pagefb['id'] == $page->fb_page_id)
							{
								$page_access_token = $pagefb['access_token'];
								break;
							}
						}

						Log::debug("Facebook page access token: ".$page_access_token);

						//Fare replace sul testo "message"
						/*
						$todoReplacement = array("%contestname%", "%type%", "%companylink%", "%companyname%", "%hashtag%");
						$newWords = array($contest->name, $contest->contestType, "http://app.buzzmybrand.it/" . $contest->contest_route, $contest->company_name, $contest->hashtag);
						$message = str_replace($todoReplacement, $newWords, Lang::get('post.' . $contest->objective['sentence_post_key']));
						*/


						$message = utf8_decode($essay->name);

						Log::debug("Message for page post: ".$message);

						/*
						$fileToUpload = '';
						if (($contest->should_insert_contest_watermark) || ($contest->should_insert_company_watermark))
						{
							$photoToUpload = public_path()."/photos/".$photo->filename;
							$inputPhoto = explode('.', $photoToUpload);
							$fileToUpload = $inputPhoto[0]."_".$page->id.".".$inputPhoto[1];
						}
						else
						{
							$fileToUpload = public_path()."/photos/".$photo->filename;
						}
						*/
						$fileToUpload = public_path()."/essays/".$essay->image;

						//upload essay to facebook page
						$data = array(
							"source" => $facebook->fileToUpload($fileToUpload),
							"message" => $message);

						if ($page->fb_no_story == "YES")
						{
							Log::debug("Contest with unpublished posts");
							$new_data = array("no_story" => "true");
							$data = array_merge($data, $new_data);

						}

						if (isset($page->fb_id_album)) {
							$response = $facebook->post("/".$page->fb_id_album."/photos", $data, $page_access_token);
						} else {
							$album_details = array(
								'name' => $contest->name,
							);

							$create_album = $facebook->post('/'.$page->fb_page_id.'/albums', $album_details, $page_access_token);

							$graphNodealbum = $create_album->getGraphNode();

							$page->fb_id_album = $graphNodealbum['id'];
							$page->save();

							$response = $facebook->post("/".$page->fb_id_album."/photos", $data, $page_access_token);
						}

						$graphNode = $response->getGraphNode();

						Log::debug("Facebook response for new Essay Upload: ".print_r($graphNode,true));
						$id_essay = $graphNode['id'];
						$id_post_essay = null;
						if (array_key_exists("post_id", $graphNode))
						{
							$id_post_essay = $graphNode['post_id'];
						}
						else
						{
							$id_post_essay = $page->fb_page_id."_".$id_essay;
						}

						//Creare record nella pivot table tra photo e fbpage array("receiver_id"=>$u2->id, "status"=>1)
						$essay->fbPages()->attach($page->id, array('fb_idessay'=>$id_essay, 'fb_idpost'=>$id_post_essay));


						try {

							$data = array("access_token" => $page_access_token);
							$responseEssay = $facebook->get("/".$id_essay."?fields=images", $page_access_token);

							$graphNodeEssay = $responseEssay->getGraphNode();
							Log::debug("Facebook response for Essay Data: ".print_r($graphNodeEssay,true));
							//$picture_post = $responseEssay['picture'];

							if ($page->custom_logo == '' || is_null($page->custom_logo))
							{
								//$source_fb = $responseEssay['source'];
								$source_fb = $graphNodeEssay['images'][0]['source'];
							}

							$essay->fb_source_url = $source_fb;
							$essay->save();
						}
						catch (Exception $ex)
						{
							Log::error($ex);
						}


					}

					$essay->uploadOnFacebookDone = '1';
					$essay->save();



					if ( $facebookProfile && $essay->user_connected_to_facebook ) {

						//Publish essay to the timeline of the user
						try
						{
							if (isset($facebookProfile->user_fb_token))
							{


								Log::debug('Publishing post to the user wall');

								/*
								$todoReplacement = array("%contestname%", "%type%", "%companylink%", "%companyname%", "%hashtag%");
								$newWords = array($contest->name, $contest->contestType, "http://app.buzzmybrand.it/" . $contest->contest_route, $contest->company_name, $contest->hashtag);
								$descriptionOwnWall = str_replace($todoReplacement, $newWords, Lang::get('post.' . $contest->objective['sentence_userpost_key']));
								*/
								$descriptionOwnWall = utf8_decode($essay->sentence);

								Log::debug("Message for user post: ".$descriptionOwnWall);

								$essayToUpload = public_path()."/essays/".$essay->image;
								//upload photo to facebook page. Message: Tagga i tuoi amici, incrementa lo score e vinci!! #INSTAPULIA #CIMEDIAPP #PUGLIA.
								//Scarica l'APP InstApulia #LINK APP# e vinci fantastici premi Pugliesi!
								$data = array(
									"source" => $facebook->fileToUpload($essayToUpload),
									"message" => $descriptionOwnWall);

								$responseUsrWall = $facebook->post("/me/photos", $data, $facebookProfile->user_fb_token);
								$graphNodeUsrWall = $responseUsrWall->getGraphNode();

								Log::debug("Facebook response for new Essay Upload to user wall: ".print_r($graphNodeUsrWall,true));
								$id_essayUsr = $graphNodeUsrWall['id'];
								$id_post_essayUsr = null;
								if (array_key_exists("post_id", $graphNodeUsrWall))
								{
									$id_post_essayUsr = $graphNodeUsrWall['post_id'];
								}

								$essay->fb_idpost_usr = $id_essayUsr;





								/*
								$filenameFull=$photo->filename;
								$filenameParts=explode('.',$filenameFull);
								$fileExt=array_pop($filenameParts);
								$filename=implode('.',$filenameParts);


								$response3 = self::message2(array(
										  'message'     => 'This is my entry for the '.$contest->description.' Contest! Like, Share and Comment to increase my chance to win! Follow @'.$page->fb_page_link.' to know more! '.$contest->hashtags,
										  'link'        => 'http://www.joinmethere.tv/FacebookSharePhoto/'.$filename.'.html',
										  'picture'     => $picture_post,
										  'name'        => $photo->name,
										  'caption'     => $contest->description,
										  'description' => $contest->name ), $user->user_fb_token);

								Log::debug("Facebook response for response of publication to user wall: ".print_r($response3,true));
								*/

							}
							else
							{
								Log::warning('User access token not present! It is not possible to publish on behalf of user');
							}

						}
						catch (Exception $ex)
						{
							Log::error('Problem during essay post publication with user access token: '.$ex);
						}

					}





					//Create link to share
					//self::replace_in_file($photo, 2);

					if ( $facebookProfile && $essay->user_connected_to_facebook ) {

						//Facebook desktop notify
						try
						{

							App::setlocale($contest->location->language);
							$messageNotif = Lang::get('messages.essay_approved', array('contestname' => $contest->name));

							//$acc_tokenResponse = $facebook->api("/oauth/access_token?client_id=".$contest->company->fbApp_clientID."&client_secret=".$contest->company->fbApp_clientSecret."&grant_type=client_credentials");
							//$app_access_token = $acc_tokenResponse["access_token"];
							$responseFBAPP = $facebook->post("/".$fbUserID."/notifications", array(
								"template" => $messageNotif,// and ask your friends to like, share, comment to increase your chances to win! The more your video is viral the more chances you have to win!
								"href" => "essay/".$essay->id,
							), $contest->company->app_accesstoken );
							Log::debug("Desktop notification done!");
						}
						catch (Exception $ex)
						{
							Log::error('Error during sending notification: '.$ex);
						}

					}


					$company_user = Company::find($contest->company_id)->belongsToManyUsers()->where('user_id', $user->id)->first();

					//Sending native notification
					if (isset($company_user->pivot->token) && $company_user->pivot->token !== '')
					{

						Log::debug("Sending native notification");

						if (strpos($company_user->pivot->token,'Android_') !== false)
						{
							$message = PushNotification::Message('Your essay entitled "'.$essay->name.'"  for (contest_description) has been approved! Share it on Facebook to win!',array(
								'title' => $contest->name,
								'url' => Config::get('app.url').'/photo/'.$essay->id));
							$app = '';
							$deviceToken = str_replace("Android_", "", $company_user->pivot->token);
							Log::debug("Device token to be used: ".$deviceToken);
							$app = $contest->company->name.'Android';

							PushNotification::app($app)
							->to($deviceToken)
							->send($message);
						}
						else if (strpos($company_user->pivot->token,'IOS_') !== false)
						{
							$message = PushNotification::Message('Your essay entitled "'.$essay->name.'" for (contest_description) has been approved! Share it on Facebook to win!', array('custom' => array('url' => 'https://www.contestengine.net/essay/'.$essay->id)));
							$app = '';
							$deviceToken = str_replace("IOS_", "", $company_user->pivot->token);
							Log::debug("Device token to be used: ".$deviceToken);
							$app = $contest->company->name;
							/*
							if ($contest->company->name == 'THAIGOVIRAL')
							{
								$app = 'TGViOS';
							}
							else if ($contest->company->name == '2014 AFF SUZUKI CUP')
							{
								$app = 'jtmiOS';
							}
							*/
							PushNotification::app($app)
							->to($deviceToken)
							->send($message);
						}



					}


				}
				catch (Exception $ex)
				{
					Log::error($ex);
				}

				$essay->save();


			}



		}
		catch (Exception $ex)
		{
			Log::error($ex);
		}
	}


	public static function message2($data, $page_token)
    {
        // need token
        $data['access_token'] = $page_token;


        // init
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v2.4/me/feed");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // execute and close
        $return = curl_exec($ch);
		Log::debug("Facebook message2 JSON: ".$return);
        curl_close($ch);

        // end
        return $return;
    }


	public static function testRun($seconds)
	{
		Log::debug("testRun, sleep ".$seconds." seconds");
		$lock = Lock::find(1);
		Log::debug("Acquired version: ".$lock->version);
		$lock->version++;
		$lock->save();
		Log::debug("Saved version: ".$lock->version);
		sleep($seconds);
		Log::debug("Finished sleeping ".$seconds." seconds");
	}

	public static function UpdateRanking($id)
	{
		Log::debug("UpdateRanking()");

		try
		{
			$videos=Video::select("videos.*")
			->where("videos.contest_id", "=", $id)
			->where("videos.approval_step_id", "=", 3)
			/*->where("videos.uploadOnFacebookDone", "!=", 0)*/
			->orderBy('score', 'desc')->orderBy('created_at', 'desc')->get(); //from the highest score to the lower score

			$position = 0;
			foreach ($videos as $video)
			{
				$position++;
				$video->rank_position = $position;
				$video->save();
			}

			$photos=Photo::select("photos.*")
			->where("photos.contest_id", "=", $id)
			->where("photos.approval_step_id", "=", 3)
			/*->where("photos.uploadOnFacebookDone", "!=", 0)*/
			->orderBy('score', 'desc')->orderBy('created_at', 'desc')->get(); //from the highest score to the lower score

			$position = 0;
			foreach ($photos as $photo)
			{
				$position++;
				$photo->rank_position = $position;
				$photo->save();
			}

			$essays=Essay::select("essays.*")
			->where("essays.contest_id", "=", $id)
			->where("essays.approval_step_id", "=", 3)
			/*->where("essays.uploadOnFacebookDone", "!=", 0)*/
			->orderBy('score', 'desc')->orderBy('created_at', 'desc')->get(); //from the highest score to the lower score

			$position = 0;
			foreach ($essays as $essay)
			{
				$position++;
				$essay->rank_position = $position;
				$essay->save();
			}

			Log::debug("Finish UpdateRanking()");
		}
		catch (Exception $ex)
		{

		}
	}


	public static function likesfromLogin()
	{
		Log::debug("Start likesfromLogin()");
		$contest=Contest::find('18');
		$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);

		$users=User::select("users.*")
			->where("users.verified", "=", 1)
			->where("users.user_fb_token", "!=", "")->get();

		$numlikePage = 0;
		foreach ($users as $user)
		{
			try
			{

				$page_id = $contest->fb_page_id;
				//$facebook->setAccessToken($user->user_fb_token);
				$likes = $facebook->get("/me/likes/".$page_id, $user->user_fb_token);
				//TODO DA RIVEDERE IL RESPONSE
				if( !empty($likes['data']) )
					$numlikePage = $numlikePage + 1;
			}
			catch (Exception $e)
			{
				Log::error("Error during Graph API page like for user fb id: ".$user->id_fb." with following error: ".$e);
			}
		}

		Log::debug("Like number from login: ".$numlikePage);
	}



	public static function UpdateScorenew($contestID)
	{


		Log::debug("UpdateScore()");

		$contest=Contest::find($contestID);

		$facebook = new Facebook\Facebook([
						'app_id' => $contest->company->fbApp_clientID,
						'app_secret' => $contest->company->fbApp_clientSecret,
						'default_graph_version' => 'v2.4',
					]);

		try
		{
			//videos
			$videos=Video::where("videos.approval_step_id", "=", 3)
			->where(function($query)
            {
                $query->where("videos.uploadOnFacebookDone", "!=", 0)
                      ->orwhere("videos.uploadOnTwitterDone", "!=", 0)
					  ->orwhere("videos.uploadOnOwnYouTubeDone", "!=", 0);
            })
			->where("videos.contest_id", $contestID)->get();


			Log::debug("Number of videos returned for the contest with id ".$contestID." : ".count($videos));
			foreach($videos as $video)
			{
				$likePages = array();






				$jmtvScoreFacebook = 0;
				$total_count_link = 0;


				try
				{

					if ( $video->uploadOnFacebookDone ) {

						//individuare tutte le pagine Facebook connesse al contest
						foreach ($contest->fbPages as $page)
						{

							$page_token = $page->fb_page_admin_at;
							//$facebook->setAccessToken($page_token);
							$page_id = $page->fb_page_id;


							//Devo trovare il post del video relativo alla pagina che sto analizzando
							//Ho sul piatto l'ID della fb page e l'ID del video
							//$video->fbPages()->where('fb_page_video.fb_page_id','=',$page_id)->get();
							//oppure
							$videosOfPage = $page->videos()->where('fb_page_video.video_id', '=', $video->id)->get();

							foreach ($videosOfPage as $video)
							{




								$unique_impressions = 0;
								$unique_consumptions = 0;
								$unique_likes = 0;
								$unique_comments = 0;
								$unique_shares = 0;
								$unique_visualization = 0;
								$post_impressions = 0;
								$post_engaged_users = 0;
								$post_video_complete_views_organic = 0;


								Log::debug("Analyzing video with id_fb_post: ".$video->pivot->fb_idpost . " and name " . $video->name);

								if ($contest->unique_impression_value > 0) {


									try
									{

										//reach
										$unique_impressions_post = $facebook->get('/'.$page_id.'_'.$video->pivot->fb_idpost.'/insights/post_impressions_unique/lifetime', $page_token);
										$data = json_decode($unique_impressions_post->getBody(), true);
										Log::debug("Response for unique impressions video post: ".print_r($data,true));

										if (!is_null($data['data']))
										{
											$unique_impressions = $data['data'][0]['values'][0]['value'];
											Log::debug("Reach: ".print_r($unique_impressions,true));
										}




									}
									catch (Exception $ex)
									{
										Log::error($ex);
									}


								}


								if ($contest->click_post > 0) 	{

									try
									{

										//post_consumptions_unique -- numero click unici sul post
										$post_consumptions_unique = $facebook->get('/'.$page_id.'_'.$video->pivot->fb_idpost.'/insights/post_consumptions_unique/lifetime', $page_token);
										$data = json_decode($post_consumptions_unique->getBody(), true);
										Log::debug("Response for unique consumptions video post: ".print_r($data,true));

										if (!is_null($data['data']))
										{
											$unique_consumptions = $data['data'][0]['values'][0]['value'];
											Log::debug("Post consumption_unique: ".print_r($unique_consumptions,true));
										}

									}
									catch (Exception $ex)
									{
										Log::error($ex);
									}

								}




								try
								{

									$likesResponse = $facebook->get('/'.$page_id.'_'.$video->pivot->fb_idpost.'/likes?summary=true', $page_token);
									$data = json_decode($likesResponse->getBody(), true);
									Log::debug("Response for likes response video post: ".print_r($data,true));
									if (array_key_exists("summary", $data)) {
										$unique_likes = $data['summary']['total_count'];
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}

								try
								{

									$commentsResponse = $facebook->get('/'.$page_id.'_'.$video->pivot->fb_idpost.'/comments?summary=true&filter=stream', $page_token);
									$data = json_decode($commentsResponse->getBody(), true);
									Log::debug("Response for comments response video post: ".print_r($data,true));
									if (array_key_exists("summary", $data)) {
										$unique_comments = $data['summary']['total_count'];
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}



								try
								{

									$sharesResponse = $facebook->get('/'.$page_id.'_'.$video->pivot->fb_idpost.'/?fields=shares', $page_token);
									$data = json_decode($sharesResponse->getBody(), true);
									Log::debug("Response for shares response video post: ".print_r($data,true));
									if (array_key_exists("shares", $data)) {
										$unique_shares = $data['shares']['count'];
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}

								if ($contest->unique_visualization_video > 0) {

									try
									{

										//VISUALIZZAZIONI VIDEO UNICHE
										$unique_visualization_video_post = $facebook->get('/'.$page_id.'_'.$video->pivot->fb_idpost.'/insights/post_video_complete_views_organic_unique/lifetime', $page_token);
										$data = json_decode($unique_visualization_video_post->getBody(), true);
										Log::debug("Response for unique visualizations post: ".print_r($data,true));
										if (!empty($data['data'])) {
												$unique_visualization = $data['data'][0]['values'][0]['value'];
											}


									}
									catch (Exception $ex)
									{
										Log::error($ex);
									}

								}





									//post_impressions_viral_unique

									//post_impressions_fan_unique

									//

								try
								{

									// impressions
									$response = $facebook->get('/'.$page_id.'_'.$video->pivot->fb_idpost.'/insights/post_impressions/lifetime', $page_token);
									$data = json_decode($response->getBody(), true);
									Log::debug("Response for post impressions: ".print_r($data,true));
									if (!empty($data['data'])) {
										$post_impressions = $data['data'][0]['values'][0]['value'];
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}


								try
								{

									// engaged_users
									$response = $facebook->get('/'.$page_id.'_'.$video->pivot->fb_idpost.'/insights/post_engaged_users/lifetime', $page_token);
									$data = json_decode($response->getBody(), true);
									Log::debug("Response for engaged users: ".print_r($data,true));
									if (!empty($data['data'])) {
										$post_engaged_users = $data['data'][0]['values'][0]['value'];
									}


								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}

								try
								{

									//VISUALIZZAZIONI VIDEO
									$response = $facebook->get('/'.$page_id.'_'.$video->pivot->fb_idpost.'/insights/post_video_complete_views_organic/lifetime', $page_token);
									$data = json_decode($response->getBody(), true);
									Log::debug("Response for video complete views: ".print_r($data,true));
									if (!empty($data['data'])) {
										$post_video_complete_views_organic = $data['data'][0]['values'][0]['value'];
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}


								$video->fbPages()->updateExistingPivot($page->id, array('fb_idpost'=>$video->pivot->fb_idpost,
													'reach'=>$unique_impressions,
													'consumptions'=>$unique_consumptions,
													'unique_likes'=>$unique_likes,
													'unique_comments'=>$unique_comments,
													'unique_shares'=>$unique_shares,
													'unique_views'=>$unique_visualization,
													'impressions'=>$post_impressions,
													'engaged_users'=>$post_engaged_users,
													'views'=>$post_video_complete_views_organic,), false);





								$jmtvScoreFacebook=$jmtvScoreFacebook +
									$unique_likes* $contest->like_value +
									$unique_comments* $contest->comment_value +
									$unique_shares* $contest->share_value +
									$unique_consumptions* $contest->click_post +
									$unique_impressions* $contest->unique_impression_value +
									$unique_visualization* $contest->unique_visualization_video;



							}


							//total count for Facebook
							try {


								$filenameFull=$video->filename;
								$filenameParts=explode('.',$filenameFull);
								$fileExt=array_pop($filenameParts);
								$filename=implode('.',$filenameParts);

								$resultjSon = self::totalCountFromLink("https://www.buzzmybrand.com/FacebookShare/".$filename.".html", $facebook, $page_token);
								//$scores = json_decode($resultjSon);

								Log::debug("Response for totalCountFromLink (video): ".print_r($resultjSon, true));

								$obj = (Array)$resultjSon['share'];

								$total_count_link = $obj['comment_count'] + $obj['share_count'];

							}
							catch (Exception $ex)
							{
								Log::error($ex);
							}

							//$jmtvScoreFacebook=$jmtvScoreFacebook + $total_count_link;

							$video->total_count_link = $total_count_link;


						}



					}



					$facebookProfile = FacebookProfile::where('user_id', $video->user->id)->where('company_id', $contest->company_id)->first();

					if ($contest->score_from_userwall && $facebookProfile && $video->user_connected_to_facebook && (!empty($video->fb_idpost_usr)) ) {

						$likes_total = 0;
						$comments_total = 0;

						try
						{


							//$facebook->setAccessToken($facebookProfile->user_fb_token);

							//count likes
							$likes_post = $facebook->get('/'.$video->fb_idpost_usr.'/likes?summary=true', $facebookProfile->user_fb_token);
							$data = json_decode($likes_post->getBody(), true);
							Log::debug("Response for likes response video post: ".print_r($data,true));
							$likes_total = $data['summary']['total_count'];
							Log::debug("Number of total likes returned: ".$likes_total);

						}
						catch (Exception $e)
						{
							Log::error("Error during Graph API - likes calculation - to the post of the user with id: ".$video->id." with following error: ".$e);
						}

						try
						{
							//$facebook->setAccessToken($facebookProfile->user_fb_token);
							$comments_post = $facebook->get('/'.$video->fb_idpost_usr.'/comments?filter=stream&summary=true', $facebookProfile->user_fb_token);
							$data = json_decode($comments_post->getBody(), true);
							Log::debug("Response for unique comments post: ".print_r($data,true));
							$comments_array = $data['data'];
							$id_users_comments = array();
							$i = 0;
							foreach ($comments_array as $comment)
							{
								$id_users_comments[$i] = $comment['from']['id'];
								$i++;
							}
							$unique_comm_users = array();
							$unique_comm_users = array_count_values($id_users_comments);
							$comments_total = count($unique_comm_users);
							//Log::debug("Unique comments users: ".print_r($unique_comm_users,true));
							Log::debug("Number of unique comments returned: ". $comments_total);

						}
						catch (Exception $e)
						{
							Log::error("Error during Graph API - comments calculation - to the post of the user with id: ".$video->id." with following error: ".$e);
						}


						//se il facebook user token è scaduto
						if ($likes_total == 0 && $video->likes_count_post > 0)
						{
							$likes_total = $video->likes_count_post;
						}
						else
						{
							$video->likes_count_post = $likes_total;
						}

						$jmtvScoreFacebook = $jmtvScoreFacebook + $likes_total * ($contest->like_value);

						//se il facebook user token è scaduto
						if ($comments_total == 0 && $video->comments_count_post > 0)
						{
							$comments_total = $video->comments_count_post;
						}
						else
						{
							$video->comments_count_post = $comments_total;
						}

						$jmtvScoreFacebook = $jmtvScoreFacebook + $comments_total * ($contest->comment_value);

					}




					//calcolo score da Twitter
					if ($video->uploadOnTwitterDone == 1)
					{

						try
						{
							foreach ($contest->twPages as $page)
							{

								$page_token = $page->tw_page_admin_at;
								$page_secret_token = $page->tw_page_secret_token;
								$page_id = $page->tw_page_id;

								$videosOfPage = $page->videos()->where('tw_page_video.video_id', '=', $video->id)->get();

								foreach ($videosOfPage as $video)
								{


									$retweets_count = 0;
									$favourite_count = 0;

									Log::debug("Analyzing video with tw_idvideo: ".$video->pivot->tw_idvideo." and name " . $video->name);

									try
									{


										//Devo trovare il post del video relativo alla pagina che sto analizzando
										//Ho sul piatto l'ID della fb page e l'ID del video

										$parameters = array(
											'include_my_retweet ' => 'true',
											'include_entities' => 'false'
										);

										$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $page->tw_page_admin_at, $page->tw_page_secret_token);
										$tweet_data = $twitteroauth->get('https://api.twitter.com/1.1/statuses/show/'.$video->pivot->tw_idvideo.'.json', $parameters);

										Log::debug('Result from twitter api post: '.print_r($tweet_data, true));

										$retweets_count = $tweet_data->retweet_count;
										$favourite_count = $tweet_data->favorite_count;




									}
									catch (Exception $ex)
									{
										Log::error("Error during Twitter API processing for video post id: ".$video->pivot->tw_idvideo." with following error: ".$ex);
									}

									$video->twPages()->updateExistingPivot($page->id, array('tw_idvideo'=>$video->pivot->tw_idvideo,
											'retweets_count'=>$retweets_count,
											'favourite_count'=>$favourite_count), false);

									$jmtvScoreFacebook=$jmtvScoreFacebook +
									$retweets_count* $contest->share_value +
									$favourite_count* $contest->like_value;

									//stabilire se l'utente segue il profilo twitter
									$likePage = false;

									$twitterProfile = TwitterProfile::where('user_id', $video->user->id)->where('company_id', $contest->company_id)->first();

									if ( $twitterProfile && $video->user_connected_to_twitter ) {

										try
										{



											$likePage = false;

											$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $page->tw_page_admin_at, $page->tw_page_secret_token);
											$followers_list = $twitteroauth->get('https://api.twitter.com/1.1/followers/ids.json?stringify_ids=true');

											Log::debug('Followers list: '.print_r($followers_list, true));

											foreach ($followers_list->ids as $follower)
											{
												if ($follower == $twitterProfile->tw_id)
												{
													$likePage = true;
													break;
												}
											}
										}
										catch (Exception $e)
										{
											Log::error("Error during Twitter API page following for video post id: ".$video->id." with following error: ".$e);
										}

									}


									if ($likePage == true)
									{
										$jmtvScoreFacebook = $jmtvScoreFacebook + (int)($page->tw_pagefollow_value);
									}
								}



							}
						}
						catch (Exception $e)
						{
							Log::error("Error during Twitter API to the post of the user with id: ".$video->id." with following error: ".$e);
						}

					}


					//calcolo score da YouTube

					if ($video->uploadOnCompanyYouTubeDone == 1)
					{

						try
						{
							foreach ($contest->ytPages as $page)
							{

								//$page_token = $page->yt_access_token;
								//$page_secret_token = $page->yt_refresh_token;
								//$page_id = $page->tw_page_id;

								$videosOfPage = $page->videos()->where('yt_page_video.video_id', '=', $video->id)->get();

								foreach ($videosOfPage as $video)
								{

									$youtube_view_count = 0;
									$youtube_like_count = 0;

									Log::debug("Analyzing video with yt_idvideo: ".$video->pivot->yt_idvideo." and name " . $video->name);

									try
									{


										//Devo trovare il post del video relativo alla pagina che sto analizzando
										//Ho sul piatto l'ID della fb page e l'ID del video


										$s = YouTube::getStatistics($page, $video->pivot->yt_idvideo);
										$youtube_view_count = $s['viewCount'];
										$youtube_like_count = $s['likeCount'];

										//$youtube_view_count = json_decode(file_get_contents('http://gdata.youtube.com/feeds/api/videos/'.$video->pivot->yt_idvideo.'?v=2&alt=json'))->entry->{'yt$statistics'}->viewCount;
										//$youtube_like_count = json_decode(file_get_contents('http://gdata.youtube.com/feeds/api/videos/'.$video->pivot->yt_idvideo.'?v=2&alt=json'))->entry->{'yt$rating'}->numLikes;




									}
									catch (Exception $ex)
									{
										Log::error("Error during YouTube API processing for video post id: ".$video->pivot->yt_idvideo." with following error: ".$ex);
									}

									$video->ytPages()->updateExistingPivot($page->id, array('yt_idvideo'=>$video->pivot->yt_idvideo,
													'yt_view_count'=>$youtube_view_count,
													'yt_like_count'=>$youtube_like_count), false);

									/*
									$jmtvScoreFacebook=$jmtvScoreFacebook +
									$youtube_view_count* $contest->views_youtube +
									$youtube_like_count* $contest->like_value;
									*/

								}



							}
						}
						catch (Exception $e)
						{
							Log::error("Error during YouTube API to the post of the user with id: ".$video->id." with following error: ".$e);
						}

					}






					//total count for Twitter
					//conteggiare retweets e favourites dal profilo twitter dell'utente

					if ($video->uploadOnTwitterDone == 1)
					{

						$twitterProfile = TwitterProfile::where('user_id', $video->user->id)->where('company_id', $contest->company_id)->first();

						if ( $twitterProfile && $video->user_connected_to_twitter)
						{

							$retweets_count = 0;
							$favourite_count = 0;

							try {


								$parameters = array(
									'include_my_retweet ' => 'true',
									'include_entities' => 'false'
								);
								$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $twitterProfile->oauth_token, $twitterProfile->oauth_token_secret);
								$tweet_data = $twitteroauth->get('https://api.twitter.com/1.1/statuses/show/'.$video->tw_idpost_usr.'.json', $parameters);

								Log::debug('Twitter API response: '.print_r($tweet_data, true));

								$retweets_count = $tweet_data->retweet_count;
								$favourite_count = $tweet_data->favorite_count;

							}
							catch (Exception $ex)
							{
								Log::error($ex);
							}


							$video->retweets_count_post = $retweets_count;
							$video->favourite_count_post = $favourite_count;

							$jmtvScoreFacebook=$jmtvScoreFacebook +
									$retweets_count* $contest->share_value +
									$favourite_count* $contest->like_value;
						}

					}




				}
				catch (Exception $e)
				{
					Log::error("Error during Social API requests video id: ".$video->id." with following error: ".$e);
				}

				$video->score=$jmtvScoreFacebook;
				$video->save();

			}








			//photos
			$photos=Photo::where("photos.approval_step_id", "=", 3)
			->where(function($query)
            {
                $query->where("photos.uploadOnFacebookDone", "!=", 0)
                      ->orwhere("photos.uploadOnTwitterDone", "!=", 0);
            })
			->where("photos.contest_id", $contestID)->get();

			Log::debug("Number of photos returned for the contest with id ".$contestID." : ".count($photos));
			foreach($photos as $photo)
			{
				$likePages = array();


				$jmtvScoreFacebook = 0;
				$total_count_link = 0;

				try
				{

					if ( $photo->uploadOnFacebookDone ) {

						//individuare tutte le pagine Facebook connesse al contest
						foreach ($contest->fbPages as $page)
						{

							$page_token = $page->fb_page_admin_at;
							//$facebook->setAccessToken($page_token);
							$page_id = $page->fb_page_id;


							//Devo trovare il post della foto relativo alla pagina che sto analizzando
							//Ho sul piatto l'ID della fb page e l'ID della foto
							//$photo->fbPages()->where('fb_page_photo.fb_page_id','=',$page_id)->get();
							//oppure
							$photosOfPage = $page->photos()->where('fb_page_photo.photo_id', '=', $photo->id)->get();
							foreach ($photosOfPage as $photo)
							{
								//Log::debug("Analyzing photo with id_fb_post: ".$photo->pivot->fb_idpost);

								$unique_impressions = 0;
								$unique_consumptions = 0;
								$unique_likes = 0;
								$unique_comments = 0;
								$unique_shares = 0;
								$post_impressions = 0;
								$post_engaged_users = 0;


								Log::debug("Analyzing photo with id_fb_post: ".$photo->pivot->fb_idpost);
								try
								{

									// reach
									$unique_impressions_post = $facebook->get('/'.$photo->pivot->fb_idpost.'/insights/post_impressions_unique/lifetime', $page_token);
									$data = json_decode($unique_impressions_post->getBody(), true);
									Log::debug("Response for unique impressions photo post: ".print_r($data,true));

									if (count($data['data']) != 0)
									{
										$unique_impressions = $data['data'][0]['values'][0]['value'];
										Log::debug("Reach: ".print_r($unique_impressions,true));
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}

								try
								{

									// impressions
									$response = $facebook->get('/'.$photo->pivot->fb_idpost.'/insights/post_impressions/lifetime', $page_token);
									$data = json_decode($response->getBody(), true);
									Log::debug("Response for impressions photo post: ".print_r($data,true));
									if (count($data['data']) != 0) {
										$post_impressions = $data['data'][0]['values'][0]['value'];
										Log::debug("Impressions: ".print_r($post_impressions,true));
									}


								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}

								try
								{

									//post_consumptions_unique -- numero click unici sul post
									$post_consumptions_unique = $facebook->get('/'.$photo->pivot->fb_idpost.'/insights/post_consumptions_unique/lifetime', $page_token);
									$data = json_decode($post_consumptions_unique->getBody(), true);
									Log::debug("Response for post_consumptions_unique photo post: ".print_r($data,true));

									if (!is_null($data['data']))
									{
										$unique_consumptions = $data['data'][0]['values'][0]['value'];
										Log::debug("Post consumption_unique: ".print_r($unique_consumptions,true));
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}

								try
								{

									// engaged_users
									$response = $facebook->get('/'.$photo->pivot->fb_idpost.'/insights/post_engaged_users/lifetime', $page_token);
									$data = json_decode($response->getBody(), true);
									Log::debug("Response for photo engaged users: ".print_r($data,true));
									if (count($data['data']) != 0) {
										$post_engaged_users = $data['data'][0]['values'][0]['value'];
										Log::debug("Post engaged user: ".print_r($post_engaged_users,true));
									}


								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}

								/*
								try
								{


									//post_story_adds_by_action_type_unique
									$unique_actions_post = $facebook->api('/v2.4/'.$photo->pivot->fb_idpost.'/insights/post_story_adds_by_action_type_unique/lifetime');
									Log::debug("Response for unique actions photo post: ".print_r($unique_actions_post,true));

									if (!empty($unique_actions_post['data']))
									{

										if (array_key_exists("like", $unique_actions_post['data'][0]['values'][0]['value'])) {
											$unique_likes = $unique_actions_post['data'][0]['values'][0]['value']['like'];
										}

										if (array_key_exists("comment", $unique_actions_post['data'][0]['values'][0]['value'])) {
											$unique_comments = $unique_actions_post['data'][0]['values'][0]['value']['comment'];
										}


										if (array_key_exists("share", $unique_actions_post['data'][0]['values'][0]['value'])) {
											$unique_shares = $unique_actions_post['data'][0]['values'][0]['value']['share'];
										}




									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}
								*/

								try
								{
									//likes
									$likesResponse = $facebook->get('/'.$photo->pivot->fb_idpost.'/likes?summary=true', $page_token);
									$data = json_decode($likesResponse->getBody(), true);
									Log::debug("Response for likes response photo post: ".print_r($data,true));
									if (array_key_exists("summary", $data)) {
										$unique_likes = $data['summary']['total_count'];
										Log::debug("Unique likes: ".print_r($unique_likes,true));
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}

								try
								{
									//comments
									$commentsResponse = $facebook->get('/'.$photo->pivot->fb_idpost.'/comments?summary=true&filter=stream', $page_token);
									$data = json_decode($commentsResponse->getBody(), true);
									Log::debug("Response for comments response array photo post: ".print_r($data,true));
									if (array_key_exists("summary", $data)) {
										$unique_comments = $data['summary']['total_count'];
										Log::debug("Total comments: ".print_r($unique_comments,true));
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}

								try
								{

									//shares
									$sharesResponse = $facebook->get('/'.$photo->pivot->fb_idpost.'/?fields=shares', $page_token);
									$data = json_decode($sharesResponse->getBody(), true);
									Log::debug("Response for shares response photo post: ".print_r($data,true));
									if (array_key_exists("shares", $data)) {
										$unique_shares = $data['shares']['count'];
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}



								$photo->fbPages()->updateExistingPivot($page->id, array('fb_idpost'=>$photo->pivot->fb_idpost,
													'reach'=>$unique_impressions,
													'consumptions'=>$unique_consumptions,
													'unique_likes'=>$unique_likes,
													'unique_comments'=>$unique_comments,
													'unique_shares'=>$unique_shares,
													'impressions'=>$post_impressions,
													'engaged_users'=>$post_engaged_users,), false);



								$jmtvScoreFacebook=$jmtvScoreFacebook +
									$unique_likes* $contest->like_value +
									$unique_comments* $contest->comment_value +
									$unique_shares* $contest->share_value +
									$unique_consumptions* $contest->click_post +
									$unique_impressions* $contest->unique_impression_value;




								/*
								$likePage = false;

								$facebookProfile = FacebookProfile::where('user_id', $photo->user->id)->where('company_id', $contest->company_id)->first();

								if ( $facebookProfile && $photo->user_connected_to_facebook ) {

									try
									{
										$facebook->setAccessToken($facebookProfile->user_fb_token);
										$likes = $facebook->api("/v2.4/me/likes/".$page_id);
										if( !empty($likes['data']) )
											$likePage = true;
										else
											$likePage = false;
									}
									catch (Exception $e)
									{
										Log::error("Error during Graph API page like for photo post id: ".$photo->id." with following error: ".$e);
									}

								}

								if ($likePage == true)
								{
									$jmtvScoreFacebook = $jmtvScoreFacebook + (int)($page->fb_pagelike_value);
								}
								*/
							}


							try {


								$filenameFull=$photo->filename;
								$filenameParts=explode('.',$filenameFull);
								$fileExt=array_pop($filenameParts);
								$filename=implode('.',$filenameParts);


								$resultjSon = self::totalCountFromLink("https://www.buzzmybrand.com/FacebookSharePhoto/".$filename.".html", $facebook, $page_token);
								//$scores = json_decode($resultjSon);

								Log::debug("Response for totalCountFromLink: ".print_r($resultjSon, true));

								//$jmtvScoreFacebook=($scores[0]['like_count'])*1+$unique_likes*2+$unique_comments*4+$unique_shares*3+$unique_impressions*1;
								//$jmtvScoreFacebook=($scores[0]['like_count'])*1+$unique_likes*2+$unique_comments*4+$unique_shares*3;
								$obj = (Array)$resultjSon['share'];

								$total_count_link = $obj['comment_count'] + $obj['share_count'];

							}
							catch (Exception $ex)
							{
								Log::error($ex);
							}

							$jmtvScoreFacebook=$jmtvScoreFacebook + $total_count_link * $contest->like_value;

							$photo->total_count_link = $total_count_link;

						}

						//calcolo punteggio da instagram
						if (!is_null($contest->instagram))
						{
							$likes_total = $photo->likes_count_post;
							$jmtvScoreFacebook = $jmtvScoreFacebook + $likes_total * ($contest->like_value);
							$comments_total = $photo->comments_count_post;
							$jmtvScoreFacebook = $jmtvScoreFacebook + $comments_total * ($contest->comment_value);
						}




						$facebookProfile = FacebookProfile::where('user_id', $photo->user->id)->where('company_id', $contest->company_id)->first();

						if ($contest->score_from_userwall && $facebookProfile && $photo->user_connected_to_facebook && (!empty($photo->fb_idpost_usr)) ) {

							$likes_total = 0;
							$comments_total = 0;

							try
							{

								/*
								//calcolare il numero di tag sulla photo pubblicata nel wall dell'utente
								$facebook->setAccessToken($facebookProfile->user_fb_token);
								$tagsPhoto = $facebook->api("/v2.4/".$photo->fb_idpost_usr."/tags");
								//contare l'array del response per capire quanti tag ci sono nella foto
								$tagScore = count($tagsPhoto)*($contest->tagfriend_value);
								$jmtvScoreFacebook = $jmtvScoreFacebook + $tagScore;
								*/

								//$facebook->setAccessToken($facebookProfile->user_fb_token);

								//count likes
								$likes_post = $facebook->get('/'.$photo->fb_idpost_usr.'/likes?summary=true', $facebookProfile->user_fb_token);
								$data = json_decode($likes_post->getBody(), true);
								Log::debug("Response for likes response photo post: ".print_r($data,true));
								$likes_total = $data['summary']['total_count'];
								Log::debug("Number of total likes returned: ".$likes_total);


							}
							catch (Exception $e)
							{
								Log::error("Error during Graph API - likes calculation - to the post of the user with id: ".$photo->id." with following error: ".$e);
							}

							try
							{
								//$facebook->setAccessToken($facebookProfile->user_fb_token);
								$comments_post = $facebook->get('/'.$photo->fb_idpost_usr.'/comments?filter=stream&summary=true', $facebookProfile->user_fb_token);

								$data = json_decode($comments_post->getBody(), true);
								Log::debug("Response for comment response photo post: ".print_r($data,true));

								$comments_array = $data['data'];
								$id_users_comments = array();
								$i = 0;
								foreach ($comments_array as $comment)
								{
									$id_users_comments[$i] = $comment['from']['id'];
									$i++;
								}
								$unique_comm_users = array();
								$unique_comm_users = array_count_values($id_users_comments);
								$comments_total = count($unique_comm_users);
								//Log::debug("Unique comments users: ".print_r($unique_comm_users,true));
								Log::debug("Number of unique comments returned: ". $comments_total);

							}
							catch (Exception $e)
							{
								Log::error("Error during Graph API - comments calculation - to the post of the user with id: ".$photo->id." with following error: ".$e);
							}


							//se il facebook user token è scaduto
							if ($likes_total == 0 && $photo->likes_count_post > 0)
							{
								$likes_total = $photo->likes_count_post;
							}
							else
							{
								$photo->likes_count_post = $likes_total;
							}



							$jmtvScoreFacebook = $jmtvScoreFacebook + $likes_total * ($contest->like_value);

							//se il facebook user token è scaduto
							if ($comments_total == 0 && $photo->comments_count_post > 0)
							{
								$comments_total = $photo->comments_count_post;
							}
							else
							{
								$photo->comments_count_post = $comments_total;
							}

							$jmtvScoreFacebook = $jmtvScoreFacebook + $comments_total * ($contest->comment_value);

						}

						$photo->score=$jmtvScoreFacebook;
						$photo->save();






					}



					if ( $photo->uploadOnTwitterDone ) {

						//calcolo score da Twitter
						try
						{
							foreach ($contest->twPages as $page)
							{

								$page_token = $page->tw_page_admin_at;
								$page_secret_token = $page->tw_page_secret_token;
								$page_id = $page->tw_page_id;

								$photosOfPage = $page->photos()->where('tw_page_photo.photo_id', '=', $photo->id)->get();

								foreach ($photosOfPage as $photo)
								{

									$retweets_count = 0;
									$favourite_count = 0;

									Log::debug("Analyzing photo with tw_idphoto: " . $photo->pivot->tw_idphoto." and name " . $photo->name);

									try
									{

										//Devo trovare il post della foto relativa alla pagina che sto analizzando
										//Ho sul piatto l'ID della tw page e l'ID della photo

										$parameters = array(
											'include_my_retweet ' => 'true',
											'include_entities' => 'false'
										);

										$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $page->tw_page_admin_at, $page->tw_page_secret_token);
										$tweet_data = $twitteroauth->get('https://api.twitter.com/1.1/statuses/show/'.$photo->pivot->tw_idphoto.'.json', $parameters);

										Log::debug('Result from twitter api post: '.print_r($tweet_data, true));

										$retweets_count = $tweet_data->retweet_count;
										$favourite_count = $tweet_data->favorite_count;




									}
									catch (Exception $ex)
									{
										Log::error("Error during Twitter API processing for video post id: ".$photo->pivot->tw_idphoto." with following error: ".$ex);
									}

									$photo->twPages()->updateExistingPivot($page->id, array('tw_idphoto'=>$photo->pivot->tw_idphoto,
										'retweets_count'=>$retweets_count,
										'favourite_count'=>$favourite_count), false);

									$jmtvScoreFacebook=$jmtvScoreFacebook +
									$retweets_count* $contest->share_value +
									$favourite_count* $contest->like_value;

									//stabilire se l'utente segue il profilo twitter
									$likePage = false;

									$twitterProfile = TwitterProfile::where('user_id', $photo->user->id)->where('company_id', $contest->company_id)->first();

									if ( $twitterProfile && $photo->user_connected_to_twitter ) {

										try
										{

											$likePage = false;

											$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $page->tw_page_admin_at, $page->tw_page_secret_token);
											$followers_list = $twitteroauth->get('https://api.twitter.com/1.1/followers/ids.json?stringify_ids=true');

											Log::debug('Followers list: '.print_r($followers_list, true));

											foreach ($followers_list->ids as $follower)
											{
												if ($follower == $twitterProfile->tw_id)
												{
													$likePage = true;
													break;
												}
											}
										}
										catch (Exception $e)
										{
											Log::error("Error during Twitter API page following for foto post id: ".$photo->id." with following error: ".$e);
										}

									}

									if ($likePage == true)
									{
										$jmtvScoreFacebook = $jmtvScoreFacebook + (int)($page->tw_pagefollow_value);
									}

								}



							}
						}
						catch (Exception $e)
						{
							Log::error("Error during Twitter API to the post of the user with id: ".$photo->id." with following error: ".$e);
						}


						//total count for Twitter
						//conteggiare retweets e favourites dal profilo twitter dell'utente

						$twitterProfile = TwitterProfile::where('user_id', $photo->user->id)->where('company_id', $contest->company_id)->first();

						if ( $twitterProfile && $photo->user_connected_to_twitter)
						{

							$retweets_count	= 0;
							$favourite_count = 0;

							try
							{

								$parameters = array(
									'include_my_retweet ' => 'true',
									'include_entities' => 'false'
								);
								$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $twitterProfile->oauth_token, $twitterProfile->oauth_token_secret);
								$tweet_data = $twitteroauth->get('https://api.twitter.com/1.1/statuses/show/'.$photo->tw_idpost_usr.'.json', $parameters);

								Log::debug('Twitter API response: '.print_r($tweet_data, true));

								$retweets_count = $tweet_data->retweet_count;
								$favourite_count = $tweet_data->favorite_count;



							}
							catch (Exception $e)
							{
								Log::error($e);
							}

							$photo->retweets_count_post = $retweets_count;
							$photo->favourite_count_post = $favourite_count;

							$jmtvScoreFacebook=$jmtvScoreFacebook +
									$retweets_count* $contest->share_value +
									$favourite_count* $contest->like_value;


						}


					}




				}
				catch (Exception $e)
				{
					Log::error("Error during Graph API requests photo id: ".$photo->id." with following error: ".$e);
				}

				$photo->score=$jmtvScoreFacebook;
				$photo->save();


			}



			//essays
			$essays=Essay::where("essays.approval_step_id", "=", 3)
			->where(function($query)
            {
                $query->where("essays.uploadOnFacebookDone", "!=", 0)
                      ->orwhere("essays.uploadOnTwitterDone", "!=", 0);
            })
			->where("essays.contest_id", $contestID)->get();


			Log::debug("Number of essays returned for the contest with id ".$contestID." : ".count($essays));
			foreach($essays as $essay)
			{
				$likePages = array();


				$jmtvScoreFacebook = 0;
				$total_count_link = 0;


				try
				{

					if ($essay->uploadOnFacebookDone == 1)
					{

						//individuare tutte le pagine Facebook connesse al contest
						foreach ($contest->fbPages as $page)
						{

							$page_token = $page->fb_page_admin_at;
							//$facebook->setAccessToken($page_token);
							$page_id = $page->fb_page_id;

							$essaysOfPage = $page->essays()->where('fb_page_essay.essay_id', '=', $essay->id)->get();

							foreach ($essaysOfPage as $essay)
							{

								$unique_impressions = 0;
								$unique_consumptions = 0;
								$unique_likes = 0;
								$unique_comments = 0;
								$unique_shares = 0;
								$post_impressions = 0;
								$post_engaged_users = 0;


								Log::debug("Analyzing essay with id_fb_post: ".$essay->pivot->fb_idpost);

								try
								{

									// impressions
									$unique_impressions_post = $facebook->get('/'.$essay->pivot->fb_idpost.'/insights/post_impressions_unique/lifetime', $page_token);
									$data = json_decode($unique_impressions_post->getBody(), true);
									Log::debug("Response for unique impressions essay post: ".print_r($data,true));
									if (!empty($data['data']))
									{
										$unique_impressions = $data['data'][0]['values'][0]['value'];
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}

								try
								{

									//post_consumptions_unique -- numero click unici sul post
									$post_consumptions_unique = $facebook->get('/'.$essay->pivot->fb_idpost.'/insights/post_consumptions_unique/lifetime', $page_token);
									$data = json_decode($post_consumptions_unique->getBody(), true);
									Log::debug("Response for unique consumptions essay post: ".print_r($data,true));
									if (!empty($data['data']))
									{
										$unique_consumptions = $data['data'][0]['values'][0]['value'];
									}


								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}



								try
								{

									$likesResponse = $facebook->get('/'.$essay->pivot->fb_idpost.'/likes?summary=true', $page_token);
									$data = json_decode($likesResponse->getBody(), true);
									Log::debug("Response for likes response essay post: ".print_r($data,true));
									if (array_key_exists("summary", $data)) {
										$unique_likes = $data['summary']['total_count'];
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}


								try
								{

									$commentsResponse = $facebook->get('/'.$essay->pivot->fb_idpost.'/comments?summary=true&filter=stream', $page_token);
									$data = json_decode($commentsResponse->getBody(), true);
									Log::debug("Response for comments response essay post: ".print_r($data,true));
									if (array_key_exists("summary", $data)) {
										$unique_comments = $data['summary']['total_count'];
									}


								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}

								try
								{

									$sharesResponse = $facebook->get('/'.$essay->pivot->fb_idpost.'/?fields=shares', $page_token);
									$data = json_decode($sharesResponse->getBody(), true);
									Log::debug("Response for shares response essay post: ".print_r($data,true));
									if (array_key_exists("shares", $data)) {
										$unique_shares = $data['shares']['count'];
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}


								try
								{

									// impressions
									$response = $facebook->get('/'.$essay->pivot->fb_idpost.'/insights/post_impressions/lifetime', $page_token);
									$data = json_decode($response->getBody(), true);
									Log::debug("Response for essay post impressions: ".print_r($data,true));
									if (!empty($data['data'])) {
										$post_impressions = $data['data'][0]['values'][0]['value'];
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}

								try
								{

									// engaged_users
									$response = $facebook->get('/'.$essay->pivot->fb_idpost.'/insights/post_engaged_users/lifetime', $page_token);
									$data = json_decode($response->getBody(), true);
									Log::debug("Response for essay engaged users: ".print_r($data,true));
									if (!empty($data['data'])) {
										$post_engaged_users = $data['data'][0]['values'][0]['value'];
									}

								}
								catch (Exception $ex)
								{
									Log::error($ex);
								}

								//da aggiungere unique_consumption
								$essay->fbPages()->updateExistingPivot($page->id, array('fb_idpost'=>$essay->pivot->fb_idpost,
													'reach'=>$unique_impressions,
													'consumptions'=>$unique_consumptions,
													'unique_likes'=>$unique_likes,
													'unique_comments'=>$unique_comments,
													'unique_shares'=>$unique_shares,
													'impressions'=>$post_impressions,
													'engaged_users'=>$post_engaged_users,), false);


								$jmtvScoreFacebook =
									$unique_likes* $contest->like_value +
									$unique_comments* $contest->comment_value +
									$unique_shares* $contest->share_value +
									$unique_consumptions* $contest->click_post +
									$unique_impressions* $contest->unique_impression_value;


							}


							try {


								$filenameFull=$essay->image;
								$filenameParts=explode('.',$filenameFull);
								$fileExt=array_pop($filenameParts);
								$filename=implode('.',$filenameParts);

								$resultjSon = self::totalCountFromLink("https://www.buzzmybrand.com/FacebookShareEssay/".$filename.".html", $facebook, $page_token);
								//$scores = json_decode($resultjSon);

								Log::debug("Response for totalCountFromLink: ".print_r($resultjSon, true));

								//$jmtvScoreFacebook=($scores[0]['like_count'])*1+$unique_likes*2+$unique_comments*4+$unique_shares*3+$unique_impressions*1;
								//$jmtvScoreFacebook=($scores[0]['like_count'])*1+$unique_likes*2+$unique_comments*4+$unique_shares*3;
								$obj = (Array)$resultjSon['share'];

								$total_count_link = $obj['comment_count'] + $obj['share_count'];

							}
							catch (Exception $ex)
							{
								Log::error($ex);
							}

							$jmtvScoreFacebook=$jmtvScoreFacebook + $total_count_link * $contest->like_value;

							$essay->total_count_link = $total_count_link;



						}


						$facebookProfile = FacebookProfile::where('user_id', $essay->user->id)->where('company_id', $contest->company_id)->first();

						if ($contest->score_from_userwall && $facebookProfile && $essay->user_connected_to_facebook && (!empty($photo->fb_idpost_usr)) ) {

							$likes_total = 0;
							$comments_total = 0;

							try
							{
								//count likes
								$likes_post = $facebook->get('/'.$essay->fb_idpost_usr.'/likes?summary=true', $facebookProfile->user_fb_token);
								$data = json_decode($likes_post->getBody(), true);
								Log::debug("Response for likes response essay post: ".print_r($data,true));
								$likes_total = $data['summary']['total_count'];
								Log::debug("Number of total likes returned: ".$likes_total);



							}
							catch (Exception $e)
							{
								Log::error("Error during Graph API to the post of the user with id: ".$essay->id." with following error: ".$e);
							}


							try
							{

								$comments_post = $facebook->get('/'.$essay->fb_idpost_usr.'/comments?filter=stream&summary=true', $facebookProfile->user_fb_token);
								$data = json_decode($comments_post->getBody(), true);
								Log::debug("Response for unique comments post: ".print_r($data,true));
								$comments_array = $data['data'];
								$id_users_comments = array();
								$i = 0;
								foreach ($comments_array as $comment)
								{
									$id_users_comments[$i] = $comment['from']['id'];
									$i++;
								}
								$unique_comm_users = array();
								$unique_comm_users = array_count_values($id_users_comments);
								$comments_total = count($unique_comm_users);
								//Log::debug("Unique comments users: ".print_r($unique_comm_users,true));
								Log::debug("Number of unique comments returned: ".$comments_total);


							}
							catch (Exception $e)
							{
								Log::error("Error during Graph API to the post of the user with id: ".$essay->id." with following error: ".$e);
							}


							//se il facebook user token è scaduto
							if ($likes_total == 0 && $essay->likes_count_post > 0)
							{
								$likes_total = $essay->likes_count_post;
							}
							else
							{
								$essay->likes_count_post = $likes_total;
							}

							$jmtvScoreFacebook = $jmtvScoreFacebook + $likes_total * ($contest->like_value);

							//se il facebook user token è scaduto
							if ($comments_total == 0 && $essay->comments_count_post > 0)
							{
								$comments_total = $essay->comments_count_post;
							}
							else
							{
								$essay->comments_count_post = $comments_total;
							}

							$jmtvScoreFacebook = $jmtvScoreFacebook + $comments_total * ($contest->comment_value);

						}

						$essay->save();


					}


					//calcolo score da Twitter

					if ( $essay->uploadOnTwitterDone ) {

						try
						{
							foreach ($contest->twPages as $page)
							{

								$page_token = $page->tw_page_admin_at;
								$page_secret_token = $page->tw_page_secret_token;
								$page_id = $page->tw_page_id;

								$essaysOfPage = $page->essays()->where('tw_page_essay.essay_id', '=', $essay->id)->get();

								foreach ($essaysOfPage as $essay)
								{

									$retweets_count = 0;
									$favourite_count = 0;

									Log::debug("Analyzing essay with tw_idessay: " . $essay->pivot->tw_idessay." and name " . $essay->name);

									try
									{


										$parameters = array(
											'include_my_retweet ' => 'true',
											'include_entities' => 'false'
										);

										$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $page->tw_page_admin_at, $page->tw_page_secret_token);
										$tweet_data = $twitteroauth->get('https://api.twitter.com/1.1/statuses/show/'.$essay->pivot->tw_idessay.'.json', $parameters);

										Log::debug('Result from twitter api post: '.print_r($tweet_data, true));

										$retweets_count = $tweet_data->retweet_count;
										$favourite_count = $tweet_data->favorite_count;




									}
									catch (Exception $ex)
									{
										Log::error("Error during Twitter API processing for essay post id: ".$essay->pivot->tw_idessay." with following error: ".$ex);
									}

									$essay->twPages()->updateExistingPivot($page->id, array('tw_idessay'=>$essay->pivot->tw_idessay,
											'retweets_count'=>$retweets_count,
											'favourite_count'=>$favourite_count), false);

									$jmtvScoreFacebook=$jmtvScoreFacebook +
									$retweets_count* $contest->share_value +
									$favourite_count* $contest->like_value;

									//stabilire se l'utente segue il profilo twitter
									$likePage = false;

									$twitterProfile = TwitterProfile::where('user_id', $essay->user->id)->where('company_id', $contest->company_id)->first();

									if ( $twitterProfile && $essay->user_connected_to_twitter ) {

										try
										{

											$likePage = false;

											$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $page->tw_page_admin_at, $page->tw_page_secret_token);
											$followers_list = $twitteroauth->get('https://api.twitter.com/1.1/followers/ids.json?stringify_ids=true');

											Log::debug('Followers list: '.print_r($followers_list, true));

											foreach ($followers_list->ids as $follower)
											{
												if ($follower == $twitterProfile->tw_id)
												{
													$likePage = true;
													break;
												}
											}
										}
										catch (Exception $e)
										{
											Log::error("Error during Twitter API page following for essay post id: ".$essay->id." with following error: ".$e);
										}

									}

									if ($likePage == true)
									{
										$jmtvScoreFacebook = $jmtvScoreFacebook + (int)($page->tw_pagefollow_value);
									}
								}



							}


						}
						catch (Exception $e)
						{
							Log::error("Error during Twitter API to the post of the user with id: ".$photo->id." with following error: ".$e);
						}

						//total count for Twitter
						//conteggiare retweets e favourites dal profilo twitter dell'utente

						$twitterProfile = TwitterProfile::where('user_id', $essay->user->id)->where('company_id', $contest->company_id)->first();

						if ( $twitterProfile && $essay->user_connected_to_twitter)
						{

							$retweets_count	= 0;
							$favourite_count = 0;

							try
							{

								$parameters = array(
									'include_my_retweet ' => 'true',
									'include_entities' => 'false'
								);
								$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $twitterProfile->oauth_token, $twitterProfile->oauth_token_secret);
								$tweet_data = $twitteroauth->get('https://api.twitter.com/1.1/statuses/show/'.$essay->tw_idpost_usr.'.json', $parameters);

								Log::debug('Twitter API response: '.print_r($tweet_data, true));

								$retweets_count = $tweet_data->retweet_count;
								$favourite_count = $tweet_data->favorite_count;

							}
							catch (Exception $e)
							{
								Log::error($e);
							}

							$essay->retweets_count_post = $retweets_count;
							$essay->favourite_count_post = $favourite_count;

							$jmtvScoreFacebook=$jmtvScoreFacebook +
									$retweets_count* $contest->share_value +
									$favourite_count* $contest->like_value;
						}


					}



				}
				catch (Exception $e)
				{
					Log::error("Error during Graph API requests photo id: ".$essay->id." with following error: ".$e);
				}

				$essay->score=$jmtvScoreFacebook;
				$essay->save();



			}

			//CHECK DA QUI

			try {

				self::dailyNewLikesANDPageEngaedUsers($contest, $facebook);

			} catch (Exception $ex) {

				Log::error($ex);

			}


			try {

				$followers_list_count = 0;

				foreach ($contest->twPages as $page) {

					$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $page->tw_page_admin_at, $page->tw_page_secret_token);
					$followers_list = $twitteroauth->get('https://api.twitter.com/1.1/followers/ids.json?stringify_ids=true');

					Log::debug("Response for follower list count " . print_r($followers_list, true));
					$followers_list_count = count($followers_list->ids);
					$follower_count_1 = 0;
					$follower_count_2 = 0;

					if ( $page->pivot->follower_count_1 !== 0 ) {
						$follower_count_2 = $followers_list_count;
					} else {
						$follower_count_1 = $followers_list_count;
					}

					$contest->twPages()->updateExistingPivot($page->id, array('follower_count_1' => $follower_count_1, 'follower_count_2' => $follower_count_2), false);

				}
				/*
				if ( $contest->follower_count_1 !== null ) {
					$contest->follower_count_2 = $followers_list_count;
				} else {
					$contest->follower_count_1 = $followers_list_count;
				}*/

				$contest->save();

			} catch (Exception $ex) {

				Log::error($ex);

			}

			Log::debug("subscribers_list_count start()");
			try {

				$subscribers_list_count = 0;

				foreach ($contest->ytPages as $page) {

					$subscribers_list_count = YouTube::getSubscriptions($page);
					$subscriptions_count_1 = 0;
					$subscriptions_count_2 = 0;

					if ( $page->pivot->subscriptions_count_1 !== 0 ) {
						$subscriptions_count_2 = $subscribers_list_count;
					} else {
						$subscriptions_count_1 = $subscribers_list_count;
					}

					$contest->ytPages()->updateExistingPivot($page->id, array('subscriptions_count_1' => $subscriptions_count_1, 'subscriptions_count_2' => $subscriptions_count_2), false);

				}

				//Log::debug("subscribers_list_count " . $subscribers_list_count);
				/*
				if ( $contest->subscribers_list_count1 !== null ) {
					$contest->subscribers_list_count2 = $subscribers_list_count;
				} else {
					$contest->subscribers_list_count1 = $subscribers_list_count;
				}
				*/

				$contest->save();

			} catch (Exception $ex) {

				Log::error($ex);

			}
			Log::debug("subscribers_list_count end()");


			Log::debug("Finish UpdateScore()");
		}
		catch (Exception $ex)
		{
			Log::error($ex);
		}




	}

	public static function totalCountFromLink($url, $facebook, $page_token) { // echo $accessToken; exit;
        /*
		$curlUrl = 'https://api.facebook.com/method/links.getStats?urls='.$url.'&format=json';

        $ch = curl_init($curlUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
		Log::debug("Facebook totalCount from Link JSON: ".$output);
        curl_close($ch);


        return $output;
		*/


		$data_array = array('id' => $url);

		$fb_request = $facebook->get('/?id=' . $url, $page_token);
		$data = json_decode($fb_request->getBody(), true);
		Log::debug("Response for totalCountLink: ".print_r($data,true));

		return $data;
    }

	//photoname senza estensione
	public static function replace_in_file($item, $type)
	{
		if ( $type == 3 ) {
			$itemname = explode('.', $item->image);
		} else {
			$itemname = explode('.', $item->filename);
		}

		$sharingFile = '';
		$targetCopy = '';
		$oldTextsArray = '';
		if ($type == 1)
		{
			$sharingFile = public_path()."/FacebookShare/FacebookSharing.html";
			$targetCopy = public_path()."/FacebookShare/".$itemname[0].".html";
			$oldTextsArray = array("_title_", "_description_", "_VideoFileName_", "_appId_");
		}
		else if ( $type == 2 )
		{
			$sharingFile = public_path()."/FacebookSharePhoto/FacebookSharing.html";
			$targetCopy = public_path()."/FacebookSharePhoto/".$itemname[0].".html";
			$oldTextsArray = array("_title_", "_description_", "_PhotoFileName_", "_appId_");
		}
		else if ( $type == 3 )
		{
			$sharingFile = public_path()."/FacebookShareEssay/FacebookSharing.html";
			$targetCopy = public_path()."/FacebookShareEssay/".$itemname[0].".html";
			$oldTextsArray = array("_title_", "_description_", "_EssayFileName_", "_appId_");
		}

		if (!file_exists($targetCopy))
		{



			copy($sharingFile, $targetCopy);
			if (file_exists($targetCopy))



			{






				try





				{
					//ottenere da DB titolo, descrizione, urlfb, photoname (già ce l'abbiamo)
					$title = $item->contest->name;
					$description = $item->contest->concept;
					$appId = $item->contest->company->fbApp_clientID;
					//$urlFB = $photo->fb_source_url;

					//$oldTextsArray = array("_title_", "_description_", "_PhotoFileName_", "_appId_");
					$newTextsArray = array($title, $description, $itemname[0], $appId);
					$arrlength = count($newTextsArray);

					for($x = 0; $x < $arrlength; $x++)




					{


						$FileContent = file_get_contents($targetCopy);
						$FileContent = str_replace($oldTextsArray[$x], $newTextsArray[$x], $FileContent);
						if(file_put_contents($targetCopy, $FileContent) > 0)
						{
							Log::warning("Write file with success");
						}
						else
						{
						   //$Result["message"] = 'Error while writing file';
						   Log::warning("Error while writing file");
						}

					}







				}
				catch(Exception $e)
				{
					Log::error($e);
				}


			} else


			{
				//echo "Failure: $targetCopy does not exist";
				Log::error("Failure: $targetCopy does not exist");

			}






		}




	}




	public static function dailyNewLikesANDPageEngaedUsers($contest, $facebook) {

		Log::debug("dailyNewLikesANDPageEngaedUsers() start for contest ".$contest->name);

		//$tz_object = new DateTimeZone($contest->location->timezoneDesc);

		$start_date = DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time);
		$end_date = DateTime::createFromFormat('Y-m-d H:i:s', $contest->end_time);



		$datetime = new DateTime();
		$now = $datetime;

		if ( $now <= $end_date ) {
			$end_date = $now;
		} else {
			$end_date->add(DateInterval::createFromDateString('1 days'));
		}



		$contestPages = DB::table('fbpage_contest')
				->leftJoin('fb_pages', 'fbpage_contest.fbpage_id', '=', 'fb_pages.id')
				->select(DB::raw('fbpage_contest.id AS id, fbpage_contest.contest_id AS contest_id, fb_pages.id as fb_p_id ,fb_pages.fb_page_id AS fb_page_id, fb_pages.fb_page_admin_at as fb_page_admin_at'))
				->where("fbpage_contest.contest_id", "=", $contest->id)
				->get();

		Log::debug("pages to process: " . count($contestPages));

		$counter = 0;
		$impressions = 0;
		$reach = 0;
		foreach ($contestPages as $p) {
			//NEW LIKES
			//$facebook->setAccessToken($p->fb_page_admin_at);
			Log::debug("calling: " . '/v2.4/'.$p->fb_page_id.'/insights/page_fan_adds_unique/day?since=' . $start_date->format('Y-m-d') . '&until=' .$end_date->format('Y-m-d'));
			$page_fan_adds_unique = $facebook->get('/'.$p->fb_page_id.'/insights/page_fan_adds_unique/day?since=' . $start_date->format('Y-m-d') . '&until=' .$end_date->format('Y-m-d'), $p->fb_page_admin_at);
			$data = json_decode($page_fan_adds_unique->getBody(), true);
			Log::debug("Response for page_fan_adds_unique: ".print_r($data,true));

			if (!empty($data['data'])) {

				foreach ($data['data'][0]['values'] as $lpd) {
					$counter += $lpd['value'];
				}

			}

			//IMPRESSIONS
			Log::debug("calling: " . '/v2.4/'.$p->fb_page_id.'/insights/page_impressions/day?since=' . $start_date->format('Y-m-d') . '&until=' .$end_date->format('Y-m-d'));
			$page_impressions = $facebook->get('/'.$p->fb_page_id.'/insights/page_impressions/day?since=' . $start_date->format('Y-m-d') . '&until=' .$end_date->format('Y-m-d'), $p->fb_page_admin_at);
			$data = json_decode($page_impressions->getBody(), true);
			Log::debug("Response for page_impressions: ".print_r($data,true));
			if (!empty($data['data'])) {

				foreach ($data['data'][0]['values'] as $lpd) {
					$impressions += $lpd['value'];
				}

			}

			//REACH
			Log::debug("calling: " . '/v2.4/'.$p->fb_page_id.'/insights/page_impressions_unique/day?since=' . $start_date->format('Y-m-d') . '&until=' .$end_date->format('Y-m-d'));
			$page_impressions_unique = $facebook->get('/'.$p->fb_page_id.'/insights/page_impressions_unique/day?since=' . $start_date->format('Y-m-d') . '&until=' .$end_date->format('Y-m-d'), $p->fb_page_admin_at);
			$data = json_decode($page_impressions_unique->getBody(), true);
			Log::debug("Response for page_impressions_unique: ".print_r($data,true));
			if (!empty($data['data'])) {

				foreach ($data['data'][0]['values'] as $lpd) {
					$reach += $lpd['value'];
				}

			}

			Log::debug("Page ID: ".$p->fb_p_id." . Impressions value: ".$impressions." . Reach value: ".$reach);

			//bisogna memorizzare impression e reach values
			$contest->fbPages()->updateExistingPivot($p->fb_p_id, array('impressions' => $impressions, 'reach' => $reach, 'new_likes' => $counter), false);




		}

		/*
		DB::table('contests')
		->where('id', $contest->id)
		->update(array('daily_new_likes' => $counter));
		*/


		$counter2 = 0;
		$counter3 = 0;
		foreach ($contestPages as $p) {

			//$facebook->setAccessToken($p->fb_page_admin_at);
			Log::debug("calling: " . '/v2.4/'.$p->fb_page_id.'/insights/page_engaged_users/day?since=' . $start_date->format('Y-m-d') . '&until=' .$end_date->format('Y-m-d'));
			$page_engaged_users = $facebook->get('/'.$p->fb_page_id.'/insights/page_engaged_users/day?since=' . $start_date->format('Y-m-d') . '&until=' .$end_date->format('Y-m-d'), $p->fb_page_admin_at);
			$data = json_decode($page_engaged_users->getBody(), true);
			Log::debug("Response for page_engaged_users: ".print_r($data,true));



			if (!empty($data['data'])) {
				foreach ($data['data'][0]['values'] as $lpd) {
					$counter2 += $lpd['value'];
				}

			}

			$start_date2 = DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time);
			// ho bisogno di creare start_date3 per memorizzare il valore di start_date2 che verrà perso
			$start_date3 = DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time);


			$end_date2 = DateTime::createFromFormat('Y-m-d H:i:s', $contest->end_time);
			if ( $now <= $end_date2 ) {
				$end_date2 = $now;
			} else {
				//$end_date2->add(DateInterval::createFromDateString('1 days'));
			}

			$interval = $start_date2->diff($end_date2);
			$start_date2->sub($interval);
			$start_date2->add(DateInterval::createFromDateString('1 days'));

			//$facebook->setAccessToken($p->fb_page_admin_at);
			Log::debug("calling: " . '/v2.4/'.$p->fb_page_id.'/insights/page_engaged_users/day?since=' . $start_date2->format('Y-m-d') . '&until=' .$start_date3->format('Y-m-d'));
			$page_engaged_users2 = $facebook->get('/'.$p->fb_page_id.'/insights/page_engaged_users/day?since=' . $start_date2->format('Y-m-d') . '&until=' .$start_date3->format('Y-m-d'), $p->fb_page_admin_at);
			$data = json_decode($page_engaged_users2->getBody(), true);
			Log::debug("Response for page_engaged_users 2: ".print_r($data,true));


			if (!empty($data['data'])) {
				foreach ($data['data'][0]['values'] as $lpd) {
					$counter3 += $lpd['value'];
				}

			}
			else
			{
				$counter3 = 100;
			}


			if ( $counter2 == 0 || $counter3 == 0 ) {
				$res = 0;
			} else {
				$res = $counter2 / $counter3 * 100;
			}


			$contest->fbPages()->updateExistingPivot($p->fb_p_id, array('engaged_users' => $res), false);

		}



		/*
		DB::table('contests')
		->where('id', $contest->id)
		->update(array('page_engaged_users' => $res));
		*/
		Log::debug("dailyNewLikesANDPageEngaedUsers() end");



	}

	public static function uploadOnTwitterPage($contest_id)
	{

		try {

			$contest = Contest::find($contest_id);



			$videos=Video::select("videos.*")
				->join("contests", "contests.id", "=", "videos.contest_id")
				->where("videos.approval_step_id", "=", 3)
				->where("videos.watermarkInsDone", "=", 1)
				->where("videos.uploadOnTwitterDone", "=", 0)
				->where("videos.uploadOnOwnYouTubeDone", "=", 1)
				->where("videos.contest_id", "=", $contest_id)->get();

			Log::debug("Number of videos returned for upload on Twitter: ".count($videos));

			foreach ($videos as $video)
			{

				try {

					Log::debug("Video that correspond to the correct requisites for upload on TW: ".$video->name);

					///////////////////////////////////////////////////
					// TODO
					// Controllare se il cliente per il contest ha una pagina youtube,
					// ovvero ha eseguito la procedura connect con youtube. Io avrò fatto la procedura di upload su youtube.
					// se esiste una tale istanza (nella tabella ytpage_contest ?) ricavare l'id della pagina
					// e con l'id del video ricavare il video youtube dalla tabella yt_page_video.

					// Se il cliente non ha una pagina youtube,
					// inserire il video youtube caricato preventivamente nel canale youtube di buzzmybrand
					// l'id del video youtube è presente nella tabella video

					/*
					YouTube::upload2YouTube($video->id);
					$newVideo = Video::find($video->id);
					*/

					$ytPage = $contest->ytPages->first();

					if ( $ytPage ) {

						$yt_page_video = $video->ytPages()->where('yt_pages.id', $ytPage->pivot->yt_page_id)->first();

						if ( $yt_page_video ) {
							$youTubeId = $yt_page_video->pivot->yt_idvideo;
							Log::debug('There is a customer channel for video, video ID: ' . $youTubeId);
						}

					} else {
						$youTubeId = $video->youtube_id;
						Log::debug('There is not a customer channel for video, video ID: ' . $youTubeId);
					}


					// PUBBLICAZIONE SU PAGINA TWITTER DEL CUSTOMER
					///////////////////////////////////////////////////
					foreach ($contest->twPages as $page)
					{


						Log::debug('Publishing post to twitter customer profile');

						$text = "https://www.youtube.com/watch?v=" . $youTubeId;

						$parameters = array(
							'status' => $text
						);
						$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $page->tw_page_admin_at, $page->tw_page_secret_token);
						$post = $twitteroauth->post('statuses/update', $parameters, false);

						Log::debug("Post for twitter customer profile: " . json_encode($post));

						$video->twPages()->attach($page->id, array('tw_idvideo' => $post->id));


					}
					////////////////////////////////////////////////////////////

					$video->uploadOnTwitterDone = '1';
					$video->save();

					$user = User::find($video->user_id);
					$contest = Contest::find($video->contest_id);
					$twitterProfile = TwitterProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();

					if ( $twitterProfile && $video->user_connected_to_twitter ) {

						try {

							// PUBBLICAZIONE SU PAGINA TWITTER DELL' UTENTE
							////////////////////////////////////////////////////////////
							Log::debug('Publishing post to twitter user profile');


							$text = " https://www.youtube.com/watch?v=" . $youTubeId;

							$parameters = array(
								'status' => $text
							);
							$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $twitterProfile->oauth_token, $twitterProfile->oauth_token_secret);
							$post = $twitteroauth->post('statuses/update', $parameters, false);

							Log::debug("Post for twitter user profile: " . json_encode($post));

							$video->tw_idpost_usr = $post->id;
							///////////////////////////////////////////////////////
							$video->save();

						} catch (Exception $ex){
							Log::error($ex);
						}


						// INVIO MESSAGGIO ALL'UTENTE
						//////////////////////////////////////////////////////
						$twPage = null;
						if ( $video->contest->twPages->count() > 0 ) {
							$twPage = $video->contest->twPages->first();
						}



						App::setlocale($contest->location->language);
						$message = Lang::get('messages.video_approved');

						$parameters = array(
							'user_id' => $twitterProfile->tw_id,
							'text' => $message,
						);

						$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $twPage->tw_page_admin_at , $twPage->tw_page_secret_token );
						$tweet_data = $twitteroauth->post('https://api.twitter.com/1.1/direct_messages/new.json', $parameters);

						Log::debug("Twitter response for notification" . print_r($tweet_data,true));

						Log::debug("Twitter notification done!");
						/////////////////////////////////////////////////////////


					}

					$video->save();

				} catch (Exception $ex){
					Log::error('Error during uploading video: '.$ex);
				}

			}

			$photos=Photo::select("photos.*")
				->join("contests", "contests.id", "=", "photos.contest_id")
				->where("photos.approval_step_id", "=", 3)
				->where("photos.watermarkInsDone", "=", 1)
				->where("photos.uploadOnTwitterDone", "=", 0)
				->where("photos.contest_id", "=", $contest_id)
				->whereNull("photos.user_connected_to_instagram")
				->get();

			Log::debug("Number of photos returned for upload on Twitter: ".count($photos));

			foreach ($photos as $photo)
			{


				try {

					// PUBBLICAZIONE SU PAGINA TWITTER DEL CUSTOMER
					//////////////////////////////////////////////
					Log::debug("Photo that correspond to the correct requisites for upload on TW: ".$photo->name);


					foreach ($contest->twPages as $page)
					{

						Log::debug('Publishing post to twitter customer profile');

						/*
						$todoReplacement = array("%contestname%", "%type%", "%companylink%", "%companyname%", "%hashtag%");
						$newWords = array($contest->name, $contest->contestType, "http://app.buzzmybrand.it/" . $contest->contest_route, $contest->company_name, $contest->hashtag);
						$sentencePost = str_replace($todoReplacement, $newWords, Lang::get('post.' . $contest->objective['tw_sentence_post_key']));
						*/
						$sentencePost = $photo->name;

						$text = $sentencePost;
						$image_path = public_path()."/photos/".$photo->filename;

						$handle = fopen($image_path,'rb');
						$image = fread($handle,filesize($image_path));
						fclose($handle);


						$parameters = array(
							'media[]' => "{$image};type=image/jpeg;filename={$image_path}",
							'status' => $text
						);
						$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $page->tw_page_admin_at, $page->tw_page_secret_token);
						$post = $twitteroauth->post('statuses/update_with_media', $parameters, true);

						Log::debug("Post for twitter customer profile: " . json_encode($post));

						$photo->twPages()->attach($page->id, array('tw_idphoto' => $post->id));


					}
					/////////////////////////////////////////////////////////////////////
					$photo->uploadOnTwitterDone = '1';
					$photo->save();


					$user = User::find($photo->user_id);
					$contest = Contest::find($photo->contest_id);
					$twitterProfile = TwitterProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();


					if ( $twitterProfile && $photo->user_connected_to_twitter ) {


						try {

							// PUBBLICAZIONE SU PAGINA TWITTER DELL'UTENTE
							/////////////////////////////////////////////////////////////////////
							Log::debug('Publishing post to twitter user profile');

							/*
							$todoReplacement = array("%contestname%", "%type%", "%companylink%", "%companyname%", "%hashtag%");
							$newWords = array($contest->name, $contest->contestType, "http://app.buzzmybrand.it/" . $contest->contest_route, $contest->company_name, $contest->hashtag);
							$sentencePost = str_replace($todoReplacement, $newWords, Lang::get('post.' . $contest->objective['tw_sentence_userpost_key']));
							*/

							$sentencePost = $photo->name;

							$text = $sentencePost;
							$image_path = public_path()."/photos/".$photo->filename;

							$handle = fopen($image_path,'rb');
							$image = fread($handle,filesize($image_path));
							fclose($handle);


							$parameters = array(
								'media[]' => "{$image};type=image/jpeg;filename={$image_path}",
								'status' => $text
							);
							$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $twitterProfile->oauth_token, $twitterProfile->oauth_token_secret);
							$post = $twitteroauth->post('statuses/update_with_media', $parameters, true);

							Log::debug("Post for twitter user profile: " . json_encode($post));

							$photo->tw_idpost_usr = $post->id;
							////////////////////////////////////////////////////////////////////////////////////
							$photo->save();

						} catch (Exception $ex){
							Log::error($ex);
						}


						// INVIO MESSAGGIO ALL' UTENTE
						///////////////////////////////////////////////////////////////////////
						$twPage = null;
						if ( $photo->contest->twPages->count() > 0 ) {
							$twPage = $photo->contest->twPages->first();
						}


						App::setlocale($contest->location->language);
						$message = Lang::get('messages.photo_approved');

						$parameters = array(
							'user_id' => $twitterProfile->tw_id,
							'text' => $message,
						);

						$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $twPage->tw_page_admin_at , $twPage->tw_page_secret_token );
						$tweet_data = $twitteroauth->post('https://api.twitter.com/1.1/direct_messages/new.json', $parameters);

						Log::debug("Twitter response for notification" . print_r($tweet_data, true));

						Log::debug("Twitter notification done!");
						/////////////////////////////////////////////////////////////////////////



					}

					$photo->save();


				} catch (Exception $ex){
					Log::error('Error during uploading photo: '.$ex);
				}

			}



			$essays=Essay::select("essays.*")
				->join("contests", "contests.id", "=", "essays.contest_id")
				->where("essays.approval_step_id", "=", 3)
				//->where("essays.watermarkInsDone", "=", 1)
				->where("essays.uploadOnTwitterDone", "=", 0)
				->where("essays.contest_id", "=", $contest_id)
				->get();

			Log::debug("Number of essays returned for upload on Twitter: ".count($essays));

			foreach ($essays as $essay)
			{


				try {

					// PUBBLICAZIONE SU PAGINA TWITTER DEL CUSTOMER
					//////////////////////////////////////////////
					Log::debug("Essay that correspond to the correct requisites for upload on TW: ".$essay->name);

					foreach ($contest->twPages as $page)
					{

						Log::debug('Publishing post to twitter customer profile');

						/*
						$todoReplacement = array("%contestname%", "%type%", "%companylink%", "%companyname%", "%hashtag%");
						$newWords = array($contest->name, $contest->contestType, "http://app.buzzmybrand.it/" . $contest->contest_route, $contest->company_name, $contest->hashtag);
						$sentencePost = str_replace($todoReplacement, $newWords, Lang::get('post.' . $contest->objective['tw_sentence_post_key']));
						*/
						$sentencePost = $essay->sentence;

						$text = $sentencePost;
						$image_path = public_path()."/essays/".$essay->image;

						$handle = fopen($image_path,'rb');
						$image = fread($handle,filesize($image_path));
						fclose($handle);


						$parameters = array(
							'media[]' => "{$image};type=image/jpeg;filename={$image_path}",
							'status' => ""
						);
						$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $page->tw_page_admin_at, $page->tw_page_secret_token);
						$post = $twitteroauth->post('statuses/update_with_media', $parameters, true);

						Log::debug("Post for twitter customer profile: " . json_encode($post));

						$essay->twPages()->attach($page->id, array('tw_idessay' => $post->id));



					}

					///////////////////////////////////////////////////////////////////////////
					$essay->uploadOnTwitterDone = '1';
					$essay->save();


					$user = User::find($essay->user_id);
					$contest = Contest::find($essay->contest_id);
					$twitterProfile = TwitterProfile::where('user_id', $user->id)->where('company_id', $contest->company_id)->first();


					if ( $twitterProfile && $essay->user_connected_to_twitter ) {


						try {

							// PUBBLICAZIONE SU PAGINA TWITTER DELL'UTENTE
							/////////////////////////////////////////////////////////////////////
							Log::debug('Publishing post to twitter user profile');

							/*
							$todoReplacement = array("%contestname%", "%type%", "%companylink%", "%companyname%", "%hashtag%");
							$newWords = array($contest->name, $contest->contestType, "http://app.buzzmybrand.it/" . $contest->contest_route, $contest->company_name, $contest->hashtag);
							$sentencePost = str_replace($todoReplacement, $newWords, Lang::get('post.' . $contest->objective['tw_sentence_userpost_key']));
							*/

							$sentencePost = $essay->sentence;

							$text = $sentencePost;
							$image_path = public_path()."/essays/".$essay->image;


							$handle = fopen($image_path,'rb');
							$image = fread($handle,filesize($image_path));
							fclose($handle);


							$parameters = array(
								'media[]' => "{$image};type=image/jpeg;filename={$image_path}",
								'status' => ""
							);
							$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $twitterProfile->oauth_token, $twitterProfile->oauth_token_secret);
							$post = $twitteroauth->post('statuses/update_with_media', $parameters, true);

							Log::debug("Post for twitter user profile: " . json_encode($post));

							$essay->tw_idpost_usr = $post->id;
							///////////////////////////////////////////////////////////////////
							$essay->save();

						}
						catch (Exception $ex)
						{
							Log::error($ex);
						}


						// INVIO MESSAGGIO ALL' UTENTE
						///////////////////////////////////////////////////////////////////////
						$twPage = null;
						if ( $essay->contest->twPages->count() > 0 ) {
							$twPage = $essay->contest->twPages->first();
						}


						App::setlocale($contest->location->language);
						$message = Lang::get('messages.essay_approved');

						$parameters = array(
							'user_id' => $twitterProfile->tw_id,
							'text' => $message,
						);

						$twitteroauth = new TwitterOAuth(Config::get('twitter')['consumer_key'], Config::get('twitter')['consumer_secret'],  $twPage->tw_page_admin_at , $twPage->tw_page_secret_token );
						$tweet_data = $twitteroauth->post('https://api.twitter.com/1.1/direct_messages/new.json', $parameters);

						Log::debug("Twitter response for notification" . print_r($tweet_data, true));

						Log::debug("Twitter notification done!");
						///////////////////////////////////////////////////////////////////////




					}

					$essay->save();


				}
				catch (Exception $ex)
				{
					Log::error("Error during uploading essay " . $ex);
				}


			}





		}
		catch (Exception $ex)
		{
			Log::error($ex);
		}

	}

	public static function uploadOnYoutubeBMBPage($contest_id) {

		try {

			$videos = Video::select("videos.*")
				->join("contests", "contests.id", "=", "videos.contest_id")
				->where("videos.approval_step_id", "=", 3)
				->where("videos.watermarkInsDone", "=", 1)
				->where("videos.uploadOnOwnYouTubeDone", "=", 0)
				->where("videos.contest_id", "=", $contest_id)->get();

			Log::debug("Number of videos returned for upload on Youtube channel buzzmybrand: ".count($videos));

			foreach ($videos as $video) {

				try {

					Log::debug("Video that correspond to the correct requisites for upload on YT: ".$video->name);

					$ytPage = ytPage::with(
									array('ytPlaylists' => function($query) use ($contest_id) {
										$query->where('yt_playlists.contest_id', '=', $contest_id);
									})
								)->where('id', 1)->first();

					$result = YouTube::rawUpload2YouTube($ytPage, $video);

					$video->youtube_id = $result["youtube_id"];
					$video->playlist_item = $result["playlist_item"];
					$video->uploadOnOwnYouTubeDone = 1;
					$video->save();

				} catch (Exception $ex) {
					Log::error($ex);
				}

			}


		} catch (Exception $ex) {
			Log::error($ex);
		}

	}

	public static function uploadOnYoutubeCorporatePage($contest_id) {

		try {

			$contest = Contest::with(
							array('ytPages.ytPlaylists' => function($query) use ($contest_id) {
								$query->where('yt_playlists.contest_id', '=', $contest_id);
							})
						)->where('id', $contest_id)->first();



			$videos=Video::select("videos.*")
				->join("contests", "contests.id", "=", "videos.contest_id")
				->where("videos.approval_step_id", "=", 3)
				->where("videos.watermarkInsDone", "=", 1)
				->where("videos.uploadOnCompanyYouTubeDone", "=", 0)
				->where("videos.contest_id", "=", $contest_id)->get();

			Log::debug("Number of videos returned for upload on Customer Channel of Youtube: ".count($videos));

			foreach ($videos as $video)
			{

				try {

					Log::debug("Video that correspond to the correct requisites for upload on Customer Channel of Youtube: ".$video->name);

					$page = $contest->ytPages->first();

					//foreach ($contest->ytPages as $page)
					//{

						$result = YouTube::rawUpload2YouTube($page, $video);
						$video->ytPages()->attach($page->id, array('yt_idvideo' => $result["youtube_id"], 'playlist_item' => $result["playlist_item"]));

					//}

					$video->uploadOnCompanyYouTubeDone = 1;
					$video->save();

				} catch (Exception $ex) {
					Log::error($ex);
				}

			}


		} catch (Exception $ex) {
			Log::error($ex);
		}

	}

	public static function checkPromotionsToConfirm() {

		$contests = Contest::all();


		Log::debug("checkPromotionsToConfirm start()");

		foreach($contests as $contest) {

			//$tz_object = new DateTimeZone($contest->location->timezoneDesc);
			$datetime = new DateTime();
			//$datetime->setTimezone($tz_object);
			$now = $datetime->format("Y-m-d");

			Log::debug("checkPromotionsToConfirm for contest " . $contest->name);

			Log::debug("checkPromotionsToConfirm now: " . $now);
			$start_date = DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time);
			Log::debug("checkPromotionsToConfirm start date: " . $start_date->format('Y-m-d'));
			$start_date->sub(DateInterval::createFromDateString('3 days'));
			Log::debug("checkPromotionsToConfirm check date: " . $start_date->format('Y-m-d'));



			$user = User::where('company_id', '=', $contest->company_id)->first();

			if ( $user ) {

				if ( ($contest->influencers_promotion || $contest->display_promotion || $contest->video_seeding_promotion) &&
						($contest->promotion_budget) && ($now == $start_date->format('Y-m-d')) && (!$contest->promotion_alert_done)  ) {

					Log::debug("checkPromotionsToConfirm check true");

					try {

						$start_date_2 = DateTime::createFromFormat('Y-m-d H:i:s', $contest->start_time);
						Mail::send('emails.request-confirmation', array('contest' => $contest, 'start_date' => $start_date_2), function($message) use ($user) {

							$message->to($user->email, $user->name . " " .$user->surname)
									->subject(Lang::get('email.object_request_confirmation'));

						});

						$contest->promotion_alert_done = 1;
						$contest->save();

					} catch (Exception $ex) {
						Log::error('Problem sending email: '.$ex);
					}



				}

			}

		}

		Log::debug("checkPromotionsToConfirm end()");

	}

}
?>
