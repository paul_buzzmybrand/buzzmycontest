<?php

use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;
/**
 * This class contains various helper functions that should be implemented and are used in
 * many places of the application.
 * 
 * Its main task is to check if user is logged on as user or company. There is no clear distinction within the code (there is no clear
 * role model in the application) and therefore it was delegated to separate Common API functions because it may be subject to change 
 */

class CommonHelper
{
	/**
	 * This function checks if simple user is logged on to the application
	 * 
	 * @return true if simple user authenticated and logged on, false otherwise
	 */
    public static function loggedInAsUser()
    {
        if (Auth::check())
        {
            $user = Auth::user();
            $companyId = $user->company_id;
            return !$companyId;
        }
        else
        {
            return false;
        }
    }
	
	
	public static function loggedInAsAdminUser()
	{
		Config::set('auth.model', 'Admin');
		if (Auth::check())
        {
            $adminuser = Auth::user();
            
            return true;
        }
        else
        {
            return false;
        }
	}
    
	/**
	 * This function checks if company user is logged on to the application
	 * 
	 * @return true if company user authenticated and logged on, false otherwise
	 */
    public static function loggedInAsCompany()
    {
        if (Auth::check())
        {
            $user = Auth::user();
            $companyId = $user->company_id;
            return $companyId;
		}
        else
        {
            return false;
        }
    }
    
   /**
    * This functions obtains an access token for a specific 
    */
    public static function getAccessToken($userData, $fullJson = false)
    {
		$url = '';
		$config = Config::get('website');
		
		if (isset($userData["client"]))
		{
			$url = '/oauth/access_token?grant_type=password&username='.$userData["username"]."&password=".$userData["password"]."&client_id=".$userData["client"]."&client_secret=".$config["client_secret"];
		}
		else
		{    	
			//Below implementation does not distinguish between company and simple user
			$url = '/oauth/access_token?grant_type=password&username='.$userData["username"]."&password=".$userData["password"]."&client_id=".$config["client_id"]."&client_secret=".$config["client_secret"];
			//$url = '/oauth/access_token?grant_type=password&username='.$userData["id_fb"]."&password=".$userData["password"]."&client_id=".$config["client_id"]."&client_secret=".$config["client_secret"];
		}
    	
    	$ch = self::curl($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		$json = curl_exec($ch);
		curl_close($ch);
		Log::debug("/oauth/access_token returned: ".$json);

		$decoded_json = json_decode($json);
		if ($fullJson) {
			return $decoded_json;
		} else if (isset($decoded_json->access_token)) {
			return $decoded_json->access_token;
		} else {
			return null;
		}
    }
    
    /**
     * This function is called whenever any access token is needed. If user access token
     * is available then it is taken from session. However, if it is not available then
     * administrator access token is generated and stored in session.
     */
    public static function needAnyAccessToken()
    {
    	$accessToken = Session::get("website.access_token");
    	if (empty($accessToken))
    	{
    		$accessToken = Session::get("website.general_access_token");
    		if (empty($accessToken))
    		{
    			$config = Config::get('website');
    			$url = '/oauth/access_token?grant_type=password&username='.$config["general_username"]."&password=".$config["general_password"]."&client_id=".$config["client_id"]."&client_secret=".$config["client_secret"];
    			$ch = self::curl($url);
    			curl_setopt($ch, CURLOPT_POST, true);
    			curl_setopt($ch, CURLOPT_POSTFIELDS, "");
    			$json = curl_exec($ch);
    			curl_close($ch);
    			Log::debug($url." returned: ".$json);
    			
    			$accessToken = json_decode($json)->access_token;
    			Session::put("website.general_access_token",$accessToken);
    		}
    	}
    	return $accessToken;
    }
    
    public static function curl($semiUrl)
    {
    	Log::debug("curl(".$semiUrl.")");
    	$url = URL::to($semiUrl);
    	Log::debug("Route: ".$url);
    	$ch = curl_init($url);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	return $ch;
    }
	
	public static function GetImageFromUrl($link) 
	{ 
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_POST, 0); 
		curl_setopt($ch,CURLOPT_URL,$link); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$result=curl_exec($ch); 
		curl_close($ch); 
		return $result; 
	}
    
	public static function random_string($length) 
	{
		$key = '';
		$keys = array_merge(range(0, 9), range('a', 'z'));

		for ($i = 0; $i < $length; $i++) {
			$key .= $keys[array_rand($keys)];
		}

		return $key;
	}
    
    public static function curlWithAnyToken($semiUrl)
    {
    	$accessToken = CommonHelper::needAnyAccessToken();
    	Log::debug("Access token to be used: ".$accessToken);
    	 
    	$ch = self::curl($semiUrl);
    	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
    	return $ch;
    }
    
    public static function booleanInputWithDefault($key, $defaultValue)
    {
    	$value = Input::get($key);
    	if (!isset($value))
    	{
    		return $defaultValue;
    	}
    	else
    	{
    		if (($value == "FALSE") || ($value == "false") || ($value == 0))
    		{
    			return false;
    		}
    		else
    		{
    			return true;
    		}
    	}
    }
    
    public static function booleanInput($key)
    {
    	$value = Input::get($key);
    	if (!isset($value))
    	{
    		throw new MissingMandatoryParametersException("Missing mandatory input parameter ".$key);
    	}
		if (($value == "FALSE") || ($value == "false") || ($value == 0))
    	{
    		return false;
		}
    	else
    	{
    		return true;
    	}
    }

    public static function optionalInput($key)
    {
    	$value = Input::get($key);
    	Log::debug("Input::get(".$key.") = ".$value);
    	return $value;
    }
    
    public static function mandatoryInput($key)
    {
    	$value = Input::get($key);
    	if (!isset($value))
    	{
    		throw new MissingMandatoryParametersException("Missing mandatory input parameter ".$key);
    	}
    	Log::debug("Input::get(".$key.") = ".$value);
    	return $value;
    }
	
	public static function image_save_from_url($my_img,$fullpath)
	{
		if($fullpath!="" && $fullpath){
			$fullpath = $fullpath."/".basename($my_img);
		}
		$ch = curl_init ($my_img);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
		$rawdata=curl_exec($ch);
		curl_close ($ch);
		if(file_exists($fullpath)){
			unlink($fullpath);
		}
		$fp = fopen($fullpath,'x');
		fwrite($fp, $rawdata);
		fclose($fp);
	}
	
	public static function get_short_link($url) {
		
		// http://james.cridland.net/code
		// v0.2 24 May 08: added a URLdecode function, to correctly cope with some charactersets
		//                 thanks to Nick at www.japansoc.com
		

		$bitly_login="paolo_cappelluti_83@msn.com";
		$bitly_accesstoken="e6cae345c7e5cbc61973b5f47cb0a7244f757da6";

		$api_call = file_get_contents("https://api-ssl.bitly.com/v3/shorten?longUrl=".$url."&access_token=".$bitly_accesstoken."&format=json");

		$bitlyinfo=json_decode(utf8_encode($api_call),true);

		if ($bitlyinfo['status_code']==200) {			
			return $bitlyinfo['data']['url'];
		} else {
			return false;
		}

	}
	
	
	public static function console($data, $priority, $debug)
	{
		if ($priority <= $debug)
		{
			if (is_array($data))
				$output = '<script>console.log("' . str_repeat(" ", $priority-1) . implode( ",", $data) . '");</script>';
			else
				$output = '<script>console.log("' . str_repeat(" ", $priority-1) . $data . '");</script>';

			echo $output;
		}
	}
	
	
	//max 120 requests per minute
	public static function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
		$output = NULL;
		if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
			$ip = $_SERVER["REMOTE_ADDR"];
			if ($deep_detect) {
				if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
					$ip = $_SERVER['HTTP_CLIENT_IP'];
			}
		}
		$purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
		$support    = array("country", "countrycode", "state", "region", "city", "location", "address");
		$continents = array(
			"AF" => "Africa",
			"AN" => "Antarctica",
			"AS" => "Asia",
			"EU" => "Europe",
			"OC" => "Australia (Oceania)",
			"NA" => "North America",
			"SA" => "South America"
		);
		if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
			$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
			if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
				switch ($purpose) {
					case "location":
						$output = array(
							"city"           => @$ipdat->geoplugin_city,
							"state"          => @$ipdat->geoplugin_regionName,
							"country"        => @$ipdat->geoplugin_countryName,
							"country_code"   => @$ipdat->geoplugin_countryCode,
							"continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
							"continent_code" => @$ipdat->geoplugin_continentCode
						);
						break;
					case "address":
						$address = array($ipdat->geoplugin_countryName);
						if (@strlen($ipdat->geoplugin_regionName) >= 1)
							$address[] = $ipdat->geoplugin_regionName;
						if (@strlen($ipdat->geoplugin_city) >= 1)
							$address[] = $ipdat->geoplugin_city;
						$output = implode(", ", array_reverse($address));
						break;
					case "city":
						$output = @$ipdat->geoplugin_city;
						break;
					case "state":
						$output = @$ipdat->geoplugin_regionName;
						break;
					case "region":
						$output = @$ipdat->geoplugin_regionName;
						break;
					case "country":
						$output = @$ipdat->geoplugin_countryName;
						break;
					case "countrycode":
						$output = @$ipdat->geoplugin_countryCode;
						break;
				}
			}
		}
		return $output;
	}

	// Debug Prints variable
    public static function pr($var, $die = false) {
        echo "<pre>";
        if(is_object($var) && is_a($var, 'Illuminate\Database\Eloquent\Collection')) {
            print_r($var->toArray());
        } else {
            print_r($var);
        }
        echo "</pre>";
        if($die) {
            die;
        }
    }
	
	
	
	
	
	
}
?>
