<?php

/**
 * Common API class to provide operation on company objects. Such operation include logging
 * obtaining information and statistics about Company, managing registration
 * 
 */
class CompanyHelper
{
    public static function registerCompany($data)
    {
    	DB::transaction(function() use($data) {
	    	$company = new Company();
	    	$company->name = $data["companyName"];
	    	$company->contact_name = $data["contactName"];
	    	$company->address = $data["address"];
	    	$company->type_id = Company::TYPE_INTERNAL;
	    	//$data["zipCode"];
	    	//$data["city"];
	    	$company->email = $data["email"];
	    	$company->website = $data["website"];
	    	$company->phone = $data["telephone"];
	    	$company->fax = $data["fax"];
	    	$company->save();
	    	$user = User::find(UserHelper::getLoggedIn()->id);
	    	if ($user)
	    	{
	    		$user->company_id = $company->id;
	    		$user->save();
	    		Request::instance()->attributes->remove("website.user_in_request");
	    	}
    	});
    }
    
    /**
     * Called when authenticating company/checking company credentials. Such a method should be a common method for authenticating user
     * as a company. $data input variable contains logging data, company name and given password.
     * 
     * If the user is logged on then method should also put a user within the session object (using Auth::login or similar method)
     * 
     * @return user object if company credentials are valid, null if are invalid
     */
    public static function loginCompany($data)
    {
    	Log::debug("loginCompany(). Calling UserHelper::loginUser()");
        $user = UserHelper::loginUser($data);
        if ($user)
        {
        	Log::debug("User ".$user->username." logged in. Checking if it is company user");
        	if ($user->company_id)
        	{
        		return $user;
        	}
        	else
        	{
        		Log::debug("User ".$user->username." is not a company. Forcing logout and flushing session");
        		Auth::logout();
        		Session::flush();
        		return null;
        	}
        }
        else
        {
        	return null;
        }
    }
    
    public static function getLoggedIn()
    {
    	return UserHelper::getLoggedIn()->company;
    }
    
    /**
     * Called when user clicks to delete company account. This method should remove a user from database or mark him as deleted
     * if records are not permanently removed from database. $userId is a company id
     */
    public static function deleteAccount($userId)
    {
        
    }
    
    /**
     * According to our code analysis it looks like existing company users may log on with facebook
     * Therefore we created a separate function to add such profiles for a company, however we find this functionality unnecessary
     */
    public static function addCompanyUserWithFacebook($data)
    {
        //example
        $user = new User;
        $user->username = $data['name'];
        $user->email = $data['email'];
        //image
        //$user->image = 'https://graph.facebook.com/'.$facebook_account['username'].'/picture?type=large';
        $user->save();

        $userId = $user->id;
        $facebookProfile = new FacebookProfile();
        $facebookProfile->username = $data['email'];
        $facebookProfile->uid = $data['id'];
        $facebookProfile->user_id = $userId;
        $facebookProfile->save();
        Log::info('Save new facebook profile to database: '.$data['id'].', userId: '.$userId.').');
        Auth::login($user);
        Log::info('Login to system with facebook(facebook_account: '.$data['id'].', userId: '.$userId.').');
    }
    
    public static function getOwnCompany()
    {
    	$company = Company::find(Config::get("admin")["own_company_id"]);
    	Log::debug("Own company: id=".$company->id.", name=".$company->name);
    	return $company;
    }
}
?>
