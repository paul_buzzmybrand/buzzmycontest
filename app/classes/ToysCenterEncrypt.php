<?php

class ToysCenterEncrypt {
    const AES_256_CBC = "aes-256-cbc";
    protected $_securekey = 'ToysCenter01';
	protected $iv = "JVALXDLPRCMPNRLS";

    /**
     * @param $input
     * @return string
     */
    public function encrypt($input)
    {
        $encryptInput = openssl_encrypt($input, self::AES_256_CBC, $this->_securekey, OPENSSL_RAW_DATA, $this->iv);
        return $encryptInput;
    }

    /**
     * @param $input
     * @return string
     */
    public function decrypt($input)
    {
        $decryptInput = openssl_decrypt($input, self::AES_256_CBC, $this->_securekey, OPENSSL_RAW_DATA, $this->iv);
        return $decryptInput;
    }
}

?>