<?php

set_include_path(get_include_path().PATH_SEPARATOR.app_path().'/classes');
require_once 'Google/Client.php';
require_once 'Google/Service/Books.php';

use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
class YouTube
{
	public static function getCompanyAccessTokenAsJson($yt_page)
	{
		$json = json_encode(array("access_token" => $yt_page->yt_access_token, "created" => time(), "expires_in" => 3600));
		Log::debug("You tube access_token as JSON: ".$json);
		return $json;
	}
	
	public static function linkToYouTubeConsentPage($companyId)
	{
		$config = Config::get("youtube");
		$link = "https://accounts.google.com/o/oauth2/auth?client_id=".$config["client_id"]."&redirect_uri=".$config["redirect_uri"]."&response_type=code&scope=https://www.googleapis.com/auth/youtube.upload&access_type=offline&approval_prompt=force&state=".$companyId;
		Log::debug("linkToYouTubeConsentPage = ".$link);
		return $link;
	}
	
	private static function jsonExchangeCodeForAccessToken($code)
	{
		$config = Config::get("youtube");
		$post = array("code" => $code, "client_id" => $config["client_id"], "client_secret" => $config["client_secret"], "redirect_uri" => $config["redirect_uri"], "grant_type" => "authorization_code");
		$url = "https://accounts.google.com/o/oauth2/token";
		$ch = curl_init($url);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    	curl_setopt($ch, CURLOPT_POST,1);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    	$json = curl_exec($ch);
    	if(curl_errno($ch))
    	{
    		$info = curl_getinfo($ch);
    		Log::warning("OAuth2 curl error: ".print_r($info,true));
    		curl_close($ch);
    		throw new IOException("Error while accessing YouTube OAuth2");
  		}
    	curl_close($ch);
    	$decoded_json = json_decode($json);
    	Log::debug($url." returned: ".json_encode($decoded_json,JSON_PRETTY_PRINT));
    	
    	return $decoded_json;
	}
	
	public static function exchangeCodeForAccessToken($code,$companyId)
	{
		Log::debug("exchangeCodeForAccessToken(code=".$code.",companyId=".$companyId.")");
		$json = self::jsonExchangeCodeForAccessToken($code);
		$company = Company::find($companyId);
		if (!isset($json->access_token))
		{
			if (isset($json->error) && isset($json->error_description))
			{
				throw new IOException("Error while accessing YouTube OAuth2: error=".$json->error.", error_description=".$json->error_description);
			}
			else
			{
				throw new IOException("Error while accessing YouTube OAuth2");
			}				
		}
		$company->yt_access_token = $json->access_token;
		$company->yt_token_type = $json->token_type;
		$company->yt_token_expires_ts = new DateTime();
		$company->yt_token_expires_ts->add(new DateInterval("PT".$json->expires_in."S"));
		$company->yt_refresh_token = $json->refresh_token;
		$company->save();
	}
	
	private static function jsonRefreshToken($refreshToken)
	{
		$config = Config::get("youtube");
		$post = array("refresh_token" => $refreshToken, "client_id" => $config["client_id"], "client_secret" => $config["client_secret"], "grant_type" => "refresh_token");
		$url = "https://accounts.google.com/o/oauth2/token";
		$ch = curl_init($url);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    	curl_setopt($ch, CURLOPT_POST,1);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    	$json = curl_exec($ch);
    	if(curl_errno($ch))
    	{
    		$info = curl_getinfo($ch);
    		Log::warning("OAuth2 curl error: ".print_r($info,true));
    		curl_close($ch);
    		throw new IOException("Error while accessing YouTube OAuth2");
    	}
    	curl_close($ch);
    	$decoded_json = json_decode($json);
    	Log::debug($url." returned: ".json_encode($decoded_json,JSON_PRETTY_PRINT));
    	
    	return $decoded_json;
	}
	
	public static function refreshToken(&$yt_page)
	{
		$json = self::jsonRefreshToken($yt_page->yt_refresh_token);
		$yt_page->yt_access_token = $json->access_token;
		$yt_page->yt_token_type = $json->token_type;
		$yt_page->yt_token_expires_ts = new DateTime();
		$yt_page->yt_token_expires_ts->add(new DateInterval("PT".$json->expires_in."S"));
		$yt_page->save();
		Log::debug("Youtube page (".$yt_page->id.") token refreshed. Youtube page structure: ".print_r($yt_page,true));
	}
	
	public static function refreshTokenIfNeeded(&$yt_page)
	{
		$now = new DateTime();
		Log::debug("Testing if youtube page (".$yt_page->id.") token expired on ".$yt_page->yt_token_expires_ts.". Now: ".$now->format('Y-m-d H:i:s'));
		if ($now > new DateTime($yt_page->yt_token_expires_ts))
		{
			Log::debug("Youtube page (".$yt_page->id.") token expired. Refreshing");
			self::refreshToken($yt_page);
		}
	}
	
	public static function upload2YouTube($videoId)
	{
		Log::debug("upload2YouTube, videoId: ".$videoId);
		$video = Video::find($videoId);
		if ($video->approval_step_id != ApprovalStep::APPROVED)
		{
			Log::warning("Video ".$video->id." not approved. Status: ".$video->approvalStep->name);
			return;
		}
		
		$ownCompany = CompanyHelper::getOwnCompany();
		$company = $video->contest->company;
		if (!$video->uploadOnOwnYouTubeDone)
		{
			if ($ownCompany->yt_access_token)
			{
				self::rawUpload2YouTube($ownCompany, $video);
				$video->uploadOnOwnYouTubeDone = 1;
				$video->save();
			}
			else
			{
				Log::error("Missing own company (".$ownCompany->id.") access token");
				throw new InvalidArgumentException("Missing own company (".$ownCompany->id.") access token");
			}
		}
		
		if ($video->contest->should_upload_to_company_youtube && $company->yt_access_token && !$video->uploadOnCompanyYouTubeDone)
		{
			self::rawUpload2YouTube($company, $video);
			$video->uploadOnCompanyYouTubeDone = 1;
			$video->save();
		}
		else
		{
			Log::warning("Company (".$company->id."): either access_token is missing or should_upload_to_company_youtube=0 or upload already done");
		}
	}
	
	public static function rawUpload2YouTube(&$yt_page,$video)
	{
		$video_id = $video->id;
		

		
	
		Log::debug("rawUpload2YouTube, video: ".$video->id.", youtube page: ".$yt_page->id);
		self::refreshTokenIfNeeded($yt_page);
		
		Log::debug("Initializing Google_client class");
		$config = Config::get("youtube");
		$client = new Google_Client();
		$client->setClientId($config["client_id"]);
		$client->setClientSecret($config["client_secret"]);
		$client->setScopes('https://www.googleapis.com/auth/youtube');
		$client->setRedirectUri($config["redirect_uri"]);
	
		// Define an object that will be used to make all API requests.
		$youtube = new Google_Service_YouTube($client);
		$client->setAccessToken(self::getCompanyAccessTokenAsJson($yt_page));
	
		$videoPath = public_path()."/videos/".$video->filename;

		// Create a snippet with title, description, tags and category ID
		// Create an asset resource and set its snippet metadata and type.
		// This example sets the video's title, description, keyword tags, and
		// video category.
		$snippet = new Google_Service_YouTube_VideoSnippet();
		$snippet->setTitle($video->name);
		
		/*
		$contest = Contest::find($video->contest_id);
		$todoReplacement = array("%contestname%", "%type%", "%companylink%", "%companyname%", "%hashtag%");
		$newWords = array($contest->name, $contest->contestType, "http://app.buzzmybrand.it/" . $contest->contest_route, $contest->company_name, $contest->hashtag);
		if ( $yt_page->id == 1 ) {
			$sentencePost = str_replace($todoReplacement, $newWords, Lang::get('post.' . $contest->objective['yt_bmb_sentence_post_key']));
		} else {
			$sentencePost = str_replace($todoReplacement, $newWords, Lang::get('post.' . $contest->objective['yt_cmp_sentence_post_key']));
		}
		*/
		
		$sentencePost = $video->name;
		
		$snippet->setDescription($sentencePost);
		$snippet->setTags($config["tags"]);

		// Numeric video category. See
		// https://developers.google.com/youtube/v3/docs/videoCategories/list
		$snippet->setCategoryId($config['category_id']);

		// Set the video's status to "public". Valid statuses are "public",
		// "private" and "unlisted".
		$status = new Google_Service_YouTube_VideoStatus();
		$status->privacyStatus = ($config['privacy_status']);

		// Associate the snippet and status objects with a new video resource.
		$video = new Google_Service_YouTube_Video();
		$video->setSnippet($snippet);
		$video->setStatus($status);

		// Specify the size of each chunk of data, in bytes. Set a higher value for
		// reliable connection as fewer chunks lead to faster uploads. Set a lower
		// value for better recovery on less reliable connections.
		$chunkSizeBytes = $config['chunk_size_bytes'];

		// Setting the defer flag to true tells the client to return a request which can be called
		// with ->execute(); instead of making the API call immediately.
		$client->setDefer(true);

		// Create a request for the API's videos.insert method to create and upload the video.
		$insertRequest = $youtube->videos->insert("status,snippet", $video);

		// Create a MediaFileUpload object for resumable uploads.
		$media = new Google_Http_MediaFileUpload(
				$client,
				$insertRequest,
				'video/*',
				null,
				true,
				$chunkSizeBytes
		);
		$media->setFileSize(filesize($videoPath));


		// Read the media file and upload it chunk by chunk.
		$status = false;
		$handle = fopen($videoPath, "rb");
		while (!$status && !feof($handle)) {
			$chunk = fread($handle, $chunkSizeBytes);
			$status = $media->nextChunk($chunk);
		}
		
		Log::debug("status: " . print_r($status, true));

		/*
		$videoToSave = Video::find($video_id);
		$videoToSave->youtube_id = $status->id;
		$videoToSave->save();
		*/
		
		fclose($handle);

		// If you want to make other calls after the file upload, set setDefer back to false
		$client->setDefer(false);
	
		$bmbVideo = Video::find($video_id);
		$bmbContest = $bmbVideo->contest;
		
		$resourceId = new Google_Service_YouTube_ResourceId();
		$resourceId->setVideoId($status['id']);
		$resourceId->setKind('youtube#video');
	
		
		if ( $yt_page->ytPlaylists->count() > 0 ) {
		
			$playlist_id = $yt_page->ytPlaylists->first()['resource_id'];
			
		} else {
			
			$playlistSnippet = new Google_Service_YouTube_PlaylistSnippet();
			$playlistSnippet->setTitle($bmbContest->name);
			$playlistSnippet->setDescription($bmbContest->concept);

			$playlistStatus = new Google_Service_YouTube_PlaylistStatus();
			$playlistStatus->setPrivacyStatus('public');

			$youTubePlaylist = new Google_Service_YouTube_Playlist();
			$youTubePlaylist->setSnippet($playlistSnippet);
			$youTubePlaylist->setStatus($playlistStatus);

			$playlistResponse = $youtube->playlists->insert('snippet,status',
			$youTubePlaylist, array());
			
			$playlist_id = $playlistResponse['id'];
			
			$newPlaylist = new ytPlaylist();
			$newPlaylist->resource_id = $playlist_id;
			$newPlaylist->contest_id = $bmbContest->id;
			$newPlaylist->yt_page_id = $yt_page->id;
			$newPlaylist->save();
			
		}
		
		$playlistItemSnippet = new Google_Service_YouTube_PlaylistItemSnippet();
		$playlistItemSnippet->setTitle($bmbVideo->name);
		$playlistItemSnippet->setPlaylistId($playlist_id);
		$playlistItemSnippet->setResourceId($resourceId);
		
		$playlistItem = new Google_Service_YouTube_PlaylistItem();
		$playlistItem->setSnippet($playlistItemSnippet);
		$playlistItemResponse = $youtube->playlistItems->insert(
			'snippet,contentDetails', $playlistItem, array());
		
		Log::debug("playlistitem response: " . print_r($playlistItemResponse, true));
		
		return array("youtube_id" => $status->id, "playlist_item" => $playlistItemResponse->id);
	}
	
	public static function deleteVideo($yt_page, $playlist_item, $video_id) {
	
		try {
	
			Log::debug("Delete video, video: ".$video_id.", youtube page: ".$yt_page->id . " playlist item " . $playlist_item);
			self::refreshTokenIfNeeded($yt_page);
			
			$config = Config::get("youtube");
			$client = new Google_Client();
			$client->setClientId($config["client_id"]);
			$client->setClientSecret($config["client_secret"]);
			$client->setScopes('https://www.googleapis.com/auth/youtube');
			$client->setRedirectUri($config["redirect_uri"]);
		
			// Define an object that will be used to make all API requests.
			$youtube = new Google_Service_YouTube($client);
			$client->setAccessToken(self::getCompanyAccessTokenAsJson($yt_page));
			
			$result = $youtube->playlistItems->delete($playlist_item);
			Log::debug("Delete playlist post: " . print_r($result, true));
			
			$result = $youtube->videos->delete($video_id);
			Log::debug("Delete video post: " . print_r($result, true));
	
		} catch (Exception $ex) {
			Log::error('Problem deleting video on youtube: '.$ex);
		}
	
	}
	
	public static function getStatistics(&$yt_page,$id)
	{
	
		self::refreshTokenIfNeeded($yt_page);
		
		$config = Config::get("youtube");
		$client = new Google_Client();
		$client->setClientId($config["client_id"]);
		$client->setClientSecret($config["client_secret"]);
		$client->setScopes('https://www.googleapis.com/auth/youtube');
		$client->setRedirectUri($config["redirect_uri"]);
	
		// Define an object that will be used to make all API requests.
		$youtube = new Google_Service_YouTube($client);
		$client->setAccessToken(self::getCompanyAccessTokenAsJson($yt_page));
		
		$listResponse = $youtube->videos->listVideos("statistics",
        array('id' => $id));
		
		Log::debug("getStatistics response: " . print_r($listResponse, true));
		
		$v = $listResponse[0];
		$s = $v['statistics'];
		
		return $s;
	
	}
	
	public static function getSubscriptions(&$yt_page) {
	
		self::refreshTokenIfNeeded($yt_page);
		
		$config = Config::get("youtube");
		$client = new Google_Client();
		$client->setClientId($config["client_id"]);
		$client->setClientSecret($config["client_secret"]);
		$client->setScopes('https://www.googleapis.com/auth/youtube');
		$client->setRedirectUri($config["redirect_uri"]);
	
		$youtube = new Google_Service_YouTube($client);
		$client->setAccessToken(self::getCompanyAccessTokenAsJson($yt_page));
		
		$totalSubscriptions = 0;
		
		$subList = $youtube->subscriptions->listSubscriptions("id,snippet, subscriberSnippet", array('mySubscribers'=>'true', 'maxResults'=>'50'));
		
		$totalSubscriptions += count( $subList->getItems() );
		
		while ( $nextPageToken = $subList->getNextPageToken() ) {

			$subList = $youtube->subscriptions->listSubscriptions("id,snippet, subscriberSnippet", array('mySubscribers'=>'true', 'maxResults'=>'50'));
			$totalSubscriptions += count( $subList->getItems() );
		}
		
		return $totalSubscriptions;
		
	
	}
	
}
?>
