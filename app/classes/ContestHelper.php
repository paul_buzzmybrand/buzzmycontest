<?php
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\Filesystem\Exception\IOException;
use GDText\Box;
use GDText\Color;
/**
 * This class provides Common API to manipulate contests. Many function within this API are already implemented,
 * however they need to be revised as it is very likely that structure of the data shall be changed.
 */

class ContestHelper
{
    /**
     * Method called to save uploaded video to the database. Moreover, this function should
     * copy files to the destination folder, rename them to keep filename convention, generate thumbnails
     * and perpahs Facebook and Twitter share links
     *
     * @param $videnoName Name of the video file that is uploaded to the standard upload folder
     * @param $duration Length of the file (in seconds?)
     * @param $contestId ID of the contest to add video
     * @param $userId ID of the user adding the video
     * @return ID of the newly-added video
     */
    public static function saveVideo($videoName, $duration, $contestId, $userId, $title)
    {
//        $accessToken = Session::get("website.access_token");
//        Log::debug("Access token to be used: ".$accessToken);
//        $config = Config::get('hdfvr');
//        $imagePath = $config['imagePath'];
//        $image = $imagePath.'/'.$videoName.'.jpg';
//        $video = 'video/'.$videoName.'.mp4';
//        $url = URL::to("/v1/video/join?video=".urlencode($video)."&duration=".urlencode($duration)."&contest=".urlencode($videoName)."&title=".urlencode($videoName)."&image=".urlencode($image));
//        Log::debug("Video recording. Route to controller: ".$url);
//        $ch = curl_init($url);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
//        $json = curl_exec($ch);
//        curl_close($ch);
//        Log::debug("/video/join: ".$json);
//
//        $decoded_json = json_decode($json); var_dump($json);exit;
//        return $decoded_json;



        $accessToken = Session::get("website.access_token");
    	Log::debug("Access token to be used: ".$accessToken);
    	$url = "/v1/video/join";
    	Log::debug("Route to controller: ".$url);
    	$config = Config::get('hdfvr');
		Log::debug("video path (before realpath()): ".$config['pathToVideoCommandLine'].$videoName.$config['videoExtension']);
    	$video = realpath($config['pathToVideoCommandLine'].$videoName.$config['videoExtension']);
		Log::debug("image path (before realpath()): ".$config['pathToImage'].$videoName.$config['imageExtension']);
        $image = realpath($config['pathToImage'].$videoName.$config['imageExtension']);
        //$contest = Contest::find($contestId);
        //$contestName = $contest->name;
		//$user = User::find($userId);

		//$contest = Contest::find($contestId);
		//$facebookProfile = FacebookProfile::where('user_id', $userId)->where('company_id', $contest->company_id)->first();
		//$post = array('video'=> new CurlFile($video),'image'=>new CurlFile($image), 'title' => $title, 'duration' => $duration, 'id_contest' => $contestId, 'fbid' => $facebookProfile->id_fb);
		$post = array('video'=> new CurlFile($video),'image'=>new CurlFile($image), 'title' => $title, 'duration' => $duration, 'id_contest' => $contestId, 'user_id' => $userId);

        Log::debug("Prepared post parameters: ".print_r($post,true));
        Log::debug("Preparing to post video to VideoController::store(). video = ".$video.", image = ".$image.", contestId = ".$contestId.", title = ".$videoName.", duration = ".$duration);
        $ch = CommonHelper::curl($url);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
        $result=curl_exec($ch);
        if(curl_errno($ch))
        {
        	Log::error("Video upload curl error: ".curl_error($ch).", info: ".print_r(curl_getinfo($ch),true));
        	curl_close($ch);
        	throw new \IOException("Error while uploading video");
        }
        curl_close($ch);
    	Log::debug("/video/join: ".$result);
//          $inputs = Input::all();
//
//            if(!isset($inputs['contest'])||!isset($inputs['title'])||!isset($inputs['video'])||!isset($inputs['duration'])||!isset($inputs['image']))
//            {
//                $description='Required Paramaters Not Found';
//                $statusCode=400;
//                return Response::make($description, $statusCode);
//            }
//
//            $videoFile=Input::file('video');
//            $videoFilePath=$videoFile->getRealPath();
//            $videoFileName=md5_file($videoFilePath);
//            $videoExtension=strtolower($videoFile->getClientOriginalExtension());
//            //$videoExtension=strtolower($videoFile->getMimeType());
//            if (!isset($videoExtension) or $videoExtension=='')
//            {
//                $videoExtension = 'mp4';
//            }
//            $videoPath =
//            $imageFile=Input::file('image');
//            $imageFilePath=$imageFile->getRealPath();
//            //$imageFileName=md5_file($imageFilePath);
//            //dd($videoFileName);
//            $imageExtension=strtolower($imageFile->getClientOriginalExtension());
//            if (!isset($imageExtension) or $imageExtension=='')
//            {
//                $imageExtension = 'jpg';
//            }
//
//            //$imageExtension=strtolower($imageFile->getMimeType());
//            if(     File::move($videoFile,'videos/'."$videoFileName.$videoExtension")&&
//                    File::move($imageFile,'images/videos/'."$videoFileName.$imageExtension")
//                    )
//            {
//
//                exec("./GenerateFacebookShareHTMLForUploadedVideo.sh $videoFileName.$videoExtension",$output);
//                exec("./GenerateThumbnails.sh $videoFileName.$videoExtension",$output);
//
//                $ownerId = ResourceServer::getOwnerId();
//                $user=User::find($userId);
//                $config = Config::get('hdfvr');
//                $videoExtension = $config['videoExtension'];
//                $imageExtension = $config['imageExtension'];
//                $video=new Video();
//                $video->filename="$videoName.$videoExtension";
//                $video->name=$videoName;
//                $video->duration=$duration;
//                $video->image="$videoName.$imageExtension";
//                $video->user_id=$userId;
//                $video->location=$user->country;
//                $video->save();
//
//               $contestName=$inputs['contest'];
//
//                if ($inputs['contest'] != "MyChannel")
//                {
//                    //dd ($contestName);
//                    $contest=Contest::where('name',$contestName)->first();
//                    $video->contests()->attach($contestId);
//                    Log::info('Save new video to database (video id: '.$video->id.', userId: '.$userId.').');
//                }
//
//                $statusCode=200;
//                return Response::make($video,$statusCode);
//
//            }
//           $description='Error in File Upload';
//           $statusCode=400;
//           return Response::make($video,$statusCode);
//                return $video->id;
    }


	    /**
     * Converts uploaded MOV file to MP4 format. It is call after video is uploaded to the portal
     * by the user from mobile friendly mini-website. It uses data in hdfvr.php config file
     *
     * @param unknown $videoName
     * @param unknown $pathToVideo
     * @return 0, null or false - conversion successful, error message (as string) otherwise
     */
    public static function convertMOV2MP4($videoName, $pathToVideo)
    {
        $error = false;
        $config = Config::get('hdfvr');
        $limit = $config['limitToProcess'];
        $desc = array(
            0 => array('pipe', 'r'),
            1 => array('pipe', 'w'),
            2 => array('pipe', 'w')
        );
        //linux
        //$cmd = '/usr/local/bin/ffmpeg -i '.$pathToVideo.$videoName.'.flv -ar 22050 '.$pathToVideo.$videoName.'.mp4';

        //windows
        $cmd = '/root/bin/ffmpeg -i '.$pathToVideo.$videoName.'.mov -vcodec copy -acodec copy '.$pathToVideo.$videoName.'.mp4';

        // spawn the process
        Log::info('Attempt convert file to mp4 (video name: '.$videoName.', cmd: '.$cmd.').');
        $process = proc_open($cmd, $desc, $pipes);
        $start = microtime(true);
        $status = proc_get_status($process);
        while($status["running"])
        {
            sleep(2);
            $status = proc_get_status($process);
            $stop = microtime(true);
            $time = bcsub($stop, $start, 3);
            if($time > $limit){

                Log::warning('Error while convert video to mp4(video name: '.$videoName.', path to video: '.$pathToVideo.', cmd:  '.$cmd.' )');
                $error = trans('messages.error_occurred');
                break;
            }
        }
        $getStatus = proc_get_status($process);
        if($getStatus['exitcode'] == 1)
        {
            $error = trans('messages.error_occurred');
            $ffmpegOutput = stream_get_contents($pipes[2]);
            Log::warning('Error while convert video to mp4(video name: '.$videoName.', path to video: '.$pathToVideo.', cmd:  '.$cmd.', ffmpeg output: '.$ffmpegOutput.' )');
        }
        Log::info("MOV2MP4 error: ".$error);
        //$ffmpegOutput = stream_get_contents($pipes[2]);var_dump($ffmpegOutput, proc_get_status($process));exit;
       // var_dump(stream_get_contents($pipes[1]), stream_get_contents($pipes[2]), stream_get_contents($pipes[0]));exit;
        return $error;
    }


    /**
     * Converts uploaded FLV (flash video format) file to MP4 format. It is call after video is uploaded to the portal
     * by the user. It uses data in hdfvr.php config file
     *
     * @param unknown $videoName
     * @param unknown $pathToVideo
     * @return 0, null or false - conversion successful, error message (as string) otherwise
     */
    public static function convertFLV2MP4($videoName, $pathToVideo)
    {
        $error = false;
        $config = Config::get('hdfvr');
        $limit = $config['limitToProcess'];
        $desc = array(
            0 => array('pipe', 'r'),
            1 => array('pipe', 'w'),
            2 => array('pipe', 'w')
        );
        //linux
        //$cmd = '/usr/local/bin/ffmpeg -i '.$pathToVideo.$videoName.'.flv -ar 22050 '.$pathToVideo.$videoName.'.mp4';

        //windows
        $cmd = '/root/bin/ffmpeg -i '.$pathToVideo.$videoName.'.flv -y -ar 22050 '.$pathToVideo.$videoName.'.mp4';

        // spawn the process
        Log::info('Attempt convert file to mp4 (video name: '.$videoName.', cmd: '.$cmd.').');
        $process = proc_open($cmd, $desc, $pipes);
        $start = microtime(true);
        $status = proc_get_status($process);
        while($status["running"])
        {
            sleep(2);
            $status = proc_get_status($process);
            $stop = microtime(true);
            $time = bcsub($stop, $start, 3);
            if($time > $limit){

                Log::warning('Error while convert video to mp4(video name: '.$videoName.', path to video: '.$pathToVideo.', cmd:  '.$cmd.' )');
                $error = trans('messages.error_occurred');
                break;
            }
        }
        $getStatus = proc_get_status($process);
        if($getStatus['exitcode'] == 1)
        {
            $error = trans('messages.error_occurred');
            $ffmpegOutput = stream_get_contents($pipes[2]);
            Log::warning('Error while convert video to mp4(video name: '.$videoName.', path to video: '.$pathToVideo.', cmd:  '.$cmd.', ffmpeg output: '.$ffmpegOutput.' )');
        }
        Log::info("FLV2MP4 error: ".$error);
        //$ffmpegOutput = stream_get_contents($pipes[2]);var_dump($ffmpegOutput, proc_get_status($process));exit;
       // var_dump(stream_get_contents($pipes[1]), stream_get_contents($pipes[2]), stream_get_contents($pipes[0]));exit;
        return $error;
    }

    /**
     * Saves contest as draft. Invoked after first and second step if user did not submitted the contest and decided
     * to save a draft only for future edit.
     *
     * @return id of the new contest
     */
    public static function saveContestAsDraft($data, $startDate = null, $endDate = null)
    {
        //example id
        return 22;
    }

    /**
     * Method should return available plans data. This API should be specified when additional information related to each plan is available
     * especially prices, options, texts, perhaps additional automatic payment options
     */
    public static function checkPlan($data, $contestId)
    {

    }

    /**
     * Return contests data
     *
     * @param $id ID of the contest
     */
    public static function get($id)
    {
    	$accessToken = CommonHelper::needAnyAccessToken();
    	Log::debug("Access token to be used: ".$accessToken);

    	$url = URL::to("/v1/contests", array($id));
    	Log::debug("Getting contest by id = ".$id.". Route to controller: ".$url);
    	$ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
    	$json = curl_exec($ch);
    	curl_close($ch);
    	$decoded_json = json_decode($json);
    	Log::debug($url." returned: ".json_encode($decoded_json,JSON_PRETTY_PRINT));


    	return $decoded_json;
   	}

    /**
     * This method is called in dashboard. It returns contest statistics based on contest ID
     *
     * @param $id ID of the contest to be returned
     * @return contest object
     */
    public static function getContestStats($id)
    {
        //example data to return
        if($id == 5)
        {
            $data = array(
                'title' => 'Example Title',
                'status' => 'draft',
                'number_to_video_selected' => 0,
                'user_engaded' => 0,
                'awerage_views_video' => 0,
                'total_views' => 0,
                'induced_virality_views' => 0,
                'campaign_jmt_score' => 0,
                'video_with_heighest_score' => 0,
                'facebook_likes' => 0,
                'facebook_comments' => 0,
                'facebook_shares' => 0,
                'twitter_favourites' => 0,
                'twitter_replies' => 0,
                'twitter_reetwets' => 0,
                'youtube_likes' => 0,
                'youtube_commnets' => 0,
                'youtube_shares' => 0
            );
        }
        else
        {
            $data = array(
                'title' => 'Example Title',
                'status' => 'active',
                'number_to_video_selected' => 0,
                'user_engaded' => 0,
                'awerage_views_video' => 0,
                'total_views' => 0,
                'induced_virality_views' => 0,
                'campaign_jmt_score' => 0,
                'video_with_heighest_score' => 0,
                'facebook_likes' => 0,
                'facebook_comments' => 0,
                'facebook_shares' => 0,
                'twitter_favourites' => 0,
                'twitter_replies' => 0,
                'twitter_reetwets' => 0,
                'youtube_likes' => 0,
                'youtube_commnets' => 0,
                'youtube_shares' => 0
            );
        }

        return $data;
    }


    /**
     * Returns all contests for the given company user. This method is called to populate selection list in dashboard
     *
     * @param $userId ID of the company user
     * @return array of arrays (id, name, status)
     */
    public static function getAllContests($userId)
    {
        //example data to return
        $data = array(
            array(
                'id' => 2,
                'name' => 'example',
                'status' => 'active',
            ),
            array(
                'id' => 3,
                'name' => 'example2',
                'status' => 'active',
            ),
            array(
                'id' => 5,
                'name' => 'example4',
                'status' => 'draft',
            ),
            array(
                'id' => 7,
                'name' => 'example7',
                'status' => 'active',
            ),
            array(
                'id' => 8,
                'name' => 'example8',
                'status' => 'active',
            ),
            array(
                'id' => 9,
                'name' => 'example9',
                'status' => 'active',
            ),
        );
        return $data;
    }

    /**
     * This function returns contest statistics based on the given company user ID, it is called from within the dashboard.
     * It returns statistics of the last contest and is called when no other contest is selected (in other words: user just clicked dashboard)
     *
     * @param $userId ID of company user
     * @return contest statistics array
     */
    public static function getLastContest($userId)
    {
        //example data to return
        $data = array(
            'id' => 2,
            'status' => 'active',
            'number_to_video_selected' => 0,
            'user_engaded' => 0,
            'awerage_views_video' => 0,
            'total_views' => 0,
            'induced_virality_views' => 0,
            'campaign_jmt_score' => 0,
            'video_with_heighest_score' => 0,
            'facebook_likes' => 0,
            'facebook_comments' => 0,
            'facebook_shares' => 0,
            'twitter_favourites' => 0,
            'twitter_replies' => 0,
            'twitter_reetwets' => 0,
            'youtube_likes' => 0,
            'youtube_commnets' => 0,
            'youtube_shares' => 0
        );
        return $data;
    }

    /**
     * This method is called from within dashboard when user chooses to edit
     */
    public static function getDraftContestById($id)
    {
        //example data to return
        $data = array(
            'contestName' => 'example',
            'startDate' => '',
            'endDate' => '',
            'briefRules' => 'exmpale'
        );
        return $data;
    }

    /**
     * This method is called from within launch a contest functionality to create or update a contest in the database
     *
     * @param $data array of entry parameters to create contest
     * @param $contestId ID of the contest to be updated/created
     */
    public static function updateContest($data, $contestId)
    {

    }

    /**
     * Generates graph (for the given ID or additional data, perhaps chart type). Therefore there are no param defined yet
     */
    public static function getGraph()
    {
        return 'images/wykres.png';
    }

    /**
     * Returns a collection of videos of the given contest. This is called in simple user/contest screen
     *
     * @param $contestId ID of contest
     * @return multitype:multitype:number arrays of video parameters
     */
    public static function getVideosToContest($contestId)
    {
        //example data to return
        $data = array(
            array(
                'id' => 1,
                'image' => '2322.png'
            ),
            array(
                'id' => 2,
                'image' => '2322.png'
            ),
            array(
                'id' => 3,
                'image' => '2322.png'
            ),
            array(
                'id' => 4,
                'image' => '2322.png'
            ),
            array(
                'id' => 5,
                'image' => '2322.png'
            ),
            array(
                'id' => 6,
                'image' => '2322.png'
            ),
            array(
                'id' => 7,
                'image' => '2322.png'
            ),
        );
        return $data;
    }

    /**
     * This method is called in simple user/contest and returns basic information about contest
     *
     * @param $contestId ID of contest
     * @return array of contest parameters
     */
    public static function getContestData($contestId)
    {
        //example data to retrun
        $data = array(
            'jmt_score' => 100,
            'brief_rules' => 'Description',
            'judges' => 'JMT score',
            'rewards' => 'Cash',
            'entries' => 'One per user'
        );
        return $data;
    }

    /**
     * Method called in launch a contest, it parses date in string format: DD/MM/YYYY
     *
     * @param unknown $date
     * @return DateTime
     */
    public static function formatDate($date)
    {
        $data = explode("/", $date);
        $day = $data[0];
        $mounth = $data[1];
        $year = $data[2];
        $date = $day.'-'.$mounth.'-'.$year;
        $date = new DateTime($date);
        return $date;
    }

    /**
     * Search for contests based on reward, keyword and location
     */
	public static function search($keyword, $reward, $location)
	{
	   	$location = Input::get('location');
	   	$name = Input::get('name');
	   	$reward = Input::get('reward');

	   	$url = "/v1/contests?External_contest=false&location=".urlencode($location)."&name=".urlencode($name)."&reward=".urlencode($reward);
	   	$ch = CommonHelper::curlWithAnyToken($url);
	   	$json = curl_exec($ch);
	   	curl_close($ch);
	   	Log::debug($url." returned: ".$json);

	   	$decoded_json = json_decode($json);
	   	return $decoded_json;
	}

	public static function getVideo($id)
	{
		$accessToken = CommonHelper::needAnyAccessToken();
		Log::debug("Access token to be used: ".$accessToken);

		$url = "/v1/videos/".$id;
		$ch = CommonHelper::curl($url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
		$json = curl_exec($ch);
		$decodedJson = json_decode($json);
		curl_close($ch);
		Log::debug($url." returned: ".json_encode($decodedJson,JSON_PRETTY_PRINT));
		return $decodedJson;
	}

	public static function checkContestShiftWatermark2YouTube($contestId)
	{
		Log::debug("Checking if watermarking is done and contest should be shifted to upload2YouTube status");
		$videos = Video::where("contest_id",$contestId)->where("watermarkInsDone",0)->where("approval_step_id",ApprovalStep::APPROVED)->get();
		if (count($videos) == 0)
		{
			Log::debug("There are no videos left assigned to this contest. Changing contest status");
			$contest = Contest::find($contestId);
			$contest->watermarkInsDone = 1;
			$contest->save();
			Log::debug("Contest ".$contest->id." successfully watermarked");
		}
		else
		{
			Log::debug("There are ".count($videos)." left");
		}
	}

	//da testare
	public static function insertWatermarkPhoto($photoId, $type)
	{
		try
		{
			Log::debug("Preparing to insert watermark to photo with Id: ".$photoId);
			$config = Config::get('admin');

			////// MOdificato ///////////
			if ( $type == 2 ) {
				$photo = Photo::find($photoId);
			} else if ( $type == 3 ) {
				$photo = Essay::find($photoId);
			}
			//////////////////////////////

			if ($photo->watermarkInsDone)
			{
				Log::debug("This photo has a watermark already. Skipping");
				return;
			}
			if ($photo->approval_step_id != ApprovalStep::APPROVED)
			{
				Log::warning("Photo ".$photo->id." not approved. Status: ".$photo->approvalStep->name);
				return;
			}

			////// MOdificato ///////////
			if ( $type == 2 ) {
				$inputPhotoFilename = $config['photos_folder_path']."/".$photo->filename;
			} else if ( $type == 3 ) {
				$inputPhotoFilename = $config['essays_folder_path']."/".$photo->image;
			}
			//////////////////////////////


			Log::debug("Watermark input file: ".$inputPhotoFilename);
			$extension = pathinfo($inputPhotoFilename)["extension"];
			$outputRotatePhotoFilename = tempnam(null,null).".".$extension;
			Log::debug("Watermark output file: ".$outputRotatePhotoFilename);
			$outputPhotoFilename = tempnam(null,null).".".$extension;
			Log::debug("Watermark output file: ".$outputPhotoFilename);

			$watermarkOwnFilename = null;
			$watermarkCompanyFilename = null;

			// create new Intervention Image
			$img = Image::make(base_path()."/".$inputPhotoFilename);

			$orientation = $img->exif('Orientation');
			Log::debug("Image orientation: ".$orientation);
			$standard_width = 800;

			if (!empty($orientation))
			{
				switch($orientation) {
					case 8:


						$img->rotate(90);
						//save it
						$img->save($outputRotatePhotoFilename);

						$img = Image::make($outputRotatePhotoFilename);
						Log::debug("Width image already rotated: ".$img->width());
						// your desired ratio
						$ratio = 4/3;
						// resize
						$img->fit($standard_width, intval($standard_width / $ratio));
						break;
					case 3:


						$img->rotate(180);
						//save it
						$img->save($outputRotatePhotoFilename);

						$img = Image::make($outputRotatePhotoFilename);
						Log::debug("Width image already rotated: ".$img->width());
						// your desired ratio
						$ratio = 4/3;
						// resize
						$img->fit($standard_width, intval($standard_width / $ratio));
						break;
					case 6:

						// resize the image to a width of 300 and constrain aspect ratio (auto height)
						/*$img->resize(640, null, function ($constraint) {
							$constraint->aspectRatio();
						});*/
						$img->rotate(-90);
						//save it
						$img->save($outputRotatePhotoFilename);

						$img = Image::make($outputRotatePhotoFilename);
						Log::debug("Width image already rotated: ".$img->width());
						// your desired ratio
						$ratio = 4/3;
						// resize
						$img->fit($standard_width, intval($standard_width / $ratio));
						break;
					default:
						$img->save($outputRotatePhotoFilename);
						$img = Image::make($outputRotatePhotoFilename);
						$ratio = 4/3;
						// resize
						$img->fit($standard_width, intval($standard_width / $ratio));
				}
			} else {
				$img->save($outputRotatePhotoFilename);
				$img = Image::make($outputRotatePhotoFilename);
				$ratio = 4/3;
				// resize
				$img->fit($standard_width, intval($standard_width / $ratio));
			}
			

			if (false && $photo->contest->should_insert_company_watermark)
			{
				$watermarkCompanyFilename = $config["watermark_company_folder_path"]."/".$photo->contest->company->image;

				$extension = pathinfo(base_path()."/".$watermarkCompanyFilename)["extension"];
				$resizedLogo = tempnam(null,null).".".$extension;

				$img_logo = Image::make(base_path()."/".$watermarkCompanyFilename);
				// resize the logo to a width of ($img->width()/8) and constrain aspect ratio (auto height)
				/*
				$img_logo->resize(intval($img->width() / 8), null, function ($constraint) {
					$constraint->aspectRatio();
				});
				$img_logo->save($resizedLogo);
				*/

				// insert watermark at bottom-right corner with 10px offset
				//$img->insert(base_path()."/".$watermarkCompanyFilename, 'top-left', 10, 10);
				$img->insert($img_logo, 'bottom-left', intval($img->width() / 64), intval($img->width() / 64));

				//save it
				$img->save($outputPhotoFilename);

				Log::debug("Watermark file created successfully. Renaming existing file and creating backup of existing one");
				if (rename(base_path()."/".$inputPhotoFilename,base_path()."/".$inputPhotoFilename."-backup-".time()) == false)
				{
					throw new IOException("Could not rename ".$inputPhotoFilename);
				}
				if (rename($outputPhotoFilename,base_path()."/".$inputPhotoFilename) == false)
				{
					throw new IOException("Could not rename ".$outputPhotoFilename." to ".$inputPhotoFilename);
				}
				Log::info("Photo successfully watermarked");
				$photo->watermarkInsDone = 1;
				$photo->save();
			}

			if ($photo->contest->should_insert_contest_watermark)
			{

				$imgWithContestWatermark = $img;

				/////////////////////////////
				/////////////////////////////
				/////////////////////////////
				if ($photo->contest->sponsor_logo) {
					$watermarkOwnFilename = "public/images/events/".$photo->contest->sponsor_logo; 
				}
				else {
					$watermarkOwnFilename = "public/images/events/bmb_logo_large.png";
				}
				$extension = pathinfo(base_path()."/".$watermarkOwnFilename)["extension"];
				$resizedLogo = tempnam(null,null).".".$extension;

				$img_logo = Image::make(base_path()."/".$watermarkOwnFilename);

				// resize the logo to a width of ($img->width()/8) and constrain aspect ratio (auto height)
				/*
				$img_logo->resize(intval($imgWithContestWatermark->width() / 8), null, function ($constraint) {
					$constraint->aspectRatio();
				});
				$img_logo->save($resizedLogo);
				*/
				// insert watermark at bottom-right corner with 10px offset
				//$img->insert(base_path()."/".$watermarkOwnFilename, 'top-right', 10, 10);

				$imgWithContestWatermark->insert($img_logo, 'bottom-left', intval($imgWithContestWatermark->width() / 64), intval($imgWithContestWatermark->width() / 64));

				//save it
				$imgWithContestWatermark->save($outputPhotoFilename);

				$inputPhoto = explode('.', $inputPhotoFilename);

				Log::debug("Watermark file created successfully");
				/*if (rename(base_path()."/".$inputPhotoFilename,base_path()."/".$inputPhotoFilename."-backup-".time()) == false)
				{
					throw new IOException("Could not rename ".$inputPhotoFilename);
				}*/
				if (rename($outputPhotoFilename,base_path()."/".$inputPhotoFilename) == false)
				{
					throw new IOException("Could not rename ".$outputPhotoFilename." to ".$inputPhotoFilename);
				}
				Log::info("Photo successfully watermarked");
				$photo->watermarkInsDone = 1;
				$photo->save();

				/////////////////////////////
				/////////////////////////////
				/////////////////////////////


				//ciclare per tutte le pagine in cui si trova la foto e metterci il corrispettivo watermark
/*
				foreach ($photo->contest->fbPages as $page)
				{
					if( $page->custom_logo != '' && $page->custom_logo != null)
					{

						$watermarkOwnFilename = "public/images/events/".$page->custom_logo;
						$extension = pathinfo(base_path()."/".$watermarkOwnFilename)["extension"];
						$resizedLogo = tempnam(null,null).".".$extension;

						$img_logo = Image::make(base_path()."/".$watermarkOwnFilename);

						// resize the logo to a width of ($img->width()/8) and constrain aspect ratio (auto height)
						$img_logo->resize(intval($imgWithContestWatermark->width() / 8), null, function ($constraint) {
							$constraint->aspectRatio();
						});
						$img_logo->save($resizedLogo);
						// insert watermark at bottom-right corner with 10px offset
						//$img->insert(base_path()."/".$watermarkOwnFilename, 'top-right', 10, 10);

						$imgWithContestWatermark->insert($resizedLogo, 'bottom-right', intval($imgWithContestWatermark->width() / 64), intval($imgWithContestWatermark->width() / 64));

					}
					if ($photo->contest->should_insert_company_watermark)
					{
						$watermarkCompanyFilename = $config["watermark_company_folder_path"]."/".$photo->contest->company->image;

						$extension = pathinfo(base_path()."/".$watermarkCompanyFilename)["extension"];
						$resizedLogo = tempnam(null,null).".".$extension;

						$img_logo = Image::make(base_path()."/".$watermarkCompanyFilename);
						// resize the logo to a width of ($img->width()/8) and constrain aspect ratio (auto height)
						$img_logo->resize(intval($imgWithContestWatermark->width() / 8), null, function ($constraint) {
							$constraint->aspectRatio();
						});
						$img_logo->save($resizedLogo);

						// insert watermark at bottom-right corner with 10px offset
						//$img->insert(base_path()."/".$watermarkCompanyFilename, 'top-left', 10, 10);
						$imgWithContestWatermark->insert($resizedLogo, 'bottom-left', intval($imgWithContestWatermark->width() / 64), intval($imgWithContestWatermark->width() / 64));
					}

					//save it
					$imgWithContestWatermark->save($outputPhotoFilename);

					$inputPhoto = explode('.', $inputPhotoFilename);

					Log::debug("Watermark file created successfully");
					/*if (rename(base_path()."/".$inputPhotoFilename,base_path()."/".$inputPhotoFilename."-backup-".time()) == false)
					{
						throw new IOException("Could not rename ".$inputPhotoFilename);
					}*/
/*
					if (rename($outputPhotoFilename,base_path()."/".$inputPhoto[0]."_".$page->id.".".$inputPhoto[1]) == false)
					{
						throw new IOException("Could not rename ".$outputPhotoFilename." to ".$inputPhotoFilename);
					}
					Log::info("Photo successfully watermarked");
					$photo->watermarkInsDone = 1;
					$photo->save();


				}
*/

				/*
				$logo_filename = explode('.', $photo->contest->image);
				$watermarkOwnFilename = "public/images/events/".$logo_filename[0]."_wt.png";

				$extension = pathinfo(base_path()."/".$watermarkOwnFilename)["extension"];
				$resizedLogo = tempnam(null,null).".".$extension;

				$img_logo = Image::make(base_path()."/".$watermarkOwnFilename);
				// resize the logo to a width of ($img->width()/8) and constrain aspect ratio (auto height)
				$img_logo->resize(intval($img->width() / 8), null, function ($constraint) {
					$constraint->aspectRatio();
				});
				$img_logo->save($resizedLogo);
				// insert watermark at bottom-right corner with 10px offset
				//$img->insert(base_path()."/".$watermarkOwnFilename, 'top-right', 10, 10);

				$img->insert($resizedLogo, 'bottom-right', intval($img->width() / 64), intval($img->width() / 64));
				*/
			}



		}
		catch (Exception $ex)
		{
			Log::error($ex);
		}



	}



	public static function insertWatermarkVideo($videoId)
	{
		Log::debug("Preparing to insert watermark");
		$config = Config::get('admin');
		$video = Video::find($videoId);
		if ($video->watermarkInsDone)
		{
			Log::debug("This video has a watermark already. Skipping");
			return;
		}
		if ($video->approval_step_id != ApprovalStep::APPROVED)
		{
			Log::warning("Video ".$video->id." not approved. Status: ".$video->approvalStep->name);
			return;
		}
		$inputVideoFilename = $config['videos_folder_path']."/".$video->filename;
		Log::debug("Watermark input file: ".$inputVideoFilename);
		$extension = pathinfo($inputVideoFilename)["extension"];
		$outputVideoFilename = tempnam(null,null).".".$extension;
		Log::debug("Watermark output file: ".$outputVideoFilename);

		$watermarkOwnFilename = null;
		$watermarkCompanyFilename = null;

		try
		{

			//Insertion of company watermark+sponsor watermark (if any) with remaming file - compression file
			if (($video->contest->should_insert_company_watermark) && (!$video->contest->should_insert_contest_watermark))
			{
				$watermarkCompanyFilename = $config["watermark_company_folder_path"]."/".$video->contest->company->image;

				self::insertWatermarkFile($inputVideoFilename,$outputVideoFilename,$watermarkCompanyFilename,null);
			}
			else if (($video->contest->should_insert_contest_watermark) && (!$video->contest->should_insert_company_watermark))
			{
				$watermarkOwnFilename = "public/images/events/".$video->contest->sponsor_logo;

				self::insertWatermarkFile($inputVideoFilename,$outputVideoFilename,$watermarkOwnFilename,null);
			}
			else if (($video->contest->should_insert_contest_watermark) && ($video->contest->should_insert_company_watermark))
			{
				$watermarkOwnFilename = "public/images/events/".$video->contest->sponsor_logo;

				$watermarkCompanyFilename = $config["watermark_company_folder_path"]."/".$video->contest->company->image;

				self::insertWatermarkFile($inputVideoFilename,$outputVideoFilename,$watermarkOwnFilename,$watermarkCompanyFilename);
			}

			//self::insertWatermarkFile($inputVideoFilename,$outputVideoFilename,$watermarkOwnFilename,null);

			Log::debug("Watermark file created successfully. Renaming existing file and creating backup of existing one");
			if (rename(base_path()."/".$inputVideoFilename, base_path()."/".$inputVideoFilename."-backup-".time()) == false)
			{
				throw new IOException("Could not rename ".$inputVideoFilename);
			}
			if (rename($outputVideoFilename, base_path()."/".$inputVideoFilename) == false)
			{
				throw new IOException("Could not rename ".$outputVideoFilename." to ".$inputVideoFilename);
			}
			Log::info("Video ".$videoId." successfully watermarked with company logo");

			/////////////////////////////////////////////////////////


			/*
			//ciclare per tutte le pagine in cui si trova la foto e metterci il corrispettivo watermark
			foreach ($video->contest->fbPages as $page)
			{
				$watermarkCompanyFilename = null;
				if($page->should_insert_custom_wt)
				{

					$watermarkOwnFilename = "public/images/events/".$page->custom_logo;
					Log::debug("watermark: ".$watermarkOwnFilename);

					self::insertWatermarkFile($inputVideoFilename,$outputVideoFilename,$watermarkOwnFilename,$watermarkCompanyFilename);

					$inputVideo = explode('.', $inputVideoFilename);

					Log::debug("Watermark file created successfully");

					if (rename($outputVideoFilename,base_path()."/".$inputVideo[0]."_".$page->id.".".$inputVideo[1]) == false)
					{
						throw new IOException("Could not rename ".$outputVideoFilename." to ".$inputVideo[0]."_".$page->id.".".$inputVideo[1]);
					}

					Log::info("Video ".$videoId." successfully watermarked for page with id: ".$page->id);
				}

			}
			*/



			$video->watermarkInsDone = 1;
			$video->save();


		}
		catch (Exception $ex)
		{
			Log::error("Error during insertWatermarkVideo function. Error: ".$ex);
		}




		//Log::debug("Checking if entire contest is successfully watermarked");
		//self::checkContestShiftWatermark2YouTube($video->contest_id);
	}



	public static function insertWatermarkFile($filenameIn, $filenameOut, $wm1, $wm2)
	{

		$ffmpeg = "/usr/bin/ffmpeg";
		// FIX
		$ffmpeg = "/root/bin/ffmpeg";
		// END FIX
		$config = Config::get('admin');
		$timeout = $config['ffmpeg.timeout'];
		$cmdLine = null;

		$filenameIn = base_path()."/".$filenameIn;

		if (!is_null($wm1))
		{
			$wm1 = base_path()."/".$wm1;
		}
		if (!is_null($wm2))
		{
			$wm2 = base_path()."/".$wm2;
		}

		//check the orientation of the video
		//check the size of the video

		// get rotation of the video
		ob_start();
		passthru($ffmpeg . " -i " . $filenameIn . " 2>&1");
		$duration_output = ob_get_contents();
		ob_end_clean();

		// rotate?
		if (preg_match('/rotate *: (.*?)\n/', $duration_output, $matches))
		{
			$rotation = $matches[1];
			Log::debug('Rotation: '.$rotation);
			if ($rotation == "90")
			{
				$extension = pathinfo($filenameIn)["extension"];
				$filenameRotate = tempnam(null,null).".".$extension;

				$cmdLine = sprintf($ffmpeg.' -i '.$filenameIn.' -metadata:s:v:0 rotate=0 -vf "transpose=1" '.$filenameRotate);

				$cmdLine = str_replace("\\", "/", $cmdLine);
				Log::debug("FFMPEG rotate operation cmdline: ".$cmdLine);
				$retVal = null;
				system($cmdLine,$retVal);
				Log::debug("ffmpeg rotate operation return value: ".$retVal);
				if ($retVal != 0)
				{
					throw new IOException("Error while inserting watermark, cmdline: ".$cmdLine);
				}

				$filenameIn = $filenameRotate;
			}

			else if ($rotation == "180")
			{
				$extension = pathinfo($filenameIn)["extension"];
				$filenameRotate1 = tempnam(null,null).".".$extension;
				//fare 2 rotazioni
				$cmdLine = sprintf($ffmpeg.' -i '.$filenameIn.' -metadata:s:v:0 rotate=0 -vf "transpose=1" '.$filenameRotate1);

				$cmdLine = str_replace("\\", "/", $cmdLine);
				Log::debug("FFMPEG first rotate operation cmdline: ".$cmdLine);
				$retVal = null;
				system($cmdLine,$retVal);
				Log::debug("ffmpeg rotate operation return value: ".$retVal);
				if ($retVal != 0)
				{
					throw new IOException("Error while inserting watermark, cmdline: ".$cmdLine);
				}
				$filenameRotate2 = tempnam(null,null).".".$extension;
				$cmdLine = sprintf($ffmpeg.' -i '.$filenameRotate1.' -metadata:s:v:0 rotate=0 -vf "transpose=1" '.$filenameRotate2);

				$filenameIn = $filenameRotate2;
				//echo shell_exec($ffmpeg . ' -i ' . $output_file_full . ' -metadata:s:v:0 rotate=0 -vf "transpose=1" ' . $output_file_full . ".rot2.mp4 2>&1") . "\n";
				//echo shell_exec($ffmpeg . ' -i ' . $output_file_full . '.rot2.mp4 -metadata:s:v:0 rotate=0 -vf "transpose=1" ' . $output_file_full . ".rot.mp4 2>&1") . "\n";
				//echo shell_exec("mv $output_file_full.rot.mp4 $output_file_full") . "\n";
			}
			else if ($rotation == "270")
			{
				$extension = pathinfo($filenameIn)["extension"];
				$filenameRotate = tempnam(null,null).".".$extension;
				//fare una rotazione di valore 2
				$cmdLine = sprintf($ffmpeg.' -i '.$filenameIn.' -metadata:s:v:0 rotate=0 -vf "transpose=2" '.$filenameRotate);

				$cmdLine = str_replace("\\", "/", $cmdLine);
				Log::debug("FFMPEG rotate operation cmdline: ".$cmdLine);
				$retVal = null;
				system($cmdLine,$retVal);
				Log::debug("ffmpeg rotate operation return value: ".$retVal);
				if ($retVal != 0)
				{
					throw new IOException("Error while inserting watermark, cmdline: ".$cmdLine);
				}

				$filenameIn = $filenameRotate;
				//echo shell_exec($ffmpeg . ' -i ' . $output_file_full . ' -metadata:s:v:0 rotate=0 -vf "transpose=2" ' . $output_file_full . ".rot.mp4 2>&1") . "\n";
				//echo shell_exec("mv $output_file_full.rot.mp4 $output_file_full") . "\n";
			}
		}

		$extension = pathinfo($filenameIn)["extension"];
		$filenamewithLogo = tempnam(null,null).".".$extension;

		//ridimensionare i loghi in accordo alla dimensione del video
		$dimensionVideo = self::get_video_dimensions($filenameIn);
		$ih = $dimensionVideo['height'];
		$iw = $dimensionVideo['width'];

		Log::debug('Height: '.$ih);
		Log::debug('Width: '.$iw);



		if ($wm2 == null)
		{
			$extensionLogo = pathinfo($wm1)["extension"];
			$resizedLogo1 = tempnam(null,null).".".$extensionLogo;

			$img_logo = Image::make($wm1);
			// resize the logo to a width of ($img->width()/8) and constrain aspect ratio (auto height)
			//$img_logo->resize(intval($iw / 8), null, function ($constraint) {
			//	$constraint->aspectRatio();
			//});
			$img_logo->save($resizedLogo1);

			$cmdLine = sprintf($config['ffmpeg.insert_watermark_cmdline_wm1'],$filenameIn,$resizedLogo1,$filenamewithLogo);
		}
		else if ($wm1 == null)
		{
			$extensionLogo = pathinfo($wm2)["extension"];
			$resizedLogo2 = tempnam(null,null).".".$extensionLogo;

			$img_logo = Image::make($wm2);
			// resize the logo to a width of ($img->width()/8) and constrain aspect ratio (auto height)
			//$img_logo->resize(intval($iw / 8), null, function ($constraint) {
			//	$constraint->aspectRatio();
			//});
			$img_logo->save($resizedLogo2);

			$cmdLine = sprintf($config['ffmpeg.insert_watermark_cmdline_wm2'],$filenameIn,$resizedLogo2,$filenamewithLogo);
		}
		else
		{
			$extensionLogo = pathinfo($wm1)["extension"];
			$resizedLogo1 = tempnam(null,null).".".$extensionLogo;

			Log::debug("Path watermark1: ".$wm1);

			$img_logo = Image::make($wm1);
			// resize the logo to a width of ($img->width()/8) and constrain aspect ratio (auto height)
			//$img_logo->resize(intval($iw / 8), null, function ($constraint) {
			//	$constraint->aspectRatio();
			//});
			$img_logo->save($resizedLogo1);

			$extensionLogo = pathinfo($wm2)["extension"];
			$resizedLogo2 = tempnam(null,null).".".$extensionLogo;

			$img_logo = Image::make($wm2);
			// resize the logo to a width of ($img->width()/8) and constrain aspect ratio (auto height)
			//$img_logo->resize(intval($iw / 8), null, function ($constraint) {
			//	$constraint->aspectRatio();
			//});
			$img_logo->save($resizedLogo2);

			$cmdLine = sprintf($config['ffmpeg.insert_watermark_cmdline_wm1_wm2'],$filenameIn,$resizedLogo1,$resizedLogo2,$filenamewithLogo);
		}
		$cmdLine = str_replace("\\", "/", $cmdLine);
		Log::debug("FFMPEG logo insert cmdline: ".$cmdLine);
		$retVal = null;
		system($cmdLine,$retVal);
		Log::debug("ffmpeg return value: ".$retVal);
		if ($retVal != 0)
		{
			throw new IOException("Error while inserting watermark, cmdline: ".$cmdLine);
		}

		/*
		$dimensionVideo = self::get_video_dimensions($filenamewithLogo);
		$ih = $dimensionVideo['height'];
		$iw = $dimensionVideo['width'];

		Log::debug('Height: '.$ih);
		Log::debug('Width: '.$iw);
		*/
		//Resize and scale the video to fill the player area 640x480
		$cmdLine = sprintf($ffmpeg.' -i '.$filenamewithLogo.' -filter:v "scale='.$iw.'*min(640/'.$iw.'\,480/'.$ih.'):'.$ih.'*min(640/'.$iw.'\,480/'.$ih.'), pad=640:480:(640-'.$iw.'*min(640/'.$iw.'\,480/'.$ih.'))/2:(480-'.$ih.'*min(640/'.$iw.'\,480/'.$ih.'))/2" '.$filenameOut);
		Log::debug("FFMPEG scale&resize video cmdline: ".$cmdLine);
		$retVal = null;
		system($cmdLine,$retVal);
		Log::debug("ffmpeg scale&resize return value: ".$retVal);
		if ($retVal != 0)
		{
			throw new IOException("Error while scale&resize, cmdline: ".$cmdLine);
		}



		//Then retrieve width and height of the watermarked video and launch the following command:
		//ffmpeg -i in.mp4 -filter:v "scale=iw*min(640/iw\,480/ih):ih*min(640/iw\,480/ih), pad=640:480:(640-iw*min(640/iw\,480/ih))/2:(480-ih*min(640/iw\,480/ih))/2" out.mp4
		//iw and ih are witdh and height of the input video - 640x480 is the area for the player

		/*
		$desc = array(
				0 => array('pipe', 'r'),
				1 => array('pipe', 'w'),
				2 => array('pipe', 'w')
		);

		$process = proc_open($cmdLine, $desc, $pipes);
		$start = microtime(true);
		$status = proc_get_status($process);
		Log::debug("PID: ".$status["pid"]);
		while($status["running"])
		{
			Log::debug("Loop iteration");
			sleep(2);
			$status = proc_get_status($process);
			$stop = microtime(true);
			$time = bcsub($stop, $start, 3);
			if($time > $timeout)
			{
				Log::warning("Error while inserting watermark, timeout reached (".$timeout."), cmdline: ".$cmdLine);
				throw new IOException("Error while inserting watermark, timeout reached (".$timeout."), cmdline: ".$cmdLine);
			}
		}
		Log::debug("Current status running: ".$status["running"]);

		$getStatus = proc_get_status($process);
		$exitCode = $getStatus['exitcode'];
		if ($exitCode != 0)
		{
			Log::debug("Error occured while watermarking");
			$ffmpegOutput = stream_get_contents($pipes[2]);
			Log::warning("Error while inserting watermark, exitCode: ".$exitCode.', cmd: '.$cmdLine.', ffmpeg output: '.$ffmpegOutput.' )');
			throw new IOException("Error while inserting watermark, exitCode: ".$exitCode.", cmdline: ".$cmdLine,$exitCode);
		}
		else
		{
			Log::debug("Watermark insert finished successfully, cmdLine: ".$cmdLine);
		}*/
	}


	public static function get_video_dimensions($video = false)
	{
		if (file_exists ( $video ))
		{
			$ffmpeg = "/usr/bin/ffmpeg";
			// FIX
			$ffmpeg = "/root/bin/ffmpeg";
			// END FIX
			$command = $ffmpeg . ' -i ' . $video . ' -vstats 2>&1';
			$output = shell_exec ( $command );

			$result = preg_match ( '/([0-9]{2,4})x([0-9]{2,4})/', $output, $regs );

			if (isset ( $regs [0] ))
			{
				$vals = (explode ( 'x', $regs [0] ));
				$width = $vals [0] ? $vals [0] : null;
				$height = $vals [1] ? $vals [1] : null;
				return array ('width' => $width, 'height' => $height );
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}

	}


	public static function get_video_attributes($video)
	{

		// FIX
		$codec = null;
		$width = null;
		$height = null;
		$hours = null;
		$mins = null;
		$secs = null;
		$ms = null;
		// END FIX

		$ffmpeg = "/usr/bin/ffmpeg";
		// FIX
		$ffmpeg = "/root/bin/ffmpeg";
		// END FIX

		$command = $ffmpeg . ' -i ' . $video . ' -vstats 2>&1';
		Log::debug("get video attributes ffmpeg command: ". $command);
		$output = shell_exec($command);
		Log::debug("get video attributes output ". $output);
		$regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/";
		if (preg_match($regex_sizes, $output, $regs)) {
			$codec = $regs [1] ? $regs [1] : null;
			$width = $regs [3] ? $regs [3] : null;
			$height = $regs [4] ? $regs [4] : null;
		 }

		$regex_duration = "/Duration: ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}).([0-9]{1,2})/";
		if (preg_match($regex_duration, $output, $regs)) {
			$hours = $regs [1] ? $regs [1] : null;
			$mins = $regs [2] ? $regs [2] : null;
			$secs = $regs [3] ? $regs [3] : null;
			$ms = $regs [4] ? $regs [4] : null;
		}

		return array ('codec' => $codec,
				'width' => $width,
				'height' => $height,
				'hours' => $hours,
				'mins' => $mins,
				'secs' => $secs,
				'ms' => $ms
		);

	}

	public static function createThumbnailVideo($video)
	{
		$ffmpeg = "/usr/bin/ffmpeg";
		// FIX
		$ffmpeg = "/root/bin/ffmpeg";
		// END FIX

		//ottenere nome video senza estensione
		//$filename = pathinfo($video)['filename'];
		$ext = pathinfo($video, PATHINFO_EXTENSION);
		$filename = basename($video, ".".$ext);

		//ffmpeg -i $basepath/videos/$file -s 480x360 -ss 1 -r 1 -t 1 -f image2 $basepath/images/videos/$name-facebook.jpg &>/dev/null
		$command = $ffmpeg.' -i '.$video.' -s 480x360 -ss 1 -r 1 -t 1 -f image2 '.base_path().'/public/images/videos/'.$filename.'.jpg 2>&1';
		Log::debug("command FFMPEG: ".$command);
		$output = shell_exec($command);
		Log::debug("Output createThumbnailVideo: ".$output);
		return $filename.'.jpg';
	}


	public static function saveVideoPart($videoName, $duration, $contestId, $userId, $videopart)
	{


		$accessToken = Session::get("website.access_token");
		Log::debug("Access token to be used: ".$accessToken);
		$url = "/v1/video/join";
		Log::debug("Route to controller: ".$url);
		$config = Config::get('hdfvr');
		Log::debug("video path (before realpath()): ".$config['pathToVideoCommandLine'].$videoName.$config['videoExtension']);
		$video = realpath($config['pathToVideoCommandLine'].$videoName.$config['videoExtension']);
		Log::debug("image path (before realpath()): ".$config['pathToImage'].$videoName.$config['imageExtension']);
		$image = realpath($config['pathToImage'].$videoName.$config['imageExtension']);
		$contest = Contest::find($contestId);
		$contestName = $contest->name;
		$post = array('video'=> new CurlFile($video),'image'=>new CurlFile($image), 'title' => 'video_part', 'duration' => $duration, 'contest' => $contestName, 'sub_videopart' => $videopart);
		Log::debug("Prepared post parameters: ".print_r($post,true));
		Log::debug("Preparing to post video to VideoController::store(). video = ".$video.", image = ".$image.", contestName = ".$contestName.", videopart = ".$videopart.", duration = ".$duration);
		$ch = CommonHelper::curl($url);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
		$result=curl_exec($ch);
		if(curl_errno($ch))
		{
			Log::error("Video upload curl error: ".curl_error($ch).", info: ".print_r(curl_getinfo($ch),true));
			curl_close($ch);
			throw new \IOException("Error while uploading video");
		}
		curl_close($ch);
		Log::debug("/video/join: ".$result);

	}

	public static function createEssayPicture($text)
	{
		//$text = "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sei una merda!";        

		$backgrounds = array("red.jpg", "blu.jpg", "green.jpg", "yellow.jpg");
		$rand_key = array_rand($backgrounds, 1);

		$im = @imagecreatefromjpeg(public_path().'/essays/backgrounds/'.$backgrounds[$rand_key]);

		$textbox = new Box($im);
		$textbox->setFontSize(44);
		$textbox->setFontFace(public_path().'/essays/backgrounds/opensans.ttf');
		$textbox->setFontColor(new Color(20, 20, 20)); // black
		$textbox->setTextShadow(
			new Color(0, 0, 0, 80), // black color, but 60% transparent
			0,
			-1 // shadow shifted 1px to top
		);
		$textbox->setBox(
			40,  // distance from left edge
			10,  // distance from top edge
			imagesx($im) - 80, // textbox width, equal to image width
			imagesy($im) - 20  // textbox height, equal to image height
		);

		// now we have to align the text horizontally and vertically inside the textbox
		// the texbox covers whole image, so text will be centered relatively to it
		$textbox->setTextAlign('left', 'center');
		// it accepts multiline text
		$textbox->draw($text);

		$uploads_dir = public_path().'/essays/';
		//image file name
		//$name ="$fbid.png";
		$essayname = md5(time());
		$filename = $uploads_dir.$essayname.".jpg"; //this saves the image inside uploaded_files folder
		imagejpeg($im, $filename, 75);
		imagedestroy($im);

		return $filename;


	}

public static function getExcelFile($id)
	{
		//$id è l'id del contest
		//$id = '22';

		$users = DB::table('users')
            ->join('contest_user', 'users.id', '=', 'contest_user.user_id')
			->where('contest_user.contest_id', $id)
            ->select(['survey_name as Name', 'survey_surname as Surname', 'survey_email as Email', 'survey_birthdate as Birthdate', 'survey_phone as Phone', 'survey_city as City'])
            ->get();


		for ($i = 0, $c = count($users); $i < $c; ++$i) {
			$users[$i] = (array) $users[$i];
		}



		Excel::create('Analytics', function($excel) use($users) {

			// Set the title
			$excel->setTitle('Registered Users');

			// Chain the setters
			$excel->setCreator('BuzzMyBrand')
				  ->setCompany('BuzzMyBrand');

			// Call them separately
			$excel->setDescription('A collection of data regarding registered users');

			$excel->sheet('Users', function($sheet) use($users) {

				$sheet->fromArray($users);

			});

		})->export('xls');
	}

}
?>
