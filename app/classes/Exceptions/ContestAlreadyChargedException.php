<?php

use Lang;

class ContestAlreadyChargedException extends Exception {
    protected $message = 'This contest had already been charged.';
}