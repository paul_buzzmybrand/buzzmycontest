<?php

class InstagramMigration {
	
	public static function migration() {
		
		Log::debug("InstagramMigration");
		
		//Devo individuare il contest e quindi le foto instagram
		//attribuire ad ogni foto un nome random ( $photoFileName=md5(time()); ) e sostituirlo all'url che ora è presente
		//Salvare tutti gli url presi da $photo->filename in public_path()."/photos/"
		
		
		
		$contest = Contest::find(228);
		
		$photos=Photo::select("photos.*")
			->where("photos.contest_id", "=", 228)
			->where("photos.approval_step_id", "=", 3)
			->where("photos.user_connected_to_instagram", "=", 1)->get(); 
			
		foreach ($photos as $photo) {
			
			Log::debug("Analyzing photo id: ".$photo->id);
						
			try 
			{			
				/*
				$photoFileName = md5(time());
				
				Log::debug("New file name of the photo: ".$photoFileName);
			
				self::downloadFile($photo->filename, public_path()."/photos/".$photoFileName.".jpg");
			
				$photo->filename = $photoFileName.".jpg";
				
				$photo->save();

				//watermarkare la foto + creare html per sharing
				ContestHelper::insertWatermarkPhoto($photo->id, 2);
				Scheduler::replace_in_file($photo, 2);
				*/
				
				//caricare la foto su facebook
				//*******************************************************************
				
				Log::debug("Photo that correspond to the correct requisites for upload on FB: ".$photo->name);

				$facebook = new Facebook\Facebook([
					'app_id' => $contest->company->fbApp_clientID,
					'app_secret' => $contest->company->fbApp_clientSecret,
					'default_graph_version' => 'v2.4',
				]);
				
				//$facebook->setFileUploadSupport(true);
										
				$pages_array = $contest->fbPages;
				
				$source_fb = '';
				
				//Qui devo ciclare per tutte le pagine FB connesse al contest
				//Quindi devo costruirmi l'array per individuare le pagine FB connesse al contest
				foreach ($contest->fbPages as $page)
				{	
					
					$admin_access_token = $page->fb_page_admin_at;
					$fb_page_id = $page->fb_page_id;
					
					//get PAGE ACCESS TOKEN
					$info = array("access_token" => $admin_access_token);
					$fb_request = $facebook->get('/me/accounts', $admin_access_token);
					Log::debug("Facebook response for getting access token: ".print_r($fb_request->getBody(),true));
					//scorrere l'array finche non trovi la pagina giusta
					//$pages_array = $fb_request['data'];
					$pages_array = $fb_request->getGraphEdge();
					$page_access_token = null;
					foreach ($pages_array as $pagefb)
					{
						if ($pagefb['id'] == $page->fb_page_id)
						{
							$page_access_token = $pagefb['access_token'];
							break;
						}
					}
					
					Log::debug("Facebook page access token: ".$page_access_token);
					
					$message = $photo->name;
					
					Log::debug("Message for page post: ".$message);
					
					
					$fileToUpload = public_path()."/photos/".$photo->filename;
					
					//upload photo to facebook page
					$data = array(
						"source" => $facebook->fileToUpload($fileToUpload),
						"message" => $message,
						"access_token" => $page_access_token);
					
					if ($page->fb_no_story == "YES")
					{
						Log::debug("Contest with unpublished posts");
						$new_data = array("no_story" => "true");
						$data = array_merge($data, $new_data);
						
					} 
					
					if ( $page->fb_id_album == null || $page->fb_id_album == '') {
						
						$album_details = array(
							'name' => $contest->name,
						);
						
						$create_album = $facebook->post('/me/albums', $album_details, $page_access_token);
						
						$album_uid = $create_album['id'];
						
						$page->fb_id_album = $album_uid;
						$page->save();

					}
					
					//$response = $facebook->api("/v2.3/".$page->fb_page_id."/photos", "POST", $data);
					$response = $facebook->post("/".$page->fb_id_album."/photos", $data, $page_access_token);
					$graphNode = $response->getGraphNode();

					Log::debug("Facebook response for new Photo Upload: ".print_r($graphNode,true));
					$id_photo = $graphNode['id'];
					$id_post_photo = null;
					if (array_key_exists("post_id", $graphNode)) 
					{
						$id_post_photo = $graphNode['post_id'];
					}
					else
					{
						$id_post_photo = $page->fb_page_id."_".$id_photo;
					}
					
					
					//Creare record nella pivot table tra photo e fbpage array("receiver_id"=>$u2->id, "status"=>1)
					$photo->fbPages()->attach($page->id, array('fb_idphoto'=>$id_photo, 'fb_idpost'=>$id_post_photo)); 

					
					try {
				
						$data = array("access_token" => $page_access_token);
						$responsePhoto = $facebook->get("/".$id_photo, $page_access_token);
						$graphNodePhoto = $responsePhoto->getGraphNode();
						Log::debug("Facebook response for Photo Data: ".print_r($graphNodePhoto,true));
						$picture_post = $graphNodePhoto['picture'];
						
						
						if ($page->custom_logo == '' || is_null($page->custom_logo))
						{
							$source_fb = $graphNodePhoto['source'];
						}
						
						$photo->fb_source_url = $source_fb;				
						$photo->save();
					
					}
					catch (Exception $ex)
					{
						Log::error($ex);
					}
					
					
				}
				

				$photo->uploadOnFacebookDone = '1';					
				$photo->save();

				




















				
			} 
			catch (Exception $ex) 
			{
				Log::error($ex);
			}
			
			
			
		}
		
	}
	
	
	private static function downloadFile ($url, $path) {
		
		$newfname = $path;
		$file = fopen ($url, "rb");
		if ($file) {
			$newf = fopen ($newfname, "wb");

			if ($newf)
			while(!feof($file)) {
			  fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
			}
		}

		if ($file) {
			fclose($file);
		}

		if ($newf) {
			fclose($newf);
		}
		
	}
	
	
}

?>