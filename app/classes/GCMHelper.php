<?php

use Symfony\Component\Filesystem\Exception\IOException;

class GCMHelper
{
	public static function schedule($video)
	{
		if (isset($video->gcm_registration_id) && $video->gcm_registration_id)
		{
			Log::debug("About to schedule gcmNotify, video id: ".$video->id);
			Scheduler::schedule("gcmNotify;".$video->id);
			Log::debug("gcmNotify;".$video->id." scheduled");
		}
		else
		{
			Log::debug("video ".$video->id." does not have gcm_registration_id. Skipping gcmNotify");
		}
	}

	public static function notify($videoId)
	{
		$config = Config::get('gcm');
		Log::debug("Starting GCMHelper::notify(".$videoId.")");
		

		
		try
		{	
			$video = Video::find($videoId);
			$user = User::find($video->user_id);
	
			if (strpos($user->token,'Android_') !== false)
			{
				//stringa presente
				$deviceToken = str_replace("Android_", "", $user->token);
				Log::debug("Device token to be used: ".$deviceToken);
			}
			else if (strpos($user->token,'iOS_') !== false)
			{
				$deviceToken = str_replace("iOS_", "", $user->token);
				Log::debug("Device token to be used: ".$deviceToken);
			}
		
			
			$message = null;
			//$post = json_encode(array("registration_ids" => array($video->gcm_registration_id), "data" => array("video_id" => $video->id, "video_approval_status" => $video->approvalStep->name, "video_title" => $video->name)));
			$post = json_encode(array("registration_ids" => array($deviceToken), "data" => array("video_id" => $video->id, "video_approval_status" => $video->approvalStep->name, "video_title" => $video->name)));
			$ch = CommonHelper::curl($config["gcm_post_url"]);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: key=".$config["gcm_server_key"], "Content-Type: application/json"));
			$json = curl_exec($ch);
			if(curl_errno($ch))
			{
				Log::error("GCM notification curl error: ".curl_error($ch).", info: ".print_r(curl_getinfo($ch),true));
				curl_close($ch);
				throw new \IOException("Error while accessing GCM");
			}
			curl_close($ch);
	    	$decoded_json = json_decode($json);
	    	Log::debug("GCM notify returned: ".json_encode($decoded_json,JSON_PRETTY_PRINT));
			
			if ($decoded_json->success == 0)
			{
				Log::error("GCM rejected POST notify (zero success). registration_id: ".$video->gcm_registration_id);
				throw new IOException("GCM rejected POST notify (zero success). registration_id: ".$video->gcm_registration_id);
			}
			
			Log::info("GCM registration_id: ".$video->gcm_registration_id." successfully notified, video id: ".$video->id);
		}
		catch(\Exception $e)
		{
			if ($config["should_retry"]) {
				Log::error($e);
				throw $e;
			} else {
				Log::warning($e);
			}
		}
	}
	
	public static function notifyPhoto($photoId)
	{
		$config = Config::get('gcm');
		Log::debug("Starting GCMHelper::notify(".$photoId.")");
		
		try
		{	
			$photo = Photo::find($photoId);
			$user = User::find($photo->user_id);
			
			if (strpos($user->token,'Android_') !== false)
			{
				//stringa presente
				$deviceToken = str_replace("Android_", "", $user->token);
				Log::debug("Device token to be used: ".$deviceToken);
			}
			else if (strpos($user->token,'iOS_') !== false)
			{
				$deviceToken = str_replace("iOS_", "", $user->token);
				Log::debug("Device token to be used: ".$deviceToken);
			}
			
			
			$message = null;
			//$post = json_encode(array("registration_ids" => array($video->gcm_registration_id), "data" => array("video_id" => $video->id, "video_approval_status" => $video->approvalStep->name, "video_title" => $video->name)));
			$post = json_encode(array("registration_ids" => array($deviceToken), "data" => array("photo_id" => $photo->id, "video_approval_status" => $photo->approvalStep->name, "photo_title" => $photo->name)));
			$ch = CommonHelper::curl($config["gcm_post_url"]);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: key=".$config["gcm_server_key"], "Content-Type: application/json"));
			$json = curl_exec($ch);
			if(curl_errno($ch))
			{
				Log::error("GCM notification curl error: ".curl_error($ch).", info: ".print_r(curl_getinfo($ch),true));
				curl_close($ch);
				throw new \IOException("Error while accessing GCM");
			}
			curl_close($ch);
	    	$decoded_json = json_decode($json);
	    	Log::debug("GCM notify returned: ".json_encode($decoded_json,JSON_PRETTY_PRINT));
			
			if ($decoded_json->success == 0)
			{
				Log::error("GCM rejected POST notify (zero success). registration_id: ".$deviceToken);
				throw new IOException("GCM rejected POST notify (zero success). registration_id: ".$deviceToken);
			}
			
			Log::info("GCM registration_id: ".$user->token." successfully notified, photo id: ".$photo->id);
		}
		catch(\Exception $e)
		{
			if ($config["should_retry"]) {
				Log::error($e);
				throw $e;
			} else {
				Log::warning($e);
			}
		}
	}
}
	