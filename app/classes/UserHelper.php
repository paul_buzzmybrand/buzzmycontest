<?php

use Illuminate\Routing\UrlGenerator;

/**
 * Common API class for managing users (simple users)
 */
class UserHelper
{
    /**
     * Method handling signing up with Twitter. It is called when user signs up on Twitter (both in widget and simple user functionality)
     *  
     * @param unknown $data
     */
    public static function signUpWithTwitter($data)
    {
        
    }
    
    
    /**
     * Registers a new simple user in the database after registration process is completed
     * 
     * @return new user id
     */
    public static function registerUser($data)
    {
    	$accessToken = CommonHelper::needAnyAccessToken();
    	Log::debug("Access token to be used: ".$accessToken);
    	$url = URL::to("/v1/users");
    	Log::debug("Route to controller: ".$url);
    	
    	$file_name = realpath($data["images"]);
		$post = array('username' => $data["username"], "password" => $data["password"], "email" => $data["email"],"file"=>new CurlFile($file_name));
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    	$json = curl_exec($ch);
    	curl_close($ch);
    	$decoded_json = json_decode($json);
    	Log::debug($url." returned: ".json_encode($decoded_json,JSON_PRETTY_PRINT));
		return $decoded_json->id;
    }

    /**
     * Returns user based on access token stored in Session (logged in)
     *
     * @param unknown $accessToken
     */
    public static function getLoggedIn()
    {
    	Log::debug("getLoggedIn(). Checking if object is already put in request");
    	$decoded_json = Request::instance()->attributes->get("website.user_in_request");
    	if (!$decoded_json)
    	{
    		Log::debug("website.user_in_request not found. Getting via API call");
	    	$accessToken = Session::get("website.access_token");
	    	Log::debug("getByAccessToken(): Access token to be used: ".$accessToken);
	    	$url = "/v1/users/index";
	    	Log::debug("Route to controller: ".$url);
	    	$ch = CommonHelper::curl($url);
	    	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
	    	$json = curl_exec($ch);
	    	curl_close($ch);
	    	$decoded_json = json_decode($json);
	    	Log::debug($url." returned: ".json_encode($decoded_json,JSON_PRETTY_PRINT));
	    	Request::instance()->attributes->set("website.user_in_request",$decoded_json);
    	}
    	else
    	{
    		Log::debug("website.user_in_request found. Returning it");
    	}
    	return $decoded_json;
    }
	
	public static function getAdminUserLoggedIn()
	{
		Log::debug("getAdminUserLoggedIn(). Checking if object is already put in request");
    	$adminUsername = Session::get("username");
    	if (!$adminUsername)
    	{
    		/*Log::debug("admin.user_in_request not found. Getting via API call");
	    	$accessToken = Session::get("admin.access_token");
	    	Log::debug("getByAccessToken(): Access token to be used: ".$accessToken);
	    	$url = "/v1/users/index";
	    	Log::debug("Route to controller: ".$url);
	    	$ch = CommonHelper::curl($url);
	    	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
	    	$json = curl_exec($ch);
	    	curl_close($ch);
	    	$decoded_json = json_decode($json);
	    	Log::debug($url." returned: ".json_encode($decoded_json,JSON_PRETTY_PRINT));
	    	Request::instance()->attributes->set("admin.user_in_request",$decoded_json);
			*/
    	}
    	else
    	{
    		Log::debug("username found. Returning it");
    	}
    	return $adminUsername;
	}
        
    /**
     * Returns user based on access token
     * 
     * @param unknown $accessToken
     */
    public static function getByAccessToken($accessToken)
    {
    	Log::debug("getByAccessToken(): Access token to be used: ".$accessToken);
    	$url = "/v1/users/getUserInformation";
    	Log::debug("Route to controller: ".$url);
    	$ch = CommonHelper::curl($url);
    	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
    	$json = curl_exec($ch);
    	curl_close($ch);
    	$decoded_json = json_decode($json);
    	Log::debug($url." returned: ".json_encode($decoded_json,JSON_PRETTY_PRINT));
    	 
    	return $decoded_json;    	
    }
    
    /**
     * Method called for checking logging as simple user (also should put User in session when logged on, see Auth::login)
     * 
     *  if username or password is wrong must return null
     *  if user is logged must return User model object
     * 
     */
    public static function loginUser($userData)
    {
		$accessToken = CommonHelper::getAccessToken($userData);
		if ($accessToken)
		{
			Log::debug("Storing access token in session");
			Session::put("website.access_token",$accessToken);
			
			//@TODO - API should return additional user id
			$user = User::find(UserHelper::getByAccessToken($accessToken)->id);
			Auth::login($user);
			return $user;
		}
		else
		{
			Log::warning("User (username,password) not found: ".print_r($userData,true));
			return null;
		}
    }
    
    /**
     * This method is responsible for updating a photo.
     * 
     * @param $image filename of the photo image
     * @param $userId simple user ID
     */
    public static function updatePhoto($image, $userId)
    {
    	$accessToken = Session::get("website.access_token");
    	Log::debug("Access token to be used: ".$accessToken);
    	$url = "/v1/users/update";
    	Log::debug("Route to controller: ".$url);
    	
    	$file_name = realpath($image);
		$post = array('type' => 'picture','picture'=>new CurlFile($file_name));
		$ch = CommonHelper::curl($url);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
		$result=curl_exec($ch);
		curl_close($ch);
    	Log::debug("update photo reply: ".$result);
    }
    
    /**
     * This method deletes simple user account
     * 
     * @param $userId id of the user to be deleted
     */
    public static function deleteAccount($userId)
    {
    	$accessToken = Session::get("website.access_token");
    	Log::debug("Access token to be used: ".$accessToken);
    	$url = "/v1/users/delete";
    	Log::debug("Route to controller: ".$url);
    	 
    	$ch = CommonHelper::curl($url);
    	curl_setopt($ch, CURLOPT_POST,1);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, array());
    	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
    	$result=curl_exec($ch);
    	curl_close($ch);
    	Log::debug($url." reply: ".$result);        
    }
    
    /**
     * This method is called in Simple User/All Contests section to return all contests
     * 
     * @return array of contests
     */
    public static function getAllContest()
    {
    	$url = "/v1/contests?External_contest=false";
    	Log::debug("Getting all contests. Route to controller: ".$url);
    	$ch = CommonHelper::curlWithAnyToken($url);
    	$json = curl_exec($ch);
    	curl_close($ch);
    	Log::debug("/v1/contests returned: ".$json);
    	
    	$decoded_json = json_decode($json);
    	return $decoded_json;
    }
    
    /**
    * It called in Simple User/My Contests section
    * 
    * @param $userId id of the user to return contests
    * @return array of user contests
    */
    public static function getUserContests($userId)
    {
    	$accessToken = Session::get("website.access_token");
    	Log::debug("Access token to be used: ".$accessToken);
    	$url = "/v1/eventList?External_contest=false";
    	Log::debug("Getting all contests. Route to controller: ".$url);
    	$ch = CommonHelper::curl($url);
    	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
    	$json = curl_exec($ch);
    	curl_close($ch);
    	$decoded_json = json_decode($json);
    	Log::debug($url." returned: ".json_encode($decoded_json,JSON_PRETTY_PRINT));
    	return $decoded_json;
    }
    
    /**
    * This function returns featured contests of the given user in section Simple User/Featured Contest
    * 
    * @return collection of contests
    */
    public static function getFeaturedContests()
    {
        //data for example
        $contests = Contest::all();
        return $contests;
    }

    /**
     * This is a specialized function that should be used for sending e-mails. It is called in several places where additional e-mail confirmations
     * are sent. It should implement safe e-mail queue. Sending e-mail just by SMTP is not reliable, there should be additional SMTP (i.e. sendmail)
     * queue mechanism implemented
     * 
     * @param $data array with various additional information regards e-mail. Details regarding e-mail should be described by the system architect
     */
    public static function sendEmail($data)
    {
        
    }
    
    /**
     * Authenticates the user with facebook profile
     */
    public static function addUserWithFacebook($data)
    {
        //example
        $user = new User;
        $user->username = $data['name'];
        $user->email = $data['email'];
        //image
        //$user->image = 'https://graph.facebook.com/'.$facebook_account['username'].'/picture?type=large';
        $user->save();

        $userId = $user->id;
        $facebookProfile = new FacebookProfile();
        $facebookProfile->username = $data['email'];
        $facebookProfile->uid = $data['id'];
        $facebookProfile->user_id = $userId;
        $facebookProfile->save();
        Log::info('Save new facebook profile to database: '.$data['id'].', userId: '.$userId.').');
        Auth::login($user);
        Log::info('Login to system with facebook(facebook_account: '.$data['id'].', userId: '.$userId.').');
    }
    
    public static function joinTheContest($id)
    {
    	$accessToken = Session::get("website.access_token");
    	Log::debug("Access token to be used: ".$accessToken);
    	$url = URL::to("/v1/contests/join",array($id));
    	Log::debug("Route to controller: ".$url);
    	
    	$ch = curl_init($url);
    	curl_setopt($ch, CURLOPT_POST,1);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, array());
    	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$accessToken));
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    	$json=curl_exec($ch);
    	curl_close($ch);
    	$decodedJson = json_decode($json);
    	Log::debug("Join the contest reply: ".json_encode($decodedJson,JSON_PRETTY_PRINT));
    	return $decodedJson;
    }
    
}
?>
