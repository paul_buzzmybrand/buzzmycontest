<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/
Artisan::add(new UpdateSocialScoresCommand);
Artisan::add(new InsertWatermarkCommand);
Artisan::add(new RunSchedulerCommand);
Artisan::add(new RunApprovalCommand);
Artisan::add(new RunUpdateTrendsCommand);
Artisan::add(new RunUpdateMediaFromInstagram);
Artisan::add(new RunMakeTabCommand);
Artisan::add(new RunMigrationInstagramCommand);
Artisan::add(new RunUpdateLocationsCommand);
Artisan::add(new RunScoreCommand);
Artisan::add(new InstantWinNotificationCommand);
Artisan::add(new emailNotification);