<?php

class ContestVideoTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('contest_video')->truncate();

		$contest_video = array(
                    array(
                        'video_id'=>1,
                        'contest_id'=>1,
                        ),
                    array(
                        'video_id'=>1,
                        'contest_id'=>2,
                        ),
		);

		// Uncomment the below to run the seeder
		DB::table('contest_video')->insert($contest_video);
	}

}
