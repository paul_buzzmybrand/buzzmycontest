<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('users')->truncate();

		$users = array(
                    'first_name' => 'Khalid Bashir',
                    'last_name' => 'Bajwa',
                    'username' => 'khalid',
                    'email' => 'khalid.bajwa@galaxonic.com',
                    'password' => Hash::make('kha123lid'),
		);

		// Uncomment the below to run the seeder
                DB::table('users')->insert($users);
	}

}
