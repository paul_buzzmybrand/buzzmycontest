<?php

class Oauth_clientsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		 //DB::table('oauth_clients')->truncate();

		$oauth_clients = array(
                    array(
                        'id'=>'IPhone',
                        'secret'=>'12345',
                        'name'=>'Test IOS Client',
                        ),
                    array(
                        'id'=>'Droid',
                        'secret'=>'12345',
                        'name'=>'Test Android Client',
                        ),

		);

		// Uncomment the below to run the seeder
		 DB::table('oauth_clients')->insert($oauth_clients);
	}

}
