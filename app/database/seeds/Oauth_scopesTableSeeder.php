<?php

class Oauth_scopesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		 //DB::table('oauth_scopes')->truncate();

		$oauth_scopes = array(
                        array(
                            'scope'=>'IOS',
                            'name'=>'IOS',
                            'description'=>'Scope for IOS Devices',
                            ),
                        array(
                            'scope'=>'Android',
                            'name'=>'Android',
                            'description'=>'Scope for Android Devices',
                            ),
                        array(
                            'scope'=>'Desktop',
                            'name'=>'Desktop',
                            'description'=>'Scope for Desktop Devices',
                            ),
                        array(
                            'scope'=>'Web',
                            'name'=>'Web',
                            'description'=>'Scope for Web Devices',
                            ),

		);

		// Uncomment the below to run the seeder
		 DB::table('oauth_scopes')->insert($oauth_scopes);
	}

}
