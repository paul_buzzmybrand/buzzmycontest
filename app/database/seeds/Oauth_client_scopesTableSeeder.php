<?php

class Oauth_client_scopesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('oauth_client_scopes')->truncate();

		$oauth_client_scopes = array(
                        array(
                            'client_id'=>'IPhone',
                            'scope_id'=>'1',
                            ),
                        array(
                            'client_id'=>'Droid',
                            'scope_id'=>'2',
                            ),

		);

		// Uncomment the below to run the seeder
		 DB::table('oauth_client_scopes')->insert($oauth_client_scopes);
	}

}
