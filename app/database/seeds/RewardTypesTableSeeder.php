<?php

class RewardTypesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('reward_types')->truncate();

		$reward_types = array(
                    array('reward_type'=>'cash'),
                    array('reward_type'=>'product'),
                    array('reward_type'=>'trip'),
                    array('reward_type'=>'other')
		);

		// Uncomment the below to run the seeder
		 DB::table('reward_types')->insert($reward_types);
	}

}
