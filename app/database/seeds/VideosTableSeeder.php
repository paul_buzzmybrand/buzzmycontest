<?php

class VideosTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('videos')->truncate();

		$videos = array(
                     'name'=>'JMTV demo',
                        'duration' => 27.433,
                        'image' => 'ea10e114f40c97032d6388f6fad33f78.jpg',
                        'filename'=> '03a5afe5df7bc5cbb251436b8a5ec793.mp4',
                        'score' => '5',
                        'user_id'=> 1,
                        'location'=>'Singapore'
		);

		// Uncomment the below to run the seeder
		 DB::table('videos')->insert($videos);
	}

}
