<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsersTableSeeder');
		
		$this->call('Oauth_clientsTableSeeder');
		$this->call('Oauth_scopesTableSeeder');
		$this->call('Oauth_client_scopesTableSeeder');
		$this->call('ContestsTableSeeder');
		$this->call('CompanysTableSeeder');
		$this->call('RewardTypesTableSeeder');
		$this->call('VideosTableSeeder');
		$this->call('ContestVideoTableSeeder');
	}

}