<?php

class CompanysTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		 DB::table('companys')->truncate();

		$companys = array(
                    array(
                         'name'=>'JMTV',
                        'description'=>'JoinMeThere.TV',
                        'image'=>'',
                        'address'=>'Singapore',
                        'contact_name'=>'Max',
                        'email'=>'info@joinmethere.tv',
                        'website'=>'www.joinmethere.tv',
                        'phone'=>'1234',
                        'fax'=>'1234'
                    ),
                    array(
                         'name'=>'Galaxonic',
                        'description'=>'Accelerate your vision',
                        'image'=>'',
                        'address'=>'Germany',
                        'contact_name'=>'ML',
                        'email'=>'info@galaxonic.com',
                        'website'=>'www.galaxonic.com',
                        'phone'=>'1234',
                        'fax'=>'1234'
                    ),
                    array(
                         'name'=>'Chinatown',
                        'description'=>'China Town',
                        'image'=>'',
                        'address'=>'Singapore',
                        'contact_name'=>'ML',
                        'email'=>'info@chinatownfestivals.sg',
                        'website'=>'www.chinatownfestivals.sg',
                        'phone'=>'1234',
                        'fax'=>'1234'
                    )

		);

		// Uncomment the below to run the seeder
		 DB::table('companys')->insert($companys);
	}

}
