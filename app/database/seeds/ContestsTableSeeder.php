<?php

class ContestsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		 DB::table('contests')->truncate();
                       
		$contests = array(
                    array(
                        'name'=> 'Valentine\'s Day at the Gardens',
                        'company_id'=>'1',
                        'start_time' => "2014-02-14 00:00:00",
                        'end_time' => "2014-02-14 23:59:59",
                        'location' => 'Singapore',
                        //'description' => 'Video contest',
                        'rules' => 'Open for all',
                        'is_contest' => 1,
                        'reward1_type' => 'cash',
                        'reward1_desc' => 'SGD 50',
                        'reward2_type' => '',
                        'reward2_desc' => '',
                        'reward3_type' => '',
                        'reward3_desc' => '',
                        'judges' => 'JMTV Score (The + you share, the + the chances to win)',
                        'concept' => '-	Come to the Gardens by the Bay and shoot the best salsa video of the evening! 
                                    Address: Supertree Grove, 18 Marina Gardens Drive Gardens by the Bay, Singapore 018953
                                    Tel: 6420 6848
                                    -	Minimum 5 entries.
                                    -	1 entry for each user.',
                        'image'=>'VALENTINES_DAY.jpg',
                        
                        'contact_name'=>'Max',
                        'email'=>'info@joinmethere.tv',
                        'website'=>'www.joinmethere.tv',
                        'phone'=>'1234',
                        'fax'=>'1234'
                    ),
                    array(
                        'name'=> 'CNY Celebrations',
                        'company_id'=>'3',
                        'start_time' => "2014-01-11 00:00:00",
                        'end_time' => "2014-02-28 23:59:59",
                        'location' => 'Singapore',
                        //'description' => 'Video contest',
                        'rules' => 'Open for all',
                        'is_contest' => 1,
                        'reward1_type' => 'cash',
                        'reward1_desc' => 'SGD 300',
                        'reward2_type' => 'cash',
                        'reward2_desc' => 'SGD 140',
                        'reward3_type' => 'cash',
                        'reward3_desc' => 'SGD 60',
                        'judges' => 'JMTV Score (The + you share, the + the chances to win)',
                        'concept' => 'Usher in the prosperous Year of the Horse with 88 illuminated lantern scuptures of horses galloping towards prosperity and countless gold coins suspended along the street. Shoot the video of the event and share it with your friend! More info @ chinatownfestivals.sg
                                     Minimum 20 entries.
                                    1 entry for each user.',
                        'image'=>'cny_2014.png',
                        
                        'contact_name'=>'Mustermann',
                        'email'=>'info@chinatownfestivals.sg',
                        'website'=>'chinatownfestivals.sg',
                        'phone'=>'1234',
                        'fax'=>'1234'
                    ),
                    array(
                        'name'=> 'LITTLE INDIA INSPIRATIONS',
                        'company_id'=>'2',
                        'start_time' => "2014-01-11 00:00:00",
                        'end_time' => "2014-02-28 23:59:50",
                        'location' => 'Beijing',
                        //'description' => 'Video contest',
                        'rules' => 'Open for all',
                        'is_contest' => 1,
                        'reward1_type' => 'cash',
                        'reward1_desc' => 'SGD 50',
                        'reward2_type' => '',
                        'reward2_desc' => '',
                        'reward3_type' => '',
                        'reward3_desc' => '',
                        'judges' => 'JMTV Score (The + you share, the + the chances to win)',
                        'concept' => 'Come to little India and shoot a video saying why this traditional area of Singapore inspires you a lot.
                                    Minimum 5 entries.
                                    1 entry for each user.',
                        'image'=>'Little_India.jpg',
                                                
                        'contact_name'=>'ML',
                        'email'=>'info@galaxonic.com',
                        'website'=>'www.galaxonic.com',
                        'phone'=>'1234',
                        'fax'=>'1234'
                    ),
                    array(
                        'name'=> 'SPUNGRY, EAT & SHOOT!',
                        'company_id'=>'2',
                        'start_time' => "2014-01-03 00:00:00",
                        'end_time' => "2014-02-15 23:59:50",
                        'location' => 'Taiwan',
                        //'description' => 'Video contest',
                        'rules' => 'Open for all',
                        'is_contest' => 1,
                        'reward1_type' => 'cash',
                        'reward1_desc' => 'SGD 80',
                        'reward2_type' => '',
                        'reward2_desc' => '',
                        'reward3_type' => '',
                        'reward3_desc' => '',
                        'judges' => 'JMTV Score (The + you share, the + the chances to win)',
                        'concept' => 'Have the chance to win a voucher worth 80 SGD shooting a video while in one of Spungry Restaurant, showing your favorite dish and saying “Hungry? Spungry!”
                                    Minimum 10 entries.
                                    1 entry for each user.',
                        'image'=>'Spungry.png',
                                                
                        'contact_name'=>'ML',
                        'email'=>'info@galaxonic.com',
                        'website'=>'www.galaxonic.com',
                        'phone'=>'1234',
                        'fax'=>'1234'
                    ),
                    array(
                        'name'=> 'AND SINGLISH FOR ALL',
                        'company_id'=>'1',
                        'start_time' => "2014-01-11 00:00:00",
                        'end_time' => "2014-02-28 23:59:50",
                        'location' => 'Singapore',
                        //'description' => 'Video contest',
                        'rules' => 'Open for all',
                        'is_contest' => 1,
                        'reward1_type' => 'cash',
                        'reward1_desc' => 'SGD 60',
                        'reward2_type' => 'cash',
                        'reward2_desc' => 'SGD 30',
                        'reward3_type' => 'cash',
                        'reward3_desc' => 'SGD 10',
                        'judges' => 'JMTV Score (The + you share, the + the chances to win)',
                        'concept' => 'Are you a real Singaporean? Show your skills of singlish in this funny video contest. Shoot and express yourself at your best, you have the chance to win prizes! 
                                    Minimum 20 entries.
                                    1 entry for each user.',
                        'image'=>'Singlish.png',
                                                
                        'contact_name'=>'ML',
                        'email'=>'info@galaxonic.com',
                        'website'=>'www.galaxonic.com',
                        'phone'=>'1234',
                        'fax'=>'1234'
                    )

		);

		// Uncomment the below to run the seeder
		 DB::table('contests')->insert($contests);
	}

}
