<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companys', function(Blueprint $table) {
			$table->increments('id');
                        
                        $table->string('name')->unique();
                        $table->text('description');
                        $table->string('image');
                        $table->string('address');
                        
                        $table->string('contact_name');
                        $table->string('email');
                        $table->string('website');
                        $table->string('phone');
                        $table->string('fax');
                        $table->string('facebook_link');
                        $table->string('twitter_link');
			
                        $table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companys');
	}

}
