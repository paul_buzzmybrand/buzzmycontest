<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function(Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->integer('user_id')->unsigned()->references('id')->on('users');
			$table->integer('card_id')->unsigned()->references('id')->on('credit_cards');
			$table->decimal('amount', 6, 2);
			$table->text('transaction_provider_code');
			$table->boolean('success')->default(true);
			$table->boolean('is_processed')->default(false);
			$table->string('response', 1024)->default('{}'); // JSON
            $table->string('description')->nullable();
            $table->integer('invoice_id')->unsigned()->nullable()->references('id')->on('invoices');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}
