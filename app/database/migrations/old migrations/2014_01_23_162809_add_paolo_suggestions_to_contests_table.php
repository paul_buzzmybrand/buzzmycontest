<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddPaoloSuggestionsToContestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contests', function(Blueprint $table) {
			$table->boolean('entries');
			$table->boolean('success_story');
			$table->boolean('approved');
			$table->string('video');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contests', function(Blueprint $table) {
			$table->dropColumn('entries');
			$table->dropColumn('success_story');
			$table->dropColumn('approved');
			$table->dropColumn('video');
		});
	}

}
