<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVideosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('videos', function(Blueprint $table) {
			$table->increments('id');
                        
                        $table->string('name');
                        $table->float('duration');
                        $table->string('image');
                        $table->string('filename')->unique();
                        $table->float('score');
                        $table->integer('user_id');
                        //$table->string('whattag');
                        
                        $table->string('location');
			
                        $table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('videos');
	}

}
