<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExpandServiceFeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('service_fees', function(Blueprint $table) {
			$table->decimal('cap', 6, 2)->nullable();
			$table->boolean('assign_on_registration')->default(false);
		});

		Schema::table('user_credits', function(Blueprint $table) {
			$table->decimal('cap', 6, 2)->nullable();;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('service_fees', function(Blueprint $table) {
			$table->dropColumn(['cap', 'assign_on_registration']);
		});

		Schema::table('user_credits', function(Blueprint $table) {
			$table->dropColumn('cap');
		});
	}

}
