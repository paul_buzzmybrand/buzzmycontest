<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendServiceFeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('service_fees', function(Blueprint $table) {
			$table->boolean('is_sellable')->default(true);
			$table->string('name')->default('');
		});

		Schema::table('user_credits', function(Blueprint $table) {
			$table->dropForeign('fk_user_credits_contest_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('service_fees', function(Blueprint $table) {
			$table->dropColumn(['is_sellable', 'name']);
		});

		Schema::table('user_credits', function(Blueprint $table) {
			$table->foreignKey('contest_id')->references('id')->on('contests');
		});
	}

}
