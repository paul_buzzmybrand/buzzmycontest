<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contests', function(Blueprint $table) {
			$table->increments('id');
			
                        
                        $table->string('name');
                        $table->integer('company_id');
                        $table->dateTime('start_time');
                        $table->dateTime('end_time');
                        $table->string('location');
                        $table->text('rules');
                        $table->boolean('is_contest');
                        $table->text('reward1_desc');
                        $table->text('reward1_type');
                        $table->text('reward2_desc');
                        $table->text ('reward2_type');
                        $table->text('reward3_desc');
                        $table->text('reward3_type');
                        $table->text('judges');
                        $table->text('concept');
                        
                        $table->string('image');
                        
                        $table->string('contact_name');
                        $table->string('email');
                        $table->string('website');
                        $table->string('phone');
                        $table->string('fax');
                        
                        $table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contests');
	}

}
