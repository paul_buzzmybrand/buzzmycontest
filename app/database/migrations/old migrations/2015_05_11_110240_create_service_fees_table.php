<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceFeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_fees', function(Blueprint $table) {
            $table->increments('id')->unsigned();
			$table->integer('service_type')->unsigned();
			$table->integer('credits');
			$table->decimal('fixed_fee', 6, 2);
			$table->decimal('varying_fee', 6, 2);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('service_fees');
	}

}
