<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCreditsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_credits', function(Blueprint $table) {
            $table->increments('id')->unsigned();
			$table->integer('user_id')->unsigned()->references('id')->on('users');
			$table->integer('fee_id')->unsigned()->references('id')->on('service_fees');
			$table->integer('service_type')->unsigned();
			$table->integer('credits');
			$table->decimal('fixed_fee', 6, 2);
			$table->decimal('varying_fee', 6, 2);
			$table->boolean('is_available')->default(true);
			$table->datetime('used_at')->nullable();
			$table->integer('contest_id')->unsigned()->references('id')->on('contests');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_credits');
	}

}
