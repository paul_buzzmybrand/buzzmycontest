<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class FacebookProfile extends Eloquent {

    protected $table = 'facebook_profiles';
    protected $softDelete = true;
    
}
