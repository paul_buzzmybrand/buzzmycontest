<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Objective extends Eloquent {

    protected $table = 'objectives';
    //protected $softDelete = true;

	public static $requiringRegistration = [4, 6];
	
	
	public function contests(){
        
		return $this->belongsToMany('Contest', 'contest_objective', 'objective_id', 'contest_id')->withPivot('name', 'surname', 'birthdate', 'email', 'phone', 'city', 'cap');
    
	}
    
}