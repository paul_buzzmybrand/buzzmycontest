<?php 

class Invoice extends Eloquent {

	protected $fillable = ['user_id', 'customer_id', 'contest_id', 'is_paid', 'amount', 'total_te', 'total_ti', 'vat', 'description', 'currency', 'coupon', 'coupon_amount'];
    protected $dates = ['created_at', 'updated_at'];
    protected $appends = ['formatted_id', 'ISO_payment_date', 'pdf_url', 'contest_name'];
    protected $hidden = ['created_at', 'updated_at'];

    /** ATTRIBUTES
        $table->integer("user_id")->unsigned();
        $table->boolean("is_paid")->default(true);
        $table->float("total_te");
        $table->float("total_ti");
        $table->float("vat");
        $table->text('description');
    **/
    
        public static function generate($user, $customer_id, $contest_id, $amount, $currency, $description, $coupon, $coupon_amount, $contest_amount, $notify = false) 
        {
            //customer_id, amount, currency, description, coupon, user_id
            $invoice = static::create([
                'user_id' => $user->id,
                'customer_id' => $customer_id,
                'contest_id' => $contest_id,
                'amount' => $amount,
                'currency' => $currency,
                'coupon' => $coupon,
                'coupon_amount' => $coupon_amount,
                'description' => $description,
				'contest_amount' => $contest_amount,
                'is_paid' => '0'
            ]); 
            
            //if($notify)
            //    $this->sendNotification();
    
            return $invoice;
        }

    /*
    public static function generate($user, $total_ti, $description, $notify = false)
    {
        $vat = Config::get('ecommerce.vat');
        $total_te = round($total_ti / (1 + $vat / 100), Config::get('ecommerce.decimal_places'));
        $invoice = static::create([
            'user_id' => $user->id,
            'total_ti' => $total_ti,
            'total_te' => $total_te,
            'description' => $description,
            'vat' => $vat
        ]);

        if($notify)
            $this->sendNotification();

        return $invoice;
    }
    */

    public function getFormattedIdAttribute()
    {
        return $this->getFormattedId();
    }

    public function getISOPaymentDateAttribute()
    {
        return $this->created_at->format('d/m/Y');
    }

    public function getPdfUrlAttribute()
    {
        return url('invoices/' . $this->id);
    }

    public function getFormattedId()
    {
        return str_pad($this->id, Config::get('ecommerce.invoice_npadding'), '0', STR_PAD_LEFT) . '/' . Config::get('ecommerce.invoice_prefix');
    }

    public function getContestNameAttribute()
    {
        return strlen($this->contest['name']) > 0 ? $this->contest['name'] : '-';
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function contest()
    {
        return $this->belongsTo('Contest');
    }

    public function transactions()
    {
        return $this->hasMany('Transaction')->where('success', 1);
    }

    public function sendNotification()
    {
        // TODO: create the template
        return;

        $user = $this->user;
        $invoice = $this;
        Mail::send("emails.invoice_notification", 
            ['invoice' => $this, 'user' => $user], function($message) use ($user, $invoice) {
                $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                $message->to($user->email)->subject('Scarica la fattura n. ' . $invoice->getFormattedId());
        });
    }

    public function getPdf()
    {
        $pdf = PDF::loadView('invoices.pdf', ['invoice' => $this]);
        return $pdf;
    }
	
	public function card()
	{
		if ((isset($this->customer_id)) && (!empty($this->customer_id))) {
			return CreditCard::withTrashed()->where('customer_id', $this->customer_id)->first();
		} else {
			return 0;
		}
		
		
	}
	
	public function plan()
	{
		return ServiceFee::where('name', $this->description)->first();
	}

}