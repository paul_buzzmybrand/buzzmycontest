<?php

class Contest extends Eloquent {
    protected $guarded = array();
    protected $softDelete = true;
    protected $table = "contests";

    const STATUS_DRAFT = 1;
    const STATUS_ACTIVE = 2;

    const ENTRIES_ONE_PER_USER = 1;
    const ENTRIES_UNLIMITED = 2;

    const SERVICE_ALL = 0;
    const SERVICE_INSTANT = 1;
    const SERVICE_STANDARD = 2;

    public static $serviceTypeVerbose = [
        0 => '', // No specification = allows all contest types
        1 => 'BUZZ',
        2 => 'SUPERBUZZ'
    ];

    public static $rules = array();

    public function company()
    {
        return $this->belongsTo('Company');
    }

	public function fbPages()
    {
        return $this->belongsToMany('fbPage', 'fbpage_contest', 'contest_id', 'fbpage_id')->withPivot('impressions', 'reach', 'new_likes', 'engaged_users');
    }

	public function location()
	{
		return $this->belongsTo('Location');
	}
    public function users()
    {
        return $this->belongsToMany('User')->withPivot('login_website', 'login_mobile', 'login_instagram', 'instantwin_done');
    }

	public function registeredUsers()
	{
		//$data = $this->belongsToMany('User');

		return $this->belongsToMany('User')->select(['id','survey_name', 'survey_surname', 'survey_email']);



		/*
		return $this->belongsToMany('User')->select(DB::raw('
			users.survey_name as Name,
			users.survey_surname as Surname,
			users.survey_birthdate as Birthdate,
			users.survey_email as Email,
			users.survey_phone as Phone,
			users.survey_city as City
		'));*/
	}

	public function contest_type()
	{
		return $this->belongsTo('ContestType', 'type_id');
	}

	public function rewards()
    {
        return $this->hasMany('Reward')->orderBy('rank', 'asc');
    }

    public function videos()
    {
        return $this->hasMany('Video')->orderBy('created_at', 'desc');
    }

	public function photos()
	{
		return $this->hasMany('Photo')->orderBy('created_at', 'desc');
	}

	public function essays()
	{
		return $this->hasMany('Essay')->orderBy('created_at', 'desc');
	}

    /**
     * Returns all videos sorted by approval status (awaiting are first) //all entries tab
     */
    public function videosForApproval()
    {
    	return $this->hasMany('Video')->orderBy('approval_step_id','asc')->orderBy('rank_position','asc');
    }

	/**
     * Returns all videos to approve //to approve tab
     */
    public function videosToApprove()
    {
    	return $this->hasMany('Video')->where('approval_step_id', '1');
    }

	/**
     * Returns all videos already approved //approved tab
     */
    public function videosAlreadyApproved()
    {
    	return $this->hasMany('Video')->where('approval_step_id', '3')->orderBy('rank_position','asc');
    }

		/**
     * Returns all videos not approved // not approved tab
     */
    public function videosNotApproved()
    {
    	return $this->hasMany('Video')->where('approval_step_id', '2');
    }

	/**
     * Returns all photos sorted by approval status (awaiting are first)
     */
    public function photosForApproval()
    {
    	return $this->hasMany('Photo')->orderBy('approval_step_id','asc')->orderBy('rank_position','asc');
    }

	/**
     * Returns all photos to approve //to approve tab
     */
    public function photosToApprove()
    {
    	return $this->hasMany('Photo')->where('approval_step_id', '1');
    }

	/**
     * Returns all photos already approved //approved tab
     */
    public function photosAlreadyApproved()
    {
    	return $this->hasMany('Photo')->where('approval_step_id', '3')->orderBy('rank_position','asc');
    }

		/**
     * Returns all photos not approved // not approved tab
     */
    public function photosNotApproved()
    {
    	return $this->hasMany('Photo')->where('approval_step_id', '2');
    }

	//video -- ordinamento virale
	public function videosMostViral()
	{
		return $this->hasMany('Video')->where('approval_step_id', '3')->where('uploadOnFacebookDone', '1')->orderby('score', 'desc');
	}

	//video -- ordinamento temporale
	public function videosMostRecent()
	{
		return $this->hasMany('Video')->where('approval_step_id', '3')->where('uploadOnFacebookDone', '1')->orderby('created_at', 'desc');
	}

	//foto -- ordinamento virale
	public function photosMostViral()
	{
		return $this->hasMany('Photo')->where('approval_step_id', '3')->where('uploadOnFacebookDone', '1')->orderby('score', 'desc');
	}

	//foto -- ordinamento temporale
	public function photosMostRecent()
	{
		return $this->hasMany('Photo')->where('approval_step_id', '3')->where('uploadOnFacebookDone', '1')->orderby('created_at', 'desc');
	}

	public function objective()
    {
		return $this->belongsTo('Objective');
    }

	public function contestType()
    {
		return $this->belongsTo('ContestType', 'type_id');
    }

	public function twPages()
    {
        return $this->belongsToMany('twPage', 'twpage_contest', 'contest_id', 'tw_page_id')->withPivot('follower_count_1', 'follower_count_2');
    }

	public function ytPages()
    {
        return $this->belongsToMany('ytPage', 'ytpage_contest', 'contest_id', 'yt_page_id')->withPivot('subscriptions_count_1', 'subscriptions_count_2');
    }

    public function needsUserRegistration()
    {
		$userNeedsReg = false;
		foreach($this->objectives as $objective) {
			if ( $objective->registration_needed ) {
				$userNeedsReg = true;
				break;
			}
		}

        return $userNeedsReg;
    }

	public function makeStepClass($step, $nSteps = null, $lang = null)
    {
        if(!$nSteps)
            $nSteps = ($this->needsUserRegistration() ? 4 : 3);
        return "step-$step-$nSteps-" . ($lang ? $lang : App::getLocale());
    }

	public function promotionalBanners() {

		return $this->hasMany('PromotionalBanner');

	}

	public function promotionalVideos() {

		return $this->hasMany('PromotionalVideo');

	}

	public function objectives()
    {
        return $this->belongsToMany('Objective', 'contest_objective', 'contest_id', 'objective_id')->withPivot('name', 'surname', 'birthdate', 'email', 'phone', 'city', 'cap');
    }

	public function notifications() {

		return $this->hasMany('Notification');

	}

  public function description() {}

	public function template()
    {
		return $this->belongsTo('Template');
    }

    public function invoice()
    {
        return $this->hasOne('Invoice');
    }

}
