<?php

class Table2TrendLocation extends Eloquent {
    protected $guarded = array();
    protected $table = "table2_trend_locations";

    public function trends()
    {
        return $this->hasMany('Table2Trend');
    }

}