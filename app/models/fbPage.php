<?php

class fbPage extends Eloquent {
    protected $guarded = array();
    protected $softDelete = true;
    protected $table = "fb_pages";
    

    public static $rules = array();
    

    public function contests()
    {
        return $this->belongsToMany('Contest', 'fbpage_contest', 'fbpage_id', 'contest_id')->withPivot('impressions', 'reach', 'new_likes', 'engaged_users');
		
    }
	
	public function photos()
    {
        return $this->belongsToMany('Photo')->withPivot('fb_idpost', 'fb_idphoto');
    }
	
	
	public function videos()
    {
        return $this->belongsToMany('Video')->withPivot('fb_idpost');
    }
	
	public function essays()
    {
        return $this->belongsToMany('Essay', 'fb_page_essay', 'fb_page_id', 'essay_id')->withPivot('fb_idpost', 'fb_idessay');
    }
}
