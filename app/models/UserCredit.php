<?php

use Carbon\Carbon;

class UserCredit extends Eloquent {

	protected $fillable = ['user_id', 'fee_id', 'service_type', 'credits', 
                           'available_credits', 'fixed_fee', 'varying_fee'];
	protected $dates = ['created_at', 'updated_at', 'used_at'];
	protected $appends = ['verbose_service_type'];

    /** ATTRIBUTES
        $table->integer('user_id')->unsigned()->references('id')->on('users');
        $table->integer('fee_id')->unsigned()->references('id')->on('service_fees');
        $table->integer('service_type')->unsigned();
        $table->integer('credits');
        $table->integer('available_credits'); // Initially == credits
        $table->decimal('fixed_fee', 6, 2);
        $table->decimal('varying_fee', 6, 2);
        $table->boolean('is_available')->default(true);
        $table->datetime('used_at')->nullable();
        $table->integer('contest_id')->unsigned()->references('id')->on('contests');
    **/

	public static function getCreditCountByServiceType($service_type, $user, $available = true)
	{
		return static::where('user_id', $user->id)
			->where(function($q) use ($service_type) {
				$q->where('service_type', $service_type);
				if($service_type > 0)
					$q->orWhere('service_type', 0); // include universal credits
			})
			->where('is_available', $available)
			->count();
	}

	public static function getCreditsForUser($user) {
		$credits = DB::table('user_credits')->select(
			DB::raw('sum(credits) as total_qty'),
			DB::raw('sum(if(!is_available,1,0)) as used_qty'),
			DB::raw('sum(if(is_available,1,0)) as available_qty'),
			'service_type')
			->where('user_id', $user->id)
			->groupBy('service_type')
			->orderBy('available_qty')
			->get();

		// Hydrate with verbose service type
		foreach($credits as $id => $credit) {
			$credits[$id]->verbose_service_type = Contest::$serviceTypeVerbose[$credit->service_type];
		}

		return $credits;
	}

	public static function userHasCredit($service_type, $user)
	{
		return (bool) static::getCreditCountByServiceType($service_type, $user) > 0;
	}

	/**
	 * Marks the corresponding credit as not available with FIFO criteria.
	 *
	 * @return integer user_credit->id
	 * @throws NoCreditAvailable
	 */
	public static function chargeCredit($user, $contest)
	{
        // Avoid double charge
        if(static::where('contest_id', $contest->id)->count())
            throw new ContestAlreadyChargedException();

        $service_type = $contest->service_type;
		$credit = static::getFirstAvailableCredit($service_type, $user);

		if(!$credit)
			throw new NoCreditException(trans('usercredit.no_credit'));

		$credit->markAsUsed($contest);
		return $credit->id;
	}

    public static function loadCredit($service_fee, $user)
    {
        $i = 0;
        while($i < $service_fee->credits) {
            static::create([
                'user_id' => $user->id,
                'fee_id' => $service_fee->id,
                'service_type' => $service_fee->service_type,
                'credits' => $service_fee->credits,
                'fixed_fee' => $service_fee->fixed_fee,
                'varying_fee' => $service_fee->varying_fee,
                'cap' => $service_fee->cap
            ]);
            $i++;
        }

        return $i;
    }

	public static function getFirstAvailableCredit($service_type, $user)
	{
		$credit = static::where('user_id', $user->id)
			->where(function($q) {
				$q->where('service_type', $service_type);
				if($service_type > 0)
					$q->orWhere('service_type', 0); // include universal credits
			})
			->where('is_available', true)
			->orderBy('created_at')
			->first();

		return $credit;
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function getVerboseServiceTypeAttribute()
	{
		return Contest::$serviceTypeVerbose[$this->service_type];
	}

	public function markAsUsed($contest)
	{
		$this->is_available = false;
		$this->used_at = Carbon::now();
		$this->contest_id = $contest->id;
		$this->save();
	}

    public function getVaryingFee($cpm)
    {
        return round($this->varying_fee * $cpm, 2);
    }
}
