<?php

class Company extends Eloquent {
    protected $guarded = array();
    protected $table = 'companys';
    public static $rules = array();
    protected $softDelete = true;
    
    const TYPE_INTERNAL = 1;
    const TYPE_EXTERNAL = 2;

    public function users()
    {
        return $this->hasMany('User');
    }
    
    public function contests()
    {
        return $this->hasMany('Contest');
    } 

    public function belongsToManyUsers()
    {
        return $this->belongsToMany('User')->withPivot('token');
    }	
}
