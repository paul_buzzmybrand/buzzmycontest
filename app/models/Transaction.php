<?php

use Carbon\Carbon;

class Transaction extends Eloquent {

	protected $softDelete = true;
	protected $fillable = ['user_id', 'card_id', 'amount', 'transaction_provider_code',
						   'success', 'is_processed', 'response'];
	protected $dates = ['created_at', 'updated_at'];
    protected $hidden = ['deleted_at'];

	/** ATTRIBUTES
		$table->integer('user_id')->unsigned()->references('id')->on('users');
		$table->integer('card_id')->unsigned()->references('id')->on('credit_cards');
		$table->decimal('amount', 6, 2);
		$table->text('transaction_provider_code');
		$table->boolean('success')->default(true);
		$table->boolean('is_processed')->default(false);
		$table->text('response')->default('{}'); // JSON
	**/

	/** RELATIONS **/

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function card()
	{
		return CreditCard::withTrashed()->find($this->card_id)->first();
	}

	/**  /RELATIONS **/

}
