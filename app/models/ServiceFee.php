<?php

class ServiceFee extends Eloquent {
    protected $guarded = array();
    protected $table = "service_fees";
    
	protected $fillable = [];

    public function getInvoiceDescription()
    {
        return Lang::get('service_fee.invoice_description', [
            'fixed_fee' => $this->fixed_fee,
            'varying_fee' => $this->varying_fee,
            'credits' => $this->credits,
            'service_type' => Contest::$serviceTypeVerbose[$this->service_type]
        ]);
    }
	

}