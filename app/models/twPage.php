<?php

class twPage extends Eloquent {
    protected $guarded = array();
    protected $softDelete = true;
    protected $table = "tw_pages";
    

    public static $rules = array();
    

    public function contests()
    {
        return $this->belongsToMany('Contest', 'twpage_contest', 'tw_page_id', 'contest_id')->withPivot('follower_count_1', 'follower_count_2');
    }
	
	public function photos()
    {
        return $this->belongsToMany('Photo', 'tw_page_photo', 'tw_page_id', 'photo_id')->withPivot('tw_idphoto');
    }
	
	
	public function videos()
    {
        return $this->belongsToMany('Video')->withPivot('tw_idvideo');
    }
	
	public function essays()
    {
        return $this->belongsToMany('Essay', 'tw_page_essay', 'tw_page_id', 'essay_id' )->withPivot('tw_idessay');
    }
	
}