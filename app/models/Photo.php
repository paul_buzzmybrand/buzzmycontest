<?php

class Photo extends Eloquent {
    protected $guarded = array();
    protected $softDelete = true;
	protected $table = "photos";
	
    public static $rules = array();
    
    public function user()
    {
        return $this->belongsTo('User');
    }
    
    public function contest()
    {
        return $this->belongsTo('Contest');
    }
   
    public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function approvalStep()
    {
    	return $this->belongsTo('ApprovalStep');
    }
	
	public function fbPages()
    {
        return $this->belongsToMany('fbPage')->withPivot('fb_idpost', 'fb_idphoto', 'reach', 'impressions', 'engaged_users', 'unique_likes', 'unique_comments', 'unique_shares');
    }
	
	public function twPages()
    {
        return $this->belongsToMany('twPage', 'tw_page_photo', 'photo_id', 'tw_page_id')->withPivot('tw_idphoto', 'retweets_count', 'favourite_count');
    }
	
}
