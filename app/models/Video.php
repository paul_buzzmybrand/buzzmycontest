<?php

class Video extends Eloquent {
    protected $guarded = array();
    protected $softDelete = true;
	protected $table = "videos";
	
    public static $rules = array();
    
    public function user()
    {
        return $this->belongsTo('User');
    }
	
    public function extrainfo()
	{
		return $this->belongsTo('ExtraInfo');
	}
    
    public function contest()
    {
        return $this->belongsTo('Contest');
    }
   
    public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function approvalStep()
    {
    	return $this->belongsTo('ApprovalStep');
    }
	
	public function fbPages()
    {
        return $this->belongsToMany('fbPage')->withPivot('fb_idpost', 'reach', 'impressions', 'engaged_users', 'unique_views', 'views', 'unique_likes', 'unique_comments', 'unique_shares');
		
	}
	
	public function twPages()
    {
        return $this->belongsToMany('twPage', 'tw_page_video', 'video_id', 'tw_page_id')->withPivot('tw_idvideo', 'retweets_count', 'favourite_count');
    }
	
	public function ytPages()
    {
        return $this->belongsToMany('ytPage', 'yt_page_video', 'video_id', 'yt_page_id')->withPivot('yt_idvideo', 'yt_view_count', 'yt_like_count');
    }

}
