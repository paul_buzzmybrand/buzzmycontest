<?php

class Table1TrendLocation extends Eloquent {
    protected $guarded = array();
    protected $table = "table1_trend_locations";

    public function trends()
    {
        return $this->hasMany('Table1Trend');
    }

}