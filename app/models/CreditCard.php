<?php

use Carbon\Carbon;
//use \PaymillHelper;
//use Paymill\Services\PaymillException;
use Laravel\Cashier\Customer;

class CreditCard extends Eloquent {

    const EXPIRATION_WARN_TIME = 3; // Months

    protected $softDelete = true;
    protected $fillable = ['user_id', 'is_active', 'last_four', 'card_holder', 'description',
                           'expire_month', 'expire_year', 'provider', 'token', 'customer_id', 'card_id'];
    protected $dates = ['created_at', 'updated_at'];
    protected $appends = ['is_expiring'];

    /** ATTRIBUTES:
        $table->integer('user_id')->unsigned()->references('id')->on('users');
        $table->boolean('is_active')->default(true);
        $table->integer('last_four');
        $table->string('card_holder');
        $table->integer('expire_month');
        $table->integer('expire_year');
        $table->string('provider');
        $table->string('token');
    **/

    public function user()
    {
        return $this->belongsTo('User');
    }

    public static function getCardByUser($user)
    {
        return static::where('user_id', $user->id)->first();
    }

    public static function userHasActiveCard($user)
    {
        return (bool) static::getCardByUser($user);
    }

    public static function userHasValidCard($user)
    {
        $card = static::getCardByUser($user);
        if(!$card)
            return false;

        return $card->isValid();
    }

    public static function makeStripeCard($customer, $user, $token)
    {
        try {
            $card = static::create([
                'user_id' => $user->id,
                'card_holder' => $customer->sources->data[0]->name,
                'expire_month' => $customer->sources->data[0]->exp_month,
                'expire_year' => $customer->sources->data[0]->exp_year,
                'last_four' => $customer->sources->data[0]->last4,
                'provider' => 'stripe',
                'token' => $token,
                'customer_id' => $customer->id,
                'card_id' => $customer->sources->data[0]->id
            ]);

            return $card;

        } catch (PaymentException $err) {
            Log::error("Cannot create new card: " . $err->getMessage(), ['token' => $token]);
            throw new PaymentException($err->getMessage());
        }
    }

    public static function makeCard($token, $user)
    {
        try {
            $payment = PaymillHelper::makePaymentCard($token);

            // No exception, so let's create the card...
            $card = static::create([
                'user_id' => $user->id,
                'card_holder' => $payment->getCardHolder(),
                'expire_month' => $payment->getExpireMonth(),
                'expire_year' => $payment->getExpireYear(),
                'last_four' => $payment->getLastFour(),
                'provider' => 'paymill',
                'token' => $payment->getId()
            ]);

            return $card;

        } catch (PaymentException $err) {
            Log::error("Cannot create new card: " . $err->getMessage(), ['token' => $token]);
            throw new PaymentException($err->getMessage());
        }
    }

    /**
      * This method is used to initialize a new card while charging a payment.
      * If the user already has a registered card, use $card->charge()
      *
      * @return Transaction object
      */
    public static function makeAndCharge($token, $amount, $user, $description = null)
    {
        if(static::userHasActiveCard($user))
            throw new PaymentException(trans('payments.duplicate_initial_payment'));

        try {

            $response = PaymillHelper::makeTransaction($amount, $token);

            // Make the card
            $card = static::makeCard($token, $user);

            // ...and the transaction
            $transaction = Transaction::create([
                'card_id' => $card->id,
                'user_id' => $user->id,
                'amount' => PaymillHelper::getDecimalAmount($amount),
                'transaction_provider_code' => $response->getId(),
                'description' => $description
            ]);

            return $transaction;

        } catch (PaymentException $err) {
            $transaction = Transaction::create([
                'user_id' => $user->id,
                'amount' => $amount,
                'transaction_provider_code' => $token,
                'success' => false,
                'description' => $description
            ]);
            throw new PaymentException($err->getMessage());
        }
    }

    public static function chargeUser($user, $amount, $description = null)
    {
        $card = static::getCardByUser($user);
        if(!$card)
            throw new PaymentException(trans('payments.user_has_no_active_card'));

        return $card->charge($amount, $description = null);
    }

    /**
     * @return Carbon obj
     **/
    public static function getExpirationWarningLimit()
    {
        return Carbon::now()->addMonths(3);
    }

    public static function getExpiringCards()
    {
        $expiration = static::getExpirationWarningLimit();
        return static::where('expire_month', '<=', $expiration->month)
                    ->where('expire_year', '<=', $expiration->year);
    }

    /**
      * Charges an existing card.
      *
      * @return Transaction object
      */
    public function charge($amount, $description = null)
    {
        try {
            $response = PaymillHelper::makeTransaction($amount, null, $this->token, $description);
            // Store the transaction
            $transaction = Transaction::create([
                'card_id' => $this->id,
                'user_id' => $this->user_id,
                'amount' => $amount,
                'transaction_provider_code' => $response->getId()
            ]);
            return $transaction;
        } catch (PaymillException $err) {
            $transaction = Transaction::create([
                'user_id' => $user->id,
                'card_id' => $this->id,
                'amount' => $amount,
                'transaction_provider_code' => $this->token,
                'success' => false
            ]);
            throw new PaymentException($err->getMessage());
        }
    }

    public function isValid()
    {
        $now = new Carbon();
        $expiration = Carbon::now()->month($this->expire_month)->year($this->expire_year);
        return $now->lt($expiration);
    }

    public function isExpiring()
    {
        $limit = static::getExpirationWarningLimit();
        return ($this->expire_month <= $limit->month && $this->expire_year <= $limit->year);
    }

    public function getIsExpiringAttribute()
    {
        return (bool) $this->isExpiring();
    }

    //per cancellare una card, dobbiamo ottenere la chiave del customer
    //e dobbiamo ricavarla ottenendo un parametro
    public function delete()
    {
        $card = Input::get('card');

        Stripe::setApiKey("sk_test_OeVAPOsmdoAqkC4zkwzU0P5o");

        $customer = Customer::retrieve($card['customer_id']);
        $customer->sources->retrieve($card['card_id'])->delete();
        //PaymillHelper::removePayment($this->token);
        parent::delete();
    }

}
