<?php

class ExtraInfo extends Eloquent {
    protected $guarded = array();
    protected $softDelete = true;
	protected $table = "extrainfos";
    public static $rules = array();
    
	public function videos()
    {
        return $this->hasMany('Video');
    }
}
