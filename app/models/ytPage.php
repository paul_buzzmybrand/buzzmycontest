<?php

class ytPage extends Eloquent {
    protected $guarded = array();
    protected $softDelete = true;
    protected $table = "yt_pages";
    

    public static $rules = array();
    
	public function contests()
    {
        return $this->belongsToMany('Contest', 'ytpage_contest', 'yt_page_id', 'contest_id')->withPivot('subscriptions_count_1', 'subscriptions_count_2');
    }
	
	public function photos()
    {
        return $this->belongsToMany('Photo', 'yt_page_photo', 'yt_page_id', 'photo_id')->withPivot('yt_idphoto');
    }
	
	public function videos()
    {
        return $this->belongsToMany('Video', 'yt_page_video', 'yt_page_id', 'video_id')->withPivot('yt_idvideo', 'playlist_item');
    }
	
	public function ytPlaylists()
    {
    	return $this->hasMany('ytPlaylist');
    }
	
}