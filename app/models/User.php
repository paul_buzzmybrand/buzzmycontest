<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Laravel\Cashier\BillableInterface;
use Laravel\Cashier\BillableTrait;

class User extends Eloquent implements UserInterface, RemindableInterface, BillableInterface {

    use BillableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $softDelete = true;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }
    
    public function company()
    {
        return $this->belongsTo('Company');
    }
    
    public function contests()
    {
        return $this->belongsToMany('Contest')->withPivot('login_website', 'login_mobile', 'login_instagram', 'instantwin_done');
    }
    
   /* public function contests()
    {
        return $this->hasMany('Contest');
    }*/
    
    public function videos()
    {
        return $this->hasMany('Video')->select(['user_id','name','id']);
    }
	
	public function photos()
	{
		return $this->hasMany('Photo')->select(['user_id','name','id']);
	}
    
    public function facebookProfiles()
    {
        return $this->hasMany('FacebookProfile');
    }
	
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
		return 'remember_token';
	}
	
	public function belongsToManyCompanys()
    {
        return $this->belongsToMany('Company')->withPivot('token');
    }	
	
	public function twitterProfiles()
    {
        return $this->hasMany('TwitterProfile');
    }

    public function isCompany() 
    {
        return (bool) $this->company_id !== null;
    }

    public function isSuperAdmin()
    {
		//Log::debug("Company_id: ".$this->company_id);
		if ($this->company_id == 225)
			return true;
		else
			return false;
        //return (bool) $this->company_id == 225;
    }

    public function creditCard()
    {
        return CreditCard::getCardByUser($this);
    }
    
    public function hasValidCreditCard()
    {
        return CreditCard::userHasValidCard($this);
    }

    public function getFullNameAttribute()
    {
        return $this->name . ' ' . $this->surname;
    }

    public static function boot() {

        parent::boot();

        static::created(function($user) {
			
			
			
			if (!is_null($user->company_id))
			{
				$fee = ServiceFee::where('assign_on_registration', true)->first();
				UserCredit::loadCredit($fee, $user);
				Log::info('Assigned default credit to ' . $user->email);
			}
			
            
        });

    }

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];
}