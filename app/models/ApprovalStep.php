<?php

class ApprovalStep extends Eloquent {
    protected $guarded = array();
    protected $softDelete = true;

    public static $rules = array();
    
    const NO_APPROVAL = 1;
    const NOT_APPROVED = 2;
    const APPROVED = 3;
}
