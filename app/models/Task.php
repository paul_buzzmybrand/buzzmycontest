<?php


class Task extends Eloquent {
	const SCHEDULED = 1;
	const IN_PROGRESS = 2;
	const SUCCESS = 3;
}