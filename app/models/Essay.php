<?php

class Essay extends Eloquent {
    protected $guarded = array();
    protected $softDelete = true;
	protected $table = "essays";
	
    public static $rules = array();
    
    public function user()
    {
        return $this->belongsTo('User');
    }
    
    public function contest()
    {
        return $this->belongsTo('Contest');
    }
   

    public function approvalStep()
    {
    	return $this->belongsTo('ApprovalStep');
    }
	
    public function fbPages()
    {
        return $this->belongsToMany('fbPage', 'fb_page_essay', 'essay_id', 'fb_page_id')->withPivot('fb_idpost', 'fb_idessay', 'reach', 'impressions', 'engaged_users', 'unique_likes', 'unique_comments', 'unique_shares');
    }
	
    public function twPages()
    {
        return $this->belongsToMany('twPage', 'tw_page_essay', 'essay_id', 'tw_page_id')->withPivot('tw_idessay', 'retweets_count', 'favourite_count');
    }
	
}
