var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

window.AnimationInOut = (function() {
  function AnimationInOut(ref) {
    this.ref = ref;
    this.onScroll = bind(this.onScroll, this);
    this.w = $(window);
    this.items = this.ref.find('.js-in-out-item');
    this.w.scroll(this.onScroll);
    this.onScroll();
  }

  AnimationInOut.prototype.onScroll = function() {
    return this.items.each(function() {
      var offset;
      offset = $(this).attr('data-offset') != null ? parseInt($(this).attr('data-offset')) : 0;
      if ($(this).isOnScreen(offset)) {
        $(this).addClass('animation-in');
        if ($(this).hasClass('js-hasSiblings')) {
          return $($(this).attr('data-sibling')).addClass('animation-in');
        }
      } else {
        if (!$(this).hasClass('js-outOff')) {
          return $(this).removeClass('animation-in');
        }
      }
    });
  };

  return AnimationInOut;

})();

if (!window.console) {
  window.console = {
    log: (function(obj) {})
  };
}

$(function() {
  var animation_in_out_ref, fieldanm_ref, fixedHeader, getIOSVersion, i, j, k, l, overlay_ref, ref, ref1, ref2, results, shitem_ref, toggle_ref, window_ref;
  window_ref = $(window);
  window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
  getIOSVersion = function() {
    var v;
    if (/iP(hone|od|ad)/.test(navigator.platform)) {
      v = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
      return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
    } else {
      return false;
    }
  };
  window.has_ios = getIOSVersion();
  window.is_mobile = navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/Android/i) ? true : false;
  window.is_iphone = navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) ? true : false;
  window.is_ipad = navigator.userAgent.match(/iPad/i) ? true : false;
  if (is_mobile) {
    $('body').addClass('deviceMobile');
  }
  window.transEndEventNames = {
    WebkitTransition: "webkitTransitionEnd",
    MozTransition: "transitionend",
    transition: "transitionend"
  };
  window.transEndEventName = window.transEndEventNames[Modernizr.prefixed("transition")];
  Modernizr.addTest("highres", function() {
    var dpr;
    dpr = window.devicePixelRatio || (window.screen.deviceXDPI / window.screen.logicalXDPI) || 1;
    return !!(dpr > 1);
  });
  if (typeof Modernizr.prefixed("transform") === 'string') {
    window.prefixedTransform = Modernizr.prefixed("transform").replace(/([A-Z])/g, function(str, m1) {
      return "-" + m1.toLowerCase();
    }).replace(/^ms-/, "-ms-");
  }
  overlay_ref = $('.j-overlay');
  if (overlay_ref.length === 1) {
    new Overlay(overlay_ref);
  }
  animation_in_out_ref = $('.js-in-out-animation');
  if (animation_in_out_ref.length === 1) {
    new AnimationInOut(animation_in_out_ref);
  }
  toggle_ref = $('.j-toggle');
  if (toggle_ref.length > 0) {
    for (i = j = 0, ref = toggle_ref.length; 0 <= ref ? j < ref : j > ref; i = 0 <= ref ? ++j : --j) {
      new Toggle($(toggle_ref[i]));
    }
  }
  shitem_ref = $('.j-sh');
  if (shitem_ref.length > 0) {
    for (i = k = 0, ref1 = shitem_ref.length; 0 <= ref1 ? k < ref1 : k > ref1; i = 0 <= ref1 ? ++k : --k) {
      new shItem($(shitem_ref[i]));
    }
  }
  fixedHeader = $('.j-fixedHeader');
  if (fixedHeader.length > 0) {
    new fixHeader(fixedHeader);
  }
  fieldanm_ref = $('.j-field');
  if (fieldanm_ref.length > 0) {
    results = [];
    for (i = l = 0, ref2 = fieldanm_ref.length; 0 <= ref2 ? l < ref2 : l > ref2; i = 0 <= ref2 ? ++l : --l) {
      results.push(new fieldAnm($(fieldanm_ref[i])));
    }
    return results;
  }
});

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

window.fieldAnm = (function() {
  function fieldAnm(ref) {
    this.ref = ref;
    this.initInputItem = bind(this.initInputItem, this);
    this.item = this.ref.find('.j-fieldItem');
    this.initInputItem();
  }

  fieldAnm.prototype.initInputItem = function() {
    var i, item, j, ref, results;
    results = [];
    for (i = j = 0, ref = this.item.length; 0 <= ref ? j < ref : j > ref; i = 0 <= ref ? ++j : --j) {
      item = $(this.item[i]);
      results.push(item.focusout((function(_this) {
        return function() {
          if (item.val() !== '') {
            return item.addClass('filled');
          } else {
            return item.removeClass('filled');
          }
        };
      })(this)));
    }
    return results;
  };

  return fieldAnm;

})();

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

window.fixHeader = (function() {
  function fixHeader(ref) {
    this.ref = ref;
    this.onScroll = bind(this.onScroll, this);
    this.window_ref = $(window);
    this.window_ref.on('scroll', this.onScroll);
    this.ref_h = this.ref.height();
  }

  fixHeader.prototype.onScroll = function() {
    var currentTop, item, itemTop;
    item = $('.j-fixHeaderItem');
    itemTop = Math.floor(item.offset().top);
    currentTop = this.window_ref.scrollTop();
    if (currentTop > (this.ref_h + 10)) {
      this.ref.addClass('pause');
    } else {
      this.ref.removeClass('pause');
    }
    if (currentTop >= itemTop) {
      return this.ref.addClass('inverted');
    } else {
      return this.ref.removeClass('inverted');
    }
  };

  return fixHeader;

})();

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

window.Overlay = (function() {
  function Overlay(ref) {
    this.ref = ref;
    this.closeOverlay = bind(this.closeOverlay, this);
    this.openOverlay = bind(this.openOverlay, this);
    this.init = bind(this.init, this);
    this.close_btn = this.ref.find('.j-overlayClose');
    this.contents = this.ref.find('.j-overlayContents');
    this.items = this.contents.find('.j-overlayItem');
    this.openBtns = $('.j-overlayBtn');
    this.body = $('body');
    this.init();
  }

  Overlay.prototype.init = function() {
    var i, j, openBtn, ref;
    for (i = j = 0, ref = this.openBtns.length; 0 <= ref ? j < ref : j > ref; i = 0 <= ref ? ++j : --j) {
      openBtn = $(this.openBtns[i]);
      openBtn.click((function(_this) {
        return function(e) {
          var content;
          e.preventDefault();
          content = $(e.currentTarget).attr('data-overlayBtn');
          return _this.openOverlay(content);
        };
      })(this));
    }
    return this.close_btn.click((function(_this) {
      return function(e) {
        e.preventDefault();
        return _this.closeOverlay();
      };
    })(this));
  };

  Overlay.prototype.openOverlay = function(content) {
    this.ref.addClass('overlayOpen');
    this.body.addClass('noScroll');
    return this.ref.find($("[data-overlay='" + content + "']")).addClass('showContent');
  };

  Overlay.prototype.closeOverlay = function() {
    this.ref.removeClass('overlayOpen');
    this.body.removeClass('noScroll');
    this.items.removeClass('showContent');
    return window.menu.resetMenu();
  };

  return Overlay;

})();

var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

window.shItem = (function() {
  function shItem(ref) {
    this.ref = ref;
    this.initToggleItem = bind(this.initToggleItem, this);
    this.item = this.ref.find('.j-shItem');
    this.initToggleItem();
  }

  shItem.prototype.initToggleItem = function() {
    var b, btn, duration, i, item, j, ref, results;
    results = [];
    for (i = j = 0, ref = this.item.length; 0 <= ref ? j < ref : j > ref; i = 0 <= ref ? ++j : --j) {
      item = $(this.item[i]);
      btn = item.find('.j-shBtn');
      b = $('html,body');
      duration = 500;
      results.push(btn.click((function(_this) {
        return function(e) {
          var item_toOpen, item_toOpenTop, target_top;
          e.preventDefault();
          item_toOpen = $(e.currentTarget).closest('.j-shItem');
          item_toOpen.siblings('.j-shItem').removeClass('detailOpen');
          item_toOpen.toggleClass('detailOpen');
          item_toOpenTop = Math.floor(item_toOpen.offset().top);
          target_top = item_toOpenTop;
          return b.stop().animate({
            scrollTop: "" + target_top
          }, {
            duration: duration,
            easing: 'swing'
          });
        };
      })(this)));
    }
    return results;
  };

  return shItem;

})();

window.Toggle = (function() {
  function Toggle(ref) {
    this.ref = ref;
    this.items = this.ref.find('.j-toggleItem');
    this.openBtn = this.ref.find('.j-toggleBtn');
    this.openBtn.click((function(_this) {
      return function(e) {
        e.preventDefault();
        return _this.ref.toggleClass('toggleOpen');
      };
    })(this));
  }

  return Toggle;

})();
