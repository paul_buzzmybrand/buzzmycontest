class window.AnimationInOut
	constructor: (@ref) ->
		@w = $(window)
		@items = @ref.find '.js-in-out-item'
		@w.scroll @onScroll
		@onScroll()

	onScroll : =>
		@items.each(->
			offset = if $(@).attr('data-offset')? then parseInt $(@).attr('data-offset') else 0
			if $(@).isOnScreen(offset)
				$(@).addClass 'animation-in'
				if $(@).hasClass('js-hasSiblings')
					$($(@).attr('data-sibling')).addClass('animation-in')
			else
				if !$(@).hasClass('js-outOff') then $(@).removeClass 'animation-in'
		)

unless window.console then window.console = {log: ((obj) ->)}

$ ->

	window_ref = $ window

	window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame

	##################
	# USER AGENT CHECK
	##################

	getIOSVersion = ->
		if /iP(hone|od|ad)/.test(navigator.platform)
			v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/)
			return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)]
		else
			return false

	window.has_ios = getIOSVersion()
	window.is_mobile = if navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/Android/i) then true else false
	window.is_iphone = if navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) then true else false
	window.is_ipad = if navigator.userAgent.match(/iPad/i) then true else false

	if is_mobile then $('body').addClass 'deviceMobile'

	#################
	# MODERNIZR TESTS
	#################

	# TRANSITIONEND
	window.transEndEventNames = {
		WebkitTransition: "webkitTransitionEnd" # Saf 6, Android Browser
		MozTransition: "transitionend" # only for FF < 15
		transition: "transitionend" # IE10, Opera, Chrome, FF 15+, Saf 7+
	}
	window.transEndEventName = window.transEndEventNames[Modernizr.prefixed("transition")]

	# DPI Modernizr.highres
	Modernizr.addTest "highres", ->
		dpr = window.devicePixelRatio or (window.screen.deviceXDPI / window.screen.logicalXDPI) or 1
		!!(dpr > 1)

	# TRANSFORM PREFIXED
	if typeof Modernizr.prefixed("transform") is 'string'
		window.prefixedTransform = Modernizr.prefixed("transform").replace(/([A-Z])/g, (str, m1) -> "-" + m1.toLowerCase()).replace /^ms-/, "-ms-"


	###########
	# OVERLAY #
	###########

	overlay_ref = $ '.j-overlay'
	if overlay_ref.length is 1
		new Overlay overlay_ref


	####################
	# ANIMATION IN-OUT #
	####################

	animation_in_out_ref = $ '.js-in-out-animation'
	if animation_in_out_ref.length is 1
		new AnimationInOut animation_in_out_ref

	##########
	# Toggle #
	##########

	toggle_ref = $ '.j-toggle'
	if toggle_ref.length > 0
		for i in [0...toggle_ref.length]
			new Toggle $ toggle_ref[i]


	##########
	# shItem #
	##########

	shitem_ref = $ '.j-sh'
	if shitem_ref.length > 0
		for i in [0...shitem_ref.length]
			new shItem $ shitem_ref[i]


	#############
	# fixHeader #
	#############

	fixedHeader = $ '.j-fixedHeader'
	if fixedHeader.length > 0
			new fixHeader fixedHeader


	############
	# fieldAnm #
	############

	fieldanm_ref = $ '.j-field'
	if fieldanm_ref.length > 0
		for i in [0...fieldanm_ref.length]
			new fieldAnm $ fieldanm_ref[i]

class window.fieldAnm
	constructor: (@ref) ->
		@item = @ref.find '.j-fieldItem'
		@initInputItem()

	initInputItem: =>
		for i in [0...@item.length]
			item = $ @item[i]

			item.focusout ()=>
				if item.val() != ''
					item.addClass('filled')
				else
					item.removeClass('filled')

class window.fixHeader

	constructor: (@ref) ->

		@window_ref = $ window
		@window_ref.on 'scroll', @onScroll
		@ref_h = @ref.height()

		# @previousTop = 0
		# @canvas_h = $('.canvasHeader').height()
		# $(window).resize @onResize
		# @onResize()


	# onResize : =>
	# 	@canvas_h = $('.canvasHeader').height()
	# 	console.log @canvas_h


	onScroll : =>
		item = $('.j-fixHeaderItem')
		itemTop = Math.floor(item.offset().top)
		currentTop = @window_ref.scrollTop()

		if currentTop > (@ref_h + 10)
			@ref.addClass 'pause'

		else
			@ref.removeClass 'pause'

		if currentTop >= itemTop
			@ref.addClass 'inverted'

		else
			@ref.removeClass 'inverted'




class window.Overlay
	constructor:(@ref)->
		@close_btn = @ref.find '.j-overlayClose'
		@contents = @ref.find '.j-overlayContents'
		@items = @contents.find '.j-overlayItem'
		@openBtns = $ '.j-overlayBtn'
		@body = $ 'body'

		@init()

	init: =>
		for i in [0...@openBtns.length]
			openBtn = $ @openBtns[i]
			openBtn.click (e)=>
				e.preventDefault()
				content = $(e.currentTarget).attr 'data-overlayBtn'
				@openOverlay content

		@close_btn.click (e)=>
			e.preventDefault()
			@closeOverlay()

	openOverlay :(content)=>
		@ref.addClass 'overlayOpen'
		@body.addClass 'noScroll'
		@ref.find($ "[data-overlay='#{content}']").addClass 'showContent'

	closeOverlay : =>
		@ref.removeClass 'overlayOpen'
		@body.removeClass 'noScroll'
		@items.removeClass 'showContent'
		window.menu.resetMenu()




class window.shItem
	constructor: (@ref) ->
		@item = @ref.find '.j-shItem'
		@initToggleItem()

	initToggleItem: =>
		for i in [0...@item.length]
			item = $ @item[i]
			btn = item.find '.j-shBtn'
			b = $ 'html,body'
			duration = 500

			btn.click (e)=>
				e.preventDefault()

				item_toOpen = $(e.currentTarget).closest('.j-shItem')
				item_toOpen.siblings('.j-shItem').removeClass('detailOpen')
				item_toOpen.toggleClass('detailOpen')


				item_toOpenTop = Math.floor(item_toOpen.offset().top)
				target_top = item_toOpenTop


				b.stop().animate {scrollTop: "#{target_top}"}, {duration: duration, easing: 'swing'}
class window.Toggle
	constructor: (@ref) ->
		@items = @ref.find '.j-toggleItem'
		@openBtn = @ref.find '.j-toggleBtn'
		@openBtn.click (e)=>
			e.preventDefault()
			@ref.toggleClass 'toggleOpen'