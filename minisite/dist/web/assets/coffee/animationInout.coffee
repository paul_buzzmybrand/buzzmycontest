class window.AnimationInOut
	constructor: (@ref) ->
		@w = $(window)
		@items = @ref.find '.js-in-out-item'
		@w.scroll @onScroll
		@onScroll()

	onScroll : =>
		@items.each(->
			offset = if $(@).attr('data-offset')? then parseInt $(@).attr('data-offset') else 0
			if $(@).isOnScreen(offset)
				$(@).addClass 'animation-in'
				if $(@).hasClass('js-hasSiblings')
					$($(@).attr('data-sibling')).addClass('animation-in')
			else
				if !$(@).hasClass('js-outOff') then $(@).removeClass 'animation-in'
		)
