class window.Toggle
	constructor: (@ref) ->
		@items = @ref.find '.j-toggleItem'
		@openBtn = @ref.find '.j-toggleBtn'
		@openBtn.click (e)=>
			e.preventDefault()
			@ref.toggleClass 'toggleOpen'