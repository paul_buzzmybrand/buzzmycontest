class window.fixHeader

	constructor: (@ref) ->

		@window_ref = $ window
		@window_ref.on 'scroll', @onScroll
		@ref_h = @ref.height()

		# @previousTop = 0
		# @canvas_h = $('.canvasHeader').height()
		# $(window).resize @onResize
		# @onResize()


	# onResize : =>
	# 	@canvas_h = $('.canvasHeader').height()
	# 	console.log @canvas_h


	onScroll : =>
		item = $('.j-fixHeaderItem')
		itemTop = Math.floor(item.offset().top)
		currentTop = @window_ref.scrollTop()

		if currentTop > (@ref_h + 10)
			@ref.addClass 'pause'

		else
			@ref.removeClass 'pause'

		if currentTop >= itemTop
			@ref.addClass 'inverted'

		else
			@ref.removeClass 'inverted'



