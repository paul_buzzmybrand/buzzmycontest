class window.Overlay
	constructor:(@ref)->
		@close_btn = @ref.find '.j-overlayClose'
		@contents = @ref.find '.j-overlayContents'
		@items = @contents.find '.j-overlayItem'
		@openBtns = $ '.j-overlayBtn'
		@body = $ 'body'

		@init()

	init: =>
		for i in [0...@openBtns.length]
			openBtn = $ @openBtns[i]
			openBtn.click (e)=>
				e.preventDefault()
				content = $(e.currentTarget).attr 'data-overlayBtn'
				@openOverlay content

		@close_btn.click (e)=>
			e.preventDefault()
			@closeOverlay()

	openOverlay :(content)=>
		@ref.addClass 'overlayOpen'
		@body.addClass 'noScroll'
		@ref.find($ "[data-overlay='#{content}']").addClass 'showContent'

	closeOverlay : =>
		@ref.removeClass 'overlayOpen'
		@body.removeClass 'noScroll'
		@items.removeClass 'showContent'
		window.menu.resetMenu()



