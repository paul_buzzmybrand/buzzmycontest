angular.module('bmbMinisite.appConf', [])
.constant('appConf', {
  "basePageTitle": "DEV: Buzz My Brand Minisite",
  "endPoint" : "/",
  "defaultLanguage": "en_EN",
  "debug": true,
  "maxRetry":"3",
  "domain" : location.origin,
  "domainRes": "https://www.buzzmybrand.com",
  "isLogged": "logged",
  "overlayIsOpen" : false
})

