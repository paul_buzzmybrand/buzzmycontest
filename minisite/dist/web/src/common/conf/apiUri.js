angular.module('bmbMinisite.apiUri', [])
.constant('apiUri', {
  "current_site": "".concat(location.origin, "/"),
  "contest": "".concat(location.origin, "/msapi/"),
  "entries": "".concat(location.origin, "/msapi/entries/"),
  "rewards": "".concat(location.origin, "/msapi/rewards/"),
  "profile": "".concat(location.origin, "/msapi/profile/"),
  "profileEntries": "".concat(location.origin, "/msapi/profile/entries/"),
  "inviteFriend": "".concat(location.origin, "/msapi/inviteFriend/"),
  "objectives": "".concat(location.origin, "/msapi/objectives/"),
  "savePhoto": "".concat(location.origin, "/msapi/savePhoto/"),
  "saveVideo": "".concat(location.origin, "/msapi/saveVideo/"),
  "saveEssay": "".concat(location.origin, "/msapi/saveEssay/"),
  "signUp": "".concat(location.origin, "/msapi/signUp/")
})
