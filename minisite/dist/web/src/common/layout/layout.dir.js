/*jshint -W018 */
angular.module('bmbMinisite.layout', [
  'bmbMinisite.appConf',
  'bmbMinisite.fct.utils'
])



/*

  TEST DIR

*/
.directive('onKeyDown', function() {
  return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
          scope.$apply(function (){
              scope.$eval(attrs.onKeyDown);
          });
        });
    };
}).directive('parseStyle', function($interpolate) {
  return function(scope, elem) {
        var exp = $interpolate(elem.html()),
        watchFunc = function () { return exp(scope); };
        scope.$watch(watchFunc, function (html) {
            elem.html(html);
        });
    };
});;
