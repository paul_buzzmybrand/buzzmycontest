/* jshint   loopfunc: true */
angular.module( 'bmbMinisite.fct.rewards', ['bmbMinisite.apiUri','bmbMinisite.appConf'])

.factory('fctRewards', [ '$http', '$q', 'apiUri', '$log', function ($http, $q, apiUri, $log) {

  /*

    _getAPI

  */
  function _getAPI(o) {
    var deferred = $q.defer();
    $http.get(o.url, {
      headers: o.headers
    }).success(function(data) {
        deferred.resolve(data);
     }).error(function(msg, code) {
        deferred.reject(msg);
        $log.error(msg, code);
     });
    return deferred.promise;
  }



  /*

    _getData

  */
  function _getData(contestName) {
    var o = {
      url: apiUri.rewards.concat(contestName)
    };
    return _getAPI(o);
  }


  return {
    getRewards: _getData
  };

}]);
