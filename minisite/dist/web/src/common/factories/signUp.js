/* jshint   loopfunc: true */
angular.module( 'bmbMinisite.fct.signUp', ['bmbMinisite.apiUri','bmbMinisite.appConf'])

.factory('fctSignUp', [ '$http', '$q', 'apiUri', '$log', function ($http, $q, apiUri, $log) {

  /*

    _getAPI

  */
  function _getAPI(o) {
    var deferred = $q.defer();
    $http.get(o.url, {
      headers: o.headers
    }).success(function(data) {
        deferred.resolve(data);
     }).error(function(msg, code) {
        deferred.reject(msg);
        $log.error(msg, code);
     });
    return deferred.promise;
  }



  function _postAPI(o) {
    var deferred = $q.defer();
    $http({
      method: 'POST',
      url: o.url,
      data: o.data
    }).success(function(data) {
        _is_busy=false;
        deferred.resolve(data);
     }).error(function(msg, code) {
        _is_busy=false;
        deferred.reject(msg);
        $log.error(msg, code);
     });
    return deferred.promise;
  }

  /*

    _getData

  */
  function _sign_up(contestName, user) {
    var o = {
      url: apiUri.signUp.concat(contestName),
      data: user
    };
    return _postAPI(o);
  }


  return {
    signUp: _sign_up
  };

}]);
