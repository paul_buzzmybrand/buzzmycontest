/* jshint   loopfunc: true, newcap: false*/
angular.module( 'bmbMinisite.fct.utils', ['ngSanitize','bmbMinisite.apiUri', 'angularMoment'])

.factory('fctUtil', ['$sce', '$sanitize', 'apiUri', 'moment', function ($sce, $sanitize, apiUri, moment) {

  var translate_re = new Array(/&(nbsp|amp|quot|lt|gt|agrave|deg|rsquo|hellip|ndash|#39|#232|#224|#176|igrave|ograve|egrave|eacute|euro|rdquo|ldquo|ugrave);/g);
  var translate = new Array({ "nbsp": " ", "amp": "&", "quot": "\"", "lt": "<", "gt": ">", "agrave": "à", "deg": "°", "rsquo": "'", "hellip": "...", "ndash": " - ", "#39": "'","#232":"è","#224":"à","#176":"°", "igrave": "ì", "ograve": "ò", "egrave": "è", "eacute": "é","euro":"€","ldquo":"\"","rdquo":"\"","ugrave":"ù" });

  function _renderHtml(htmlCode) {
    var textTodecode ="";
    try{
        textTodecode = $sce.trustAsHtml(htmlCode);
    }catch(ex){
        textTodecode = "";
    }
    return textTodecode;
  }

  function _truncate(value,max) {
			value= value.replace(/<?(\/|)(strong|br \/|em|ul|li|ol)\>/g,'');

			if (!value) {return '';}
			max = parseInt(max, 10);
			if (!max) {return value;}
			if (value.length <= max){$sce.trustAsHtml(value);}

			value = value.substr(0, max);
			var lastspace = value.lastIndexOf(' ');
			if (lastspace != -1) {
					value = value.substr(0, lastspace);
			}
			return $sce.trustAsHtml(value + '…');
    }

  function _decodeHtml(information){
    try{
      var str_decodeUri=decodeURIComponent(information);
      information=str_decodeUri;
    }catch(ex){
      //console.log(information);
    }
    for (var k = 0; k < translate_re.length; k++) {
        information = information.replace(translate_re[k], function (match, entity) {
          return translate[k][entity];
        });
    }
    information=information.replace(/undefined/g, "").replace(/\|\#a\#\|/g,'<a').replace(/\|\#\/a\#\|/g,'<\/a>');
        //information =$sanitize(information);

    return information;
  }

  function _filepath(urlf){
    var return_value='';
    if ('undefined'!==typeof(urlf)){
        if (urlf !== null){
            urlf=urlf.replace(/^\s+|\s+$/g, '');
            if (urlf !== ''){return_value=apiUri.pathfile + urlf.toLowerCase().replace(/bmbMinisite viaggi e turismo\/bmbMinisitevt\//g, '');}
        }
    }
    return return_value;
  }

  function _getNumber(num) {
      //if (num === 0) {return [1];}
      return Array.apply(null, Array(num)).map(function(item,index) { return index+1; });
  }

  function _dateServerToClient(date){
    var newDate = moment(date,'YYYY-MM-DD').format('YYYY-MM-DD');
    return newDate;
  }

  function _dateTimeFromString(date, time, format){
    var strInf = date;
    if (format === undefined){ format='YYYY-MM-DD HH:mm:ss';}
    if (time !==undefined){ strInf+=(' '+ time).replace(/  /g,' ');}
    return moment(strInf,format);
  }

  function _getLimitDate() {
    return moment().add(-1, 'days').format('YYYY-MM-DD');
  }

  function _validate_email(emailAddress){
    EMAIL_REGEXP = /[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g;
    var resolution=new RegExp(EMAIL_REGEXP);
    return resolution.test(emailAddress);
  }

  function _get_time_duration(from, to) {
    var date_fix='01-01-0001 ';
    if (moment(from, "hh:mm:ss").hours()>moment(to, "hh:mm:ss").hours()){date_fix='02-01-0001 ';}
    var diff = Math.abs(moment.duration(moment(date_fix +to, "DD-MM-YYYY hh:mm:ss").diff(moment('01-01-0001 ' +from, "DD-MM-YYYY hh:mm:ss"))).asMinutes());
    var hour = Math.floor(diff/60);
    var minute = diff - (hour*60);
    return hour+'h '+minute+'min';
  }

  function _get_time_left(to) {

    if(!_.isEmpty(to)) {

      var diff = moment(moment()).diff(to),
      duration =  moment.duration(diff);

      if(moment(to).year() >= moment().year()) {
        return {
          long: function() {
            var res = "".concat(duration.days(), " days ", duration.hours(), " hours ", duration.minutes(), " minutes");
            return res.replace(/[-]/g, "");
          },
          short: function() {
            var res = "".concat(duration.days(), "d ", duration.hours(), "h ", duration.minutes(), "m");
            return res.replace(/[-]/g, "");
          }
        }
      } else {
        return {
          long: function() {
            return "".concat(moment(to).format("DD.mm.YYYY HH:mm:ss"), " time expired");
          },
          short: function() {
            return "".concat(moment(to).format("DD.mm.YYYY"), " time expired");
          }
        }
      }
    }
  }

  return {
    renderHtml:_renderHtml,
    decodeHtml:_decodeHtml,
    truncateHtml:_truncate,
    filepath:_filepath,
    getNumber: _getNumber,
    dateServerToClient: _dateServerToClient,
    getLimitDate: _getLimitDate,
    stringTodateTime:_dateTimeFromString,
    validateEmail:_validate_email,
    getTimeDuration: _get_time_duration,
    getTimeleft: _get_time_left
  };

}]);
