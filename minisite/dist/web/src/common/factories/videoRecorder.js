/* jshint   loopfunc: true */
angular.module( 'bmbMinisite.fct.videoRecorder', ['bmbMinisite.apiUri','bmbMinisite.appConf'])

.factory('fctVideoRecorder', [ '$http', '$q', 'apiUri', '$log', function ($http, $q, apiUri, $log) {

  /*

    _getAPI

  */
  function _getAPI(o) {
    var deferred = $q.defer();
    $http.get(o.url, {
      headers: o.headers
    }).success(function(data) {
        deferred.resolve(data);
     }).error(function(msg, code) {
        deferred.reject(msg);
        $log.error(msg, code);
     });
    return deferred.promise;
  }



  function _postAPI(o) {
    var deferred = $q.defer();
    $http({
      method: 'POST',
      url: o.url,
      data: o.data
    }).success(function(data) {
        _is_busy=false;
        deferred.resolve(data);
     }).error(function(msg, code) {
        _is_busy=false;
        deferred.reject(msg);
        $log.error(msg, code);
     });
    return deferred.promise;
  }


  return {};

}]);
