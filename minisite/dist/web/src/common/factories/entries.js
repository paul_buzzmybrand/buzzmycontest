/* jshint   loopfunc: true */
angular.module( 'bmbMinisite.fct.entries', ['bmbMinisite.apiUri','bmbMinisite.appConf'])

.factory('fctEntries', [ '$http', '$q', 'apiUri', '$log', 'appConf', '$sce', function ($http, $q, apiUri, $log, appConf, $sce) {

  /*

    _getAPI

  */
  function _getAPI(o) {
    var deferred = $q.defer();
    $http.get(o.url, {
      headers: o.headers
    }).success(function(data) {
        deferred.resolve(data);
     }).error(function(msg, code) {
        deferred.reject(msg);
        $log.error(msg, code);
     });
    return deferred.promise;
  }


  /*

    _order_by

  */
  function _order_by(o) {
    if(!_.isEmpty(o)) {
      return _.sortBy(o, "rank_position");
    }
    return {};
  }



  /*

    _entries_order_by

  */
  function _entries_order_by(o) {
    if(!_.isEmpty(o)) {
      return {
        morePopular: function() {
          return _.sortBy(o, "rank_position");
        },
        lessPopular: function() {
          return _.sortBy(o, "rank_position").reverse();
        }
      }
    }
  }


  /*

    _entries_model

  */
  function _entries_model(o) {

    if(!_.isEmpty(o)) {

      var type = o[0].filename.search(".mp4");

      if(type !== -1) {

        var photoUrl = "/images/videos/";

        _.forEach(o, function(item){

          if(item.image.search(photoUrl) === -1 ) {

            item.image = "https://www.buzzmybrand.com".concat(photoUrl, item.image);

            if(isMobile.apple.device) {
              item.filename = "http://www.buzzmybrand.com:1935/vod/mp4:".concat(item.filename, "/playlist.m3u8");
            }
            if(isMobile.android.device) {
              item.filename = "rtsp://www.buzzmybrand.com:1935/vod/".concat(item.filename);
            }
            else {
              item.filename = "rtmp://www.buzzmybrand.com:1935/vod/mp4:".concat(item.filename);
            }

          }
        });

        return o;
      }

      return o;
    }
  }




  /*

    _get_profile_entries

  */
  function _get_profile_entries(contestName, token) {
    var o = {
      url: apiUri.profileEntries.concat(contestName)
      /*headers: {
        "auth" : token
      }*/
    };
    return _getAPI(o);
  }



  /*

    _get_profile

  */
  function _get_profile(contestName, token) {
    var o = {
      url: apiUri.profile.concat(contestName)
      /*headers: {
        "auth" : token
      }*/
    };

    return _getAPI(o);
  }




  /*

    _getData

  */
  function _get_entries(contestName, currentPage) {
    var o = {
      url: apiUri.entries.concat(contestName, "?page=", currentPage)
    };
    return _getAPI(o);
  }


  return {
    getEntries: _get_entries,
    getProfileEntries: _get_profile_entries,
    getProfile: _get_profile,
    orderBy: _order_by,
    entriesOrderBy: _entries_order_by,
    entriesModel: _entries_model
  };

}]);
