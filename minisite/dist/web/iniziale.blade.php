<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10 " lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="it-IT" ng-app="bmbMinisite" ng-controller="AppCtrl" >
<!--<![endif]-->
  <head>
    <base href="/" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

    <link rel="apple-touch-icon" href="">
    <link rel="android-touch-icon" href="" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" type="image/ico" href="favicon.ico" />

    <title>[% $contest->name %]</title>

    <meta property="og:url" content="[% $contest->contest_route %]" />
		<meta property="og:title" content="[% $contest->name %]" />
		<meta property="og:description" content="[% $contest->description  %]" />
		<meta property="og:site_name" content="[% $contest->name %]" />
		<meta property="og:image" content="[% $contest->image %]" />

    <meta name="twitter:card" content="[% $contest->image %]">
    <meta name="twitter:site" content="[% $contest->contest_route %]">
    <meta name="twitter:creator" content="[% $contest->name %]">
    <meta name="twitter:title" content="[% $contest->name %]">
    <meta name="twitter:description" content="[% $contest->description %]">
    <meta name="twitter:image" content="[% $contest->image %]">

    <link rel="stylesheet" type="text/css" href="[% URL::asset('minisite/assets/style-bmbMinisite-1.0.67.css') %]" />
    <link rel="stylesheet" type="text/css" href="[% URL::asset('minisite/assets/themes/' . $template . '/theme-bmbMinisite-1.0.67.css') %]" />

    <!-- compiled JavaScript -->
    
    <script type="text/javascript" src="[% URL::asset('minisite/assets/app-bmbMinisite-1.0.67.js') %]"></script>
    

    <script type="text/javascript" src="[% URL::asset('minisite/assets/app.ui-bmbMinisite-1.0.67.js') %]"></script>

  </head>
  <body>

    <div id="loading-bar" class="loader">
      <div class="loader__buzzLogo"></div>
    </div>

    <div class="fading-view" ui-view="main"></div>

    <ngckie></ngckie>
  </body>

  <!-- env: compile -->
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', '', 'auto');
    ga('send', 'pageview');
  </script>
  <!-- endenv -->

</html>
