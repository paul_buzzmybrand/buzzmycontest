angular.module('templates-common', ['layout/test.tpl.html']);

angular.module("layout/test.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("layout/test.tpl.html",
    "<div class=\"test\">\n" +
    "  test\n" +
    "</div>\n" +
    "");
}]);
