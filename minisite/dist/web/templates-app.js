angular.module('templates-app', ['home/home.tpl.html']);

angular.module("home/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/home.tpl.html",
    "<div class=\"modal js-modal-open\">\n" +
    "  <div class=\"modal__close js-modal-close\"><span>X</span></div>\n" +
    "  <div class=\"modal__window\">\n" +
    "    <video class=\"video-js vjs-default-skin\" controls preload=\"auto\" width=\"640\" height=\"264\" vjs-video vjs-media=\"videoObj\"></video>\n" +
    "  </div>\n" +
    "  <div class=\"modal__overlay\"></div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<div class=\"wrapper\" ng-cloak>\n" +
    " <!-- header-->\n" +
    "\n" +
    "\n" +
    "  <!-- Stateviewall -->\n" +
    "<!--<button type=\"button\" ng-click=\"activeView('signIn--s1', 'signIn')\">signIn--s1</button>\n" +
    "<button type=\"button\" ng-click=\"activeView('signIn--s2', 'signIn')\">signIn--s2</button>\n" +
    "<button type=\"button\" ng-click=\"activeView('personal', 'personal')\">personal</button>\n" +
    "<button type=\"button\" ng-click=\"activeView('partecipate--s1', 'partecipate')\">partecipate--s1</button>\n" +
    "<button type=\"button\" ng-click=\"activeView('partecipate--s2', 'partecipate')\">partecipate--s2</button>\n" +
    "<button type=\"button\" ng-click=\"activeView('partecipate--s3', 'partecipate')\">partecipate--s3</button>\n" +
    "<button type=\"button\" ng-click=\"activeView('partecipate--s4', 'partecipate')\">partecipate--s4</button>\n" +
    "<button type=\"button\" ng-click=\"activeView('partecipate--s5', 'partecipate')\">partecipate--s5</button>\n" +
    "<button type=\"button\" ng-click=\"activeView('partecipate--s6', 'partecipate')\">partecipate--s6</button>\n" +
    "<button type=\"button\" ng-click=\"activeView('partecipate--s7', 'partecipate')\">partecipate--s7</button>-->\n" +
    "\n" +
    "\n" +
    "<!-- CSS FOR TEMPLATE CUSTOM -->\n" +
    "<style parse-style ng-if=\"customTheme.template_id == 0\">\n" +
    "  .header,\n" +
    "  .nav__signiIn,\n" +
    "  .canvasHeader__subtitle,\n" +
    "  .canvasHeader__title {\n" +
    "    color: #{{ customTheme.font_color }};\n" +
    "  }\n" +
    "  .canvasHeader__bg {\n" +
    "    background-image: url(/images/events/{{ customTheme.canvas_header }});\n" +
    "  }\n" +
    "</style>\n" +
    "\n" +
    "<header class=\"header j-fixedHeader\">\n" +
    "  <div class=\"servicesNav\">\n" +
    "    <span href=\"#\" class=\"servicesNav__label\" ng-show=\"contest.sponsor_logo != null\">\n" +
    "      {{ \"SponsoredBy\" | translate }}\n" +
    "    </span>\n" +
    "    <span href=\"#\" class=\"servicesNav__sponsor\" style=\"background-image: url({{ domainRes }}/images/events/{{ contest.sponsor_logo }})\" ng-show=\"contest.sponsor_logo != null\"></span>\n" +
    "    <nav class=\"socialNav\">\n" +
    "      <span class=\"socialNav__label\">{{ \"ShareOn\" | translate }}</span>\n" +
    "      <a class=\"socialNav__fb\" href=\"#\"\n" +
    "        socialshare\n" +
    "        socialshare-provider=\"facebook\"\n" +
    "        socialshare-url=\"{{ urlContest }}\"\n" +
    "      ></a>\n" +
    "      <a class=\"socialNav__tw\" href=\"#\"\n" +
    "        socialshare\n" +
    "        socialshare-provider=\"twitter\"\n" +
    "        socialshare-url=\"{{ urlContest }}\"\n" +
    "      ></a>\n" +
    "    </nav>\n" +
    "    <nav class=\"socialNav socialNav--email\">\n" +
    "      <span class=\"socialNav__label\" ng-click=\"activeView('invitefriend--s1', 'invitefriend')\">{{ \"InviteFriend\" | translate }}</span>\n" +
    "      <a class=\"socialNav__email\" href=\"#\" ng-click=\"activeView('invitefriend--s1', 'invitefriend')\"></a>\n" +
    "    </nav>\n" +
    "    <ul class=\"servicesCounter\">\n" +
    "      <!--<li class=\"servicesCounter__item\"><timer countdown=\"10041\" max-time-unit=\"'day'\" interval=\"1000\">{{days}}d {{hours}}h {{minutes}}m {{seconds}}s</timer></li>-->\n" +
    "      <li class=\"servicesCounter__item\">{{ contest.time_left_top }}</li>\n" +
    "    </ul>\n" +
    "  </div>\n" +
    "  <nav class=\"navigation\">\n" +
    "    <div ng-if=\"contest.status == 3\">\n" +
    "      <a class=\"nav__signiIn\" href=\"#\" ng-click=\"toastr.warning('{{ 'SheduledMsg' |  translate }}')\">{{ \"PartecipatePhoto\" | translate }}</a>\n" +
    "    </div>\n" +
    "    <div ng-if=\"contest.status == 1\">\n" +
    "      <a ng-if=\"showPartecipate(contest.type_id).photo() && !isAuthenticated()\"  class=\"nav__signiIn\" href=\"#\" ng-click=\"activeView('signIn--s1', 'signIn')\">{{ \"PartecipatePhoto\" | translate }}</a>\n" +
    "      <a ng-if=\"showPartecipate(contest.type_id).video() && !isAuthenticated()\" class=\"nav__signiIn\" href=\"#\" ng-click=\"activeView('signIn--s1', 'signIn')\">{{ \"PartecipateVideo\" | translate }}</a>\n" +
    "      <a ng-if=\"showPartecipate(contest.type_id).text() && !isAuthenticated()\" class=\"nav__signiIn\" href=\"#\" ng-click=\"activeView('signIn--s1', 'signIn')\">{{ \"PartecipateText\" | translate }}</a>\n" +
    "    </div>\n" +
    "    <ul class=\"overlayMenu\" ng-if=\"isAuthenticated()\">\n" +
    "      <li class=\"overlayMenu__item\" ng-show=\"showPartecipate(contest.type_id).photo()\">\n" +
    "        <a class=\"overlayMenu__btnLoad\" href=\"#\" ng-click=\"activeView('partecipate--s1', 'partecipate')\"></a>\n" +
    "      </li>\n" +
    "      <li class=\"overlayMenu__item\" ng-show=\"showPartecipate(contest.type_id).video()\">\n" +
    "        <a class=\"overlayMenu__btnLoad\" href=\"#\" ng-click=\"activeView('partecipate--s4', 'partecipate')\"></a>\n" +
    "      </li>\n" +
    "      <li class=\"overlayMenu__item\" ng-show=\"showPartecipate(contest.type_id).text()\">\n" +
    "        <a class=\"overlayMenu__btnLoad\" href=\"#\" ng-click=\"activeView('partecipate--s6', 'partecipate')\"></a>\n" +
    "      </li>\n" +
    "      <li class=\"overlayMenu__item\">\n" +
    "        <a class=\"overlayMenu__btnPersonal\" ng-if=\"!profileEntries.isEmpty\" ng-click=\"activeView('personal', 'personal')\" href=\"#\"></a>\n" +
    "        <a class=\"overlayMenu__btnPersonal\" ng-if=\"profileEntries.isEmpty\" ng-click=\"activeView('noUserEntries', 'partecipate')\" href=\"#\"></a>\n" +
    "      </li>\n" +
    "      <li class=\"overlayMenu__item overlayMenu__item--logout\">\n" +
    "        <a class=\"nav__logout\" ng-click=\"logout()\" href=\"#\" >{{ \"Logout\" | translate }}</a>\n" +
    "      </li>\n" +
    "    </ul>\n" +
    "  </nav>\n" +
    "</header>\n" +
    "<!-- overlay-->\n" +
    "<div class=\"overlay j-overlay\" ng-class=\"{ 'overlayOpen' : overlayIsOpen }\">\n" +
    "  <a href=\"\" class=\"overlay__close\" ng-click=\"overlayToggle().close()\">\n" +
    "    <div class=\"overlay__closeContent\">\n" +
    "      <span class=\"closeLine closeLine--first\"></span>\n" +
    "      <span class=\"closeLine closeLine--second\"></span>\n" +
    "    </div>\n" +
    "  </a>\n" +
    "  <!-- overlay item-->\n" +
    "  <div class=\"overlay__contents j-overlayContents\">\n" +
    "\n" +
    "    <div class=\"overlay__item j-overlayItem\" ng-class=\"{ 'activeViewItem' : activeViewItem == 'invitefriend' }\">\n" +
    "\n" +
    "      <div class=\"overlay__title\">{{ \"InviteFriend\" | translate }}</div>\n" +
    "\n" +
    "      <div class=\"signIn signIn--s2\" ng-class=\"{ 'activeView' : stateView == 'invitefriend--s1' }\" >\n" +
    "          <form name=\"inviteFriendForm\" novalidate id=\"inviteFriendForm\">\n" +
    "              <div class=\"genericForm\">\n" +
    "                <fieldset class=\"genericForm__field j-field\">\n" +
    "                  <label class=\"genericForm__label\"  for=\"nameS3\">\n" +
    "                    <input class=\"genericForm__input j-fieldItem\" ng-class=\"{ 'filled' : inviteFriendForm.nameS3.$valid }\" ng-model=\"nameS3\" type=\"text\" id=\"nameS3\" name=\"nameS3\" autocomplete=\"off\" required>\n" +
    "                    <span>{{ \"YourName\" | translate }}</span>\n" +
    "                  </label>\n" +
    "                  <span class=\"genericForm__error\">{{ \"Errore\" | translate }}</span>\n" +
    "                </fieldset>\n" +
    "\n" +
    "                <fieldset class=\"genericForm__field j-field\">\n" +
    "                  <label class=\"genericForm__label\"  for=\"addresseeNameS3\">\n" +
    "                    <input class=\"genericForm__input j-fieldItem\" ng-class=\"{ 'filled' : inviteFriendForm.recipientS3.$valid }\"  ng-model=\"addresseeNameS3\" type=\"text\" id=\"addresseeNameS3\" name=\"addresseeNameS3\" autocomplete=\"off\" required>\n" +
    "                    <span>{{ \"YourFriendName\" | translate }}</span>\n" +
    "                  </label>\n" +
    "                  <span class=\"genericForm__error\">{{ \"Errore\" | translate }}</span>\n" +
    "                </fieldset>\n" +
    "\n" +
    "                <fieldset class=\"genericForm__field j-field\">\n" +
    "                  <label class=\"genericForm__label\"  for=\"recipientS3\">\n" +
    "                    <input class=\"genericForm__input j-fieldItem\" ng-class=\"{ 'filled' : inviteFriendForm.emailS2.$valid }\"  ng-model=\"recipientS3\" type=\"email\" id=\"recipientS3\" name=\"recipientS3\" autocomplete=\"off\" required>\n" +
    "                    <span>{{ \"YourFriendEmail\" | translate }}</span>\n" +
    "                  </label>\n" +
    "                  <span class=\"genericForm__error\">{{ \"Errore\" | translate }}</span>\n" +
    "                </fieldset>\n" +
    "\n" +
    "                <p ng-show=\"enabledInviteMessageSuccessMessage\" class=\"overlay__inviteFriendMessage\">{{ \"MessageSent\" | translate }}</p>\n" +
    "\n" +
    "                <a href=\"#\" class=\"genericForm__btn\" ng-click=\"sendInviteFriend()\">{{ \"Invite\" | translate }}</a><br>\n" +
    "              </div>\n" +
    "          </form>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "\n" +
    "      <div class=\"overlay__item j-overlayItem\" ng-class=\"{ 'activeViewItem' : activeViewItem == 'signIn' }\">\n" +
    "\n" +
    "        <div class=\"overlay__title\" ng-if=\"showPartecipate(contest.type_id).photo()\"> {{ \"PartecipatePhoto\" | translate }}</div>\n" +
    "        <div class=\"overlay__title\" ng-if=\"showPartecipate(contest.type_id).video()\">{{ \"PartecipateVideo\" | translate }}</div>\n" +
    "        <div class=\"overlay__title\" ng-if=\"showPartecipate(contest.type_id).text()\">{{ \"PartecipateText\" | translate }}</div>\n" +
    "\n" +
    "        <div class=\"signIn signIn--s2\" ng-class=\"{ 'activeView' : stateView == 'signIn--s1' }\" ng-show=\"signInFields.registration_needed\">\n" +
    "          <div class=\"signIn__col signIn__col--left\" >\n" +
    "            <form name=\"signUpForm\" novalidate id=\"signUpForm\">\n" +
    "                <div class=\"genericForm\">\n" +
    "                  <fieldset ng-if=\"signInFields.fields.name == 1\" class=\"genericForm__field j-field\">\n" +
    "                    <label class=\"genericForm__label\"  for=\"nameS2\">\n" +
    "                      <input class=\"genericForm__input j-fieldItem\" ng-class=\"{ 'filled' : signUpForm.nameS2.$valid }\" ng-blur=\"submitSignUpNameS2()\" on-key-down=\"submitSignUp()\" ng-model=\"nameS2\" type=\"text\" id=\"nameS2\" name=\"nameS2\" autocomplete=\"off\" required>\n" +
    "                      <span>{{ \"Name\" | translate }}</span>\n" +
    "                    </label>\n" +
    "                    <span class=\"genericForm__error\" ng-hide=\"signUpFormNameS2Valid\">{{ \"ErroreName\" | translate }}</span>\n" +
    "                  </fieldset>\n" +
    "\n" +
    "                <fieldset ng-if=\"signInFields.fields.surname == 1\" class=\"genericForm__field j-field\">\n" +
    "                    <label class=\"genericForm__label\"  for=\"surnameS2\">\n" +
    "                      <input class=\"genericForm__input j-fieldItem\" ng-class=\"{ 'filled' : signUpForm.surnameS2.$valid }\" ng-blur=\"submitSignUpSurnameS2()\" on-key-down=\"submitSignUp()\" ng-model=\"surnameS2\" type=\"text\" id=\"surnameS2\" name=\"surnameS2\" autocomplete=\"off\" required>\n" +
    "                      <span>{{ \"Surname\" | translate }}</span>\n" +
    "                    </label>\n" +
    "                    <span class=\"genericForm__error\" ng-hide=\"signUpFormSurnameS2Valid\">{{ \"ErroreEmail\" | translate }}</span>\n" +
    "                  </fieldset>\n" +
    "\n" +
    "                  <fieldset ng-if=\"signInFields.fields.birthdate == 1\" class=\"genericForm__field j-field\">\n" +
    "                    <label class=\"genericForm__label\"  for=\"birthdateS2\">\n" +
    "                      <input class=\"genericForm__input j-fieldItem\" ng-class=\"{ 'filled' : signUpForm.birthdateS2.$valid }\" ng-blur=\"submitSignUpBirthdateS2()\" on-key-down=\"submitSignUp()\" ng-model=\"birthdateS2\" type=\"text\" id=\"birthdateS2\" name=\"birthdateS2\" autocomplete=\"off\" required format=\"number\">\n" +
    "                      <span>{{ \"Birthdate\" | translate }}</span>\n" +
    "                    </label>\n" +
    "                    <span class=\"genericForm__error\" ng-hide=\"signUpFormBirthdateS2Valid\">{{ \"ErroreBirthdate\" | translate }}</span>\n" +
    "                  </fieldset>\n" +
    "\n" +
    "                  <fieldset ng-if=\"signInFields.fields.email == 1\" class=\"genericForm__field j-field\">\n" +
    "                    <label class=\"genericForm__label\"  for=\"emailS2\">\n" +
    "                      <input class=\"genericForm__input j-fieldItem\" ng-class=\"{ 'filled' : signUpForm.emailS2.$valid }\" ng-blur=\"submitSignUpEmailS2()\" on-key-down=\"submitSignUp()\" ng-model=\"emailS2\" type=\"email\" id=\"emailS2\" name=\"emailS2\" autocomplete=\"off\" required>\n" +
    "                      <span>{{ \"Email\" | translate }}</span>\n" +
    "                    </label>\n" +
    "                    <span class=\"genericForm__error\" ng-hide=\"signUpFormEmailS2Valid\">{{ \"ErroreEmail\" | translate }}</span>\n" +
    "                  </fieldset>\n" +
    "\n" +
    "                  <fieldset ng-if=\"signInFields.fields.phone == 1\" class=\"genericForm__field j-field\">\n" +
    "                    <label class=\"genericForm__label\"  for=\"phoneS2\">\n" +
    "                      <input class=\"genericForm__input j-fieldItem\" ng-class=\"{ 'filled' : signUpForm.phoneS2.$valid }\" ng-blur=\"submitSignUpPhoneS2()\" on-key-down=\"submitSignUp()\" ng-model=\"phoneS2\" type=\"text\" id=\"phoneS2\" name=\"phoneS2\" autocomplete=\"off\" required>\n" +
    "                      <span>{{ \"Phone\" | translate }}</span>\n" +
    "                    </label>\n" +
    "                    <span class=\"genericForm__error\" ng-hide=\"signUpFormPhoneS2Valid\">{{ \"ErrorePhone\" | translate }}</span>\n" +
    "                  </fieldset>\n" +
    "\n" +
    "                  <fieldset ng-if=\"signInFields.fields.cap == 1\" class=\"genericForm__field j-field\">\n" +
    "                    <label class=\"genericForm__label\"  for=\"capS2\">\n" +
    "                    <input class=\"genericForm__input j-fieldItem\" ng-class=\"{ 'filled' : signUpForm.capS2.$valid }\" ng-blur=\"submitSignUpCapS2()\" on-key-down=\"submitSignUp()\" ng-model=\"capS2\" type=\"text\" id=\"capS2\" name=\"capS2\" autocomplete=\"off\" required>\n" +
    "                      <span>{{ \"Cap\" | translate }}</span>\n" +
    "                  </label>\n" +
    "                    <span class=\"genericForm__error\" ng-hide=\"signUpFormCapS2Valid\">{{ \"ErroreCap\" | translate }}</span>\n" +
    "                  </fieldset>\n" +
    "\n" +
    "                  <fieldset ng-if=\"signInFields.fields.city == 1\" class=\"genericForm__field j-field\">\n" +
    "                    <label class=\"genericForm__label\"  for=\"cityS2\">\n" +
    "                      <input class=\"genericForm__input j-fieldItem\" ng-class=\"{ 'filled' : signUpForm.cityS2.$valid }\" ng-blur=\"submitSignUpCityS2()\" on-key-down=\"submitSignUp()\" ng-model=\"cityS2\" type=\"text\" id=\"cityS2\" name=\"cityS2\" autocomplete=\"off\" required>\n" +
    "                      <span>{{ \"City\" | translate }}</span>\n" +
    "                    </label>\n" +
    "                    <span class=\"genericForm__error\" ng-hide=\"signUpFormCityS2Valid\">{{ \"ErroreCity\" | translate }}</span>\n" +
    "                  </fieldset>\n" +
    "\n" +
    "                  <!--<p>\n" +
    "                    <span ng-show=\"signInDone\">Thank you, <br>\n" +
    "                    we need few more steps to complete the registration</span>\n" +
    "                    <input class=\"genericForm__btn disabled genericForm__btn--desktop\" ng-click=\"submit()\" type=\"submit\" value=\"Sign Up\">\n" +
    "                  </p>-->\n" +
    "                </div>\n" +
    "            </form>\n" +
    "          </div>\n" +
    "          <div class=\"signIn__col signIn__col--right\">\n" +
    "            <div class=\"externalsignIn\" ng-if=\"enabledSocialLogin\">\n" +
    "              <a href=\"#\" class=\"externalsignIn__btn\" ng-if=\"contest.isFacebookLogin\" ng-class=\"{ 'enabled' : enabledSocialLogin }\" ng-click=\"authenticate('facebook')\"><span class=\"externalsignIn__fb\"></span>\n" +
    "                <span ng-if=\"isLogged.facebook\">{{ \"SocialLogged\" | translate }}</span>\n" +
    "                <span ng-if=\"!isLogged.facebook\">{{ \"SignInWithFacebook\" | translate }}</span>\n" +
    "              </a> <br>\n" +
    "              <a href=\"#\" class=\"externalsignIn__btn\" ng-if=\"contest.isTwitterLogin\" ng-class=\"{ 'enabled' : enabledSocialLogin }\" ng-click=\"authenticate('twitter')\"><span class=\"externalsignIn__tw\"></span>\n" +
    "                <span ng-if=\"isLogged.twitter\">{{ \"SocialLogged\" | translate }}</span>\n" +
    "                <span ng-if=\"!isLogged.twitter\">{{ \"SignInWithTwitter\" | translate }}</span>\n" +
    "              </a>\n" +
    "              <div ng-if=\"isAuthenticated()\">\n" +
    "                <a href=\"#\" class=\"genericForm__btn genericForm__btn--continue\" ng-show=\"showPartecipate(contest.type_id).photo()\" ng-click=\"activeView('partecipate--s1', 'partecipate')\"><span>{{ \"Continue\" | translate }}</span></a>\n" +
    "                <a href=\"#\" class=\"genericForm__btn genericForm__btn--continue\" ng-show=\"showPartecipate(contest.type_id).video()\" ng-click=\"activeView('partecipate--s4', 'partecipate')\"><span>{{ \"Continue\" | translate }}</span></a>\n" +
    "                <a href=\"#\" class=\"genericForm__btn genericForm__btn--continue\" ng-show=\"showPartecipate(contest.type_id).text()\" ng-click=\"activeView('partecipate--s6', 'partecipate')\"><span>{{ \"Continue\" | translate }}</span></a>\n" +
    "              </div>\n" +
    "              <p class=\"overlay__loginInfo\" ng-if=\"contest.isFacebookLogin && contest.isTwitterLogin\">{{ \"ConnectBothSocial\" | translate }}</p>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"signIn signIn--s1\" ng-class=\"{ 'activeView' : stateView == 'signIn--s1' }\" ng-hide=\"signInFields.registration_needed\">\n" +
    "          <div class=\"signIn__col signIn__col--left\" >\n" +
    "            <div class=\"externalsignIn\" ng-if=\"enabledSocialLogin\">\n" +
    "              <a href=\"#\" class=\"externalsignIn__btn\" ng-if=\"contest.isFacebookLogin\" ng-class=\"{ 'enabled' : enabledSocialLogin }\" ng-click=\"authenticate('facebook')\"><span class=\"externalsignIn__fb\"></span>\n" +
    "                <span ng-if=\"isLogged.facebook\">{{ \"SocialLogged\" | translate }}</span>\n" +
    "                <span ng-if=\"!isLogged.facebook\">{{ \"SignInWithFacebook\" | translate }}</span>\n" +
    "              </a> <br>\n" +
    "              <a href=\"#\" class=\"externalsignIn__btn\" ng-if=\"contest.isTwitterLogin\" ng-class=\"{ 'enabled' : enabledSocialLogin }\" ng-click=\"authenticate('twitter')\"><span class=\"externalsignIn__tw\"></span>\n" +
    "                <span ng-if=\"isLogged.twitter\">{{ \"SocialLogged\" | translate }}</span>\n" +
    "                <span ng-if=\"!isLogged.twitter\">{{ \"SignInWithTwitter\" | translate }}</span>\n" +
    "              </a>\n" +
    "              <div ng-if=\"isAuthenticated()\">\n" +
    "                <a href=\"#\" class=\"genericForm__btn genericForm__btn--continue\" ng-show=\"showPartecipate(contest.type_id).photo()\" ng-click=\"activeView('partecipate--s1', 'partecipate')\"><span>{{ \"Continue\" | translate }}</span></a>\n" +
    "<a href=\"#\" class=\"genericForm__btn genericForm__btn--continue\" ng-show=\"showPartecipate(contest.type_id).video()\" ng-click=\"activeView('partecipate--s4', 'partecipate')\"><span>{{ \"Continue\" | translate }}</span></a>\n" +
    "                <a href=\"#\" class=\"genericForm__btn genericForm__btn--continue\" ng-show=\"showPartecipate(contest.type_id).text()\" ng-click=\"activeView('partecipate--s6', 'partecipate')\"><span>{{ \"Continue\" | translate }}</span></a>\n" +
    "              </div>\n" +
    "              <p class=\"overlay__loginInfo\" ng-if=\"contest.isFacebookLogin && contest.isTwitterLogin\">{{ \"ConnectBothSocial\" | translate }}</p>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <!-- sign In step 2 -->\n" +
    "        <div class=\"signIn signIn--s2\" ng-class=\"{ 'activeView' : stateView == 'signIn--s2' }\">\n" +
    "          <!--<form name=\"signUpForm\" novalidate id=\"signUpForm\">\n" +
    "            <div class=\"signIn__col signIn__col--left\">\n" +
    "              <div class=\"genericForm\">\n" +
    "\n" +
    "                <fieldset ng-if=\"signInFields.fields.name == 1\" class=\"genericForm__field j-field\">\n" +
    "                  <label class=\"genericForm__label\"  for=\"name\">\n" +
    "                    <input class=\"genericForm__input j-fieldItem\" ng-model=\"user.name\" type=\"text\" id=\"name\" name=\"name\" autocomplete=\"off\" required>\n" +
    "                    <span>Name</span>\n" +
    "                  </label>\n" +
    "                  <span class=\"genericForm__error\">{{ \"Errore\" | translate }}</span>\n" +
    "                </fieldset>\n" +
    "\n" +
    "                <fieldset ng-if=\"signInFields.fields.surname == 1\" class=\"genericForm__field j-field\">\n" +
    "                  <label class=\"genericForm__label\"  for=\"surname\">\n" +
    "                    <input class=\"genericForm__input j-fieldItem\" ng-model=\"user.surname\" type=\"text\" id=\"surname\" name=\"surname\" autocomplete=\"off\" required>\n" +
    "                  <span>Surname</span>\n" +
    "                  </label>\n" +
    "                  <span class=\"genericForm__error\">{{ \"Errore\" | translate }}</span>\n" +
    "                </fieldset>\n" +
    "\n" +
    "                <fieldset ng-if=\"signInFields.fields.birthdate == 1\" class=\"genericForm__field j-field\">\n" +
    "                  <label class=\"genericForm__label\"  for=\"birthdate\">\n" +
    "                    <input class=\"genericForm__input j-fieldItem\" ng-model=\"user.birthdate\" type=\"text\" id=\"birthdate\" name=\"birthdate\" autocomplete=\"off\" required>\n" +
    "                    <span>Birthdate</span>\n" +
    "                  </label>\n" +
    "                  <span class=\"genericForm__error\">{{ \"Errore\" | translate }}</span>\n" +
    "                </fieldset>\n" +
    "\n" +
    "                <fieldset ng-if=\"signInFields.fields.email == 1\" class=\"genericForm__field j-field\">\n" +
    "                  <label class=\"genericForm__label\"  for=\"emailS2\">\n" +
    "                    <input class=\"genericForm__input j-fieldItem\" ng-model=\"user.email\" type=\"email\" id=\"emailS2\" name=\"emailS2\" autocomplete=\"off\" required>\n" +
    "                    <span>Email</span>\n" +
    "                  </label>\n" +
    "                  <span class=\"genericForm__error\">{{ \"Errore\" | translate }}</span>\n" +
    "                </fieldset>\n" +
    "\n" +
    "                <fieldset ng-if=\"signInFields.fields.phone == 1\" class=\"genericForm__field j-field\">\n" +
    "                  <label class=\"genericForm__label\"  for=\"phone\">\n" +
    "                    <input class=\"genericForm__input j-fieldItem\" ng-model=\"user.phone\" type=\"text\" id=\"phone\" name=\"phone\" autocomplete=\"off\" required>\n" +
    "                    <span>Phone</span>\n" +
    "                  </label>\n" +
    "                  <span class=\"genericForm__error\">{{ \"Errore\" | translate }}</span>\n" +
    "                </fieldset>\n" +
    "\n" +
    "                <fieldset ng-if=\"signInFields.fields.cap == 1\" class=\"genericForm__field j-field\">\n" +
    "                  <label class=\"genericForm__label\"  for=\"cap\">\n" +
    "                    <input class=\"genericForm__input j-fieldItem\" ng-model=\"user.cap\" type=\"text\" id=\"cap\" name=\"cap\" autocomplete=\"off\" required>\n" +
    "                    <span>Cap</span>\n" +
    "                  </label>\n" +
    "                  <span class=\"genericForm__error\">{{ \"Errore\" | translate }}</span>\n" +
    "                </fieldset>\n" +
    "\n" +
    "                <fieldset ng-if=\"signInFields.fields.city == 1\" class=\"genericForm__field j-field\">\n" +
    "                  <label class=\"genericForm__label\"  for=\"city\">\n" +
    "                    <input class=\"genericForm__input j-fieldItem\" ng-model=\"user.city\" type=\"text\" id=\"city\" name=\"city\" autocomplete=\"off\" required>\n" +
    "                    <span>City</span>\n" +
    "                  </label>\n" +
    "                  <span class=\"genericForm__error\">{{ \"Errore\" | translate }}</span>\n" +
    "                </fieldset>\n" +
    "\n" +
    "              </div>\n" +
    "            </div>\n" +
    "            <div class=\"signIn__col signIn__col--right\">\n" +
    "              <p class=\"signIn__text\">\n" +
    "                <span ng-show=\"signInDone\">Thank you, <br>\n" +
    "                we need few more steps to complete the registration</span>\n" +
    "                <input class=\"genericForm__btn disabled genericForm__btn--desktop\" ng-click=\"submit()\" type=\"submit\" value=\"Sign Up\">\n" +
    "              </p>\n" +
    "            </div>\n" +
    "          </form>\n" +
    "          <input class=\"genericForm__btn disabled genericForm__btn--mobile\"  ng-click=\"submit()\" type=\"submit\" value=\"Sign Up\">-->\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"overlay__bottomInfo\">\n" +
    "          <span data-ng-bind-html=\"'BySigningIn' | translate | htmlSafe\"></span>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"overlay__item j-overlayItem overlay--personal\" ng-class=\"{ 'activeView' : stateView == 'personal' }\">\n" +
    "        <div class=\"overlay__title\">{{ profile.name }}</div>\n" +
    "        <div class=\"myListWrapper\">\n" +
    "          <ul class=\"myList\">\n" +
    "            <li class=\"myList__item\" ng-repeat=\"pe in profileEntries\">\n" +
    "              <div class=\"myList__col\">\n" +
    "                <h4 class=\"myList__name\">\n" +
    "                  <span class=\"myList__label\">{{ \"Name\" | translate }}</span>\n" +
    "                  <span class=\"myList__info\">{{ pe.name }}</span>\n" +
    "                </h4>\n" +
    "                <p class=\"myList__rank\">\n" +
    "                  <span class=\"myList__label\">{{ \"Rank\" | translate }}</span>\n" +
    "                  <span class=\"myList__info\">{{ pe.rank_position }}</span>\n" +
    "                </p>\n" +
    "                <p class=\"myList__buzz\">\n" +
    "              <span class=\"myList__label\">{{ \"SocialBuzz\" | translate }}</span>\n" +
    "              <span class=\"myList__info\">{{ pe.score }}</span>\n" +
    "            </p>\n" +
    "          </div>\n" +
    "          <div class=\"myList__col\">\n" +
    "              <div class=\"myList__share\">\n" +
    "                <span class=\"myList__shareLabel\">{{ \"Share\" | translate }}</span>\n" +
    "\n" +
    "                <div ng-if=\"contest.type_id == contestType.photos\">\n" +
    "                  <a href=\"#\" class=\"myList__shareFb\"\n" +
    "                    socialshare\n" +
    "                    socialshare-provider=\"facebook\"\n" +
    "                    socialshare-url=\"{{ domain }}/FacebookSharePhoto/{{ entry.image | getHtml }}\"></a>\n" +
    "                  <a href=\"#\" class=\"myList__shareTw\"\n" +
    "                    socialshare\n" +
    "                    socialshare-provider=\"twitter\"\n" +
    "                    socialshare-url=\"{{ domain }}/FacebookSharePhoto/{{ entry.image | getHtml }}\"></a>\n" +
    "                </div>\n" +
    "\n" +
    "                <div ng-if=\"contest.type_id == contestType.videos\">\n" +
    "                  <a href=\"#\" class=\"myList__shareFb\"\n" +
    "                    socialshare\n" +
    "                    socialshare-provider=\"facebook\"\n" +
    "                    socialshare-url=\"{{ domain }}/FacebookShare/{{ entry.image | getHtml }}\"></a>\n" +
    "                  <a href=\"#\" class=\"myList__shareTw\"\n" +
    "                    socialshare\n" +
    "                    socialshare-provider=\"twitter\"\n" +
    "                    socialshare-url=\"{{ domain }}/FacebookShare/{{ entry.image | getHtml }}\"></a>\n" +
    "                </div>\n" +
    "\n" +
    "                <div ng-if=\"contest.type_id == contestType.text\">\n" +
    "                  <a href=\"#\" class=\"myList__shareFb\"\n" +
    "                    socialshare\n" +
    "                    socialshare-provider=\"facebook\"\n" +
    "                    socialshare-url=\"{{ domain }}/FacebookShareEssay/{{ entry.image | getHtml }}\"></a>\n" +
    "                  <a href=\"#\" class=\"myList__shareTw\"\n" +
    "                    socialshare\n" +
    "                    socialshare-provider=\"twitter\"\n" +
    "                    socialshare-url=\"{{ domain }}/FacebookShareEssay/{{ entry.image | getHtml }}\"></a>\n" +
    "                </div>\n" +
    "\n" +
    "              </div>\n" +
    "              <div class=\"myList__video\" ng-if=\"contest.type_id == contestType.videos\">\n" +
    "                <a class=\"myList__videoPlay\"></a>\n" +
    "              </div>\n" +
    "              <div class=\"myList__image\" ng-if=\"contest.type_id == contestType.photos\">\n" +
    "                <img src=\"{{ domainRes }}/photos/{{ pe.image }}\" alt=\"{{ pe.name }}\" />\n" +
    "              </div>\n" +
    "              <div class=\"myList__image\" ng-if=\"contest.type_id == contestType.text\">\n" +
    "                <img src=\"{{ domainRes }}/essays/{{ pe.image }}\" alt=\"{{ pe.name }}\" />\n" +
    "              </div>\n" +
    "          </div>\n" +
    "        </li>\n" +
    "      </ul>\n" +
    "    </div>\n" +
    "    <div class=\"overlay__bottomInfo\">\n" +
    "      <span data-ng-bind-html=\"'TermsOfServiceAndPrivacyPolicy' | translate | htmlSafe\"></span>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "\n" +
    "  <div class=\"overlay__item j-overlayItem\" ng-class=\"{ 'activeViewItem' : activeViewItem == 'partecipate' }\" >\n" +
    "    <!-- upload video s1 upload and record button -->\n" +
    "    <div class=\"partecipate\" ng-class=\"{ 'activeView' : stateView == 'partecipate--s1' }\">\n" +
    "\n" +
    "      <!--<div class=\"recapVideo\" ng-class=\"{ 'recording' : recording }\" ng-if=\"isHTML5\">\n" +
    "        <video controls autoplay id=\"recordVideo\"></video>\n" +
    "      </div>-->\n" +
    "\n" +
    "      <div class=\"partecipate__content\" ng-if=\"contest.type_id == contestType.photos\">\n" +
    "        <div class=\"partecipate__btn partecipate__upload\" ngf-select=\"sendContest($file).step1()\" ngf-fix-orientation=\"true\" ng-model=\"file\" name=\"file\" ngf-pattern=\"'image/*'\"\n" +
    "          ngf-accept=\"'image/*'\" ngf-max-size=\"20MB\" ngf-min-height=\"100\" ngf-resize=\"{width: 800, height: 600}\">{{ \"Upload\" | translate }}</div>\n" +
    "      </div>\n" +
    "\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <!-- if record strem s2 record btn -->\n" +
    "    <div class=\"partecipate\" ng-class=\"{ 'activeView' : stateView == 'partecipate--s2' }\">\n" +
    "      <div class=\"partecipate__content\">\n" +
    "        <a href=\"#\" class=\"partecipate__btn partecipate__startRecord\">\n" +
    "          {{ \"StartRecord\" | translate }}\n" +
    "        </a>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <!-- if record stream upload video s3 countdown and video stremaing container -->\n" +
    "    <div class=\"partecipate\" ng-class=\"{ 'activeView' : stateView == 'partecipate--s3' }\">\n" +
    "      <div class=\"partecipate__content\">\n" +
    "        <span class=\"partecipate__countdown\">10</span>\n" +
    "        <!-- <a href=\"#\" class=\"partecipate__btn partecipate__pause\"></a> -->\n" +
    "      </div>\n" +
    "      <div class=\"partecipate__VideoStream\"></div>\n" +
    "    </div>\n" +
    "    <!-- upload video s4 Recap form -->\n" +
    "    <div class=\"partecipate\" ng-class=\"{ 'activeView' : stateView == 'partecipate--s4' }\">\n" +
    "      <div class=\"partecipate__recap\">\n" +
    "        <div class=\"partecipate__recapCol\"></div>\n" +
    "        <div class=\"partecipate__recapCol\">\n" +
    "          <div class=\"recapUpload\">\n" +
    "\n" +
    "\n" +
    "            <div class=\"recapUpload__col\" ng-show=\"activeVideoForm\">\n" +
    "              <form>\n" +
    "                <fieldset class=\"genericForm__field j-field\">\n" +
    "                  <label class=\"genericForm__label\"  for=\"videoName\">\n" +
    "                    <input class=\"genericForm__input j-fieldItem\" type=\"text\" ng-model=\"sendVideoForm__title\" id=\"videoName\" name=\"name\">\n" +
    "                    <span>{{ \"VideoName\" | translate }}</span>\n" +
    "                  </label>\n" +
    "                  <!--<span class=\"genericForm__error\">{{ \"Errore\" | translate }}</span>-->\n" +
    "                </fieldset>\n" +
    "                <div class=\"genericForm__hashtag\">\n" +
    "                  <span>{{ contest.hashtag }}</span>\n" +
    "                </div>\n" +
    "                <input class=\"genericForm__btn disabled\" ng-click=\"recordAgain()\" type=\"submit\" value=\"Record Again\">\n" +
    "                <input class=\"genericForm__btn\" ng-click=\"saveVideo()\" type=\"submit\" value=\"Confirm\">\n" +
    "              </form>\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "            <div class=\"recapUpload__col\" ng-show=\"!activeVideoForm\">\n" +
    "              <div class=\"partecipate__content\" ng-if=\"contest.type_id == contestType.videos\">\n" +
    "                <!--<input type=\"file\" class=\"partecipate__btn partecipate__upload\">{{ \"Upload\" | translate }}</a>-->\n" +
    "                <a href=\"#\" ng-click=\"startRecording()\" ng-show=\"rec\" class=\"partecipate__btn partecipate__startRecord\">{{ \"StartRecord\" | translate }}</a>\n" +
    "                <a href=\"#\" ng-click=\"stopRecording()\" ng-show=\"enabledStop && pause == false\" class=\"partecipate__btn partecipate__startRecord\">{{ \"Stop\" | translate }}</a>\n" +
    "                <a href=\"#\" ng-click=\"pauseRecording()\"  ng-show=\"enabledStop && pause == false\" class=\"partecipate__btn partecipate__pause\">{{ \"Pause\" | translate }}</a>\n" +
    "                <a href=\"#\" ng-click=\"resumeRecording()\" ng-show=\"enabledStop && pause\" class=\"partecipate__btn recapVideo__play--resume\">{{ \"Resume\" | translate }}</a>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "            <div class=\"recapUpload__col recapUpload__col--2\">\n" +
    "              <div id=\"flashContent\" ng-if=\"!isHTML5 && showPartecipate(contest.type_id).video()\">\n" +
    "          			<form method=\"post\" enctype=\"multipart/form-data\" id=\"recordingForm\">\n" +
    "          				<input name=\"FileInput\" id=\"FileInput\" type=\"file\" accept=\"video/*\" capture=\"camcorder\"/>\n" +
    "          				<input type=\"submit\"  id=\"submit-btn\" value=\"Upload\" />\n" +
    "          				<img src=\"../website_public/hdfvr/ajax-loader.gif\" id=\"loading-img\" style=\"display:none;\" alt=\"Please Wait\"/>\n" +
    "          			</form>\n" +
    "          			<div id=\"progressbox\">\n" +
    "                  <div id=\"progressbar\"></div>\n" +
    "                  <div id=\"statustxt\">0%</div>\n" +
    "                </div>\n" +
    "          			<div id=\"output\"></div>\n" +
    "          			<video id='recorderVideo' controls width=\"320\" height=\"240\">\n" +
    "          				<source src=\"\">\n" +
    "          			</video>\n" +
    "        			</div>\n" +
    "              <!--<a class=\"recapVideo__play\" ng-click=\"playVideo()\"></a>-->\n" +
    "            </div>\n" +
    "\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <!-- upload video s5 Feedback -->\n" +
    "    <div class=\"partecipate\" ng-class=\"{ 'activeView' : stateView == 'partecipate--s5' }\">\n" +
    "      <div class=\"partecipate__content\">\n" +
    "        <p class=\"uploadFeedback\">\n" +
    "          <span data-ng-bind-html=\"'ThanksPartecipate' | translate | htmlSafe\"></span>\n" +
    "        </p>\n" +
    "        <p class=\"uploadFeedback__share\">\n" +
    "          <span data-ng-bind-html=\"'ShareMsn' | translate | htmlSafe\"></span>\n" +
    "        </p>\n" +
    "        <div class=\"uploadFeedback__link\">\n" +
    "            <a class=\"uploadFeedback__fb\" href=\"#\"\n" +
    "              socialshare\n" +
    "              socialshare-provider=\"facebook\"\n" +
    "              socialshare-url=\"{{ urlContest }}\"\n" +
    "            ></a>\n" +
    "            <a class=\"uploadFeedback__tw\" href=\"#\"\n" +
    "              socialshare\n" +
    "              socialshare-provider=\"twitter\"\n" +
    "              socialshare-url=\"{{ urlContest }}\"\n" +
    "            ></a>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"partecipate noUserEntries\" ng-class=\"{ 'activeView' : stateView == 'noUserEntries' }\">\n" +
    "      <div class=\"partecipate__content\">\n" +
    "        <p class=\"uploadFeedback\">\n" +
    "          <span data-ng-bind-html=\"'NoUserEntriesMsn' | translate | htmlSafe\"></span>\n" +
    "        </p>\n" +
    "        <a href=\"#\" class=\"canvasHeader__partecipate\" ng-click=\"activeView('partecipate--s1', 'partecipate')\" ng-if=\"showPartecipate(contest.type_id).photoLogged()\">{{ \"PartecipatePhoto\" | translate }}</a>\n" +
    "        <a href=\"#\" class=\"canvasHeader__partecipate\" ng-click=\"activeView('partecipate--s1', 'partecipate')\" ng-if=\"showPartecipate(contest.type_id).videoLogged()\">{{ \"PartecipateVideo\" | translate }}</a>\n" +
    "        <a href=\"#\" class=\"canvasHeader__partecipate canvasHeader__partecipateText\" ng-click=\"activeView('partecipate--s6', 'partecipate')\" ng-if=\"showPartecipate(contest.type_id).textLogged()\">{{ \"PartecipateText\" | translate }}</a>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"partecipate\" ng-class=\"{ 'activeView' : stateView == 'partecipate--s6' }\">\n" +
    "      <div class=\"partecipate__content\">\n" +
    "        <form name=\"esSaysForm\">\n" +
    "          <fieldset class=\"partecipate__text\">\n" +
    "            <textarea class=\"genericForm__textArea\" name=\"sentence\" id=\"\" cols=\"30\" rows=\"10\" ng-model=\"sentence\" placeholder=\"insert your text\"></textarea>\n" +
    "          </fieldset>\n" +
    "          <button class=\"genericForm__btn\" ng-click=\"sendContest().esSaysStep1()\">{{ \"Confirm\" | translate }}</button>\n" +
    "        </form>\n" +
    "      </div>\n" +
    "      </div>\n" +
    "    <!-- upload text s2 Recap form -->\n" +
    "    <div class=\"partecipate\" ng-class=\"{ 'activeView' : stateView == 'partecipate--s7' }\">\n" +
    "        <div class=\"partecipate__recap\">\n" +
    "          <div class=\"partecipate__recapCol\"></div>\n" +
    "          <div class=\"partecipate__recapCol\">\n" +
    "            <div class=\"recapUpload\">\n" +
    "              <div class=\"recapUpload__col\">\n" +
    "                <form name=\"sendContestForm\" class=\"sendContestForm\">\n" +
    "                  <fieldset class=\"genericForm__field j-field\">\n" +
    "                    <label class=\"genericForm__label\"  for=\"videoName\">\n" +
    "                      <input class=\"genericForm__input j-fieldItem\" type=\"text\" ng-model=\"sendContestForm__title\" id=\"videoName\" placeholder=\"{{ 'TitlePlaceholder' | translate }}\" autocomplete=\"off\" required name=\"title\">\n" +
    "                    </label>\n" +
    "                    <span class=\"genericForm__error\" ng-show=\"sendContestForm__ErrorName\">{{ \"TitleError\" | translate }}</span>\n" +
    "                  </fieldset>\n" +
    "                  <div class=\"genericForm__hashtag\">\n" +
    "                    <span>{{ contest.hashtag }}</span>\n" +
    "                  </div>\n" +
    "                  <input class=\"genericForm__btn\" ng-if=\"contest.type_id == contestType.photos\" ng-click=\"sendContest().step2()\" type=\"submit\" value=\"Confirm\" ng-disabled>\n" +
    "                  <input class=\"genericForm__btn\" ng-if=\"contest.type_id == contestType.text\" ng-click=\"sendContest().esSaysStep2()\" type=\"submit\" value=\"Confirm\">\n" +
    "                  <button class=\"genericForm__btn disabled\" ng-if=\"contest.type_id == contestType.photos\" ng-click=\"uploadAgain()\" >{{ \"UploadAgain\" | translate }}</button>\n" +
    "                </form>\n" +
    "              </div>\n" +
    "              <div class=\"recapUpload__col recapUpload__col--2 recapUpload__mobile\" style=\"background-image: url({{ objSendContest.file }})\">\n" +
    "                <img class=\"recapUpload__img\" src=\"{{ objSendContest.file }}\" alt=\"\" />\n" +
    "              </div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "  </div>\n" +
    "\n" +
    "</div>\n" +
    "</div>\n" +
    "  </div>\n" +
    "  <!-- canvas -->\n" +
    "  <section class=\"canvasHeader\" ng-cloak>\n" +
    "<!-- <img src=\"assets/theme-01/images/bgCanvas.jpg\" class=\"tilt-effect\" alt=\"\"  data-tilt-options='{ \"opacity\" : 0.8, \"bgfixed\" : false, \"extraImgs\" : 4, \"movement\": { \"perspective\" : 1500, \"translateX\" : 80, \"translateY\" : 80, \"translateZ\" : 0, \"rotateY\" : 20 } }' /> -->\n" +
    "<div class=\"canvasHeader__bg\"></div>\n" +
    "<div class=\"canvasHeader__text\">\n" +
    "  <h1 class=\"canvasHeader__title\" ng-bind-html=\"contest.name\"></h1>\n" +
    "  <h2 class=\"canvasHeader__subtitle\">{{ contest.hashtag }}</h2>\n" +
    "</div>\n" +
    "<div ng-if=\"contest.status == 3\">\n" +
    "  <a href=\"#\" class=\"canvasHeader__partecipate\" ng-click=\"toastr.warning('{{ 'SheduledMsg' |  translate }}')\">{{ \"PartecipatePhoto\" | translate }}</a>\n" +
    "</div>\n" +
    "<div ng-if=\"contest.status == 1\">\n" +
    "  <a href=\"#\" class=\"canvasHeader__partecipate\" ng-click=\"activeView('signIn--s1', 'signIn')\" ng-if=\"showPartecipate(contest.type_id).photo()\">{{ \"PartecipatePhoto\" | translate }}</a>\n" +
    "  <a href=\"#\" class=\"canvasHeader__partecipate\" ng-click=\"activeView('signIn--s1', 'signIn')\" ng-if=\"showPartecipate(contest.type_id).video()\">{{ \"PartecipateVideo\" | translate }}</a>\n" +
    "  <a href=\"#\" class=\"canvasHeader__partecipate canvasHeader__partecipateText\" ng-click=\"activeView('signIn--s1', 'signIn')\" ng-if=\"showPartecipate(contest.type_id).text()\">{{ \"PartecipateText\" | translate }}</a>\n" +
    "  <a href=\"#\" class=\"canvasHeader__partecipate\" ng-click=\"activeView('partecipate--s1', 'partecipate')\" ng-if=\"showPartecipate(contest.type_id).photoLogged()\">{{ \"PartecipatePhoto\" | translate }}</a>\n" +
    "  <a href=\"#\" class=\"canvasHeader__partecipate\" ng-click=\"activeView('partecipate--s1', 'partecipate')\" ng-if=\"showPartecipate(contest.type_id).videoLogged()\">{{ \"PartecipateVideo\" | translate }}</a>\n" +
    "  <a href=\"#\" class=\"canvasHeader__partecipate canvasHeader__partecipateText\" ng-click=\"activeView('partecipate--s6', 'partecipate')\" ng-if=\"showPartecipate(contest.type_id).textLogged()\">{{ \"PartecipateText\" | translate }}</a>\n" +
    "</div>\n" +
    "\n" +
    "  </section>\n" +
    " <!-- content -->\n" +
    "<div class=\"content js-in-out-animation j-fixHeaderItem\">\n" +
    "<article class=\"competition js-in-out-item js-outOff j-toggle\" data-offset=\"10\">\n" +
    "  <div class=\"competition__intro\">\n" +
    "    <p class=\"competition__abstract\" ng-bind-html=\"contest.concept\"></p>\n" +
    "    <a href=\"#\" class=\"competition__more j-toggleBtn\">{{ \"More\" | translate }}</a>\n" +
    "  </div>\n" +
    "  <div class=\"competition__detail j-toggleItem\">\n" +
    "    <div class=\"competition__colLeft\">\n" +
    "      <p class=\"competition__description\">\n" +
    "        {{ contest.how_to_win }}\n" +
    "      </p>\n" +
    "    </div>\n" +
    "    <div class=\"competition__colRight\">\n" +
    "      <div class=\"competition__aux\">\n" +
    "        <dl class=\"competition__info\">\n" +
    "          <dd>{{ \"OpeiningData\" | translate }}</dd>\n" +
    "          <dt>{{ contest.start_time | date : 'MMM d, y h:mm:ss a' }}</dt>\n" +
    "          <dd>{{ \"ClosingData\" | translate }}</dd>\n" +
    "          <dt>{{ contest.end_time }}</dt>\n" +
    "          <dd>{{ \"TimeLeft\" | translate }}</dd>\n" +
    "          <dt>{{ contest.time_left }}</dt>\n" +
    "          <dd class=\"competition__label\">{{ \"Share\" | translate }}</dd>\n" +
    "          <dt class=\"competition__icon\">\n" +
    "            <a class=\"competition__fb\" href=\"#\"\n" +
    "              socialshare\n" +
    "              socialshare-provider=\"facebook\"\n" +
    "              socialshare-url=\"{{ urlContest }}\"\n" +
    "            ></a>\n" +
    "            <a class=\"competition__tw\" href=\"#\"\n" +
    "              socialshare\n" +
    "              socialshare-provider=\"twitter\"\n" +
    "              socialshare-url=\"{{ urlContest }}\"\n" +
    "            ></a>\n" +
    "          </dt>\n" +
    "\n" +
    "          <dd class=\"competition__label\" ng-if=\"showPartecipate(contest.type_id).photo()\">{{ \"PartecipatePhoto\" | translate }}</dd>\n" +
    "          <dd class=\"competition__label\" ng-if=\"showPartecipate(contest.type_id).video()\">{{ \"PartecipateVideo\" | translate }}</dd>\n" +
    "          <dd class=\"competition__label\" ng-if=\"showPartecipate(contest.type_id).text()\">{{ \"PartecipateText\" | translate }}</dd>\n" +
    "\n" +
    "          <div ng-if=\"isAuthenticated()\">\n" +
    "            <dt class=\"competition__icon\" ng-if=\"showPartecipate(contest.type_id).photo()\">\n" +
    "              <a class=\"competition__upload\" href=\"#\" ng-click=\"activeView('partecipate--s1', 'partecipate')\"></a>\n" +
    "            </dt>\n" +
    "            <dt class=\"competition__icon\" ng-if=\"showPartecipate(contest.type_id).video()\">\n" +
    "              <a class=\"competition__upload\" href=\"#\" ng-click=\"activeView('partecipate--s4', 'partecipate')\"></a>\n" +
    "            </dt>\n" +
    "            <dt class=\"competition__icon\" ng-if=\"showPartecipate(contest.type_id).text()\">\n" +
    "              <a class=\"competition__upload\" href=\"#\" ng-click=\"activeView('partecipate--s6', 'partecipate')\"></a>\n" +
    "            </dt>\n" +
    "          </div>\n" +
    "          <div ng-if=\"!isAuthenticated()\">\n" +
    "            <dt class=\"competition__icon\" ng-if=\"contest.status == 3\">\n" +
    "              <a class=\"competition__upload\" href=\"#\" ng-click=\"toastr.warning('{{ 'SheduledMsg' |  translate }}')\"></a>\n" +
    "            </dt>\n" +
    "            <dt class=\"competition__icon\" ng-if=\"contest.status == 1\">\n" +
    "              <a class=\"competition__upload\" href=\"#\" ng-click=\"activeView('signIn--s1', 'signIn')\"></a>\n" +
    "            </dt>\n" +
    "          </div>\n" +
    "        </dl>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</article>\n" +
    "<section class=\"awards js-in-out-item js-outOff j-toggle\">\n" +
    "  <ul class=\"awards__list\">\n" +
    "    <li class=\"awards__item\" ng-repeat=\"reward in rewards\" ng-class=\"{ 'awards__itemCol1' : $index == 0 }\" ng-show=\"$index <= 2\">\n" +
    "      <span class=\"awards__icon awards__icon--{{ $index }}\">\n" +
    "      </span>\n" +
    "      <p class=\"awars__text\">\n" +
    "        <span class=\"awars__title\" ng-bind-html=\"reward.title\"></span><br>\n" +
    "        <span class=\"awars__desc\" ng-bind-html=\"reward.description\"></span>\n" +
    "      </p>\n" +
    "    </li>\n" +
    "  </ul>\n" +
    "  <a href=\"#\" class=\"awards__more j-toggleBtn\" ng-show=\"$index >= 3\">{{ \"More\" | translate }}</a>\n" +
    "  <ul class=\"awards__list awards__list--more j-toggleItem\">\n" +
    "    <li class=\"awards__item\" ng-repeat=\"reward in rewards\" ng-show=\"$index >= 3\">\n" +
    "      <span class=\"awards__icon awards__icon--{{ $index }}\">\n" +
    "      </span>\n" +
    "      <p class=\"awars__text\">{{ reward.title }} <br> <span class=\"awars__desc\" ng-bind-html=\"reward.description\"></span></p>\n" +
    "    </li>\n" +
    "  </ul>\n" +
    "</section>\n" +
    "\n" +
    "<section class=\"nominations\">\n" +
    "  <botton class=\"nominations__orderBtn\" ng-class=\"{ 'nominations__orderBtn--active': storedEntries.orderBy == 'morePopular' }\" ng-click=\"btnEntriesOrderBy('morePopular')\">{{ \"MorePopular\" | translate }}</botton> /\n" +
    "  <botton class=\"nominations__orderBtn\" ng-class=\"{ 'nominations__orderBtn--active': storedEntries.orderBy == 'lessPopular' }\" ng-click=\"btnEntriesOrderBy('lessPopular')\">{{ \"LessPopular\" | translate }}</botton>\n" +
    "  <ul class=\"nominations__top\" infinite-scroll='btnLoadMore()' infinite-scroll-distance='0'>\n" +
    "    <li class=\"nominations__topItem js-in-out-item js-outOff\" ng-repeat=\"entry in entries\">\n" +
    "      <div class=\"nominations__box\">\n" +
    "        <div class=\"nominations__image\" ng-if=\"contest.type_id == contestType.videos\">\n" +
    "            <img src=\"{{ entry.image }}\" alt=\"{{ entry.name }}\" />\n" +
    "        </div>\n" +
    "        <div class=\"nominations__image\" ng-if=\"contest.type_id == contestType.photos\">\n" +
    "          <img src=\"{{ domainRes }}/photos/{{ entry.image }}\" alt=\"{{ entry.name }}\" />\n" +
    "        </div>\n" +
    "        <div class=\"nominations__image\" ng-if=\"contest.type_id == contestType.text\">\n" +
    "          <img src=\"{{ domainRes }}/essays/{{ entry.image }}\" alt=\"{{ entry.name }}\" />\n" +
    "        </div>\n" +
    "        <article class=\"nominations__info\">\n" +
    "          <h3 class=\"nominations__Name\">{{ entry.name }}</h3>\n" +
    "          <div class=\"nominations__infoBottom\">\n" +
    "            <nav class=\"nominations__share\">\n" +
    "              <span class=\"nominations__label nominations__label--share\">{{ \"Share\" | translate }}</span>\n" +
    "              <ul ng-if=\"contest.type_id == contestType.photos\">\n" +
    "                <li class=\"nominations__shareItem\">\n" +
    "                  <a href=\"#\" class=\"nominations__fb\"\n" +
    "                    socialshare\n" +
    "                    socialshare-provider=\"facebook\"\n" +
    "                    socialshare-url=\"{{ domain }}/FacebookSharePhoto/{{ entry.image | getHtml }}\"></a></li>\n" +
    "                <li class=\"nominations__shareItem\">\n" +
    "                  <a href=\"#\" class=\"nominations__tw\"\n" +
    "                    socialshare\n" +
    "                    socialshare-provider=\"twitter\"\n" +
    "                    socialshare-url=\"{{ domain }}/FacebookSharePhoto/{{ entry.image | getHtml }}\"></a>\n" +
    "                </li>\n" +
    "              </ul>\n" +
    "              <ul ng-if=\"contest.type_id == contestType.videos\">\n" +
    "                <li class=\"nominations__shareItem\">\n" +
    "                  <a href=\"#\" class=\"nominations__fb\"\n" +
    "                    socialshare\n" +
    "                    socialshare-provider=\"facebook\"\n" +
    "                    socialshare-url=\"{{ domain }}/FacebookShare/{{ entry.image | getHtml }}\"></a></li>\n" +
    "                <li class=\"nominations__shareItem\">\n" +
    "                  <a href=\"#\" class=\"nominations__tw\"\n" +
    "                    socialshare\n" +
    "                    socialshare-provider=\"twitter\"\n" +
    "                    socialshare-url=\"{{ domain }}/FacebookShare/{{ entry.image | getHtml }}\"></a>\n" +
    "                </li>\n" +
    "              </ul>\n" +
    "              <ul ng-if=\"contest.type_id == contestType.text\">\n" +
    "                <li class=\"nominations__shareItem\">\n" +
    "                  <a href=\"#\" class=\"nominations__fb\"\n" +
    "                    socialshare\n" +
    "                    socialshare-provider=\"facebook\"\n" +
    "                    socialshare-url=\"{{ domain }}/FacebookShareEssay/{{ entry.image | getHtml }}\"></a></li>\n" +
    "                <li class=\"nominations__shareItem\">\n" +
    "                  <a href=\"#\" class=\"nominations__tw\"\n" +
    "                    socialshare\n" +
    "                    socialshare-provider=\"twitter\"\n" +
    "                    socialshare-url=\"{{ domain }}/FacebookShareEssay/{{ entry.image | getHtml }}\"></a>\n" +
    "                </li>\n" +
    "              </ul>\n" +
    "            </nav>\n" +
    "            <div class=\"nominations__rightCol\">\n" +
    "              <div class=\"nominations__rank\"><span class=\"nominations__label\">{{ \"Rank\" | translate }}</span>{{ entry.rank_position }}</div>\n" +
    "              <div class=\"nominations__buzz\"><span class=\"nominations__label\">{{ \"SocialBuzz\" | translate }}</span>{{ entry.score }}</div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <a class=\"recapVideo__play\" ng-click=\"openVideo(entry)\" ng-if=\"contest.type_id == contestType.videos\"></a>\n" +
    "        </article>\n" +
    "      </div>\n" +
    "    </li>\n" +
    "  </ul>\n" +
    "</section>\n" +
    " </div>\n" +
    "  <!-- footer -->\n" +
    "  <footer class=\"footer\" ng-if=\"contest.service_type == 1\">\n" +
    "  <div class=\"footer__content\">\n" +
    "      <span class=\"footer__label\"><a href=\"https://www.buzzmybrand.com\" target=\"_black\">Powered by BuzzMyBrand</a></span>\n" +
    "      <a href=\"https://www.buzzmybrand.com\" target=\"_black\"><span class=\"footer__logo\"></span></a>\n" +
    "  </div>\n" +
    "  </footer>\n" +
    "</div>\n" +
    "");
}]);
