/*jshint -W018 */
angular.module('bmbMinisite.layout', [
  'bmbMinisite.appConf',
  'bmbMinisite.fct.utils'
])



/*

  TEST DIR

*/
.directive('onKeyDown', function() {
  return function (scope, element, attrs) {
        element.bind("keyup keypress", function (event) {
          scope.$apply(function (){
              scope.$eval(attrs.onKeyDown);
          });
        });
    };
}).directive('parseStyle', function($interpolate) {
  return function(scope, elem) {
        var exp = $interpolate(elem.html()),
        watchFunc = function () { return exp(scope); };
        scope.$watch(watchFunc, function (html) {
            elem.html(html);
        });
    };
}).directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;

            function nextFocus(){
              var d = angular.element(elem).parents().next();
              angular.element("input", d).focus();
            }

            elem.bind("focus", function(){
              elem.val('');
            });

            ctrl.$formatters.unshift(function (a) {
                return "";
            });

            ctrl.$parsers.unshift(function (viewValue) {
              var pattern =/^\d+$/,
                  date = viewValue,
                  d = date.replace(/[/]/g, "");
              if(pattern.test(d)) {
                if(date.length == 2 || date.length == 5) {
                  date += "/";
                }
                if(date.length >= 10) {
                  date = date.slice(0,10);
                  nextFocus();
                }
              }
              elem.val(date);
              return date;
            });
        }
    };
}]);
