angular.module('bmbMinisite.filter', [])
.filter('cut', function () {
  return function (value, wordwise, max, tail) {
    if (!value) {return '';}

    max = parseInt(max, 10);
    if (!max) {return value;}
    if (value.length <= max){ return value;}

    value = value.substr(0, max);
    if (wordwise) {
        var lastspace = value.lastIndexOf(' ');
        if (lastspace != -1) {
            value = value.substr(0, lastspace);
        }
    }
    return value + (tail || ' …');
  };
})
.filter('getHtml', function () {
  return function (value) {
    if (!value) return;
    return ((value.split(".")).shift()).concat(".html");
  };
})
.filter('spaceless',function(){
    return function(input){
        return input.replace(' ','');
    };
})
.filter('test',function(){
    return function(input){
        return input;
    };
})
.filter("htmlSafe", ['$sce', function($sce) {
    return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
    };
}]);
