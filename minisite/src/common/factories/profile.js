/* jshint   loopfunc: true */
angular.module( 'bmbMinisite.fct.profile', ['bmbMinisite.apiUri','bmbMinisite.appConf'])

.factory('fctProfile', [ '$http', '$q', 'apiUri', '$log', function ($http, $q, apiUri, $log) {

  /*

    _getAPI

  */
  function _getAPI(o) {
    var deferred = $q.defer();
    $http.get(o.url, {
      headers: o.headers
    }).success(function(data) {
        deferred.resolve(data);
     }).error(function(msg, code) {
        deferred.reject(msg);
        $log.error(msg, code);
     });
    return deferred.promise;
  }



  function _postAPI(o) {
    var deferred = $q.defer();
    $http({
      method: 'POST',
      url: o.url,
      data: o.data
    }).success(function(data) {
        _is_busy=false;
        deferred.resolve(data);
     }).error(function(msg, code) {
        _is_busy=false;
        deferred.reject(msg);
        $log.error(msg, code);
     });
    return deferred.promise;
  }


  /*

    _getData

  */
  function _getData(emailUser, token) {
    var o = {
      url: apiUri.profile.concat(emailUser),
      headers: { "authorization": token }
    };
    return _getAPI(o);
  }



  /*

    _invite_friend

  */
  function _invite_friend(contestName, data) {
    var o = {
      url: apiUri.inviteFriend.concat(contestName),
      data: data
    };
    return _postAPI(o);
  }


  return {
    getProfile: _getData,
    inviteFriend: _invite_friend
  };

}]);
