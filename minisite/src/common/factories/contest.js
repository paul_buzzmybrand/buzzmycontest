/* jshint   loopfunc: true */
angular.module( 'bmbMinisite.fct.contest', ['bmbMinisite.apiUri','bmbMinisite.appConf', 'bmbMinisite.fct.utils'])

.factory('fctContest', [ '$http', '$q', 'apiUri', '_', 'moment', 'fctUtil', '$log', function ($http, $q, apiUri, _, moment, fctUtil, $log) {



  /*

    _getAPI

  */
  function _getAPI(o) {
    var deferred = $q.defer();
    $http.get(o.url, {
      headers: o.headers
    }).success(function(data) {
        deferred.resolve(data);
     }).error(function(msg, code) {
        deferred.reject(msg);
        $log.error(msg, code);
     });
    return deferred.promise;
  }



    function _postAPI(o) {
      var deferred = $q.defer();
      $http({
        method: 'POST',
        url: o.url,
        data: o.data
      }).success(function(data) {
          _is_busy=false;
          deferred.resolve(data);
       }).error(function(msg, code) {
          _is_busy=false;
          deferred.reject(msg);
          $log.error(msg, code);
       });
      return deferred.promise;
    }






  /*

    _formatting_date

  */
  function _formatting_data(o) {

    if(!_.isEmpty(o)) {

      o.time_left_top = fctUtil.getTimeleft(o.end_time).short();
      o.time_left = fctUtil.getTimeleft(o.end_time).long();
      o.start_time = moment(o.start_time).format("DD.MM.YYYY");
      o.end_time = moment(o.end_time).format("DD.MM.YYYY");
      o.hashtag = o.hashtag.replace(/[;]/g,' ');
    }
    return o;
  }




  /*

    _get_fields

  */
  function _get_fields(o) {

    var result = {
      registration_needed: null,
      fields: null
    };

    if(!_.isEmpty(o)) {

      var row = _.where(o, {registration_needed : _.isNumber(o[0].registration_needed) ? 1 : "1" })

      if(!_.isEmpty(row)) {

        row = _.first(row);

        result.registration_needed = !!parseInt(row.registration_needed),
        result.fields = row.pivot
      }
    }

    return result;
  }





  /*

    _get_objectives

  */
  function _get_objectives(contestName) {
    var o = {
      url: apiUri.objectives.concat(contestName)
    };
    return _getAPI(o);
  }






  /*

    _getData

  */
  function _getData(contestName) {
    var o = {
      url: apiUri.contest.concat(contestName)
    };
    return _getAPI(o);
  }





  /*

    _send_essays

  */
  function _send_essays(contestName, sentence) {
    var o = {
      url: apiUri.saveEssay.concat(contestName),
      data: sentence
    };
    return _postAPI(o);
  }



  /*

    _send_photo

  */
  function _send_photo(contestName, data) {
    var o = {
      url: apiUri.savePhoto.concat(contestName),
      data: data
    };
    return _postAPI(o);
  }



  /*

    _save_video

  */
  function _send_video(contestName, data) {
    var o = {
      url: apiUri.saveVideo.concat(contestName),
      data: data
    };
    return _postAPI(o);
  }



  return {
    getContest: _getData,
    formattingData: _formatting_data,
    getObjectives: _get_objectives,
    getFields: _get_fields,
    sendEssays: _send_essays,
    sendPhoto: _send_photo,
    sendVideo: _send_video
  };

}]);
