class window.fieldAnm
	constructor: (@ref) ->
		@item = @ref.find '.j-fieldItem'
		@initInputItem()

	initInputItem: =>
		for i in [0...@item.length]
			item = $ @item[i]

			item.focusout ()=>
				if item.val() != ''
					item.addClass('filled')
				else
					item.removeClass('filled')
