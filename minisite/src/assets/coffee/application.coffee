unless window.console then window.console = {log: ((obj) ->)}

$ ->

	window_ref = $ window

	window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame

	##################
	# USER AGENT CHECK
	##################

	getIOSVersion = ->
		if /iP(hone|od|ad)/.test(navigator.platform)
			v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/)
			return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)]
		else
			return false

	window.has_ios = getIOSVersion()
	window.is_mobile = if navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/Android/i) then true else false
	window.is_iphone = if navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) then true else false
	window.is_ipad = if navigator.userAgent.match(/iPad/i) then true else false

	if is_mobile then $('body').addClass 'deviceMobile'

	#################
	# MODERNIZR TESTS
	#################

	# TRANSITIONEND
	window.transEndEventNames = {
		WebkitTransition: "webkitTransitionEnd" # Saf 6, Android Browser
		MozTransition: "transitionend" # only for FF < 15
		transition: "transitionend" # IE10, Opera, Chrome, FF 15+, Saf 7+
	}
	window.transEndEventName = window.transEndEventNames[Modernizr.prefixed("transition")]

	# DPI Modernizr.highres
	Modernizr.addTest "highres", ->
		dpr = window.devicePixelRatio or (window.screen.deviceXDPI / window.screen.logicalXDPI) or 1
		!!(dpr > 1)

	# TRANSFORM PREFIXED
	if typeof Modernizr.prefixed("transform") is 'string'
		window.prefixedTransform = Modernizr.prefixed("transform").replace(/([A-Z])/g, (str, m1) -> "-" + m1.toLowerCase()).replace /^ms-/, "-ms-"


	###########
	# OVERLAY #
	###########

	overlay_ref = $ '.j-overlay'
	if overlay_ref.length is 1
		new Overlay overlay_ref


	####################
	# ANIMATION IN-OUT #
	####################

	animation_in_out_ref = $ '.js-in-out-animation'
	if animation_in_out_ref.length is 1
		new AnimationInOut animation_in_out_ref

	##########
	# Toggle #
	##########

	toggle_ref = $ '.j-toggle'
	if toggle_ref.length > 0
		for i in [0...toggle_ref.length]
			new Toggle $ toggle_ref[i]


	##########
	# shItem #
	##########

	shitem_ref = $ '.j-sh'
	if shitem_ref.length > 0
		for i in [0...shitem_ref.length]
			new shItem $ shitem_ref[i]


	#############
	# fixHeader #
	#############

	fixedHeader = $ '.j-fixedHeader'
	if fixedHeader.length > 0
			new fixHeader fixedHeader


	############
	# fieldAnm #
	############

	fieldanm_ref = $ '.j-field'
	if fieldanm_ref.length > 0
		for i in [0...fieldanm_ref.length]
			new fieldAnm $ fieldanm_ref[i]
