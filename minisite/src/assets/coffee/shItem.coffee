class window.shItem
	constructor: (@ref) ->
		@item = @ref.find '.j-shItem'
		@initToggleItem()

	initToggleItem: =>
		for i in [0...@item.length]
			item = $ @item[i]
			btn = item.find '.j-shBtn'
			b = $ 'html,body'
			duration = 500

			btn.click (e)=>
				e.preventDefault()

				item_toOpen = $(e.currentTarget).closest('.j-shItem')
				item_toOpen.siblings('.j-shItem').removeClass('detailOpen')
				item_toOpen.toggleClass('detailOpen')


				item_toOpenTop = Math.floor(item_toOpen.offset().top)
				target_top = item_toOpenTop


				b.stop().animate {scrollTop: "#{target_top}"}, {duration: duration, easing: 'swing'}