/* jshint   loopfunc: true */
angular.module( 'bmbMinisite.home', ['ui.router.state',

	'bmbMinisite.appConf',
	'bmbMinisite.layout',
	'bmbMinisite.fct.contest',
	'bmbMinisite.fct.entries',
	'bmbMinisite.fct.rewards',
	'bmbMinisite.fct.profile',
	'bmbMinisite.fct.signUp',
	'bmbMinisite.fct.videoRecorder',
	'bmbMinisite.apiUri',
	'timer',
  '720kb.socialshare',
	'ngFileUpload'

]).config(function config( $stateProvider, appConf ) {

	function getUrl() {

		var minisite = location.pathname;

		if(minisite.search("minisite") != -1) {
			return "/minisite/:idContest";
		}

		if(minisite.search("preview") != -1) {
			return "/preview?template_id&canvas_header&font_color&sponsor_logo&lang&title&hashtag";
		}

		return "/:idContest";
	}

	$stateProvider.state( 'home', {
		url: getUrl(),
		views: {
			"main": {
				controller: 'homeCtrl',
				templateUrl: 'home/home.tpl.html'
			}
		},
		data: {
			isLogged: appConf.isLogged,
			overlayIsOpen: appConf.overlayIsOpen,
			pageTitle: appConf.basePageTitle,
			bodyClass: "",
			hostName: location.origin
		}
	});
})




/**
 *home controller
 */
.controller( 'homeCtrl', [

	'$scope',
	'$stateParams',
	'$sce',
	'$state',
	'appConf',
	'apiUri',
	'fctUtil',
	'ModalService',
	'fctContest',
	'fctEntries',
	'fctRewards',
	'fctProfile',
	'fctSignUp',
	'fctVideoRecorder',
	'toastr',
	'$rootScope',
	'_',
	'$auth',
	'Socialshare',
	'Upload',
	'$timeout',
	'$interval',
	'$translate',

	function homeController(

		$scope,
		$stateParams,
		$sce,
		$state,
		appConf,
		apiUri,
		fctUtil,
		ModalService,
		fctContest,
		fctEntries,
		fctRewards,
		fctProfile,
		fctSignUp,
		fctVideoRecorder,
		toastr,
		$rootScope,
		_,
		$auth,
		Socialshare,
		Upload,
		$timeout,
		$interval,
		$translate) {




			/**
			**
			**		VARS
			**
			**/


			$scope.contestName = $stateParams.idContest;

			$scope.isPreview = location.pathname.search("preview") != -1 ? true : false;
			$scope.urlContest	= appConf.domain.concat("/", $scope.contestName);

			$scope.emailUser = null;

			$scope.contest = {};
			$scope.entries = {};
			$scope.rewards = {};

			$scope.profile = {};
			$scope.profileEntries = {};
			$scope.signInFields = {};

			$scope.toastr = toastr;
			$scope.islogged = appConf.isLogged;

			$scope.storedEntries = {
				to: 0,
				total: 1,
				orderBy: "morePopular"
			};

			$scope.contestType = {
				videos: 1,
				photos: 2,
				text: 3
			};

			$scope.lang = {
				"1": "en_EN",
				"2": "it_IT",
				"3": "en_EN"
			};

			$scope.stateView = "partecipate--s1";
			$scope.activeViewItem = "partecipate";
			$scope.overlayIsOpen = false;









			/**
			**
			**		CUSTOM THEME
			**
			**/

			if($scope.isPreview) {

				$scope.customTheme = $stateParams;

				$scope.contest.name = $scope.customTheme.title;
				$scope.contest.hashtag = "#".concat($scope.customTheme.hashtag);
				$scope.contest.sponsor = true;
				$scope.contest.type_id = 2;
				$scope.contest.sponsor = $scope.customTheme.sponsor_logo;

				//hide custom loader
				angular.element("#loading-bar").hide();

				$translate.use($scope.lang[$scope.customTheme.lang || 1]);

				return;
			}


			var loadCustomTheme = function() {
				if($scope.contest.template_id == 0 || $scope.contest.template_id == null) {
					$scope.customTheme = {};
					$scope.customTheme.font_color = $scope.contest.font_color;
					$scope.customTheme.canvas_header = $scope.contest.image_minisite_bg;
					$scope.customTheme.sponsor = $scope.contest.font_color == null ? "" : $scope.contest.font_color.replace("#", "");
					$scope.customTheme.template_id = 0;
				}
			};



			/**
			**
			**		CUSTOM LOADER
			**
			**/
			if(!$scope.isPreview) {
				$rootScope.$on('cfpLoadingBar:completed', function(){
					angular.element("body").addClass("loaded");
				});
			}





			/**
			**
			**		GET SERVICES API
			**
			**/

			$scope.getIframeSrc = function (domainRes, filename) {
				return $sce.getTrustedResourceUrl(domainRes.concat("/videos/", filename));
			};

			$scope.decodeHtml = fctUtil.renderHtml;
			$scope.domainRes = appConf.domainRes;
			$scope.domain = appConf.domain;

			fctContest.getContest($scope.contestName).then(function(item) {
				$scope.contest = fctContest.formattingData(item);
				$scope.contestName = $scope.contest.contest_route;
				loadCustomTheme();
				$translate.use($scope.lang[$scope.contest.location_id]);
			});

			fctContest.getObjectives($scope.contestName).then(function(item) {
				$scope.signInFields = fctContest.getFields(item);
				if($scope.signInFields.registration_needed) {
					$scope.enabledSocialLogin = false;
				}
			});

			fctEntries.getEntries($scope.contestName).then(function(item) {
				var entries = {};
				if(!_.isEmpty(item.data)) {
					entries = fctEntries.entriesOrderBy(item.data)[$scope.storedEntries.orderBy]();
				}
				$scope.entries = fctEntries.entriesModel(entries);
				$scope.current_page = item.current_page;
			});

			fctRewards.getRewards($scope.contestName).then(function(item) {
				$scope.rewards = item;
			});

			$scope.getProfileEnties = function() {
				fctEntries.getProfileEntries($scope.contestName, $scope.profile.token).then(function(item) {
					$scope.profileEntries = fctEntries.orderBy(item.data);
					$scope.profileEntries.isEmpty = _.isEmpty($scope.profileEntries);
				});
				fctEntries.getProfile($scope.contestName, $scope.profile.token).then(function(item) {
					$scope.profile = _.extend($scope.profile, item);
				});
			}





			/**
			**
			**		ACTIONS CALLS FROM VIEW
			**
			**/

			$scope.btnLoadMore = function(event) {
				++$scope.current_page;
				if($scope.storedEntries.to < $scope.storedEntries.total && !_.isEmpty($scope.entries)) {
					fctEntries.getEntries($scope.contestName, $scope.current_page).then(function(item) {
						var entries = fctEntries.entriesOrderBy(_.union(item.data, $scope.entries))[$scope.storedEntries.orderBy]();
						$scope.entries = fctEntries.entriesModel(entries);
						$scope.storedEntries.to = item.to;
						$scope.storedEntries.total = item.total;
					});
				}
				event.preventDefault();
			};

			$scope.btnEntriesOrderBy = function(order) {
				$scope.storedEntries.orderBy = order;
				$scope.entries = fctEntries.entriesOrderBy($scope.entries)[order]();
				if(event) {
					event.preventDefault();
				}
			}




			/**
			**
			**		AUTHENTICATE
			**
			**/
			$scope.isLogged = {};
			$scope.isLogged.facebook = false;
			$scope.isLogged.twitter = false;
			$scope.isLogged.provider = null;

			if($auth.isAuthenticated()) {
				$auth.logout();
			}

			var stateOk = function(res) {
				return res.state == "ok";
			};

			var isSocialLogged = function(prv) {
				$scope.isLogged[$scope.isLogged.provider] = true;
			};

			var isLoggedWithsocial = function(){

			}

			$scope.authenticate = function(provider) {
				var _this = this,
						auth	= $auth;

					$scope.isLogged.provider = provider;

					//if sign up is active
					if($scope.signInFields.registration_needed) {

						if($scope.submitSignUp()) {

							$auth.authenticate(provider).then(function(item) {

								$scope.profile.token = auth.getToken();
								_this.getProfileEnties();
								isSocialLogged();

								fctSignUp.signUp($scope.contestName, $scope.userData).then(function(res) {
									if(stateOk(res)) {
										console.log("sign up done");
									}
								})
							});
						}
					}
					else {

						$auth.authenticate(provider).then(function(item){
							$scope.profile.token = auth.getToken();
							_this.getProfileEnties();
							isSocialLogged();
						});
					}
			};



			$scope.isAuthenticated = function() {
				if($auth.isAuthenticated()) {
					return true;
				}
				return $auth.isAuthenticated();
			};

			$scope.logout = function() {
				$scope.isLogged.facebook = false;
				$scope.isLogged.twitter = false;
			  return $auth.logout();
			};

			$scope.showPartecipate = function(type_id) {
			  return {
					media: function(){
						return type_id == $scope.contestType.videos || type_id == $scope.contestType.photos;
					},
					video: function(){
						return type_id == $scope.contestType.videos;
					},
					photo: function(){
						return type_id == $scope.contestType.photos;
					},
					text: function(){
						return type_id == $scope.contestType.text;
					},
					videoLogged: function(){
						return $auth.isAuthenticated() && (type_id == $scope.contestType.videos);
					},
					photoLogged: function(){
						return $auth.isAuthenticated() && (type_id == $scope.contestType.photos);
					},
					textLogged: function(){
						return $auth.isAuthenticated() && (type_id == $scope.contestType.text);
					},
					mediaLogged: function(){
						return $auth.isAuthenticated() && (type_id == $scope.contestType.videos || type_id == $scope.contestType.photos);
					}
				}
			};




			/**
			**
			**		STATE VIEW OVERLAY
			**
			**/
			$scope.activeView = function(view, item) {
				$scope.stateView = view;
				$scope.activeViewItem = item;
				$scope.overlayToggle().open();
				event.preventDefault();
			};

			$scope.overlayToggle = function() {
				return {
					open: function(){
						if ($scope.overlayIsOpen) return;
						$scope.overlayIsOpen = true;
						angular.element("body").addClass("noScroll");
					},
					close: function(){
						if (!$scope.overlayIsOpen) return;
						$scope.overlayIsOpen = false;
						angular.element("body").removeClass("noScroll");
					}
				}
				event.preventDefault();
			};





			/**
			**
			**		SEND CONTEST
			**
			**/
			$scope.objSendContest = {
				file: null,
				title: null,
				hashtag: null
			};

			$scope.objSendContestEsSays = {
				sentence: null,
				title: null,
				hashtag: null
			};

			$scope.sendContestForm__ErrorName = false;
			$scope.sendContestForm__Errorhashtag = false;
			$scope.loginEmail = false;
			$scope.enabledSend = true;


			$scope.resetSendContest = function() {
				$scope.sendContestForm__title = "";
				$scope.sendContestForm__hashtag = "";
				$scope.sendContestForm__ErrorName = false;
				$scope.sendContestForm__Errorhashtag = false;
			};

			$scope.uploadAgain = function() {
				$scope.resetSendContest();
				$scope.activeView('partecipate--s1', 'partecipate');
			};

			$scope.sendContest = function (file) {

				return {

					step1: function() {

						/* Resizes an image. Returns a promise */
						// options: width, height, quality, type, ratio, centerCrop, resizeIf, restoreExif
						//resizeIf(width, height) returns boolean. See ngf-resize directive for more details of options.
						var options = {
							width: 800,
							height: 600,
							quality: .8,
							type: 'image/jpeg',
							centerCrop: true,
							pattern: '.jpg',
							restoreExif: false
						};

						Upload.resize(file, options).then(function(resizedFile){
							Upload.base64DataUrl(resizedFile).then(function(file) {
									if(!_.isEmpty(file)) {
										$scope.objSendContest.file = file;
										$scope.activeView('partecipate--s7', 'partecipate');
									}
								});
							});
					},

					step2: function() {

						$scope.enabledSend = false;

						$scope.objSendContest.title = $scope.sendContestForm__title;
						$scope.objSendContest.hashtag = $scope.sendContestForm__hashtag;

						if(!$scope.sendContestForm.$valid) {

							if(!$scope.objSendContest.title) {
								$scope.sendContestForm__ErrorName = true;
							}
							else { $scope.sendContestForm__ErrorName = false; }

							if(!$scope.objSendContest.hashtag) {
								$scope.sendContestForm__Errorhashtag = true;
							}
							else { $scope.sendContestForm__Errorhashtag = false; }

							return;
						}

						$scope.sendContestForm__ErrorName = false;
						$scope.sendContestForm__Errorhashtag = false;

						fctContest.sendPhoto($scope.contestName, $scope.objSendContest).then(function(state) {
							$scope.resetSendContest();
							$scope.enabledSend = true;
 							$scope.activeView('partecipate--s5', 'partecipate');
 						});

						/*Upload.upload({
							 url: 'minisite/'.concat($scope.contestName,'/savePhoto/'),
							 data: $scope.objSendContest
						 })
						 .then(function (resp) {
								$scope.activeView('partecipate--s5', 'partecipate')
						 }, function (resp) {
									console.log('Error status: ' + resp.status);
						 }, function (evt) {
									//var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
									console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
						 });*/

					},

					esSaysStep1: function() {

						var sentence = $scope.esSaysForm.sentence.$modelValue;

						if(!_.isEmpty(sentence)) {
							$scope.objSendContestEsSays.sentence = $scope.esSaysForm.sentence.$modelValue;
							$scope.activeView('partecipate--s7', 'partecipate');
						}

					},
					esSaysStep2: function() {

						$scope.objSendContestEsSays.title = $scope.sendContestForm.title.$modelValue;

						fctContest.sendEssays($scope.contestName, $scope.objSendContestEsSays).then(function(state) {
							$scope.activeView('partecipate--s5', 'partecipate');
						});

					}
				}
			};




			/**
			**
			**		SIGN UP
			**
			**/
			$scope.signInDone = false;
			$scope.enabledSocialLogin = true;

			$scope.userData = {
				name: null,
				surname: null,
				birthdate: null,
				email: null,
				phone: null,
				cap: null,
				city: null
			};

			$scope.signUpFormNameS2Valid = true;
			$scope.signUpFormSurnameS2Valid = true;
			$scope.signUpFormBirthdateS2Valid = true;
			$scope.signUpFormEmailS2Valid = true;
			$scope.signUpFormPhoneS2Valid = true;
			$scope.signUpFormCapS2Valid = true;
			$scope.signUpFormCityS2Valid = true;

			$scope.birthdateS2 = "";

			$scope.submitSignUpNameS2 = function() { $scope.signUpFormNameS2Valid = $scope.signUpForm.nameS2.$valid; };
			$scope.submitSignUpSurnameS2 = function() { $scope.signUpFormSurnameS2Valid = $scope.signUpForm.surnameS2.$valid; };

			$scope.submitSignUpBirthdateS2 = function() {
				var pattern =/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
				if(pattern.test($scope.signUpForm.birthdateS2.$modelValue)) {
					$scope.signUpFormBirthdateS2Valid = $scope.signUpForm.birthdateS2.$valid;
				}
				else {
					$scope.signUpFormBirthdateS2Valid = false
				}
			};

			$scope.submitSignUpEmailS2 = function() {
				$scope.signUpFormEmailS2Valid = $scope.signUpForm.emailS2.$valid;
				var pattern =/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
				if(pattern.test($scope.signUpForm.emailS2.$modelValue)) {
					$scope.signUpFormEmailS2Valid = true;
				}
				else {
					$scope.signUpFormEmailS2Valid = false
				}
			};

			$scope.submitSignUpPhoneS2 = function() { $scope.signUpFormPhoneS2Valid = $scope.signUpForm.phoneS2.$valid; };
			$scope.submitSignUpCapS2 = function() { $scope.signUpFormCapS2Valid = $scope.signUpForm.capS2.$valid; };
			$scope.submitSignUpCityS2 = function() { $scope.signUpFormCityS2Valid = $scope.signUpForm.cityS2.$valid; };

			$scope.submitSignUp = function() {

				$scope.submitSignUpEmailS2();

				if ($scope.signUpForm.$valid && $scope.signUpFormEmailS2Valid) {

					$scope.userData.name = !_.isEmpty($scope.signUpForm.nameS2) ? $scope.signUpForm.nameS2.$modelValue : null;
					$scope.userData.surname = !_.isEmpty($scope.signUpForm.surnameS2) ? $scope.signUpForm.surnameS2.$modelValue : null;
					$scope.userData.birthdate = !_.isEmpty($scope.signUpForm.birthdateS2) ? $scope.signUpForm.birthdateS2.$modelValue : null;
					$scope.userData.email = !_.isEmpty($scope.signUpForm.emailS2) ? $scope.signUpForm.emailS2.$modelValue : null;
					$scope.userData.phone = !_.isEmpty($scope.signUpForm.phoneS2) ? $scope.signUpForm.phoneS2.$modelValue : null;
					$scope.userData.cap = !_.isEmpty($scope.signUpForm.capS2) ? $scope.signUpForm.capS2.$modelValue : null;
					$scope.userData.city = !_.isEmpty($scope.signUpForm.cityS2) ? $scope.signUpForm.cityS2.$modelValue : null;

					$scope.enabledSocialLogin = true;

					$scope.signInDone = true;

					return true;

			 }
			 else {
	 				$scope.enabledSocialLogin = false;
					return false
			 }
			};




			/**
			**
			**		INVITE A FRIEND
			**
			**/
			$scope.inviteFriend = {
				name: null,
				surname: null,
				email: null
			};

			$scope.enabledInviteMessageSuccessMessage = false;

			$scope.sendInviteFriend = function() {

				var resetFields = function() {
					$scope.nameS3 = "";
					$scope.addresseeNameS3 = "";
					$scope.recipientS3 = "";
				};

				if ($scope.inviteFriendForm.$valid) {

					$scope.inviteFriend.name = $scope.nameS3;
					$scope.inviteFriend.addressee_name = $scope.addresseeNameS3;
					$scope.inviteFriend.recipient = $scope.recipientS3;
					$scope.inviteFriend.contest_name = $scope.contestName;

					fctProfile.inviteFriend($scope.contestName, $scope.inviteFriend).then(function(res) {

						if(stateOk(res)) {
							$scope.enabledInviteMessageSuccessMessage = true;
							resetFields();
							$timeout(function () {
								$scope.overlayToggle().close();
								$scope.enabledInviteMessageSuccessMessage = false;
							}, 2000);
						}
					});
			 }
			};



			/**
			**
			**		VIDEO RECORDING
			**
			**/
			//var isHTML5 = navigator.mediaDevices;
			var isHTML5 = false;
			$scope.recorder = null;
			$scope.recording = false;
			$scope.enabledStop = false;
			$scope.rec = true;
			$scope.pause = false;
			$scope.video_length_default = 6;
			$scope.video_length_count = null;
			$scope.activeVideoForm = false;

			var recordVideoDom = angular.element('#recordVideo');
			var recapVideoDom = angular.element('#recapVideo');

			function videoLength() {
				if(_.isEmpty($scope.contest.video_length)) {
					return $scope.video_length_default;
				}
				return parseInt($scope.contest.video_length);
			}

			function videoDuration() {

				var count = 0,
				video_length = videoLength();

				return function(p, r, stop) {

					var pause = p || false,
							resume = r || false;

					if(pause) {
						$interval.cancel(videoDurationPromise);
						return;
					}

					if(resume) {
						video_length = $scope.video_length_default - $scope.video_length_count;
						$scope.video_length_default = video_length;
						videoDurationPromise = $interval(iter, 1000);
						return;
					}

					if(stop) {
						$interval.cancel(videoDurationPromise);
						return;
					}

					function iter() {
						if(count === video_length) {
							$interval.cancel(videoDurationPromise);
							$scope.stopRecording();
							return;
						}
						count++;
						$scope.video_length_count = count;
					}

					videoDurationPromise = $interval(iter, 1000);

				}
			}

			if(isHTML5) {

				$scope.recordAgain = function(){
					$scope.recording = false;
					$scope.enabledStop = false;
					$scope.rec = true;
					recordVideoDom.attr("src", null);
					recapVideoDom.attr("src", null);
				};

				$scope.pauseRecording = function() {
					$scope.recorder.pauseRecording();
					videoDuration()(true, false);
					$scope.pause = true;
				};

				$scope.resumeRecording = function() {
					$scope.recorder.resumeRecording();
					videoDuration()(false, true);
					$scope.pause = false;
				};

				$scope.startRecording = function() {

					if($scope.recording) return;

					videoDuration()();

					$scope.recording = true;

					function successCallback(stream) {

							recordVideoDom.attr("src", URL.createObjectURL(stream));
							recordVideoDom.attr("muted", true);
							recordVideoDom.attr("controls", false);

							var options = {
								mimeType: 'video/mp4', // or video/mp4 or audio/ogg
								audioBitsPerSecond: 128000,
								videoBitsPerSecond: 128000,
								bitsPerSecond: 128000 // if this line is provided, skip above two
							};

							$scope.recorder = RecordRTC(stream, options);

							$scope.recorder.startRecording();

							$timeout(function(){
								$scope.enabledStop = true;
								$scope.rec = false;
							}, 3000);
					}

					function errorCallback(error) {
							// maybe another application is using the device
					}

					var mediaConstraints = { video: true, audio: true };

					navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
				};

				$scope.stopRecording = function() {

					$scope.recorder.stopRecording(function (audioVideoWebMURL) {

							recapVideoDom.attr("controls", audioVideoWebMURL);
			        recapVideoDom.attr("src", audioVideoWebMURL);

			        var recordedBlob = $scope.recorder.getBlob();

							$scope.objSendVideo.video = recordedBlob;

							$scope.recorder.clearRecordedData();

							//$scope.recorder.save("my rec");

							$scope.recorder.getDataURL(function(dataURL) {
								$scope.objSendVideo.video = dataURL;
							});

			    });

					$scope.activeView('partecipate--s4', 'partecipate');




					/**
					**
					**		SEND VIDEO
					**
					**/
					$scope.objSendVideo = {
						video: null,
						title: null,
						hashtag: null
					};

					$scope.sendVideo = function(order) {

						VideoRecorder.save();

						$scope.objSendVideo.title = $scope.sendVideoForm__title;

						fctContest.sendVideo($scope.contestName, $scope.objSendVideo).then(function(state) {

							$scope.activeView('partecipate--s5', 'partecipate');
						});
						event.preventDefault();
					}

				};

			} else {

				/*
				document.VideoRecorder.record()
				document.VideoRecorder.pauseRecording()
				document.VideoRecorder.resumeRecording()
				document.VideoRecorder.stopVideo()
				document.VideoRecorder.playVideo()
				document.VideoRecorder.pause()
				document.VideoRecorder.save()
				*/

				$scope.recordAgain = function(){
					$scope.recording = false;
					$scope.enabledStop = false;
					$scope.rec = true;
					$scope.activeVideoForm = false;
				};

				$scope.pauseRecording = function() {
					window.document.VideoRecorder.pauseRecording();
					videoDuration()(true, false);
					$scope.pause = true;
				};

				$scope.resumeRecording = function() {
					window.document.VideoRecorder.resumeRecording();
					videoDuration()(false, true);
					$scope.pause = false;
				};

				$scope.startRecording = function() {
					if($scope.recording) return;
					videoDuration()();
					$scope.recording = true;
					$timeout(function(){
						$scope.enabledStop = true;
						$scope.rec = false;
					}, 3000);
					window.document.VideoRecorder.record();
				};

				$scope.stopRecording = function() {
					window.document.VideoRecorder.stopVideo();
					videoDuration()(false, false, true);
					$scope.activeVideoForm = true;
					//$scope.activeView('partecipate--s4', 'partecipate');
				};

				$scope.playVideo = function() {
					window.document.VideoRecorder.playVideo();
				};

				$scope.saveVideo = function() {
					window.document.VideoRecorder.save();
				};

				if($scope.contest.type_id == $scope.contestType.videos) {

					/*HDFVR*/
					var flashvars = {
						userId : "XXY",
						qualityurl: "500x350x30x90.xml",
						recorderId: "123",
						sscode: "php",
						lstext : "Loading Settings..."
					};
					var params = {
						quality : "high",
						bgcolor : "#dfdfdf",
						play : "true",
						loop : "false",
						allowscriptaccess: "",
						base: "../website_public/hdfvr/",
						settingsurl: "../website_public/hdfvr/avc_settings.php"
					};
					var attributes = {
						name : "VideoRecorder",
						id :   "VideoRecorder",
						align : "middle"
					};

					swfobject.embedSWF("../website_public/hdfvr/VideoRecorder.swf", "flashContent", "500", "350", "10.3.0", "", flashvars, params, attributes);

				}


				/**
				**
				**		SEND VIDEO
				**
				**/
				$scope.objSendVideo = {
					video: null,
					videoName: null,
					duration: null,
					title: null,
					hashtag: null
				};

				$scope.sendVideo = function(order) {
					console.log($scope.objSendVideo);
					fctContest.sendVideo($scope.contestName, $scope.objSendVideo).then(function(state) {
						$scope.activeView('partecipate--s5', 'partecipate');
					});
					event.preventDefault();
				}

				window.onSaveOk = function(streamName, streamDuration, userId, cameraName, micName, recorderId) {

					$scope.objSendVideo.title = $scope.sendVideoForm__title;
					$scope.objSendVideo.videoName = streamName;
					$scope.objSendVideo.duration = streamDuration;

					$scope.sendVideo();
				};

			}
























			/**
			**
			**		VIDEO PLAYER
			**
			**/

			/*
			**  DOM
			*/
			var $html = angular.element("html, body"),
					$body = angular.element("body"),
					$window = angular.element(window),
					$modalOpen = angular.element(".js-modal-open"),
					$modalClose = angular.element(".js-modal-close"),
					$modal = angular.element(".js-modal");


			$modalClose.on("click", function(e) {
				$modalOpen.removeClass("open");
				$modalOpen.addClass("closing");
				$html.removeClass("modalOpen");
				setTimeout(function(){
					$modalOpen.removeClass("closing");
					$modalOpen.addClass("closed");
				}, 1000);
				angular.element(".modal__content .detailBox__content").remove();
				e.preventDefault();
			});

			$scope.openVideo = function(ctx) {

				var videoObj = {
					sources: [
							{
									src: ctx.filename,
									type: 'rtmp/mp4'
							}
					],
					tracks: [],
					poster: ctx.image
				};

				$scope.videoObj = videoObj;

				clearTimeout();
				$modalOpen.addClass("open");
				$html.addClass("modalOpen");
				$modalOpen.removeClass("closed");
				angular.element(".detailBox__content", $(this).parent()).clone().appendTo(".modal__content");

			};

			$scope.videoObj = {
				sources: [
				    {
				        src: '',
				        type: 'video/mp4'
				    }
				],
				tracks: [],
				poster: ''
			};

			//listen for when the vjs-media object changes
		 $scope.$on('vjsVideoMediaChanged', function (e, data) {
		 	//console.log('vjsVideoMediaChanged event was fired');
		 });













/*
			window.start_jw_player = function(tab, id, v, i) {

	      var mediaplayerid = 'mediaplayer';

	      if (tab == '0') {
	          mediaplayerid += 'all';
	      } else if (tab == '1') {
	          mediaplayerid += 'toapp';
	      } else if (tab == '3') {
	          mediaplayerid += 'app';
	      } else if (tab == '2') {
	          mediaplayerid += 'notapp';
	      }

	      if( navigator.userAgent.match(/webOS/i)
	           || navigator.userAgent.match(/iPhone/i)
	           || navigator.userAgent.match(/iPad/i)
	           || navigator.userAgent.match(/iPod/i) ) {

	          window.location.assign("http://www.buzzmybrand.com:1935/vod/mp4:" + v + "/playlist.m3u8")


	      } else if ( navigator.userAgent.match(/Android/i) ) {

	          window.location.assign("rtsp://www.buzzmybrand.com:1935/vod/" + v)

	      } else {

	          jwplayer(mediaplayerid  + id).setup({
	              'file': 'rtmp://www.buzzmybrand.com:1935/vod/mp4:' + v,
	              'width': 'auto',
	              'height': '250',
	              'autostart': 'true',
	              'events': {
	                  onComplete: function(e) {
	                      jwplayer(mediaplayerid + id).remove();
	                      $('#' + mediaplayerid + id).html('<a href="javascript:void(0)" onClick="start_jw_player(\'' + tab + '\', \'' + id + '\', \'' + v + '\', \'' + i + '\');" style=\'color:#555;\'><img class="img-thumbnail" src="/images/videos/' + i + '" width="width" height="250"/></a>');
	                  }
	              }
	          });
	      }
	  }
*/



}]);
