angular.module('templates-app', []);
angular.module('templates-common', []);
angular.module(
    'bmbMinisite',
    [
        'templates-app',
        'templates-common',
        'LocalStorageModule',
        'angular-loading-bar',
        'ngAnimate',
        'ngSanitize',
        'ui.router',
        'bmbMinisite.appConf',
        'bmbMinisite.home',
        'bmbMinisite.layout',
        'bmbMinisite.filter',
        'satellizer',
        'angucomplete-alt',
        'pascalprecht.translate',
        'ngTouch',
        'infinite-scroll',
        'underscore',
        'angular-cache',
        'ngCookies',
        'angularModalService',
        'toastr',
        'angularMoment',
        'valdr',
      	'vjs.video'
    ]
).config(['$urlRouterProvider', '$translateProvider', '$authProvider', 'appConf', '$logProvider','CacheFactoryProvider','$locationProvider', 'valdrProvider',
  function myappConf($urlRouterProvider,  $translateProvider, $authProvider, appConf, $logProvider, CacheFactoryProvider,$locationProvider, valdrProvider) {
    angular.extend(CacheFactoryProvider.defaults, {maxAge: 60 * 60 * 1000 });

    var contestName = location.pathname.split("/").pop();
    var comeFrom = window.is_mobile  ? "is_mobile" : "is_desktop";

    $authProvider.facebook({
      //clientId: '1156360941089059', //fab
      clientId: '566800206740950', //buzz
      url: '/msapi/auth/facebook'.concat("/", contestName, "&", comeFrom),
      redirectUri: window.location.origin.concat("/", contestName)
    });

    $authProvider.twitter({
      url: '/msapi/auth/twitter'.concat("/", contestName, "&", comeFrom),
      redirectUri: window.location.origin.concat("/", contestName)
    });

    //set translation preferences
    $translateProvider.useStaticFilesLoader({
      files: [{
          prefix: 'minisite/i18n/',
          suffix: '.json'
      }]
    });
    //$translateProvider.preferredLanguage(appConf.defaultLanguage);
    $translateProvider.preferredLanguage(appConf.defaultLanguage);
    $translateProvider.useSanitizeValueStrategy('escaped');

    //enable debug state
    $logProvider.debugEnabled(appConf.debug);

    //set home state
    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode(true);

 }])
.run(['$rootScope', '$state', '$log','$http','CacheFactory', 'moment', 'valdrMessage', function run($rootScope, $state, $log,$http,fctCache, moment) {
  if (!fctCache.get('contenutiCache')){
      fctCache('dataCache', {
      maxAge: 15 * 60 * 1000,
      cacheFlushInterval: 60 * 60 * 1000,
      deleteOnExpire: 'aggressive',
      onExpire: function (key, value) {
        $log.error('Expires: '+ key, '5005');
        $http.get(key).success(function (data) {profileCache.put(key, data);});
      }
    });
  }
  if (!fctCache.get('gpsCache')){
      fctCache('gpsCache', {
      maxAge: 5 * 60 * 1000,
      cacheFlushInterval: 10 * 60 * 1000,
      deleteOnExpire: 'aggressive'
    });
  }

  if (!fctCache.get('rprintCache')){
      fctCache('rprintCache', {
      maxAge: 5 * 60 * 1000,
      cacheFlushInterval: 7 * 60 * 1000,
      deleteOnExpire: 'aggressive'
    });
  }


}])
.controller('AppCtrl', ['$scope','$rootScope', '$location', '$state', 'cfpLoadingBar',
  function AppCtrl($scope, $rootScope, $location, $state, cfpLoadingBar) {

    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        $rootScope._state = toState;
        cfpLoadingBar.complete();
    });
    $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        cfpLoadingBar.start();
    });
}]);
