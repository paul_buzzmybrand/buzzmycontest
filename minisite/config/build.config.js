/**
 * This file/module contains all configuration for the build process.
 */
module.exports = {
  /**
   * The `build_dir` folder is where our projects are compiled during
   * development and the `compile_dir` folder is where our app resides once it's
   * completely built.
   */
  base_dir: 'dist',
  build_dir: 'dist',
  compile_dir: 'dist',
  css_parser: 'sass',

  /**
   * This is a collection of file patterns that refer to our app code (the
   * stuff in `src/`). These file paths are used in the configuration of
   * build tasks. `js` is all project javascript, less tests. `ctpl` contains
   * our reusable components' (`src/common`) template HTML files, while
   * `atpl` contains the same, but for our app's code. `html` is just our
   * main HTML file, `less` is our main stylesheet, and `unit` contains our
   * app's unit tests.
   */
  app_files: {
    js: ['src/**/*.js', 'src/conf.js',  '!src/**/*.spec.js', '!src/assets/**/*.js' ],
    coffee: {
      //'dist/build/web/src/common/layout/': 'src/common/layout/',
    },
    jsunit: [ 'src/**/*.spec.js' ],
    atpl: [ 'src/app/**/*.tpl.html' ],
    ctpl: [ 'src/common/**/*.tpl.html' ],
    html: [ 'src/index.html' ],
    sass: ['src/sass/application.sass', 'src/app/*/**/.sass'],
    sass_theme_default: ['src/assets/themes/default/sass/theme.sass', 'src/assets/themes/default/*/**/.sass'],
    sass_theme_business: ['src/assets/themes/business/sass/theme.sass', 'src/assets/themes/business/*/**/.sass'],
    sass_theme_fashion: ['src/assets/themes/fashion/sass/theme.sass', 'src/assets/themes/fashion/*/**/.sass'],
    sass_theme_food: ['src/assets/themes/food/sass/theme.sass', 'src/assets/themes/food/*/**/.sass'],
    sass_theme_love: ['src/assets/themes/love/sass/theme.sass', 'src/assets/themes/love/*/**/.sass'],
    sass_theme_music: ['src/assets/themes/music/sass/theme.sass', 'src/assets/themes/music/*/**/.sass'],
    sass_theme_nature: ['src/assets/themes/nature/sass/theme.sass', 'src/assets/themes/nature/*/**/.sass'],
    sass_theme_socialCause: ['src/assets/themes/social-cause/sass/theme.sass', 'src/assets/themes/social-cause/*/**/.sass'],
    sass_theme_sport: ['src/assets/themes/sport/sass/theme.sass', 'src/assets/themes/sport/*/**/.sass'],
    sass_theme_travel: ['src/assets/themes/travel/sass/theme.sass', 'src/assets/themes/travel/*/**/.sass']
  },

  /**
   * This is a collection of files used during testing only.
   */
  test_files: {
    js: [
      'bower_components/angular-mocks/angular-mocks.js'
    ]
  },

  /**
   * This is the same as `app_files`, except it contains patterns that
   * reference vendor code (`bower_components/`) that we need to place into the build
   * process somewhere. While the `app_files` property ensures all
   * standardized files are collected for compilation, it is the user's job
   * to ensure non-standardized (i.e. vendor-related) files are handled
   * appropriately in `vendor_files.js`.
   *
   * The `vendor_files.js` property holds files to be automatically
   * concatenated and minified with our project source files.
   *
   * The `vendor_files.css` property holds any CSS files to be automatically
   * included in our app.
   *
   * The `vendor_files.assets` property holds any assets to be copied along
   * with our app's assets. This structure is flattened, so it is not
  'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
   * recommended that you use wildcards.
   */
  vendor_files: {
    js: [
      'bower_components/jquery/dist/jquery.js',
      'bower_components/angular/angular.js',
      'bower_components/angular-i18n/angular-locale_it-it.js',
      'bower_components/bootstrap/dist/js/bootstrap.js',
      'bower_components/jquery.stellar/jquery.stellar.js',
      'bower_components/jquery-touchswipe/jquery.touchSwipe.js',
      'bower_components/jquery-is-on-screen/jquery.isonscreen.min.js',
      'bower_components/angular-cache/dist/angular-cache.js',
      'bower_components/angular-ui-map/ui-map.js',
      'bower_components/angular-route/angular-route.js',
      'bower_components/angular-slick/dist/slick.js',
      'bower_components/angular-socialshare/lib/angular-socialshare.js',
      'bower_components/angular-mass-autocomplete/massautocomplete.js',
      'bower_components/angular-cookies/angular-cookies.js',
      'bower_components/angular-resource/angular-resource.js',
      'bower_components/angular-sanitize/angular-sanitize.js',
      'bower_components/angular-scroll/angular-scroll.js',
      'bower_components/angular-animate/angular-animate.js',
      'bower_components/angular-translate/angular-translate.js',
      'bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
      'bower_components/ng-file-upload-shim/ng-file-upload-shim.min.js',
      'bower_components/ng-file-upload-shim/ng-file-upload.min.js',
      'bower_components/angular-ui-router/release/angular-ui-router.js',
      'bower_components/angular-ui-utils/modules/route/route.js',
      'bower_components/angular-loading-bar/build/loading-bar.js',
      'bower_components/angular-local-storage/dist/angular-local-storage.js',
      'bower_components/angular-bootstrap/ui-bootstrap.js',
      'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
      'bower_components/angular-resource/angular-resource.js',
      'bower_components/angular-toastr/dist/angular-toastr.tpls.min.js',
      'bower_components/angular-touch/angular-touch.js',
      'bower_components/angularjs-datepicker/src/js/angular-datepicker.js',
      'bower_components/angucomplete-alt/angucomplete-alt.js',
      'bower_components/moment/moment.js',
      'bower_components/angular-moment/angular-moment.min.js',
      'bower_components/moment/locale/it.js',
      'bower_components/moment-countdown/dist/moment-countdown.min.js',
      'bower_components/humanize-duration/humanize-duration.js',
      'bower_components/modernizr/modernizr.js',
      'bower_components/angular-timer/dist/angular-timer.min.js',
      'bower_components/slick-carousel/slick/slick.js',
      'bower_components/satellizer/satellizer.js',
      'bower_components/ngInfiniteScroll/build/ng-infinite-scroll.js',
      'bower_components/ngCordova/dist/ng-cordova.js',
      'bower_components/ng-device-detector/ng-device-detector.js',
      'bower_components/valdr/valdr.min.js',
      'bower_components/valdr/valdr-message.min.js',
      'bower_components/underscore/underscore-min.js',
      'bower_components/deep_pick/index.js',
      'bower_components/angular-underscore-module/angular-underscore-module.js',
      'bower_components/angular-modal-service/dst/angular-modal-service.js',
      'bower_components/ngmap/build/scripts/ng-map.min.js',
      'bower_components/OwlCarousel/owl-carousel/owl.carousel.min.js',
      'bower_components/jquery.smartbanner/jquery.smartbanner.js',
      'bower_components/recordrtc/RecordRTC.min.js',
      'bower_components/video.js/dist/video.min.js',
      'bower_components/vjs-video/dist/vjs-video.min.js',
      'bower_components/isMobile/isMobile.min.js',
      'bower_components/swfobject/swfobject/swfobject.js',
      'bower_components/exif-js/exif.js'
    ],
    css: [],
    fonts: [],
    assets: []
  }
};
