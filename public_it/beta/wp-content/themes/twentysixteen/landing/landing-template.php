<?php /* Template Name: Landing Page Template */ ?>
<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/landing/application.css">
	<?php wp_head(); ?>
	<style type="text/css">
		body:not(.custom-background-image):before, body:not(.custom-background-image):after{height: 0 !important;}
	</style>
</head>

<body <?php body_class(); ?>>
	
	<main id="content" role="main">
		<div id="free_signup_container">
			<div class="container signup">
				<div class="signup_header">
					<div class="logo">
						<a href=""><img height="45" src="<?php echo get_template_directory_uri();?>/landing/logo-navbar.png" width="264"></a>
					</div>
					
					
				</div>
				
				<div class="signup_container">
					<div class="signup_info">
						<h2><?php echo get_post_meta( get_the_ID(), 'Sign_up_title', true ); ?></h2>
						<!--<h4>Use Wistia and all its features for free, forever!</h4>-->
					</div>
					
					<div class="signup_form">
						<div class="form_wrapper">
							<?php echo get_post_meta( get_the_ID(), 'embed_form_code', true ); ?>
							<div class="email_suggestion" style="">Did you mean<span class="email"><span class="address">asf</span>@<span class="domain">gmail.com</span></span>?</div><div class="fineprint"><p>Cliccando questo pulsante acconsenti ai <a class="terms" href="/terms" target="_blank">Termini e Condizioni d'uso</a>.</p></div>
						</div>
					</div>
				</div>
			</div>

			<section id="pricing-questions">
				<div class="container">
					<?php the_content(); ?>
				</div>
			</section>
		</div>
	</main>

</body>
</html>