<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
class ContestController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            
            $location = Input::get('location');
            $name = Input::get('name');
            $reward = Input::get('reward');
            
            //return $location;
            $limit = Input::get('limit');
            if(!isset($limit))
            {
                $limit = 10;
            }
            //return $name;
        
                
                $contestList = Contest::with('company','videos')->where('name','like',$name.'%')->where('location','like',$location.'%')
                        ->where( function($query) use ($reward)
                        {
                            $query->orWhere('reward1_type', 'like', $reward.'%')
                                  ->orWhere('reward2_type', 'like', $reward.'%')
                                  ->orWhere('reward3_type', 'like', $reward.'%');
                        })->orderBy('start_time','desc')->paginate($limit);
             /*
                $contestList = Contest::with('company','videos')->where(function($query) use ($name, $location, $reward, $limit)
                {                   
                    $query->where('name', 'like', $name.'%')
                          ->where('location', 'like', $location.'%')
                          ->where('reward1_type','like',$reward.'%')->orWhere('reward2_type','like',$reward.'%');
                })->paginate($limit);
              * 
              */
                $statusCode = 200;
                $response = Response::make($contestList,$statusCode);
                return $response;
   
              
            //return Contest::with('company','videos')->orderBy('created_at','ASC')->paginate($limit);
	}
        
        public function search()
	{
            $location = Input::get('location');
            $name = Input::get('name');
            $reward = Input::get('reward');
            
            //return $location;
            $limit = Input::get('limit');
            if(!isset($limit))
            {
                $limit = 10;
            }
          try
            {
                
                //$contestList = Contest::with('company','video_contests.videos')->where('name','like',$name.'%')->orWhere('location','like',$location.'%')->paginate($limit);
                $contestList = Contest::with('company','video_contests.videos')->where(function($query) use ($name, $location, $reward, $limit)
                {
                    $query->where('name', 'like', $name.'%')
                          ->where('location', 'like', $location.'%')
                          ->where('prize_money','like',$reward.'%');
                })->paginate($limit);
                return $contestList;
            }
            catch(ModelNotFoundException $e)
            {
                $errorArray=array(
                    'Error' => '404',
                    'Message' => 'Not Found'
                );
                return $errorArray;
            } 
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('contests.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
            //return Company::with('contests')->find($id);
            try
            {
                $contestList = Contest::with('company')->where('id', $id)->first();//findOrFail($id);
                
                $contestIsJoin = Contest::where('id',$id)->whereHas('users', function($q)
                {
                    $ownerId = ResourceServer::getOwnerId();
                    
                   $q->where('user_id', $ownerId);
                })->get();
                $contestVideos = Contest::with('videos')->where('id',$id)->get()->toArray();
                
                $contestJMTScore = 0;
                $i = 0;
                foreach($contestVideos[0]['videos'] as $videos)
                {
                    $contestJMTScore = $videos['score'] + $contestJMTScore;
                    $contestVideosArray[$i]['id'] = $videos['id'];
                    $contestVideosArray[$i]['name'] = $videos['name'];
                    $contestVideosArray[$i]['duration'] = $videos['duration'];
                    $contestVideosArray[$i]['image'] = $videos['image'];
                    $contestVideosArray[$i]['filename'] = $videos['filename'];
                    $contestVideosArray[$i]['score'] = $videos['score'];
                    $contestVideosArray[$i]['user_id'] = $videos['user_id'];
                    $contestVideosArray[$i]['location'] = $videos['location'];
                    $contestId = $contestVideos[0]['videos'][$i]['pivot']['contest_id'];
                    $contestName = Contest::where('id',$contestId)->get(array('name'));
                    //return $contestName;
                    $contestVideosArray[$i]['contestName'] = $contestName[0]['name'];
                    $i++;
                }
                //return $contestVideosArray;
                //$contestList['contestScore'] = $contestJMTScore;
                //return $contestVideos[0]['videos'][0]['pivot'];
                //return $contestIsJoin;
                $ownerId = ResourceServer::getOwnerId();
                //return $ownerId;
                if ( $contestIsJoin->count())
                {
                    $userIsJoin = TRUE;
                }
                else
                {
                    $userIsJoin = FALSE;
                }
                $statusCode = 200;
                $contestArray=$contestList->toArray();
                $contestArray['videos']=$contestVideosArray;
                $contestArray['contestJMTScore']=$contestJMTScore;
                $contestArray['userIsJoin']=$userIsJoin;
                
                $response = Response::json($contestArray, $statusCode);    
                return $response;
            }
            catch(ModelNotFoundException $e)
            {
                $statusCode = 200;
                $description = 'Event Not Found';
                $returnResponse = array('description'=>$description,'errorCode'=>404);
                $response = Response::json($returnResponse,$statusCode);
                return $response;
            }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('contests.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//Company::with('contests')->find($id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
        public function eventList()
	{
            //return Contest::all(['name']);
	
            $userWhatList = Contest::whereHas('users', function($q)
                {
                    $ownerId = ResourceServer::getOwnerId();
                    
                   $q->where('user_id', $ownerId);
                })->get(array('id','name'));
            return $userWhatList;
        }
        
        public function rewardList()
	{
            return Reward::all(['reward_type']);
	}
        
        public function join($id)
	{
            $ownerId = ResourceServer::getOwnerId();
            Contest::find($id)->users()->attach($ownerId);
            $statusCode=200;
            $description='User Successfully joined Contest';
            $returnResponse = array('description'=>$description,'errorCode'=>200);
            $response = Response::json($returnResponse,$statusCode);
            return $response;
	}
       
        /*********  Controller method for Contest *******/
        /* web part*/
        /* web part*/
        /* web part*/
        
        public function postEvent()
        {
            $rules = array(
                'city'=>'required|min:4',
                'contest_name'=>'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            
            if ($validator->fails()) {
                
                Session::flash('message1', 'Error! please enter mandatory fields to add event!');
                return Redirect::to('company/profile');
            }
            else 
            {
                $contest = new Contest;

                $image = Input::file('event_image');
                if($image)
                {
                    $image_name = $image -> getClientOriginalName();
                    $destinationPath = 'images/events';
                    $contest->image = $image_name;
                    $upload_appImage = Input::file('event_image')->move($destinationPath, $image_name);
                }
                $contest -> company_id = Input::get('id');
                $contest -> name = Input::get('contest_name');
                $contest -> location = Input::get('city');
                //$contest -> score = Input::get('score');

                $contest -> save();



                return Redirect::to('company/profile');
            }
        }
        
        public function deleteEvent($id)
        {
            $event = Contest::find($id);
            $event -> delete();
            
            return Redirect::to('company/profile');
        }
        
        public function editEvent($id)
	{
            $event = Contest::find($id);
            return Redirect::to('company');
        }
        
        
        public function eventEdit()
        {
            $rules = array(
                'event_name'=>'required',
                'city'=>'required'  ,
                'email'=>'required|email',
                'phone'=>'numeric'
            );
            
            $validator = Validator::make(Input::all(), $rules);
            
            if($validator->passes())
            {    
                $event_id = Input::get('event_id');
                $contest = Contest::find($event_id);
                
                $image = Input::file('app_image');
                if($image)
                {
                    $image_name = $image -> getClientOriginalName();
                    $destinationPath = 'images/events';
                    $upload_appImage = Input::file('app_image')->move($destinationPath, $image_name);
                
                    $contest -> image = $image_name;
                }
                
                $contest -> name = Input::get('event_name');
                
                $contest->location = Input::get('city');
                $contest->concept = Input::get('concept');
                $contest->contact_name = Input::get('contact_name');
                $contest->phone = Input::get('phone');
                $contest->email = Input::get('email');
                $contest->rules = Input::get('rules');
                $contest->judges = Input::get('judges');
                
                $contest-> reward1_type = Input::get('reward_type1');
                $contest-> reward2_type = Input::get('reward_type2');
                $contest-> reward3_type = Input::get('reward_type3');
                
                $contest-> reward1_desc = Input::get('reward1');
                $contest-> reward2_desc = Input::get('reward2');
                $contest-> reward3_desc = Input::get('reward3');
                
                $contest-> save();
                
                Session::flash('message1', 'Event record successfully updated');
                return Redirect::to('company/profile');
                
            }
            else
            {
                Session::flash('message1', 'Error! please enter mandatory fields to edit event details!');
                return Redirect::to('company/profile');
            }
        }
}

