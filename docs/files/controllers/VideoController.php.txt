<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class VideoController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
             $limit = Input::get('limit');
             $location = Input::get('location');
             $title = Input::get('title');

            if(!isset($limit))
            {
                $limit = 10;
            }
            $videoList = Video::where('location','like',$location.'%')->where('name','like',$title.'%')->orderBy('score','desc')->paginate($limit);
            $statusCode = 200;
            $response = Response::make($videoList, $statusCode);
            return $response;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            return View::make('videos.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            //Inputs contest,title,video,duration,image

            $inputs = Input::all();

            if(!isset($inputs['contest'])||!isset($inputs['title'])||!isset($inputs['video'])||!isset($inputs['duration'])||!isset($inputs['image']))
            {
                $description='Required Paramaters Not Found';
                $statusCode=400;
                return Response::make($description, $statusCode);
            }



            $videoFile=Input::file('video');
            $videoFilePath=$videoFile->getRealPath();
            $videoFileName=md5_file($videoFilePath);
            $videoExtension=strtolower($videoFile->getClientOriginalExtension());
            //$videoExtension=strtolower($videoFile->getMimeType());
            if (!isset($videoExtension) or $videoExtension=='')
            {
                $videoExtension = 'mp4';
            }
            $imageFile=Input::file('image');
            $imageFilePath=$imageFile->getRealPath();
            //$imageFileName=md5_file($imageFilePath);
            //dd($videoFileName);
            $imageExtension=strtolower($imageFile->getClientOriginalExtension());
            if (!isset($imageExtension) or $imageExtension=='')
            {
                $imageExtension = 'jpg';
            }

            //$imageExtension=strtolower($imageFile->getMimeType());
            if(     File::move($videoFile,'videos/'."$videoFileName.$videoExtension")&&
                    File::move($imageFile,'images/videos/'."$videoFileName.$imageExtension")
                    )
            {

                exec("./GenerateFacebookShareHTMLForUploadedVideo.sh $videoFileName.$videoExtension",$output);
                exec("./GenerateThumbnails.sh $videoFileName.$videoExtension",$output);

                $ownerId = ResourceServer::getOwnerId();
                $user=User::find($ownerId);


                $video=new Video();
                $video->filename="$videoFileName.$videoExtension";
                $video->name=$inputs['title'];
                $video->duration=$inputs['duration'];
                $video->image="$videoFileName.$imageExtension";
                $video->user_id=$ownerId;
                $video->location=$user->country;
                $video->save();

                $contestName=$inputs['contest'];

                if ($inputs['contest'] != "MyChannel")
                {
                    //dd ($contestName);
                    $contest=Contest::where('name',$contestName)->first();
                    $video->contests()->attach($contest->id);
                }

                $statusCode=200;
                return Response::make($video,$statusCode);

            }
            $description='Error in File Upload';
            $statusCode=400;
            return Response::make($video,$statusCode);


	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
            try
            {
                $videoList = Video::findOrFail($id);

                return $videoList;
            }
            catch(ModelNotFoundException $e)
            {
                $statusCode = 200;
                $description = 'Video Not Found';
                $response = Response::json(array('description'=>$description, 'errorCode'=>404),$statusCode);
                return $response;
            }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            return View::make('videos.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
