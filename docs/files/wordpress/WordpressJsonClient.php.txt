<?php

class WordpressJsonClient
{
    protected $wordpressUrl;
    
    protected $jsonPluginSuffix;
    
    protected $maxJsonLength;
    
    protected $wordpressContentUrl;
    
    protected $jmtvContentUrl;
    
    public function __construct()
    {
        $config = Config::get('wordpress');
        $this->wordpressUrl = $config['wordpressUrl'];
        $this->jsonPluginSuffix = $config['jsonPluginSuffix'];
        $this->maxJsonLength = $config['maxJsonLength'];
    }
    
    public function getUrl()
    {
            return $this->wordpressUrl;
    }

    public function rawJsonRPC($url)
    {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $json = curl_exec($ch);
            Log::info("rawJsonRPC('".$url."') = ".$json);
            return $json;
    }

    public function jsonRPC($name)
    {
            return $this->rawJsonRPC($this->wordpressUrl.$name.$this->jsonPluginSuffix);
    }

    public function getPage($name,$locale)
    {
            try
            {
                
                    //trying to get page matching locale
                    $json_str = $this->jsonRPC(empty($locale) ? $name : $name."-".$locale);
                    $json = json_decode($json_str);
                    if ($json->status == "ok")
                    {
                            return $json;
                    }
            }
            catch (\Exception $e) {}

            try
            {
                    //page matching locale not found. Now trying without locale
                    $json_str = $this->jsonRPC($name);
                    $json = json_decode($json_str);
                    return $json;
            }
            catch (\Exception $e)
            {
                    return null;
            }
    }
}
?>

