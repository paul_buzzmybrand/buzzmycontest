{{ HTML::style('website_public/css/bootstrap.css') }}
<div class="row">
    <div class="col-md-12">
        <div class="well" style="height:100%;">
            <h3>{{ trans('messages.app_picture') }}</h3>
            <div class="jumbotron">
                {{ trans('messages.desc_upload_app') }}
            </div>
        </div>
    </div>
</div>
