{{ HTML::style('website_public/css/bootstrap.css') }}
<div class="row">
    <div class="col-md-12">
        <div class="well" style="height:100%;">
            <h3>{{ trans('messages.end_date') }}</h3>
            <div class="jumbotron">
                {{ trans('messages.desc_end_date') }}
            </div>
        </div>
    </div>
</div>
