'use strict';

angular.module('dashboard')
    .controller('CompanyCtrl', ["$scope", "auth", "backend", function ($scope, auth, backend) {
      $scope.company = {};
      $scope.message = null;
      $scope.success = null;
      $scope.company_id = null;
      $scope.loading = true;

      auth.user().then(function(response) {
        $scope.company_id = response.data.company_id;
        backend.get('/admin/get-company/' + $scope.company_id)
          .success(function(data) {
            // Populate country select
            $scope.countries = COUNTRY_LIST;
            $scope.company = data;
            $scope.company.language = window.location.href.indexOf('.com') > -1 ? 'en' : 'it';
            $scope.company.company_id = $scope.company_id;
            $scope.loading = false;
          })
          .error(function(err) {
            $scope.message = err;
            $scope.success = false;
          });
      });

      $scope.setSuccess = function(message) {
        if(message)
          $scope.message = message;

        $scope.success = true;
        // Clear the password fields
        $scope.company.user_password = '';
        $scope.company.user_password_repeat = '';

        // Auto-hide after 3 seconds
        setTimeout(function() {
          $scope.message = null;
          $scope.success = null;
          $scope.$apply();
        }, 3000);
      }

      $scope.setError = function(message) {
        $scope.message = message;
        $scope.$apply();
        setTimeout(function() {
          $scope.message = null;
          $scope.$apply();
        }, 5000);
      }

      $scope.submit = function(data) {
        $scope.loading = true;
        backend.post('/admin/edit-company', data)
          .success(function(data) {
            $scope.loading = false;
            if(data.result == '0') {
              $scope.setError(data.message);
              $scope.success = false;
              return;
            }

            $scope.setSuccess('COMPANY_FORM_MESSAGE_SUCCESS');
            // Reload user data
            angular.element($('#auth-block')).scope().loadUser();
            return;

          })
          .error(function(err) {
            $scope.loading = false;
            $scope.setError(err.message);
            $scope.success = false;
          });
      }
      
    }]);