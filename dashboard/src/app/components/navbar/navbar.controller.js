'use strict';

angular.module('dashboard')
    .controller('NavbarCtrl', ['$scope', '$location', 'backend', function ($scope, $location, backend) {
        $scope.launchPath = '#/launch';
        // $scope.instantOnly = backend.getSetting('INSTANT_ONLY');

        $scope.sections = [
            {
                name: 'YOUR_CONTESTS',
                path: '#/contests',
                position: 10,
                icon: 'tachometer'
            },
            {
                name: 'COMPANY_SETTINGS',
                path: '#/company',
                position: 20,
                icon: 'cog'
            },
            {
                name: 'PAYMENTS',
                path: '#/payments',
                position: 30,
                icon: 'money'
            },
            {
                name: 'LOGOUT',
                path: '/company/logout',
                position: 40,
                icon: 'sign-out'
            }
        ];

        // $scope.isLaunch = function() 
        // {
          // return $location.url().indexOf('launch') > -1;
        // }

        // $scope.toggleLaunch = function()
        // {
          // if(!$scope.instantOnly)
            // $('#nav-launch').slideToggle();
          // else
            // $scope.goTo('#/launch/instant');
        // }

        // $scope.goTo = function(path)
        // {
            // External links must bypass $location
            // if(path.substring(0,1) !== '#') {
                // window.location = path;
                // return;
            // }

            // $location.url(path.substring(1, path.length + 1));
            // if(angular.element('#btn-navbar-collapse').css('display') == 'block')
              // angular.element('#btn-navbar-collapse').click();
        // };

        $scope.isActive = function(path) 
        {
            // Exclude external links
            if(path.substring(0,1) !== '#')
                return false;

            path = path.replace('#', '');

            // Handle root url
            if(path.length === 1 && $location.path().length === 1)
                return true;

            var fullPath = $location.path().substring(1, $location.path().length + 1);
            var basePathLen = fullPath.indexOf('contests') > -1 && fullPath.indexOf('/') >= 0 ? fullPath.indexOf('/') : fullPath.length + 1;
            var basePath = fullPath.substring(0, basePathLen);

            return path.indexOf(basePath) > 0;
        };
		
		$scope.user = function()
		{
			return backend.api({
                    'method': 'GET', 
                    'url': '/dashboard-api/utils/current-user'
                })
                .success(function(data, status) {
                    
                    
                    $scope.userData = data;
                    $scope.userIsAdmin = data.is_admin;
                    
                })                
                .catch(function(err) {
                    redirectToLogin();
                    return err;
                });
		}
  }]);
  