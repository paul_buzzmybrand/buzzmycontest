'use strict';

angular.module('dashboard')
    .controller('AuthCtrl', ["$scope", "auth", function ($scope, auth) {

      $scope.loadUser = function() {
        auth.user(true).then(function(response) {
          $scope.user = response.data;
        }).catch(function(exception) {
          console.log(exception);
        });
      }

      $scope.loadUser();

    }]);