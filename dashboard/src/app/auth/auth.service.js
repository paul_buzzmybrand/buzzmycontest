'use strict';

angular.module('dashboard')
    .factory('auth', ["$q", "$rootScope", "backend", function($q, $rootScope, backend) {

        var _user = null;

        var redirectToLogin = function()
        {
            window.location = '/';
        }

        var showPage = function()
        {
            // Show the page
            $('html').addClass('show');
        }

        var user = function(force) {

              var force = force || false;

              if(_user && !force)
                // Return cached user
                return $q(function(resolve, reject) {
                  resolve({data: _user});
                });

              return backend.api({
                    'method': 'GET', 
                    'url': '/dashboard-api/utils/current-user'
                })
                .success(function(data, status) {
                    if(typeof(data) !== 'object') {
                        redirectToLogin();
                        return;
                    }
                    // Cache data
                    _user = data;

                    showPage();
                    $rootScope.isEmailConfirmed = data.is_email_confirmed;
                    $rootScope.companyId = data.company_id;
                    $rootScope.userId = data.user_id;
                    $rootScope.userCountry = data.user_country;
                    $rootScope.userIsAdmin = data.is_admin;
                    if(!$rootScope.isEmailConfirmed)
                      pollEmailConfirmation();
                    
                    return _user;
                })
                .error(function(err, status) {
                    redirectToLogin();
                    return err;
                })
                .catch(function(err) {
                    redirectToLogin();
                    return err;
                });
            }

        var pollEmailConfirmation = function() 
        {
          if($rootScope.isEmailConfirmed) {
            return;
          }

          else
            setTimeout(function() {
              // Poll again
              user(true);
            }, 2000);
        }

        $rootScope.resendVerificationEmail = function()
        {
          
          var loader = angular.element('#email-verification-box');
          loader.addClass('loading');
          backend.post('/admin/send-welcome-email/' + $rootScope.userId, {
            'language': backend.getSetting('LOCALE'),
          })
          .success(function(data) {
            loader.removeClass('loading');
            $rootScope.emailConfirmationSent = true;
          })
          .catch(function(err) {
            loader.addClass('loading');
          });
        }

        return {
            user: user
        }

    }]);