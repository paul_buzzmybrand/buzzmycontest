'use strict';

angular.module('dashboard')
.directive('creditCards', ['backend', '$http', 'noCAPTCHA', function(backend, $http, noCAPTCHA) {
    return {
      restrict: 'E',
      templateUrl: 'app/credit-cards/index.html',
	  controller: 'ContestEditCtrl',
      link: function(scope, element, attrs) {
        /*
          try {
          paymill !== undefined
        } catch(Error) {
          var s = document.createElement('script'); // use global document since Angular's $document is weak
          s.src = 'https://bridge.paymill.com/';
          document.body.appendChild(s);
        }*/

        scope.card = null;
        scope.paymentError = null;
        scope.loading = scope.loading ? scope.loading : {'cc': true};
        scope.recaptcha = '';

        scope.isEmpty = function (obj) {
           return angular.equals({},obj); 
        };

        // Get card
        backend.get('/dashboard-api/payments/card')
          .success(function(data) {
            scope.card = data;
            scope.loading['cc'] = false;
          });
		  
	    function resetCreditCardForm() {
			$(".submit-button").removeAttr("disabled");
			
			$("#card-number").val('');
			$("#card-name").val('');
			$("#card-cvc").val('');
			$("#card-expiry-month").val('');
			$("#card-expiry-year").val('');
		};

        function removeCardPost() {
            backend.post('/dashboard-api/payments/remove-card', {
                'card': scope.card
            })
            .success(function(data) {
				grecaptcha.reset();
				resetCreditCardForm();
                scope.loading['cc'] = false;
                scope.card = {};
            })
            .error(function(err) {
                scope.loading['cc'] = false;
                // scope.card = cardSource;
                scope.paymentError = err.error.message;
            });
        };
		
		function updateInvoices() {
			// Get invoices
			backend.api({
			  url: '/dashboard-api/payments/invoices'
			}).success(function(data) {
				scope.invoices = data;
				scope.loading['invoices'] = false;
			});
		}

        function removeLoading () {
            // body...
        }

        scope.removeCard = function() {
          var cardSource = angular.copy(scope.card);
          var canCancelCard = true;
          scope.loading['cc'] = true;
          backend.get('/dashboard-api/contests/get-scheduled')
          .success(function(data) {
                if (data.length > 0) {
                    var cancelButton;
                    var title;
                    var description;
                    var locale = backend.getSetting('LOCALE').toLowerCase();
                    if(locale == 'en') {
                        title = 'Attention please';
                        description = 'Your scheduled contests are going to be switched to status "draft".<br>Are you sure you want continue?';
                        cancelButton = 'CANCEL';
                    } else {
                        title = 'Attenzione';
                        description = 'I tuoi contest schedulati saranno portati allo status "bozza".<br>Sei sicuro di voler continuare?';
                        cancelButton = 'ANNULLA';
                    }
                    swal({
                        title: title,
                        text: description,
                        type: 'warning',
                        html: true,
                        confirmButtonColor: "#337ab7",
                        confirmButtonText: "OK",
                        showCancelButton: true,
                        cancelButtonText: cancelButton,
                        closeOnConfirm: true,
                        closeOnCancel: true 
                        }, 
                        function(isConfirm) {
                            if (isConfirm) {
								scope.loading['invoices'] = true;
                                backend.get('/dashboard-api/contests/switch-scheduled-contests')
                                .success(function(data) {
                                    removeCardPost();
                                    updateInvoices();
                                })
                                .error(function(err) {
									scope.loading['invoices'] = false;
                                    scope.loading['cc'] = false;
                                    scope.card = cardSource;
                                    scope.paymentError = err.error.message;
                                    scope.$apply();
                                });
                            } else {
                                scope.loading['cc'] = false;
                                scope.loading['cc'] = false;
                                scope.$apply();
                            }
                    });
                } else {
                    removeCardPost();
                    
                }
            })
            .error(function(err) {
                scope.loading['cc'] = false;
                scope.card = cardSource;
                scope.paymentError = err.error.message;
                scope.$apply();
            });
        }
        /*
        scope.PaymillResponseHandler = function (error, result) {
            if (error) {
                // Show the error message above the form
                scope.paymentError = error.apierror;
            } else {
                scope.paymentError = '';
                var form = $("#payment-form");
                // Token
                var token = result.token;
                scope.loading['cc'] = true;
                backend.post('/dashboard-api/payments/make-card', {
                  'paymillToken': result.token
                }).success(function(data) {
                  scope.card = data;
                  scope.loading['cc'] = false;
                }).error(function(err) {
                  scope.loading['cc'] = false;
                  if(typeof(err) == 'string')
                    scope.paymentError = err;
                  else
                    scope.paymentError = "Server error: " + err.error.message;
                });
            }
            $(".submit-button").removeAttr("disabled");
        }

        scope.PaymillSubmit = function () {
            // Deactivate the submit button to avoid further clicks
            $('.submit-button').attr("disabled", "disabled");
            if (false == paymill.validateCardNumber($('.card-number').val())) {
              scope.paymentError = 'CC_INVALID_CARD_NUMBER';
              $(".submit-button").removeAttr("disabled");
              return false;
            }

            if (false == paymill.validateExpiry($('.card-expiry-month').val(), $('.card-expiry-year').val())) {
              scope.paymentError = "CC_INVALID_DATE_OF_EXPIRY";
              $(".submit-button").removeAttr("disabled");
              return false;
            }
            
            var method = 'cc';
            var params = {
                amount:         $('.card-amount-int').val(),
                currency:       $('.card-currency').val(),
                number:         $('.card-number').val(),
                exp_month:      $('.card-expiry-month').val(),
                exp_year:       $('.card-expiry-year').val(),
                cvc:            $('.card-cvc').val(),
                cardholder:     $('.card-holdername').val()
            };

            paymill.createToken(params, scope.PaymillResponseHandler);

            return false;
        };
        */
        scope.StripeEventHandler = function (status, response) {

            // Grab the form:
            var $form = $('#payment-form');
            // retrieve coupon
            //var coupon_code = $("#card-coupon").val();

            if (response.error) { // Problem!
                // Show the error message above the form
                scope.paymentError = response.error.message;
                $form.find('.submit-button').prop('disabled', false); // Re-enable submission
            } else { // Token was created!
                scope.paymentError = '';
                // Get the token ID:
                var token = response.id;
                scope.loading['cc'] = true;

                backend.post('/dashboard-api/payments/make-customer', {
                    'stripeToken': response.id,
                    'service_type': scope.contest.contest_service_type,
                    //'coupon': coupon_code,
                    'customer_id': ''
                }).success(function(data) {
                    scope.card = data;
                    scope.loading['cc'] = false;
                    var locale = backend.getSetting('LOCALE').toLowerCase();
                    if(locale == 'en') {
                        swal("CARD VERIFIED", "Your credit card has been verified successfully!", "success");
                    } else {
                        swal("CARTA VERIFICATA", "La tua carta di credito è stata verificata con successo", "success");
                    }
                }).error(function(err) {
                    scope.loading['cc'] = false;
                    $form.find('.submit-button').prop('disabled', false); // Re-enable submission
                    if(typeof(err) == 'string')
                        scope.paymentError = err;
                    else
                        scope.paymentError = err.error.message;
                });
                
                
                
            }
        }
          
        

        scope.StripeSubmit = function () {
            var error_message = '';
            var locale = backend.getSetting('LOCALE').toLowerCase();

            $('.submit-button').attr("disabled", "disabled");

            if (false == Stripe.card.validateCardNumber($('.card-number').val())) {
                scope.paymentError = 'CC_INVALID_CARD_NUMBER';
                $(".submit-button").removeAttr("disabled");
                return false;
            }

            if (false == Stripe.card.validateExpiry($('.card-expiry-month').val(), $('.card-expiry-year').val())) {
                scope.paymentError = "CC_INVALID_DATE_OF_EXPIRY";
                $(".submit-button").removeAttr("disabled");
                return false;
            }

            if (false == Stripe.card.validateCVC($('.card-cvc').val())) {
                scope.paymentError = "CC_INVALID_CVC";
                $(".submit-button").removeAttr("disabled");
                return false;
            }

            //var $form = $('#payment-form');
            if (scope.recaptchaCreditCard.length > 0) {
                backend.post('/dashboard-api/payments/check-recaptcha', {'recaptcha': scope.recaptchaCreditCard })
                .success(function(data) {
									
					
                    if (!data.success) {
                        if(locale == 'en') {
                            error_message = 'Recaptcha error, please refresh the page before continue.';
                        } else {
                            error_message = 'Errore nel Recaptcha, ricarica la pagine prima di continuare.';
                        }
                        scope.paymentError = error_message;
                        // $(".submit-button").removeAttr("disabled");
                        return false;
                    }
					else {
						var params = {
							number:    $('.card-number').val(),
							exp_month: $('.card-expiry-month').val(),
							exp_year:  $('.card-expiry-year').val(),
							cvc:       $('.card-cvc').val(),
							name:      $('.card-holdername').val()
						};

						// Request a token from Stripe:
						Stripe.card.createToken(params, scope.StripeEventHandler);
					}
						
                })
                .error(function(err) {
                    if(locale == 'en') {
                        error_message = 'Recaptcha error, please refresh the page before continue.';
                    } else {
                        error_message = 'Errore nel Recaptcha, ricarica la pagine prima di continuare.';
                    }
                    scope.paymentError = error_message;
                    // $(".submit-button").removeAttr("disabled");
                    return false;
                });
            } else {
                if(locale == 'en') {
                    error_message = 'Accept captcha before submit!';
                } else {
                    error_message = 'Accetta il Recaptcha prima di aggiungere la carta!';
                }
                scope.paymentError = error_message;
                $(".submit-button").removeAttr("disabled");
                return false;
            };
			
			/*
            var params = {
                number:    $('.card-number').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year:  $('.card-expiry-year').val(),
                cvc:       $('.card-cvc').val(),
                name:      $('.card-holdername').val()
            };

            // Request a token from Stripe:
            Stripe.card.createToken(params, scope.StripeEventHandler);

            // Prevent the form from being submitted:
            return false;
			*/
        }

      }
    };
  }]
);