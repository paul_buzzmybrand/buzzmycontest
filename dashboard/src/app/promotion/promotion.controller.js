'use strict';

angular.module('dashboard')
.directive('promotion', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/promotion/index.html',
      controller: ['$scope', function($scope) {
        var contest = $scope.contest;

        $('[data-toggle="tooltip"]').tooltip();
      }]
    };
  }
);