'use strict';

angular.module('dashboard')
  .controller('EditCtrl', ["$scope", "backend", "$routeParams", "contestUtils", "$location", function ($scope, backend, $routeParams, contestUtils, $location) {
    
    var c_id = $routeParams['id'];
    var contestType = null;

    $scope.subtitle = 'CONTEST_edit_SUBTITLE';
    $scope.contest = null;
    $scope.error = null;
    $scope.currentTab = 'edit';
    $scope.utils = contestUtils;

    $scope.setError = function(message)
    {
      $scope.error = message;
      setTimeout(function() {
        $scope.error = null;
        $scope.$apply();
      }, 3000);
    }

    $scope.delete = function(c_id)
    {
      angular.element('#delete-confirmation-' + c_id).modal('hide');
      // Set the loading class on the deleted contest
      angular.element('#contest-box-' + c_id).addClass('loading');
      backend.post('/admin/delete-contest/' + c_id)
        .success(function(data) {
          if(data.result == '0') {
            $scope.setError(data.message);
            angular.element('#contest-box-' + c_id).removeClass('loading');
            return;
          }

          // Redirect to contest list
          $location.url('/contests');
        })
        .error(function(err) {
          $scope.setError(err.message);
          angular.element('#contest-box-' + c_id).removeClass('loading');
          return;
        });
    }

    // Load basic contest data
    backend.get('/admin/contest-description/' + c_id)
      .success(function(data) {
        $scope.utils.checkAccess(data, $scope.currentTab);

        $scope.contest = data;
        $scope.lastSaved = angular.copy($scope.contest);
        contestType = data['contest_type'];
      })
      .error(function(err) {
        $scope.contest = {};
        $scope.error = err.message;
      });

  }]);