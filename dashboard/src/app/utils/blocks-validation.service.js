'use strict';

angular.module('dashboard')
  .directive('startDate', function() {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        // if(!ctrl || scope.serviceType == 'instant') return;

        // Also validate when related field changes
        scope.$watch('contest.' + attrs.startDate, function() {
          ctrl.$validate();
        });

        ctrl.$validators.startDate = function(modelValue, viewValue) {
          return ctrl.$isEmpty(viewValue) || !scope.contest[attrs.startDate] || modelValue < scope.contest[attrs.startDate];
        };
      }
    };
  })
  .directive('instantDate', function() {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;


        ctrl.$validators.instantDate = function(modelValue, viewValue) {
          return ctrl.$isEmpty(viewValue) || modelValue >= moment().add(1, 'hours');
        };
      }
    };
  })
  .directive('currentDate', function() {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        ctrl.$validators.currentDate = function(modelValue, viewValue) {
          return ctrl.$isEmpty(viewValue) || modelValue.getDate() >= new Date().getDate();
        };
      }
    };
  })
  .directive('futureDate', function() {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        ctrl.$validators.futureDate = function(modelValue, viewValue) {
          return ctrl.$isEmpty(viewValue) || modelValue >= moment(); // .add(1, 'days')
        };
      }
    };
  })
  .directive('autoEndDate', function() {
    // Force end date = start_date + 2 days
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        // Only for instant contests
        // if(!ctrl || scope.contest.contest_service_type == 2) return;

        var contest = scope.contest;

        var updateEndDate = function() {
         if(!contest.contest_start_date)
            return;

          if (contest.contest_end_date == "" || typeof contest.contest_end_date == "undefined") {
            contest.contest_end_date = new Date(contest.contest_start_date);
            contest.contest_end_date.setDate(contest.contest_start_date.getDate() + 2);
          } else {
            if (contest.contest_end_date.getDate() > contest.contest_start_date.getDate() + 31) {
              $scope.shortModal('CONTEST TITLE, DATES, CONTEST BRIEF', 'ALLOWED 31 DAYS CONTESTS');
            }
          }
        }

        // Update on start_date change
        scope.$watch('contest.contest_start_date', updateEndDate);
      }
    };
  })
  .directive('fileRequired', function() {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        scope.$watch(attrs.fileModel, function() {
          ctrl.$validate();
        });

        scope.$watch(attrs.alsoWatch, function() {
          ctrl.$validate();
        });

        ctrl.$validators.fileRequired = function(modelValue, viewValue) {
          if(typeof(scope.$eval(attrs.fileModel)) == 'string')
            return true;
          
          return !scope.$eval(attrs.fileRequired) || scope.$eval(attrs.fileModel) !== undefined;
        };
      }
    };
  })
  .directive('fileType', function() {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        scope.$watch(attrs.fileModel, function() {
          ctrl.$validate();
        });

        ctrl.$validators.fileType = function(modelValue, viewValue) {
          // Ignore strings (they are already-uploaded files)
          if(typeof(scope.$eval(attrs.fileModel)) == 'string')
            return true;

          return !scope.$eval(attrs.fileModel) || !typeof(scope.$eval(attrs.fileModel)) == 'object' || scope.$eval(attrs.fileModel+ '.type').indexOf(attrs.fileType) > -1;
        };
      }
    };
  })
  .directive('imageWidth', ["$q", function($q) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        ctrl.$asyncValidators.imageWidth = function(modelValue, viewValue) {
          var def = $q.defer();
          
          if(!attrs.imageWidth) {
            def.resolve(true)
            return def.promise;
          }

          var _URL = window.URL || window.webkitURL;
          var file = elm[0].files[0];
          var img = new Image();
          var isValid = null;

          img.onload = function() {
            if(this.width == attrs.imageWidth) {
              def.resolve(true)
            } else {
              def.reject();
            }
          }

		  if (typeof file == "object") {
			img.src = _URL.createObjectURL(file);
		  }	

          return def.promise;
        };
      }
    };
  }])
  .directive('imageHeight', ["$q", function($q) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        ctrl.$asyncValidators.imageHeight = function(modelValue, viewValue) {
          var def = $q.defer();

          if(!attrs.imageHeight) {
            def.resolve(true)
            return def.promise;
          }

          var _URL = window.URL || window.webkitURL;
          var file = elm[0].files[0];
          var img = new Image();
          var isValid = null;

          img.onload = function() {
            if(this.height == attrs.imageHeight) {
              def.resolve(true)
            } else {
              def.reject();
            }
          }

		  if (typeof file == "object") {
			img.src = _URL.createObjectURL(file);
		  }	

          return def.promise;
        };
      }
    };
  }])
  .directive('imageMaxWidth', ["$q", function($q) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        ctrl.$asyncValidators.imageMaxWidth = function(modelValue, viewValue) {
          var def = $q.defer();
          
          if(!attrs.imageMaxWidth) {
            def.resolve(true)
            return def.promise;
          }

          var _URL = window.URL || window.webkitURL;
          var file = elm[0].files[0];
          var img = new Image();
          var isValid = null;

          img.onload = function() {
            if(this.width <= attrs.imageMaxWidth) {
              def.resolve(true)
            } else {
              def.reject();
            }
          }

		  if (typeof file == "object") {
			img.src = _URL.createObjectURL(file);
		  }	

          return def.promise;
        };
      }
    };
  }])
  .directive('imageMaxHeight', ["$q", function($q) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        ctrl.$asyncValidators.imageMaxHeight = function(modelValue, viewValue) {
          var def = $q.defer();

          if(!attrs.imageMaxHeight) {
            def.resolve(true)
            return def.promise;
          }

          var _URL = window.URL || window.webkitURL;
          var file = elm[0].files[0];
          var img = new Image();
          var isValid = null;

          img.onload = function() {
            if(this.height <= attrs.imageMaxHeight) {
              def.resolve(true)
            } else {
              def.reject();
            }
          }

		  if (typeof file == "object") {
			img.src = _URL.createObjectURL(file);
		  }	

          return def.promise;
        };
      }
    };
  }])
  .directive('imageMinWidth', ["$q", function($q) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        ctrl.$asyncValidators.imageMinWidth = function(modelValue, viewValue) {
          var def = $q.defer();
          
          if(!attrs.imageMinWidth) {
            def.resolve(true)
            return def.promise;
          }

          var _URL = window.URL || window.webkitURL;
          var file = elm[0].files[0];
          var img = new Image();
          var isValid = null;

          img.onload = function() {
            if(this.width >= attrs.imageMinWidth) {
              def.resolve(true)
            } else {
              def.reject();
            }
          }

		  if (typeof file == "object") {
			img.src = _URL.createObjectURL(file);
		  }	
          return def.promise;
        };
      }
    };
  }])
  .directive('imageMinHeight', ["$q", function($q) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        ctrl.$asyncValidators.imageMinHeight = function(modelValue, viewValue) {
          var def = $q.defer();

          if(!attrs.imageMinHeight) {
            def.resolve(true)
            return def.promise;
          }

          var _URL = window.URL || window.webkitURL;
          var file = elm[0].files[0];
          var img = new Image();
          var isValid = null;

          img.onload = function() {
            if(this.height >= attrs.imageMinHeight) {
              def.resolve(true)
            } else {
              def.reject();
            }
          }

		  if (typeof file == "object") {	
			img.src = _URL.createObjectURL(file);
          }

          return def.promise;
        };
      }
    };
  }])
  .directive('imageWh', ["$q", function($q) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        ctrl.$asyncValidators.imageWh = function(modelValue, viewValue) {
          var def = $q.defer();

          if(!attrs.imageWh) {
            def.resolve(true)
            return def.promise;
          }

          var _URL = window.URL || window.webkitURL;
          var file = elm[0].files[0];
          var img = new Image();
          var isValid = null;

          img.onload = function() {
            if(this.height == this.width) {
              def.resolve(true)
            } else {
              def.reject();
            }
          }
		  if (typeof file == "object") {	
			img.src = _URL.createObjectURL(file);
          }
          return def.promise;
        };
      }
    };
  }])
  .directive('imageBgWh', ["$q", function($q) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        ctrl.$asyncValidators.imageBgWh = function(modelValue, viewValue) {
          var def = $q.defer();

          if(!attrs.imageBgWh) {
            def.resolve(true)
            return def.promise;
          }

          var _URL = window.URL || window.webkitURL;
          var file = elm[0].files[0];
          var img = new Image();
          var isValid = null;

          img.onload = function() {
            if((this.height/5 - this.width/8) < 1 && (this.height/5 - this.width/8) > -1) {
              def.resolve(true)
            } else {
              def.reject();
            }
          }

		  if (typeof file == "object") {
			img.src = _URL.createObjectURL(file);
		  }	

          return def.promise;
        };
      }
    };
  }])
  .directive('imageLogo', ["$q", function($q) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        ctrl.$asyncValidators.imageLogo = function(modelValue, viewValue) {
          var def = $q.defer();

          if(!attrs.imageLogo) {
            def.resolve(true)
            return def.promise;
          }

          var _URL = window.URL || window.webkitURL;
          var file = elm[0].files[0];
          var img = new Image();
          var isValid = null;
          
          var preview_logo_left = angular.element('#custom_logo_left');
          var preview_logo_right = angular.element('#custom_logo_right');
          
          if (typeof file == "object") {
            img.src = _URL.createObjectURL(file);
            var preview_logo_img = angular.element(img);
            var reader = new window.FileReader();
            reader.readAsDataURL(file); 
            reader.onloadend = function() {
              preview_logo_left.css({
                'background-image': 'url(' + reader.result + ')',
                'background-size': 'contain',
                'background-repeat': 'no-repeat',
                'position': 'absolute',
                'top': '23%',
                'left': '34%',
                'width': '3%',
                'height': '3%',
				'border-radius': '2px'
              });
            }
          }

          return def.promise;
        };
      }
    };
  }])
  .directive('imageBg', ["$q", function($q) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        ctrl.$asyncValidators.imageBg = function(modelValue, viewValue) {
          var def = $q.defer();

          if(!attrs.imageBg) {
            def.resolve(true)
            return def.promise;
          }

          var _URL = window.URL || window.webkitURL;
          var file = elm[0].files[0];
          var img = new Image();
          var isValid = null;
          
          var preview_bg_area = angular.element('#custom_bg_image');
          
          if (typeof file == "object") {
            img.src = _URL.createObjectURL(file);
            var preview_bg_img = angular.element(img);
            var reader = new window.FileReader();
            reader.readAsDataURL(file); 
            reader.onloadend = function() {
              preview_bg_area.css({
                'background-image': 'url(' + reader.result + ')',
                'background-size': 'cover',
                'position': 'absolute',
                'top': '23%',
                'bottom': '29%',
                'left': '28.5%',
                'right': '28.5%',
              });
            }
          }
          

          return def.promise;
        };
      }
    };
  }])
  .directive('imageFlyer', ["$q", function($q) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        ctrl.$asyncValidators.imageFlyer = function(modelValue, viewValue) {
          var def = $q.defer();

          if(!attrs.imageFlyer) {
            def.resolve(true)
            return def.promise;
          }

          var _URL = window.URL || window.webkitURL;
          var file = elm[0].files[0];
          var img = new Image();
          var isValid = null;
          
          var preview_flyer_area = angular.element('#custom_flyer_image');
          var preview_flyer_area2 = angular.element('#custom_flyer_image2');
          
          if (typeof file == "object") {
            img.src = _URL.createObjectURL(file);
            var preview_flyer_img = angular.element(img);
            var reader = new window.FileReader();
            reader.readAsDataURL(file); 
            reader.onloadend = function() {
              preview_flyer_area.css({
                'background-image': 'url(' + reader.result + ')',
                'width': '300px',
                'height': '300px',
              });
              preview_flyer_area2.css({
                'background-image': 'url(' + reader.result + ')',
                'width': '300px',
                'height': '300px',
              });
            }
          }

          return def.promise;
        };
      }
    };
  }])
  .directive('availableRoute', ["$q", "backend", function($q, backend) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ctrl) {
        if(!ctrl) return;

        ctrl.$asyncValidators.availableRoute = function(modelValue, viewValue) {
          // Skip if empty
          if(ctrl.$isEmpty(modelValue))
            return $q.when();

          // Set a loading class
          angular.element(elm).addClass('loading-input');

          var def = $q.defer();
		  
		  var patt = /^[-a-z0-9]{3,15}[^\sA-Z@_]+$/;

          backend.get('/admin/check-av-contest-route', {
            'params': {'contest_route': modelValue.replace(/\W+/g, "_"), 'contest_id': scope.contest.contest_id}})
          .success(function(data) {
            if ((data.result == "1") && (patt.test(modelValue)))
              def.resolve(true);
            else
              def.reject();

            angular.element(elm).removeClass('loading-input');
          })
          .catch(function() {
            def.reject();
            angular.element(elm).removeClass('loading-input');
          });

          return def.promise;
        };
      }
    };
  }])
  .factory('blocksValidation', function() {

    var rules = {

      'objectives': function(contest) {
        var valid = true;
        var errors = {
          'objectives': null,
          // 'sales': null,
          // 'contest_term_cond_off_line_sales': null,
          // 'contest_term_cond_on_line_sales': null,
        }

        if(contest.contest_objectives.length == 0) {
          valid = false;
          if(errors)
            errors['objectives'] = 'ERROR_OBJECTIVE_REQUIRED';
        }

        // if(contest.contest_objectives.indexOf(999) > -1 && contest.contest_objectives.indexOf(4) < 0 && contest.contest_objectives.indexOf(5) < 0) {
          // valid = false;
          // if(errors)
            // errors['sales'] = 'ERROR_SALES_SELECT_ONLINE_OFFLINE';
        // }

        // Check offline sales t&c
        // if(contest.contest_objectives.indexOf(4) > -1 && !contest['contest_term_cond_off_line_sales']) {
          // valid = false;
          // if(errors)
            // errors['contest_term_cond_off_line_sales'] = 'ERROR_CUSTOM_TC_OFFLINE_REQUIRED';
        // }

        // Check online sales t&c
        // if(contest.contest_objectives.indexOf(5) > -1 && !contest['contest_term_cond_on_line_sales']) {
          // valid = false;
          // if(errors)
            // errors['contest_term_cond_on_line_sales'] = 'ERROR_CUSTOM_TC_ONLINE_REQUIRED';
        // }

        return {valid: valid, errors: errors};
      }
    }

    return function(contest, block, errors) {
      var errors = errors || true; // Show errors by default
      // Return true if no rules
      if(rules.hasOwnProperty(block) === false)
        return {valid: true, errors: {}}

      var rslt = rules[block](contest);
      return {'valid': rslt.valid, 'errors': errors ? rslt.errors : {}}
    }

  });