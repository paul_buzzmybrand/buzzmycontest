'use strict';

angular.module('dashboard')
  .controller('ProfileImageCtrl', ["$scope", function ($scope) {
    $scope.error = null;

    $scope.browse = function() 
    {
      $('#id-profile-image-upload').click();
    }

    $scope.uploadProfileImage = function()
    {
      var fd = new FormData();

      fd.append('image', $('#id-profile-image-upload')[0].files[0]);
      fd.append('company_id', $scope.companyId);

      angular.element('#profile-image .modal-body').addClass('loading');

      $.ajax({
          type: 'POST',
          url: '/admin/save-dashboard-image',
          data: fd,
          processData: false,
          contentType: false ,  // tell jQuery not to set contentType
          success: function(data, status) {
              angular.element('#profile-image .modal-body').removeClass('loading');
              var response = $.parseJSON(data);
              
              if(response.result == 1) {
                $scope.error = null;
                angular.element($('#auth-block')).scope().loadUser();
                angular.element('#profile-image').modal('hide');
                $scope.$apply();
              }

              if(response.result == 0) {
                $scope.error = response.message;
                $scope.$apply();
                return false;
              }
          },
          error: function(data) {
            angular.element('#profile-image .modal-body').removeClass('loading');
            var response = $.parseJSON(data);
            $scope.error = response.error.message;
            $scope.$apply();
          }
      });
    }

  }]);