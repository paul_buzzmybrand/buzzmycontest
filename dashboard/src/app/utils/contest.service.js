'use strict';

angular.module('dashboard')
  .factory('contestUtils', ["$location", function($location) {

    var contestTypeMap = {
      1: 'video',
      2: 'photo',
      3: 'essay'
    }

    var statusDef = {
      '1': {
        name: 'FILTER.IN_PROGRESS',
        label: 'STATUS.IN_PROGRESS',
        'class': 'in-progress',
        position: 1,
        buttons: {
          'analytics': true,
          'approve': true,
          'edit': false,
          'delete': false
        }
      },
      '2': {
        name: 'FILTER.OVER',
        label: 'STATUS.OVER',
        'class': 'over',
        position: 2,
        buttons: {
          'analytics': true,
          'approve': false,
          'edit': false,
          'delete': false
        }
      },
      '3': {
        name: 'FILTER.SCHEDULED',
        label: 'STATUS.SCHEDULED',
        'class': 'scheduled',
        position: 3,
        buttons: {
          'analytics': false,
          'approve': false,
          'edit': true,
          'delete': false
        }
      },
      '4': {
        name: 'FILTER.DRAFT',
        label: 'STATUS.DRAFT',
        'class': 'draft',
        position: 4,
        buttons: {
          'analytics': false,
          'approve': false,
          'edit': true,
          'delete': true
        }
      },
      /*'6': {
        name: 'FILTER.REJECTED',
        label: 'STATUS.REJECTED',
        'class': 'over',
        position: 4,
        buttons: {
          'analytics': false,
          'approve': false,
          'edit': false,
          'delete': true
        }
      },*/
      '': {
        name: 'FILTER.ALL',
        position: 5,
        buttons: {}
      },
    }

    var getStatusLabel =  function(id)
    {
      return statusDef[id].label;
    }

    var getStatusClass =  function(id)
    {
      return statusDef[id]['class'];
    }

    var isDisabledButton =  function(statusId, button)
    {
      return statusDef[statusId]['buttons'][button] == false;
    }

    var getHrefFor = function(contest, action)
    {
      if(isDisabledButton(contest.contest_status, action))
        return null;

      return '#/contests/' + contest.contest_id + '/' + action;
    }

    var getContestTypeName = function(id)
    {
      return contestTypeMap[id];
    }

    var checkAccess = function(contest, action)
    {
      if(isDisabledButton(contest.contest_status, action))
        $location.url('/contests');
    }

    return {
      getStatusLabel: getStatusLabel,
      getStatusClass: getStatusClass,
      isDisabledButton: isDisabledButton,
      getHrefFor: getHrefFor,
      getContestTypeName: getContestTypeName,
      checkAccess: checkAccess,
      statusDef: statusDef,
      openSharingPopup: function(post, target)
      {

        var sharingUrl = encodeURI(location.protocol + '//' + location.host + "/FacebookSharePhoto/" + post.post_image.replace('.jpg', '.html').replace('.png', '.html'));
        var popupUrl = '';
        if(target == 'twitter')
          popupUrl = "http://twitter.com/share?text=" + encodeURI(post.post_name) + '&url=' + sharingUrl + '&text=Check%20it%20out!+&via=bmbdemo';
        if(target == 'facebook')
          popupUrl = "http://facebook.com/sharer/sharer.php?u=" + sharingUrl + '&title=' + post.post_name;

        if(popupUrl == '')
          return;

        window.open(popupUrl, "Share", "width=600,height=400");
      },
      getObjectives: function(level) 
      {
        // 1: top level, 2: sales sublevel
        var level = level || 1;
        // Get objectives lazily because they're not available at boot time
        var objectives = Immutable.OrderedMap(OBJECTIVES);
        return objectives.filter(function(v, k) {
          if(level == 1 && !v.sales_child)
            return true;
          if(level == 2 && v.sales_child)
            return true;
        }).toObject();
      },
      getCurrentLanguageCode: function() {
        return window.location.href.indexOf('.com') > -1 ? 'en' : 'it';
      }
    }


  }]);