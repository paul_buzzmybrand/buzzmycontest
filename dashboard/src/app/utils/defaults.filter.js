'use strict';

angular.module('dashboard')
.filter('defaults', function() {
    return function(input, defaultTo) {
        if(input == '' || input === null || input === undefined)
            return defaultTo;

        return input;
    }
}).filter('startFrom', function() {
    return function(input, start) {
        start = +start;
        return input.slice(start);
    }
});