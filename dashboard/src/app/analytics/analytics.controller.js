'use strict';

angular.module('dashboard')
  .controller('AnalyticsCtrl', ["$scope", "backend", "$routeParams", "contestUtils", function ($scope, backend, $routeParams, contestUtils) {
    
    window.scrollTo(0, 0);
    
    var c_id = $routeParams['id'];
    var contestType = null;
    
    var contestTypeMap = {
      1: 'video',
      2: 'photo',
      3: 'essay'
    }

    $scope.subtitle = 'CONTEST_ANALYTICS_SUBTITLE';
    $scope.contest = null;
    $scope.analytics = null;
    $scope.posts = null;
    $scope.error = null;
    $scope.isBusy = false;
    $scope.currentTab = 'analytics';
    $scope.nextPage = 1;
    $scope.utils = contestUtils;
    $scope.loading = $scope.loading ? $scope.loading : {'an': true};

    $scope.loadPosts = function()
    {
        // Avoid errors if we're already at the last page
        if($scope.nextPage === null) return;

        if($scope.isBusy === true) return; // request in progress, return
        $scope.isBusy = true;

        backend.get('/admin/single-post-data-for-' + contestTypeMap[contestType] + 's/' + c_id, {
            params: {
              page: $scope.nextPage,
              col: 4
            }
        })
        .success(function(data) {
          if(data.length > 0) {
            if($scope.posts === null)
              $scope.posts = [];
            // Append to the correct list
            $scope.posts = $scope.posts.concat(data);
          }

          if(data.length < 4)
            // We're at the last page
            $scope.nextPage = null;
          else
            // Increment page
            $scope.nextPage++;
            
          $scope.isBusy = false;
          return;
        });
        $scope.loading['an'] = false;
    }

    $scope.notify = function(post)
    {
      $scope.error = 'notify(' + post.post_name + ') NOT IMPLEMENTED';
    }

    $scope.notifyAll = function()
    {
      $scope.error = 'notifyAll() NOT IMPLEMENTED';
    }

    // Load basic contest data
    var blocks = angular.element('.analytics-block');
    blocks.addClass('loading');
    backend.get('/admin/contest-detail/' + c_id)
      .success(function(data) {
        $scope.utils.checkAccess(data, $scope.currentTab);

        $scope.contest = data;
        $scope.lastSaved = angular.copy($scope.contest);
        contestType = data['contest_type'];
        // Turn off loading
        blocks.removeClass('loading');
        // Load the first page of posts
        $scope.loadPosts();

        console.log($scope.posts);
      })
      .error(function(err) {
        $scope.contest = {};
        blocks.removeClass('loading');
        $scope.error = err.message;
      });

    // Load analytics
    backend.get('/admin/contest-data/' + c_id)
      .success(function(data) {
        $scope.analytics = data;
      })
      .error(function(err) {
        $scope.analytics = {};
        $scope.error = err.message;
      });



  }]);