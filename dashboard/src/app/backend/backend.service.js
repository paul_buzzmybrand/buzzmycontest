'use strict';

angular.module('dashboard')
    .factory('backend', ["$rootScope", "$http", "$location", function($rootScope, $http, $location) {

        var baseUrl = 'local.buzzmybrand.co';
        var environment = 'development';

        // Environment detection
        if(location.host.indexOf('localhost') === -1) {
            baseUrl = '//' + location.host;
            if(baseUrl.indexOf(':') > -1) {
                environment = 'testing';
            }
        } 
        else 
        {
            environment = 'development';
            baseUrl = '//' + location.host;
        }

        $rootScope.startPlayer = function(tab, id, v, i)
        {
          window.start_jw_player(tab, id, v, i);
        }
		$rootScope.fullscreenPopup = function(url, name)
		{
			
			
			if( navigator.userAgent.match(/webOS/i)
                     || navigator.userAgent.match(/iPhone/i)
                     || navigator.userAgent.match(/iPad/i)
                     || navigator.userAgent.match(/iPod/i)
					 ||	navigator.userAgent.match(/Android/i)) {
                    
                    window.location.assign(url)
                 
                } else {
					fullscreen_popup(url, name);
				}
			
			
			
			
		}

        // Dirty hack: create an accessor for a global variable
        $rootScope.getContactUsMail = function () { return CONTACT_US_EMAIL }

        return {

            'environment': environment,
            'baseUrl': baseUrl,

            // Simple wrapper around $http to point to the correct backend url
            'api': function(config) {
                config.url = baseUrl + config.url;
                return $http(config);
            },

            'get': function(url, config) {
                var url = baseUrl + url;
                return $http.get(url, config);
            },

            'post': function(url, data, config) {
                var config = config || null;
                var url = baseUrl + url;
                return $http.post(url, data, config);
            },

            // Lazy access to global settings
            'getSetting': function(key) {
              return window[key];
            },

            'getContestUrl': function(c) {
              var url = 'https://local.buzzmybrand.co';
              if(environment == 'testing')
                url = url + ':8081';
              
              return url + '/' + c.contest_route;
            }
        }
    }]);