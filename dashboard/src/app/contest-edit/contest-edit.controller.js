'use strict';

angular.module('dashboard').controller('ContestEditCtrl', ['$scope', '$filter', 'backend', '$routeParams', 'contestUtils', '$location', 'auth', 'blocksValidation', '$anchorScroll', '$cookies', 'modalService', '$http', '$timeout', 'noCAPTCHA', 'md5', function($scope, $filter, backend, $routeParams, contestUtils, $location, auth, blocksValidation, $anchorScroll, $cookies, modalService, $http, $timeout, noCAPTCHA, md5) {
        var c_id = null;
        var saveChangePopup = true;
        var today = new Date();
        today.setHours(today.getHours() + 2);
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var absUrl = $location.absUrl();

        // Check user comes from email verification and initialize tooltip
        angular.element(document).ready(function() {
            if ('welcome' in $routeParams) {
                swal($filter('translate')('WELCOME_TITLE'), $filter('translate')('WELCOME_MESSAGE'), 'success');
            }
        });

        // setInterval(function() {
        //    console.log('ID: '+$scope.contest.contest_template_id);
        //   }, 3000);
        $scope.check_terms = 0;
        $scope.currencyRate = 1.11;
        //$scope.recaptchaContestEdit = '';

        var contestDefaults = {
            'contest_id': 0,
            'contest_status': 4,
            'contest_service_type': 0,
            'contest_currency': 'EUR',
            'customer_connected_to_facebook_page': '',
            'customer_connected_to_twitter_page': '',
            'customer_connected_to_youtube_page': '',
            'contest_objectives': [],
            'contest_hashtag': '',
            'contest_auto_start': 1,
            'contest_start_time': today,
            'contest_end_time': today,
            'contest_start_date': today,
            'contest_approval': 0,
            'rewards': [],
			'contest_how_to_win': '',
            'contest_no_rewards': 0,
            'use_standard_tc': 1,
			'valid_tc': true,
            'contest_win_modality': '',
            'post_visibility': '',
            'contest_route': '',
            'contest_video_seeding_promotion': 0,
            'contest_display_promotion': null,
            'contest_coupon_code': null,
			'use_template': 1,
			'confirmed_by_customer':'',
			'contest_promotion':null,
			'amount':'',
            // 'contest_start_time': new Date(),
            // 'contest_influencers_promotion': 0,
        };

        $scope.host = $location.host();
        $scope.date = new Date();

        $scope.approvalStatusDict = {
            0: 'STATUS_TO_APPROVE',
            1: 'STATUS_APPROVED',
            2: 'STATUS_NOT_APPROVED',
        };

        $scope.currentTab = 'edit';
        $scope.noChoisenTmp = null;
        $scope.showAlert = false;
        $scope.getCurrentTemplate = null;
        $scope.getCurrentUser = null;
        $scope.startDateError = false;
        $scope.equalDateError = false;
        $scope.locationChangeStarted = false;
        $scope.contest = null;
        $scope.reward = null;
        $scope.templates = [];
        $scope.lastSaved = [];
        $scope.subObjectives = [];
        $scope.twitter_trends = [];
        $scope.globalError = null;
        $scope.twitterLocations = [];
        $scope.hasPromotionType = '';
        $scope.hasCredit = null;
        $scope.company = null;
        $scope.validCompany = [];
        $scope.locale = backend.getSetting('LOCALE').toLowerCase();
        // $scope.instantOnly = backend.getSetting('INSTANT_ONLY');
        $scope.datePickersOpen = {
            'contest_start_date': false,
            'contest_end_date': false,
            'contest_vote_start': false,
            'contest_vote_end': false
        };
        $scope.dateFormat = contestUtils.getCurrentLanguageCode == 'en' ? 'MM/dd/YYYY' : 'dd/MM/yyyy';
        $scope.errors = {};
        $scope.currentBlock = null;
        $scope.minDate = new Date();
        $scope.minDate.setDate($scope.minDate.getDate() + 1);
        // $scope.maxDate = new Date();
        // $scope.maxDate.setDate($scope.minDate.getDate() + 31);
        $scope.utils = contestUtils;
        $scope.countries = backend.getSetting('COUNTRY_LIST');
        $scope.getContestUrl = backend.getContestUrl;
        $scope.layout = null; // edit or create
        $scope.serviceType = null; // null or instant or standard
        $scope.ready = false;
        $scope.languages = backend.getSetting('LANGUAGES');
        $scope.colors = backend.getSetting('TEMPLATE_HEADINGS_COLORS');
        $scope.currentPage = 0;
        $scope.pageSize = 10;
        $scope.fonts = [];
        $scope.hstep = 1;
        $scope.mstep = 15;
        $scope.GeoIp = null;
        $scope.GeoIp_error = null;
        // Keep this in the actual order you need
        $scope.blocks = {
            'contest_type': false,
            'connect': false,
            'packages': false,
            'objectives': false,
            'trends': false,
            'brief': false,
            'approveSettings': false,
            'rewards': false,
            'tc': false,
            'template': false,
            'postSettings': false,
            'promotion': false,
            'coupon': false,
			'companyinfo': false,
            'payments': false
        };
		//$scope.isCheckoutDisabled = false;

        // Credit Card
        $scope.card = null;
        $scope.paymentError = null;
        $scope.loading = $scope.loading ? $scope.loading : {
            'cc': true,
			'pa': false,
			'pl': true,
			'po':false,
        };
		
		// Get plans
		backend.api({
			url: '/dashboard-api/payments/get-plans'
		}).success(function(data) {
			$scope.plans = data;
			$scope.loading['pl'] = false;
		});

        $scope.isEmpty = function(obj) {
            return angular.equals({}, obj);
        };

        // Get card
        backend.get('/dashboard-api/payments/card')
            .success(function(data) {
                $scope.card = data;
                $scope.loading['cc'] = false;
            });

        // Get currency convert rate		
        $.ajax({
            type: 'get',
            url: 'https://api.fixer.io/latest?symbols=USD,EUR',
            success: function(data) {
                $scope.currencyRate = data.rates.USD;
            }
        });
		

        $scope.setError = function(message) {
            $scope.globalError = message;
            $scope.$apply();
            setTimeout(function() {
                $scope.globalError = null;
                $scope.$apply();
            }, 5000);
        };

        $scope.delete = function(c_id) {
            angular.element('#delete-confirmation-' + c_id).modal('hide');
            // Set the loading class on the deleted contest
            angular.element('#contest-box-' + c_id).addClass('loading');
            backend.post('/admin/delete-contest/' + c_id)
                .success(function(data) {
                    if (data.result == '0') {
                        $scope.setError(data.message);
                        angular.element('#contest-box-' + c_id).removeClass('loading');
                        return;
                    }

                    // Redirect to contest list
                    $location.url('/contests');
                })
                .error(function(err) {
                    $scope.setError(err.message);
                    angular.element('#contest-box-' + c_id).removeClass('loading');

                });
        };

        $scope.showHowItWorks = function(type) {
            var helpText = '';
            switch (type) {
                case 'twitter':
                    helpText = $filter('translate')('TW_CONNECT_HELP_TEXT');
                    break;
                case 'youtube':
                    helpText = $filter('translate')('YT_CONNECT_HELP_TEXT');
                    break;
                default:
                    helpText = $filter('translate')('FB_CONNECT_HELP_TEXT');
            }
            swal({
                title: $filter('translate')('HOW_IT_WORKS'),
                text: helpText,
                html: true
            });
        };

        $scope.shortModal = function(title, description, action) {
            var action = action || false;
            var redirect = false;
            var button = 'OK';

            if (action == 'scheduled') {
                button = $filter('translate')('GO_TO_CONTESTS_LIST');
                redirect = true;
            }

            swal({
                    title: title,
                    text: description,
                    type: 'success',
                    showCancelButton: action == 'scheduled',
                    confirmButtonColor: '#337ab7',
                    confirmButtonText: button,
                    cancelButtonText: 'OK',
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (redirect && isConfirm) {
                        $scope.$apply(function() {
                            $scope.locationChangeStarted = true;
                            $location.path($location.url('contests').hash());
                        });
                    }
                });

            
        };

        $scope.deletePermanently = function(c_id) {
            // angular.element('#delete-confirmation-' + c_id).modal('hide');
            // Set the loading class on the deleted contest
            angular.element('#contest-box-' + c_id).addClass('loading');
            $.ajax({
                type: 'POST',
                url: '/admin/delete-permanently-contest/' + c_id,
                async: false,
                processData: false,
                contentType: false, // tell jQuery not to set contentType
                success: function(data, status) {
                    if (data.result == '0') {
                        $scope.setError(data.message);
                        angular.element('#contest-box-' + c_id).removeClass('loading');

                    }
                },
                error: function(err) {
                    $scope.setError(err.message);
                    angular.element('#contest-box-' + c_id).removeClass('loading');

                }
            });
        };
		
		$scope.setContestPromotion = function() {
			if ($scope.contest.contest_promotion == null)
				$scope.contest.contest_promotion = "NO";
		}

        $scope.notifyEmailBudget = function() {
            // Set the loading class on the deleted contest

            if ($scope.contest.contest_promotion != undefined) {
                if ($scope.contest.contest_promotion == "YES") {
					
                    auth.user(true).then(function(response) {
                        $scope.getCurrentUser = response.data;

                        var user_id = $scope.getCurrentUser.user_id;
                        // console.log(user_id);
                        $.ajax({
                            type: 'post',
                            url: '/admin/notify-email-budget',
                            data: {
                                id: user_id
                            },
                            error: function(err) {
                                $scope.setError(err.message);
                            }
                        });
                    }).catch(function(exception) {
                        console.log(exception);
                    });
					
                }
            } else {
                $scope.contest.contest_promotion = null;
            }
        };

        $scope.activateBlock = function(block) {
			if (block == 'ALL') {
				$scope.currentBlock = 'contest_type';
			}
			else {
				$scope.currentBlock = block;
			}			
            var found = false;
            $.each($scope.blocks, function(k, v) {
                if (block == 'ALL') {
                    $scope.blocks[k] = true;
                    return;
                }

                if (!found) {
                    $scope.blocks[k] = true;
                }

                if (k == block)
                    found = true;
            });
            // $scope.$apply();
        };

        $scope.openDatePicker = function($event, name) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.datePickersOpen[name] = true;
        };

        $scope.validate = function(block, withErrors) {
            var validation = blocksValidation($scope.contest, block, withErrors);
            angular.extend($scope.errors, validation['errors']);
            return validation['valid'];
        };

        $scope.nextBlock = function(block, validate, checktmp) {
            var found = false;
            var validate = validate || false;
            var checktmp = checktmp || false;

            if (checktmp) {
                if ($scope.contest.use_template == 1 && (typeof $scope.contest.contest_template_id == "undefined" || $scope.contest.contest_template_id == "")) {
                    $scope.setnoChoisenTmp(true);
                    return false;
                } else {
                    $scope.closeBlock('template');
                }
            }

            if (block == "summary") {
				
				if ($scope.contest.contest_coupon_code == null)
					$scope.contest.contest_coupon_code = '';
				
                $scope.updateStartTime();
                $scope.updateEndTime();
				$scope.getCurrentTemplateCall();
                if ($scope.startDateErrorStatus() || $scope.equalDateErrorStatus() || $scope.contestForm.briefForm.$error.instantDate) {
                    $scope.summaryModal('show', 'error_dates');
                } else {
                    $scope.not_valid_dates = false;
                }
                $scope.summaryModal('show');
                return;
            }

            if (block == "tc" && $scope.contest.rewards.length == 0) {
                $scope.contest.contest_no_rewards = 1;
            }
			else if (block == "tc" && $scope.contest.rewards.length > 0) {
				$scope.contest.contest_no_rewards = 0;
			}

            var seq = Immutable.OrderedMap($scope.blocks).keys();

            // Do not proceed if we have errors
            /*
			  if(block && !$scope.validate(block, validate)) {
				return;
			}*/

			if (block == 'companyinfo_or_payments') {
				if ($scope.checkCompany()) {
					block = 'payments';
				}
				else
					block = 'companyinfo';
			}

            do {
                var l = seq.next();
                var b = l.value;

                $scope.activateBlock(b);

                if (block == b)
                    break;

            } while (!l.done);

            setTimeout(function() {
                var toggleBlockId = jQuery('#block_' + b);
                jQuery('html, body').animate({
                    scrollTop: toggleBlockId.offset().top
                }, 500);
                toggleBlockId.addClass('active');
                toggleBlockId.find('.contest-block-content').slideDown('fast');
            }, 500);
        };

        $scope.closeBlock = function(block) {
            if ($scope.contest.contest_status == 4) {    
                var toggleBlockId = jQuery('#block_' + block);
                toggleBlockId.find('.contest-block-content').slideUp('fast');
                toggleBlockId.removeClass('active');
            }
        };

        $scope.toggleBlock = function(block, action) {

            var toggleBlockId = jQuery('#block_' + block);

            if (!action) {
                if (toggleBlockId.hasClass('active')) {
                    //qui si chiude la slide
                    toggleBlockId.find('.contest-block-content').slideUp('fast');
                    toggleBlockId.removeClass('active');
                } else {
                    //qui si apre la slide
                    toggleBlockId.find('.contest-block-content').slideDown('fast');
                    toggleBlockId.addClass('active');
                }
            } else {
                if (action == "add") {
                    if (toggleBlockId.hasClass('active')) {
                        return;
                    } else {
                        toggleBlockId.find('.contest-block-content').slideDown('fast');
                        toggleBlockId.addClass('active');
						$scope.currentBlock = block;
                    }
                }

                if (action == "remove") {
                    if (toggleBlockId.hasClass('active')) {
                        toggleBlockId.find('.contest-block-content').slideUp('fast');
                        toggleBlockId.removeClass('active');
                    }
                }
            }


        };

        $scope.gotoBlock = function(block) {
            jQuery('html, body').animate({
                scrollTop: jQuery("#block_" + block).offset().top
            }, 500);
        };

        $scope.approveContest = function(step) {
            var loader = angular.element('.contest-action-buttons');
            loader.addClass('loading');
            backend.post('/admin/approve-contest', {
                    'contest_id': $scope.contest.contest_id,
                    'approval_step': step
                })
                .success(function(data) {
                    loader.removeClass('loading');
                    if (data.result == 1) {
                        $scope.contest.contest_approval = step;
                        return;
                    }

                    $scope.setError(data.message);
                })
                .catch(function(data) {
                    loader.removeClass('loading');
                    $scope.setError(data.error.message);
                });
        };


        $scope.togglePromotionType = function(t) {
            var tLabel = 'contest_' + t + '_promotion';
            $scope.contest[tLabel] = $scope.contest[tLabel] == 0 ? 1 : 0;
            $scope.contest.hasPromotionType = $scope.contest.contest_influencers_promotion || $scope.contest.contest_display_promotion || $scope.contest.contest_video_seeding_promotion ? 'OK' : '';
        };

        $scope.getNextRewardRank = function() {
            // return $scope.contest.rewards ? $scope.contest.rewards.length + 1 : 1;
            var counter = 1;
            angular.forEach($scope.contest.rewards, function(v, k) {
                $scope.contest.rewards[k].rank = counter;
                counter += 1;
            });

            return counter;
        };

        $scope.showStartDate = function(status) {
			if (status == 'no') {
				$scope.contest.contest_auto_start = 0;
			}
			else
				$scope.contest.contest_auto_start = 1;
            
        }

        $scope.initReward = function() {
            $scope.reward = {
                'rank': $scope.getNextRewardRank()
            }
        };

        $scope.addReward = function(reward, form) {
            if (typeof reward.title == "undefined" || reward.title.length < 1) {
                swal({
                    title: $filter('translate')('PRIZE_CHARACTERS_TITLE'),
                    text: $filter('translate')('MINIMUM_CHARACTERS_TEXT'),
                    type: 'warning',
                    confirmButtonColor: "#337ab7",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                });
                return false;
            }

            if (!$scope.contest.rewards) {
                $scope.contest.rewards = [reward];
            } else {
                if ($scope.contest.rewards.length <= 10) {
                    $scope.contest.rewards.push(reward);
                }
            }

            $scope.initReward();
            // form.$setPristine();

            setTimeout(function() {
                if ($scope.contest.rewards.length == 10) {
                    swal({
                        title: $filter('translate')('PRIZE_CHARACTERS_TITLE'),
                        text: $filter('translate')('MAXIMUM_CHARACTERS_TEXT'),
                        type: 'warning',
                        confirmButtonColor: "#337ab7",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    });
                }
            }, 500);
        };

        $scope.removeReward = function(rank) {
            $scope.contest.rewards = $scope.contest.rewards.filter(function(v) {
                return rank !== v.rank;
            });

            $scope.initReward();
        };

        $scope.editReward = function(rank, form) {
            angular.forEach($scope.contest.rewards, function(v) {
                if (v.rank == rank) {
                    $scope.reward = {
                        'rank': $scope.getNextRewardRank() - 1,
                        'title': v.title,
                        'description': v.description
                    };
                    $scope.contest.rewards = $scope.contest.rewards.filter(function(v) {
                        return rank !== v.rank;
                    });
                }
            });
        };

        $scope.setContestType = function(typeId) {
            if ($scope.contest['contest_type'] == typeId) {
                $scope.contest['contest_type'] = null;
            } else {
                $scope.contest['contest_type'] = typeId;
            }
        };

        $scope.toggleObjective = function(id) {
            var objectives = $scope.contest.contest_objectives;
            var id = parseInt(id); // Ensure all ids are integers

            if (objectives.indexOf(id) === -1) {
                // Remove the other sales_child, if present
                if ($scope.utils.getObjectives(2)[id]) {
                    removeSalesChildren();
                }
                objectives.push(id);
                return;
            }

            objectives.splice(objectives.indexOf(id), 1);
            // Also remove sales children
            if ($scope.utils.getObjectives()[id] && $scope.utils.getObjectives()[id].sales_father) {
                removeSalesChildren();
            }
        };

        var removeSalesChildren = function() {
            var objectives = $scope.contest.contest_objectives;

            $.each($scope.utils.getObjectives(2), function(k, v) {
                k = Number(k);
                if ($scope.isObjectiveEnabled(k))
                    objectives.splice(objectives.indexOf(k), 1);
            });
        };

        $scope.isObjectiveEnabled = function(id) {
            var id = parseInt(id);
            return $scope.contest.contest_objectives !== undefined && $scope.contest.contest_objectives.indexOf(id) > -1;
        };
		
		$scope.changeTemplateId = function() {
			if ($scope.contest.use_template == 2)
			{
				$scope.contest.template_id = null;
			}
		}

        $scope.setTemplate = function(id) {
            $scope.contest.contest_template_id = id;
            $scope.templates.map(function(v, k) {
                if (v.template_id == id) {
                    $scope.templates[k].active = true;
                    $scope.currentTemplate = $scope.templates[k];
                } else {
                    $scope.templates[k].active = false
                }
            });
        };

        $scope.setTemplateClick = function(id) {
            $scope.contest.contest_template_id = id;
            $scope.templates.map(function(v, k) {
                if (v.template_id == id) {
                    $scope.templates[k].active = true;
                    $scope.currentTemplate = $scope.templates[k];
                    var modalText = "";
                    var header_text = "";

                    swal({
                        title: $filter('translate')('LAYOUT_SETTINGS_TITLE'),
                        text: backend.getSetting("ALERT_CHOOSE_TEMPLATE_" + $scope.locale.toUpperCase()) + " " + $scope.currentTemplate.template_name,
                        html: true,
                        type: "success"
                    });
                } else {
                    $scope.templates[k].active = false
                }
            });
        };

		/*
        $scope.rewardTitleValid = function() {
            return angular.element('#reward_title').val().length >= 3;
        };
		*/

        $scope.setWebSiteLanguage = function() {
            if ($scope.contest.contest_location == '')
                return;

            var lang = $scope.contest.contest_location == 1 ? 'en' : 'it';

            $.ajax({
                url: '/lang/' + lang,
                async: false,
                dataType: 'html',
                success: function(data) {

                }
            });

            window.open('/note-legali', '_blank');
        };

        $scope.checkLang = function() {

			if ($scope.contest.contest_location == '1') {
				//$scope.contest.contest_how_to_win = backend.getSetting('WIN_MODALITY_INSTRUCTIONS_BUZZ_EN');
			} else {				
				//$scope.contest.contest_how_to_win = backend.getSetting('WIN_MODALITY_INSTRUCTIONS_BUZZ_IT');
				
				swal({
                    html: true,
                    title: $filter('translate')('LANG_MODAL_TITLE'),
                    text: $filter('translate')('LANG_MODAL_BODY'),
                    customClass: 'contest-rules'
                });
			}
			
        };

        $scope.checkCompany = function() {
            var valid = true;

            // loadCompanyInfo();

            angular.forEach($scope.validCompany, function(v, k) {
                if (v == null || v == "") {
                    valid = false;
                }
            });
			
            return valid;
        };
		
		$scope.payment_step_num = function() {
			var step_num = 0;
			
			if ( $scope.contest.customer_connected_to_facebook == 1 && !$scope.checkCompany() ) 
				step_num = 15;
			else if (( $scope.contest.customer_connected_to_facebook == 1 && $scope.checkCompany() ) || ( $scope.contest.customer_connected_to_facebook != 1 && !$scope.checkCompany() ))
				step_num = 14;
			else
				step_num = 13;
			
			return step_num;
		}
		
		$scope.checkTemporaryCompany = function() {
            var valid = true;

            // loadCompanyInfo();

            angular.forEach($scope.company, function(v, k) {
                if ((v == null || v == "") && (k != 'user_district')) {
                    valid = false;
                }
            });

            return valid;
        };

		$scope.checkTerms = function() {
			if ($scope.check_terms == 0)
				$scope.check_terms = 1;
			else
				$scope.check_terms = 0;			
		}
		
		
		
        $scope.checkPayment = function() {
            return $scope.card && $scope.card.id > 0 && ($scope.check_terms == 1) && $scope.checkTemporaryCompany();
        }

        $scope.checkValidImg = function(action) {
            if (angular.element('.type-err-msg').text().length || angular.element('.size-err-msg').text().length || angular.element('.wh-err-msg').text().length || angular.element('.bg-wh-err-msg').text().length) {
                return false;
            } else {
                return true;
            }
        };

        $scope.checkTemplateFormErrors = function(){
			
			if ($scope.contest.use_template == 2) {
				
				if (angular.isUndefined($scope.contest.contest_watermark) || angular.isUndefined($scope.contest.contest_image_minisite_bg)
					|| ($scope.contest.contest_watermark == null) || ($scope.contest.contest_image_minisite_bg == null)
					|| (Object.keys($scope.contestForm.templateForm.$error).length > 0)) {
						return true;
					} else
						return false;
				
				
				
			} else
				
				return Object.keys($scope.contestForm.templateForm.$error).length > 0;//se è true il form non è valido
        } 

        $scope.getCurrentTemplateCall = function() {
			if ($scope.contest.use_template == 1)
			{
				angular.forEach($scope.templates, function(v, k) {
					if (v.template_id == $scope.contest.contest_template_id) {
						$scope.getCurrentTemplate = v;
					}
				});
			}
			else
			{
				$scope.getCurrentTemplate = $filter('translate')('CUSTOM_TEMPLATE');
			}
            
        };

        $scope.setPostVisibility = function(action) {
            $scope.contest.post_visibility = action;
        };

        $scope.setApproveContest = function(action) {
            $scope.contest.contest_approval = action;
        };

        $scope.getApproveContest = function() {
            if ($scope.contest.contest_approval) {
                return 1;
            } else {
                return 0;
            }
        };

        $scope.skipCoupon = function() {
            $scope.contest.contest_coupon_code = '';
        }

        $scope.setnoChoisenTmp = function(action) {
            $scope.noChoisenTmp = action;
        };

        $scope.checkBudget = function() {
            return ($scope.contest.contest_promotion != null ? 'NEXT' : 'SKIP');
        };

        $scope.checkCoupon = function() {
            return ($scope.contest.contest_coupon_code && $scope.contest.contest_coupon_code.length == 5 && $scope.coupon && $scope.coupon.is_valid ? 'NEXT' : 'SKIP');
        };

        $scope.checkRewards = function() {
            return ($scope.contest.rewards.length > 0 ? 'NEXT' : 'BTN_NO_PRIZES');
        };

        $scope.checkLangReward = function() {
            if ($scope.contest.contest_location == '2') {
                swal({
                    html: true,
                    title: $filter('translate')('LANG_MODAL_TITLE'),
                    text: $filter('translate')('LANG_MODAL_BODY'),
                    customClass: 'contest-rules'
                });

            }
        };

        $scope.getApproved = function() {
            var status = $scope.approvalStatusDict[$scope.getApproveContest()];
            return status.replace("STATUS_", "");
        };

        $scope.summaryModal = function(action, error) {
            if (error) {
                $scope.not_valid_dates = true;
            }
            angular.element('#go-to-summary').modal(action);
        };

        $scope.langModal = function(action) {
            angular.element('#it-rules').modal(action);
        };

        $scope.langModalReward = function(action) {
            angular.element('#it-rules-reward').modal(action);
        };

        $scope.socialConnect = function(social) {
			
            if ($scope.contest['customer_connected_to_' + social] == 1) {
                $scope.socialDisconnect(social);
            } else {
                window.open('/admin/' + social + '-customer-login/' + $scope.contest.contest_id, "Connect", "width=900,height=600,scrollbars=yes");
            }
        };

        $scope.socialDisconnect = function(social) {
            backend.get('/admin/' + social + '-customer-disconnect/' + $scope.contest.contest_id)
                .success(function(data) {
                    angular.element('#' + social + '_action').text('connect');
                    $scope.contest['customer_connected_to_' + social] = 0;
                    $scope.contest['customer_connected_to_' + social + '_page'] = '';
                    $scope.contest['customer_connected_to_' + social + '_image'] = '';
                    angular.element('btn-connect-' + social).removeClass('btn-connect-active');
                })
                .error(function(err) {
                    $scope.showAlert = true;
                    $scope.error = err.message;
                });


        };

        $scope.socialConnectResult = function(social, success, data, image) {
            image = image || 'default.png';
            if (success == true) {
                $scope.contest['customer_connected_to_' + social] = 1;
                $scope.contest['customer_connected_to_' + social + '_page'] = data;
                $scope.contest['customer_connected_to_' + social + '_image'] = image;
                angular.element('#' + social + '_action').text('disconnect');
                $scope.$apply();
            } else {
                $scope.contest['customer_connected_to_' + social] = 0;
                $scope.contest['customer_connected_to_' + social + '_page'] = '';
                $scope.contest['customer_connected_to_' + social + '_image'] = '';
                $scope.$apply();
                $scope.setError(data);
            }
        };

        $scope.validateSocials = function() {
            // At least one must be connected
            var contest = $scope.contest;
			
            return ((contest.customer_connected_to_facebook == 1) || (contest.customer_connected_to_twitter == 1) || (contest.customer_connected_to_youtube == 1));
            //return !((contest.customer_connected_to_facebook == 0) || (contest.customer_connected_to_twitter == 0) || (contest.customer_connected_to_youtube == 0));
        };

        $scope.checkIsValidCoupon = function() {
            if ($scope.contest.contest_coupon_code.length == 5) {
                $scope.coupon = {
                    'is_valid': true
                };
                
				if ($scope.contest.contest_service_type == 1) {
				
					if ($scope.contest.contest_currency == 'USD')
						$scope.contest.contest_amount = ($scope.plans[0].fixed_fee/100)*$scope.currencyRate;
					else
						$scope.contest.contest_amount = ($scope.plans[0].fixed_fee/100);
					
				};
				if ($scope.contest.contest_service_type == 2) {
					
					if ($scope.contest.contest_currency == 'USD')
						$scope.contest.contest_amount = ($scope.plans[1].fixed_fee/100)*$scope.currencyRate;
					else
						$scope.contest.contest_amount = ($scope.plans[1].fixed_fee/100);
				};

                backend.post('/admin/check-coupon', {
                        'code': $scope.contest.contest_coupon_code,
                        'amount': $scope.contest.contest_amount*100
                    })
                    .success(function(data) {
                        if (!data.invalid_coupon) {
                            $scope.coupon = {
                                'code': $scope.contest.contest_coupon_code,
                                'is_valid': true,
                                'percent_off': data.percent_off,
                                'amount_off': data.amount_off,
                                'discount': data.discount,
                                'final_prize': data.final_prize
                            };

                            // Get card
                            backend.get('/dashboard-api/payments/card')
                                .success(function(data) {
                                    $scope.card = data;
                                });

                        } else {
                            $scope.coupon = {
                                'is_valid': false
                            };
                        };
                    })
                    .error(function(err) {
                        $scope.setError(err.error.message);
                    });
            } else {
                $scope.coupon = {
                    'is_valid': false
                };
            }
        };

        $scope.usingValidCoupon = function() {
            var free = ($scope.coupon && $scope.coupon.percent_off) ? parseInt($scope.coupon.percent_off) == 100 : false;
            return ($scope.coupon && $scope.coupon.is_valid && free && ($scope.layout == 'create' || $scope.contest.contest_status == 4));
        }

        $scope.getFonts = function() {
            return backend.get('/admin/get-fonts')
                .then(function(response) {
                    $scope.fonts = response.data;
                })
                .catch(function(response) {
                    $scope.setError(response.data.error.message);
                });
        };


        $scope.getTwitterLocations = function() {

            backend.get('/admin/get-twitter-locations-trends-available')
                .then(function(response) {
                    $scope.twitterLocations = response.data;
                })
                .catch(function(response) {
                    $scope.setError(response.data.error.message);
                });


            if ($scope.city) {
                return backend.get('/admin/get-twitter-locations-trends-available/' + $scope.city)
                    .then(function(response) {
                        //$scope.twitterLocations = response.data;

                        $scope.contest.contest_twitter_location = response.data[0];

                        if ($scope.contest.contest_twitter_location) {
                            $scope.loadTrends();
                        } else {
                            return backend.get('/admin/get-twitter-locations-trends-available/' + $scope.country)
                                .then(function(response) {
                                    //$scope.twitterLocations = response.data;

                                    $scope.contest.contest_twitter_location = response.data[0];
                                    $scope.loadTrends();

                                });
                        }


                    });
            }
            

        };

        $scope.startDateErrorStatus = function() {
            return $scope.startDateError;
        };

        $scope.equalDateErrorStatus = function() {
            return $scope.equalDateError;
        };

        $scope.updateStartTime = function() {
            if ($scope.contest.contest_start_date && $scope.contest.contest_start_time) {
                $scope.contest.contest_start_date.setDate($scope.contest.contest_start_date.getDate());
                $scope.contest.contest_start_date.setHours($scope.contest.contest_start_time.getHours());
                $scope.contest.contest_start_date.setMinutes($scope.contest.contest_start_time.getMinutes());
                $scope.contest.contest_start_date.setSeconds(0);
            }

            delete $scope.contest.contest_start_datetime;
            if ($scope.contest.contest_start_date) {
                $scope.contest.contest_start_datetime = angular.copy($scope.contest.contest_start_date);
            }

            if ($scope.contest.contest_start_datetime > $scope.contest.contest_end_datetime) {
                $scope.startDateError = true;
            } else {
                $scope.startDateError = false;
            }

            if ((!angular.isUndefined($scope.contest.contest_start_datetime)) && (!angular.isUndefined($scope.contest.contest_end_datetime))) {

                if ($scope.contest.contest_start_datetime.getTime() === $scope.contest.contest_end_datetime.getTime()) {
                    $scope.equalDateError = true;
                } else {
                    $scope.equalDateError = false;
                }

            }
            console.log('Start DateTime: ' + $scope.contest.contest_start_datetime);
        };

        $scope.updateEndTime = function() {
            if ($scope.contest.contest_end_date && $scope.contest.contest_end_time) {
                $scope.contest.contest_end_date.setDate($scope.contest.contest_end_date.getDate());
                $scope.contest.contest_end_date.setHours($scope.contest.contest_end_time.getHours());
                $scope.contest.contest_end_date.setMinutes($scope.contest.contest_end_time.getMinutes());
                $scope.contest.contest_end_date.setSeconds(0);
            }

            delete $scope.contest.contest_end_datetime;
            if ($scope.contest.contest_end_date) {
                $scope.contest.contest_end_datetime = angular.copy($scope.contest.contest_end_date);
            }

            if ($scope.contest.contest_end_datetime < $scope.contest.contest_start_datetime) {
                $scope.startDateError = true;
            } else {
                $scope.startDateError = false;
            }

            if ((!angular.isUndefined($scope.contest.contest_start_datetime)) && (!angular.isUndefined($scope.contest.contest_end_datetime))) {

                if ($scope.contest.contest_start_datetime.getTime() === $scope.contest.contest_end_datetime.getTime()) {
                    $scope.equalDateError = true;
                } else {
                    $scope.equalDateError = false;
                }

            }



            console.log('End DateTime: ' + $scope.contest.contest_end_datetime);
        };

        $scope.isOkToSave = function() {
			
			
            var contestForm = $scope.contestForm;            
			
			var validflow = contestForm.trendsForm.$valid 
			&& contestForm.briefForm.$valid 
			&& contestForm.rulesForm.$valid && $scope.contest.contest_win_modality != '' && $scope.contest.contest_how_to_win != ''
			&& contestForm.tcForm.$valid
			&& contestForm.promotionForm.$valid
			&& ($scope.contest.contest_location !== undefined)
			&& ($scope.contest.contest_approval != 0)
			&& ($scope.contest.contest_service_type != 0)
			&& ($scope.validateSocials())
			&& ($scope.validate('objectives'))
			&& (!$scope.checkTemplateFormErrors())
			&& (!$scope.startDateErrorStatus())
			&& (!$scope.equalDateErrorStatus());
			
			if ($scope.contest.use_standard_tc == 0)  {
				validflow = validFlow && ($scope.contest.valid_tc == true)
			}
			/*
			if ($scope.contest.contest_no_rewards != "1") {
				validflow = validflow && contestForm.rulesForm$valid;
			}
			*/
			if ($scope.contest.customer_connected_to_facebook == 1) {
				validflow = validflow && ($scope.contest.post_visibility.length != 0);
			}
			
			return validflow;
            
            
        };

        $scope.showAlertMsg = function() {
            if ($scope.showAlert) {
                return true;
            } else {
                return false;
            }
        };

        $scope.canBeEdited = function() {
            if (!$scope.contest)
                return false;

            var status = $scope.contest.contest_status;
            return status == 4 || status == 3;
        };

        $scope.checkAvRouteAndSave = function(valid) {
            if (!valid) {
                swal({
                    title: $filter('translate')('CONTEST_BRIEF_TITLE'),
                    text: $filter('translate')('CONTEST_BRIEF_URL_USED'),
                    type: 'warning',
                    html: true,
                    confirmButtonColor: "#337ab7",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                });
                angular.element('#contest_name').focus();
            } else {
                if ($scope.contest.contest_status == 3) {
                    $scope.save(true, true);
                    //closeBlock('brief');
                } else if ($scope.contest.contest_status == 4) {
                    $scope.save(false, true);
                }
            }
        };
		
		$scope.createStringByArray = function(array, separator) {
			var output = '';
			angular.forEach(array, function (object) {
				angular.forEach(object, function (value, key) {
					//output += key + ',';
					output += value + separator;
				});
			});
			output = output.substring(0, output.length - 1);
			return output;
		}

        $scope.save = function(customer_confirm, popupSave, checktmp, saveCompanyinfo) {
            if (!$scope.canBeEdited) {
                $scope.setError('ERROR_SAVE_FORBIDDEN');
                return;
            }

            if (typeof $scope.contest.contest_type == "undefined" || $scope.contest.contest_type == "" || typeof $scope.contest.contest_location == "undefined" || $scope.contest.contest_location == "") {
                $scope.gotoBlock('contest_type');
                $scope.toggleBlock('contest_type', 'add');
                return;
            }
			
			

            $scope.getCurrentTemplateCall();

            var customer_confirm = customer_confirm || false;
            var popupSave = popupSave || false;
            var checktmp = checktmp || false;
			var saveCompanyinfo = saveCompanyinfo || false;
			
			if (saveCompanyinfo) {
				saveCompanyInfo();
			}

            if (checktmp) {
                if ($scope.contest.use_template == 1 && (typeof $scope.contest.contest_template_id == "undefined" || $scope.contest.contest_template_id == "")) {
                    $scope.setnoChoisenTmp(true);
                    return false;
                }
            }

            if ($scope.contest.use_template == 1 && (typeof $scope.contest.contest_template_id == "undefined" || $scope.contest.contest_template_id == "")) {
                $scope.contest.contest_template_id = null;
            }

            if (typeof $scope.contest.contest_term_cond_company_sponsoring == "undefined" || $scope.contest.contest_term_cond_company_sponsoring == "") {
                $scope.contest.contest_term_cond_company_sponsoring = null;
            }

            // if (typeof $scope.contest.contest_template_id == "undefined" || $scope.contest.contest_template_id == "") {
            // $scope.contest.contest_template_id = 1;
            // }

            //if (typeof $scope.contest.contest_name != "undefined" && $scope.contest.contest_name != "") {
            //  $scope.contest.contest_route = $scope.contest.contest_name.replace(/\W+/g, "_");
            //}
            // if (typeof $scope.contest.contest_promotion == "undefined" || $scope.contest.contest_promotion == "") {
            //  $scope.contest.contest_promotion = 0;
            // }
			if (($scope.contest.valid_tc == false) && ($scope.contest.use_standard_tc == '0')) {
				$scope.contest.contest_term_conditions = '';
			}
						
            var data = angular.copy($scope.contest);

            // Enabled loader
            $scope.contest.loading = true;
			
			data.contest_hashtag = $scope.createStringByArray($scope.contest.contest_hashtag, ';');

            // Send objects as JSON
            data.contest_objectives = JSON.stringify(data.contest_objectives);
            data.contest_rewards = JSON.stringify(data.rewards);
     		
			//il confirmed_by_customer 	
			if (customer_confirm) 
				data.confirmed_by_customer = 1;	
			else
				data.confirmed_by_customer = 0;	
				
			
            var fd = new FormData();

            angular.forEach(data, function(v, k) {
                // Format dates as iso strings
                if (k.indexOf('time') > -1 || k.indexOf('date') > -1) {
                    //è necessario convertire la data da local time in UTC
                    v = moment.utc(v).format();
                    //v = moment(v).format();

                }


                fd.append(k, v);
            });

            if ((data.use_template == 2) && (data.contest_watermark))
				fd.append('contest_watermark', document.getElementsByName('contest_watermark')[0].files[0]);

            if ((data.use_template == 2) && (data.contest_image_minisite_bg))
				fd.append('contest_image_minisite_bg', document.getElementsByName('contest_image_minisite_bg')[0].files[0]);
			/*
            if (data.contest_image_minisite_sq)
				fd.append('contest_image_minisite_sq', document.getElementsByName('contest_image_minisite_sq')[0].files[0]);
			*/

            if ((data.contest_term_conditions) && (data.valid_tc == true))
                fd.append('contest_term_conditions', document.getElementsByName('contest_term_conditions')[0].files[0]);
			
						

            $.ajax({
                type: 'POST',
                url: '/admin/edit-contest',
                data: fd,
                async: false,
                processData: false,
                contentType: false, // tell jQuery not to set contentType
                success: function(data, status) {
                    // Disable loader
                    $scope.contest.loading = false;

                    var response = $.parseJSON(data);
                    if (response.id) {
                        // First save                        
                        $scope.contest.contest_id = parseInt(response.id);
                        

                        $scope.globalError = null;
      
                         if (popupSave) {
                             swal({
                                 title: $filter('translate')('CONTEST_SAVE_CHANGES_TITLE'),
                                 text: $filter('translate')('CONTEST_SAVE_CHANGES_TEXT_DRAFT'),
                                 type: 'success',
                                 html: true,
                                 confirmButtonColor: "#337ab7",
                                 confirmButtonText: "OK",
                                 closeOnConfirm: true
                             });
                        }
						
						/*
                        if ((customer_confirm) && (!(popupSave))) {
                            $scope.contest.submitted = true;
                            angular.element('#go-to-summary').modal('hide');
                            // setTimeout(function(event) {
                            //       $scope.locationChangeStarted = true;
                            //       $location.path($location.url('contests').hash());
                            // }, 500);
                        }*/
                    }

                    $scope.lastSaved = angular.copy($scope.contest);

                    // if (($scope.contest.contest_status == 3)) {
                    //     swal({
                    //         title: $filter('translate')('CONTEST_SAVE_CHANGES_TITLE'),
                    //         text: $filter('translate')('CONTEST_SAVE_CHANGES_TEXT_SCHEDULED'),
                    //         type: 'success',
                    //         html: true,
                    //         confirmButtonColor: "#337ab7",
                    //         confirmButtonText: "OK",
                    //         closeOnConfirm: true
                    //     });
                    // }

                    if (response.result == "0") {
                        $scope.setError(response.message);
                        return false;
                    }
                },
                error: function(err) {
                    $scope.contest.loading = false;
                    $scope.setError(err.message);
                }
            });

        };

        var loadCompanyInfo = function() {
            backend.get('/admin/get-company/' + $scope.companyId)
                .success(function(data) {
                    $scope.company = data;
                    $scope.validCompany = angular.copy($scope.company);
                })
                .error(function(err) {
                    $scope.setError(err.error.message);
                });
        };

        loadCompanyInfo();

        var saveCompanyInfo = function() {
            backend.post('/admin/edit-company', $scope.company)
                .success(function(data) {
                    // Reload user data
                    angular.element($('#auth-block')).scope().loadUser();
                })
                .error(function(err) {
                    $scope.setError(err.error.message);
                });
        };

        // var postConfirm = function() {
        //   backend.post('/admin/approve-contest', {
        //     'contest_id': $scope.contest.contest_id,
        //     'approval_step': 0
        //   })
        //   .success(function(data) {
        //     $scope.contest.loading = false;
        //     if(data.result == 1) {
        //       $scope.contest.submitted = true;
        //       return;
        //     }

        //     $scope.setError(data.message);
        //   })
        //   .catch(function(data) {
        //     $scope.setError(data.error.message);
        //   });
        // }


        var loadTemplates = function(template_id) {
            var id = template_id || null;

            backend.get('/admin/get-template-list')
                .success(function(data) {
                    $scope.templates = data;
					/*
                    if (id != null) {
                        //$scope.contest.use_template = 1;
                        $scope.setTemplate(id);
                    } else {
                        // $scope.contest.use_template = 2;
                        // $scope.contest.use_template = 1;
                    }
					*/
                });
        };


        $scope.changesFound = function(scopes) {
            //console.log($scope.contest);

            var scopes = scopes || false;
            var found = false;
            var skip = false;
            angular.forEach($scope.lastSaved, function(vLast, kLast) {
                if (!found) {
                    if (scopes) {
                        angular.forEach(scopes, function(vScope, kScope) {
                            if (kLast == vScope) {
                                if (kLast != "rewards" && typeof vLast == "object") {
                                    vLast = JSON.stringify(vLast);
                                }
								
                                angular.forEach($scope.contest, function(vNew, kNew) {
																										
                                    if (kNew != "rewards" && typeof vNew == "object") {
                                        vNew = JSON.stringify(vNew);
                                    }
									
									if (kLast == kNew && kLast == "contest_hashtag" && !angular.isUndefined(vNew)) {
										//devo trasformare il json di vNew in array
										var vNewArray = [];
										vNewArray = JSON.parse(vNew);
										
										var value_array = [];
										
										for(var i=0; i<vNewArray.length; i++) {
											
											var element = String(vNewArray[i].text);
											
											if (vLast.indexOf(element) == -1) {
												//non trovato
												found = true;
											}
											
										}
										
										var vLastArray = []; 
										vLastArray = JSON.parse(vLast);
										
										if (vLastArray.length != vNewArray.length)
											found = true;
										
									} 
									
                                    if ((kNew != "rewards") && (!(kLast == kNew && kLast == "contest_hashtag"))) {
                                        if (kLast == kNew && vLast != vNew) {
                                            found = true;

                                        }
                                    } else if (!(kLast == kNew && kLast == "contest_hashtag")) {
                                        if ($scope.contest.rewards.length != $scope.lastSaved.rewards.length || vNew.id != vLast.id || vNew.rank != vLast.rank || vNew.title != vLast.title || vNew.description != vLast.description) {
                                            found = true;

                                        }
                                    }
									/*
									if ((kNew != "rewards") && (kNew != "contest_twitter_location") && (kNew != "templateslist") && (!(kLast == kNew && kLast == "contest_hashtag"))) {
										if (kLast == kNew && vLast != vNew) {
											console.log(kLast);
											console.log(vLast);
											console.log(vNew);
											found = true;

										}
									} else if (kNew == "rewards") {
										if ($scope.contest.rewards.length != $scope.lastSaved.rewards.length || vNew.id != vLast.id || vNew.rank != vLast.rank || vNew.title != vLast.title || vNew.description != vLast.description) {
											found = true;

										}
									}
									else if ((kNew != "contest_twitter_location") && (kNew != "templateslist")) {
										var new_twitter_location_obj = $.parseJSON(vNew);
										var last_twitter_location_obj = $.parseJSON(vLast);
										if (new_twitter_location_obj.name != last_twitter_location_obj.name) {
											found = true;
										}
									}
									*/
                                });
                            }
                        });
                    } else {
                        if (kLast != "rewards" && typeof vLast == "object") {
                            vLast = JSON.stringify(vLast);
                        }

                        angular.forEach($scope.contest, function(vNew, kNew) {
                            if (kNew != "rewards" && typeof vNew == "object") {
                                vNew = JSON.stringify(vNew);
                            }
							
							if (kLast == kNew && kLast == "contest_hashtag" && !angular.isUndefined(vNew)) {
								//devo trasformare il json di vNew in array
								var vNewArray = []; 
								vNewArray = JSON.parse(vNew);
								
								var value_array = [];
								
								for(var i=0; i<vNewArray.length; i++) {
									
									var element = String(vNewArray[i].text);
									
									if (vLast.indexOf(element) == -1) {
										//non trovato
										found = true;
									}
									
								}
								
								var vLastArray = []; 
								vLastArray = JSON.parse(vLast);
								
								if (vLastArray.length != vNewArray.length)
									found = true;
								
							} 
							
							if ((kNew != "rewards") && (kNew != "contest_twitter_location") && (kNew != "templateslist") && (!(kLast == kNew && kLast == "contest_hashtag"))) {
                                if (kLast == kNew && vLast != vNew) {
                                    console.log(kLast);
                                    console.log(vLast);
                                    console.log(vNew);
                                    found = true;

                                }
                            } else if (kNew == "rewards") {
                                if ($scope.contest.rewards.length != $scope.lastSaved.rewards.length || vNew.id != vLast.id || vNew.rank != vLast.rank || vNew.title != vLast.title || vNew.description != vLast.description) {
                                    found = true;

                                }
                            }
							else if ((kNew != "contest_twitter_location") && (kNew != "templateslist")) {
                                var new_twitter_location_obj = $.parseJSON(vNew);
                                var last_twitter_location_obj = $.parseJSON(vLast);
                                if (new_twitter_location_obj.name != last_twitter_location_obj.name) {
                                    found = true;
                                }
                            }
                        });
                    }
                }
            });

            return found;
        };

        $scope.loadTrends = function(userCountry) {
            if (!$scope.contest.contest_twitter_location) {
                return;
            }

            backend.get('/admin/get-twitter-trends-available/' + $scope.contest.contest_twitter_location.woeid)
                .success(function(data) {
                    $scope.twitter_trends = data;

                    $scope.numberOfPages = function() {
                        return Math.ceil($scope.twitter_trends.length / $scope.pageSize);
                    }
                })
                .catch(function(data) {
                    $scope.setError(data.error.message);
                });
        };
		
		$scope.verifyCheckedTrend = function (hashtag) {
			var hashtagArray = $scope.contest.contest_hashtag;
			
			if ((hashtagArray.length > 0) && (!angular.isUndefined(hashtagArray))) {
				var obj = hashtagArray.filter(function ( obj ) {
					return obj.text === hashtag;
				})[0];
				if (obj)
					return true;
				else
					return false;
			}
			else
				return false;
			
		}
		
		$scope.tagRemoved = function (tag) {
			//devo trovare l'elemento da "twitter_trends"
			//se c'è devo prendere l'id dell'elemento, deselezionare la checkbox
			var obj = $scope.twitter_trends.filter(function ( obj ) {
				return obj === tag.text;
			})[0];
			if (document.getElementById("hash_" + obj) && (!angular.isUndefined(obj))) {
				if ((document.getElementById("hash_" + obj).checked) ) {
					document.getElementById("hash_" + obj).checked = false;
				}
			}
			
		};

        $scope.setHashtag = function(id, hashtag) {
            // Add # if there isn't - aggiunto da server
            //hashtag = hashtag.indexOf('#') == -1 ? '#' + hashtag : hashtag;
            //hashtag = hashtag.replace(' ', '');
			if (document.getElementById("hash_" + hashtag).checked) {
                if (angular.isUndefined($scope.contest.contest_hashtag) || $scope.contest.contest_hashtag.length == 0) {
					var hashtagObject = {};
					hashtagObject['text'] = hashtag;
					var hashtagArray = [];
					hashtagArray.push(hashtagObject);
                    $scope.contest.contest_hashtag = hashtagArray;
                } else {
					var hashtagObject = {};
					hashtagObject['text'] = hashtag;
					var hashtagArray = $scope.contest.contest_hashtag;
					hashtagArray.push(hashtagObject);
                    $scope.contest.contest_hashtag = hashtagArray;
                }
            } else {
				
				var hashtagArray = $scope.contest.contest_hashtag;
				
				if (hashtagArray.length > 0) {
					hashtagArray = hashtagArray.filter(function( obj ) {
						return obj.text !== hashtag;
					});
				}
				$scope.contest.contest_hashtag = hashtagArray;
				
                
            }
            
        };

        $scope.gotoContestListWithoutSave = function() {


            $scope.saveChangePopup = false;


            // Redirect to contest list
            // $location.url('/contests');
            $scope.$apply(function() {
                $scope.locationChangeStarted = true;
                $location.path($location.url('contests').hash());
            });


        };

        $scope.checkitout = function(env) {
            //$scope.isCheckoutDisabled = true;
			if (env == 'end') {
				$scope.loading['pa'] = true;
			} //end step
			else {
				$scope.loading['po'] = true;
			}

            $scope.save(true);

			var amount = 0;
            var coupon_code = null;
            var coupon_amount = 0;

            // Company info
			saveCompanyInfo();
			
			if ($scope.contest.contest_service_type == 1) {
				
				if ($scope.contest.contest_currency == 'USD')
					$scope.contest.contest_amount = ($scope.plans[0].fixed_fee/100)*$scope.currencyRate;
				else
					$scope.contest.contest_amount = ($scope.plans[0].fixed_fee/100);
				
			};
			if ($scope.contest.contest_service_type == 2) {
				
				if ($scope.contest.contest_currency == 'USD')
					$scope.contest.contest_amount = ($scope.plans[1].fixed_fee/100)*$scope.currencyRate;
				else
					$scope.contest.contest_amount = ($scope.plans[1].fixed_fee/100);
			};
			            
			
            if ($scope.coupon && $scope.coupon.is_valid) {
                coupon_code = $scope.coupon.code;
                coupon_amount = $scope.coupon.discount * 100;
                amount = $scope.coupon.final_prize * 100;
            }
			else {
				amount = $scope.contest.contest_amount * 100;
			}

                        
            backend.post('/dashboard-api/payments/make-checkout', {
                'contest_id': $scope.contest.contest_id,
				'contest_auto_start': $scope.contest.contest_auto_start,
                'service_type': $scope.contest.contest_service_type,
                'amount': amount,
                'description': $scope.contest.selectedServiceFee,
                'coupon': coupon_code,
				'currency': $scope.contest.contest_currency,
                'coupon_amount': coupon_amount,
                'customer_id': $scope.card.customer_id,
				'contest_amount': $scope.contest.contest_amount * 100
            }).success(function(data) {
                if (env == 'popup') {
					$scope.loading['po'] = false;
					$scope.summaryModal('hide'); 
				} //end step
				else {
					$scope.loading['pa'] = false;
				}

                //$form.find('.submitcoupon-button0').prop('disabled', true);
                //$form.find('.submitcoupon-button1').prop('disabled', true);

                if (angular.isUndefined(data.invalid_coupon)) {
					
					//qui il coupon è valido ma devo capire se è partenza lanciata all'istante o meno
					
					if ($scope.contest.contest_auto_start == 1) {
						//PARTENZA ALL'ISTANTE CON COUPON
						swal({
                            title: $filter('translate')('THANKS_CHECKOUT'),
                            text: $filter('translate')('BODY_THANKS_CHECKOUT_COUPON'),
                            type: 'success',
                            html: true,
                            confirmButtonColor: "#337ab7",
                            confirmButtonText: $filter('translate')('GO_TO_CONTESTS_LIST'),
                            closeOnConfirm: true
                        },
                        function(isConfirm) {
                            $scope.gotoContestListWithoutSave();
                        });
						
					} else {
						//PARTENZA POSTPOSTA CON COUPON
						swal({
                            title: $filter('translate')('THANKS_CHECKOUT'),
                            text: $filter('translate')('BODY_THANKS_CHECKOUT'),
                            type: 'success',
                            html: true,
                            confirmButtonColor: "#337ab7",
                            confirmButtonText: $filter('translate')('GO_TO_CONTESTS_LIST'),
                            closeOnConfirm: true
                        },
                        function(isConfirm) {
                            $scope.gotoContestListWithoutSave();
                        });
						
					}
					
                    

                } else if (data.invalid_coupon) {
                    //devo dire nel popup che il coupon non è valido (forse questa condizione nn viene mai toccata)
                    $scope.error = 'not valid coupon';
                    $scope.activate_checkoutbutton = 1;

                    swal({
                        title: $filter('translate')('ERROR_COUPON'),
                        text: $filter('translate')('BODY_ERROR_COUPON'),
                        type: 'error',
                        html: true,
                        confirmButtonColor: "#337ab7",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    });

                } 
				else 
				{                    

                    var description = $filter('translate')('BODY_THANKS_CHECKOUT');
                    if ($scope.percent_off) {
                        description += '<br>' + $filter('translate')('BODY_THANKS_COUPON') + ' ' + $scope.coupon.percent_off + ' %';
                    }
                    if ($scope.amount_off) {
                        description += '<br>' + $filter('translate')('BODY_THANKS_COUPON') + ' ' + $scope.coupon.amount_off;
                    }

                    swal({
                            title: $filter('translate')('THANKS_CHECKOUT'),
                            text: description,
                            type: 'success',
                            html: true,
                            confirmButtonColor: "#337ab7",
                            confirmButtonText: $filter('translate')('GO_TO_CONTESTS_LIST'),
                            closeOnConfirm: true
                        },
                        function(isConfirm) {
                            $scope.gotoContestListWithoutSave();
                        });
                }
            }).error(function(err) {
                $scope.showAlert = true;
				if (env == 'popup') {
					$scope.loading['po'] = false;
				} //end step
				else {
					$scope.loading['pa'] = false;
				}
                
                $scope.error = err.message;
            });

            //$scope.loading['cc'] = false;
        };



        //******************************************
        

        $scope.isEmpty = function(obj) {
            return angular.equals({}, obj);
        };

        $scope.resetPaymentForm = function(service_type) {
            if (service_type == 1) {
                $('.submitcoupon-button0').attr("disabled", false);
                $("#buzz-form :input[name='coupon']").val('');
                $("#buzz-form :input[name='coupon']").attr("disabled", false);
            } else if (service_type == 2) {
                $('.submitcoupon-button1').attr("disabled", false);
                $("#superbuzz-form :input[name='coupon']").val('');
                $("#superbuzz-form :input[name='coupon']").attr("disabled", false);
            }



        };



        $scope.setPackage = function(service_type) {

			            
            if (service_type == 1) {
                $scope.contest.selectedServiceFee = "BUZZ CONTEST";
                $scope.contest.contest_service_type = service_type;
				$scope.contest.use_template = 1;
				
				
				
            };
            if (service_type == 2) {
                $scope.contest.selectedServiceFee = "SUPERBUZZ CONTEST";
                $scope.contest.contest_service_type = service_type;
				
				
            };

            
        };

		$scope.isPreviousStep = function(step) {
			
			var condition = false;
			
			
			
			var toggleBlockId = jQuery('#block_' + step);
			
			$.each($scope.blocks, function(k, v) {
                
                if (k == step) {
					
					if ($scope.contest.contest_status == 4) {
						condition = (v == true) && (toggleBlockId.hasClass('completed'));
					} else {
						condition = (v == true) && (toggleBlockId.hasClass('completed')) && $scope.isOkToSave();
					}
					
					if (condition) {
						//devo chiudere lo step contenuto in $scope.currentBlock
						$scope.toggleBlock($scope.currentBlock);
						//apro lo step che ho cliccato
						$scope.toggleBlock(step);
						//setto currentBlock quello che ho appena aperto
						$scope.currentBlock = step;
						
						setTimeout(function() {
							//var toggleBlockId = jQuery('#block_' + b);
							jQuery('html, body').animate({
								scrollTop: toggleBlockId.offset().top
							}, 500);
							//toggleBlockId.addClass('active');
							toggleBlockId.find('.contest-block-content').slideDown('fast');
						}, 500);
						
						return true;
					}						
					else
						return;
				}
                    
            });
		}

        $scope.checkPreviousStep = function(step) {
            // console.log('checkPreviousStep: '+$scope.contest.contest_id);
            // console.log('checkPreviousStep: '+$scope.contest_id);

            //Noonic debug
            if ($scope.contest.contest_status != 4) {
                return true;
            };

            
            var canGoOn = false;

            var type            = $scope.contest.contest_type && $scope.contest.contest_location;
            var connect         = $scope.contest.customer_connected_to_facebook || $scope.contest.customer_connected_to_twitter || $scope.contest.customer_connected_to_youtube;
            var objectives      = $scope.contest.contest_objectives.length > 0;
            var packages        = $scope.contest.contest_service_type;
            var trends          = $scope.contest.contest_hashtag;
            var brief           = $scope.contest.contest_name && $scope.contest.contest_start_datetime && $scope.contest.contest_end_datetime && $scope.contest.contest_start_time && $scope.contest.contest_end_time && $scope.contest.contest_concept;
            var approveSettings = $scope.contest.contest_approval != 0;
            var postSettings    = $scope.contest.customer_connected_to_facebook == 1 ? $scope.contest.post_visibility : true;
            var rewards         = $scope.contest.contest_win_modality && $scope.contest.contest_how_to_win.length >= 5;
            var tc              = $scope.contest.contest_term_cond_company_sponsoring || $scope.contest.contest_term_conditions.name || $scope.contest.contest_term_conditions;
            var template        = (($scope.contest.use_template == 1 && $scope.contest.contest_template_id) || ($scope.contest.use_template == 2)) && $scope.contest.contest_route;
            var promotion       = $scope.contest.contest_promotion != "" || $scope.contest.contest_promotion != null;
            var coupon          = $scope.contest.contest_coupon != null;
            // no promotion, it's not "required"

            switch (step) {
                case 'connect':
                    if (type) {
                        canGoOn = true;
                    };
                    break;
                case 'packages':
                    if (type && connect) {
                        canGoOn = true;
                    };
                    break;
                case 'objectives':
                    if (type && connect && packages) {
                        canGoOn = true;
                    };
                    break;
                case 'trends':
                    if (type && connect && packages && objectives) {
                        canGoOn = true;
                    };
                    break;
                case 'brief':
                    if (type && connect && packages && objectives && trends) {
                        canGoOn = true;
                    };
                    break;
                case 'approveSettings':
                    if (type && connect && packages && objectives && trends && brief) {
                        canGoOn = true;
                    };
                    break;
                case 'postSettings':
                    if (type && connect && packages && objectives && trends && brief && approveSettings) {
                        canGoOn = true;
                    };
                    break;
                case 'rewards':
                    if (type && connect && packages && objectives && trends && brief && approveSettings && postSettings) {
                        canGoOn = true;
                    };
                    break;
                case 'tc':
                    if (type && connect && packages && objectives && trends && brief && approveSettings && postSettings && rewards) {
                        canGoOn = true;
                    };
                    break;
                case 'template':
                    if (type && connect && packages && objectives && trends && brief && approveSettings && postSettings && rewards && tc) {
                        canGoOn = true;
                    };
                    break;
                case 'promotion':
                    if (type && connect && packages && objectives && trends && brief && approveSettings && postSettings && rewards && tc && template) {
                        canGoOn = true;
                    };
                    break;
                case 'coupon':
                    if (type && connect && packages && objectives && trends && brief && approveSettings && postSettings && rewards && tc && template && promotion) {
                        canGoOn = true;
                    };
                    break;
                case 'payments':
                    if (type && connect && packages && objectives && trends && brief && approveSettings && postSettings && rewards && tc && template && promotion && coupon) {
                        canGoOn = true;
                    };
                    break;
                default:

            }

            return canGoOn;
        };

		var geolocation = function() {
			
			if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function showPosition(position) {

                    $scope.lat = position.coords.latitude;
                    $scope.lng = position.coords.longitude;

                    jQuery.getJSON('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + $scope.lat + ',' + $scope.lng + '&language=en', function(data) {
                        $scope.GeoIp = data;

                        if ($scope.GeoIp.status == 'OK') {
                            for (var i = 0; i < $scope.GeoIp.results[0].address_components.length; i++) {
                                for (var b = 0; b < $scope.GeoIp.results[0].address_components[i].types.length; b++) {

                                    if ($scope.GeoIp.results[0].address_components[i].types[b] == "country") {
                                        //this is the object you are looking for
                                        $scope.country = $scope.GeoIp.results[0].address_components[i].long_name;
                                    }
                                }

                                for (var b = 0; b < $scope.GeoIp.results[0].address_components[i].types.length; b++) {
                                    if ($scope.GeoIp.results[0].address_components[i].types[b] == "administrative_area_level_1") {
                                        //this is the object you are looking for
                                        $scope.city = $scope.GeoIp.results[0].address_components[i].long_name;
                                    }
                                }

                                if (($scope.country) && ($scope.city)) {									
                                    $scope.getTwitterLocations();
                                    break;
                                }
                            }
                            
                        }
						//Console.log("Nation: " + $scope.country);
						//Console.log("City: " + $scope.city);
                    });



                }, function showError(error) {
                    switch (error.code) {
                        case error.PERMISSION_DENIED:
                            $scope.GeoIp_error = "User denied the request for Geolocation."
                            break;
                        case error.POSITION_UNAVAILABLE:
                            $scope.GeoIp_error = "Location information is unavailable."
                            break;
                        case error.TIMEOUT:
                            $scope.GeoIp_error = "The request to get user location timed out."
                            break;
                        case error.UNKNOWN_ERROR:
                            $scope.GeoIp_error = "An unknown error occurred."
                            break;
                    }
                })
            } else {
                $scope.GeoIp_error = "Geolocation is not supported by this browser.";
            }
			
		}

        var init = function() {
            // If the email is not confirmed, loop to check for confirmation
            if (!$scope.isEmailConfirmed) {
                return;
            }

            var onRouteChangeOff = $scope.$on('$locationChangeStart', function routeChange(event, next, current) {

                if (!$scope.locationChangeStarted) {
                    $scope.locationChangeStarted = true;

                    if (typeof $scope.contest.contest_name == "undefined" || $scope.contest.contest_name == "") {
                        $scope.deletePermanently($scope.contest.contest_id);
                        return;
                    }

                    var modalOptions = {};
                    modalOptions = {
                        closeButtonText: $filter('translate')('LEAVE_CONTEST_BUTTON_CLOSE'),
                        actionButtonText: $filter('translate')('LEAVE_CONTEST_BUTTON_ACTION'),
                        headerText: $filter('translate')('LEAVE_CONTEST_TITLE'),
                        bodyText: $filter('translate')('LEAVE_CONTEST_TEXT')
                    };

                    if ($scope.changesFound() && (saveChangePopup)) {
                        modalService.showModal({}, modalOptions).then(function(result) {
                            if (typeof result != 'undefined') {
                                $scope.locationChangeStarted = false;

                                switch (result) {
                                    case "yes":
									
										if ($scope.contest.contest_status == 3 && !$scope.isOkToSave()) {
											swal({
													 title: $filter('translate')('CONTEST_CANNOT_SAVE'),
													 text: $filter('translate')('REVIEW_SETTINGS'),
													 type: 'error',
													 html: true,
													 confirmButtonColor: "#337ab7",
													 confirmButtonText: "OK",
													 closeOnConfirm: true
												 });
											return; 
										}
										else if ($scope.contest.contest_status == 3 && $scope.isOkToSave())
										{
											$scope.save(true, true);
										}
										else {
											$scope.save();
										}										
											
                                        break;
									case "no":
										if (($scope.layout = 'create') && (typeof $scope.contest.contest_name == "undefined" || $scope.contest.contest_name == "")) {
											$scope.deletePermanently($scope.contest.contest_id);
										}
										break;
                                    case "close":										
                                        return;
                                }

                                event.preventDefault();
                                onRouteChangeOff();
                                $location.path($location.url(next).hash());
                            }
                        });
                    } else {
                        event.preventDefault();
                        onRouteChangeOff();
                        $location.path($location.url(next).hash());
                    }

                    event.preventDefault();

                }


            });


            // Initialize the controller
            if ($routeParams['id'] === undefined) {
				
				backend.get('/admin/get-template-list')
                .success(function(data) {
                    $scope.templates = data;
					
					// We're creating a new contest
					$scope.layout = 'create';
					$scope.activateBlock('contest_type');
					
					// $scope.activateBlock('ALL');
					// $scope.serviceType = $routeParams['service_type'];

					// if(['instant', 'standard'].indexOf($scope.serviceType) == -1) {
					// $scope.setError('ERROR_INVALID_SERVICE_TYPE');
					// } else {    
					$scope.contest = angular.copy(contestDefaults);
					initStep2(); 
					
					if ((window.location.pathname == '/dashboard/index.html') && (window.location.hash == '#/launch')) {
						geolocation();
					}
					// console.log('chiamo il recaptcha da initStep1');
					// onloadCallbackRecaptcha();

					// }
					if ($scope.isEmailConfirmed)
						$scope.ready = true;
                });
				

            } else {
                $scope.layout = 'edit';				
				$scope.activateBlock('ALL');   
				//$scope.activateBlock('contest_type');
				
                var c_id = $routeParams['id'];
                // Load basic contest data
                backend.get('/admin/get-contest/' + c_id)
                    .success(function(data) {

                        if (data.result == 0) {
                            $scope.showAlert = true;
                            return;
                        }
						
												
						$scope.templates = data.templateslist;
						
						$scope.contest = angular.copy(contestDefaults);
                        initStep2();
						$scope.contest.use_template = data.use_template;
						$scope.setTemplate(data.contest_template_id);
						
                        $scope.contest.contest_service_type = data.contest_service_type;						

						$scope.setPackage(data.contest_service_type);
												
                        $scope.utils.checkAccess(data, $scope.currentTab);
                        //dal server abbiamo orario in UTC
                        //l'orario è presente in contest_start_date, contest_end_date, contest_score_start_date, contest_score_end_date
                        //dobbiamo convertire orario da UTC in local time
                        //questa operazione è fatta da "new Date(data in UTC)"
                        data.contest_start_date = new Date(data.contest_start_date);
                        data.contest_end_date = new Date(data.contest_end_date);
                        data.contest_score_start_date = new Date(data.contest_score_start_date);
                        data.contest_score_end_date = new Date(data.contest_score_end_date);

                        // Force auto start to 0 (false)
                        //data.contest_auto_start = 0;

                        data.contest_start_time = data.contest_start_date;
                        data.contest_end_time = data.contest_end_date;
						
						$scope.contest.contest_how_to_win = data.contest_how_to_win;
						
						$scope.contest.contest_hashtag = data.contest_hashtag;

                        // If child objectives are selected, enable the father
                        // angular.forEach(data.contest_objectives, function(v, k) {
                        //   var found = false;
                        //   if(!found && backend.getSetting('OBJECTIVES')[v].sales_child) {
                        //     data.contest_objectives.push(999);
                        //     found = true;
                        //   }
                        // });

                        // Cast int all array IDs
                        angular.forEach(data.contest_objectives, function(v, k) {
                            data.contest_objectives[k] = parseInt(v);
                        });
						
						if (data.contest_term_cond_company_sponsoring == 'null') {
							data.contest_term_cond_company_sponsoring = '';
						}
                        //console.log(data.contest_term_cond_company_sponsoring);
                        //console.log(typeof(data.contest_term_cond_company_sponsoring));

                        angular.extend($scope.contest, data);
						/*
						if ($scope.contest.contest_status == 3) {
							$scope.activateBlock('ALL');
						} else if ($scope.contest.contest_status == 4) {
							$scope.activateBlock('contest_type');
						}
						*/
						
                        $scope.initReward();

                        if ($scope.contest.contest_term_conditions)
                            $scope.contest.use_standard_tc = 0;
                        else
                            $scope.contest.use_standard_tc = 1;

                        $scope.ready = true;
                        $scope.locationChangeStarted = false;
                        $scope.lastSaved = angular.copy($scope.contest);

						geolocation();
                        //////////////////////
                        // console.log('chiamo il recaptcha da initStep2');
                        //  onloadCallbackRecaptcha();
                        $scope.showAlert = false;
                    })
                    .error(function(err) {
                        $scope.showAlert = true;
                        initStep2();
                        $scope.error = err.message;
                    });
            }
        };

        var initStep2 = function() {
            //$scope.contest = angular.copy(contestDefaults);
            //$scope.contest.customer_connected_to_facebook == 0;
            //$scope.contest.customer_connected_to_twitter == 0;
            //$scope.contest.customer_connected_to_youtube == 0;



            // Set the subtitle
            // if($scope.serviceType == 'instant') {
            // $scope.subtitle = 'INSTANT CONTEST';
            // }

            // if($scope.serviceType == 'standard') {
            // $scope.subtitle = 'STANDARD CONTEST';
            // }

            // $scope.contest.contest_service_type = serviceTypes[$scope.serviceType];

            // Check credit availability
            //backend.get('/dashboard-api/credits/has-available-credit/' + $scope.contest.contest_service_type)
            //.success(function(data) {
            //  $scope.hasCredit = data;
            //})
            //.catch(function(data) {
            //  $scope.setError(data.error.message);
            //});

            $scope.initReward();

            //$scope.getTwitterLocations();
            $scope.getFonts();

            // Unset current template if use_template == 2
            $scope.$watch('contest.use_template', function(n, o) {
                if (n == 2) {
                    $scope.currentTemplate = null;
                    $scope.contest.contest_template_id = null;
                }
            });

            $scope.$watch('contest.use_standard_tc', function(n, o) {
                if (n == 1) {
                    // $scope.contest.terms_conditions = undefined;
                    // $scope.contest.contest_term_conditions = undefined;
                }
            });

            $scope.$watch('contest.contest_win_modality', function(n, o) {
                if ((n == 1) && (n != o))  {
                    if ($scope.contest.contest_location == '1')
                        $scope.contest.contest_how_to_win = backend.getSetting('WIN_MODALITY_INSTRUCTIONS_BUZZ_EN');
                    else
                        $scope.contest.contest_how_to_win = backend.getSetting('WIN_MODALITY_INSTRUCTIONS_BUZZ_IT');
                }
				else {
					if ((n == 2) || (n == ''))
						$scope.contest.contest_how_to_win = '';
				}
					
            });

            $scope.$watch('contest.contest_location', function(n, o) {
                if (($scope.contest.contest_win_modality == 1) && (n != o)) {
                    if ($scope.contest.contest_location == '1') {
                        $scope.contest.contest_how_to_win = backend.getSetting('WIN_MODALITY_INSTRUCTIONS_BUZZ_EN');
                    } else {
                        $scope.contest.contest_how_to_win = backend.getSetting('WIN_MODALITY_INSTRUCTIONS_BUZZ_IT');
                    }
                }
            });
			
			$scope.$watch('contest.contest_twitter_location.name', function(n, o) {
				if ($scope.contest.contest_twitter_location) {
					$scope.loadTrends();
				}
			})

            $scope.$watch('contest.contest_start_date', function(n, o) {
                if ($scope.contest.contest_start_date) {
                    $scope.updateStartTime();
                    if ($scope.contest.contest_end_date == "" || typeof $scope.contest.contest_end_date == "undefined") {
                        $scope.contest.contest_end_date = new Date($scope.contest.contest_start_date);
                        $scope.contest.contest_end_date.setDate($scope.contest.contest_start_date.getDate() + 2);
                    } else {
                        var one_day = 1000 * 60 * 60 * 24;
                        var difference_ms = Math.abs($scope.contest.contest_end_date.getTime() - $scope.contest.contest_start_date.getTime());

                        if (Math.round(difference_ms / one_day) > 31) {
                            swal({
                                title: $filter('translate')('CONTEST_BRIEF_TITLE'),
                                text: $filter('translate')('CONTEST_BRIEF_MAX_DAYS'),
                                type: 'warning',
                                confirmButtonColor: "#337ab7",
                                confirmButtonText: "OK",
                                closeOnConfirm: true
                            });
                            $scope.contest.contest_end_date = new Date($scope.contest.contest_start_date);
                            $scope.contest.contest_end_date.setDate($scope.contest.contest_start_date.getDate() + 31);
                        }
                    }
                }

                if (!$scope.contest.contest_score_start_date || o == $scope.contest.contest_score_start_date)
                    $scope.contest.contest_score_start_date = n;

                if ($scope.layout == 'create') {
                    $scope.updateStartTime();
                    $scope.updateEndTime();
                }
            });

            $scope.$watch('contest.contest_end_date', function(n, o) {
                if (!$scope.contest.contest_score_end_date || o == $scope.contest.contest_score_end_date)
                    $scope.contest.contest_score_end_date = n;

                $scope.updateEndTime();
                var one_day = 1000 * 60 * 60 * 24;
                var difference_ms = Math.abs($scope.contest.contest_end_date.getTime() - $scope.contest.contest_start_date.getTime());

                if (Math.round(difference_ms / one_day) > 31) {
                    swal({
                        title: $filter('translate')('CONTEST_BRIEF_TITLE'),
                        text: $filter('translate')('CONTEST_BRIEF_MAX_DAYS'),
                        type: 'warning',
                        confirmButtonColor: "#337ab7",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    });
                    $scope.contest.contest_end_date = new Date($scope.contest.contest_start_date);
                    $scope.contest.contest_end_date.setDate($scope.contest.contest_start_date.getDate() + 31);
                }
            });


            setTimeout(function() {
                if ($scope.contest.contest_id) {
                    var cid = $scope.contest.contest_id;
                    if ($cookies[cid + '_facebook'])
                        $scope.contest.customer_connected_to_facebook = 1;
                    if ($cookies[cid + '_twitter'])
                        $scope.contest.customer_connected_to_twitter = 1;
                    if ($cookies[cid + '_youtube'])
                        $scope.contest.customer_connected_to_youtube = 1;

                    $scope.$on('$destroy', function() {
                        window.onbeforeunload = undefined;
                    });
                }
            }, 2000);

            window.onbeforeunload = function() {
                if ($scope.layout == 'edit') {
                    if ($scope.changesFound() && typeof $scope.contest.contest_id != "undefined") {
                        if (typeof $scope.contest.contest_name == "undefined" || $scope.contest.contest_name == "") {
                            $scope.deletePermanently($scope.contest.contest_id);
                            return;
                        }

                        if (typeof $scope.contest.contest_name != "undefined" && $scope.contest.contest_name != "") {
                            // $scope.save();
                            return $filter('translate')('LEAVE_CONTEST_TEXT');
                        }
                    }
                } else if ($scope.layout == 'create') {
                    if (typeof $scope.contest.contest_id != "undefined") {
                        if (typeof $scope.contest.contest_name == "undefined" || $scope.contest.contest_name == "") {
                            $scope.deletePermanently($scope.contest.contest_id);
                            return;
                        }

                        if (typeof $scope.contest.contest_name != "undefined" && $scope.contest.contest_name != "") {
                            // $scope.save();
                            return $filter('translate')('LEAVE_CONTEST_TEXT');
                        }
                    }
                }
            };
        };
		
		function resetCreditCardForm() {
			$(".submit-button").removeAttr("disabled");
			
			$("#card-number").val('');
			$("#card-name").val('');
			$("#card-cvc").val('');
			$("#card-expiry-month").val('');
			$("#card-expiry-year").val('');
		};

        function removeCardPost() {
            backend.post('/dashboard-api/payments/remove-card', {
                    'card': $scope.card
                })
                .success(function(data) {
                    $scope.loading['cc'] = false;
                    $scope.card = {};
					resetCreditCardForm();
					grecaptcha.reset();
                })
                .error(function(err) {
                    $scope.loading['cc'] = false;
                    // $scope.card = cardSource;
                    $scope.paymentError = err.error.message;
                });
        };

        $scope.removeCard = function() {
            var cardSource = angular.copy($scope.card);
            var canCancelCard = true;
            $scope.loading['cc'] = true;
            backend.get('/dashboard-api/contests/get-scheduled')
                .success(function(data) {
                    if (data.length > 0) {
                        swal({
                                title: $filter('translate')('REMOVE_CARD_TITLE'),
                                text: $filter('translate')('REMOVE_CARD_TEXT'),
                                type: 'warning',
                                html: true,
                                confirmButtonColor: "#337ab7",
                                confirmButtonText: "OK",
                                showCancelButton: true,
                                cancelButtonText: $filter('translate')('BTN_CANCEL'),
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    backend.get('/dashboard-api/contests/switch-scheduled-contests')
                                        .success(function(data) {
                                            removeCardPost();
                                        })
                                        .error(function(err) {
                                            $scope.loading['cc'] = false;
                                            $scope.card = cardSource;
                                            $scope.paymentError = err.error.message;
                                            $scope.$apply();
                                        });
                                } else {
                                    $scope.loading['cc'] = false;
                                    $scope.card = cardSource;
                                    $scope.$apply();
                                }
                            });
                    } else {
                        removeCardPost();
                    }
                })
                .error(function(err) {
                    $scope.loading['cc'] = false;
                    $scope.card = cardSource;
                    $scope.paymentError = err.error.message;
                    $scope.$apply();
                });
        }

        $scope.StripeEventHandler = function(status, response) {

            // Grab the form:
            var $form = $('#payment-form');
            // retrieve coupon
            //var coupon_code = $("#card-coupon").val();

            if (response.error) { // Problem!
                // Show the error message above the form
                $scope.paymentError = response.error.message;
                $form.find('.submit-button').prop('disabled', false); // Re-enable submission
            } else { // Token was created!
                $scope.paymentError = '';
                // Get the token ID:
                var token = response.id;
                $scope.loading['cc'] = true;

                backend.post('/dashboard-api/payments/make-customer', {
                    'stripeToken': response.id,
                    'service_type': $scope.contest.contest_service_type,
                    //'coupon': coupon_code,
                    'customer_id': ''
                }).success(function(data)  {
                    $scope.card = data;
                    $scope.loading['cc'] = false;
                    swal($filter('translate')('VERIFIED_CARD_TITLE'), $filter('translate')('VERIFIED_CARD_MESSAGE'), "success");
                }).error(function(err) {
                    $scope.loading['cc'] = false;
                    $form.find('.submit-button').prop('disabled', false); // Re-enable submission
                    if (typeof(err) == 'string')
                        $scope.paymentError = err;
                    else
                        $scope.paymentError = err.error.message;
                });



            }
        }

        $scope.StripeSubmit = function() {
            var error_message = '';
            var locale = backend.getSetting('LOCALE').toLowerCase();

            $('.submit-button').attr("disabled", "disabled");

            if (false == Stripe.card.validateCardNumber($('.card-number').val())) {
                $scope.paymentError = 'CC_INVALID_CARD_NUMBER';
                $(".submit-button").removeAttr("disabled");
                return false;
            }

            if (false == Stripe.card.validateExpiry($('.card-expiry-month').val(), $('.card-expiry-year').val())) {
                $scope.paymentError = "CC_INVALID_DATE_OF_EXPIRY";
                $(".submit-button").removeAttr("disabled");
                return false;
            }

            if (false == Stripe.card.validateCVC($('.card-cvc').val())) {
                $scope.paymentError = "CC_INVALID_CVC";
                $(".submit-button").removeAttr("disabled");
                return false;
            }

            //var $form = $('#payment-form');
            var recaptcha = $('.g-recaptcha-response').val();
            if (recaptcha.length > 0) {
                backend.post('/dashboard-api/payments/check-recaptcha', {'recaptcha': recaptcha})
                    .success(function(data) {
                        if (!data.success) {
                            $scope.paymentError = $filter('translate')('RECAPTCHA_ERROR_MESSAGE');
                            grecaptcha.reset();
                            $(".submit-button").removeAttr("disabled");
                            return false;
                        } else {
							
							var params = {
								number: $('.card-number').val(),
								exp_month: $('.card-expiry-month').val(),
								exp_year: $('.card-expiry-year').val(),
								cvc: $('.card-cvc').val(),
								name: $('.card-holdername').val()
							};

							// Request a token from Stripe:
							Stripe.card.createToken(params, $scope.StripeEventHandler);
							
						}
                    })
                    .error(function(err) {
                        $scope.paymentError = $filter('translate')('RECAPTCHA_ERROR_MESSAGE');
                        grecaptcha.reset();
                        $(".submit-button").removeAttr("disabled");
                        return false;
                    });
            } else {
                $scope.paymentError = $filter('translate')('RECAPTCHA_ACCEPT_MESSAGE');
                grecaptcha.reset();
                $(".submit-button").removeAttr("disabled");
                return false;
            };

			/*
            var params = {
                number: $('.card-number').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year: $('.card-expiry-year').val(),
                cvc: $('.card-cvc').val(),
                name: $('.card-holdername').val()
            };

            // Request a token from Stripe:
            Stripe.card.createToken(params, $scope.StripeEventHandler);

            // Prevent the form from being submitted:
            return false;
			*/
        }
		
		$scope.templatePreview = function (template_id) {
			if ($scope.contest.use_template == 1) {
				
				window.open('http://bmb.buzzmybrand.com/preview?template_id=' + template_id ,'_blank');
				
			} else if ($scope.contest.use_template == 2) {
				
				//convert to md5
				var contest_title_md5 = md5.createHash($scope.contest.contest_name);
				var hashtag_md5 = md5.createHash($scope.createStringByArray($scope.contest.contest_hashtag, ';'));
				
				if ((document.getElementsByName('contest_watermark')[0].files.length > 0) && (document.getElementsByName('contest_image_minisite_bg')[0].files.length > 0)) {
					
					
					var fd = new FormData();
				
					fd.append('contest_watermark', document.getElementsByName('contest_watermark')[0].files[0]);
					
					fd.append('contest_image_minisite_bg', document.getElementsByName('contest_image_minisite_bg')[0].files[0]);
					
					$http.post('/admin/savepreview', fd, {
						transformRequest: angular.identity,
						headers: {'Content-Type': undefined}
					})
					.success(function(data){
						
						if (data.result == "1") {
							//se immagini salvate con successo, aprire nuova tab del browser
							window.open('http://bmb.buzzmybrand.com/preview?template_id=0&canvas_header=' + data.contest_image_minisite_bg + '&font_color=' + $scope.contest.template_title_color.replace('#', '') + '&sponsor_logo=' + data.contest_watermark + '&lang=' + $scope.contest.contest_location + '&title=' + contest_title_md5 + '&hashtag=' + hashtag_md5,'_blank');
						}
						
						
							
					})
					.error(function(data){
						
					});
					
					
				} else if  ($scope.contest.contest_image_minisite_bg && $scope.contest.template_title_color && $scope.contest.contest_watermark) {
					//se immagini salvate con successo, aprire nuova tab del browser
					window.open('http://bmb.buzzmybrand.com/preview?template_id=0&canvas_header=' + $scope.contest.contest_image_minisite_bg + '&font_color=' + $scope.contest.template_title_color.replace('#', '') + '&sponsor_logo=' + $scope.contest.contest_watermark + '&lang=' + $scope.contest.contest_location + '&title=' + contest_title_md5 + '&hashtag=' + hashtag_md5,'_blank');
				}
				
				
			}
		}
		
		$scope.setFile = function(element) {
			
			var filename = element.files[0].name;
			console.log(filename.length)
			var index = filename.lastIndexOf(".");
			var strsubstring = filename.substring(index, filename.length);
			if (strsubstring == '.pdf')
			{
			  console.log('File Uploaded sucessfully');
			  $scope.contest.valid_tc = true;
			}
			else {
				//element.files[0] = '';
				$scope.contest.valid_tc = false;
				//console.log('please upload correct File Name, File extension should be .pdf'
			}
			
		};
		
		$scope.check_TandC = function() {
			if ($scope.contest.use_standard_tc == 0) {
				$scope.contest.contest_term_cond_company_sponsoring = '';
			}
			else if ($scope.contest.use_standard_tc == 1) {
				$scope.contest.contest_term_conditions = '';
				//$scope.contest.terms_conditions = '';
			}
			$scope.valid_tc = true;	
		}

        auth.user().then(init);

    }])
    .directive('onlyNum', function() {
        return function(scope, element, attrs) {

            var keyCode = [8, 9, 37, 39, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110];
            element.bind("keydown", function(event) {
                if ($.inArray(event.which, keyCode) == -1) {
                    scope.$apply(function() {
                        scope.$eval(attrs.onlyNum);
                        event.preventDefault();
                    });
                    event.preventDefault();
                }

            });
        };
    })
    .directive('customLogo', function() {
        return {
            restrict: 'E',
            templateUrl: 'app/contest-edit/custom-logo.html'
        }
    })
    .directive('fileInput', function() {
        return {
            restrict: 'E',
            templateUrl: 'app/contest-edit/file-input.html',
            scope: {
                contest: '=contest',
                id: '=id',
                name: '=name',
                model: '=myModel',
                file_type: '=fileType',
                watch: '=alsoWatch',
                label: '=label',
                required: '=myRequired',
                myForm: '=myForm',
                helpText: '=helpText',
                imageWidth: '=imageWidth',
                imageHeight: '=imageHeight',
                imageMaxWidth: '=imageMaxWidth',
                imageMaxHeight: '=imageMaxHeight',
                imageMinWidth: '=imageMinWidth',
                imageMinHeight: '=imageMinHeight',
                imageWh: '=imageWh',
                imageBgWh: '=imageBgWh',
                imageLogo: '=imageLogo',
                imageBg: '=imageBg',
                imageFlyer: '=imageFlyer'
            },
            link: function(scope, element) {
                scope.browse = function() {
                    $('#id-' + scope.id).click();
                }
            }
        }
    })
    .directive('fileModel', ['$parse', function($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function() {
                    scope.$apply(function() {
                        if (element[0].files.length > 1) {
                            modelSetter(scope, element[0].files);
                        } else {
                            modelSetter(scope, element[0].files[0]);
                        }
                    });
                });
            }
        };
    }]);