'use strict';

angular.module('dashboard')
  .controller('ApproveCtrl', ["$scope", "backend", "$routeParams", "contestUtils", function ($scope, backend, $routeParams, contestUtils) {
    
    var c_id = $routeParams['id'];
    var contestType = null;

    //$scope.subtitle = 'CONTEST_APPROVE_SUBTITLE';
    $scope.contest = null;
    $scope.analytics = null;
    $scope.posts = null;
    $scope.error = null;
    $scope.isBusy = false;
    $scope.currentTab = 'approve';
    $scope.currentSubTab = 1; 
    $scope.nextPage = 1;
    $scope.utils = contestUtils;

    $scope.resetPosts = function()
    {
      $scope.posts = null;
      $scope.nextPage = 1;
      $scope.loadPosts();
    }

    $scope.setSubTab = function(id) 
    {
      if(id == $scope.currentSubTab) 
        return;

      $scope.currentSubTab = id;
      // Reset and reload posts
      $scope.resetPosts();
    }

    $scope.deletePost = function(id)
    {
      angular.element('#post-cover-' + id).addClass('loading');
      // TODO: implement rejection
      // if(status == 2) {
      //   $scope.setError("Disapprove not implemented");
      //   return;
      // }

      backend.post('/admin/delete-' + $scope.utils.getContestTypeName(contestType) + '-from-contest/' + id)
        .success(function(data) {
          angular.element('post-cover-' + id).removeClass('loading');
          if(data.result == 0) {
            $scope.setError(data.message);
            return;
          }

          $scope.resetPosts();
        })
        .error(function(err) {
          angular.element('#post-cover-' + id).removeClass('loading');
          $scope.setError(err.error.message);
        });
    }

    $scope.setApprovedStatus = function(id, status)
    {
      angular.element('#post-cover-' + id).addClass('loading');
      // TODO: implement rejection
      // if(status == 2) {
      //   $scope.setError("Disapprove not implemented");
      //   return;
      // }

      backend.post('/admin/approve-' + $scope.utils.getContestTypeName(contestType) + '/' + id,
      {
        approvalStep: status
      })
        .success(function(data) {
          angular.element('post-cover-' + id).removeClass('loading');
          if(data.result == 0) {
            $scope.setError(data.message);
            return;
          }

          $scope.resetPosts();
        })
        .error(function(err) {
          angular.element('#post-cover-' + id).removeClass('loading');
          $scope.setError(err.error.message);
        });
    }

    $scope.setError = function(message)
    {
      $scope.error = message;
      setTimeout(function() {
        $scope.error = null;
        $scope.$apply();
      }, 3000);
    }

    $scope.loadPosts = function()
    {
      // Avoid errors if we're already at the last page
      if($scope.nextPage === null) return;

      if($scope.isBusy === true) return; // request in progress, return
      $scope.isBusy = true;

      backend.get('/admin/' + $scope.utils.getContestTypeName(contestType) + 's-of-contest/' + c_id, {
        params: {
          page: $scope.nextPage,
          col: 4,
          approvation_id: $scope.currentSubTab
        }
      })
        .success(function(data) {
          if(data.length > 0) {
            if($scope.posts === null)
              $scope.posts = [];
            // Append to the correct list
            $scope.posts = $scope.posts.concat(data);
          }
          if(data.length < 4)
            // We're at the last page
            $scope.nextPage = null;
          else
            // Increment page
            $scope.nextPage++;

          if ((data.length == 0) && ($scope.posts === null)) {
			  $scope.posts = [];
		  }
		              
            
          $scope.isBusy = false;
          return;
        });
    }

    $scope.notifyAll = function()
    {
      $scope.setError('notifyAll() NOT IMPLEMENTED');
    }

    $scope.notify = function(post)
    {
      $scope.setError('notify() NOT IMPLEMENTED');
    }

    // Load basic contest data
    backend.get('/admin/contest-detail/' + c_id)
      .success(function(data) {
        $scope.utils.checkAccess(data, $scope.currentTab);
        
        $scope.contest = data;
        $scope.lastSaved = angular.copy($scope.contest);
        contestType = data['contest_type'];
        // Load the first page of posts
        $scope.loadPosts();
      })
      .error(function(err) {
        $scope.contest = {};
        $scope.error = err.message;
      });
      
  }]);