'use strict';

angular.module('dashboard')
.directive('notifyAll', ["$templateRequest", "$compile", "backend", function($templateRequest, $compile, backend) {
    return {
      restrict: 'E',
      templateUrl: 'app/notify-all/index.html',

      link: function(scope, element, attrs) {

        scope.notificationError = null;

        scope.sendMassNotification = function(notification)
        {
          angular.element('.modal-body').addClass('loading');
		  notification.href = 'minisite/' + scope.contest.contest_id;
          backend.post('/admin/send-mass-notification-ft/' + scope.contest.contest_id, notification)
          .success(function(data) {
            angular.element('.modal-body').removeClass('loading');
            if(data.result == 1) {
              angular.element('#notify-all-modal').modal('hide');
            } else {
              scope.error = data.message;
            }
            //scope.$apply();
          })
          .catch(function(err) {
            angular.element('.modal-body').removeClass('loading');
            scope.notificationError = err.statusText;
            scope.$apply();
          });
        }

      }
    };
  }]
);