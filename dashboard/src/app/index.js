'use strict';

var app = angular.module('dashboard', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ngRoute', 'pascalprecht.translate', 'ui.bootstrap', 'ui.select', 'ngSanitize', 'infinite-scroll', 'noCAPTCHA', 'flow', 'ngTagsInput', 'angular-md5'])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        redirectTo: '/contests'
      })
      .when('/contests', {
        templateUrl: 'app/contest-list/index.html',
        controller: 'ContestListCtrl',
        controllerAs: 'clist'
      })
      .when('/contests/:id/analytics', {
        templateUrl: 'app/analytics/index.html',
        controller: 'AnalyticsCtrl',
        controllerAs: 'anal'
      })
      .when('/contests/:id/approve', {
        templateUrl: 'app/approve/index.html',
        controller: 'ApproveCtrl',
      })
      .when('/contests/:id/edit', {
        templateUrl: 'app/contest-edit/index.html',
        controller: 'ContestEditCtrl',
      })
      /*
      .when('/contests/:id/promotion', {
        templateUrl: 'app/contest-promotion/index.html',
        controller: 'ContestPromotionCtrl',
      })
    */
      .when('/launch', {
        templateUrl: 'app/contest-edit/index.html',
        controller: 'ContestEditCtrl',
        reloadOnSearch: false
      })
      .when('/company', {
        templateUrl: 'app/company/index.html',
        controller: 'CompanyCtrl',
        controllerAs: 'company'
      })
      .when('/payments', {
        templateUrl: 'app/payments/index.html',
        controller: 'PaymentsCtrl',
        controllerAs: 'payments'
      })
      .otherwise({
        templateUrl: '404.html'
      });
  })
  .config(['googleGrecaptchaProvider', function (googleGrecaptchaProvider) {
    googleGrecaptchaProvider.setLanguage(LOCALE);
  }
  ])
  .config(['noCAPTCHAProvider', function (noCaptchaProvider) {
    noCaptchaProvider.setTheme('light');
  }
  ])
  .config(function($translateProvider) {
    $translateProvider.useSanitizeValueStrategy('escaped');
    $translateProvider.preferredLanguage(LOCALE);
    // $translateProvider.determinePreferredLanguage();
    $translateProvider.translations('en', _LANG_EN);
    $translateProvider.translations('it', _LANG_IT);
    $translateProvider.translations('ar', _LANG_AR);
    $translateProvider.translations('es', _LANG_ES);
    $translateProvider.translations('fr', _LANG_FR);
    $translateProvider.translations('ru', _LANG_RU);

    // $translateProvider.useStaticFilesLoader({
    //     prefix: '//www.buzzmybrand.it/dashboard-api/utils/translation-strings/',
    //     suffix: ''
    // });
  })
  .directive('pwCheck', [function () {
    return {
      require: 'ngModel',
      scope: {
        otherModelValue: '=pwCheck'
      },
      link: function (scope, elem, attrs, ngModel) {
        ngModel.$validators.compareTo = function(modelValue) {
          return modelValue === scope.otherModelValue;
        };

        scope.$watch("otherModelValue", function() {
          ngModel.$validate();
        });
      }
    }
  }])
  .directive('contestTabsNav', function() {
    return {
      restrict: 'E',
      templateUrl: 'app/components/contest-tabs-nav.html'
    }
  })
  .directive('companyInfo', function() {
    return {
      restrict: 'E',
      templateUrl: 'app/components/company-info.html'
    }
  })
  .directive('analyticsBox', function() {
    return {
      restrict: 'E',
      templateUrl: 'app/analytics/analytics-box.html',
      scope: {
        'contest': '=',
        'value': '=',
        'label': '=',
        'size': '=',
        'icon': '=',
        'color': '=',
        'help': '=',
        'percent': '='
      }
    }
  })
  .directive('post', ["backend", function(backend) {
    return {
      restrict: 'E',
      templateUrl: 'app/components/post.html',
      link: function(scope, elem, attrs, nacled) {
        scope.facebook_notificationError = null;
        scope.twitter_notificationError = null;
        scope.notification = {};

        scope.sendPostNotification = function(notification, post_id)
        {
          angular.element('.modal-body').addClass('loading');
          var targets = Array();
          if(notification.notify_facebook)
            targets.push('facebook');
          if(notification.notify_twitter)
            targets.push('twitter');

          notification.content_type = scope.contest.contest_type;
		  notification.href = 'minisite/' + scope.contest.contest_id;

          angular.forEach(targets, function(v, k) {
            backend.post('/admin/send-' + v + '-notification/' + post_id, notification)
            .success(function(data) {
              angular.element('.modal-body').removeClass('loading');
              if(data.result == 1) {
                angular.element('#notify-post-modal-' + post_id).modal('hide');
              } else {
                scope[v + '_notificationError'] = data.message;
              }
            })
            .catch(function(err) {
              angular.element('.modal-body').removeClass('loading');
              scope[v + '_notificationError'] = err.statusText;
            });
          });
          //scope.$apply();
        }
      }
    }
  }]);
  