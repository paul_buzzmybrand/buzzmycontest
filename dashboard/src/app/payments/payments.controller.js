'use strict';

angular.module('dashboard')
  .controller('PaymentsCtrl', ["$scope", "$filter", "$rootScope", "backend", "$translate", "$http", "$timeout", function ($scope, $filter, $rootScope, backend, $translate, $http, $timeout) {

    $scope.card = null;
    $scope.paymentError = null;
    $scope.credits = null;
    $scope.invoices = null;

    $scope.loading = {
      credits: true,
      invoices: true,
      cc: true
    }

    $scope.isEmpty = function (obj) {
       return angular.equals({},obj); 
    };

    // Get user credits
    backend.api({
      url: '/dashboard-api/credits/credits'
    }).success(function(data) {
      $scope.credits = data;
    });

    // Get invoices
    backend.api({
      url: '/dashboard-api/payments/invoices'
    }).success(function(data) {
        $scope.invoices = data;
    });
}]);