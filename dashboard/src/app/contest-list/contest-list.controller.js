'use strict';

angular.module('dashboard')
  .controller('ContestListCtrl', ['$scope', 'backend', 'contestUtils', '$filter', function ($scope, backend, contestUtils, $filter) {
      $scope.params = {
        status: '', // Default is all
        search: ''
      };
	  /*
	  auth.user().then(function(response) {
		  $scope.company_id = response.data.company_id;)
	  };
		*/
      $scope.error = null;
      $scope.contests = null;
      $scope.utils = contestUtils;
      $scope.baseUrl = backend.baseUrl;
	  //$scope.isAdmin = false;

      $scope.setFilter = function(value)
      {
        $scope.params.status = value;
      }
	  
	  $scope.location_host = function()
      {
          return location.host.replace('www.', '');
      }

      var setError = function(message) 
      {
        $scope.error = message;
        setTimeout(function() {
          $scope.error = null;
          $scope.$apply();
        }, 5000);
      }
	  /*
	$scope.user_is_admin = function() {

		  return backend.api({
				'method': 'GET', 
				'url': '/dashboard-api/utils/current-user'
			})
			.success(function(data, status) {
				if(typeof(data) !== 'object') {
					redirectToLogin();
					return;
				}
				// Cache data
                //_user = data;				
				//$scope.isAdmin = data.is_admin;
				if (data.is_admin)
					return true;
				else
					return false;
				

				
			})
			.error(function(err, status) {
				redirectToLogin();
				
			})
			.catch(function(err) {
				redirectToLogin();
				
			});
		}
*/
      $scope.deleteContest = function(c_id)
      {
        swal({
            title: $filter('translate')('DELETE_CONTEST_TITLE'),
            text: $filter('translate')('DELETE_CONTEST_TEXT'),
            type: 'warning',
            html: true,
            confirmButtonColor: "#337ab7",
            confirmButtonText: "OK",
            showCancelButton: true,
            cancelButtonText: $filter('translate')('BTN_CANCEL'),
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                angular.element('#contest-box-' + c_id).addClass('loading');
                backend.post('/admin/delete-contest/' + c_id)
                  .success(function(data) {
                    if(data.result == '0') {
                      setError(data.message);
                      angular.element('#contest-box-' + c_id).removeClass('loading');
                      return;
                    }
                    // Remove the contest from the list
                    $scope.contests = $scope.contests.filter(function(el) { console.log(el.contest_id, c_id); return el.contest_id != c_id });
                  })
                  .error(function(err) {
                    setError(err.message);
                    angular.element('#contest-box-' + c_id).removeClass('loading');
                    return;
                  });
            }
        });
      }
	  
	  	  

      var getContests = function()
      {
        $scope.contests = null;
        // Avoid altering $scope.params by backend.get
        var params = angular.copy($scope.params);

        backend.get('/admin/contest-list/' + $scope.companyId, {params: params}) // 
          .success(function(data) {
            if(data.result == 0) {
              setError(data.message);
            } else {
              $scope.error = null;
              $scope.contests = data;
            }
          })
          .error(function(err) {
            $scope.error = err.error.message;
          });
      }
	  
	  $scope.openMiniWebSite = function(contest_route)
	  {		  
		window.open('https://bmb.' + host + '/' + contest_route , '_blank');
          
	  }
	  
	  $scope.createStringByArray = function(array, separator) {
			var output = '';
			angular.forEach(array, function (object) {
				angular.forEach(object, function (value, key) {
					//output += key + ',';
					output += value + separator;
				});
			});
			output = output.substring(0, output.length - 1);
			return output;
		}
	  
	  

      $scope.$watch('params', function(newValue, oldValue) {
        getContests();
      }, true);

  }]);